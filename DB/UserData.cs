﻿// Decompiled with JetBrains decompiler
// Type: DB.UserData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserData : BaseData
  {
    private string _language = string.Empty;
    private string _userName = string.Empty;
    private string _languageGroup = string.Empty;
    private ArrayList _unlockPortraits = new ArrayList();
    [JsonIgnore]
    public Dictionary<int, int> settings = new Dictionary<int, int>();
    [JsonIgnore]
    public Dictionary<long, int> mods = new Dictionary<long, int>();
    public const long INVALID_ID = 0;
    public const string USER_PORTRAIT_ICON_PATH_PRE = "Texture/Hero/Portrait_Icon/player_portrait_icon_";
    private long _ctime;
    private int _title;
    private int _dragon_power;
    private long _copper_coin;
    private long _dragon_coin;
    private int _monster_level;
    [JsonIgnore]
    public bool online;
    [JsonIgnore]
    public long lastLogin;
    private long _uid;
    private long _allianceId;
    private long _power;
    private int _portrait;
    private long _auctionNormalGold;
    private long _auctionHiddenGold;
    private long _worldAuctionGold;
    private long _merlinCoin;
    private int _lordTitle;
    private int _acGroupId;
    private long _acAllianceId;
    private int _acEndTime;
    [JsonIgnore]
    public Currency currency;
    [JsonIgnore]
    public int giftMode;
    [JsonIgnore]
    public DailyStreak dailyStreak;
    [JsonIgnore]
    public long vipPoint;
    [JsonIgnore]
    public long vipJobId;
    private int _troopsKilled;
    private long _channelId;
    private string _toturialStep;
    private string _signature;
    private int _spirit;
    private long _spirit_recover_time;
    private int _skillPoint;
    private long _skillPointRecoverTime;
    private int _timeChestUpdateTime;
    private int _world_id;
    private int _current_world_id;
    private long _icon_cd;
    private string _icon;
    private int _icon_status;
    private int _shutup_time;
    private int _mute_admin;
    private int _current_score;
    private bool singleValueChangeFlag;
    private bool tmpJobExistFlag;

    public UserData()
    {
      this._uid = 0L;
      this._allianceId = 0L;
    }

    public long key
    {
      get
      {
        return this._uid;
      }
    }

    public long CTime
    {
      get
      {
        return this._ctime;
      }
    }

    public int Title
    {
      get
      {
        return this._title;
      }
    }

    public string TitleIconPath
    {
      get
      {
        return Utils.GetUserTitleIconPath(this.Title);
      }
    }

    public string TitleBgPath
    {
      get
      {
        return Utils.GetUserTitleBgPath(this.Title);
      }
    }

    public int dragon_power
    {
      get
      {
        return this._dragon_power;
      }
    }

    public string language
    {
      get
      {
        return this._language;
      }
    }

    public long copper_coin
    {
      get
      {
        return this._copper_coin;
      }
    }

    public long dragon_coin
    {
      get
      {
        return this._dragon_coin;
      }
    }

    public int monster_level
    {
      get
      {
        return this._monster_level;
      }
    }

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    [JsonIgnore]
    public long allianceId
    {
      get
      {
        return this._allianceId;
      }
      set
      {
        this._allianceId = value;
      }
    }

    public string userName
    {
      set
      {
        this._userName = value;
      }
      get
      {
        return this._userName;
      }
    }

    public string languageGroup
    {
      get
      {
        return this._languageGroup;
      }
      set
      {
        this._languageGroup = value;
      }
    }

    public string groupName
    {
      get
      {
        if (!string.IsNullOrEmpty(this._languageGroup))
          return Utils.XLAT(ConfigManager.inst.DB_KingdomConfig.languageGroupKeys[int.Parse(this._languageGroup)]);
        return (string) null;
      }
    }

    public string userName_Kingdom_Alliance_Name
    {
      get
      {
        string str = "K" + (object) this._world_id + " - ";
        if (DBManager.inst != null && DBManager.inst.DB_Alliance != null && this.allianceId != 0L)
        {
          AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.allianceId);
          if (allianceData != null)
            str = str + "(" + allianceData.allianceAcronym + ")";
        }
        return str + this.userName;
      }
    }

    public string userName_Alliance_Name
    {
      get
      {
        string str = string.Empty;
        if (DBManager.inst != null && DBManager.inst.DB_Alliance != null && this.allianceId != 0L)
        {
          AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.allianceId);
          if (allianceData != null)
            str = str + "[" + allianceData.allianceAcronym + "]";
        }
        return str + this.userName;
      }
    }

    public long power
    {
      get
      {
        return this._power;
      }
    }

    public int portrait
    {
      get
      {
        return this._portrait;
      }
      set
      {
        this._portrait = value;
      }
    }

    public long AuctionNormalGold
    {
      get
      {
        return this._auctionNormalGold;
      }
    }

    public long AuctionHiddenGold
    {
      get
      {
        return this._auctionHiddenGold;
      }
    }

    public long WorldAuctionGold
    {
      get
      {
        return this._worldAuctionGold;
      }
    }

    public long MerlinCoin
    {
      get
      {
        return this._merlinCoin;
      }
    }

    public int LordTitle
    {
      get
      {
        return this._lordTitle;
      }
    }

    public int AcGroupId
    {
      set
      {
        this._acGroupId = value;
      }
      get
      {
        return this._acGroupId;
      }
    }

    public long AcAllianceId
    {
      set
      {
        this._acAllianceId = value;
      }
      get
      {
        return this._acAllianceId;
      }
    }

    public int AcEndTime
    {
      set
      {
        this._acEndTime = value;
      }
      get
      {
        return this._acEndTime;
      }
    }

    public bool IsPortraitUnlocked(int portrait)
    {
      if (this._unlockPortraits != null)
      {
        for (int index = 0; index < this._unlockPortraits.Count; ++index)
        {
          if (portrait.ToString() == this._unlockPortraits[index].ToString())
            return true;
        }
      }
      return false;
    }

    public string PortraitIconPath
    {
      get
      {
        return "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) this._portrait;
      }
    }

    public static string GetPortraitIconPath(int portrait)
    {
      return "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) portrait;
    }

    public string PortraitPath
    {
      get
      {
        return "Texture/Hero/player_portrait_" + (object) this._portrait;
      }
    }

    public static string BuildPortraitPath(int portrait)
    {
      return "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) portrait;
    }

    public int troopsKilled
    {
      get
      {
        return this._troopsKilled;
      }
    }

    public long channelId
    {
      get
      {
        return this._channelId;
      }
    }

    public string toturialStep
    {
      get
      {
        return this._toturialStep;
      }
    }

    public string signature
    {
      get
      {
        return this._signature;
      }
    }

    public int spirit
    {
      get
      {
        return this._spirit;
      }
    }

    public long spirit_recover_time
    {
      get
      {
        return this._spirit_recover_time;
      }
    }

    [JsonIgnore]
    public int skillPoint
    {
      get
      {
        return this._skillPoint;
      }
      set
      {
        this._skillPoint = value;
      }
    }

    public long skillPointRecoverTime
    {
      get
      {
        return this._skillPointRecoverTime;
      }
    }

    public int timeChestUpdateTime
    {
      get
      {
        return this._timeChestUpdateTime;
      }
    }

    public int world_id
    {
      get
      {
        return this._world_id;
      }
    }

    public int current_world_id
    {
      get
      {
        if (this._current_world_id == 0)
          return this._world_id;
        return this._current_world_id;
      }
    }

    public bool IsForeigner
    {
      get
      {
        if (this.IsPit)
          return this._world_id != PlayerData.inst.userData.world_id;
        if (this._current_world_id != 0)
          return this._current_world_id != this._world_id;
        return false;
      }
    }

    public bool IsPit
    {
      get
      {
        return this._current_world_id > 1000000;
      }
    }

    [JsonIgnore]
    public long IconCd
    {
      set
      {
        this._icon_cd = value;
      }
      get
      {
        return this._icon_cd;
      }
    }

    public string Icon
    {
      set
      {
        this._icon = value;
      }
      get
      {
        return this._icon;
      }
    }

    public int IconStatus
    {
      set
      {
        this._icon_status = value;
      }
      get
      {
        return this._icon_status;
      }
    }

    public bool IsKing
    {
      get
      {
        return this.Title == 1;
      }
    }

    public int ShutUpTime
    {
      set
      {
        this._shutup_time = value;
      }
      get
      {
        return this._shutup_time;
      }
    }

    public bool isMuteAdmin
    {
      get
      {
        return this._mute_admin == 1;
      }
    }

    public int CurrentScore
    {
      get
      {
        return this._current_score;
      }
    }

    public void Set(long inUid, string inName, int inPower, int inProtrait, long inAllianceId)
    {
      this._uid = inUid;
      this._userName = inName;
      this._power = (long) inPower;
      this._portrait = inProtrait;
      this._allianceId = inAllianceId;
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (updateTime < this._updateTime || orgData == null || long.TryParse(orgData.ToString(), out this._uid))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "name", ref this._userName) | DatabaseTools.UpdateData(inData, "portrait", ref this._portrait) | DatabaseTools.UpdateData(inData, "language_group", ref this._languageGroup) | DatabaseTools.UpdateData(inData, "normal_gold", ref this._auctionNormalGold) | DatabaseTools.UpdateData(inData, "hidden_gold", ref this._auctionHiddenGold) | DatabaseTools.UpdateData(inData, "world_auction_gold", ref this._worldAuctionGold) | DatabaseTools.UpdateData(inData, "merlin_coin", ref this._merlinCoin) | DatabaseTools.UpdateData(inData, "lord_title", ref this._lordTitle) | DatabaseTools.UpdateData(inData, "ac_group_id", ref this._acGroupId) | DatabaseTools.UpdateData(inData, "ac_alliance_id", ref this._acAllianceId) | DatabaseTools.UpdateData(inData, "ac_end_time", ref this._acEndTime);
      this.singleValueChangeFlag = false;
      long power = this._power;
      bool flag2 = flag1 | (this.singleValueChangeFlag = DatabaseTools.UpdateData(inData, "power", ref this._power));
      if (this.singleValueChangeFlag && this.uid == PlayerData.inst.uid)
        GameEngine.Instance.events.Send_OnPlayerStatChanged(new Events.PlayerStatChangedArgs(Events.PlayerStat.Power, (double) power, (double) this._power));
      bool flag3 = flag2 | this.currency.Decode((object) inData) | DatabaseTools.UpdateData(inData, "gift_mode", ref this.giftMode);
      this.dailyStreak.Decode(inData[(object) "daily_streak"]);
      bool flag4 = flag3 | (this.singleValueChangeFlag = DatabaseTools.UpdateData(inData, "vip_points", ref this.vipPoint));
      this.singleValueChangeFlag = false;
      this.tmpJobExistFlag = this.vipJobId != 0L;
      bool flag5 = flag4 | DatabaseTools.UpdateData(inData, "vip_job_id", ref this.vipJobId);
      if (this.singleValueChangeFlag && this.uid == PlayerData.inst.uid)
        GameEngine.Instance.events.Send_OnPlayerVIPStateChanged(new Events.PlayerVIPStatusChanged(this.tmpJobExistFlag, this.vipJobId != 0L, this.vipJobId));
      bool flag6 = flag5 | DatabaseTools.UpdateData(inData, "troops_killed", ref this._troopsKilled) | DatabaseTools.UpdateData(inData, "chat_channel", ref this._channelId) | DatabaseTools.UpdateData(inData, "tutorial", ref this._toturialStep) | DatabaseTools.UpdateData(inData, "signature", ref this._signature) | DatabaseTools.UpdateData(inData, "spirit", ref this._spirit) | DatabaseTools.UpdateData(inData, "spirit_recover_time", ref this._spirit_recover_time) | DatabaseTools.UpdateData(inData, "monster_level", ref this._monster_level) | DatabaseTools.UpdateData(inData, "skill_point", ref this._skillPoint) | DatabaseTools.UpdateData(inData, "skill_point_recover_time", ref this._skillPointRecoverTime) | DatabaseTools.UpdateData(inData, "dragon_power", ref this._dragon_power) | DatabaseTools.UpdateData(inData, "language", ref this._language) | DatabaseTools.UpdateData(inData, "last_login", ref this.lastLogin) | DatabaseTools.UpdateData(inData, "copper_coin", ref this._copper_coin) | DatabaseTools.UpdateData(inData, "dragon_coin", ref this._dragon_coin) | DatabaseTools.UpdateData(inData, "title", ref this._title) | DatabaseTools.UpdateData(inData, "world_id", ref this._world_id) | DatabaseTools.UpdateData(inData, "current_world_id", ref this._current_world_id) | DatabaseTools.UpdateData(inData, "icon", ref this._icon) | DatabaseTools.UpdateData(inData, "icon_cd", ref this._icon_cd) | DatabaseTools.UpdateData(inData, "review_icon_status", ref this._icon_status) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "shutup_time", ref this._shutup_time) | DatabaseTools.UpdateData(inData, "mute_admin", ref this._mute_admin) | DatabaseTools.UpdateData(inData, "daily_point", ref this._current_score);
      if (inData.ContainsKey((object) "unlock_portrait") && inData[(object) "unlock_portrait"] != null && this._unlockPortraits != null)
        flag6 |= DatabaseTools.CheckAndParseOrgData(inData[(object) "unlock_portrait"], out this._unlockPortraits);
      if (inData.ContainsKey((object) "daily_point") && this._uid == PlayerData.inst.uid)
        DBManager.inst.DB_DailyActives.UpdateDailyScore(inData[(object) "daily_point"] as Hashtable);
      if (inData.ContainsKey((object) "notification_config"))
        NotificationManager.Instance.SetConfig(inData[(object) "notification_config"]);
      if (inData.ContainsKey((object) "setting"))
      {
        Hashtable hashtable = inData[(object) "setting"] as Hashtable;
        if (hashtable != null)
        {
          this.settings.Clear();
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
            this.settings.Add(int.Parse(enumerator.Key.ToString()), int.Parse(enumerator.Value.ToString()));
        }
      }
      if (inData.ContainsKey((object) "mod"))
      {
        this.mods.Clear();
        Hashtable hashtable = inData[(object) "mod"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
            this.mods.Add(long.Parse(enumerator.Key.ToString()), int.Parse(enumerator.Value.ToString()));
        }
      }
      if (this._skillPoint > 20)
        this._skillPoint = 20;
      return flag6 | DatabaseTools.UpdateData(inData, "timer_chest_active_time", ref this._timeChestUpdateTime);
    }

    public bool IsSettingOn(int key)
    {
      int num = 0;
      if (this.settings.TryGetValue(key, out num))
        return num == 1;
      return true;
    }

    public bool InMod(long uid)
    {
      if (this.mods.ContainsKey(uid))
        return this.mods[uid] > NetServerTime.inst.ServerTimestamp;
      return false;
    }

    public string ShowInfo()
    {
      return "<color=#FFFF00>User Data " + (object) this.uid + " </color>\n" + "name : " + this.userName + "\n" + "power : " + (object) this.power + "\n" + "alliance_id : " + (object) this.allianceId + "\n" + "channel :" + (object) this.channelId + "\n";
    }

    public bool IsShutUp
    {
      get
      {
        return this.ShutUpTime > NetServerTime.inst.ServerTimestamp;
      }
    }

    public int ShutUpLeftTime
    {
      get
      {
        return this.ShutUpTime - NetServerTime.inst.ServerTimestamp;
      }
    }

    public static class IconStatusValue
    {
      public const int HAVE_NOT_UPLOAD = 0;
      public const int UPLOAD_REFUSED = 1;
      public const int UPLOADING = 2;
      public const int UPLOAD_CONFIRMED = 3;
    }

    public struct Params
    {
      public const string KEY = "user_info";
      public const string UID = "uid";
      public const string LANGUAGE_GROUP = "language_group";
      public const string ALLIANCE_ID = "alliance_id";
      public const string USER_NAME = "name";
      public const string POWER = "power";
      public const string PORTRAIT = "portrait";
      public const string GIFT_MODE = "gift_mode";
      public const string DAILY_STREAK = "daily_streak";
      public const string VIP_POINTS = "vip_points";
      public const string VIP_JOB_ID = "vip_job_id";
      public const string TROOP_KILLED = "troops_killed";
      public const string CHANNEL_ID = "chat_channel";
      public const string TOTURAL = "tutorial";
      public const string SIGNATURE = "signature";
      public const string SPIRIT = "spirit";
      public const string SPIRIT_RCOVER_TIME = "spirit_recover_time";
      public const string SKILL_POINT = "skill_point";
      public const string SKILL_RECOVER_TIME = "skill_point_recover_time";
      public const string MONSTER_LEVEL = "monster_level";
      public const string TIME_CHEST_UPDATE_TIME = "timer_chest_active_time";
      public const string DAILY_POINT = "daily_point";
      public const string DRAGON_POWER = "dragon_power";
      public const string LANGUAGE = "language";
      public const string LAST_LOGIN = "last_login";
      public const string NOTIFICATION = "notification_config";
      public const string SETTING = "setting";
      public const string COPPER_COIN = "copper_coin";
      public const string DRAGON_COIN = "dragon_coin";
      public const string TITLE = "title";
      public const string WORLD_ID = "world_id";
      public const string CURRENT_WORLD_ID = "current_world_id";
      public const string ICON = "icon";
      public const string ICON_CD = "icon_cd";
      public const string ICON_STATUS = "review_icon_status";
      public const string CTIME = "ctime";
      public const string SHUTUP_TIME = "shutup_time";
      public const string MUTE_ADMIN = "mute_admin";
      public const string MOD = "mod";
      public const string UNLOCK_PORTRAIT = "unlock_portrait";
      public const string AUCTION_NORMAL_GOLD = "normal_gold";
      public const string AUCTION_HIDDEN_GOLD = "hidden_gold";
      public const string WORLD_AUCTION_GOLD = "world_auction_gold";
      public const string MERLIN_COIN = "merlin_coin";
      public const string LORD_TITLE = "lord_title";
      public const string AC_GROUP_ID = "ac_group_id";
      public const string AC_ALLIANCE_ID = "ac_alliance_id";
      public const string AC_END_TIME = "ac_end_time";
    }
  }
}
