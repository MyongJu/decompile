﻿// Decompiled with JetBrains decompiler
// Type: DB.ChatRoomMember
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class ChatRoomMember : BaseData
  {
    public const string USER_PORTRAIT_ICON_PATH_PRE = "Texture/Hero/Portrait_Icon/player_portrait_icon_";
    private long _roomId;
    private long _uid;
    private string _name;
    private int _portrait;
    private long _channelId;
    private int _title;
    private int _status;

    public long roomId
    {
      get
      {
        return this._roomId;
      }
    }

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public string userName
    {
      get
      {
        return this._name;
      }
    }

    public int portrait
    {
      get
      {
        return this._portrait;
      }
    }

    public long channelId
    {
      get
      {
        return this._channelId;
      }
    }

    public string PortraitIconPath
    {
      get
      {
        return "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) this._portrait;
      }
    }

    public int titleIndex
    {
      get
      {
        return this._title;
      }
    }

    public ChatRoomMember.Title title
    {
      get
      {
        return (ChatRoomMember.Title) this._title;
      }
    }

    public ChatRoomMember.Status status
    {
      get
      {
        return (ChatRoomMember.Status) this._status;
      }
    }

    public bool IsNormalMember()
    {
      if (this.title != ChatRoomMember.Title.admin && this.title != ChatRoomMember.Title.member)
        return this.title == ChatRoomMember.Title.owner;
      return true;
    }

    public bool IsManageMember()
    {
      if (this.title != ChatRoomMember.Title.admin)
        return this.title == ChatRoomMember.Title.owner;
      return true;
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "room_id", ref this._roomId) | DatabaseTools.UpdateData(dataHt, "uid", ref this._uid) | DatabaseTools.UpdateData(dataHt, "name", ref this._name) | DatabaseTools.UpdateData(dataHt, "portrait", ref this._portrait) | DatabaseTools.UpdateData(dataHt, "title", ref this._title) | DatabaseTools.UpdateData(dataHt, "status", ref this._status) | DatabaseTools.UpdateData(dataHt, "channelId", ref this._channelId);
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Chat memeber : " + (object) this.uid + "\n" + str + "roomId : " + (object) this.roomId + "\n" + str + "name : " + this.userName + "\n" + str + "title : " + (object) this.title + "\n" + str + "status : " + (object) this.status + "\n";
    }

    public struct Params
    {
      public const string KEY = "chat_room_roster";
      public const string ROOM_ID = "room_id";
      public const string UID = "uid";
      public const string USERNAME = "name";
      public const string PORTRAIT = "portrait";
      public const string TITLE = "title";
      public const string STATUS = "status";
      public const string CHANNEL_ID = "channelId";
    }

    public enum Title
    {
      blackList = 1,
      application = 2,
      member = 3,
      admin = 4,
      owner = 5,
      beinvited = 6,
    }

    public enum Status
    {
      normal,
      silence,
    }
  }
}
