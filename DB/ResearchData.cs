﻿// Decompiled with JetBrains decompiler
// Type: DB.ResearchData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class ResearchData : BaseData
  {
    public const int INVALID_ID = 0;
    private int _researchId;
    private long _jobId;

    public int researchId
    {
      get
      {
        return this._researchId;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "research_id", ref this._researchId) | DatabaseTools.UpdateData(dataHt, "job_id", ref this._jobId);
    }

    public static int GetResearchId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return int.Parse(data[(object) "research_id"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Research Id: " + (object) this.researchId + ":" + (object) this.jobId + "\n";
    }

    public struct Params
    {
      public const string KEY = "research";
      public const string RESEARCH_ID = "research_id";
      public const string RESEARCH_JOB_ID = "job_id";
    }
  }
}
