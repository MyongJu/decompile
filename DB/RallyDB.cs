﻿// Decompiled with JetBrains decompiler
// Type: DB.RallyDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class RallyDB
  {
    private SortedDictionary<long, RallyData> _datas = new SortedDictionary<long, RallyData>();
    private List<long> _rallys = new List<long>();
    private List<long> _defenses = new List<long>();
    public List<long> targetList = new List<long>();

    public event System.Action<long> onRallyDataChanged;

    public event System.Action<long> onRallyDataCreated;

    public event System.Action<long> onRallyDataUpdated;

    public event System.Action<long> onRallyDataRemove;

    public event System.Action onRallyDataRemoved;

    public event System.Action<long> onRallyTargetDataCreated;

    public event System.Action<long> onRallyTargetDataChanged;

    public event System.Action<long> onRallyTargetDataRemove;

    public event System.Action onRallyTargetDataRemoved;

    public event System.Action<long, long> onRallyMarchJoined;

    public event System.Action<long, long> onRallyMarchDataChanged;

    public event System.Action<long, long> onRallyMarchDataCanceled;

    public List<long> rallys
    {
      get
      {
        this._rallys.Clear();
        SortedDictionary<long, RallyData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.ownerAllianceId == PlayerData.inst.allianceId)
            this._rallys.Add(enumerator.Current.rallyId);
        }
        return this._rallys;
      }
    }

    public List<long> defenses
    {
      get
      {
        this._defenses.Clear();
        SortedDictionary<long, RallyData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.targetAllianceId == PlayerData.inst.allianceId)
            this._defenses.Add(enumerator.Current.rallyId);
        }
        return this._defenses;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (RallyData), (long) this._datas.Count);
    }

    public bool HasJoinWonderRally()
    {
      bool flag = false;
      SortedDictionary<long, RallyData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null && (WonderUtils.IsWonder(enumerator.Current.location) || WonderUtils.IsWonderTower(enumerator.Current.location)))
        {
          flag = enumerator.Current.isJoined;
          if (flag)
            break;
        }
      }
      return flag;
    }

    public void Init()
    {
    }

    private void Publish_OnRallyTargetDataCreated(long rallyId)
    {
      if (this.onRallyTargetDataCreated != null)
        this.onRallyTargetDataCreated(rallyId);
      this.Publish_OnRallyTargetDataChanged(rallyId);
    }

    private void Publish_OnRallyTargetDataChanged(long rallyId)
    {
      if (this.onRallyTargetDataChanged == null)
        return;
      this.onRallyTargetDataChanged(rallyId);
    }

    private void Publish_OnRallyTargetDataRemove(long rallyId)
    {
      if (this.onRallyTargetDataRemove != null)
        this.onRallyTargetDataRemove(rallyId);
      this.Publish_OnRallyTargetDataChanged(rallyId);
    }

    private void Publish_OnRallyTargetDataRemoved()
    {
      if (this.onRallyTargetDataRemoved == null)
        return;
      this.onRallyTargetDataRemoved();
    }

    public void Publish_OnRallyDataChanged(long rallyId)
    {
      if (this.onRallyDataChanged != null)
        this.onRallyDataChanged(rallyId);
      if (!this.targetList.Contains(rallyId))
        return;
      this.Publish_OnRallyTargetDataChanged(rallyId);
    }

    private void Publish_OnRallyDataCreated(long rallyId)
    {
      if (this.onRallyDataCreated != null)
        this.onRallyDataCreated(rallyId);
      this.Publish_OnRallyDataChanged(rallyId);
    }

    private void Publish_OnRallyDataUpdated(long rallyId)
    {
      if (this.onRallyDataUpdated != null)
        this.onRallyDataUpdated(rallyId);
      this.Publish_OnRallyDataChanged(rallyId);
    }

    private void Publish_OnRallyDataRemove(long rallyId)
    {
      this.Publish_OnRallyDataChanged(rallyId);
      if (this.onRallyDataRemove == null)
        return;
      this.onRallyDataRemove(rallyId);
    }

    private void Publish_OnRallyDataRemoved()
    {
      if (this.onRallyDataRemoved == null)
        return;
      this.onRallyDataRemoved();
    }

    private void Publish_OnRallyMarchDataJoined(long rallyId, long marchId)
    {
      if (this.onRallyMarchJoined != null)
        this.onRallyMarchJoined(rallyId, marchId);
      this.Publish_OnRallyDataChanged(rallyId);
    }

    private void Publish_OnRallyMarchDataCanceled(long rallyId, long marchId)
    {
      if (this.onRallyMarchDataCanceled != null)
        this.onRallyMarchDataCanceled(rallyId, marchId);
      this.Publish_OnRallyDataChanged(rallyId);
    }

    private void Publish_OnRallyMarchDataChanged(long rallyId, long marchId)
    {
      if (this.onRallyMarchDataChanged != null)
        this.onRallyMarchDataChanged(rallyId, marchId);
      this.Publish_OnRallyDataChanged(rallyId);
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      RallyData rallyData = new RallyData();
      if (!rallyData.Decode((object) hashtable, updateTime) || rallyData.rallyId == -1L || (this._datas.ContainsKey(rallyData.rallyId) || rallyData.state == RallyData.RallyState.done))
        return false;
      this._datas.Add(rallyData.rallyId, rallyData);
      if (rallyData.targetUid == PlayerData.inst.uid)
      {
        this.targetList.Add(rallyData.rallyId);
        this.Publish_OnRallyTargetDataCreated(rallyData.rallyId);
      }
      this.Publish_OnRallyDataCreated(rallyData.rallyId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = this.CheckKeyFromData((object) hashtable);
      if (index == -1L)
        return false;
      if (!this._datas.ContainsKey(index) || !this._datas[index].Decode((object) hashtable, updateTime))
        return this.Add(orgData, updateTime);
      this.Publish_OnRallyDataUpdated(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this.Publish_OnRallyDataRemove(id);
      this._datas.Remove(id);
      if (this.targetList.Contains(id))
      {
        this.Publish_OnRallyTargetDataRemove(id);
        this.targetList.Remove(id);
        this.Publish_OnRallyTargetDataRemoved();
      }
      this.Publish_OnRallyDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse(arrayList[index].ToString()), updateTime);
    }

    public RallyData Get(long id)
    {
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (RallyData) null;
    }

    public List<RallyData> GetRallyesByAllianceId(long allianceId)
    {
      List<RallyData> rallyDataList = new List<RallyData>();
      SortedDictionary<long, RallyData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.ownerAllianceId == allianceId || enumerator.Current.targetAllianceId == allianceId)
          rallyDataList.Add(enumerator.Current);
      }
      rallyDataList.Sort((Comparison<RallyData>) ((a, b) => a.waitTimeDuration.startTime.CompareTo(b.waitTimeDuration.startTime)));
      return rallyDataList;
    }

    public List<RallyData> GetRallyesByAllianceIdInCurrentKingdom(long allianceId)
    {
      List<RallyData> rallyDataList = new List<RallyData>();
      SortedDictionary<long, RallyData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.worldId == PlayerData.inst.playerCityData.cityLocation.K && (enumerator.Current.ownerAllianceId == allianceId || enumerator.Current.targetAllianceId == allianceId))
          rallyDataList.Add(enumerator.Current);
      }
      rallyDataList.Sort((Comparison<RallyData>) ((a, b) => a.waitTimeDuration.startTime.CompareTo(b.waitTimeDuration.startTime)));
      return rallyDataList;
    }

    public bool IsTargetUserBeRallied(long uid)
    {
      int index = 0;
      for (int count = this.rallys.Count; index < count; ++index)
      {
        if (this.Get(this.rallys[index]).targetUid == uid)
          return true;
      }
      return false;
    }

    private void OnRallyDataRecived(object data)
    {
      Hashtable hashtable = data as Hashtable;
      DBManager.inst.UpdateDatas(data, 0L);
      this.Update(hashtable[(object) nameof (data)], 0L);
    }

    public void ShowInfo()
    {
      string str1 = string.Empty + "RallyDB Count : " + (object) this._datas.Count + "\n" + "rally Count : " + (object) this.rallys.Count + "\n" + "defense Count : " + (object) this.defenses.Count + "\n";
      using (SortedDictionary<long, RallyData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str1 = str1 + string.Empty + (object) current + ":" + (object) this._datas[current].slotsInfo_joined.Count + ":" + (object) this._datas[current].slotsInfo_unlockForUser.Count + "\n";
        }
      }
      string str2 = str1 + "\n";
      using (SortedDictionary<long, RallyData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
        }
      }
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return -1;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return -1;
      long result = -1;
      if (long.TryParse(hashtable[(object) "rally_id"].ToString(), out result))
        return result;
      return -1;
    }
  }
}
