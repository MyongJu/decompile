﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceInviteApplyKey
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct AllianceInviteApplyKey
  {
    public long uid;
    public long allianceId;

    public bool Decode(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "uid", ref this.uid) | DatabaseTools.UpdateData(inData, "alliance_id", ref this.allianceId);
    }

    public bool IsValid
    {
      get
      {
        if (this.uid != 0L)
          return this.allianceId != 0L;
        return false;
      }
    }

    public struct Param
    {
      public const string ALLIANCE_ID = "alliance_id";
      public const string UID = "uid";
    }
  }
}
