﻿// Decompiled with JetBrains decompiler
// Type: DB.HeroEquipments
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class HeroEquipments
  {
    private Dictionary<int, int> m_Slots = new Dictionary<int, int>();

    public bool Decode(Hashtable data)
    {
      this.m_Slots = new Dictionary<int, int>();
      if (data == null)
        return true;
      bool flag = false;
      IDictionaryEnumerator enumerator = data.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result1;
        int.TryParse(enumerator.Key.ToString(), out result1);
        int result2;
        int.TryParse(enumerator.Value.ToString(), out result2);
        this.m_Slots[result1] = result2;
        flag = true;
      }
      return flag;
    }

    public int GetSlotEquipID(int type)
    {
      int num = 0;
      this.m_Slots.TryGetValue(type, out num);
      return num;
    }

    public List<int> GetAllEquipments()
    {
      List<int> intList = new List<int>();
      using (Dictionary<int, int>.Enumerator enumerator = this.m_Slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, int> current = enumerator.Current;
          intList.Add(current.Value);
        }
      }
      return intList;
    }
  }
}
