﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public class AllianceData : BaseData
  {
    private string _announcement = string.Empty;
    private AllianceTreasuryChestData _treasuryChestData = new AllianceTreasuryChestData();
    [JsonIgnore]
    public AllianceMembersContainer members = new AllianceMembersContainer();
    [JsonIgnore]
    public AllianceTradeContainer trades = new AllianceTradeContainer();
    [JsonIgnore]
    public AllianceDiplomacyContainer diplomacy = new AllianceDiplomacyContainer();
    [JsonIgnore]
    public AllianceHelpContainer helps = new AllianceHelpContainer();
    public const long INVALID_ID = 0;
    private long _worldId;
    private long _allianceId;
    private string _allianceName;
    private string _allianceAcronym;
    private int _allianceSymbolCode;
    private int _allianceLevel;
    private long _fund;
    private int _xp;
    private long _power;
    private int _powerLimit;
    private int _levelLimit;
    private int _memberCount;
    private int _memberMax;
    private bool _isPrivate;
    private string _publicMassage;
    private string _language;
    private int _dailySteak;
    private long _giftPoints;
    private int _manaPoint;
    private int _acRegisterTime;
    private int _acGroupId;
    private int _elixirLimit;
    private string _allianceFullName;
    private long _creatorId;
    private long _chatChannelId;
    private long _pushChannelId;
    private int _techMark;
    private string _lordName;
    private string _dukeName;
    private string _baronName;
    private string _knightName;
    private string _vassalName;
    private int _bossLevel;

    public AllianceData()
    {
      this._allianceId = 0L;
    }

    public AllianceData(long id)
    {
      this._allianceId = id;
    }

    public long key
    {
      get
      {
        return this._allianceId;
      }
    }

    public long worldId
    {
      get
      {
        return this._worldId;
      }
    }

    public long allianceId
    {
      get
      {
        return this._allianceId;
      }
    }

    public string allianceName
    {
      set
      {
        this._allianceName = value;
      }
      get
      {
        return this._allianceName;
      }
    }

    public string allianceAcronym
    {
      get
      {
        return this._allianceAcronym;
      }
      set
      {
        this._allianceAcronym = value;
      }
    }

    public int allianceSymbolCode
    {
      get
      {
        return this._allianceSymbolCode;
      }
    }

    public int allianceLevel
    {
      get
      {
        return this._allianceLevel;
      }
    }

    public long allianceFund
    {
      get
      {
        return this._fund;
      }
    }

    public int xp
    {
      get
      {
        return this._xp;
      }
    }

    public long power
    {
      get
      {
        return this._power;
      }
    }

    public int powerLimit
    {
      get
      {
        return this._powerLimit;
      }
    }

    public int levelLimit
    {
      get
      {
        return this._levelLimit;
      }
    }

    public int memberCount
    {
      get
      {
        return this._memberCount;
      }
    }

    public int memberMax
    {
      get
      {
        return this._memberMax;
      }
    }

    public bool isPirvate
    {
      get
      {
        return this._isPrivate;
      }
    }

    public string publicMessage
    {
      get
      {
        return this._publicMassage;
      }
    }

    public string announcement
    {
      get
      {
        return this._announcement;
      }
    }

    public string language
    {
      get
      {
        return this._language;
      }
    }

    public int dailySteak
    {
      get
      {
        return this._dailySteak;
      }
    }

    public long giftPoints
    {
      get
      {
        return this._giftPoints;
      }
    }

    public int manaPoint
    {
      get
      {
        return this._manaPoint;
      }
    }

    public int AcRegisterTime
    {
      set
      {
        this._acRegisterTime = value;
      }
      get
      {
        return this._acRegisterTime;
      }
    }

    public int AcGroupId
    {
      set
      {
        this._acGroupId = value;
      }
      get
      {
        return this._acGroupId;
      }
    }

    public int elixirLimit
    {
      get
      {
        int num = 0;
        GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_temple_donation_repletion_reduce_time");
        if (data != null)
          num = (int) ((long) NetServerTime.inst.ServerTimestamp - this._updateTime / 1000L) / (int) (data.Value * 60.0);
        return Mathf.Max(0, this._elixirLimit - num);
      }
    }

    public int TreasureChestAmount
    {
      get
      {
        return this._treasuryChestData.amount;
      }
    }

    public int TreasureRefreshedTime
    {
      get
      {
        return this._treasuryChestData.refreshedTime;
      }
    }

    public bool CanSendTreasureChest
    {
      get
      {
        return this._treasuryChestData.status == 0;
      }
    }

    public string allianceFullName
    {
      get
      {
        return this._allianceFullName;
      }
    }

    public long creatorId
    {
      get
      {
        return this._creatorId;
      }
    }

    public long chatChannelId
    {
      get
      {
        return this._chatChannelId;
      }
    }

    public long pushChannelId
    {
      get
      {
        return this._pushChannelId;
      }
    }

    public int techMark
    {
      get
      {
        return this._techMark;
      }
    }

    public string lordName
    {
      get
      {
        return this._lordName;
      }
    }

    public string dukeName
    {
      get
      {
        return this._dukeName;
      }
    }

    public string baronName
    {
      get
      {
        return this._baronName;
      }
    }

    public string knightName
    {
      get
      {
        return this._knightName;
      }
    }

    public string vassalName
    {
      get
      {
        return this._vassalName;
      }
    }

    public int bossLevel
    {
      get
      {
        return this._bossLevel;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData1 = orgData as Hashtable;
      if (inData1 == null)
        return false;
      bool flag1 = false;
      Hashtable inData2 = inData1[(object) "roster_names"] as Hashtable;
      if (inData2 != null)
        flag1 = flag1 | DatabaseTools.UpdateData(inData2, 6.ToString(), ref this._lordName) | DatabaseTools.UpdateData(inData2, 4.ToString(), ref this._dukeName) | DatabaseTools.UpdateData(inData2, 3.ToString(), ref this._baronName) | DatabaseTools.UpdateData(inData2, 2.ToString(), ref this._knightName) | DatabaseTools.UpdateData(inData2, 1.ToString(), ref this._vassalName);
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData1, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData1, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData1, "name", ref this._allianceName) | DatabaseTools.UpdateData(inData1, "acronym", ref this._allianceAcronym) | DatabaseTools.UpdateData(inData1, "symbol_code", ref this._allianceSymbolCode) | DatabaseTools.UpdateData(inData1, "level", ref this._allianceLevel) | DatabaseTools.UpdateData(inData1, "xp", ref this._xp) | DatabaseTools.UpdateData(inData1, "power", ref this._power) | DatabaseTools.UpdateData(inData1, "power_limit", ref this._powerLimit) | DatabaseTools.UpdateData(inData1, "level_limit", ref this._levelLimit) | DatabaseTools.UpdateData(inData1, "member_count", ref this._memberCount) | DatabaseTools.UpdateData(inData1, "member_max", ref this._memberMax);
      int outData = !this._isPrivate ? 0 : 1;
      bool flag3 = flag2 | DatabaseTools.UpdateData(inData1, "is_private", ref outData);
      this._isPrivate = outData == 1;
      bool flag4 = flag3 | DatabaseTools.UpdateData(inData1, "public_message", ref this._publicMassage) | DatabaseTools.UpdateData(inData1, "announcement", ref this._announcement) | DatabaseTools.UpdateData(inData1, "language", ref this._language) | DatabaseTools.UpdateData(inData1, "daily_steak", ref this._dailySteak) | DatabaseTools.UpdateData(inData1, "gift_points", ref this._giftPoints) | DatabaseTools.UpdateData(inData1, "alliance_creator_id", ref this._creatorId) | DatabaseTools.UpdateData(inData1, "chat_channel", ref this._chatChannelId) | DatabaseTools.UpdateData(inData1, "push_channel", ref this._pushChannelId) | DatabaseTools.UpdateData(inData1, "fund", ref this._fund) | DatabaseTools.UpdateData(inData1, "tech_mark", ref this._techMark) | DatabaseTools.UpdateData(inData1, "boss_level", ref this._bossLevel) | DatabaseTools.UpdateData(inData1, "mana_point", ref this._manaPoint) | DatabaseTools.UpdateData(inData1, "elixir_limit", ref this._elixirLimit) | DatabaseTools.UpdateData(inData1, "ac_register_time", ref this._acRegisterTime) | DatabaseTools.UpdateData(inData1, "ac_group_id", ref this._acGroupId);
      if (inData1.ContainsKey((object) "treasury_chest"))
      {
        Hashtable inData3 = inData1[(object) "treasury_chest"] as Hashtable;
        if (inData3 != null)
          flag4 = flag4 | DatabaseTools.UpdateData(inData3, "amount", ref this._treasuryChestData.amount) | DatabaseTools.UpdateData(inData3, "status", ref this._treasuryChestData.status) | DatabaseTools.UpdateData(inData3, "refreshed_time", ref this._treasuryChestData.refreshedTime);
      }
      if (inData1.ContainsKey((object) "benefits") && this._allianceId == PlayerData.inst.allianceId)
        DBManager.inst.DB_Local_Benefit.Update(inData1[(object) "benefits"], updateTime, true);
      this.CalcFullName();
      return flag4;
    }

    public void CalcFullName()
    {
      this._allianceFullName = string.Format("[{0}]{1}", (object) this.allianceAcronym, (object) this.allianceName);
    }

    public string GetSymbolSpriteName()
    {
      return string.Format("shield_{0}", (object) (this.allianceSymbolCode + 1));
    }

    public string ShowInfo()
    {
      return "<color=#FFFF00>Alliance Data " + (object) this.allianceId + " </color>\n" + "name : " + this.allianceFullName + "\n" + "acronym : " + this.allianceAcronym + "\n" + "level : " + (object) this.allianceLevel + "\n" + "symbol : " + (object) this.allianceSymbolCode + "\n" + "channel : " + (object) this.chatChannelId + "\n";
    }

    public AllianceMemberData GetHighLord()
    {
      using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = this.members.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceMemberData current = enumerator.Current;
          if (current.title == 6)
            return current;
        }
      }
      return (AllianceMemberData) null;
    }

    public AllianceMemberData GetTempHighLord()
    {
      using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = this.members.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceMemberData current = enumerator.Current;
          if (current.title == 5)
            return current;
        }
      }
      return (AllianceMemberData) null;
    }

    public bool FortressContainsCoordinate(Coordinate coordinate)
    {
      List<ZoneBorderData> bordersByAllianceId = DBManager.inst.DB_ZoneBorder.GetZoneBordersByAllianceID(PlayerData.inst.allianceId);
      for (int index = 0; index < bordersByAllianceId.Count; ++index)
      {
        if (bordersByAllianceId[index].Contains(coordinate))
          return true;
      }
      return false;
    }

    public static string GetAllianceIconSpriteName(int symbolCode)
    {
      return string.Format("shield_{0}", (object) (symbolCode + 1));
    }

    public string GetTitleName(int title)
    {
      string format = ScriptLocalization.Get("alliance_rank_level_placeholder", true);
      switch (title)
      {
        case 0:
          return string.Format(format, (object) 0);
        case 1:
          if (string.IsNullOrEmpty(this._vassalName))
            return string.Format(format, (object) 1);
          return this._vassalName;
        case 2:
          if (string.IsNullOrEmpty(this._knightName))
            return string.Format(format, (object) 2);
          return this._knightName;
        case 3:
          if (string.IsNullOrEmpty(this._baronName))
            return string.Format(format, (object) 3);
          return this._baronName;
        case 4:
          if (string.IsNullOrEmpty(this._dukeName))
            return string.Format(format, (object) 4);
          return this._dukeName;
        case 6:
          if (string.IsNullOrEmpty(this._lordName))
            return string.Format(format, (object) 5);
          return this._lordName;
        default:
          return string.Empty;
      }
    }

    public struct Params
    {
      public const string KEY = "alliance";
      public const string ALLIANCE_ID = "alliance_id";
      public const string ALLIANCE_CREATOR_ID = "alliance_creator_id";
      public const string ALLIANCE_NAME = "name";
      public const string ALLIANCE_ACRONYM = "acronym";
      public const string ALLIANCE_SYMBOL_CODE = "symbol_code";
      public const string ALLIANCE_LEVEL = "level";
      public const string ALLIANCE_XP = "xp";
      public const string ALLIANCE_POWER = "power";
      public const string ALLIANCE_POWER_LIMIT = "power_limit";
      public const string ALLIANCE_LEVEL_LIMIT = "level_limit";
      public const string ALLIANCE_MEMBER_COUNT = "member_count";
      public const string ALLIANCE_MEMBER_MAX = "member_max";
      public const string ALLIANCE_IS_PRIVATE = "is_private";
      public const string ALLIANCE_MESSAGE = "public_message";
      public const string ALLIANCE_LANGUAGE = "language";
      public const string ALLIANCE_GIFT_POINTS = "gift_points";
      public const string ALLIANCE_DAILY_STEAK = "daily_steak";
      public const string WORLD_ID = "world_id";
      public const string WORLD_MAP_CHAT_CHANNEL = "chat_channel";
      public const string WORLD_MAP_PUSH_CHANNEL = "push_channel";
      public const string ALLIANCE_TECH_MARK = "tech_mark";
      public const string ALLIANCE_FUND = "fund";
      public const string ANNOUNCEMENT = "announcement";
      public const string ALLIANCE_BENEFIT = "benefits";
      public const string ALLIANCE_ROSTER_NAMES = "roster_names";
      public const string ALLIANCE_BOSS_LEVEL = "boss_level";
      public const string ALLIANCE_MANA_POINT = "mana_point";
      public const string ALLIANCE_ELIXIR_LIMIT = "elixir_limit";
      public const string TREASURY_POINT = "treasury_point";
      public const string TREASURY_CHEST = "treasury_chest";
      public const string TREASURY_CHEST_AMOUNT = "amount";
      public const string TREASURY_CHEST_STATUS = "status";
      public const string TREASURY_CHEST_REFRESHED_TIME = "refreshed_time";
      public const string AC_REGISTER_TIME = "ac_register_time";
      public const string AC_GROUP_ID = "ac_group_id";
    }
  }
}
