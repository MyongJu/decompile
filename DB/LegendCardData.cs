﻿// Decompiled with JetBrains decompiler
// Type: DB.LegendCardData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class LegendCardData : BaseData
  {
    private long _uid;
    private int _legendId;
    private int _position;
    private int _level;
    private long _xp;
    private int _star;

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public int LegendId
    {
      get
      {
        return this._legendId;
      }
    }

    public int Position
    {
      get
      {
        return this._position;
      }
    }

    public bool PositionAssigned
    {
      get
      {
        return this._position > 0;
      }
    }

    public int OriginPosition
    {
      get
      {
        ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendId);
        if (parliamentHeroInfo != null)
          return parliamentHeroInfo.ParliamentPosition;
        return -1;
      }
    }

    public int Level
    {
      get
      {
        return this._level;
      }
    }

    public long XP
    {
      get
      {
        return this._xp;
      }
    }

    public long Score
    {
      get
      {
        ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendId);
        if (parliamentHeroInfo != null)
          return (long) ((double) parliamentHeroInfo.gradeRate * (double) (this._level + this._star));
        return 0;
      }
    }

    public string HeroName
    {
      get
      {
        ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendId);
        if (parliamentHeroInfo != null)
          return parliamentHeroInfo.Name;
        return string.Empty;
      }
    }

    public string HeroFragmentName
    {
      get
      {
        ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this._legendId);
        if (parliamentHeroInfo != null)
        {
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(parliamentHeroInfo.chipItem);
          if (itemStaticInfo != null)
            return itemStaticInfo.LocName;
        }
        return string.Empty;
      }
    }

    public long CurrentLevelXP
    {
      get
      {
        long num = 0;
        for (int level = 1; level < this.Level; ++level)
          num += HeroCardUtils.GetRequiredExpToNextLevel(this.LegendId, level);
        return this.XP - num;
      }
    }

    public bool IsMaxLevel
    {
      get
      {
        return this._level >= HeroCardUtils.GetHeroMaxLevel(this._legendId);
      }
    }

    public int Star
    {
      get
      {
        return this._star;
      }
    }

    public KeyValuePair<long, int> ParamKey
    {
      get
      {
        return new KeyValuePair<long, int>(this._uid, this._legendId);
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "legend_id", ref this._legendId) | DatabaseTools.UpdateData(inData, "position", ref this._position) | DatabaseTools.UpdateData(inData, "level", ref this._level) | DatabaseTools.UpdateData(inData, "xp", ref this._xp) | DatabaseTools.UpdateData(inData, "star", ref this._star);
      return true;
    }

    public struct Params
    {
      public const string KEY = "legend";
      public const string UID = "uid";
      public const string LEGEND_ID = "legend_id";
      public const string POSITION = "position";
      public const string LEVEL = "level";
      public const string XP = "xp";
      public const string STAR = "star";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
