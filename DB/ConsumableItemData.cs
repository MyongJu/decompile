﻿// Decompiled with JetBrains decompiler
// Type: DB.ConsumableItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ConsumableItemData : BaseData
  {
    private List<long> _properties = new List<long>();
    private List<KeyValuePair<int, int>> _sellQuantityList = new List<KeyValuePair<int, int>>();
    private List<KeyValuePair<int, int>> _sellCooldownTimeList = new List<KeyValuePair<int, int>>();
    public const int INVALID_ID = 0;
    private int _internalId;
    private int _quantity;
    private int _curCooldownTime;
    private bool _isFreeze;
    private bool _isNew;

    public ConsumableItemData()
    {
    }

    public ConsumableItemData(int itemId)
    {
      this._internalId = itemId;
    }

    public int internalId
    {
      get
      {
        return this._internalId;
      }
      set
      {
        this._internalId = value;
      }
    }

    public int quantity
    {
      get
      {
        return this._quantity;
      }
      set
      {
        this._quantity = value;
      }
    }

    public List<long> properties
    {
      get
      {
        return this._properties;
      }
    }

    public bool IsIncrease { get; set; }

    public List<KeyValuePair<int, int>> SellQuantityList
    {
      get
      {
        return this._sellQuantityList;
      }
    }

    public List<KeyValuePair<int, int>> SellCooldownTimeList
    {
      get
      {
        return this._sellCooldownTimeList;
      }
    }

    public int FreezeQuantity
    {
      get
      {
        int num = 0;
        for (int index = 0; index < this.SellCooldownTimeList.Count; ++index)
        {
          if (NetServerTime.inst.ServerTimestamp <= this.SellCooldownTimeList[index].Key)
            num += this.SellCooldownTimeList[index].Value;
        }
        return 0;
      }
    }

    public int CurCooldownTime
    {
      get
      {
        return this._curCooldownTime;
      }
      set
      {
        this._curCooldownTime = value;
      }
    }

    public bool IsFreeze
    {
      get
      {
        return NetServerTime.inst.ServerTimestamp <= this._curCooldownTime;
      }
    }

    public string FreezeHint
    {
      get
      {
        return ScriptLocalization.GetWithPara("exchange_hall_item_frozen_description", new Dictionary<string, string>()
        {
          {
            "0",
            TradingHallPayload.Instance.HowManyDays(this._curCooldownTime - NetServerTime.inst.ServerTimestamp).ToString()
          }
        }, true);
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "item_property_id", ref this._internalId);
      this.IsIncrease = false;
      if (inData.ContainsKey((object) "quantity"))
      {
        int outData = 0;
        flag |= DatabaseTools.UpdateData(inData, "quantity", ref outData);
        this.IsIncrease = this._quantity < outData;
        this._quantity = outData;
      }
      if (inData.ContainsKey((object) "property"))
      {
        this._properties.Clear();
        flag = ((flag ? 1 : 0) | 1) != 0;
        ArrayList arrayList = inData[(object) "property"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            long result = 0;
            long.TryParse(arrayList[index].ToString(), out result);
            this._properties.Add(result);
          }
        }
      }
      if (inData.ContainsKey((object) "sell_quantity"))
      {
        this._sellQuantityList.Clear();
        Hashtable hashtable = inData[(object) "sell_quantity"] as Hashtable;
        if (hashtable != null)
        {
          foreach (object key in (IEnumerable) hashtable.Keys)
          {
            int result1 = 0;
            int result2 = 0;
            int.TryParse(key.ToString(), out result1);
            int.TryParse(hashtable[(object) result1.ToString()].ToString(), out result2);
            this._sellQuantityList.Add(new KeyValuePair<int, int>(result1, result2));
          }
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "sell_cd_time"))
      {
        this._sellCooldownTimeList.Clear();
        Hashtable hashtable = inData[(object) "sell_cd_time"] as Hashtable;
        if (hashtable != null)
        {
          foreach (object key1 in (IEnumerable) hashtable.Keys)
          {
            string yymmdd = key1.ToString();
            string s = hashtable[(object) yymmdd].ToString();
            int key2 = (int) Utils.DateTime2ServerTime(yymmdd, "-");
            int result = 0;
            int.TryParse(s, out result);
            this._sellCooldownTimeList.Add(new KeyValuePair<int, int>(key2, result));
          }
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return string.Empty;
    }

    public static int GetInternalId(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      int result = 0;
      if (hashtable.ContainsKey((object) "item_property_id"))
        int.TryParse(hashtable[(object) "item_property_id"].ToString(), out result);
      return result;
    }

    public bool isNew
    {
      get
      {
        return this._isNew;
      }
      set
      {
        this._isNew = value;
      }
    }

    public struct Params
    {
      public const string UID = "uid";
      public const string KEY = "user_consumable";
      public const string ITEM_INTERNAL_ID = "item_property_id";
      public const string ITEM_PROPERTY = "property";
      public const string ITEM_QUANTITY = "quantity";
      public const string ITEM_SELL_QUANTITY = "sell_quantity";
      public const string ITEM_SELL_CD_TIME = "sell_cd_time";
    }
  }
}
