﻿// Decompiled with JetBrains decompiler
// Type: DB.EmprieQuestLocalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class EmprieQuestLocalDB
  {
    private Dictionary<long, EmpireQuestData> _datas = new Dictionary<long, EmpireQuestData>();
    public Action<long> onDataChanged;
    public Action<long> onDataCreated;
    public Action<long> onDataUpdated;
    public Action<long> onDataRemove;
    public Action onDataRemoved;

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      EmpireQuestData empireQuestData = new EmpireQuestData();
      if (!empireQuestData.Decode((object) data, updateTime) || (long) empireQuestData.questID == 0L || this._datas.ContainsKey((long) empireQuestData.questID))
        return false;
      this._datas.Add((long) empireQuestData.questID, empireQuestData);
      this.Publish_onDataCreated((long) empireQuestData.questID);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      long questId = EmpireQuestData.GetQuestId((object) data);
      if (questId == 0L)
        return false;
      if (!this._datas.ContainsKey(questId))
        return this.Add(orgData, updateTime);
      if (!this._datas[questId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated(questId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public EmpireQuestData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (EmpireQuestData) null;
    }

    public bool Remove(long questId, long updateTime)
    {
      if (!this._datas.ContainsKey(questId) || !this._datas[questId].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(questId);
      this.Publish_onDataRemove(questId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(EmpireQuestData.GetQuestId(arrayList[index]), updateTime);
    }

    public EmpireQuestData.QuestStatus GetEmpireQuestStateByID(long questId)
    {
      if (this._datas.ContainsKey(questId))
        return this._datas[questId].questState;
      return EmpireQuestData.QuestStatus.STATUS_INACTIVE;
    }

    public void ShowInfo()
    {
      using (Dictionary<long, EmpireQuestData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          EmpireQuestData current = enumerator.Current;
        }
      }
    }

    public Dictionary<long, EmpireQuestData> Datas
    {
      get
      {
        return this._datas;
      }
    }
  }
}
