﻿// Decompiled with JetBrains decompiler
// Type: DB.LegendData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class LegendData : BaseData
  {
    public const long INVALID_ID = 0;
    public const int INVALID_SKILL_ID = 0;
    public const int MAX_SKILL_COUNT = 3;
    private long _uid;
    private int _legendId;
    private long _worldId;
    private long _xp;
    private long _power;
    private int _status;
    private long _marchId;
    private long _altarId;
    private long _ctime;
    private int[] _skills;
    private int _level;

    public long Uid
    {
      get
      {
        return this._uid;
      }
      set
      {
        this._uid = value;
      }
    }

    public int LegendID
    {
      get
      {
        return this._legendId;
      }
      set
      {
        this._legendId = value;
      }
    }

    public long WorldId
    {
      get
      {
        return this._worldId;
      }
      set
      {
        this._worldId = value;
      }
    }

    public long Xp
    {
      get
      {
        return this._xp;
      }
      set
      {
        this._xp = value;
      }
    }

    public long Power
    {
      get
      {
        return this._power;
      }
      set
      {
        this._power = value;
      }
    }

    public int Status
    {
      get
      {
        return this._status;
      }
      set
      {
        this._status = value;
      }
    }

    public long MarchId
    {
      get
      {
        return this._marchId;
      }
      set
      {
        this._marchId = value;
      }
    }

    public long AltarId
    {
      get
      {
        return this._altarId;
      }
      set
      {
        this._altarId = value;
      }
    }

    public long Ctime
    {
      get
      {
        return this._ctime;
      }
      set
      {
        this._ctime = value;
      }
    }

    public int[] Skills
    {
      get
      {
        return this._skills;
      }
      set
      {
        this._skills = value;
      }
    }

    public int Level
    {
      get
      {
        return this._level;
      }
    }

    public void Init(int legendId, long power, long xp)
    {
      this._legendId = legendId;
      this._power = power;
      this._xp = xp;
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (updateTime < this._updateTime || orgData == null || long.TryParse(orgData.ToString(), out this._uid))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "legend_id", ref this._legendId) | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData, "xp", ref this._xp) | DatabaseTools.UpdateData(inData, "power", ref this._power) | DatabaseTools.UpdateData(inData, "status", ref this._status) | DatabaseTools.UpdateData(inData, "march_id", ref this._marchId) | DatabaseTools.UpdateData(inData, "altar_id", ref this._altarId) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "level", ref this._level);
      ArrayList data = (ArrayList) null;
      DatabaseTools.CheckAndParseOrgData(inData[(object) "skill"], out data);
      if (data != null)
      {
        flag = true;
        if (data.Count > 0)
        {
          this._skills = new int[data.Count];
          for (int index = 0; index < data.Count && index < 3; ++index)
            this._skills[index] = int.Parse(data[index].ToString());
        }
        else
          this._skills = (int[]) null;
      }
      return flag;
    }

    public struct Params
    {
      public const string KEY = "legend";
      public const string UID = "uid";
      public const string LEGEND_ID = "legend_id";
      public const string WORLD_ID = "world_id";
      public const string XP = "xp";
      public const string POWER = "power";
      public const string SKILL = "skill";
      public const string STATUS = "status";
      public const string MARCH_ID = "march_id";
      public const string ALTAR_ID = "altar_id";
      public const string C_TIME = "ctime";
      public const string LEVEL = "level";
    }

    public enum LegendState
    {
      Free,
      Battle,
      Defend,
      Dismiss,
    }
  }
}
