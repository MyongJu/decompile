﻿// Decompiled with JetBrains decompiler
// Type: DB.MonthCardDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class MonthCardDB
  {
    private Dictionary<long, MonthCardData> _datas = new Dictionary<long, MonthCardData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (MonthCardData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      MonthCardData monthCardData = new MonthCardData();
      if (!monthCardData.Decode((object) data, updateTime) || monthCardData.uid == 0L || this._datas.ContainsKey(monthCardData.uid))
        return false;
      this._datas.Add(monthCardData.uid, monthCardData);
      this.Publish_onDataCreated(monthCardData.uid);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int mainKey = MonthCardData.GetMainKey((object) data);
      if (mainKey == 0)
        return false;
      if (!this._datas.ContainsKey((long) mainKey))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) mainKey].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated((long) mainKey);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public MonthCardData Get(long key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (MonthCardData) null;
    }

    public bool Remove(int uid, long updateTime)
    {
      if (!this._datas.ContainsKey((long) uid) || !this._datas[(long) uid].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove((long) uid);
      this.Publish_onDataRemove((long) uid);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(MonthCardData.GetMainKey(arrayList[index]), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<long, MonthCardData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MonthCardData current = enumerator.Current;
        }
      }
    }

    public Dictionary<long, MonthCardData> Datas
    {
      get
      {
        return this._datas;
      }
    }
  }
}
