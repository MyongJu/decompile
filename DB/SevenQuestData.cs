﻿// Decompiled with JetBrains decompiler
// Type: DB.SevenQuestData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class SevenQuestData : BaseData, IDBDataDecoder
  {
    private int _configId;
    private int _days;
    private long _startTime;
    private string _questType;
    private int _status;
    private long _ctime;
    private long _mtime;

    public long ID
    {
      get
      {
        return (long) this._configId;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public int Days
    {
      get
      {
        return this._days;
      }
    }

    public long StartTime
    {
      get
      {
        return this._startTime;
      }
    }

    public string QuestType
    {
      get
      {
        return this._questType;
      }
    }

    public int Status
    {
      get
      {
        return this._status;
      }
    }

    public bool IsFinish
    {
      get
      {
        return this._status == 1;
      }
    }

    public bool IsClaim
    {
      get
      {
        return this._status == 2;
      }
    }

    public bool IsExpired
    {
      get
      {
        return (int) (this._startTime + 86400L) < NetServerTime.inst.ServerTimestamp;
      }
    }

    public bool IsStart
    {
      get
      {
        return (int) this._startTime <= NetServerTime.inst.ServerTimestamp;
      }
    }

    public int RemainTime
    {
      get
      {
        return NetServerTime.inst.ServerTimestamp - (int) this._startTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      bool flag = false;
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return flag | DatabaseTools.UpdateData(inData, "quest_id", ref this._configId) | DatabaseTools.UpdateData(inData, "status", ref this._status) | DatabaseTools.UpdateData(inData, "days", ref this._days) | DatabaseTools.UpdateData(inData, "start_time", ref this._startTime) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "quest_type", ref this._questType) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
    }

    public struct Params
    {
      public const string KEY = "seven_days_quest";
      public const string QUEST_ID = "quest_id";
      public const string DAYS = "days";
      public const string START_TIME = "start_time";
      public const string QUEST_TYPE = "quest_type";
      public const string REWARDS_STATUES = "status";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }

    public struct QuestStatus
    {
      public const int UN_FINISH = 0;
      public const int FINISH = 1;
      public const int CLAIM = 2;
    }
  }
}
