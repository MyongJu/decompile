﻿// Decompiled with JetBrains decompiler
// Type: DB.MarchData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

namespace DB
{
  public class MarchData : BaseData
  {
    private long _rallyId = -1;
    private DragonInfo _dragon = new DragonInfo();
    public const long INVALID_ID = 0;
    public const MarchData.MarchState IsExpeditionState = MarchData.MarchState.reinforcing | MarchData.MarchState.trading | MarchData.MarchState.scouting | MarchData.MarchState.encamping | MarchData.MarchState.marching | MarchData.MarchState.gathering | MarchData.MarchState.rally_attacking;
    public const MarchData.MarchState IsMatchState = MarchData.MarchState.marching | MarchData.MarchState.rally_attacking;
    public const MarchData.MarchState IsAttackState = MarchData.MarchState.marching | MarchData.MarchState.rally_attacking;
    public const MarchData.MarchState IsEncampState = MarchData.MarchState.encamping;
    public const MarchData.MarchState IsReturningState = MarchData.MarchState.returning | MarchData.MarchState.scoutReturning;
    public const MarchData.MarchState updateBySecond = MarchData.MarchState.gathering;
    public const MarchData.MarchState updateByFrame = MarchData.MarchState.trading | MarchData.MarchState.scouting | MarchData.MarchState.marching | MarchData.MarchState.returning;
    public const MarchData.MarchState StateCanBeSpeedUp = MarchData.MarchState.marching | MarchData.MarchState.returning;
    public const MarchData.MarchState StateCanBeRecall = MarchData.MarchState.reinforcing | MarchData.MarchState.scouting | MarchData.MarchState.encamping | MarchData.MarchState.marching | MarchData.MarchState.gathering | MarchData.MarchState.defending_fortress | MarchData.MarchState.demolishing_fortress | MarchData.MarchState.holding_wonder | MarchData.MarchState.building | MarchData.MarchState.dig;
    public const MarchData.MarchState StateCanBeRecallWithOutItem = MarchData.MarchState.reinforcing | MarchData.MarchState.encamping | MarchData.MarchState.gathering | MarchData.MarchState.defending_fortress | MarchData.MarchState.demolishing_fortress | MarchData.MarchState.holding_wonder | MarchData.MarchState.building | MarchData.MarchState.dig;
    public const MarchData.MarchState StateShowTimerHUD = MarchData.MarchState.marching | MarchData.MarchState.returning;
    private long _typeUpdateTime;
    private long _stateUpdateTime;
    private long _marchId;
    private int _worldId;
    private ArrayList _legends;
    private MarchData.MarchState _state;
    private MarchData.MarchType _type;
    private float _speed;
    private long _ownerUid;
    private long _ownerCityId;
    private Coordinate _ownerLocation;
    private Vector3 _ownerPosition;
    private float _worldSpeed;
    private long _targetUid;
    private long _targetAllianceId;
    private long _targetCityId;
    private Coordinate _targetLocation;
    private Vector3 _targetPosition;
    private TimeDurationInfo _timeDuration;
    private TroopsInfo _troopsInfo;
    private ResourceInfo _resourceInfo;
    private long _jobId;
    private ArrayList _skills;
    public Hashtable battleLog;
    private int _withDragon;

    public MarchData()
    {
      this._marchId = 0L;
    }

    public MarchData(MarchData orgData)
    {
      this._marchId = orgData.marchId;
      this._worldId = orgData.worldId;
      this._rallyId = orgData.rallyId;
      this._state = orgData.state;
      this._type = orgData.type;
      this._speed = orgData.speed;
      this._worldSpeed = orgData.worldSpeed;
      this._ownerUid = orgData.ownerUid;
      this._ownerCityId = orgData.ownerCityId;
      this._ownerLocation = orgData.ownerLocation;
      this._ownerPosition = orgData.ownerPosition;
      this._targetUid = orgData.targetUid;
      this._targetCityId = orgData.targetCityId;
      this._targetLocation = orgData.targetLocation;
      this._targetPosition = orgData.targetPosition;
      this._timeDuration = new TimeDurationInfo(orgData.timeDuration);
      this._troopsInfo = new TroopsInfo(orgData.troopsInfo);
      this._resourceInfo = new ResourceInfo(orgData.resourcesInfo);
      this._dragon = orgData.dragon == null ? (DragonInfo) null : new DragonInfo(orgData.dragon);
      this._jobId = orgData.jobId;
    }

    public long marchId
    {
      get
      {
        return this._marchId;
      }
    }

    public int worldId
    {
      get
      {
        return this._worldId;
      }
    }

    public long rallyId
    {
      get
      {
        return this._rallyId;
      }
    }

    public long dragonId
    {
      get
      {
        if (this.dragon != null)
          return this.ownerUid;
        return 0;
      }
    }

    public ArrayList legends
    {
      get
      {
        return this._legends;
      }
    }

    public MarchData.MarchState state
    {
      get
      {
        return this._state;
      }
    }

    public string loc_marchState
    {
      get
      {
        MarchData.MarchState state = this._state;
        switch (state)
        {
          case MarchData.MarchState.reinforcing:
            return ScriptLocalization.Get("time_bar_reinforcing", true);
          case MarchData.MarchState.trading:
            return ScriptLocalization.Get("time_bar_trading", true);
          default:
            if (state == MarchData.MarchState.scouting)
              return ScriptLocalization.Get("time_bar_scouting", true);
            if (state != MarchData.MarchState.encamping)
            {
              if (state == MarchData.MarchState.marching)
                return ScriptLocalization.Get("time_bar_marching", true);
              if (state == MarchData.MarchState.gathering)
                return ScriptLocalization.Get("time_bar_gathering", true);
              if (state == MarchData.MarchState.returning)
                return ScriptLocalization.Get("time_bar_returning", true);
              if (state == MarchData.MarchState.rallying)
                return ScriptLocalization.Get("time_bar_rally", true);
              if (state == MarchData.MarchState.rally_attacking)
                return ScriptLocalization.Get("time_bar_attacking", true);
              if (state != MarchData.MarchState.building)
                return string.Empty;
            }
            return ScriptLocalization.Get("time_bar_encamping", true);
        }
      }
    }

    public MarchData.MarchType type
    {
      get
      {
        return this._type;
      }
    }

    public float speed
    {
      get
      {
        return this._speed;
      }
    }

    public long ownerUid
    {
      get
      {
        return this._ownerUid;
      }
    }

    public string ownerUserName
    {
      get
      {
        UserData userData = DBManager.inst.DB_User.Get(this._ownerUid);
        if (userData != null)
          return userData.userName;
        return string.Empty;
      }
    }

    public long ownerAllianceId
    {
      get
      {
        UserData userData = DBManager.inst.DB_User.Get(this._ownerUid);
        if (userData != null)
          return userData.allianceId;
        return 0;
      }
    }

    public long ownerCityId
    {
      get
      {
        return this._ownerCityId;
      }
    }

    public Coordinate ownerLocation
    {
      get
      {
        return this._ownerLocation;
      }
    }

    public Coordinate startLocation
    {
      get
      {
        return this._ownerLocation;
      }
    }

    public Vector3 ownerPosition
    {
      get
      {
        return this._ownerPosition;
      }
    }

    public Vector3 startPosition
    {
      get
      {
        return this._ownerPosition;
      }
    }

    public float worldSpeed
    {
      get
      {
        return this._worldSpeed;
      }
    }

    public long targetUid
    {
      get
      {
        return this._targetUid;
      }
    }

    public string targetUserName
    {
      get
      {
        UserData userData = DBManager.inst.DB_User.Get(this._targetUid);
        if (userData != null)
          return userData.userName;
        return string.Empty;
      }
    }

    public long targetAllianceId
    {
      get
      {
        return this._targetAllianceId;
      }
    }

    public long targetCityId
    {
      get
      {
        return this._targetCityId;
      }
    }

    public Coordinate targetLocation
    {
      get
      {
        return this._targetLocation;
      }
    }

    public Coordinate endLocation
    {
      get
      {
        return this._targetLocation;
      }
    }

    public Vector3 targetPosition
    {
      get
      {
        return this._targetPosition;
      }
    }

    public Vector3 endPosition
    {
      get
      {
        return this._targetPosition;
      }
    }

    public TimeDurationInfo timeDuration
    {
      get
      {
        return this._timeDuration;
      }
    }

    public int startTime
    {
      get
      {
        return this._timeDuration.startTime;
      }
    }

    public int endTime
    {
      get
      {
        return this._timeDuration.endTime;
      }
    }

    public TroopsInfo troopsInfo
    {
      get
      {
        return this._troopsInfo;
      }
    }

    public ResourceInfo resourcesInfo
    {
      get
      {
        return this._resourceInfo;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
    }

    public ArrayList skills
    {
      get
      {
        return this._skills;
      }
    }

    public DragonInfo dragon
    {
      get
      {
        return this._dragon;
      }
      set
      {
        this._dragon = value;
      }
    }

    public bool isDragonIn
    {
      get
      {
        return this.dragon != null;
      }
    }

    public bool IsExpedition
    {
      get
      {
        return this.IsCurrentMarchState(MarchData.MarchState.reinforcing | MarchData.MarchState.trading | MarchData.MarchState.scouting | MarchData.MarchState.encamping | MarchData.MarchState.marching | MarchData.MarchState.gathering | MarchData.MarchState.rally_attacking);
      }
    }

    public bool IsAttack
    {
      get
      {
        return this.IsCurrentMarchState(MarchData.MarchState.marching | MarchData.MarchState.rally_attacking);
      }
    }

    public bool IsAttackFortress
    {
      get
      {
        return this.type == MarchData.MarchType.fortress_attack;
      }
    }

    public bool IsAttackingFortress(long fortressId)
    {
      AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(this.endLocation.X, this.endLocation.Y);
      return allianceFortressData != null && allianceFortressData.fortressId == fortressId && this.type == MarchData.MarchType.fortress_attack;
    }

    public bool IsScoutingFortress(long fortressId)
    {
      AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(this.endLocation.X, this.endLocation.Y);
      return allianceFortressData != null && allianceFortressData.fortressId == fortressId && this.type == MarchData.MarchType.scout;
    }

    public bool IsInFortress
    {
      get
      {
        return DBManager.inst.DB_AllianceFortress.Get(this.endLocation.X, this.endLocation.Y) != null;
      }
    }

    public bool IsWonderOrTower
    {
      get
      {
        if (!WonderUtils.IsWonder(this.endLocation))
          return WonderUtils.IsWonderTower(this.endLocation);
        return true;
      }
    }

    public bool IsCanAttackFortress
    {
      get
      {
        if (this.IsAttackFortress)
        {
          AllianceFortressData afd = DBManager.inst.DB_AllianceFortress.Get(this.endLocation.X, this.endLocation.Y);
          if (afd != null)
            return AllianceBuildUtils.HasEnemyTroop(this, afd);
        }
        return false;
      }
    }

    public bool IsEncamp
    {
      get
      {
        return this.IsCurrentMarchState(MarchData.MarchState.encamping);
      }
    }

    public bool IsReturn
    {
      get
      {
        return this.IsCurrentMarchState(MarchData.MarchState.returning | MarchData.MarchState.scoutReturning);
      }
    }

    public bool IsAttackMe
    {
      get
      {
        if (this.IsCurrentMarchState(MarchData.MarchState.marching | MarchData.MarchState.rally_attacking))
          return this.targetUid == PlayerData.inst.uid;
        return false;
      }
    }

    public string GetMarchDescription()
    {
      if (this.state == MarchData.MarchState.returning || this.state == MarchData.MarchState.scoutReturning)
        return ScriptLocalization.Get("time_bar_uppercase_returning", true);
      if (this.state == MarchData.MarchState.marching)
      {
        switch (this.type)
        {
          case MarchData.MarchType.attack:
          case MarchData.MarchType.monster_attack:
            return string.Empty;
          case MarchData.MarchType.trade:
            return string.Empty;
          case MarchData.MarchType.encamp:
            return ScriptLocalization.Get("time_bar_uppercase_encamping", true);
          case MarchData.MarchType.gather:
            return string.Empty;
          case MarchData.MarchType.reinforce:
            return string.Empty;
        }
      }
      return (string) null;
    }

    private bool IsCurrentMarchState(MarchData.MarchState sts)
    {
      return (this.state & sts) > MarchData.MarchState.invalid;
    }

    public bool isNormalAttack
    {
      get
      {
        if (this.type != MarchData.MarchType.city_attack && this.type != MarchData.MarchType.encamp_attack)
          return this.type == MarchData.MarchType.gather_attack;
        return true;
      }
    }

    public static bool IsNormalAttack(MarchData.MarchType marchType)
    {
      if (marchType != MarchData.MarchType.city_attack && marchType != MarchData.MarchType.encamp_attack)
        return marchType == MarchData.MarchType.gather_attack;
      return true;
    }

    public bool IsNpcMarch
    {
      get
      {
        if (this.ownerUid != 0L)
          return ConfigManager.inst.DB_NPC.GetDataByUid(this.ownerUid) != null;
        return false;
      }
    }

    public bool withDragon
    {
      get
      {
        return this._withDragon == 1;
      }
    }

    public bool IsMarchTypeNeedCount
    {
      get
      {
        if (this.type > MarchData.MarchType.TYPE_NOT_COUNT)
          return this.type < MarchData.MarchType.TYPE_COUNT;
        return false;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      bool flag1 = false;
      bool flag2;
      try
      {
        flag2 = flag1 | this.ParseStateData(orgData, updateTime) | this.ParseTypeData(orgData, updateTime) | this.ParseBasicData(orgData, updateTime);
        this.StartTimer();
      }
      catch (MarchTypeNotDefineException ex)
      {
        D.error((object) ("march type not define : " + ex.defineType));
        return false;
      }
      catch (MarchStateNotDefineException ex)
      {
        D.error((object) ("march state not define : " + ex.defineState));
        return false;
      }
      return flag2;
    }

    private bool ParseStateData(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      bool flag = false;
      if (hashtable.Contains((object) "state") && BaseData.CheckAndResetUpdateTime(updateTime, ref this._stateUpdateTime))
      {
        string state = hashtable[(object) "state"].ToString();
        if (state == "goHome")
          state = "returning";
        if (!Enum.IsDefined(typeof (MarchData.MarchState), (object) state))
          throw new MarchStateNotDefineException(state);
        MarchData.MarchState localState = MarchData.VerifyState(this._state, (MarchData.MarchState) Enum.Parse(typeof (MarchData.MarchState), state));
        if (localState != this._state)
        {
          this.CheckStateShift(this._state, localState);
          this._state = localState;
          flag = true;
        }
      }
      return flag;
    }

    private static MarchData.MarchState VerifyState(MarchData.MarchState current, MarchData.MarchState incoming)
    {
      switch (current)
      {
        case MarchData.MarchState.holding_wonder:
          if (incoming == MarchData.MarchState.marching)
            return current;
          break;
        case MarchData.MarchState.dig:
          if (incoming == MarchData.MarchState.marching)
            return current;
          break;
      }
      return incoming;
    }

    private void CheckStateShift(MarchData.MarchState _state, MarchData.MarchState localState)
    {
      if (PlayerData.inst.uid != this.ownerUid)
        return;
      if (_state == MarchData.MarchState.marching && localState != MarchData.MarchState.returning && (localState != MarchData.MarchState.dig && this.MarchNeedToast()))
      {
        string type = "toast_march_reach_target";
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("0", this.GetTypeKey(this.type));
        ToastData data = ConfigManager.inst.DB_Toast.GetData(type);
        if (data != null)
          UIManager.inst.ShowToast("BASIC_TOAST", (object) new ToastArgs(data, para));
      }
      if (_state != MarchData.MarchState.returning || localState != MarchData.MarchState.done || !this.ReturnNeedToast())
        return;
      string type1 = "toast_march_come_back";
      Dictionary<string, string> para1 = new Dictionary<string, string>();
      para1.Add("0", this.GetTypeKey(this.type));
      ToastData data1 = ConfigManager.inst.DB_Toast.GetData(type1);
      if (data1 == null)
        return;
      UIManager.inst.ShowToast("BASIC_TOAST", (object) new ToastArgs(data1, para1));
    }

    private bool ParseTypeData(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      bool flag = false;
      if (hashtable.Contains((object) "type") && BaseData.CheckAndResetUpdateTime(updateTime, ref this._typeUpdateTime))
      {
        string type = hashtable[(object) "type"].ToString();
        if (!Enum.IsDefined(typeof (MarchData.MarchType), (object) type))
          throw new MarchTypeNotDefineException(type);
        MarchData.MarchType marchType = (MarchData.MarchType) Enum.Parse(typeof (MarchData.MarchType), type);
        if (marchType != this._type)
        {
          this._type = marchType;
          flag = true;
        }
      }
      return flag;
    }

    private bool ParseBasicData(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "march_id", ref this._marchId) | DatabaseTools.UpdateData(inData, "rally_id", ref this._rallyId) | DatabaseTools.UpdateData(inData, "speed", ref this._speed) | DatabaseTools.UpdateData(inData, "uid", ref this._ownerUid) | DatabaseTools.UpdateData(inData, "city_id", ref this._ownerCityId) | DatabaseTools.UpdateData(inData, "opp_uid", ref this._targetUid) | DatabaseTools.UpdateData(inData, "opp_city_id", ref this._targetCityId) | DatabaseTools.UpdateData(inData, "opp_alliance_id", ref this._targetAllianceId) | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | this._ownerLocation.Decode(inData[(object) "world_id"], inData[(object) "start_x"], inData[(object) "start_y"]);
      Coordinate ownerLocation = this._ownerLocation;
      if (!this.IsReturn)
      {
        switch (this.type)
        {
          case MarchData.MarchType.city_attack:
          case MarchData.MarchType.rally_attack:
            if (!this.IsWonderOrTower)
            {
              ++ownerLocation.Y;
              break;
            }
            break;
        }
      }
      else
      {
        switch (this.type)
        {
          case MarchData.MarchType.city_attack:
          case MarchData.MarchType.rally_attack:
            if (!this.IsWonderOrTower)
            {
              ++ownerLocation.Y;
              break;
            }
            break;
        }
      }
      this._ownerPosition = this.MapData.ConvertTileKXYToWorldPosition(ownerLocation);
      bool flag2 = flag1 | this._targetLocation.Decode(inData[(object) "world_id"], inData[(object) "end_x"], inData[(object) "end_y"]);
      Coordinate targetLocation = this._targetLocation;
      if (this.IsReturn)
      {
        switch (this.type)
        {
          case MarchData.MarchType.city_attack:
          case MarchData.MarchType.rally_attack:
            if (!this.IsWonderOrTower)
            {
              ++targetLocation.Y;
              break;
            }
            break;
        }
      }
      else
      {
        switch (this.type)
        {
          case MarchData.MarchType.city_attack:
          case MarchData.MarchType.rally_attack:
            if (!this.IsWonderOrTower)
            {
              ++targetLocation.Y;
              break;
            }
            break;
        }
      }
      this._targetPosition = this.MapData.ConvertTileKXYToWorldPosition(targetLocation);
      bool flag3 = flag2 | this._timeDuration.Decode(inData[(object) "start_time"], inData[(object) "end_time"]);
      this._worldSpeed = (this._targetPosition - this._ownerPosition).magnitude / (float) this.timeDuration.duration;
      bool flag4 = flag3 | this._troopsInfo.Decode(inData[(object) "troops"]) | this._resourceInfo.Decode(inData[(object) "resources"]);
      ArrayList data1 = (ArrayList) null;
      bool flag5 = flag4 | DatabaseTools.CheckAndParseOrgData(inData[(object) "legends"], out data1);
      if (data1 != null)
        this._legends = data1;
      ArrayList data2 = (ArrayList) null;
      bool flag6 = flag5 | DatabaseTools.CheckAndParseOrgData(inData[(object) "skill"], out data2);
      if (data1 != null)
        this._legends = data1;
      bool flag7 = flag6 | DatabaseTools.UpdateData(inData, "with_dragon", ref this._withDragon);
      if (inData.ContainsKey((object) "dragon"))
      {
        this._dragon.Clear();
        bool flag8 = this._dragon.Decode(inData[(object) "dragon"]);
        flag7 |= flag8;
        this._withDragon = !flag8 ? 0 : 1;
      }
      if (inData.Contains((object) "battle_log"))
        this.battleLog = inData[(object) "battle_log"] as Hashtable;
      return flag7;
    }

    public DynamicMapData MapData
    {
      get
      {
        if (this._worldId > 1000000)
          return PitMapData.MapData;
        return PVPMapData.MapData;
      }
    }

    private void RemoveTimer()
    {
      if (this._jobId == 0L)
        return;
      JobManager.Instance.RemoveJob(this._jobId);
      this._jobId = 0L;
    }

    public void ResetTroops(object troopsData)
    {
      this._troopsInfo.Decode(troopsData);
    }

    private bool MarchNeedToast()
    {
      switch (this.type)
      {
        case MarchData.MarchType.rally:
        case MarchData.MarchType.gve_rally:
        case MarchData.MarchType.rab_rally:
          RallyData rallyData = DBManager.inst.DB_Rally.Get(this.rallyId);
          if (rallyData != null)
            return rallyData.ownerUid != PlayerData.inst.uid;
          return false;
        case MarchData.MarchType.trade:
        case MarchData.MarchType.encamp:
        case MarchData.MarchType.gather:
        case MarchData.MarchType.reinforce:
          return true;
        default:
          return false;
      }
    }

    private bool ReturnNeedToast()
    {
      switch (this.type)
      {
        case MarchData.MarchType.rally:
        case MarchData.MarchType.gve_rally:
        case MarchData.MarchType.rab_rally:
          RallyData rallyData = DBManager.inst.DB_Rally.Get(this.rallyId);
          if (rallyData != null)
            return rallyData.ownerUid != PlayerData.inst.uid;
          return false;
        case MarchData.MarchType.trade:
        case MarchData.MarchType.encamp:
        case MarchData.MarchType.gather:
        case MarchData.MarchType.reinforce:
        case MarchData.MarchType.city_attack:
        case MarchData.MarchType.encamp_attack:
        case MarchData.MarchType.gather_attack:
        case MarchData.MarchType.monster_attack:
        case MarchData.MarchType.wonder_attack:
        case MarchData.MarchType.rally_attack:
        case MarchData.MarchType.gve_rally_attack:
        case MarchData.MarchType.rab_rally_attack:
        case MarchData.MarchType.trade_warehouse:
        case MarchData.MarchType.fetch_warehouse:
        case MarchData.MarchType.world_boss_attack:
          return true;
        default:
          return false;
      }
    }

    private string GetTypeKey(MarchData.MarchType type)
    {
      string Term;
      switch (type)
      {
        case MarchData.MarchType.rally:
        case MarchData.MarchType.gve_rally:
        case MarchData.MarchType.trade:
        case MarchData.MarchType.encamp:
        case MarchData.MarchType.gather:
        case MarchData.MarchType.reinforce:
        case MarchData.MarchType.city_attack:
        case MarchData.MarchType.encamp_attack:
        case MarchData.MarchType.gather_attack:
        case MarchData.MarchType.monster_attack:
        case MarchData.MarchType.wonder_attack:
        case MarchData.MarchType.rally_attack:
        case MarchData.MarchType.gve_rally_attack:
          Term = ((int) type).ToString() + "_id";
          break;
        case MarchData.MarchType.rab_rally:
          Term = "rally_id";
          break;
        case MarchData.MarchType.world_boss_attack:
          Term = "monster_attack_id";
          break;
        default:
          Term = (string) null;
          break;
      }
      if (!string.IsNullOrEmpty(Term))
        return ScriptLocalization.Get(Term, true);
      return (string) null;
    }

    private void StartTimer()
    {
      JobHandle job = JobManager.Instance.GetJob(this._jobId);
      if (job != null)
      {
        switch (this._state)
        {
          case MarchData.MarchState.reinforcing:
          case MarchData.MarchState.encamping:
          case MarchData.MarchState.gathering:
          case MarchData.MarchState.holding_wonder:
          case MarchData.MarchState.building:
          case MarchData.MarchState.dig:
            job.Reset(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime);
            job.OnFinished = (System.Action) null;
            break;
          case MarchData.MarchState.defending_fortress:
            Hashtable hashtable1 = new Hashtable();
            hashtable1[(object) "fortress"] = (object) DBManager.inst.DB_AllianceFortress.GetByMarchId(this.marchId);
            hashtable1[(object) "marchId"] = (object) this.marchId;
            hashtable1[(object) "marchData"] = (object) this;
            job.Reset(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime);
            job.Data = (object) hashtable1;
            job.OnFinished = (System.Action) null;
            break;
          case MarchData.MarchState.rallying:
            this.OnDataTimerEnd();
            RallyData rallyData = DBManager.inst.DB_Rally.Get(this.rallyId);
            if (rallyData == null)
              break;
            rallyData.StartTimer();
            break;
          case MarchData.MarchState.rally_attacking:
          case MarchData.MarchState.done:
            this.OnDataTimerEnd();
            break;
          default:
            job.Reset(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime);
            job.OnFinished = (System.Action) null;
            break;
        }
      }
      else
      {
        if (this.type == MarchData.MarchType.rally_attack || this.type == MarchData.MarchType.gve_rally_attack || (this.type == MarchData.MarchType.rab_rally_attack || this._state == MarchData.MarchState.rallying))
          return;
        if (this._state == MarchData.MarchState.building || this._state == MarchData.MarchState.encamping || (this._state == MarchData.MarchState.holding_wonder || this._state == MarchData.MarchState.reinforcing) || (this._state == MarchData.MarchState.gathering || this._state == MarchData.MarchState.dig))
        {
          Hashtable hashtable2 = new Hashtable();
          hashtable2[(object) "marchId"] = (object) this.marchId;
          hashtable2[(object) "marchData"] = (object) this;
          this._jobId = JobManager.Instance.CreateClientJob(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime, (object) hashtable2, (System.Action) null);
        }
        else if (this._state == MarchData.MarchState.defending_fortress)
        {
          Hashtable hashtable2 = new Hashtable();
          hashtable2[(object) "fortress"] = (object) DBManager.inst.DB_AllianceFortress.GetByMarchId(this.marchId);
          hashtable2[(object) "marchId"] = (object) this.marchId;
          hashtable2[(object) "marchData"] = (object) this;
          this._jobId = JobManager.Instance.CreateClientJob(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime, (object) hashtable2, (System.Action) null);
        }
        else
        {
          if (this._timeDuration.duration <= 0 || NetServerTime.inst.ServerTimestamp >= this._timeDuration.endTime)
            return;
          Hashtable hashtable2 = new Hashtable();
          hashtable2[(object) "marchId"] = (object) this.marchId;
          hashtable2[(object) "marchData"] = (object) this;
          this._jobId = JobManager.Instance.CreateClientJob(this.CalcJobEvent(this._state), this._timeDuration.startTime, this._timeDuration.endTime, (object) hashtable2, (System.Action) null);
        }
      }
    }

    private void OnDataTimerEnd()
    {
      this.RemoveTimer();
    }

    public string ShowInfo()
    {
      return "<color=#FFFF00>March Data " + (object) this.marchId + ":" + (object) (this._updateTime % 10000L) + "</color>\n" + "March state : " + this.state.ToString() + "\n" + "March type : " + this.type.ToString() + "\n" + "March user : (" + (object) this._ownerUid + ":" + this.ownerUserName + ") -> (" + (object) this._targetUid + ":" + this.targetUserName + ")\n\t" + "March time : ( " + (object) (this.timeDuration.startTime % 10000) + " -> " + (object) (this.timeDuration.endTime % 10000) + " ) : " + (object) (NetServerTime.inst.ServerTimestamp % 1000) + " \n" + "Location : (" + (object) this.ownerLocation.K + ", " + (object) this.ownerLocation.X + ", " + (object) this.ownerLocation.Y + ") -> (" + (object) this.targetLocation.K + ", " + (object) this.targetLocation.X + ", " + (object) this.targetLocation.Y + ")\n" + "Position : (" + (object) this.ownerPosition.x + ", " + (object) this.ownerPosition.y + ", " + (object) this.ownerPosition.z + ") -> (" + (object) this.targetPosition.x + ", " + (object) this.targetPosition.y + ", " + (object) this.targetPosition.z + ")\n" + this.troopsInfo.ShowInfo() + this.resourcesInfo.ShowInfo() + this.dragon.ShowInfo() + string.Empty;
    }

    public static int CalcMarchTime(Coordinate from, Coordinate to, float slowestUnitSpeed, bool isTargetCity)
    {
      int y = to.Y;
      if (isTargetCity)
        ++y;
      int num1 = to.X - from.X;
      int num2 = y - from.Y - 1;
      float num3 = 6f * Mathf.Pow((float) (num1 * num1 + num2 * num2), 0.42f) / slowestUnitSpeed;
      float[] numArray = PathTimeCalaculater.CalcPathPercent(from.X, from.Y + 1, to.X, y);
      Coordinate location = PlayerData.inst.CityData.Location;
      return Mathf.Abs(location.X - 640) + Mathf.Abs(location.Y - 1278) >= 64 ? (int) (float) ((double) num3 * ((double) numArray[1] - (double) numArray[0]) * 5.0 + (double) num3 * (1.0 - (double) numArray[1] + (double) numArray[0])) : (int) (float) ((double) num3 * ((double) numArray[1] - (double) numArray[0]) + (double) num3 * (1.0 - (double) numArray[1] + (double) numArray[0]) * 5.0);
    }

    public static int CalcTradeTime(Coordinate from, Coordinate to)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("trade_march_speed");
      return MarchData.CalcMarchTime(from, to, (float) data.Value, true);
    }

    public static int CalcScoutTime(Coordinate from, Coordinate to)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("scout_march_speed");
      return MarchData.CalcMarchTime(from, to, (float) data.Value, true);
    }

    public JobEvent CalcJobEvent(MarchData.MarchState marchState)
    {
      switch (marchState)
      {
        case MarchData.MarchState.reinforcing:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_REINFORCING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.encamping:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_ENCAMPING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.marching:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_MARCHING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.gathering:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_GATHERING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.returning:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_RETURNING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.defending_fortress:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_DEFENDING_FORTRESS : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.demolishing_fortress:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_DEMOLISHING_FORTRESS : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.holding_wonder:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_HODING_WONDER : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.building:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_BUILDING : JobEvent.JOB_PVP_STAY;
        case MarchData.MarchState.dig:
          return this.ownerUid == PlayerData.inst.uid ? JobEvent.JOB_PVP_DIGGING : JobEvent.JOB_PVP_STAY;
        default:
          return JobEvent.JOB_PVP_STAY;
      }
    }

    public int CalculateTroopLoad()
    {
      int num = 0;
      Dictionary<string, Unit>.Enumerator enumerator = this.troopsInfo.troops.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string id = enumerator.Current.Value.Definition.ID;
        int count = enumerator.Current.Value.Count;
        ConfigBenefitCalc.UnitFinalStruct unitFinalData = ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(id, ConfigBenefitCalc.UnitCalcType.load, ConfigBenefitCalc.CalcOption.none);
        num += (int) ((double) unitFinalData.load * (double) count);
      }
      return num;
    }

    public ConfigBenefitCalc.CalcOption calcType
    {
      get
      {
        ConfigBenefitCalc.CalcOption calcOption = ConfigBenefitCalc.CalcOption.none;
        if (this.isDragonIn)
        {
          MarchData.MarchType type = this.type;
          switch (type)
          {
            case MarchData.MarchType.encamp:
            case MarchData.MarchType.city_attack:
            case MarchData.MarchType.encamp_attack:
            case MarchData.MarchType.gather_attack:
            case MarchData.MarchType.rally_attack:
              calcOption |= ConfigBenefitCalc.CalcOption.dragonAttack;
              break;
            case MarchData.MarchType.gather:
              calcOption |= ConfigBenefitCalc.CalcOption.dragonGather;
              break;
            case MarchData.MarchType.reinforce:
              calcOption |= ConfigBenefitCalc.CalcOption.dragonDefense;
              break;
            case MarchData.MarchType.monster_attack:
              calcOption |= ConfigBenefitCalc.CalcOption.dragonAttackMonster;
              break;
            default:
              if (type != MarchData.MarchType.rally)
              {
                if (type == MarchData.MarchType.world_boss_attack)
                  goto case MarchData.MarchType.monster_attack;
                else
                  break;
              }
              else
                goto case MarchData.MarchType.encamp;
          }
        }
        return calcOption;
      }
    }

    public void Dispose()
    {
      this.RemoveTimer();
    }

    public struct Params
    {
      public const string KEY = "march";
      public const string MARCH_ID = "march_id";
      public const string MARCH_RALLY_ID = "rally_id";
      public const string MARCH_STATE = "state";
      public const string MARCH_TYPE = "type";
      public const string MARCH_SPEED = "speed";
      public const string START_UID = "uid";
      public const string START_CITY_ID = "city_id";
      public const string TARGET_UID = "opp_uid";
      public const string TARGET_CITY_ID = "opp_city_id";
      public const string TARGET_ALLIANCE_ID = "opp_alliance_id";
      public const string WORLD_ID = "world_id";
      public const string START_X = "start_x";
      public const string START_Y = "start_y";
      public const string END_X = "end_x";
      public const string END_Y = "end_y";
      public const string END_TIME = "end_time";
      public const string START_TIME = "start_time";
      public const string TROOPS_INFO = "troops";
      public const string RESOURCES_INFO = "resources";
      public const string SKILLS = "skill";
      public const string LEGENDS_ID = "legends";
      public const string WITH_DRAGON = "with_dragon";
      public const string DRAGON = "dragon";
      public const string BATTLE_LOG = "battle_log";
    }

    public enum MarchState
    {
      invalid = 0,
      reinforcing = 2,
      trading = 4,
      scouting = 8,
      encamping = 16, // 0x00000010
      marching = 32, // 0x00000020
      gathering = 64, // 0x00000040
      returning = 128, // 0x00000080
      scoutReturning = 256, // 0x00000100
      defending_fortress = 512, // 0x00000200
      demolishing_fortress = 1024, // 0x00000400
      rallying = 4096, // 0x00001000
      rally_attacking = 8192, // 0x00002000
      holding_wonder = 16384, // 0x00004000
      building = 32768, // 0x00008000
      dig = 65536, // 0x00010000
      done = 1048576, // 0x00100000
    }

    public enum MarchType
    {
      none,
      rally,
      gve_rally,
      rab_rally,
      TYPE_NOT_COUNT,
      attack,
      trade,
      scout,
      encamp,
      gather,
      reinforce,
      fortress_reinforce,
      temple_reinforce,
      city_attack,
      encamp_attack,
      gather_attack,
      monster_attack,
      fortress_attack,
      temple_attack,
      wonder_attack,
      wonder_reinforce,
      rally_attack,
      gve_rally_attack,
      rab_rally_attack,
      alliance_city_build,
      trade_warehouse,
      fetch_warehouse,
      visit_npc,
      dig,
      dig_attack,
      world_boss_attack,
      TYPE_COUNT,
    }
  }
}
