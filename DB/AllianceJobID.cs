﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceJobID
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct AllianceJobID
  {
    public long allianceId;
    public long uid;
    public long jobId;

    public void Set(long alliance_id, long user_id, long job_id)
    {
      this.allianceId = alliance_id;
      this.uid = user_id;
      this.jobId = job_id;
    }

    public override string ToString()
    {
      return string.Format("{0}, {1}, {2}", (object) this.allianceId, (object) this.uid, (object) this.jobId);
    }

    public bool Decode(object data)
    {
      bool flag = false;
      Hashtable inData = data as Hashtable;
      if (inData != null)
        flag = flag | DatabaseTools.UpdateData(inData, "alliance_id", ref this.allianceId) | DatabaseTools.UpdateData(inData, "uid", ref this.uid) | DatabaseTools.UpdateData(inData, "job_id", ref this.jobId);
      return flag;
    }

    public static class Params
    {
      public const string ALLIANCE_ID = "alliance_id";
      public const string UID = "uid";
      public const string JOB_ID = "job_id";
    }
  }
}
