﻿// Decompiled with JetBrains decompiler
// Type: DB.UserAllianceGiftDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserAllianceGiftDB
  {
    private Dictionary<long, UserAllianceGiftData> _datas = new Dictionary<long, UserAllianceGiftData>();

    public event System.Action<UserAllianceGiftData> onDataChanged;

    public event System.Action<UserAllianceGiftData> onDataUpdate;

    public event System.Action<UserAllianceGiftData> onDataRemove;

    public event System.Action<UserAllianceGiftData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (UserAllianceGiftData), (long) this._datas.Count);
    }

    private void Publish_OnDataChanged(UserAllianceGiftData gift)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(gift);
    }

    private void Publish_OnDataCreate(UserAllianceGiftData gift)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(gift);
      this.Publish_OnDataChanged(gift);
    }

    private void Publish_OnDataUpdate(UserAllianceGiftData gift)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(gift);
      this.Publish_OnDataChanged(gift);
    }

    private void Publish_OnDataRemove(UserAllianceGiftData gift)
    {
      this.Publish_OnDataChanged(gift);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(gift);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable orgData1 = orgData as Hashtable;
      if (orgData1 == null)
        return false;
      UserAllianceGiftData gift = new UserAllianceGiftData();
      if (!gift.Decode(orgData1, updateTime) || gift.giftId == -1 || this._datas.ContainsKey((long) gift.giftId))
        return false;
      this._datas.Add((long) gift.giftId, gift);
      this.Publish_OnDataCreate(gift);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable orgData1 = orgData as Hashtable;
      if (orgData1 == null)
        return false;
      long key = this.CheckKeyFromData((object) orgData1);
      if (key == -1L)
        return false;
      if (!this._datas.ContainsKey(key))
        return this.Add(orgData, updateTime);
      if (!this._datas[key].Decode(orgData1, updateTime))
        return false;
      this.Publish_OnDataUpdate(this._datas[key]);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse((arrayList[index] as Hashtable)[(object) "gift_id"].ToString()), updateTime);
    }

    public UserAllianceGiftData Get(long id)
    {
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (UserAllianceGiftData) null;
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return -1;
      Hashtable hashtable = orgData as Hashtable;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      if (hashtable == null)
        return -1;
      long result = -1;
      if (long.TryParse(hashtable[(object) "gift_id"].ToString(), out result))
        return result;
      return -1;
    }
  }
}
