﻿// Decompiled with JetBrains decompiler
// Type: DB.GemDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class GemDB
  {
    private readonly Dictionary<long, GemData> _datas = new Dictionary<long, GemData>();
    public System.Action<long, long> onDataChanged;
    public System.Action<long, long> onDataCreated;
    public System.Action<long, long> onDataUpdated;
    public System.Action<long, long> onDataRemove;
    public System.Action<long, long> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (GemData), (long) this._datas.Count);
    }

    private void Publish_onDataChanged(long uid, long gemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(uid, gemId);
    }

    private void Publish_onDataCreated(long uid, long gemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(uid, gemId);
      this.Publish_onDataChanged(uid, gemId);
    }

    private void Publish_onDataUpdated(long uid, long gemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(uid, gemId);
      this.Publish_onDataChanged(uid, gemId);
    }

    private void Publish_onDataRemove(long uid, long gemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(uid, gemId);
      this.Publish_onDataChanged(uid, gemId);
    }

    private void Publish_onDataRemoved(long uid, long gemId)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(uid, gemId);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      GemData gemData = new GemData();
      if (!gemData.Decode(data, updateTime) || this._datas.ContainsKey(gemData.ID))
        return false;
      this._datas.Add(gemData.ID, gemData);
      this.Publish_onDataCreated(gemData.UID, gemData.ID);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      long gemId = GemData.GetGemId(data);
      if (gemId == -1L)
        return false;
      if (!this._datas.ContainsKey(gemId))
        return this.Add(orgData, updateTime);
      if (!this._datas[gemId].Decode(data, updateTime))
        return false;
      this.Publish_onDataUpdated(this._datas[gemId].UID, gemId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int count = arrayList.Count;
      for (int index = 0; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long gemId, long updateTime)
    {
      if (!this._datas.ContainsKey(gemId) || !this._datas[gemId].CheckUpdateTime(updateTime))
        return false;
      long uid = this._datas[gemId].UID;
      this.Publish_onDataRemove(uid, gemId);
      this._datas.Remove(gemId);
      this.Publish_onDataRemoved(uid, gemId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(GemData.GetGemId(arrayList[index] as Hashtable), updateTime);
    }

    public List<GemData> GetGemsBySlotType(SlotType slotType)
    {
      List<GemData> gemDataList = new List<GemData>();
      Dictionary<long, GemData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        GemData gemData = enumerator.Current.Value;
        if (gemData.SlotType == slotType)
          gemDataList.Add(gemData);
      }
      return gemDataList;
    }

    public GemData Get(long gemId)
    {
      if (this._datas.ContainsKey(gemId))
        return this._datas[gemId];
      return (GemData) null;
    }

    public List<GemData> GetEdibleGems(SlotType slotType, long excludeGemId = -1)
    {
      List<GemData> gemDataList = new List<GemData>();
      Dictionary<long, GemData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Key != excludeGemId)
        {
          GemData gemData = enumerator.Current.Value;
          if (gemData.SlotType == slotType && !gemData.Inlaid)
            gemDataList.Add(gemData);
        }
      }
      return gemDataList;
    }

    public List<GemData> GetAllExpGems(SlotType slotType)
    {
      List<GemData> gemDataList = new List<GemData>();
      Dictionary<long, GemData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        GemData gemData = enumerator.Current.Value;
        if (gemData.SlotType == slotType && gemData.GemType == GemType.EXP)
          gemDataList.Add(gemData);
      }
      return gemDataList;
    }

    public long GetSocketableGemCount()
    {
      long num = 0;
      Dictionary<long, GemData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        GemData gemData = enumerator.Current.Value;
        if (gemData.GemType != GemType.EXP && !gemData.Inlaid)
          num += (long) gemData.Quantity;
      }
      return num;
    }

    public List<KeyValuePair<long, long>> GetSomeExpGems(ConfigEquipmentGemInfo gemConfig, long count)
    {
      List<KeyValuePair<long, long>> keyValuePairList = new List<KeyValuePair<long, long>>();
      if (count == 0L)
        return keyValuePairList;
      List<GemData> allExpGems = this.GetAllExpGems(gemConfig.slotType);
      allExpGems.Sort((Comparison<GemData>) ((x, y) => x.Version.CompareTo(y.Version)));
      long num = count;
      List<GemData>.Enumerator enumerator = allExpGems.GetEnumerator();
      while (enumerator.MoveNext())
      {
        GemData current = enumerator.Current;
        if (current.ConfigId == gemConfig.internalId)
        {
          if (num >= (long) current.Quantity)
          {
            keyValuePairList.Add(new KeyValuePair<long, long>(current.ID, (long) current.Quantity));
            num -= (long) current.Quantity;
          }
          else
          {
            keyValuePairList.Add(new KeyValuePair<long, long>(current.ID, num));
            return keyValuePairList;
          }
        }
      }
      if (num > 0L)
        D.error((object) "Want to eat exp gems more than you have!!!");
      return keyValuePairList;
    }

    public void ClearThirdPartyInventory()
    {
      List<long> longList = new List<long>();
      Dictionary<long, GemData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.UID != PlayerData.inst.uid)
          longList.Add(enumerator.Current.Key);
      }
      for (int index = 0; index < longList.Count; ++index)
        this._datas.Remove(longList[index]);
    }
  }
}
