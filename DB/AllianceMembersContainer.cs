﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceMembersContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceMembersContainer
  {
    public Dictionary<long, AllianceMemberData> datas = new Dictionary<long, AllianceMemberData>();

    public event Action<AllianceMemberData> onDataChanged;

    public event Action<AllianceMemberData> onDataUpdate;

    public event Action<AllianceMemberData> onDataCreate;

    public event Action<AllianceMemberData> onDataRemoved;

    private void Publish_OnDataChanged(AllianceMemberData memeber)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(memeber);
    }

    private void Publish_OnDataUpdate(AllianceMemberData memeber)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(memeber);
      this.Publish_OnDataChanged(memeber);
    }

    private void Publish_OnDataCreate(AllianceMemberData memeber)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(memeber);
      this.Publish_OnDataChanged(memeber);
    }

    private void Publish_OnDataRemoved(AllianceMemberData memeber)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(memeber);
      this.Publish_OnDataChanged(memeber);
    }

    public bool Update(object orgData, long updateTime)
    {
      long uid = AllianceMemberData.GetUid(orgData);
      if (this.datas.ContainsKey(uid))
      {
        bool flag = this.datas[uid].Decode(orgData, updateTime);
        if (flag)
          this.Publish_OnDataUpdate(this.datas[uid]);
        return flag;
      }
      AllianceMemberData memeber = new AllianceMemberData();
      if (!memeber.Decode(orgData, updateTime))
        return false;
      this.datas.Add(uid, memeber);
      this.Publish_OnDataCreate(memeber);
      return true;
    }

    public bool Remove(object orgData, long updateTime)
    {
      long uid = AllianceMemberData.GetUid(orgData);
      if (!this.datas.ContainsKey(uid) || !this.datas[uid].CheckUpdateTime(updateTime))
        return false;
      AllianceMemberData data = this.datas[uid];
      this.datas.Remove(uid);
      this.Publish_OnDataRemoved(data);
      return true;
    }

    public AllianceMemberData Get(long uid)
    {
      AllianceMemberData allianceMemberData = (AllianceMemberData) null;
      this.datas.TryGetValue(uid, out allianceMemberData);
      return allianceMemberData;
    }

    public static void UpdateDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(AllianceMemberData.GetAllianceID(arrayList[index]));
        if (allianceData != null)
          allianceData.members.Update(arrayList[index], updateTime);
      }
    }

    public static void RemoveDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(AllianceMemberData.GetAllianceID(arrayList[index]));
        if (allianceData != null)
          allianceData.members.Remove(arrayList[index], updateTime);
      }
    }
  }
}
