﻿// Decompiled with JetBrains decompiler
// Type: DB.SaleItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace DB
{
  public class SaleItemData
  {
    private int _itemId;
    private int _maxCount;
    private int _price;
    private int _remain;
    private float _discount;

    public int ItemId
    {
      get
      {
        return this._itemId;
      }
      set
      {
        this._itemId = value;
      }
    }

    public int MaxCount
    {
      get
      {
        return this._maxCount;
      }
      set
      {
        this._maxCount = value;
      }
    }

    public int NowPrice
    {
      get
      {
        return this._price;
      }
    }

    public int Price
    {
      get
      {
        return this._price;
      }
      set
      {
        this._price = value;
      }
    }

    public int Remain
    {
      get
      {
        return this._remain;
      }
      set
      {
        this._remain = value;
      }
    }

    public float Discount
    {
      get
      {
        return this._discount;
      }
      set
      {
        this._discount = value;
      }
    }
  }
}
