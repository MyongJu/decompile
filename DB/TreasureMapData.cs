﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasureMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class TreasureMapData : BaseData
  {
    private long _mapId;
    private int _mapX;
    private int _mapY;
    private int _configId;

    public long MapId
    {
      get
      {
        return this._mapId;
      }
    }

    public int MapX
    {
      get
      {
        return this._mapX;
      }
    }

    public int MapY
    {
      get
      {
        return this._mapY;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "map_id", ref this._mapId) | DatabaseTools.UpdateData(inData, "map_x", ref this._mapX) | DatabaseTools.UpdateData(inData, "map_y", ref this._mapY) | DatabaseTools.UpdateData(inData, "config_id", ref this._configId);
    }

    public struct Params
    {
      public const string KEY = "treasure_map";
      public const string MAP_ID = "map_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string MAP_CONFIG_ID = "config_id";
    }
  }
}
