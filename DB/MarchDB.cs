﻿// Decompiled with JetBrains decompiler
// Type: DB.MarchDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class MarchDB
  {
    private SortedDictionary<long, MarchData> _datas = new SortedDictionary<long, MarchData>();
    public List<long> marchOwner = new List<long>();
    public List<long> marchTarget = new List<long>();
    public List<List<long>> marchOwnerList = new List<List<long>>();
    public List<List<long>> marchTargetList = new List<List<long>>();
    public List<long> othersMarch = new List<long>();
    private List<long> _doneList = new List<long>();
    private int _marchOwnerCount;

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataCreate;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataRemove;

    public event System.Action onDataRemoved;

    public event System.Action<long> onOwnerDataChanged;

    public event System.Action<long> onOwnerDataCreate;

    public event System.Action<long> onOwnerDataUpdate;

    public event System.Action<long> onOwnerDataRemove;

    public event System.Action<long> onOwnerDataRemoved;

    public event System.Action<long> onTargetDataChanged;

    public event System.Action<long> onTargetDataCreate;

    public event System.Action<long> onTargetDataUpdate;

    public event System.Action<long> onTargetDataRemove;

    public event System.Action<long> onTargetDataRemoved;

    public int marchOwnerCount
    {
      get
      {
        return this._marchOwnerCount;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (MarchData), (long) this._datas.Count);
    }

    public void Init()
    {
      int num = 0;
      for (int index = 31; num < index; ++num)
      {
        this.marchOwnerList.Add(new List<long>());
        this.marchTargetList.Add(new List<long>());
      }
    }

    public bool HasWonderMarch()
    {
      bool flag = false;
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null && enumerator.Current.ownerUid == PlayerData.inst.uid && (WonderUtils.IsWonder(enumerator.Current.startLocation) || WonderUtils.IsWonder(enumerator.Current.endLocation) || (WonderUtils.IsWonderTower(enumerator.Current.startLocation) || WonderUtils.IsWonderTower(enumerator.Current.endLocation))))
        {
          flag = true;
          break;
        }
      }
      return flag;
    }

    public string GetErrorMarchData()
    {
      SortedDictionary<long, MarchData>.Enumerator enumerator = this._datas.GetEnumerator();
      string empty = string.Empty;
      while (enumerator.MoveNext())
      {
        MarchData marchData = enumerator.Current.Value;
        if (marchData.ownerUid == PlayerData.inst.uid)
        {
          if (marchData.state == MarchData.MarchState.returning && marchData.endTime < NetServerTime.inst.ServerTimestamp)
            return marchData.marchId.ToString() + " MarchNotDone From returning";
          if (marchData.state == MarchData.MarchState.marching && (marchData.type == MarchData.MarchType.city_attack || marchData.type == MarchData.MarchType.monster_attack || (marchData.type == MarchData.MarchType.world_boss_attack || marchData.type == MarchData.MarchType.scout)))
          {
            int num = marchData.endTime - marchData.startTime;
            if (marchData.endTime + num < NetServerTime.inst.ServerTimestamp)
              return marchData.marchId.ToString() + " MarchNotDone From marching";
          }
        }
      }
      return string.Empty;
    }

    public bool HasMarchTowardTargetLocation(Coordinate targetLocation)
    {
      SortedDictionary<long, MarchData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.targetLocation.Equals(targetLocation))
          return true;
      }
      return false;
    }

    public void Publish_OnDataChanged(long marchId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(marchId);
    }

    public void Publish_OnDataCreate(long marchId)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(marchId);
      this.Publish_OnDataChanged(marchId);
    }

    public void Publish_OnDataUpdate(long marchId)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(marchId);
      this.Publish_OnDataChanged(marchId);
      if (this.marchOwner.Contains(marchId))
        this.Publish_OnOwnerDataUpdate(marchId);
      if (!this.marchTarget.Contains(marchId))
        return;
      this.Publish_OnTargetDataUpdate(marchId);
    }

    public void Publish_OnDataRemove(long marchId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(marchId);
      this.Publish_OnDataChanged(marchId);
    }

    public void Publish_OnDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    private void Publish_OnOwnerDataChanged(long marchId)
    {
      if (this.onOwnerDataChanged == null)
        return;
      this.onOwnerDataChanged(marchId);
    }

    private void Publish_OnOwnerDataCreate(long marchId)
    {
      if (this.onOwnerDataCreate != null)
        this.onOwnerDataCreate(marchId);
      this.Publish_OnOwnerDataChanged(marchId);
    }

    private void Publish_OnOwnerDataUpdate(long marchId)
    {
      if (this.onOwnerDataUpdate != null)
        this.onOwnerDataUpdate(marchId);
      this.Publish_OnOwnerDataChanged(marchId);
    }

    private void Publish_OnOwnerDataRemove(long marchId)
    {
      if (this.onOwnerDataRemove != null)
        this.onOwnerDataRemove(marchId);
      this.Publish_OnOwnerDataChanged(marchId);
    }

    private void Publish_OnOwnerDataRemoved(long marchId)
    {
      if (this.onOwnerDataRemoved == null)
        return;
      this.onOwnerDataRemoved(marchId);
    }

    private void Publish_OnTargetDataChanged(long marchId)
    {
      if (this.onTargetDataChanged == null)
        return;
      this.onTargetDataChanged(marchId);
    }

    private void Publish_OnTargetDataCreate(long marchId)
    {
      if (this.onTargetDataCreate != null)
        this.onTargetDataCreate(marchId);
      this.Publish_OnTargetDataChanged(marchId);
    }

    private void Publish_OnTargetDataUpdate(long marchId)
    {
      if (this.onTargetDataUpdate != null)
        this.onTargetDataUpdate(marchId);
      this.Publish_OnTargetDataChanged(marchId);
    }

    private void Publish_OnTargetDataRemove(long marchId)
    {
      if (this.onTargetDataRemove != null)
        this.onTargetDataRemove(marchId);
      this.Publish_OnTargetDataChanged(marchId);
    }

    private void Publish_OnTargetDataRemoved(long marchId)
    {
      if (this.onTargetDataRemoved == null)
        return;
      this.onTargetDataRemoved(marchId);
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long num = this.CheckKeyFromData((object) hashtable);
      if (num == 0L || this._doneList.Contains(num) || this._datas.ContainsKey(num))
        return false;
      MarchData marchData = new MarchData();
      if (!marchData.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(marchData.marchId))
        return false;
      if (marchData.state == MarchData.MarchState.done)
      {
        if (!this._doneList.Contains(num))
          this._doneList.Add(num);
        return false;
      }
      this._datas.Add(num, marchData);
      this._AddMarch(marchData);
      this.Publish_OnDataCreate(num);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = this.CheckKeyFromData((object) hashtable);
      if (index == 0L || this._doneList.Contains(index))
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      if (this._datas[index].state == MarchData.MarchState.done)
        this.Remove(index, updateTime);
      else
        this._AddMarch(this._datas[index]);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this._datas[id].Dispose();
      this.Publish_OnDataRemove(id);
      this._RemoveMarch(this._datas[id]);
      this._datas.Remove(id);
      if (!this._doneList.Contains(id))
        this._doneList.Add(id);
      this.Publish_OnDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse(arrayList[index].ToString()), updateTime);
    }

    public MarchData Get(long id)
    {
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (MarchData) null;
    }

    public List<MarchData> GetMarchListByUserId(long uid)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      using (SortedDictionary<long, MarchData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, MarchData> current = enumerator.Current;
          if (current.Value.ownerUid == uid)
            marchDataList.Add(current.Value);
        }
      }
      return marchDataList;
    }

    public List<long> GetOwnerListByType(MarchData.MarchType marchType)
    {
      return this.marchOwnerList[(int) marchType];
    }

    public List<long> GetTargetListByType(MarchData.MarchType marchType)
    {
      return this.marchTargetList[(int) marchType];
    }

    public bool IsTargetUserBeReinforced(long uid)
    {
      int index = 0;
      for (int count = this.marchOwnerList[10].Count; index < count; ++index)
      {
        if (this.Get(this.marchOwnerList[10][index]).targetUid == uid)
          return true;
      }
      return false;
    }

    private void _AddMarch(MarchData marchData)
    {
      if (marchData == null)
        return;
      if (marchData.ownerUid == GameEngine.Instance.PlayerData.uid && marchData.type != MarchData.MarchType.rally_attack && (marchData.type != MarchData.MarchType.gve_rally_attack && marchData.type != MarchData.MarchType.rab_rally_attack))
      {
        if (this.marchOwner.Contains(marchData.marchId))
        {
          for (int index = 0; index < this.marchOwnerList.Count; ++index)
          {
            if (this.marchOwnerList[index].Contains(marchData.marchId))
              this.marchOwnerList[index].Remove(marchData.marchId);
          }
        }
        else
        {
          this.marchOwner.Add(marchData.marchId);
          if (marchData.IsMarchTypeNeedCount)
            ++this._marchOwnerCount;
        }
        this.marchOwnerList[(int) marchData.type].Add(marchData.marchId);
        this.Publish_OnOwnerDataCreate(marchData.marchId);
      }
      if (marchData.targetUid == GameEngine.Instance.PlayerData.uid && marchData.type != MarchData.MarchType.rally)
      {
        if (this.marchTarget.Contains(marchData.marchId))
        {
          for (int index = 0; index < this.marchTargetList.Count; ++index)
          {
            if (this.marchTargetList[index].Contains(marchData.marchId))
              this.marchTargetList[index].Remove(marchData.marchId);
          }
        }
        else
          this.marchTarget.Add(marchData.marchId);
        this.marchTargetList[(int) marchData.type].Add(marchData.marchId);
        this.Publish_OnTargetDataCreate(marchData.marchId);
      }
      if (marchData.ownerUid == GameEngine.Instance.PlayerData.uid || marchData.ownerUid == GameEngine.Instance.PlayerData.uid || this.othersMarch.Contains(marchData.marchId))
        return;
      this.othersMarch.Add(marchData.marchId);
    }

    private void _RemoveMarch(MarchData marchData)
    {
      if (marchData == null)
        return;
      if (this.marchOwner.Contains(marchData.marchId))
      {
        this.Publish_OnOwnerDataRemove(marchData.marchId);
        this.marchOwner.Remove(marchData.marchId);
        this.marchOwnerList[(int) marchData.type].Remove(marchData.marchId);
        this.Publish_OnOwnerDataRemoved(marchData.marchId);
        if (marchData.IsMarchTypeNeedCount)
          --this._marchOwnerCount;
      }
      if (this.marchTarget.Contains(marchData.marchId))
      {
        this.Publish_OnTargetDataRemove(marchData.marchId);
        this.marchTarget.Remove(marchData.marchId);
        this.marchTargetList[(int) marchData.type].Remove(marchData.marchId);
        this.Publish_OnTargetDataRemoved(marchData.marchId);
      }
      if (!this.othersMarch.Contains(marchData.marchId))
        return;
      this.othersMarch.Remove(marchData.marchId);
    }

    public List<MarchData> GetAttackMarchesByAllianceId(long checkAllianceId)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if ((enumerator.Current.state & (MarchData.MarchState.marching | MarchData.MarchState.rally_attacking)) > MarchData.MarchState.invalid && (enumerator.Current.ownerAllianceId == checkAllianceId && (enumerator.Current.type == MarchData.MarchType.city_attack || enumerator.Current.type == MarchData.MarchType.fortress_attack) || enumerator.Current.targetAllianceId == checkAllianceId && (enumerator.Current.type == MarchData.MarchType.city_attack || enumerator.Current.type == MarchData.MarchType.fortress_attack)))
          marchDataList.Add(enumerator.Current);
      }
      marchDataList.Sort((Comparison<MarchData>) ((a, b) => a.startTime.CompareTo(b.startTime)));
      return marchDataList;
    }

    public List<MarchData> GetMarchesByAllianceIdInCurrentKingdom(long checkAllianceId)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.worldId == PlayerData.inst.playerCityData.cityLocation.K && (enumerator.Current.ownerAllianceId == checkAllianceId || enumerator.Current.targetAllianceId == checkAllianceId))
          marchDataList.Add(enumerator.Current);
      }
      return marchDataList;
    }

    public List<MarchData> Marches
    {
      get
      {
        List<MarchData> marchDataList = new List<MarchData>();
        SortedDictionary<long, MarchData>.Enumerator enumerator = this._datas.GetEnumerator();
        while (enumerator.MoveNext())
          marchDataList.Add(enumerator.Current.Value);
        return marchDataList;
      }
    }

    public List<MarchData> GetReinforceByUid(long uid)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.type == MarchData.MarchType.reinforce && enumerator.Current.targetUid == uid && (enumerator.Current.state == MarchData.MarchState.marching || enumerator.Current.state == MarchData.MarchState.reinforcing))
          marchDataList.Add(enumerator.Current);
      }
      return marchDataList;
    }

    public void ClearThirdPartMarch()
    {
      List<long> longList = new List<long>();
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null && enumerator.Current.ownerUid != PlayerData.inst.uid && enumerator.Current.targetUid != PlayerData.inst.uid)
        {
          if (enumerator.Current.type == MarchData.MarchType.city_attack || enumerator.Current.type == MarchData.MarchType.encamp_attack || (enumerator.Current.type == MarchData.MarchType.gather_attack || enumerator.Current.type == MarchData.MarchType.rally_attack) || (enumerator.Current.type == MarchData.MarchType.fortress_attack || enumerator.Current.type == MarchData.MarchType.temple_attack || (enumerator.Current.type == MarchData.MarchType.wonder_attack || enumerator.Current.type == MarchData.MarchType.rally)) || (enumerator.Current.type == MarchData.MarchType.gve_rally || enumerator.Current.type == MarchData.MarchType.gve_rally_attack))
          {
            if (enumerator.Current.ownerAllianceId != 0L && enumerator.Current.ownerAllianceId == PlayerData.inst.allianceId || enumerator.Current.targetAllianceId != 0L && enumerator.Current.targetAllianceId == PlayerData.inst.allianceId)
              continue;
          }
          else if ((enumerator.Current.type == MarchData.MarchType.reinforce || enumerator.Current.type == MarchData.MarchType.temple_reinforce || (enumerator.Current.type == MarchData.MarchType.wonder_reinforce || enumerator.Current.type == MarchData.MarchType.fortress_reinforce)) && (enumerator.Current.ownerAllianceId != 0L && enumerator.Current.ownerAllianceId == PlayerData.inst.allianceId))
            continue;
          longList.Add(enumerator.Current.marchId);
        }
      }
      for (int index = 0; index < longList.Count; ++index)
      {
        this._datas[longList[index]].Dispose();
        this.Publish_OnDataRemove(longList[index]);
        this._RemoveMarch(this._datas[longList[index]]);
        this._datas.Remove(longList[index]);
        this.Publish_OnDataRemoved();
      }
    }

    public void ClearThirdPartMarch(int left, int top)
    {
      List<long> longList = new List<long>();
      SortedDictionary<long, MarchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null && enumerator.Current.ownerUid != PlayerData.inst.uid && enumerator.Current.targetUid != PlayerData.inst.uid)
        {
          if (PlayerData.inst.allianceId == 0L)
          {
            if (!this.CheckBlock(enumerator.Current, left, top))
              longList.Add(enumerator.Current.marchId);
          }
          else if (enumerator.Current.ownerAllianceId != PlayerData.inst.allianceId && enumerator.Current.targetAllianceId != PlayerData.inst.allianceId && !this.CheckBlock(enumerator.Current, left, top))
            longList.Add(enumerator.Current.marchId);
        }
      }
      for (int index = 0; index < longList.Count; ++index)
      {
        this._datas[longList[index]].Dispose();
        this.Publish_OnDataRemove(longList[index]);
        this._RemoveMarch(this._datas[longList[index]]);
        this._datas.Remove(longList[index]);
        this.Publish_OnDataRemoved();
      }
    }

    private bool CheckBlock(MarchData marchData, int left, int top)
    {
      int b1x = left - (int) marchData.MapData.TilesPerBlock.x;
      int num1 = top - (int) marchData.MapData.TilesPerBlock.y;
      int b2x = left + 2 * (int) marchData.MapData.TilesPerBlock.x;
      int num2 = top + 2 * (int) marchData.MapData.TilesPerBlock.y;
      if (marchData.startLocation.X >= b1x && marchData.startLocation.X < b2x && (marchData.startLocation.Y >= num1 && marchData.startLocation.Y < num2) || marchData.endLocation.X >= b1x && marchData.endLocation.X < b2x && (marchData.endLocation.Y >= num1 && marchData.endLocation.Y < num2) || this.CheckCross(marchData.startLocation.X, marchData.startLocation.Y, marchData.endLocation.X, marchData.endLocation.Y, b1x, num1, b2x, num2))
        return true;
      return this.CheckCross(marchData.startLocation.X, marchData.startLocation.Y, marchData.endLocation.X, marchData.endLocation.Y, b1x, num2, b2x, num1);
    }

    private bool CheckCross(int a1x, int a1y, int a2x, int a2y, int b1x, int b1y, int b2x, int b2y)
    {
      return this.Product((long) (b1x - a1x), (long) (b1y - a1y), (long) (b2x - a1x), (long) (b2y - a1y)) * this.Product((long) (b1x - a2x), (long) (b1y - a2y), (long) (b2x - a2x), (long) (b2y - a2y)) <= 0L && this.Product((long) (a1x - b1x), (long) (a1y - b1y), (long) (a2x - b1x), (long) (a2y - b1y)) * this.Product((long) (a1x - b2x), (long) (a1y - b2y), (long) (a2x - b2x), (long) (a2y - b2y)) <= 0L;
    }

    private long Product(long x1, long y1, long x2, long y2)
    {
      return x1 * y2 - y1 * x2;
    }

    public void ShowInfo()
    {
      string str1 = "<color=blue>March Database Info </color>\n" + "MarchData Count : " + (object) this._datas.Count + "\n" + "Owner count : " + (object) this.marchOwner.Count + "\n" + "Target count : " + (object) this.marchTarget.Count + "\n";
      for (int index = 0; index < 31; ++index)
        str1 = str1 + "March (" + ((MarchData.MarchType) index).ToString() + ") Owner: " + (object) this.marchOwnerList[index].Count + "\n" + "March (" + ((MarchData.MarchType) index).ToString() + ") Target: " + (object) this.marchTargetList[index].Count + "\n";
      string str2 = str1 + "Done List : " + (object) this._doneList.Count + "\n";
      for (int index = 0; index < this._doneList.Count; ++index)
        str2 = str2 + "\t" + (object) this._doneList[index] + "\n";
      using (SortedDictionary<long, MarchData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
        }
      }
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return 0;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      long result = 0;
      if (long.TryParse(hashtable[(object) "march_id"].ToString(), out result))
        return result;
      return 0;
    }
  }
}
