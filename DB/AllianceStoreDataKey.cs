﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceStoreDataKey
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct AllianceStoreDataKey
  {
    public const string ALLIANCE_ID = "alliance_id";
    public const string ITEM_ID = "item_id";
    public long allianceId;
    public int itemId;

    public bool Decode(object data)
    {
      Hashtable inData = data as Hashtable;
      return false | DatabaseTools.UpdateData(inData, "alliance_id", ref this.allianceId) | DatabaseTools.UpdateData(inData, "item_id", ref this.itemId);
    }

    public bool IsValid
    {
      get
      {
        if (this.allianceId != 0L)
          return this.itemId != 0;
        return false;
      }
    }
  }
}
