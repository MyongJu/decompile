﻿// Decompiled with JetBrains decompiler
// Type: DB.UserTreasureChestDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserTreasureChestDB
  {
    protected Dictionary<UserTreasureChestData.Key, UserTreasureChestData> _tableData = new Dictionary<UserTreasureChestData.Key, UserTreasureChestData>();

    public event System.Action<UserTreasureChestData> onDataChanged;

    public event System.Action<UserTreasureChestData> onDataUpdate;

    public event System.Action<UserTreasureChestData> onDataRemove;

    public event System.Action<UserTreasureChestData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (UserTreasureChestData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(UserTreasureChestData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(UserTreasureChestData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(UserTreasureChestData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(UserTreasureChestData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(UserTreasureChestData.Key key, long updateTime)
    {
      if (!this._tableData.ContainsKey(key))
        return false;
      UserTreasureChestData data = this._tableData[key];
      this._tableData.Remove(key);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      UserTreasureChestData.Key key = new UserTreasureChestData.Key();
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "uid", ref key.uid);
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "config_id", ref key.itemId);
        this.Remove(key, updateTime);
      }
    }

    public UserTreasureChestData Get(UserTreasureChestData.Key key)
    {
      if (this._tableData.ContainsKey(key))
        return this._tableData[key];
      return (UserTreasureChestData) null;
    }

    public List<UserTreasureChestData> GetAllUserChest(long userId)
    {
      List<UserTreasureChestData> treasureChestDataList = new List<UserTreasureChestData>();
      using (Dictionary<UserTreasureChestData.Key, UserTreasureChestData>.ValueCollection.Enumerator enumerator = this._tableData.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UserTreasureChestData current = enumerator.Current;
          if (current.Uid == userId && !current.Expired)
            treasureChestDataList.Add(current);
        }
      }
      treasureChestDataList.Sort((Comparison<UserTreasureChestData>) ((a, b) =>
      {
        if (a.AlreadySend && !b.AlreadySend)
          return 1;
        if (!a.AlreadySend && b.AlreadySend)
          return -1;
        return a.CTime.CompareTo(b.CTime);
      }));
      return treasureChestDataList;
    }

    public int GetAllCanSendUserChest(long userId)
    {
      int num = 0;
      using (Dictionary<UserTreasureChestData.Key, UserTreasureChestData>.ValueCollection.Enumerator enumerator = this._tableData.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UserTreasureChestData current = enumerator.Current;
          if (current.Uid == userId && !current.AlreadySend && !current.Expired)
            ++num;
        }
      }
      return num;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      UserTreasureChestData data = new UserTreasureChestData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(new UserTreasureChestData.Key(data.Uid, data.ConfigId)))
        return false;
      this._tableData.Add(new UserTreasureChestData.Key(data.Uid, data.ConfigId), data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      UserTreasureChestData.Key key = new UserTreasureChestData.Key();
      DatabaseTools.UpdateData(inData, "uid", ref key.uid);
      DatabaseTools.UpdateData(inData, "config_id", ref key.itemId);
      if (!this._tableData.ContainsKey(key))
        return this.Add(orgData, updateTime);
      UserTreasureChestData data = this._tableData[key];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }
  }
}
