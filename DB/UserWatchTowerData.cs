﻿// Decompiled with JetBrains decompiler
// Type: DB.UserWatchTowerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class UserWatchTowerData : BaseData, IDBDataDecoder
  {
    public const long INVALID_ID = 0;
    public const int WATCH_TOWER_COLOR_ATTACK = 1;
    public const int WATCH_TOWER_COLOR_REINFORCE = 2;
    public const int WATCH_TOWER_COLOR_TRADE = 3;
    private long _uid;
    private long _eventId;
    private int _isMarch;
    private long _oppUid;
    private int _urgentLevel;
    private int _color;
    private int _ctime;
    private int _worldId;
    private long _allianceId;

    public long AllianceId
    {
      get
      {
        return this._allianceId;
      }
      set
      {
        this._allianceId = value;
      }
    }

    public int WorldId
    {
      get
      {
        return this._worldId;
      }
      set
      {
        this._worldId = value;
      }
    }

    public int CTime
    {
      get
      {
        return this._ctime;
      }
    }

    public int Color
    {
      get
      {
        return this._color;
      }
    }

    public int UrgentLevel
    {
      get
      {
        return this._urgentLevel;
      }
    }

    public long OppUid
    {
      get
      {
        return this._oppUid;
      }
    }

    public bool IsMarch
    {
      get
      {
        return this._isMarch > 0;
      }
    }

    public long EventId
    {
      get
      {
        return this._eventId;
      }
      set
      {
        this._eventId = value;
      }
    }

    public long UID
    {
      get
      {
        return this._uid;
      }
      set
      {
        this._uid = value;
      }
    }

    public bool Decode(string values)
    {
      if (string.IsNullOrEmpty(values) || values.Length != 3)
        return false;
      int result = 0;
      if (!int.TryParse(values, out result))
        return false;
      int num1 = int.Parse(values[2].ToString());
      int num2 = int.Parse(values[1].ToString());
      int num3 = int.Parse(values[0].ToString());
      bool flag = false;
      if (this._isMarch != num1)
      {
        this._isMarch = num1;
        flag = true;
      }
      if (this._urgentLevel != num2)
      {
        this._urgentLevel = num2;
        flag = true;
      }
      if (this._color != num3)
      {
        this._color = num3;
        flag = true;
      }
      return flag;
    }

    public bool Decode(object orgData, long updateTime)
    {
      bool flag = false;
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return flag | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "event_id", ref this._eventId) | DatabaseTools.UpdateData(inData, "is_march", ref this._isMarch) | DatabaseTools.UpdateData(inData, "opp_uid", ref this._oppUid) | DatabaseTools.UpdateData(inData, "urgent_level", ref this._urgentLevel) | DatabaseTools.UpdateData(inData, "color", ref this._color) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId);
    }

    public long ID
    {
      get
      {
        return this._eventId;
      }
    }

    public struct Params
    {
      public const string KEY = "user_watch_tower";
      public const string UID = "uid";
      public const string EVENT_ID = "event_id";
      public const string IS_MARCH = "is_march";
      public const string OPP_UID = "opp_uid";
      public const string URGENT_LEVEL = "urgent_level";
      public const string COLOR = "color";
      public const string CTIME = "ctime";
      public const string WORLD_ID = "world_id";
      public const string ALLIANCE_ID = "alliance_id";
    }
  }
}
