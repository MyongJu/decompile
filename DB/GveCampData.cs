﻿// Decompiled with JetBrains decompiler
// Type: DB.GveCampData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class GveCampData
  {
    public Coordinate location;
    public long jobId;
    public long startTime;
    public long endTime;
    public int knightAmount;
    public List<GveCampData.BossPair> bossList;

    public void Decode(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      DatabaseTools.UpdateData(inData, "world_id", ref this.location.K);
      DatabaseTools.UpdateData(inData, "map_x", ref this.location.X);
      DatabaseTools.UpdateData(inData, "map_y", ref this.location.Y);
      DatabaseTools.UpdateData(inData, "job_id", ref this.jobId);
      DatabaseTools.UpdateData(inData, "knight_amount", ref this.knightAmount);
      DatabaseTools.UpdateData(inData, "time_start", ref this.startTime);
      DatabaseTools.UpdateData(inData, "time_end", ref this.endTime);
      this.bossList = new List<GveCampData.BossPair>();
      Hashtable hashtable = inData[(object) "boss"] as Hashtable;
      if (hashtable == null)
        return;
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      while (enumerator.MoveNext())
        this.bossList.Add(new GveCampData.BossPair()
        {
          index = int.Parse(enumerator.Key.ToString()),
          bossId = int.Parse(enumerator.Value.ToString())
        });
      this.bossList.Sort(new Comparison<GveCampData.BossPair>(GveCampData.Compare));
    }

    private static int Compare(GveCampData.BossPair x, GveCampData.BossPair y)
    {
      return x.index - y.index;
    }

    public struct BossPair
    {
      public int index;
      public int bossId;
    }
  }
}
