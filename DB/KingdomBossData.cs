﻿// Decompiled with JetBrains decompiler
// Type: DB.KingdomBossData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class KingdomBossData : BaseData, IDBDataDecoder
  {
    private Hashtable _totalTroops = new Hashtable();
    private Hashtable _troops = new Hashtable();
    private int _bossId;
    private int _kingdomId;
    private long _placeTime;
    private long _mTime;
    private int _x;
    private int _y;
    private int _hp;

    public int bossId
    {
      get
      {
        return this._bossId;
      }
    }

    public int kingdomId
    {
      get
      {
        return this._kingdomId;
      }
    }

    public int configId
    {
      get
      {
        return this._bossId;
      }
    }

    public long placeTime
    {
      get
      {
        return this._placeTime;
      }
    }

    public long modifyTime
    {
      get
      {
        return this._mTime;
      }
    }

    public long StartTroopCount
    {
      get
      {
        long num = 0;
        IEnumerator enumerator = this._totalTroops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current != null)
          {
            string s = enumerator.Current.ToString();
            long result = 0;
            long.TryParse(s, out result);
            num += result;
          }
        }
        return num;
      }
    }

    public long CurrentTroopCount
    {
      get
      {
        long num = 0;
        if (this._troops != null)
        {
          IEnumerator enumerator = this._troops.Values.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (enumerator.Current != null)
            {
              string s = enumerator.Current.ToString();
              long result = 0;
              long.TryParse(s, out result);
              num += result;
            }
          }
        }
        return num;
      }
    }

    public int HP
    {
      get
      {
        return this._hp;
      }
    }

    public float Progress
    {
      get
      {
        return (float) this._hp / 100f;
      }
    }

    public Coordinate Location
    {
      get
      {
        return new Coordinate()
        {
          K = this.kingdomId,
          X = this._x,
          Y = this._y
        };
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "world_id", ref this._kingdomId) | DatabaseTools.UpdateData(dataHt, "ctime", ref this._placeTime) | DatabaseTools.UpdateData(dataHt, "map_x", ref this._x) | DatabaseTools.UpdateData(dataHt, "map_y", ref this._y) | DatabaseTools.UpdateData(dataHt, "boss_id", ref this._bossId);
      if (dataHt.ContainsKey((object) "start_troops") && dataHt[(object) "start_troops"] != null)
      {
        this._totalTroops.Clear();
        this._totalTroops = dataHt[(object) "start_troops"] as Hashtable;
        flag = true;
      }
      if (dataHt.ContainsKey((object) "troops") && dataHt[(object) "troops"] != null)
      {
        if (this._troops != null)
          this._troops.Clear();
        this._troops = dataHt[(object) "troops"] as Hashtable;
        flag = true;
      }
      return flag | DatabaseTools.UpdateData(dataHt, "hp", ref this._hp) | DatabaseTools.UpdateData(dataHt, "mtime", ref this._mTime);
    }

    public long ID
    {
      get
      {
        return (long) this.bossId;
      }
    }

    public struct Params
    {
      public const string KEY = "world_boss";
      public const string WORLD_MAP_KINGDOME_ID = "world_id";
      public const string WORLD_BOSS_CONFIG_ID = "config_id";
      public const string WORLD_BOSS_ID = "boss_id";
      public const string WORLD_BOSS_PLACE_TIME = "ctime";
      public const string WORLD_BOSS_X = "map_x";
      public const string WORLD_BOSS_Y = "map_y";
      public const string WORLD_BOSS_HP = "hp";
      public const string WORLD_TOTAL_TROOPS = "start_troops";
      public const string WORLD_TROOPS = "troops";
      public const string WORLD_BOSS_M_TIME = "mtime";
    }
  }
}
