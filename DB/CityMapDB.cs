﻿// Decompiled with JetBrains decompiler
// Type: DB.CityMapDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class CityMapDB
  {
    private Dictionary<CityMapKey, CityMapData> _datas = new Dictionary<CityMapKey, CityMapData>();
    private CityMapKey _checkKey;

    public event System.Action<CityMapData> onDataChanged;

    public event System.Action<CityMapData> onDataCreated;

    public event System.Action<CityMapData> onDataUpdated;

    public event System.Action<CityMapData> onDataRemove;

    public event System.Action onDataRemoved;

    public Dictionary<CityMapKey, CityMapData>.ValueCollection.Enumerator cityMapDataEnumerator
    {
      get
      {
        return this._datas.Values.GetEnumerator();
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (CityMapData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(CityMapData cityMapData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(cityMapData);
    }

    public void Publish_onDataCreated(CityMapData cityMapData)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(cityMapData);
      this.Publish_onDataChanged(cityMapData);
    }

    public void Publish_onDataUpdated(CityMapData cityMapData)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(cityMapData);
      this.Publish_onDataChanged(cityMapData);
    }

    public void Publish_onDataRemove(CityMapData cityMapData)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(cityMapData);
      this.Publish_onDataChanged(cityMapData);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      CityMapData cityMapData = new CityMapData();
      CityMapKey key = this.CheckKeyFromData(orgData);
      if (!cityMapData.Decode(orgData, updateTime) || key.isInvalid || this._datas.ContainsKey(key))
        return false;
      this._datas.Add(key, cityMapData);
      this.Publish_onDataCreated(cityMapData);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      CityMapKey key = this.CheckKeyFromData(orgData);
      if (key.isInvalid)
        return false;
      CityMapData cityMapData = this.Get(key);
      if (cityMapData == null)
        return this.Add(orgData, updateTime);
      if (!cityMapData.Decode(orgData, updateTime))
        return false;
      this.Publish_onDataUpdated(cityMapData);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public List<CityMapData> GetByUid_CityId(long uid, long cityId)
    {
      List<CityMapData> cityMapDataList = new List<CityMapData>();
      using (Dictionary<CityMapKey, CityMapData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CityMapData current = enumerator.Current;
          cityMapDataList.Add(current);
        }
      }
      return cityMapDataList;
    }

    public CityMapData Get(long uid, long cityId, long buildingId)
    {
      this._checkKey.uid = uid;
      this._checkKey.cityId = cityId;
      this._checkKey.buildingId = buildingId;
      return this.Get(this._checkKey);
    }

    public CityMapData Get(CityMapKey key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (CityMapData) null;
    }

    public bool Remove(CityMapKey key, long updateTime)
    {
      if (!this._datas.ContainsKey(key) || !this._datas[key].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(this._datas[key]);
      this._datas.Remove(key);
      this.Publish_onDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update((object) this.CheckKeyFromData(arrayList[index]), updateTime);
    }

    private CityMapKey CheckKeyFromData(object orgData)
    {
      this._checkKey.Decode(orgData);
      return this._checkKey;
    }

    public void ShowInfo()
    {
      string str = "<color=blue>City Map Database Info </color>\n" + "City Map count : " + (object) this._datas.Count + "\n";
      using (Dictionary<CityMapKey, CityMapData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CityMapKey current = enumerator.Current;
          str = str + "  City Map : " + current.ShowInfo() + "\n";
        }
      }
      using (Dictionary<CityMapKey, CityMapData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CityMapKey current = enumerator.Current;
        }
      }
    }
  }
}
