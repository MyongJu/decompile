﻿// Decompiled with JetBrains decompiler
// Type: DB.HeroData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public class HeroData : BaseData
  {
    public HeroEquipments equipments = new HeroEquipments();
    public HeroSkills skills = new HeroSkills();
    private int _equipSwitchIndex = 1;
    private Dictionary<int, int> _artifacts = new Dictionary<int, int>();
    private Dictionary<int, int> _artifactsLimited = new Dictionary<int, int>();
    private int lastSpiritModifyTime = -1;
    public const int INVALID_ID = -1;
    private const int ARTIFACT_LIMITED_SLOT_INDEX = 3;
    private int _skillGroupId;
    private long _uid;
    private long _cityId;
    private string _heroName;
    private int _heroIcon;
    private int _status;
    private int _level;
    private long _xp;
    private int _skill_points;
    private int _health;
    private int _healthMax;
    private int _cureRate;
    private long _marchId;
    private long _captureUid;
    private long _captureCityId;
    private long _executePoint;
    private long _ransomNeeded;
    private int _spirit;
    private long _spirit_recover_time;

    public int skillGroupId
    {
      get
      {
        return this._skillGroupId;
      }
    }

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public long cityId
    {
      get
      {
        return this._cityId;
      }
    }

    public string heroName
    {
      get
      {
        return this._heroName;
      }
    }

    public int heroIcon
    {
      get
      {
        return this._heroIcon;
      }
    }

    public int status
    {
      get
      {
        return this._status;
      }
    }

    public int level
    {
      get
      {
        return this._level;
      }
    }

    public long xp
    {
      get
      {
        return this._xp;
      }
    }

    public int skill_points
    {
      get
      {
        return this._skill_points;
      }
    }

    public int health
    {
      get
      {
        return this._health;
      }
    }

    public int healthMax
    {
      get
      {
        return this._healthMax;
      }
    }

    public int cureRate
    {
      get
      {
        return this._cureRate;
      }
    }

    public long marchId
    {
      get
      {
        return this._marchId;
      }
    }

    public long captureUid
    {
      get
      {
        return this._captureUid;
      }
    }

    public long captureCityId
    {
      get
      {
        return this._captureCityId;
      }
    }

    public long executePoint
    {
      get
      {
        return this._executePoint;
      }
    }

    public long ransomNeeded
    {
      get
      {
        return this._ransomNeeded;
      }
    }

    public int EquipSwitchIndex
    {
      get
      {
        return this._equipSwitchIndex;
      }
    }

    public Dictionary<int, int> Artifacts
    {
      get
      {
        return this._artifacts;
      }
    }

    public Dictionary<int, int> ArtifactsLimited
    {
      get
      {
        return this._artifactsLimited;
      }
    }

    public ArtifactData GetArtifactBySlotIndex(int index)
    {
      if (this._artifacts != null && this._artifacts.ContainsKey(index))
      {
        int artifact = this._artifacts[index];
        UserData userData = DBManager.inst.DB_User.Get(this.uid);
        if (userData != null)
          return DBManager.inst.DB_Artifact.GetArtifactData(userData.world_id, artifact);
      }
      return (ArtifactData) null;
    }

    public bool IsArtifactEquiped(int artifactId)
    {
      if (this._artifacts == null)
        return false;
      return this._artifacts.ContainsValue(artifactId);
    }

    public bool IsArtifactLimitedEquiped(int purchasedIndex = -1)
    {
      CityData byUid = DBManager.inst.DB_City.GetByUid(this._uid);
      if (byUid == null)
        return false;
      if (purchasedIndex == -1 && this._artifactsLimited != null && this._artifactsLimited.Count > 0)
        this._artifactsLimited.TryGetValue(3, out purchasedIndex);
      ArtifactLimitedData dataByPurchasedId = byUid.GetArtifactLimitedDataByPurchasedId(purchasedIndex);
      return this._artifactsLimited != null && this._artifactsLimited.ContainsValue(purchasedIndex) && (dataByPurchasedId != null && dataByPurchasedId.EndTime > NetServerTime.inst.ServerTimestamp);
    }

    public int GetEquippedArtifactLimitedBuyIndex()
    {
      int num = -1;
      if (this._artifactsLimited != null && this._artifactsLimited.TryGetValue(3, out num))
        return num;
      return -1;
    }

    public int spirit
    {
      get
      {
        int num1 = 100;
        GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("spirit_max");
        if (data1 != null)
          num1 = data1.ValueInt;
        int num2 = 0;
        GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("prop_spirit_recover_speed_base_value");
        if (data2 != null)
          num2 = Mathf.CeilToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) data2.ValueInt, "calc_spirit_recover_speed"));
        if (this._spirit < num1)
        {
          int num3 = Mathf.CeilToInt((float) ((int) this._spirit_recover_time - NetServerTime.inst.ServerTimestamp) / (float) num2);
          this._spirit = num1 - num3;
        }
        return this._spirit;
      }
      set
      {
        this._spirit = value;
      }
    }

    public long spirit_recover_time
    {
      get
      {
        return this._spirit_recover_time;
      }
      set
      {
        this._spirit_recover_time = value;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null)
        return false;
      Hashtable inData1 = orgData as Hashtable;
      bool flag1 = false | DatabaseTools.UpdateData(inData1, "uid", ref this._uid) | DatabaseTools.UpdateData(inData1, "current_skill_group_id", ref this._skillGroupId) | DatabaseTools.UpdateData(inData1, "city_id", ref this._cityId);
      this._cityId = this._uid;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData1, "name", ref this._heroName) | DatabaseTools.UpdateData(inData1, "icon", ref this._heroIcon) | DatabaseTools.UpdateData(inData1, "status", ref this._status) | DatabaseTools.UpdateData(inData1, "level", ref this._level);
      long xp = this._xp;
      bool flag3;
      bool flag4 = flag2 | (flag3 = DatabaseTools.UpdateData(inData1, "xp", ref this._xp));
      if (flag3 && this.uid == PlayerData.inst.uid)
        GameEngine.Instance.events.Send_OnHeroStatChanged(new Events.HeroStatChangedArgs((int) this.uid, Events.HeroStat.Experience, (double) xp, (double) this._xp));
      bool flag5 = flag4 | DatabaseTools.UpdateData(inData1, "skill_points", ref this._skill_points) | DatabaseTools.UpdateData(inData1, "health", ref this._health) | DatabaseTools.UpdateData(inData1, "health_max", ref this._healthMax) | DatabaseTools.UpdateData(inData1, "cure_rate", ref this._cureRate);
      if (inData1.ContainsKey((object) "equipment"))
        flag5 |= this.equipments.Decode(inData1[(object) "equipment"] as Hashtable);
      bool flag6 = flag5 | DatabaseTools.UpdateData(inData1, "march_id", ref this._marchId) | DatabaseTools.UpdateData(inData1, "capture_uid", ref this._captureUid) | DatabaseTools.UpdateData(inData1, "capture_city_id", ref this._captureCityId) | DatabaseTools.UpdateData(inData1, "execute_points", ref this._executePoint) | DatabaseTools.UpdateData(inData1, "ransom_needed", ref this._ransomNeeded);
      if (inData1.ContainsKey((object) "outfit"))
      {
        Hashtable inData2 = inData1[(object) "outfit"] as Hashtable;
        if (inData2 != null)
          flag6 |= DatabaseTools.UpdateData(inData2, "equip_outfit", ref this._equipSwitchIndex);
      }
      if (inData1.ContainsKey((object) "active_skill"))
      {
        this.skills.Decode(inData1[(object) "active_skill"]);
        flag6 = ((flag6 ? 1 : 0) | 1) != 0;
      }
      if (DatabaseTools.UpdateData(inData1, "spirit", ref this._spirit))
      {
        this.lastSpiritModifyTime = NetServerTime.inst.ServerTimestamp;
        flag6 = ((flag6 ? 1 : 0) | 1) != 0;
      }
      bool flag7 = flag6 | DatabaseTools.UpdateData(inData1, "spirit_recover_time", ref this._spirit_recover_time);
      if (inData1.ContainsKey((object) "artifact"))
      {
        this._artifacts.Clear();
        Hashtable hashtable = inData1[(object) "artifact"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 1;
            int result2 = 0;
            int.TryParse(enumerator.Current.ToString(), out result1);
            int.TryParse(hashtable[(object) enumerator.Current.ToString()].ToString(), out result2);
            if (!this._artifacts.ContainsKey(result1))
              this._artifacts.Add(result1, result2);
          }
        }
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      if (inData1.ContainsKey((object) "artifact_limit"))
      {
        this._artifactsLimited.Clear();
        Hashtable hashtable = inData1[(object) "artifact_limit"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = -1;
            int result2 = -1;
            int.TryParse(enumerator.Current.ToString(), out result1);
            if (hashtable[(object) enumerator.Current.ToString()] != null)
              int.TryParse(hashtable[(object) enumerator.Current.ToString()].ToString(), out result2);
            if (!this._artifactsLimited.ContainsKey(result1))
              this._artifactsLimited.Add(result1, result2);
          }
        }
        flag7 = ((flag7 ? 1 : 0) | 1) != 0;
      }
      return flag7;
    }

    public static int GetHeroId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data) && data.ContainsKey((object) "uid"))
        return int.Parse(data[(object) "uid"].ToString());
      return -1;
    }

    public struct Params
    {
      public const string Key = "user_hero";
      public const string HERO_UID = "uid";
      public const string HERO_CITY_ID = "city_id";
      public const string HERO_NAME = "name";
      public const string HERO_ICON = "icon";
      public const string HERO_STATUS = "status";
      public const string HERO_LEVEL = "level";
      public const string HERO_XP = "xp";
      public const string SKILL_POINTS = "skill_points";
      public const string ACTIVE_SKILL = "active_skill";
      public const string HERO_HEALTH = "health";
      public const string HERO_HEALTH_MAX = "health_max";
      public const string HERO_CURE_RATE = "cure_rate";
      public const string HERO_EQUIPMENT = "equipment";
      public const string HERO_HEAD_ITEM = "head_item";
      public const string HERO_HAIR_ITEM = "hair_item";
      public const string HERO_MARCH_ID = "march_id";
      public const string HERO_CAPTURE_UID = "capture_uid";
      public const string HERO_CAPTURE_CITY_ID = "capture_city_id";
      public const string HERO_EXECUTE_POINT = "execute_points";
      public const string HERO_RANSOM_NEEDED = "ransom_needed";
      public const string HERO_ARTIFACT = "artifact";
      public const string HERO_ARTIFACT_LIMITED = "artifact_limit";
      public const string HERO_OUTFIT = "outfit";
      public const string HERO_OUTFIT_INDEX = "equip_outfit";
      public const string SPIRIT = "spirit";
      public const string SPIRIT_RECOVER_TIME = "spirit_recover_time";
      public const string CURRENT_SKILL_GROUP = "current_skill_group_id";
    }
  }
}
