﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceStoreData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceStoreData : BaseData
  {
    public const string QUANTITY = "quantity";
    private AllianceStoreDataKey _dataKey;
    private int _quantity;

    public AllianceStoreDataKey dataKey
    {
      get
      {
        return this._dataKey;
      }
    }

    public int quantity
    {
      get
      {
        return this._quantity;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | this._dataKey.Decode(orgData) | DatabaseTools.UpdateData(inData, "quantity", ref this._quantity);
    }
  }
}
