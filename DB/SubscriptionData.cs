﻿// Decompiled with JetBrains decompiler
// Type: DB.SubscriptionData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class SubscriptionData : BaseData
  {
    public const int INVALID_ID = 0;
    private long _uid;
    private int _prosperityValue;
    private double _expiration_date;
    private double _last_award_time;
    private bool _subscription_started;

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public int prosperityvalue
    {
      get
      {
        return this._prosperityValue;
      }
    }

    public double expiration_date
    {
      get
      {
        return this._expiration_date;
      }
    }

    public double last_award_time
    {
      get
      {
        return this._last_award_time;
      }
    }

    public bool subscription_started
    {
      get
      {
        return this._subscription_started;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "uid", ref this._uid) | DatabaseTools.UpdateData(dataHt, "prosperity_val", ref this._prosperityValue) | DatabaseTools.UpdateData(dataHt, "expiration_date", ref this._expiration_date) | DatabaseTools.UpdateData(dataHt, "last_award_time", ref this._last_award_time) | DatabaseTools.UpdateData(dataHt, "subscription_started", ref this._subscription_started);
    }

    public static int GetMainKey(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return int.Parse(data[(object) "uid"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Research Id: " + (object) this.uid + ":" + (object) this.prosperityvalue + "\n";
    }

    public struct Params
    {
      public const string KEY = "subscription";
      public const string UID = "uid";
      public const string PROSPERITY_VAL = "prosperity_val";
      public const string EXPIRATION_DATE = "expiration_date";
      public const string LAST_AWARD_TIME = "last_award_time";
      public const string SUBSCRIPTION_STARTED = "subscription_started";
    }
  }
}
