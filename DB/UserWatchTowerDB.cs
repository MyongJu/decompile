﻿// Decompiled with JetBrains decompiler
// Type: DB.UserWatchTowerDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserWatchTowerDB : DBBase
  {
    protected override string GetDataKey()
    {
      return "event_id";
    }

    protected override BaseData CreateData()
    {
      return (BaseData) new UserWatchTowerData();
    }

    public UserWatchTowerData GetDataById(long id)
    {
      return this.GetData(id) as UserWatchTowerData;
    }

    public List<long> GetAllDatas()
    {
      List<long> longList = new List<long>();
      Dictionary<long, BaseData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null)
        {
          IDBDataDecoder current = enumerator.Current as IDBDataDecoder;
          longList.Add(current.ID);
        }
      }
      return longList;
    }

    protected override bool NeedToFilter
    {
      get
      {
        return true;
      }
    }

    protected override bool CustomParse(object orgData)
    {
      Hashtable hashtable = orgData as Hashtable;
      bool flag = false;
      List<long> longList = new List<long>();
      if (hashtable != null)
      {
        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current != null && enumerator.Current.ToString() != "uid")
          {
            string str = enumerator.Current.ToString();
            string values = hashtable[enumerator.Current].ToString();
            string[] strArray = str.Split(':');
            if (strArray.Length != 3)
            {
              flag = ((flag ? 1 : 0) | 0) != 0;
            }
            else
            {
              long num1 = long.Parse(strArray[2]);
              int num2 = int.Parse(strArray[0]);
              int num3 = int.Parse(strArray[1]);
              longList.Add(num1);
              if (this._datas.ContainsKey(num1))
              {
                if (this.GetDataById(num1).Decode(values))
                {
                  this.Publish_OnDataUpdated(num1);
                  flag = ((flag ? 1 : 0) | 1) != 0;
                }
                else
                  flag = ((flag ? 1 : 0) | 0) != 0;
              }
              else
              {
                UserWatchTowerData userWatchTowerData = new UserWatchTowerData();
                userWatchTowerData.WorldId = num2;
                userWatchTowerData.EventId = num1;
                if (userWatchTowerData.Decode(values))
                {
                  if (userWatchTowerData.IsMarch)
                    userWatchTowerData.UID = (long) num3;
                  else
                    userWatchTowerData.AllianceId = (long) num3;
                  this._datas.Add(num1, (BaseData) userWatchTowerData);
                  this.Publish_onDataCreated(num1);
                  flag = ((flag ? 1 : 0) | 1) != 0;
                }
              }
            }
          }
        }
      }
      List<long> allDatas = this.GetAllDatas();
      for (int index = 0; index < allDatas.Count; ++index)
      {
        if (!longList.Contains(allDatas[index]))
          this.Remove(allDatas[index], (long) NetServerTime.inst.ServerTimestamp);
      }
      return flag;
    }

    private List<long> GetCurrentAllList()
    {
      List<long> longList = new List<long>();
      Dictionary<long, BaseData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator();
      while (enumerator.MoveNext())
        longList.Add(enumerator.Current);
      return longList;
    }

    protected override void OnRemove()
    {
      List<long> currentAllList = this.GetCurrentAllList();
      for (int index = 0; index < currentAllList.Count; ++index)
        this.Remove(currentAllList[index], (long) NetServerTime.inst.ServerTimestamp);
    }

    protected override bool IsCustomParse
    {
      get
      {
        return true;
      }
    }
  }
}
