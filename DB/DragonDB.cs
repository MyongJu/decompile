﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonDB
  {
    private Dictionary<long, DragonData> datas = new Dictionary<long, DragonData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataRemove;

    public event System.Action<long> onDataCreate;

    public Dictionary<long, DragonData> Datas
    {
      get
      {
        return this.datas;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (DragonData), (long) this.datas.Count);
    }

    private void Publish_OnDataChanged(long generalID)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(generalID);
    }

    private void Publish_OnDataCreate(long generalID)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataUpdate(long generalID)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataRemove(long generalID)
    {
      this.Publish_OnDataChanged(generalID);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(generalID);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      DragonData dragonData = new DragonData();
      if (!dragonData.Decode((object) hashtable, updateTime) || this.datas.ContainsKey(dragonData.Uid))
        return false;
      this.datas.Add(dragonData.Uid, dragonData);
      this.Publish_OnDataCreate(dragonData.Uid);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = long.Parse(hashtable[(object) "uid"].ToString());
      if (!this.datas.ContainsKey(index) || !this.datas[index].Decode(orgData, updateTime))
        return this.Add(orgData, updateTime);
      this.Publish_OnDataUpdate(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public DragonData Get(long id)
    {
      if (this.datas.ContainsKey(id))
        return this.datas[id];
      return (DragonData) null;
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this.datas.ContainsKey(id) || !this.datas[id].CheckUpdateTime(updateTime))
        return false;
      this.datas.Remove(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse(arrayList[index].ToString()), updateTime);
    }
  }
}
