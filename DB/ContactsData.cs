﻿// Decompiled with JetBrains decompiler
// Type: DB.ContactsData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class ContactsData : BaseData
  {
    private long _contactUid;
    private int _status;
    private int _shutUp_time;
    private int _block_time;

    public long contactUid
    {
      get
      {
        return this._contactUid;
      }
    }

    public ContactsData.ContactStatus status
    {
      get
      {
        return (ContactsData.ContactStatus) this._status;
      }
    }

    public bool IsShutUp
    {
      get
      {
        return this._shutUp_time > NetServerTime.inst.ServerTimestamp;
      }
    }

    public bool IsBlocked
    {
      get
      {
        return this._block_time > NetServerTime.inst.ServerTimestamp;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "contact_uid", ref this._contactUid) | DatabaseTools.UpdateData(dataHt, "contact_status", ref this._status) | DatabaseTools.UpdateData(dataHt, "shutup_time", ref this._shutUp_time);
    }

    public static long GetUid(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return long.Parse(data[(object) "uid"].ToString());
      return 0;
    }

    public static long GetContactUid(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return long.Parse(data[(object) "contact_uid"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Contact Uit : " + (object) this.contactUid + ":" + this.status.ToString();
    }

    public enum ContactStatus
    {
      STATUS_FRIEND = 1,
      STATUS_INVITING = 2,
      STATUS_BE_INVITED = 4,
      STATUS_BLOCKING = 8,
      STATUS_BE_BLOCKED = 16, // 0x00000010
      STATUS_TEMP_BLOCKED = 18, // 0x00000012
    }

    public struct Params
    {
      public const string KEY = "user_contacts";
      public const string CONTACTS_UID = "uid";
      public const string CONTACTS_CONTACTS_UID = "contact_uid";
      public const string CONTACTS_STATUS = "contact_status";
      public const string SHUTUP_TIME = "shutup_time";
      public const string BLOCK_TIME = "block_time";
    }
  }
}
