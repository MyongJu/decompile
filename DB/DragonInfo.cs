﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DragonInfo
  {
    public int level;
    public DragonData.Tendency tendency;

    public DragonInfo()
    {
      this.level = 0;
      this.tendency = DragonData.Tendency.normal;
    }

    public DragonInfo(DragonInfo orgData)
    {
      this.level = orgData.level;
      this.tendency = orgData.tendency;
    }

    public bool isDragonIn
    {
      get
      {
        return this.level > 0;
      }
    }

    public void Clear()
    {
      this.level = 0;
      this.tendency = DragonData.Tendency.normal;
    }

    public bool Decode(object orgData)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return false;
      DatabaseTools.UpdateData(data, "level", ref this.level);
      int outData = 0;
      DatabaseTools.UpdateData(data, "tendency", ref outData);
      this.tendency = (DragonData.Tendency) outData;
      return true;
    }

    public string ShowInfo()
    {
      return string.Empty + "dragon : (" + (object) this.level + ") : " + (object) this.tendency + "\n";
    }

    public struct Param
    {
      public const string LEVEL = "level";
      public const string TENDENCY = "tendency";
    }
  }
}
