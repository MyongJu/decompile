﻿// Decompiled with JetBrains decompiler
// Type: DB.SevenQuestDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace DB
{
  public class SevenQuestDB : DBBase
  {
    protected override BaseData CreateData()
    {
      return (BaseData) new SevenQuestData();
    }

    protected override string GetDataKey()
    {
      return "quest_id";
    }

    protected override bool NeedToFilter
    {
      get
      {
        return true;
      }
    }

    public SevenQuestData GetSevenQuestData(int configid)
    {
      return this.GetData((long) configid) as SevenQuestData;
    }

    public List<SevenQuestData> GetSevenQuestDataByDay(int day)
    {
      List<SevenQuestData> sevenQuestDataList = new List<SevenQuestData>();
      Dictionary<long, BaseData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null)
        {
          SevenQuestData current = enumerator.Current as SevenQuestData;
          if (current.Days == day)
            sevenQuestDataList.Add(current);
        }
      }
      return sevenQuestDataList;
    }

    public bool IsAllClaim(int day)
    {
      List<SevenQuestData> sevenQuestDataByDay = this.GetSevenQuestDataByDay(day);
      bool flag = true;
      if (sevenQuestDataByDay.Count == 0)
        return false;
      for (int index = 0; index < sevenQuestDataByDay.Count; ++index)
      {
        if (!sevenQuestDataByDay[index].IsClaim)
        {
          flag = false;
          break;
        }
      }
      return flag;
    }

    public bool IsFinish(int day)
    {
      List<SevenQuestData> sevenQuestDataByDay = this.GetSevenQuestDataByDay(day);
      bool flag = false;
      for (int index = 0; index < sevenQuestDataByDay.Count; ++index)
      {
        flag = sevenQuestDataByDay[index].IsFinish;
        if (flag)
          break;
      }
      return flag;
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (SevenQuestData), (long) this._datas.Count);
    }
  }
}
