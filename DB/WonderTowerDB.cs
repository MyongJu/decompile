﻿// Decompiled with JetBrains decompiler
// Type: DB.WonderTowerDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class WonderTowerDB
  {
    private Dictionary<long, WonderTowerData> _datas = new Dictionary<long, WonderTowerData>();

    public event System.Action<WonderTowerData> onDataChanged;

    public event System.Action<WonderTowerData> onDataUpdate;

    public event System.Action<WonderTowerData> onDataCreate;

    public event System.Action<WonderTowerData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (WonderTowerData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(WonderTowerData wondertowerData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(wondertowerData);
    }

    private void PublishOnDataCreateEvent(WonderTowerData wondertowerData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(wondertowerData);
      this.PublishOnDataChangedEvent(wondertowerData);
    }

    private void PublishOnDataUpdateEvent(WonderTowerData wondertowerData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(wondertowerData);
      this.PublishOnDataChangedEvent(wondertowerData);
    }

    private void PublishOnDataRemovedEvent(WonderTowerData wondertowerData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(wondertowerData);
      this.PublishOnDataChangedEvent(wondertowerData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      WonderTowerData wondertowerData = new WonderTowerData();
      if (!wondertowerData.Decode((object) hashtable, updateTime))
        return false;
      this._datas.Add(this.GetWonderTowerTableKey(wondertowerData.TOWER_ID, (long) wondertowerData.WORLD_ID), wondertowerData);
      this.PublishOnDataCreateEvent(wondertowerData);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData1 = 0;
      long outData2 = 0;
      DatabaseTools.UpdateData(inData, "tower_id", ref outData1);
      DatabaseTools.UpdateData(inData, "world_id", ref outData2);
      long wonderTowerTableKey = this.GetWonderTowerTableKey(outData1, outData2);
      if (!this._datas.ContainsKey(wonderTowerTableKey))
        return this.Add(orgData, updateTime);
      WonderTowerData data = this._datas[wonderTowerTableKey];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long tableKey, long updateTime)
    {
      if (!this._datas.ContainsKey(tableKey))
        return false;
      WonderTowerData data = this._datas[tableKey];
      this._datas.Remove(tableKey);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData1 = 0;
        long outData2 = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "tower_id", ref outData1);
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "world_id", ref outData2);
        this.Remove(this.GetWonderTowerTableKey(outData1, outData2), updateTime);
      }
    }

    public List<MarchData> GetMarchsByWonderTowerKey(long wonderTowerKey)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Get(wonderTowerKey).Troops.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
        if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
          marchDataList.Add(marchData);
      }
      return marchDataList;
    }

    public WonderTowerData Get(long tableKey)
    {
      WonderTowerData wonderTowerData;
      this._datas.TryGetValue(tableKey, out wonderTowerData);
      return wonderTowerData;
    }

    public WonderTowerData Get(long towerid, long worldid)
    {
      WonderTowerData wonderTowerData;
      this._datas.TryGetValue(this.GetWonderTowerTableKey(towerid, worldid), out wonderTowerData);
      return wonderTowerData;
    }

    public long GetWonderTowerTableKey(long wondertowerId, long worldId)
    {
      return worldId * 10L + wondertowerId;
    }
  }
}
