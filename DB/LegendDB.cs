﻿// Decompiled with JetBrains decompiler
// Type: DB.LegendDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class LegendDB
  {
    private Dictionary<long, LegendData> datas = new Dictionary<long, LegendData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataRemove;

    public event System.Action<long> onDataCreate;

    public Dictionary<long, LegendData> Datas
    {
      get
      {
        return this.datas;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (LegendData), (long) this.datas.Count);
    }

    private void Publish_OnDataChanged(long generalID)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(generalID);
    }

    private void Publish_OnDataCreate(long generalID)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataUpdate(long generalID)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataRemove(long generalID)
    {
      this.Publish_OnDataChanged(generalID);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(generalID);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null || long.Parse(hashtable[(object) "uid"].ToString()) != PlayerData.inst.uid)
        return false;
      LegendData legendData = new LegendData();
      if (!legendData.Decode((object) hashtable, updateTime) || this.datas.ContainsKey((long) legendData.LegendID))
        return false;
      this.datas.Add((long) legendData.LegendID, legendData);
      this.Publish_OnDataCreate((long) legendData.LegendID);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null || long.Parse(hashtable[(object) "uid"].ToString()) != PlayerData.inst.uid)
        return false;
      long index = long.Parse(hashtable[(object) "legend_id"].ToString());
      if (!this.datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this.datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this.datas.ContainsKey(id) || !this.datas[id].CheckUpdateTime(updateTime))
        return false;
      this.datas.Remove(id);
      return true;
    }

    public LegendData GetLegend(long legendId)
    {
      return this.datas[legendId];
    }

    public List<long> GetLegendList(int status)
    {
      List<long> longList = new List<long>();
      Dictionary<long, LegendData>.KeyCollection.Enumerator enumerator = this.datas.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (this.datas[enumerator.Current].Status == status)
          longList.Add(enumerator.Current);
      }
      return longList;
    }

    public List<LegendData> GetCurrentLegend(int status)
    {
      List<LegendData> legendDataList = new List<LegendData>();
      using (Dictionary<long, LegendData>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LegendData current = enumerator.Current;
          if (current.Status == status)
            legendDataList.Add(current);
        }
      }
      return legendDataList;
    }

    public bool CheckLegendIsAviable(int legendId)
    {
      return this.datas[(long) legendId].Status == 0;
    }

    public bool CheckLegendIsMarching(int legendId)
    {
      return this.datas[(long) legendId].Status == 1;
    }

    public bool CheckLegendIsUnderRecruit(int legendId)
    {
      bool flag = false;
      List<LegendData> underRecruitLegends = this.GetTotalUnderRecruitLegends();
      for (int index = 0; index < underRecruitLegends.Count; ++index)
      {
        if (underRecruitLegends[index].LegendID == legendId)
        {
          flag = true;
          break;
        }
      }
      return flag;
    }

    public List<LegendData> GetTotalUnderRecruitLegends()
    {
      List<LegendData> legendDataList = new List<LegendData>();
      using (Dictionary<long, LegendData>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LegendData current = enumerator.Current;
          if (current.Status != 3)
            legendDataList.Add(current);
        }
      }
      return legendDataList;
    }

    public LegendData GetLegendDataByLegendID(int legend_id)
    {
      using (Dictionary<long, LegendData>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LegendData current = enumerator.Current;
          if (current.LegendID == legend_id)
            return current;
        }
      }
      return (LegendData) null;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse((arrayList[index] as Hashtable)[(object) "legend_id"].ToString()), updateTime);
    }

    public bool IsMaxSlot()
    {
      return ConfigManager.inst.DB_GameConfig.GetData("legend_slot_number_max").ValueInt == this.GetSlotNum();
    }

    public bool IsMaxLegendCount()
    {
      return this.GetTotalUnderRecruitLegends().Count == this.GetSlotNum();
    }

    public int GetSlotNum()
    {
      List<CityMapData> byUidCityId = DBManager.inst.DB_CityMap.GetByUid_CityId(PlayerData.inst.uid, (long) PlayerData.inst.cityId);
      int num = 0;
      using (List<CityMapData>.Enumerator enumerator = byUidCityId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          List<InBuildingData> inBuildingData = enumerator.Current.inBuildingData;
          for (int index = 0; index < inBuildingData.Count; ++index)
          {
            if (inBuildingData[index].type == "slot_num")
            {
              num = int.Parse(inBuildingData[index].value);
              break;
            }
          }
        }
      }
      return num;
    }
  }
}
