﻿// Decompiled with JetBrains decompiler
// Type: DB.NpcStoreLocalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class NpcStoreLocalDB
  {
    private List<int> _goods;
    private long _syncTime;
    private int _buyTimes;

    public long updateTime { private set; get; }

    public int MAX_SLOT_COUNT
    {
      get
      {
        return (int) ConfigManager.inst.DB_GameConfig.GetData("npc_store_slot_number").Value;
      }
    }

    public long syncTime
    {
      get
      {
        return this._syncTime;
      }
    }

    public int buyTimes
    {
      get
      {
        return this._buyTimes;
      }
    }

    public bool Update(object orgData, long inUpdateTime)
    {
      Hashtable data1;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data1) || data1.Contains((object) "uid") && long.Parse(data1[(object) "uid"].ToString()) != PlayerData.inst.uid)
        return false;
      bool flag = false | DatabaseTools.UpdateData(data1, "last_sync_time", ref this._syncTime) | DatabaseTools.UpdateData(data1, "buy_times", ref this._buyTimes);
      if (this._goods == null || this._goods.Count < this.MAX_SLOT_COUNT)
      {
        this._goods = new List<int>(this.MAX_SLOT_COUNT);
        for (int index = 0; index < this.MAX_SLOT_COUNT; ++index)
          this._goods.Add(0);
      }
      Hashtable data2;
      if (DatabaseTools.CheckAndParseOrgData(data1[(object) "goods"], out data2))
      {
        IEnumerator enumerator = data2.Keys.GetEnumerator();
        while (enumerator.MoveNext())
          this._goods[int.Parse(enumerator.Current.ToString()) - 1] = int.Parse(data2[enumerator.Current].ToString());
        flag = true;
      }
      return flag;
    }

    public void UpdateDatas(object orgDatas, long inUpdateTime)
    {
      if (inUpdateTime <= this.updateTime)
        return;
      this.updateTime = inUpdateTime;
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], inUpdateTime);
    }

    public List<int> GetGoods()
    {
      return this._goods;
    }

    public struct Param
    {
      public const string KEY = "user_npc_store";
      public const string UID = "uid";
      public const string LAST_SYNC_TIME = "last_sync_time";
      public const string GOODS = "goods";
      public const string BUY_TIMES = "buy_times";
    }
  }
}
