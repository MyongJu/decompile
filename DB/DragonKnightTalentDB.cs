﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightTalentDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightTalentDB
  {
    private Dictionary<int, DragonKnightTalentData> _datas = new Dictionary<int, DragonKnightTalentData>();
    public System.Action<int> onDataChanged;
    public System.Action<int> onDataCreated;
    public System.Action<int> onDataUpdated;
    public System.Action<int> onDataRemove;
    public System.Action onDataRemoved;

    public Dictionary<int, DragonKnightTalentData> Datas
    {
      get
      {
        return this._datas;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (DragonKnightTalentData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(int itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(int itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(int itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(int itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      DragonKnightTalentData knightTalentData = new DragonKnightTalentData();
      if (!knightTalentData.Decode((object) data, updateTime) || (long) knightTalentData.UserId == 0L || this._datas.ContainsKey(knightTalentData.TalentId))
        return false;
      this._datas.Add(knightTalentData.TalentId, knightTalentData);
      this.Publish_onDataCreated(knightTalentData.TalentId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int talentId = DragonKnightTalentData.GetTalentId((object) data);
      if (talentId == 0)
        return false;
      if (!this._datas.ContainsKey(talentId))
        return this.Add(orgData, updateTime);
      if (!this._datas[talentId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated(talentId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public DragonKnightTalentData Get(int key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (DragonKnightTalentData) null;
    }

    public bool Remove(int talentId, long updateTime)
    {
      if (!this._datas.ContainsKey(talentId) || !this._datas[talentId].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(talentId);
      this._datas.Remove(talentId);
      this.Publish_onDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(DragonKnightTalentData.GetTalentId(arrayList[index]), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<int, DragonKnightTalentData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DragonKnightTalentData current = enumerator.Current;
        }
      }
    }

    public bool IsTalentOwned(int talentId)
    {
      return this._datas.ContainsKey(talentId);
    }

    public List<DragonKnightTalentData> GetTalentListByTreeIndex(int treeIndex)
    {
      List<DragonKnightTalentData> knightTalentDataList = new List<DragonKnightTalentData>();
      Dictionary<int, DragonKnightTalentData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        DragonKnightTalentData knightTalentData = enumerator.Current.Value;
        DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(knightTalentData.TalentId);
        if (knightTalentInfo != null)
        {
          DragonKnightTalentTreeInfo talentTreeInfo = ConfigManager.inst.DB_DragonKnightTalentTree.GetTalentTreeInfo(knightTalentInfo.talentTreeInternalId);
          if (talentTreeInfo != null && talentTreeInfo.tree == treeIndex)
            knightTalentDataList.Add(knightTalentData);
        }
      }
      return knightTalentDataList;
    }

    public int GetTalentTreeTalentCount(int treeIndex)
    {
      return this.GetTalentListByTreeIndex(treeIndex).Count;
    }
  }
}
