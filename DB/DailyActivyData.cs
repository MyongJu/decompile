﻿// Decompiled with JetBrains decompiler
// Type: DB.DailyActivyData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DailyActivyData : BaseData
  {
    public const int INVALID_ID = 0;
    private int progress;
    private int uid;
    private int currentPoint;
    private bool isChanged;
    private int daily_activy_id;
    private int updateTime;

    public int Progress
    {
      get
      {
        return this.progress;
      }
    }

    public int UID
    {
      get
      {
        return this.uid;
      }
    }

    private int CurrentPoint
    {
      get
      {
        return this.currentPoint;
      }
    }

    public bool IsDataChanged
    {
      get
      {
        return this.isChanged;
      }
    }

    public int DailyActivyID
    {
      get
      {
        return this.daily_activy_id;
      }
    }

    public int CurrentScore
    {
      get
      {
        DailyActivyInfo dailyActivyInfo = ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.daily_activy_id);
        int num = 0;
        int maxScore = dailyActivyInfo.MaxScore;
        if (dailyActivyInfo != null)
        {
          num = this.Progress / dailyActivyInfo.Value * dailyActivyInfo.Score;
          if (num > maxScore)
            num = maxScore;
        }
        return num;
      }
    }

    public bool IsFinish
    {
      get
      {
        return ConfigManager.inst.DB_DailyActivies.GetDailyActivyInfo(this.daily_activy_id).MaxScore == this.CurrentScore;
      }
    }

    public int UpdateTime
    {
      get
      {
        return this.updateTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag1 = false;
      int outData = 0;
      bool flag2 = flag1 | DatabaseTools.UpdateData(dataHt, "progress", ref outData);
      if (outData != this.progress)
        this.progress = outData;
      if (flag2 && this.progress > 0)
        this.isChanged = flag2;
      return flag2 | DatabaseTools.UpdateData(dataHt, "uid", ref this.uid) | DatabaseTools.UpdateData(dataHt, "quest_id", ref this.daily_activy_id) | DatabaseTools.UpdateData(dataHt, "mtime", ref updateTime);
    }

    public static bool CheckUID(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long result = 0;
      if (hashtable.ContainsKey((object) "uid"))
        long.TryParse(hashtable[(object) "uid"].ToString(), out result);
      return PlayerData.inst.uid == result;
    }

    public static int GetInternalId(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      int result = 0;
      if (hashtable.ContainsKey((object) "quest_id"))
        int.TryParse(hashtable[(object) "quest_id"].ToString(), out result);
      return result;
    }

    public struct Params
    {
      public const string KEY = "daily_quest";
      public const string PROGRESS = "progress";
      public const string UID = "uid";
      public const string DAILY_ACTIVY_ID = "quest_id";
      public const string MODIFY_TIME = "mtime";
    }
  }
}
