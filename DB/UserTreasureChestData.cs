﻿// Decompiled with JetBrains decompiler
// Type: DB.UserTreasureChestData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class UserTreasureChestData : BaseData
  {
    private long _uid;
    private int _configId;
    private string _status;
    private int _cTime;
    private int _expireTime;

    public bool AlreadySend
    {
      get
      {
        return this._status == "done";
      }
    }

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public string Status
    {
      get
      {
        return this._status;
      }
    }

    public int CTime
    {
      get
      {
        return this._cTime;
      }
    }

    public int ExpireTime
    {
      get
      {
        return this._expireTime;
      }
    }

    public bool Expired
    {
      get
      {
        if (this._status == "done")
          return this._expireTime < NetServerTime.inst.ServerTimestamp;
        return false;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "config_id", ref this._configId) | DatabaseTools.UpdateData(inData, "status", ref this._status) | DatabaseTools.UpdateData(inData, "ctime", ref this._cTime) | DatabaseTools.UpdateData(inData, "expire_time", ref this._expireTime);
    }

    public struct Key
    {
      public long uid;
      public int itemId;

      public Key(long uid, int itemId)
      {
        this.uid = uid;
        this.itemId = itemId;
      }
    }

    public struct Params
    {
      public const string KEY = "user_treasure_chest";
      public const string UID = "uid";
      public const string CONFIG_ID = "config_id";
      public const string STATUS = "status";
      public const string CTIME = "ctime";
      public const string EXPIRE_TIME = "expire_time";
    }
  }
}
