﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceHospitalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceHospitalDB
  {
    private Dictionary<long, AllianceHospitalData> m_HospitalDict = new Dictionary<long, AllianceHospitalData>();

    public event System.Action<AllianceHospitalData> onDataChanged;

    public event System.Action<AllianceHospitalData> onDataUpdate;

    public event System.Action<AllianceHospitalData> onDataRemove;

    public event System.Action<AllianceHospitalData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceHospitalData), (long) this.m_HospitalDict.Count);
    }

    public void Clear()
    {
      this.m_HospitalDict.Clear();
    }

    private void Publish_OnDataChanged(AllianceHospitalData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(AllianceHospitalData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(AllianceHospitalData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(AllianceHospitalData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long buildingId, long updateTime)
    {
      if (!this.m_HospitalDict.ContainsKey(buildingId))
        return false;
      AllianceHospitalData data = this.m_HospitalDict[buildingId];
      this.m_HospitalDict.Remove(buildingId);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "building_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AllianceHospitalData Get(long buildingId)
    {
      if (this.m_HospitalDict.ContainsKey(buildingId))
        return this.m_HospitalDict[buildingId];
      return (AllianceHospitalData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceHospitalData data = new AllianceHospitalData();
      if (!data.Decode((object) hashtable, updateTime) || this.m_HospitalDict.ContainsKey(data.BuildingID))
        return false;
      this.m_HospitalDict.Add(data.BuildingID, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "building_id", ref outData);
      if (!this.m_HospitalDict.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceHospitalData data = this.m_HospitalDict[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public AllianceHospitalData GetMyHospitalData()
    {
      using (Dictionary<long, AllianceHospitalData>.Enumerator enumerator = this.m_HospitalDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceHospitalData> current = enumerator.Current;
          if (current.Value.AllianceID == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceHospitalData) null;
    }

    public AllianceHospitalData GetMyHospitalDataByBuildingConfigId(int allianceBuildingConfigId)
    {
      using (Dictionary<long, AllianceHospitalData>.Enumerator enumerator = this.m_HospitalDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceHospitalData> current = enumerator.Current;
          if (current.Value.AllianceID == PlayerData.inst.allianceId && current.Value.ConfigId == allianceBuildingConfigId)
            return current.Value;
        }
      }
      return (AllianceHospitalData) null;
    }

    public AllianceHospitalData GetDataByCoordinate(Coordinate location)
    {
      using (Dictionary<long, AllianceHospitalData>.Enumerator enumerator = this.m_HospitalDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceHospitalData> current = enumerator.Current;
          if (current.Value.Location.K == location.K && current.Value.Location.X == location.X && current.Value.Location.Y == location.Y)
            return current.Value;
        }
      }
      return (AllianceHospitalData) null;
    }
  }
}
