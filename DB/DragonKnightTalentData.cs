﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightTalentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DragonKnightTalentData : BaseData
  {
    public const int INVALID_ID = 0;
    private int _uid;
    private int _talentId;

    public int UserId
    {
      get
      {
        return this._uid;
      }
    }

    public int TalentId
    {
      get
      {
        return this._talentId;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "uid", ref this._uid) | DatabaseTools.UpdateData(dataHt, "talent_id", ref this._talentId);
    }

    public static int GetTalentId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return int.Parse(data[(object) "talent_id"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "talent info: " + (object) this.UserId + ":" + (object) this.TalentId + "\n";
    }

    public struct Params
    {
      public const string KEY = "dragon_knight_talent";
      public const string USER_ID = "uid";
      public const string TALENT_ID = "talent_id";
    }
  }
}
