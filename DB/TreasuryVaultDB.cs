﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasuryVaultDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TreasuryVaultDB
  {
    protected Dictionary<long, TreasuryVaultData> _tableData = new Dictionary<long, TreasuryVaultData>();

    public event Action<TreasuryVaultData> onDataChanged;

    public event Action<TreasuryVaultData> onDataUpdate;

    public event Action<TreasuryVaultData> onDataRemove;

    public event Action<TreasuryVaultData> onDataCreate;

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(TreasuryVaultData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(TreasuryVaultData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(TreasuryVaultData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(TreasuryVaultData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this._tableData.ContainsKey(id))
        return false;
      TreasuryVaultData data = this._tableData[id];
      this._tableData.Remove(id);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public TreasuryVaultData Get(long id)
    {
      if (this._tableData.ContainsKey(id))
        return this._tableData[id];
      return (TreasuryVaultData) null;
    }

    public List<TreasuryVaultData> GetAllChest(long allianceId)
    {
      List<TreasuryVaultData> treasuryVaultDataList = new List<TreasuryVaultData>();
      using (Dictionary<long, TreasuryVaultData>.ValueCollection.Enumerator enumerator = this._tableData.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TreasuryVaultData current = enumerator.Current;
          if (current.AllianceId == allianceId)
            treasuryVaultDataList.Add(current);
        }
      }
      return treasuryVaultDataList;
    }

    public List<TreasuryVaultData> GetAllCanReceiveChest(long allianceId)
    {
      List<TreasuryVaultData> treasuryVaultDataList = new List<TreasuryVaultData>();
      using (Dictionary<long, TreasuryVaultData>.ValueCollection.Enumerator enumerator = this._tableData.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TreasuryVaultData current = enumerator.Current;
          if (current.AllianceId == allianceId && current.CanReceive)
            treasuryVaultDataList.Add(current);
        }
      }
      return treasuryVaultDataList;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      TreasuryVaultData data = new TreasuryVaultData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.Id))
        return false;
      this._tableData.Add(data.Id, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "id", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      TreasuryVaultData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }
  }
}
