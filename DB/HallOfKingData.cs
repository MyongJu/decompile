﻿// Decompiled with JetBrains decompiler
// Type: DB.HallOfKingData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class HallOfKingData : BaseData
  {
    private Dictionary<int, long> _skillCds = new Dictionary<int, long>();
    private Dictionary<int, int> _skillUseTimes = new Dictionary<int, int>();
    private long _worldId;
    private string _kingMessage;
    private int _kingdomCoin;

    public long WorldId
    {
      get
      {
        return this._worldId;
      }
    }

    public string KingMessage
    {
      get
      {
        return this._kingMessage;
      }
    }

    public int KingdomCoin
    {
      get
      {
        return this._kingdomCoin;
      }
    }

    public long GetLeftSkillCdTime(int skillId)
    {
      long num = 0;
      if (this._skillCds.ContainsKey(skillId))
        num = this._skillCds[skillId] - (long) NetServerTime.inst.ServerTimestamp;
      return num;
    }

    public int GetSkillUsedTimes(int skillId)
    {
      int num = 0;
      if (this._skillUseTimes.ContainsKey(skillId))
        num = this._skillUseTimes[skillId];
      return num;
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "world_id", ref this._worldId) | DatabaseTools.UpdateData(dataHt, "king_message", ref this._kingMessage) | DatabaseTools.UpdateData(dataHt, "kingdom_coin", ref this._kingdomCoin);
      if (dataHt.ContainsKey((object) "skill_cd_time"))
      {
        this._skillCds.Clear();
        Hashtable hashtable = dataHt[(object) "skill_cd_time"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            long result2 = 0;
            if (int.TryParse(enumerator.Key.ToString(), out result1) && long.TryParse(enumerator.Value.ToString(), out result2))
              this._skillCds.Add(result1, result2);
            else
              D.error((object) "parse skill cd time error.");
          }
        }
        flag = true;
      }
      if (dataHt.ContainsKey((object) "skill_use_times"))
      {
        this._skillUseTimes.Clear();
        Hashtable hashtable = dataHt[(object) "skill_use_times"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            int result2 = 0;
            if (int.TryParse(enumerator.Key.ToString(), out result1) && int.TryParse(enumerator.Value.ToString(), out result2))
              this._skillUseTimes.Add(result1, result2);
            else
              D.error((object) "parse skill cd time error.");
          }
        }
        flag = true;
      }
      return flag;
    }

    public struct Params
    {
      public const string Key = "hall_of_king";
      public const string WORLD_ID = "world_id";
      public const string KING_MESSAGE = "king_message";
      public const string SKILL_CD_TIME = "skill_cd_time";
      public const string KINGDOM_COIN = "kingdom_coin";
      public const string SKILL_USE_TIME = "skill_use_times";
    }
  }
}
