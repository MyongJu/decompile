﻿// Decompiled with JetBrains decompiler
// Type: DB.AchievementDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AchievementDB
  {
    private Dictionary<int, AchievementData> _datas = new Dictionary<int, AchievementData>();
    private List<AchievementData> totals = new List<AchievementData>();
    public System.Action onPointChanged;
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;
    public System.Action onDataSetChanged;

    public AchievementData Get(int questId)
    {
      if (this._datas.ContainsKey(questId))
        return this._datas[questId];
      return (AchievementData) null;
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AchievementData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataSetChanged()
    {
      if (this.onDataSetChanged == null)
        return;
      this.onDataSetChanged();
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AchievementData achievementData = new AchievementData();
      if (!achievementData.Decode((object) hashtable, updateTime) || achievementData.AchievementId == 0 || this._datas.ContainsKey(achievementData.AchievementId))
        return false;
      this._datas.Add(achievementData.AchievementId, achievementData);
      this.Publish_onDataCreated((long) achievementData.AchievementId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null || !AchievementData.CheckUID(orgData))
        return false;
      int internalId = AchievementData.GetInternalId((object) hashtable);
      if ((long) internalId == 0L)
        return false;
      if (!this._datas.ContainsKey(internalId))
        return this.Add(orgData, updateTime);
      if (!this._datas[internalId].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated((long) internalId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
      this.Publish_onDataSetChanged();
    }

    public bool Remove(int contactUid, long updateTime)
    {
      if (!this._datas.ContainsKey(contactUid) || !this._datas[contactUid].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(contactUid);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(AchievementData.GetInternalId(arrayList[index]), updateTime);
      this.Publish_onDataRemoved();
      this.Publish_onDataSetChanged();
    }

    public int GetCompleteCount()
    {
      int num = 0;
      Dictionary<int, AchievementData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.IsFinish)
          ++num;
      }
      return num;
    }

    public List<AchievementData> GetTotalDatas()
    {
      this.totals.Clear();
      Dictionary<int, AchievementData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
        this.totals.Add(enumerator.Current);
      this.totals.Sort(new Comparison<AchievementData>(this.OnComparse));
      return this.totals;
    }

    private int OnComparse(AchievementData a, AchievementData b)
    {
      bool flag1 = a.IsFinish && !a.IsClaim;
      bool flag2 = b.IsFinish && !b.IsClaim;
      if (flag1 && !flag2)
        return -1;
      if (!flag1 && flag2)
        return 1;
      return a.AchievementId.CompareTo(b.AchievementId);
    }
  }
}
