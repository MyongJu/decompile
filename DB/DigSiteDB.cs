﻿// Decompiled with JetBrains decompiler
// Type: DB.DigSiteDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DigSiteDB
  {
    private Dictionary<long, DigSiteData> _datas = new Dictionary<long, DigSiteData>();

    public event System.Action<DigSiteData> onDataChanged;

    public event System.Action<DigSiteData> onDataUpdate;

    public event System.Action<DigSiteData> onDataCreate;

    public event System.Action<DigSiteData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (DigSiteData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(DigSiteData digSite)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(digSite);
    }

    private void PublishOnDataCreateEvent(DigSiteData digSite)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(digSite);
      this.PublishOnDataChangedEvent(digSite);
    }

    private void PublishOnDataUpdateEvent(DigSiteData digSite)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(digSite);
      this.PublishOnDataChangedEvent(digSite);
    }

    private void PublishOnDataRemovedEvent(DigSiteData digSite)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(digSite);
      this.PublishOnDataChangedEvent(digSite);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      DigSiteData digSite = new DigSiteData();
      if (!digSite.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(digSite.SiteId))
        return false;
      this._datas.Add(digSite.SiteId, digSite);
      this.PublishOnDataCreateEvent(digSite);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "site_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      DigSiteData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long siteId, long updateTime)
    {
      if (!this._datas.ContainsKey(siteId))
        return false;
      DigSiteData data = this._datas[siteId];
      this._datas.Remove(siteId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "site_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public DigSiteData Get(long siteId)
    {
      DigSiteData digSiteData;
      this._datas.TryGetValue(siteId, out digSiteData);
      return digSiteData;
    }
  }
}
