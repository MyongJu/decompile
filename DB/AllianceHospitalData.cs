﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceHospitalData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceHospitalData : BaseData
  {
    private string _stateName = "invalid";
    private Dictionary<long, long> _troops = new Dictionary<long, long>();
    private List<long> _troopsSort = new List<long>();
    private Dictionary<long, Dictionary<string, AllianceHospitalHealingData>> _injuredTroops = new Dictionary<long, Dictionary<string, AllianceHospitalHealingData>>();
    private Dictionary<long, long> _jobs = new Dictionary<long, long>();
    private long _allianceId;
    private long _buildingId;
    private long _ownerUid;
    private Coordinate _location;
    private long _durability;
    private long _timer_end;
    private int _clear_away;
    private long _utime;
    private string _name;
    private AllianceHospitalData.State _state;
    private int _configid;

    public long AllianceID
    {
      get
      {
        return this._allianceId;
      }
    }

    public long BuildingID
    {
      get
      {
        return this._buildingId;
      }
    }

    public long OwnerUid
    {
      get
      {
        return this._ownerUid;
      }
    }

    public Coordinate Location
    {
      get
      {
        return this._location;
      }
    }

    public long Durability
    {
      get
      {
        return this._durability;
      }
    }

    public long TimerEnd
    {
      get
      {
        return this._timer_end;
      }
    }

    public int ClearAway
    {
      get
      {
        return this._clear_away;
      }
    }

    public long Utime
    {
      get
      {
        return this._utime;
      }
    }

    public string Name
    {
      get
      {
        return this._name;
      }
    }

    public AllianceHospitalData.State CurrentState
    {
      get
      {
        return this._state;
      }
    }

    public string CurrentStateName
    {
      get
      {
        return this._stateName;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configid;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this._troops;
      }
    }

    public List<long> TroopsSort
    {
      get
      {
        return this._troopsSort;
      }
    }

    public long MaxTroopsCount { get; set; }

    public long CurrentTroopsCount
    {
      get
      {
        long num = 0;
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
          if (marchData != null)
            num += (long) marchData.troopsInfo.totalCount;
        }
        return num;
      }
    }

    public long MyTroopId
    {
      get
      {
        Dictionary<long, long>.KeyCollection.Enumerator enumerator = this.Troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
            return this.Troops[enumerator.Current];
        }
        return 0;
      }
    }

    public Dictionary<long, Dictionary<string, AllianceHospitalHealingData>> InjuredTroops
    {
      get
      {
        return this._injuredTroops;
      }
    }

    public Dictionary<long, long> Jobs
    {
      get
      {
        return this._jobs;
      }
    }

    public long MyJobId
    {
      get
      {
        long num = 0;
        if (this._jobs.TryGetValue(PlayerData.inst.uid, out num))
          return num;
        return 0;
      }
    }

    public long GetInjuredTroopCount()
    {
      long num = 0;
      Dictionary<long, Dictionary<string, AllianceHospitalHealingData>>.Enumerator enumerator1 = this._injuredTroops.GetEnumerator();
      while (enumerator1.MoveNext())
      {
        Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator2 = enumerator1.Current.Value.GetEnumerator();
        while (enumerator2.MoveNext())
          num += enumerator2.Current.Value.total;
      }
      return num;
    }

    public long GetInjuredMemberCount()
    {
      return (long) this._injuredTroops.Count;
    }

    public Dictionary<string, AllianceHospitalHealingData> GetMyHealingData()
    {
      return this.GetHealingData(PlayerData.inst.uid);
    }

    public Dictionary<string, AllianceHospitalHealingData> GetHealingData(long uid)
    {
      Dictionary<string, AllianceHospitalHealingData> dictionary;
      this._injuredTroops.TryGetValue(uid, out dictionary);
      return dictionary;
    }

    public long GetMyInjuredTroopCount()
    {
      long num = 0;
      Dictionary<string, AllianceHospitalHealingData> myHealingData = this.GetMyHealingData();
      if (myHealingData != null)
      {
        using (Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = myHealingData.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, AllianceHospitalHealingData> current = enumerator.Current;
            num += current.Value.total;
          }
        }
      }
      return num;
    }

    public long GetMyHealingTroopCount()
    {
      long num = 0;
      Dictionary<string, AllianceHospitalHealingData> myHealingData = this.GetMyHealingData();
      if (myHealingData != null)
      {
        using (Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = myHealingData.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, AllianceHospitalHealingData> current = enumerator.Current;
            num += current.Value.healing;
          }
        }
      }
      return num;
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "timer_end", ref this._timer_end) | DatabaseTools.UpdateData(inData, "utime", ref this._utime) | DatabaseTools.UpdateData(inData, "clear_away", ref this._clear_away) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "building_id", ref this._buildingId) | DatabaseTools.UpdateData(inData, "owner_uid", ref this._ownerUid) | DatabaseTools.UpdateData(inData, "durability", ref this._durability) | DatabaseTools.UpdateData(inData, "name", ref this._name) | DatabaseTools.UpdateData(inData, "config_id", ref this._configid) | this._location.Decode(inData[(object) "world_id"], inData[(object) "map_x"], inData[(object) "map_y"]) | DatabaseTools.UpdateData(inData, "state", ref this._stateName);
      string stateName = this._stateName;
      if (stateName != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceHospitalData.\u003C\u003Ef__switch\u0024map66 == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceHospitalData.\u003C\u003Ef__switch\u0024map66 = new Dictionary<string, int>(4)
          {
            {
              "building",
              0
            },
            {
              "complete",
              1
            },
            {
              "freeze",
              2
            },
            {
              "unComplete",
              3
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceHospitalData.\u003C\u003Ef__switch\u0024map66.TryGetValue(stateName, out num))
        {
          switch (num)
          {
            case 0:
              this._state = AllianceHospitalData.State.BUILDING;
              break;
            case 1:
              this._state = AllianceHospitalData.State.COMPLETE;
              break;
            case 2:
              this._state = AllianceHospitalData.State.FREEZE;
              break;
            case 3:
              this._state = AllianceHospitalData.State.UNCOMPLETE;
              break;
          }
        }
      }
      if (inData.ContainsKey((object) "wounded_troops"))
      {
        this._injuredTroops = new Dictionary<long, Dictionary<string, AllianceHospitalHealingData>>();
        Hashtable hashtable1 = inData[(object) "wounded_troops"] as Hashtable;
        if (hashtable1 != null)
        {
          IDictionaryEnumerator enumerator1 = hashtable1.GetEnumerator();
          while (enumerator1.MoveNext())
          {
            long key1 = long.Parse(enumerator1.Key.ToString());
            Hashtable hashtable2 = enumerator1.Value as Hashtable;
            if (hashtable2 != null)
            {
              Dictionary<string, AllianceHospitalHealingData> dictionary = new Dictionary<string, AllianceHospitalHealingData>();
              this._injuredTroops.Add(key1, dictionary);
              IDictionaryEnumerator enumerator2 = hashtable2.GetEnumerator();
              while (enumerator2.MoveNext())
              {
                string key2 = enumerator2.Key.ToString();
                AllianceHospitalHealingData hospitalHealingData = new AllianceHospitalHealingData();
                hospitalHealingData.Decode(enumerator2.Value);
                dictionary.Add(key2, hospitalHealingData);
              }
            }
          }
        }
        flag1 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      bool flag2;
      if (inData.ContainsKey((object) "troops"))
      {
        this._troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this._troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag2 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "build_job_id"))
      {
        this._jobs = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "build_job_id"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
            this._jobs.Add(long.Parse(enumerator.Key.ToString()), long.Parse(enumerator.Value.ToString()));
        }
      }
      if (inData.Contains((object) "troops_sort"))
      {
        flag2 = true;
        this._troopsSort.Clear();
        ArrayList arrayList = inData[(object) "troops_sort"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            long result = 0;
            if (long.TryParse(arrayList[index].ToString(), out result))
              this._troopsSort.Add(result);
          }
        }
      }
      return true;
    }

    public struct Params
    {
      public const string KEY = "alliance_hospital";
      public const string ALLIANCE_ID = "alliance_id";
      public const string BUILDING_ID = "building_id";
      public const string OWNER_UID = "owner_uid";
      public const string WORLD_ID = "world_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string DURABILITY = "durability";
      public const string NAME = "name";
      public const string STATE = "state";
      public const string CONFIG_ID = "config_id";
      public const string TROOPS = "troops";
      public const string WOUNDED_TROOPS = "wounded_troops";
      public const string BUILD_JOB_ID = "build_job_id";
      public const string STATE_INVALID = "invalid";
      public const string STATE_BUILDING = "building";
      public const string STATE_UNCOMPLETE = "unComplete";
      public const string STATE_COMPLETE = "complete";
      public const string STATE_FREEZE = "freeze";
      public const string TIMER_END = "timer_end";
      public const string UTIME = "utime";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
      public const string TROOPS_SORT = "troops_sort";
      public const string CLEAR_AWAY = "clear_away";
    }

    public enum State
    {
      UNCOMPLETE = 1,
      BUILDING = 2,
      COMPLETE = 3,
      FREEZE = 4,
    }
  }
}
