﻿// Decompiled with JetBrains decompiler
// Type: DB.CityHealingTroopsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public struct CityHealingTroopsInfo
  {
    public Dictionary<string, CityHealingTroopInfo> healingTroops;
    private long _totalTroopsCount;
    private long _healingTroopsCount;

    public long totalTroopsCount
    {
      get
      {
        return this._totalTroopsCount;
      }
    }

    public long healingTroopsCount
    {
      get
      {
        return this._healingTroopsCount;
      }
    }

    public void Decode(object orgData)
    {
      if (this.healingTroops == null)
        this.healingTroops = new Dictionary<string, CityHealingTroopInfo>();
      this.healingTroops.Clear();
      this._totalTroopsCount = 0L;
      this._healingTroopsCount = 0L;
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return;
      IDictionaryEnumerator enumerator = data.GetEnumerator();
      while (enumerator.MoveNext())
      {
        CityHealingTroopInfo healingTroopInfo = new CityHealingTroopInfo();
        healingTroopInfo.Decode(enumerator.Value);
        this.healingTroops.Add(enumerator.Key.ToString(), healingTroopInfo);
        this._totalTroopsCount += healingTroopInfo.totalCount;
        this._healingTroopsCount += healingTroopInfo.healingCount;
      }
    }

    public long GetTotalCount(string troopsId)
    {
      if (this.healingTroops.ContainsKey(troopsId))
        return this.healingTroops[troopsId].totalCount;
      return 0;
    }

    public long GetHealingCount(string troopsId)
    {
      if (this.healingTroops.ContainsKey(troopsId))
        return this.healingTroops[troopsId].healingCount;
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string empty = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        empty += "\t";
      string str = string.Empty;
      if (this.healingTroops != null)
      {
        using (Dictionary<string, CityHealingTroopInfo>.KeyCollection.Enumerator enumerator = this.healingTroops.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            str = str + empty + current + " : " + (object) this.healingTroops[current].totalCount + ":" + (object) this.healingTroops[current].healingCount + "\n";
          }
        }
      }
      return str;
    }
  }
}
