﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightAssignedSkill
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightAssignedSkill
  {
    public Dictionary<int, int> talentSkills = new Dictionary<int, int>();

    public void Decode(object orgData)
    {
      this.talentSkills.Clear();
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return;
      IEnumerator enumerator = data.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result1 = 0;
        int.TryParse(enumerator.Current.ToString(), out result1);
        int result2 = 0;
        int.TryParse(data[(object) enumerator.Current.ToString()].ToString(), out result2);
        if (!this.talentSkills.ContainsKey(result1))
          this.talentSkills.Add(result1, result2);
      }
    }

    public int GetTalentSkill(int slotIndex)
    {
      if (this.talentSkills.ContainsKey(slotIndex))
        return this.talentSkills[slotIndex];
      return 0;
    }
  }
}
