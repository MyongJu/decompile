﻿// Decompiled with JetBrains decompiler
// Type: DB.CityMapKey
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct CityMapKey
  {
    public long uid;
    public long cityId;
    public long buildingId;

    public void Decode(object orgData)
    {
      this.Clear();
      if (orgData == null)
        return;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      DatabaseTools.UpdateData(inData, "uid", ref this.uid);
      DatabaseTools.UpdateData(inData, "city_id", ref this.cityId);
      DatabaseTools.UpdateData(inData, "building_id", ref this.buildingId);
    }

    public void Clear()
    {
      this.uid = 0L;
      this.cityId = -1L;
      this.buildingId = 0L;
    }

    public bool isInvalid
    {
      get
      {
        if (this.uid != 0L && this.uid == PlayerData.inst.uid && this.cityId != -1L)
          return this.buildingId == 0L;
        return true;
      }
    }

    public override bool Equals(object obj)
    {
      CityMapKey cityMapKey = (CityMapKey) obj;
      if (cityMapKey.uid == this.uid && cityMapKey.cityId == this.cityId)
        return cityMapKey.buildingId == this.buildingId;
      return false;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public string ShowInfo()
    {
      return string.Format("uid : {0}; cityId: {1}; buildingId: {2}", (object) this.uid, (object) this.cityId, (object) this.buildingId);
    }

    public struct Params
    {
      public const string CITY_MAP_KEY_UID = "uid";
      public const string CITY_MAP_KEY_CITY_ID = "city_id";
      public const string CITY_MAP_KEY_BUILDING_ID = "building_id";
    }
  }
}
