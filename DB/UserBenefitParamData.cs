﻿// Decompiled with JetBrains decompiler
// Type: DB.UserBenefitParamData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class UserBenefitParamData : BaseData
  {
    private long _uid;
    private int _kingdomTrainTotal;
    private int _kingdomTrainLeft;

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public int KingdomTrainTotal
    {
      get
      {
        return this._kingdomTrainTotal;
      }
    }

    public int KingdomTrainLeft
    {
      get
      {
        return this._kingdomTrainLeft;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "uid", ref this._uid) | DatabaseTools.UpdateData(dataHt, "kingdom_train_total", ref this._kingdomTrainTotal) | DatabaseTools.UpdateData(dataHt, "kingdom_train_left", ref this._kingdomTrainLeft);
    }

    public struct Params
    {
      public const string KEY = "user_benefit_param";
      public const string UID = "uid";
      public const string KINGDOM_TRAIN_TOTAL = "kingdom_train_total";
      public const string KINGDOM_TRAIN_LEFT = "kingdom_train_left";
    }
  }
}
