﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightActiveTalent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightActiveTalent
  {
    public Dictionary<long, DragonKnightActiveTalent.Talent> talents = new Dictionary<long, DragonKnightActiveTalent.Talent>();

    public void Decode(object orgData)
    {
      this.talents.Clear();
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return;
      IEnumerator enumerator = data.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long key = long.Parse(enumerator.Current.ToString());
        DragonKnightActiveTalent.Talent talent = new DragonKnightActiveTalent.Talent();
        talent.Decode(data[enumerator.Current]);
        this.talents.Add(key, talent);
      }
    }

    public DragonKnightActiveTalent.Talent GetTalent(long talentId)
    {
      if (this.talents.ContainsKey(talentId))
        return this.talents[talentId];
      return (DragonKnightActiveTalent.Talent) null;
    }

    public class Talent
    {
      protected int _activeTime;
      protected long _jobId;

      public int ActiveTime
      {
        get
        {
          return this._activeTime;
        }
      }

      public long JobId
      {
        get
        {
          return this._jobId;
        }
      }

      public bool Decode(object orgData)
      {
        Hashtable data;
        if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
          return false;
        return false | DatabaseTools.UpdateData(data, "active_time", ref this._activeTime) | DatabaseTools.UpdateData(data, "job_id", ref this._jobId);
      }

      public struct Params
      {
        public const string ACTIVE_TIME = "active_time";
        public const string JOB_ID = "job_id";
      }
    }
  }
}
