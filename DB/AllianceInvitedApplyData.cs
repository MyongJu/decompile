﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceInvitedApplyData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceInvitedApplyData : BaseData
  {
    private string _comment = string.Empty;
    private AllianceInviteApplyKey _key;
    private int _status;

    public long uid
    {
      get
      {
        return this.key.uid;
      }
    }

    public long allianceId
    {
      get
      {
        return this.key.allianceId;
      }
    }

    public AllianceInviteApplyKey key
    {
      get
      {
        return this._key;
      }
    }

    public AllianceInvitedApplyData.Status status
    {
      get
      {
        return this._status == 1 ? AllianceInvitedApplyData.Status.asking : AllianceInvitedApplyData.Status.invited;
      }
    }

    public string comment
    {
      get
      {
        return this._comment;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | this._key.Decode((object) dataHt) | DatabaseTools.UpdateData(dataHt, "status", ref this._status) | DatabaseTools.UpdateData(dataHt, "comment", ref this._comment);
    }

    public struct Param
    {
      public const string KEY = "alliance_invite_apply";
      public const string ALLIANCE_ID = "alliance_id";
      public const string UID = "uid";
      public const string STATUS = "status";
      public const string COMMENT = "comment";
    }

    public enum Status
    {
      invited,
      asking,
    }
  }
}
