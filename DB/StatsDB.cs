﻿// Decompiled with JetBrains decompiler
// Type: DB.StatsDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class StatsDB
  {
    private Dictionary<string, StatsData> _datas = new Dictionary<string, StatsData>();
    public const string LUA_TABLE_NAME = "Stats";
    public System.Action<string> onDataChanged;
    public System.Action<string> onDataCreated;
    public System.Action<string> onDataUpdated;
    public System.Action<string> onDataRemove;
    public System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (StatsData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(string statsName)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(statsName);
    }

    public void Publish_onDataCreated(string statsName)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(statsName);
      this.Publish_onDataChanged(statsName);
    }

    public void Publish_onDataUpdated(string statsName)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(statsName);
      this.Publish_onDataChanged(statsName);
    }

    public void Publish_onDataRemove(string statsName)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(statsName);
      this.Publish_onDataChanged(statsName);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      StatsData statsData = new StatsData();
      if (!statsData.Decode((object) hashtable, updateTime) || string.IsNullOrEmpty(statsData.statsName) || this._datas.ContainsKey(statsData.statsName))
        return false;
      this._datas.Add(statsData.statsName, statsData);
      this.Publish_onDataCreated(statsData.statsName);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      string statasName = StatsData.GetStatasName((object) hashtable);
      if (string.IsNullOrEmpty(statasName) || StatsData.GetStatsUid(orgData) != PlayerData.inst.uid)
        return false;
      if (!this._datas.ContainsKey(statasName))
        return this.Add(orgData, updateTime);
      if (!this._datas[statasName].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated(statasName);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public StatsData Get(string key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (StatsData) null;
    }

    public bool Remove(string statsName, long updateTime)
    {
      if (!this._datas.ContainsKey(statsName) || !this._datas[statsName].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(statsName);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove((arrayList[index] as Hashtable)[(object) "name"].ToString(), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<string, StatsData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          StatsData current = enumerator.Current;
        }
      }
    }
  }
}
