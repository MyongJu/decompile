﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceMagicData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceMagicData : BaseData
  {
    protected List<long> _allTargetUser = new List<long>();
    protected List<long> _allTargetAlliance = new List<long>();
    protected long world_id;
    protected long alliance_id;
    protected long magic_id;
    protected long uid;
    protected int power;
    protected string state;
    protected int donation;
    protected long timer_end;
    protected int config_id;
    protected long ctime;
    protected long mtime;
    public bool Interrupt;
    public bool Trigger;

    public long WorldId
    {
      get
      {
        return this.world_id;
      }
    }

    public long AllianceId
    {
      get
      {
        return this.alliance_id;
      }
    }

    public long MagicId
    {
      get
      {
        return this.magic_id;
      }
    }

    public long Uid
    {
      get
      {
        return this.uid;
      }
    }

    public int Power
    {
      get
      {
        return this.power;
      }
    }

    public string CurrentState
    {
      get
      {
        return this.state;
      }
    }

    public int Donation
    {
      get
      {
        return this.donation;
      }
    }

    public long TimerEnd
    {
      get
      {
        return this.timer_end;
      }
    }

    public int ConfigId
    {
      get
      {
        return this.config_id;
      }
    }

    public long Ctime
    {
      get
      {
        return this.ctime;
      }
    }

    public long Mtime
    {
      get
      {
        return this.mtime;
      }
    }

    public List<long> allTargetUser
    {
      get
      {
        return this._allTargetUser;
      }
    }

    public List<long> allTargetAlliance
    {
      get
      {
        return this._allTargetAlliance;
      }
    }

    public virtual bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id) | DatabaseTools.UpdateData(inData, "magic_id", ref this.magic_id) | DatabaseTools.UpdateData(inData, "uid", ref this.uid) | DatabaseTools.UpdateData(inData, "power", ref this.power) | DatabaseTools.UpdateData(inData, "donation", ref this.donation) | DatabaseTools.UpdateData(inData, "timer_end", ref this.timer_end) | DatabaseTools.UpdateData(inData, "config_id", ref this.config_id) | DatabaseTools.UpdateData(inData, "ctime", ref this.ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this.mtime);
      string state = this.state;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "state", ref this.state);
      this.Interrupt = state == "charging" && this.state == "interrupt";
      this.Trigger = state == "charging" && this.state == "releasing";
      if (inData.ContainsKey((object) "target"))
      {
        Hashtable data = inData[(object) "target"] as Hashtable;
        this.TryGetLongListFromHashtable(data, "uid", ref this._allTargetUser);
        this.TryGetLongListFromHashtable(data, "alliance_id", ref this._allTargetAlliance);
        flag2 = ((flag2 ? 1 : 0) | 1) != 0;
      }
      return flag2;
    }

    protected void TryGetLongListFromHashtable(Hashtable data, string key, ref List<long> result)
    {
      result.Clear();
      if (data == null || !data.ContainsKey((object) key))
        return;
      ArrayList arrayList = data[(object) key] as ArrayList;
      if (arrayList == null)
        return;
      long result1 = 0;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (long.TryParse(arrayList[index].ToString(), out result1))
          result.Add(result1);
      }
    }

    public struct Params
    {
      public const string KEY = "magic";
      public const string WORLD_ID = "world_id";
      public const string ALLIANCE_ID = "alliance_id";
      public const string MAGIC_ID = "magic_id";
      public const string UID = "uid";
      public const string OPS = "ops";
      public const string POWER = "power";
      public const string STATE = "state";
      public const string DONATION = "donation";
      public const string TIMER_END = "timer_end";
      public const string CONFIG_ID = "config_id";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
      public const string TARGET = "target";
      public const string TARGET_USER_ID = "uid";
      public const string TARGET_ALLIANCE_ID = "alliance_id";
    }

    public struct State
    {
      public const string RELEASING = "releasing";
      public const string CHARGING = "charging";
      public const string INTERRUPT = "interrupt";
    }
  }
}
