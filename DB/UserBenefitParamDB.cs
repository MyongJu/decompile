﻿// Decompiled with JetBrains decompiler
// Type: DB.UserBenefitParamDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserBenefitParamDB
  {
    private Dictionary<long, UserBenefitParamData> _datas = new Dictionary<long, UserBenefitParamData>();
    private bool _dataDirty;

    public event System.Action<UserBenefitParamData> onDataChanged;

    public event System.Action<UserBenefitParamData> onDataUpdate;

    public event System.Action<UserBenefitParamData> onDataCreate;

    public event System.Action<UserBenefitParamData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (UserBenefitParamData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(UserBenefitParamData userBenefitParamData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(userBenefitParamData);
    }

    private void PublishOnDataCreateEvent(UserBenefitParamData userBenefitParamData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(userBenefitParamData);
      this.PublishOnDataChangedEvent(userBenefitParamData);
    }

    private void PublishOnDataUpdateEvent(UserBenefitParamData userBenefitParamData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(userBenefitParamData);
      this.PublishOnDataChangedEvent(userBenefitParamData);
    }

    private void PublishOnDataRemovedEvent(UserBenefitParamData userBenefitParamData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(userBenefitParamData);
      this.PublishOnDataChangedEvent(userBenefitParamData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      UserBenefitParamData userBenefitParamData = new UserBenefitParamData();
      if (!userBenefitParamData.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(userBenefitParamData.Uid))
        return false;
      this._datas.Add(userBenefitParamData.Uid, userBenefitParamData);
      this.PublishOnDataCreateEvent(userBenefitParamData);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      UserBenefitParamData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long uid, long updateTime)
    {
      if (!this._datas.ContainsKey(uid))
        return false;
      UserBenefitParamData data = this._datas[uid];
      this._datas.Remove(uid);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "uid", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public UserBenefitParamData Get(long uid)
    {
      UserBenefitParamData benefitParamData;
      this._datas.TryGetValue(uid, out benefitParamData);
      return benefitParamData;
    }

    public void MarkDataDirty()
    {
      this._dataDirty = true;
    }

    public void TryUpdateFromServer(System.Action<bool, object> callback = null)
    {
      if (!this._dataDirty)
        return;
      this._dataDirty = false;
      RequestManager.inst.SendRequest("king:getTrainTroopBuffDetail", (Hashtable) null, (System.Action<bool, object>) ((result, data) =>
      {
        if (callback == null)
          return;
        callback(result, data);
      }), true);
    }
  }
}
