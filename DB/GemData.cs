﻿// Decompiled with JetBrains decompiler
// Type: DB.GemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using GemConstant;
using System;
using System.Collections;

namespace DB
{
  public class GemData : BaseData
  {
    public const int INVALID_ID = -1;
    private long _uid;
    private long _id;
    private int _configId;
    private int _inlaid;
    private long _exp;
    private int _level;
    private int _quantity;
    private int _version;

    public long UID
    {
      get
      {
        return this._uid;
      }
    }

    public long ID
    {
      get
      {
        return this._id;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public SlotType SlotType { get; set; }

    public GemType GemType { get; set; }

    public bool Inlaid
    {
      get
      {
        return this._inlaid != 0;
      }
    }

    public long Exp
    {
      get
      {
        return this._exp;
      }
    }

    public int Level
    {
      get
      {
        return this._level;
      }
    }

    public int Quantity
    {
      get
      {
        return this._quantity;
      }
    }

    public int Version
    {
      get
      {
        return this._version;
      }
    }

    public bool Decode(Hashtable ht, long updateTime)
    {
      if (ht == null || !this.CheckAndResetUpdateTime(updateTime))
        return false;
      bool flag = false | DatabaseTools.UpdateData(ht, "uid", ref this._uid) | DatabaseTools.UpdateData(ht, "id", ref this._id) | DatabaseTools.UpdateData(ht, "config_id", ref this._configId) | DatabaseTools.UpdateData(ht, "inlaid", ref this._inlaid) | DatabaseTools.UpdateData(ht, "exp", ref this._exp) | DatabaseTools.UpdateData(ht, "quantity", ref this._quantity) | DatabaseTools.UpdateData(ht, "version", ref this._version) | DatabaseTools.UpdateData(ht, "level", ref this._level);
      string empty = string.Empty;
      DatabaseTools.UpdateData(ht, "slot_type", ref empty);
      if (Enum.IsDefined(typeof (SlotType), (object) empty))
      {
        this.SlotType = (SlotType) Enum.Parse(typeof (SlotType), empty);
        flag = true;
      }
      int outData = -1;
      DatabaseTools.UpdateData(ht, "gem_type", ref outData);
      if (Enum.IsDefined(typeof (GemType), (object) outData))
      {
        this.GemType = (GemType) outData;
        flag = true;
      }
      return flag;
    }

    public static long GetGemId(Hashtable ht)
    {
      if (ht == null || !ht.ContainsKey((object) "id"))
        return -1;
      return long.Parse(ht[(object) "id"].ToString());
    }

    public struct Params
    {
      public const string KEY = "user_gem";
      public const string UID = "uid";
      public const string ID = "id";
      public const string CONFIG_ID = "config_id";
      public const string SLOT_TYPE = "slot_type";
      public const string GEM_TYPE = "gem_type";
      public const string INLAID = "inlaid";
      public const string EXP = "exp";
      public const string LEVEL = "level";
      public const string QUANTITY = "quantity";
      public const string VERSION = "version";
    }
  }
}
