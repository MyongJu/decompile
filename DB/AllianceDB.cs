﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceDB
  {
    private SortedDictionary<long, AllianceData> _datas = new SortedDictionary<long, AllianceData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataCreate;

    public event System.Action<long> onDataRemoved;

    public event System.Action onHelpRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceData), (long) this._datas.Count);
    }

    public void Publish_OnHelpRemoved()
    {
      if (this.onHelpRemoved == null)
        return;
      this.onHelpRemoved();
    }

    public void Publish_OnDataChanged(long allianceId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(allianceId);
    }

    public void Publish_OnDataCreate(long allianceId)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(allianceId);
      this.Publish_OnDataChanged(allianceId);
    }

    public void Publish_OnDataUpdate(long allianceId)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(allianceId);
      this.Publish_OnDataChanged(allianceId);
    }

    public void Publish_OnDataRemoved(long allianceId)
    {
      this.Publish_OnDataChanged(allianceId);
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(allianceId);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceData allianceData = new AllianceData();
      if (!allianceData.Decode((object) hashtable, updateTime) || allianceData.allianceId == 0L || this._datas.ContainsKey(allianceData.allianceId))
        return false;
      this._datas.Add(allianceData.allianceId, allianceData);
      this.Publish_OnDataCreate(allianceData.allianceId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = this.CheckKeyFromData((object) hashtable);
      if (index == 0L)
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(id);
      this.Publish_OnDataRemoved(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse((arrayList[index] as Hashtable)[(object) "alliance_id"].ToString()), updateTime);
    }

    public AllianceData Get(long id)
    {
      if (id == 0L)
        return (AllianceData) null;
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (AllianceData) null;
    }

    public void ShowInfo()
    {
      string str = "<color=blue>Alliance Database Info </color>\n" + "Alliance count : " + (object) this._datas.Count + "\n";
      using (SortedDictionary<long, AllianceData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str = str + "  Alliance : " + (object) current + " : " + this._datas[current].allianceFullName;
        }
      }
      using (SortedDictionary<long, AllianceData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
        }
      }
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return 0;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      long result = 0;
      if (long.TryParse(hashtable[(object) "alliance_id"].ToString(), out result))
        return result;
      return 0;
    }

    public SortedDictionary<long, AllianceData>.ValueCollection Values
    {
      get
      {
        return this._datas.Values;
      }
    }

    public void RemoveRosterByUid(long uid, int status)
    {
      using (SortedDictionary<long, AllianceData>.ValueCollection.Enumerator enumerator1 = this.Values.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          AllianceData current1 = enumerator1.Current;
          using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator2 = current1.members.datas.Values.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              AllianceMemberData current2 = enumerator2.Current;
              if (current2.uid == uid && current2.status == status)
              {
                current1.members.datas.Remove(uid);
                break;
              }
            }
          }
        }
      }
    }

    public void Clear()
    {
      this._datas.Clear();
    }
  }
}
