﻿// Decompiled with JetBrains decompiler
// Type: DB.Currency
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct Currency
  {
    public long gold;
    public long honor;
    public long essence;
    public long steel;

    public bool Decode(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "gold", ref this.gold) | DatabaseTools.UpdateData(inData, "honor", ref this.honor) | DatabaseTools.UpdateData(inData, "essence", ref this.essence) | DatabaseTools.UpdateData(inData, "steel", ref this.steel);
    }

    public struct Params
    {
      public const string CURRENCY_GOLD = "gold";
      public const string CURRENCY_HONOR = "honor";
      public const string CURRENCY_ESSENCE = "essence";
      public const string CURRENCY_STEEL = "steel";
    }
  }
}
