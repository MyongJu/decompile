﻿// Decompiled with JetBrains decompiler
// Type: DB.WonderTowerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class WonderTowerData : BaseData
  {
    public const long INVALID_ID = 0;
    private long _stateChangeTime;
    protected long tower_id;
    protected long owner_uid;
    protected long owner_alliance_id;
    protected int world_id;
    protected long protected_end_time;
    protected long skill_job_id;
    protected long mtime;
    protected Dictionary<long, long> troops;
    protected string state;

    public long StateChangeTime
    {
      get
      {
        return this._stateChangeTime;
      }
    }

    public long TOWER_ID
    {
      get
      {
        return this.tower_id;
      }
    }

    public string Name
    {
      get
      {
        if (this.tower_id >= 1L && this.tower_id <= 4L)
          return Utils.XLAT("throne_miniwonder_" + (object) this.tower_id + "_name");
        return string.Empty;
      }
    }

    public string TowerPrefabName
    {
      get
      {
        switch (this.tower_id)
        {
          case 1:
            return "tower_northern01_01";
          case 2:
            return "tower_northern01_02";
          case 3:
            return "tower_northern02_01";
          case 4:
            return "tower_northern02_02";
          default:
            return "tower_northern01_01";
        }
      }
    }

    public long OWNER_UID
    {
      get
      {
        return this.owner_uid;
      }
    }

    public long MaxTroopsCount { get; set; }

    public long OWNER_ALLIANCE_ID
    {
      get
      {
        return this.owner_alliance_id;
      }
    }

    public long CurrentTroopsCount
    {
      get
      {
        long num = 0;
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
          if (marchData != null)
            num += (long) marchData.troopsInfo.totalCount;
        }
        return num;
      }
    }

    public long MyTroopId
    {
      get
      {
        Dictionary<long, long>.KeyCollection.Enumerator enumerator = this.Troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
            return this.Troops[enumerator.Current];
        }
        return 0;
      }
    }

    public int WORLD_ID
    {
      get
      {
        return this.world_id;
      }
    }

    public long PROTECTED_END_TIME
    {
      get
      {
        return this.protected_end_time;
      }
    }

    public long SKILL_JOB_ID
    {
      get
      {
        return this.skill_job_id;
      }
    }

    public long ModifyTime
    {
      get
      {
        return this.mtime;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this.troops;
      }
    }

    public string State
    {
      get
      {
        return this.state;
      }
    }

    public bool CanShowPeaceShield
    {
      get
      {
        string state = this.state;
        if (state != null)
        {
          if (WonderTowerData.\u003C\u003Ef__switch\u0024map70 == null)
            WonderTowerData.\u003C\u003Ef__switch\u0024map70 = new Dictionary<string, int>(2)
            {
              {
                "protected",
                0
              },
              {
                "unOpen",
                0
              }
            };
          int num;
          if (WonderTowerData.\u003C\u003Ef__switch\u0024map70.TryGetValue(state, out num) && num == 0)
            return true;
        }
        return false;
      }
    }

    public bool HasMarch(long marchId)
    {
      if (this.troops != null)
      {
        Dictionary<long, long>.Enumerator enumerator = this.troops.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value == marchId)
            return true;
        }
      }
      return false;
    }

    public virtual bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "tower_id", ref this.tower_id) | DatabaseTools.UpdateData(inData, "owner_uid", ref this.owner_uid) | DatabaseTools.UpdateData(inData, "owner_alliance_id", ref this.owner_alliance_id) | DatabaseTools.UpdateData(inData, "state", ref this.state) | DatabaseTools.UpdateData(inData, "protected_end_time", ref this.protected_end_time) | DatabaseTools.UpdateData(inData, "skill_job_id", ref this.skill_job_id) | DatabaseTools.UpdateData(inData, "mtime", ref this.mtime) | DatabaseTools.UpdateData(inData, "state_change_time", ref this._stateChangeTime);
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public struct Params
    {
      public const string KEY = "wonder_tower";
      public const string WORLD_ID = "world_id";
      public const string TOWER_ID = "tower_id";
      public const string OWNER_UID = "owner_uid";
      public const string OWNER_ALLIANCE_ID = "owner_alliance_id";
      public const string TROOPS = "troops";
      public const string STATE = "state";
      public const string PROTECTED_END_TIME = "protected_end_time";
      public const string STATE_CHANGE_TIME = "state_change_time";
      public const string SKILL_JOB_ID = "skill_job_id";
      public const string MTIME = "mtime";
    }

    public struct WonderTowerState
    {
      public const string UNOPEN = "unOpen";
      public const string FIGHTING = "fighting";
      public const string PROTECTED = "protected";
    }
  }
}
