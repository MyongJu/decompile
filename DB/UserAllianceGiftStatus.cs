﻿// Decompiled with JetBrains decompiler
// Type: DB.UserAllianceGiftStatus
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace DB
{
  public static class UserAllianceGiftStatus
  {
    public const int STATUS_UNOPENED = 1;
    public const int STATUS_OPENED = 2;
    public const int STATUS_EXPIRED = 3;
    public const int STATUS_FINISHED = 4;
  }
}
