﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceDiplomacyContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceDiplomacyContainer
  {
    public Dictionary<long, AllianceDiplomacyData> datas = new Dictionary<long, AllianceDiplomacyData>();

    public AllianceDiplomacyData Get(long allianceId)
    {
      AllianceDiplomacyData allianceDiplomacyData = (AllianceDiplomacyData) null;
      this.datas.TryGetValue(allianceId, out allianceDiplomacyData);
      return allianceDiplomacyData;
    }

    public static void UpdateDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        long allianceId = AllianceDiplomacyData.GetAllianceID(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
        if (allianceData != null)
        {
          allianceData.diplomacy.Update(arrayList[index], updateTime);
          DBManager.inst.DB_Alliance.Publish_OnDataUpdate(allianceId);
        }
      }
    }

    public static void RemoveDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        long allianceId = AllianceDiplomacyData.GetAllianceID(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
        if (allianceData != null)
        {
          allianceData.diplomacy.Remove(arrayList[index], updateTime);
          DBManager.inst.DB_Alliance.Publish_OnDataUpdate(allianceId);
        }
      }
    }

    private bool Update(object orgData, long updateTime)
    {
      long oppAllianceId = AllianceDiplomacyData.GetOppAllianceID(orgData);
      if (this.datas.ContainsKey(oppAllianceId))
        return this.datas[oppAllianceId].Decode(orgData, updateTime);
      AllianceDiplomacyData allianceDiplomacyData = new AllianceDiplomacyData();
      if (!allianceDiplomacyData.Decode(orgData, updateTime))
        return false;
      this.datas.Add(oppAllianceId, allianceDiplomacyData);
      return true;
    }

    private bool Remove(object orgData, long updateTime)
    {
      long oppAllianceId = AllianceDiplomacyData.GetOppAllianceID(orgData);
      if (!this.datas.ContainsKey(oppAllianceId) || !this.datas[oppAllianceId].CheckUpdateTime(updateTime))
        return false;
      this.datas.Remove(oppAllianceId);
      return true;
    }
  }
}
