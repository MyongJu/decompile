﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightAttibute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightAttibute
  {
    private Dictionary<string, long> _attributes = new Dictionary<string, long>();

    public bool Decode(Hashtable sources)
    {
      if (sources == null)
        return false;
      bool flag = false;
      if (sources != null)
      {
        this._attributes.Clear();
        foreach (object key1 in (IEnumerable) sources.Keys)
        {
          string key2 = key1.ToString();
          long outData = 0;
          DatabaseTools.UpdateData(sources, key2, ref outData);
          this._attributes[key2] = outData;
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public static string Type2String(DragonKnightAttibute.Type type)
    {
      string str = string.Empty;
      switch (type)
      {
        case DragonKnightAttibute.Type.Strong:
          str = "str";
          break;
        case DragonKnightAttibute.Type.Intelligence:
          str = "int";
          break;
        case DragonKnightAttibute.Type.Constitution:
          str = "con";
          break;
        case DragonKnightAttibute.Type.Armor:
          str = "arm";
          break;
        case DragonKnightAttibute.Type.Damge:
          str = "atk";
          break;
        case DragonKnightAttibute.Type.MagiceDamge:
          str = "mtk";
          break;
        case DragonKnightAttibute.Type.Defense:
          str = "def";
          break;
        case DragonKnightAttibute.Type.Health:
          str = "hp";
          break;
        case DragonKnightAttibute.Type.HealthRecover:
          str = "hp_re";
          break;
        case DragonKnightAttibute.Type.MP:
          str = "mp";
          break;
        case DragonKnightAttibute.Type.MPRecover:
          str = "mp_re";
          break;
        case DragonKnightAttibute.Type.Speed:
          str = "sp";
          break;
        case DragonKnightAttibute.Type.Rebund:
          str = "reb";
          break;
        case DragonKnightAttibute.Type.FireDamge:
          str = "ftk";
          break;
        case DragonKnightAttibute.Type.WaterDamge:
          str = "wtk";
          break;
        case DragonKnightAttibute.Type.NaturalDamge:
          str = "ntk";
          break;
        case DragonKnightAttibute.Type.Stamina:
          str = "sta";
          break;
        case DragonKnightAttibute.Type.Bag:
          str = "bag";
          break;
        case DragonKnightAttibute.Type.Load:
          str = "load";
          break;
        case DragonKnightAttibute.Type.InitSta:
          str = "init_sta";
          break;
        case DragonKnightAttibute.Type.InitMP:
          str = "init_mp";
          break;
        case DragonKnightAttibute.Type.InitHP:
          str = "init_hp";
          break;
        case DragonKnightAttibute.Type.StaminaEff:
          str = "sta_eff";
          break;
        case DragonKnightAttibute.Type.MPEff:
          str = "mp_eff";
          break;
        case DragonKnightAttibute.Type.HPEff:
          str = "hp_eff";
          break;
        case DragonKnightAttibute.Type.SlotLimit:
          str = "skill_lim";
          break;
      }
      return str;
    }

    public long GetBaseAttibute(DragonKnightAttibute.Type type)
    {
      string key = DragonKnightAttibute.Type2String(type);
      if (!string.IsNullOrEmpty(key) && this._attributes.ContainsKey(key))
        return this._attributes[key];
      return 0;
    }

    public enum Type
    {
      None,
      Strong,
      Intelligence,
      Constitution,
      Armor,
      Damge,
      MagiceDamge,
      Defense,
      Health,
      HealthRecover,
      MP,
      MPRecover,
      Speed,
      Rebund,
      FireDamge,
      WaterDamge,
      NaturalDamge,
      Stamina,
      Bag,
      Load,
      InitSta,
      InitMP,
      InitHP,
      StaminaEff,
      MPEff,
      HPEff,
      SlotLimit,
    }
  }
}
