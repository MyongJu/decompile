﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTitle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace DB
{
  public static class AllianceTitle
  {
    public const int CANDIDATE = 0;
    public const int VASSAL = 1;
    public const int KNIGHT = 2;
    public const int BARON = 3;
    public const int DUKE = 4;
    public const int TEMP_LORD = 5;
    public const int LORD = 6;
    public const string R0 = "R0";
    public const string R1 = "R1";
    public const string R2 = "R2";
    public const string R3 = "R3";
    public const string R4 = "R4";
    public const string R5 = "R5";

    public static string TitleToRank(int title)
    {
      switch (title)
      {
        case 1:
          return "R1";
        case 2:
          return "R2";
        case 3:
          return "R3";
        case 4:
          return "R4";
        case 6:
          return "R5";
        default:
          return "R0";
      }
    }

    public static int RankToTitle(string rank)
    {
      string key = rank;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (AllianceTitle.\u003C\u003Ef__switch\u0024map68 == null)
        {
          // ISSUE: reference to a compiler-generated field
          AllianceTitle.\u003C\u003Ef__switch\u0024map68 = new Dictionary<string, int>(5)
          {
            {
              "R1",
              0
            },
            {
              "R2",
              1
            },
            {
              "R3",
              2
            },
            {
              "R4",
              3
            },
            {
              "R5",
              4
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (AllianceTitle.\u003C\u003Ef__switch\u0024map68.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              return 1;
            case 1:
              return 2;
            case 2:
              return 3;
            case 3:
              return 4;
            case 4:
              return 6;
          }
        }
      }
      return 0;
    }
  }
}
