﻿// Decompiled with JetBrains decompiler
// Type: DB.UserDKArenaData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserDKArenaData : BaseData, IDBDataDecoder
  {
    private List<string> _gainRecords = new List<string>();
    public const long INVALID_ID = 0;
    private long _maxScore;
    private long _oldScore;
    private long _oppUid;
    private long _uid;
    private long _score;
    private int _stageJoinTimes;
    private int _stageWinTimes;
    private int _stageLoseTimes;
    private int _totalJoinTimes;
    private int _totalWinTimes;
    private int _totalLoseTimes;
    private int _ctime;
    private int _mtime;
    private int _todayPvpTimes;
    private int _lastPvpTime;

    public long maxScore
    {
      get
      {
        return this._maxScore;
      }
    }

    public long oldSocre
    {
      get
      {
        return this._oldScore;
      }
    }

    public float StageWinPrecent
    {
      get
      {
        if (this.stageJoinTimes == 0)
          return 0.0f;
        return (float) Math.Round((double) ((float) this.stageWinTimes / (float) this.stageJoinTimes), 2);
      }
    }

    public float TotalWinPrecent
    {
      get
      {
        if (this.totalJoinTimes == 0)
          return 0.0f;
        return (float) Math.Round((double) ((float) this.totalWinTimes / (float) this.totalJoinTimes), 2);
      }
    }

    public long oppUid
    {
      get
      {
        return this._oppUid;
      }
    }

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public long score
    {
      get
      {
        return this._score;
      }
    }

    public int stageJoinTimes
    {
      get
      {
        return this._stageJoinTimes;
      }
    }

    public int stageWinTimes
    {
      get
      {
        return this._stageWinTimes;
      }
    }

    public int stageLoseTimes
    {
      get
      {
        return this._stageLoseTimes;
      }
    }

    public int totalJoinTimes
    {
      get
      {
        return this._totalJoinTimes;
      }
    }

    public int totalWinTimes
    {
      get
      {
        return this._totalWinTimes;
      }
    }

    public int totalLoseTimes
    {
      get
      {
        return this._totalLoseTimes;
      }
    }

    public int ctime
    {
      get
      {
        return this._ctime;
      }
    }

    public int mtime
    {
      get
      {
        return this._mtime;
      }
    }

    public bool IsClaim(string id)
    {
      return this._gainRecords.Contains(id);
    }

    public int todayPvpTimes
    {
      get
      {
        return this._todayPvpTimes;
      }
    }

    public int lastPvpTime
    {
      get
      {
        return this._lastPvpTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      bool flag1 = false;
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "current_opp_uid", ref this._oppUid);
      this._oldScore = this._score;
      bool flag3 = flag2 | DatabaseTools.UpdateData(inData, "score", ref this._score) | DatabaseTools.UpdateData(inData, "stage_max_score", ref this._maxScore) | DatabaseTools.UpdateData(inData, "stage_join_times", ref this._stageJoinTimes) | DatabaseTools.UpdateData(inData, "stage_win_times", ref this._stageWinTimes) | DatabaseTools.UpdateData(inData, "stage_lose_times", ref this._stageLoseTimes) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "total_join_times", ref this._totalJoinTimes) | DatabaseTools.UpdateData(inData, "total_win_times", ref this._totalWinTimes) | DatabaseTools.UpdateData(inData, "total_lose_times", ref this._totalLoseTimes) | DatabaseTools.UpdateData(inData, "today_pvp_times", ref this._todayPvpTimes) | DatabaseTools.UpdateData(inData, "last_pvp_time", ref this._lastPvpTime);
      if (inData.ContainsKey((object) "rewards_gain_info") && inData[(object) "rewards_gain_info"] != null)
      {
        this._gainRecords.Clear();
        ArrayList arrayList = inData[(object) "rewards_gain_info"] as ArrayList;
        for (int index = 0; index < arrayList.Count; ++index)
          this._gainRecords.Add(arrayList[index].ToString());
        flag3 = true;
      }
      return flag3;
    }

    public int GetRewardCount()
    {
      int num = 0;
      List<DKArenaTitleReward> totals = ConfigManager.inst.DB_DKArenaTitleReward.GetTotals();
      for (int index = 0; index < totals.Count; ++index)
      {
        if ((this.maxScore >= (long) totals[index].ScoreMin && this.maxScore <= (long) totals[index].ScoreMax || this.maxScore > (long) totals[index].ScoreMax) && (totals[index].Rewards != null && totals[index].Rewards.rewards.Count > 0 && !this.IsClaim(totals[index].ID)))
          ++num;
      }
      return num;
    }

    public long ID
    {
      get
      {
        return this._uid;
      }
    }

    public struct Params
    {
      public const string KEY = "user_dk_arena";
      public const string UID = "uid";
      public const string SCORE = "score";
      public const string STAGE_JOIN_TIMES = "stage_join_times";
      public const string STAGE_WIN_TIMES = "stage_win_times";
      public const string STAGE_LOSE_TIMES = "stage_lose_times";
      public const string TOTAL_JOIN_TIMES = "total_join_times";
      public const string TOTAL_WIN_TIMES = "total_win_times";
      public const string TOTAL_LOSE_TIMES = "total_lose_times";
      public const string REWARDS_INFO = "rewards_gain_info";
      public const string TODAY_PVP_TIMES = "today_pvp_times";
      public const string LAST_PVP_TIME = "last_pvp_time";
      public const string CURRENT_OPP_UID = "current_opp_uid";
      public const string STAGE_MAX_SCORE = "stage_max_score";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
