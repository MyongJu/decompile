﻿// Decompiled with JetBrains decompiler
// Type: DB.KingdomBossDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class KingdomBossDB
  {
    private Dictionary<long, KingdomBossData> _datas = new Dictionary<long, KingdomBossData>();
    public Action<long> onDataChanged;
    public Action<long> onDataCreated;
    public Action<long> onDataUpdated;
    public Action<long> onDataRemove;
    public Action onDataRemoved;

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      KingdomBossData kingdomBossData = new KingdomBossData();
      if (!kingdomBossData.Decode((object) data, updateTime) || kingdomBossData.bossId == 0 || this._datas.ContainsKey((long) kingdomBossData.bossId))
        return false;
      this._datas.Add((long) kingdomBossData.bossId, kingdomBossData);
      this.Publish_onDataCreated((long) kingdomBossData.bossId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(data, "boss_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      if (!this._datas[outData].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated(outData);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList data;
      if (!BaseData.CheckAndParseOrgData(orgDatas, out data))
        return;
      int index = 0;
      for (int count = data.Count; index < count; ++index)
        this.Update(data[index], updateTime);
    }

    public KingdomBossData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (KingdomBossData) null;
    }

    public bool Remove(long itemId, long updateTime)
    {
      if (!this._datas.ContainsKey(itemId) || !this._datas[itemId].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(itemId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "boss_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public void ShowInfo()
    {
    }
  }
}
