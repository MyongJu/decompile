﻿// Decompiled with JetBrains decompiler
// Type: DB.WorldMapDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class WorldMapDB
  {
    private Dictionary<long, WorldMapData> _datas = new Dictionary<long, WorldMapData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (WorldMapData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      WorldMapData worldMapData = new WorldMapData();
      if (!worldMapData.Decode((object) data, updateTime) || worldMapData.kingdomId == 0 || this._datas.ContainsKey((long) worldMapData.kingdomId))
        return false;
      this._datas.Add((long) worldMapData.kingdomId, worldMapData);
      this.Publish_onDataCreated((long) worldMapData.kingdomId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int kingdomId = WorldMapData.GetKingdomId((object) data);
      if (kingdomId == 0)
        return false;
      if (!this._datas.ContainsKey((long) kingdomId))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) kingdomId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated((long) kingdomId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList data;
      if (!BaseData.CheckAndParseOrgData(orgDatas, out data))
        return;
      int index = 0;
      for (int count = data.Count; index < count; ++index)
        this.Update(data[index], updateTime);
    }

    public WorldMapData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (WorldMapData) null;
    }

    public bool Remove(long itemId, long updateTime)
    {
      if (!this._datas.ContainsKey(itemId) || !this._datas[itemId].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(itemId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove((long) WorldMapData.GetKingdomId(arrayList[index]), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<long, WorldMapData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          WorldMapData current = enumerator.Current;
        }
      }
    }
  }
}
