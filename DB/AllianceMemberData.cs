﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceMemberData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceMemberData : BaseData
  {
    private long _uid;
    private long _allianceId;
    private int _title;
    private int _muted;
    private int _status;
    private long _joinTime;

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public long allianceId
    {
      get
      {
        return this._allianceId;
      }
    }

    public int title
    {
      get
      {
        return this._title;
      }
    }

    public int muted
    {
      get
      {
        return this._muted;
      }
    }

    public int status
    {
      get
      {
        return this._status;
      }
    }

    public long joinTime
    {
      get
      {
        return this._joinTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "title", ref this._title) | DatabaseTools.UpdateData(inData, "muted", ref this._muted) | DatabaseTools.UpdateData(inData, "status", ref this._status) | DatabaseTools.UpdateData(inData, "join_time", ref this._joinTime);
    }

    public string ShowInfo(int tableCount)
    {
      string empty = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        empty += "\t";
      return "Alliance Member : " + (object) this.uid + "( " + (object) this.allianceId + " )\n" + "Title : " + (object) this.title + "\n" + "Muted : " + (object) this.muted + "\n" + "Status : " + (object) this.status + "\n";
    }

    public static long GetAllianceID(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      long result = 0;
      long.TryParse(hashtable[(object) "alliance_id"].ToString(), out result);
      return result;
    }

    public static long GetUid(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      long result = 0;
      long.TryParse(hashtable[(object) "uid"].ToString(), out result);
      return result;
    }

    public bool HasAllianceStoreAccess()
    {
      switch (this.title)
      {
        case 4:
        case 5:
        case 6:
          return true;
        default:
          return false;
      }
    }

    public struct Params
    {
      public const string KEY = "alliance_roster";
      public const string ALLIANCE_MEMBER_UID = "uid";
      public const string ALLIANCE_MEMBER_ALLIANCE_ID = "alliance_id";
      public const string ALLIANCE_MEMBER_TITLE = "title";
      public const string ALLIANCE_MEMBER_MUTED = "muted";
      public const string ALLIANCE_MEMBER_STATUS = "status";
      public const string ALLIANCE_MEMBER_JOINTIME = "join_time";
    }
  }
}
