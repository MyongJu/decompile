﻿// Decompiled with JetBrains decompiler
// Type: DB.MarchTargetInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

namespace DB
{
  public struct MarchTargetInfo
  {
    public long playerId;
    private Coordinate _location;
    private Vector3 _position;

    public string playerName
    {
      get
      {
        UserData userData = DBManager.inst.DB_User.Get(this.playerId);
        if (userData != null)
          return userData.userName;
        return string.Empty;
      }
    }

    public Coordinate location
    {
      get
      {
        return this._location;
      }
    }

    public Vector3 position
    {
      get
      {
        return this._position;
      }
    }

    public long allianceId
    {
      get
      {
        if (DBManager.inst.DB_User.Get(this.playerId) != null)
          return DBManager.inst.DB_User.Get(this.playerId).allianceId;
        return 0;
      }
    }

    public long cityId
    {
      get
      {
        return DBManager.inst.DB_User.Get(this.playerId) != null ? -1L : -1L;
      }
    }

    public bool Decode(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | this._location.Decode(inData[(object) "location"]);
      if (flag)
        this._position = PVPMapData.MapData.ConvertTileKXYToWorldPosition(this._location);
      return flag | DatabaseTools.UpdateData(inData, "uid", ref this.playerId);
    }

    public struct Params
    {
      public const string UID = "uid";
      public const string TARGET_LOCATION = "location";
    }
  }
}
