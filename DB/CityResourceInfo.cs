﻿// Decompiled with JetBrains decompiler
// Type: DB.CityResourceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public struct CityResourceInfo
  {
    public CityResourceInfo.CityResource_Resource resources;
    public CityResourceInfo.CityResource_Boost rates;
    public CityResourceInfo.CityResource_Boost limits;
    public CityResourceInfo.CityResource_Upkeep upKeeps;
    public int updateTime;

    public long totalUpkeepsInSecond { get; private set; }

    public long totalUpkeepsInHour { get; private set; }

    public double GetCurrentResource(CityResourceInfo.ResourceType type)
    {
      double resourceBase = 0.0;
      double generateBaseValue = 0.0;
      string benefitCalcID = string.Empty;
      double num1 = 0.0;
      string storgeBenefitCalcID = string.Empty;
      long num2 = 0;
      switch (type)
      {
        case CityResourceInfo.ResourceType.FOOD:
          resourceBase = this.resources.food;
          generateBaseValue = (double) BenefitLocalDB.inst.Get("prop_food_generation_base_value").total;
          benefitCalcID = "calc_food_generation";
          storgeBenefitCalcID = "calc_storage_protect";
          num1 = (double) BenefitLocalDB.inst.Get("prop_food_storage_base_value").total;
          num2 = CityManager.inst.CityUpKeep;
          break;
        case CityResourceInfo.ResourceType.WOOD:
          resourceBase = this.resources.wood;
          generateBaseValue = (double) BenefitLocalDB.inst.Get("prop_wood_generation_base_value").total;
          benefitCalcID = "calc_wood_generation";
          storgeBenefitCalcID = "calc_storage_protect";
          num1 = (double) BenefitLocalDB.inst.Get("prop_wood_storage_base_value").total;
          break;
        case CityResourceInfo.ResourceType.ORE:
          resourceBase = this.resources.ore;
          generateBaseValue = (double) BenefitLocalDB.inst.Get("prop_ore_generation_base_value").total;
          benefitCalcID = "calc_ore_generation";
          storgeBenefitCalcID = "calc_storage_protect";
          num1 = (double) BenefitLocalDB.inst.Get("prop_ore_storage_base_value").total;
          break;
        case CityResourceInfo.ResourceType.SILVER:
          resourceBase = this.resources.silver;
          generateBaseValue = (double) BenefitLocalDB.inst.Get("prop_silver_generation_base_value").total;
          benefitCalcID = "calc_silver_generation";
          storgeBenefitCalcID = "calc_storage_protect";
          num1 = (double) BenefitLocalDB.inst.Get("prop_silver_storage_base_value").total;
          break;
      }
      float total = BenefitLocalDB.inst.Get("prop_storage_protect_base_value").total;
      double storgeBaseValue = num1 + (double) total;
      return this.calcResource(resourceBase, generateBaseValue, benefitCalcID, storgeBaseValue, storgeBenefitCalcID, type, (double) num2);
    }

    private double calcResource(double resourceBase, double generateBaseValue, string benefitCalcID, double storgeBaseValue, string storgeBenefitCalcID, CityResourceInfo.ResourceType type, double upkeepsInHour = 0)
    {
      double num1 = -upkeepsInHour;
      double finalData = (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) storgeBaseValue, storgeBenefitCalcID);
      double num2 = Math.Min(resourceBase, finalData);
      double num3 = resourceBase + num1 * (double) (NetServerTime.inst.ServerTimestamp - this.updateTime) / 3600.0;
      if (num3 <= finalData)
        num3 = num2;
      if (num3 <= 0.0)
        num3 = 0.0;
      return num3;
    }

    private double calcResource(double resourceBase, double generateValue, double generatePercent, double storageValue, double storagePercent, double upkeepsInHour = 0)
    {
      double num1 = (double) (NetServerTime.inst.ServerTimestamp - this.updateTime) * DatabaseTools.calc2(generateValue + (double) BenefitLocalDB.inst.Get("prop_resource_generation_value").total, generatePercent + (double) BenefitLocalDB.inst.Get("prop_resource_generation_percent").total, 0.0) / 3600.0;
      double num2 = DatabaseTools.calc2(storageValue + (double) BenefitLocalDB.inst.Get("prop_resource_storage_value").total, storagePercent + (double) BenefitLocalDB.inst.Get("prop_resource_storage_percent").total, 0.0);
      double num3 = resourceBase - upkeepsInHour * (double) (NetServerTime.inst.ServerTimestamp - this.updateTime) / 3600.0;
      if (num3 < num2)
      {
        num3 += num1;
        if (num3 > num2)
          return num2;
        if (num3 < 0.0)
          return 0.0;
      }
      return num3;
    }

    public bool Decode(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | this.resources.Decode(inData[(object) "resource"]) | this.rates.Decode(inData[(object) "rate"]) | this.limits.Decode(inData[(object) "limit"]) | this.upKeeps.Decode(inData[(object) "upkeep"]);
      this.totalUpkeepsInSecond = this.upKeeps.totalUpkeepsInSecond;
      this.totalUpkeepsInHour = this.upKeeps.totalUpkeepsInHour;
      return flag | DatabaseTools.UpdateData(inData, "update_time", ref this.updateTime);
    }

    public string ShowInfo(int tableCount)
    {
      string empty = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        empty += "\t";
      return string.Empty + empty + "resource\n" + this.resources.ShowInfo(tableCount + 1) + "\n" + empty + "rate\n" + this.rates.ShowInfo(tableCount + 1) + "\n" + empty + "limit\n" + this.limits.ShowInfo(tableCount + 1) + "\n";
    }

    public struct Params
    {
      public const string KEY = "resource";
      public const string CITY_RESOURCE_INFO_RESOURCE = "resource";
      public const string CITY_RESOURCE_INFO_RATE = "rate";
      public const string CITY_RESOURCE_INFO_LIMIT = "limit";
      public const string CITY_RESOURCE_INFO_UPKEEP = "upkeep";
      public const string CITY_RESOURCE_INFO_UPDATE_TIME = "update_time";
    }

    public struct ResourceName
    {
      public const string FOOD = "food";
      public const string WOOD = "wood";
      public const string ORE = "ore";
      public const string SILVER = "silver";
    }

    public enum ResourceType
    {
      FOOD,
      WOOD,
      ORE,
      SILVER,
    }

    public struct CityResource_Resource
    {
      public double food;
      public double wood;
      public double ore;
      public double silver;

      public bool Decode(object orgData)
      {
        this.Clear();
        if (orgData == null)
        {
          this.Clear();
          return false;
        }
        Hashtable inData = orgData as Hashtable;
        if (inData == null)
          return false;
        bool flag1 = false;
        bool flag2;
        if (inData.ContainsKey((object) "food"))
        {
          flag2 = flag1 | DatabaseTools.UpdateData(inData, "food", ref this.food);
        }
        else
        {
          this.food = 0.0;
          flag2 = ((flag1 ? 1 : 0) | 1) != 0;
        }
        bool flag3;
        if (inData.ContainsKey((object) "wood"))
        {
          flag3 = flag2 | DatabaseTools.UpdateData(inData, "wood", ref this.wood);
        }
        else
        {
          this.wood = 0.0;
          flag3 = ((flag2 ? 1 : 0) | 1) != 0;
        }
        bool flag4;
        if (inData.ContainsKey((object) "ore"))
        {
          flag4 = flag3 | DatabaseTools.UpdateData(inData, "ore", ref this.ore);
        }
        else
        {
          this.ore = 0.0;
          flag4 = ((flag3 ? 1 : 0) | 1) != 0;
        }
        bool flag5;
        if (inData.ContainsKey((object) "silver"))
        {
          flag5 = flag4 | DatabaseTools.UpdateData(inData, "silver", ref this.silver);
        }
        else
        {
          this.silver = 0.0;
          flag5 = ((flag4 ? 1 : 0) | 1) != 0;
        }
        return flag5;
      }

      public string ShowInfo(int tableCount)
      {
        string empty = string.Empty;
        for (int index = 0; index < tableCount; ++index)
          empty += "\t";
        return string.Empty + empty + "food : " + (object) this.food + "\n" + empty + "wood : " + (object) this.wood + "\n" + empty + "ore  : " + (object) this.ore + "\n" + empty + "silver:" + (object) this.silver + "\n";
      }

      public void Clear()
      {
        this.food = 0.0;
        this.wood = 0.0;
        this.ore = 0.0;
        this.silver = 0.0;
      }

      public struct Params
      {
        public const string FOOD = "food";
        public const string WOOD = "wood";
        public const string ORE = "ore";
        public const string SILVER = "silver";
      }
    }

    public struct CityResource_Upkeep
    {
      public List<CityResourceInfo.CityResource_UpkeepSlot> upkeeps;
      public long totalUpkeepsInSecond;
      public long totalUpkeepsInHour;

      public bool Decode(object orgData)
      {
        if (this.upkeeps == null)
          this.upkeeps = new List<CityResourceInfo.CityResource_UpkeepSlot>();
        this.upkeeps.Clear();
        this.totalUpkeepsInHour = this.totalUpkeepsInSecond = 0L;
        Hashtable data;
        if (!BaseData.CheckAndParseOrgData(orgData, out data))
          return false;
        foreach (string key in (IEnumerable) data.Keys)
        {
          CityResourceInfo.CityResource_UpkeepSlot resourceUpkeepSlot = new CityResourceInfo.CityResource_UpkeepSlot();
          resourceUpkeepSlot.Set(key, long.Parse(data[(object) key].ToString()));
          this.totalUpkeepsInHour += resourceUpkeepSlot.finalCount;
          this.upkeeps.Add(resourceUpkeepSlot);
        }
        this.totalUpkeepsInSecond = this.totalUpkeepsInHour / 3600L;
        return true;
      }
    }

    public struct CityResource_UpkeepSlot
    {
      public static char[] splitChars = new char[1]{ '_' };
      public string ID;
      public long baseCount;
      public double boost;
      public long finalCount;

      public void Set(string inID, long inCount)
      {
        this.ID = inID;
        this.baseCount = inCount;
        this.boost = 1.0 - (double) DBManager.inst.DB_Local_Benefit.Get(string.Format("prop_{0}_upkeep_percent", (object[]) inID.Split(CityResourceInfo.CityResource_UpkeepSlot.splitChars))).total - (double) BenefitLocalDB.inst.Get("prop_army_upkeep_percent").total;
        this.finalCount = (long) ((double) this.baseCount * this.boost);
      }
    }

    public struct CityResource_Boost
    {
      public CityResourceInfo.CityResource_BoostSlot food;
      public CityResourceInfo.CityResource_BoostSlot wood;
      public CityResourceInfo.CityResource_BoostSlot ore;
      public CityResourceInfo.CityResource_BoostSlot silver;

      public CityResourceInfo.CityResource_BoostSlot this[CityManager.ResourceTypes type]
      {
        get
        {
          switch (type)
          {
            case CityManager.ResourceTypes.FOOD:
              return this.food;
            case CityManager.ResourceTypes.SILVER:
              return this.silver;
            case CityManager.ResourceTypes.WOOD:
              return this.wood;
            case CityManager.ResourceTypes.ORE:
              return this.ore;
            default:
              return this.food;
          }
        }
      }

      public bool Decode(object orgData)
      {
        this.Clear();
        if (orgData == null)
        {
          this.Clear();
          return false;
        }
        Hashtable hashtable = orgData as Hashtable;
        if (hashtable == null)
          return false;
        bool flag1 = false;
        bool flag2;
        if (hashtable.ContainsKey((object) "food"))
        {
          flag2 = flag1 | this.food.Decode(hashtable[(object) "food"]);
        }
        else
        {
          this.food.Clear();
          flag2 = ((flag1 ? 1 : 0) | 1) != 0;
        }
        bool flag3;
        if (hashtable.ContainsKey((object) "wood"))
        {
          flag3 = flag2 | this.wood.Decode(hashtable[(object) "wood"]);
        }
        else
        {
          this.wood.Clear();
          flag3 = ((flag2 ? 1 : 0) | 1) != 0;
        }
        bool flag4;
        if (hashtable.ContainsKey((object) "ore"))
        {
          flag4 = flag3 | this.ore.Decode((object) "ore");
        }
        else
        {
          this.ore.Clear();
          flag4 = ((flag3 ? 1 : 0) | 1) != 0;
        }
        bool flag5;
        if (hashtable.ContainsKey((object) "silver"))
        {
          flag5 = flag4 | this.silver.Decode(hashtable[(object) "silver"]);
        }
        else
        {
          this.silver.Clear();
          flag5 = ((flag4 ? 1 : 0) | 1) != 0;
        }
        return flag5;
      }

      public string ShowInfo(int tableCount)
      {
        string empty = string.Empty;
        for (int index = 0; index < tableCount; ++index)
          empty += "\t";
        return string.Empty + empty + "food : " + this.food.ShowInfo(0) + "\n" + empty + "wood : " + this.wood.ShowInfo(0) + "\n" + empty + "ore  : " + this.ore.ShowInfo(0) + "\n" + empty + "silver:" + this.silver.ShowInfo(0) + "\n";
      }

      public void Clear()
      {
        this.food.Clear();
        this.wood.Clear();
        this.ore.Clear();
        this.silver.Clear();
      }

      public struct Params
      {
        public const string FOOD = "food";
        public const string WOOD = "wood";
        public const string ORE = "ore";
        public const string SILIVER = "silver";
      }
    }

    public struct CityResource_BoostSlot
    {
      public long count;
      public float boost;
      public long finalCount;

      public bool Decode(object orgData)
      {
        this.Clear();
        if (orgData == null)
          return false;
        Hashtable inData = orgData as Hashtable;
        if (inData == null)
          return false;
        bool flag = false | DatabaseTools.UpdateData(inData, "base", ref this.count) | DatabaseTools.UpdateData(inData, "boost", ref this.boost);
        this.finalCount = (long) ((double) this.count * (double) this.boost);
        return flag;
      }

      public string ShowInfo(int tableCount)
      {
        string empty = string.Empty;
        for (int index = 0; index < tableCount; ++index)
          empty += "\t";
        return string.Empty + empty + "{ base : " + (object) this.count + "; boost : " + (object) this.boost + "}";
      }

      public void Clear()
      {
        this.count = 0L;
        this.boost = 0.0f;
      }

      public struct Params
      {
        public const string BASE = "base";
        public const string BOOST = "boost";
      }
    }
  }
}
