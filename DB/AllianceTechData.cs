﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTechData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceTechData : BaseData
  {
    public const int INVALID_ID = 0;
    private int _researchId;
    private long _allianceId;
    private int _donation;
    private int _level;
    private long _jobId;
    public AllianceTechInfo info;

    public int researchId
    {
      get
      {
        return this._researchId;
      }
      set
      {
        this._researchId = value;
      }
    }

    public long allianceId
    {
      get
      {
        return this._allianceId;
      }
      set
      {
        this._allianceId = value;
      }
    }

    public int donation
    {
      get
      {
        return this._donation;
      }
    }

    public int level
    {
      get
      {
        return this._level;
      }
      set
      {
        this._level = value;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "research_id", ref this._researchId) | DatabaseTools.UpdateData(dataHt, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(dataHt, "donation", ref this._donation) | DatabaseTools.UpdateData(dataHt, "job_id", ref this._jobId);
      this.info = ConfigManager.inst.DB_AllianceTech.GetItem(this.researchId);
      return flag;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return string.Empty + str + "Research ID: " + (object) this.researchId + "\n" + str + "Alliance ID: " + (object) this.allianceId + "\n" + str + "Donation: " + (object) this.donation + "\n" + str + "JOb ID: " + (object) this.jobId + "\n";
    }

    public struct Params
    {
      public const string RESEARCH_ID = "research_id";
      public const string ALLIANCE_ID = "alliance_id";
      public const string DONATION = "donation";
      public const string JOB_ID = "job_id";
    }
  }
}
