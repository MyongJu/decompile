﻿// Decompiled with JetBrains decompiler
// Type: DB.HallOfKingDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class HallOfKingDB
  {
    private Dictionary<long, HallOfKingData> _datas = new Dictionary<long, HallOfKingData>();

    public event System.Action<HallOfKingData> onDataChanged;

    public event System.Action<HallOfKingData> onDataUpdate;

    public event System.Action<HallOfKingData> onDataCreate;

    public event System.Action<HallOfKingData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (HallOfKingData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(HallOfKingData hallOfKingData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(hallOfKingData);
    }

    private void PublishOnDataCreateEvent(HallOfKingData hallOfKingData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(hallOfKingData);
      this.PublishOnDataChangedEvent(hallOfKingData);
    }

    private void PublishOnDataUpdateEvent(HallOfKingData hallOfKingData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(hallOfKingData);
      this.PublishOnDataChangedEvent(hallOfKingData);
    }

    private void PublishOnDataRemovedEvent(HallOfKingData hallOfKingData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(hallOfKingData);
      this.PublishOnDataChangedEvent(hallOfKingData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      HallOfKingData hallOfKingData = new HallOfKingData();
      if (!hallOfKingData.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(hallOfKingData.WorldId))
        return false;
      this._datas.Add(hallOfKingData.WorldId, hallOfKingData);
      this.PublishOnDataCreateEvent(hallOfKingData);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "world_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      HallOfKingData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long worldId, long updateTime)
    {
      if (!this._datas.ContainsKey(worldId))
        return false;
      HallOfKingData data = this._datas[worldId];
      this._datas.Remove(worldId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "world_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public HallOfKingData Get(long worldId)
    {
      HallOfKingData hallOfKingData;
      this._datas.TryGetValue(worldId, out hallOfKingData);
      return hallOfKingData;
    }
  }
}
