﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceSuperMineData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceSuperMineData : BaseData
  {
    private List<long> troopsSort = new List<long>();
    protected Dictionary<long, long> troops = new Dictionary<long, long>();
    protected long alliance_id;
    protected long superMineId;
    protected long owner_uid;
    protected int world_id;
    protected int map_x;
    protected int map_y;
    protected long durability;
    protected string name;
    protected long amount;
    protected float gather_rate;
    protected AllianceSuperMineData.State state;
    protected long build_job_id;
    protected int timer_end;
    protected int config_id;
    protected long utime;
    protected long mtime;

    public Coordinate Location
    {
      get
      {
        Coordinate coordinate;
        coordinate.K = this.world_id;
        coordinate.X = this.map_x;
        coordinate.Y = this.map_y;
        return coordinate;
      }
    }

    public long AllianceId
    {
      get
      {
        return this.alliance_id;
      }
    }

    public long SuperMineId
    {
      get
      {
        return this.superMineId;
      }
    }

    public long OwnerUid
    {
      get
      {
        return this.owner_uid;
      }
    }

    public int WorldId
    {
      get
      {
        return this.world_id;
      }
    }

    public int MapX
    {
      get
      {
        return this.map_x;
      }
    }

    public int MapY
    {
      get
      {
        return this.map_y;
      }
    }

    public long Durability
    {
      get
      {
        return this.durability;
      }
    }

    public List<long> TroopsSort
    {
      get
      {
        return this.troopsSort;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this.troops;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public long Amount
    {
      get
      {
        return this.amount;
      }
    }

    public float GatherRate
    {
      get
      {
        return this.gather_rate;
      }
    }

    public AllianceSuperMineData.State CurrentState
    {
      get
      {
        return this.state;
      }
    }

    public long BuildJobId
    {
      get
      {
        return this.build_job_id;
      }
    }

    public int TimerEnd
    {
      get
      {
        return this.timer_end;
      }
    }

    public int ConfigId
    {
      get
      {
        return this.config_id;
      }
    }

    public long Utime
    {
      get
      {
        return this.utime;
      }
    }

    public long Mtime
    {
      get
      {
        return this.mtime;
      }
    }

    public long MaxTroopsCount { get; set; }

    public int BuildingType
    {
      get
      {
        AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.config_id);
        if (buildingStaticInfo != null)
          return buildingStaticInfo.type;
        return -1;
      }
    }

    public long CurrentTroopsCount
    {
      get
      {
        long num = 0;
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
          if (marchData != null)
            num += (long) marchData.troopsInfo.totalCount;
        }
        return num;
      }
    }

    public long CurrentTroopId
    {
      get
      {
        Dictionary<long, long>.KeyCollection.Enumerator enumerator = this.Troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
            return this.Troops[enumerator.Current];
        }
        return 0;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false;
      long amount = this.amount;
      AllianceSuperMineData.State state = this.state;
      if (inData.ContainsKey((object) "state"))
      {
        string key = inData[(object) "state"].ToString();
        if (key != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (AllianceSuperMineData.\u003C\u003Ef__switch\u0024map6A == null)
          {
            // ISSUE: reference to a compiler-generated field
            AllianceSuperMineData.\u003C\u003Ef__switch\u0024map6A = new Dictionary<string, int>(4)
            {
              {
                "building",
                0
              },
              {
                "complete",
                1
              },
              {
                "freeze",
                2
              },
              {
                "unComplete",
                3
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (AllianceSuperMineData.\u003C\u003Ef__switch\u0024map6A.TryGetValue(key, out num))
          {
            switch (num)
            {
              case 0:
                this.state = AllianceSuperMineData.State.BUILDING;
                goto label_15;
              case 1:
                this.state = AllianceSuperMineData.State.COMPLETE;
                goto label_15;
              case 2:
                this.state = AllianceSuperMineData.State.FREEZE;
                goto label_15;
              case 3:
                this.state = AllianceSuperMineData.State.UNCOMPLETE;
                goto label_15;
            }
          }
        }
        D.error((object) "parse super mine data error: invalid state");
label_15:
        flag1 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      if (state != this.state && this.state == AllianceSuperMineData.State.COMPLETE)
        this.mtime = (long) NetServerTime.inst.ServerTimestamp;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id) | DatabaseTools.UpdateData(inData, "building_id", ref this.superMineId) | DatabaseTools.UpdateData(inData, "owner_uid", ref this.owner_uid) | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "map_x", ref this.map_x) | DatabaseTools.UpdateData(inData, "map_y", ref this.map_y) | DatabaseTools.UpdateData(inData, "durability", ref this.durability) | DatabaseTools.UpdateData(inData, "name", ref this.name) | DatabaseTools.UpdateData(inData, "resource_amt", ref this.amount) | DatabaseTools.UpdateData(inData, "gather_rate", ref this.gather_rate) | DatabaseTools.UpdateData(inData, string.Empty, ref this.build_job_id) | DatabaseTools.UpdateData(inData, "config_id", ref this.config_id) | DatabaseTools.UpdateData(inData, "timer_end", ref this.timer_end) | DatabaseTools.UpdateData(inData, "utime", ref this.utime);
      if (inData.Contains((object) "troops_sort"))
      {
        flag2 = true;
        this.troopsSort.Clear();
        ArrayList arrayList = inData[(object) "troops_sort"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            long result = 0;
            if (long.TryParse(arrayList[index].ToString(), out result))
              this.troopsSort.Add(result);
          }
        }
      }
      if (amount != this.amount && this.state == AllianceSuperMineData.State.COMPLETE)
        this.mtime = (long) NetServerTime.inst.ServerTimestamp;
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        bool flag3 = ((flag2 ? 1 : 0) | 1) != 0;
      }
      return true;
    }

    public struct Params
    {
      public const string KEY = "alliance_super_ore";
      public const string ALLIANCE_ID = "alliance_id";
      public const string SUPERMINE_ID = "building_id";
      public const string OWNER_UID = "owner_uid";
      public const string WORLD_ID = "world_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string DURABILITY = "durability";
      public const string TROOPS = "troops";
      public const string AMOUNT = "resource_amt";
      public const string GATHER_RATE = "gather_rate";
      public const string NAME = "name";
      public const string STATE = "state";
      public const string STATE_BUILDING = "building";
      public const string STATE_UNCOMPLETE = "unComplete";
      public const string STATE_COMPLETE = "complete";
      public const string STATE_FREEZE = "freeze";
      public const string BUILDING_JOB_ID = "";
      public const string CONFIG_ID = "config_id";
      public const string TIMER_END = "timer_end";
      public const string UTIME = "utime";
      public const string MTIME = "mtime";
      public const string TROOPS_SORT = "troops_sort";
    }

    public enum State
    {
      UNCOMPLETE = 1,
      BUILDING = 2,
      COMPLETE = 3,
      FREEZE = 4,
    }
  }
}
