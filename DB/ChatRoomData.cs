﻿// Decompiled with JetBrains decompiler
// Type: DB.ChatRoomData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ChatRoomData : BaseData
  {
    public Dictionary<long, ChatRoomMember> members = new Dictionary<long, ChatRoomMember>();
    public const long INVALID_ID = 0;
    private long _roomId;
    private long _uid;
    private string _roomName;
    private string _roomTitle;
    private long _channelId;
    private int _isOpen;
    private int _isSearch;
    private int _memberCount;

    public long roomId
    {
      get
      {
        return this._roomId;
      }
    }

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public string roomName
    {
      get
      {
        return this._roomName;
      }
    }

    public string roomTitle
    {
      get
      {
        return this._roomTitle;
      }
    }

    public long channelId
    {
      get
      {
        return this._channelId;
      }
    }

    public bool isOpen
    {
      get
      {
        return this._isOpen == 1;
      }
    }

    public bool isSearch
    {
      get
      {
        return this._isSearch == 1;
      }
    }

    public int MemberCount
    {
      get
      {
        return this._memberCount;
      }
    }

    public int TotalCount
    {
      get
      {
        return ConfigManager.inst.DB_GameConfig.GetData("chat_room_max_number").ValueInt;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "room_id", ref this._roomId) | DatabaseTools.UpdateData(dataHt, "uid", ref this._uid) | DatabaseTools.UpdateData(dataHt, "name", ref this._roomName) | DatabaseTools.UpdateData(dataHt, "title", ref this._roomTitle) | DatabaseTools.UpdateData(dataHt, "chat_channel", ref this._channelId) | DatabaseTools.UpdateData(dataHt, "is_open", ref this._isOpen) | DatabaseTools.UpdateData(dataHt, "is_search", ref this._isSearch) | DatabaseTools.UpdateData(dataHt, "member_count", ref this._memberCount);
      this.ShowInfo(0);
      return flag;
    }

    public int GetNoneApplicationMemberCount()
    {
      int num = 0;
      Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.members.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.title != ChatRoomMember.Title.application)
          ++num;
      }
      return num;
    }

    public ChatRoomMember GetOwnerMember()
    {
      Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.members.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.title == ChatRoomMember.Title.owner)
          return enumerator.Current;
      }
      return (ChatRoomMember) null;
    }

    public int ApplyingCount()
    {
      int num = 0;
      Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.members.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.title == ChatRoomMember.Title.application)
          ++num;
      }
      return num;
    }

    public int InvitingCount()
    {
      int num1 = 0;
      Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.members.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.title == ChatRoomMember.Title.beinvited)
        {
          int num2 = num1;
          int num3 = num2 + 1;
          return num2;
        }
      }
      return num1;
    }

    public bool UpdateMemeber(ChatRoomMember tmpMemeber, object orgData, long updateTime)
    {
      if (tmpMemeber.roomId != this.roomId)
        return false;
      if (this.members.ContainsKey(tmpMemeber.uid))
        return this.members[tmpMemeber.uid].Decode(orgData, updateTime);
      this.members.Add(tmpMemeber.uid, tmpMemeber);
      return true;
    }

    public bool RemoveMemeber(ChatRoomMember tmpMemeber)
    {
      if (tmpMemeber.roomId != this.roomId)
        return false;
      if (this.members.ContainsKey(tmpMemeber.uid))
        this.members.Remove(tmpMemeber.uid);
      return true;
    }

    public static long GetRoomId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return long.Parse(data[(object) "room_id"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Chat Room : " + (object) this.roomId + "\n" + str + "Room name : " + this.roomName + "\n" + str + "Room title : " + this.roomTitle + "\n" + str + "Room token : " + (object) this.channelId + "\n" + str + "Room is opened : " + (object) this.isOpen + "\n" + str + "Room can be searched : " + (object) this.isSearch + "\n";
    }

    public struct Params
    {
      public const string KEY = "chat_room";
      public const string CHAT_ROOM_ID = "room_id";
      public const string CHAT_ROOM_UID = "uid";
      public const string CHAT_ROOM_NAME = "name";
      public const string CHAT_ROOM_TITLE = "title";
      public const string CHAT_ROOM_CHANNEL = "chat_channel";
      public const string CHAT_ROOM_IS_OPEN = "is_open";
      public const string CHAT_ROOM_IS_SEARCH = "is_search";
      public const string CHAT_ROOM_MEMBER_COUNT = "member_count";
    }
  }
}
