﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceFortressData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceFortressData : BaseData
  {
    protected int configId = 12300001;
    private List<long> troopsSort = new List<long>();
    protected Dictionary<int, float> benefits = new Dictionary<int, float>();
    public const long INVALID_ID = 0;
    protected int finish_time;
    protected long benefit_job_id;
    protected long build_job_id;
    protected int is_complete;
    protected long alliance_id;
    protected long fortress_id;
    protected long owner_uid;
    protected long owner_alliance_id;
    protected int world_id;
    protected int map_x;
    protected int map_y;
    protected int updateDurationTime;
    protected long durability;
    protected string name;
    protected long active_time;
    protected long ctime;
    protected long mtime;
    protected Dictionary<long, long> troops;
    protected string state;

    public int ConfigId
    {
      get
      {
        return this.configId;
      }
    }

    public int FinishTime
    {
      get
      {
        return this.finish_time;
      }
    }

    public long BenefitJobId
    {
      get
      {
        return this.benefit_job_id;
      }
    }

    public long BuildJobId
    {
      get
      {
        return this.build_job_id;
      }
    }

    public bool IsComplete
    {
      get
      {
        return this.is_complete > 0;
      }
    }

    public long allianceId
    {
      get
      {
        return this.alliance_id;
      }
    }

    public long fortressId
    {
      get
      {
        return this.fortress_id;
      }
    }

    public long OwnerUid
    {
      get
      {
        return this.owner_uid;
      }
    }

    public long OwnerAllianceID
    {
      get
      {
        return this.owner_alliance_id;
      }
    }

    public long MaxTroopsCount { get; set; }

    public Coordinate Location
    {
      get
      {
        Coordinate coordinate;
        coordinate.K = this.world_id;
        coordinate.X = this.map_x;
        coordinate.Y = this.map_y;
        return coordinate;
      }
    }

    public int UpdateDurationTime
    {
      get
      {
        return this.updateDurationTime;
      }
    }

    public long Durability
    {
      get
      {
        return this.durability;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public long ActiveTime
    {
      get
      {
        return this.active_time;
      }
    }

    public long CreateTime
    {
      get
      {
        return this.ctime;
      }
    }

    public long ModifyTime
    {
      get
      {
        return this.mtime;
      }
    }

    public List<long> TroopsSort
    {
      get
      {
        return this.troopsSort;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this.troops;
      }
    }

    public Dictionary<int, float> Benefits
    {
      get
      {
        return this.benefits;
      }
    }

    public string State
    {
      get
      {
        return this.state;
      }
    }

    public bool HasMarch(long marchId)
    {
      if (this.troops != null)
      {
        Dictionary<long, long>.Enumerator enumerator = this.troops.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value == marchId)
            return true;
        }
      }
      return false;
    }

    public virtual bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id) | DatabaseTools.UpdateData(inData, "fortress_id", ref this.fortress_id) | DatabaseTools.UpdateData(inData, "ctime", ref this.ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this.mtime) | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "map_x", ref this.map_x) | DatabaseTools.UpdateData(inData, "map_y", ref this.map_y) | DatabaseTools.UpdateData(inData, "durability", ref this.durability) | DatabaseTools.UpdateData(inData, "owner_uid", ref this.owner_uid) | DatabaseTools.UpdateData(inData, "name", ref this.name) | DatabaseTools.UpdateData(inData, "owner_alliance_id", ref this.owner_alliance_id) | DatabaseTools.UpdateData(inData, "state", ref this.state) | DatabaseTools.UpdateData(inData, "benefit_job_id", ref this.benefit_job_id) | DatabaseTools.UpdateData(inData, "build_job_id", ref this.build_job_id) | DatabaseTools.UpdateData(inData, "is_complete", ref this.is_complete) | DatabaseTools.UpdateData(inData, "timer_end", ref this.finish_time) | DatabaseTools.UpdateData(inData, "utime", ref this.updateDurationTime) | DatabaseTools.UpdateData(inData, "config_id", ref this.configId);
      if (inData.Contains((object) "troops_sort"))
      {
        flag = true;
        this.troopsSort.Clear();
        ArrayList arrayList = inData[(object) "troops_sort"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            long result = 0;
            if (long.TryParse(arrayList[index].ToString(), out result))
              this.troopsSort.Add(result);
          }
        }
      }
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      Hashtable hashtable1 = inData[(object) "benefits"] as Hashtable;
      if (hashtable1 != null)
      {
        this.benefits = new Dictionary<int, float>();
        IDictionaryEnumerator enumerator = hashtable1.GetEnumerator();
        while (enumerator.MoveNext())
          this.benefits.Add(int.Parse(enumerator.Key.ToString()), float.Parse(enumerator.Value.ToString()));
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public struct Params
    {
      public const string KEY = "alliance_fortress";
      public const string ALLIANCE_ID = "alliance_id";
      public const string FORTRESS_ID = "fortress_id";
      public const string OWNER_UID = "owner_uid";
      public const string OWNER_ALLIANCE_ID = "owner_alliance_id";
      public const string WORLD_ID = "world_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string DURABILITY = "durability";
      public const string NAME = "name";
      public const string STATE = "state";
      public const string ACTIVE_TIME = "active_time";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
      public const string ALLIANCE_BENEFIT = "benefits";
      public const string BENEFIT_JOB_ID = "benefit_job_id";
      public const string BUILD_JOB_ID = "build_job_id";
      public const string IS_COMPLETE = "is_complete";
      public const string TROOPS = "troops";
      public const string UPTIME = "utime";
      public const string TIME_END = "timer_end";
      public const string CONFIG_ID = "config_id";
      public const string TROOPS_SORT = "troops_sort";
    }

    public struct FortressState
    {
      public const string INVAILD = "invaild";
      public const string UNCOMPLETE = "unComplete";
      public const string BUILDING = "building";
      public const string UNDEFEND = "unDefend";
      public const string DEFENDING = "defending";
      public const string BROKEN = "broken";
      public const string REPAIRING = "repairing";
      public const string DEMOLISHING = "demolishing";
    }
  }
}
