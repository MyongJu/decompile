﻿// Decompiled with JetBrains decompiler
// Type: DB.ConsumableItemsDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ConsumableItemsDB
  {
    private Dictionary<long, ConsumableItemData> _datas = new Dictionary<long, ConsumableItemData>();
    public System.Action<int> onDataChanged;
    public System.Action<int> onDataCreated;
    public System.Action<int> onDataUpdated;
    public System.Action<int> onDataRemove;
    public System.Action<int> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ConsumableItemData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(int internalId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(internalId);
    }

    public void Publish_onDataCreated(int internalId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(internalId);
      this.Publish_onDataChanged(internalId);
    }

    public void Publish_OnDataUpdated(int internalId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(internalId);
      this.Publish_onDataChanged(internalId);
    }

    public void Publish_onDataRemove(int internalId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(internalId);
      this.Publish_onDataChanged(internalId);
    }

    public void Publish_onDataRemoved(int internalId)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(internalId);
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      ConsumableItemData consumableItemData = new ConsumableItemData();
      if (!consumableItemData.Decode((object) hashtable, updateTime) || consumableItemData.internalId == 0 || this._datas.ContainsKey((long) consumableItemData.internalId))
        return false;
      this._datas.Add((long) consumableItemData.internalId, consumableItemData);
      this.Publish_onDataCreated(consumableItemData.internalId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null || !hashtable.ContainsKey((object) "uid") || PlayerData.inst.uid != long.Parse(hashtable[(object) "uid"].ToString()))
        return false;
      int internalId = ConsumableItemData.GetInternalId((object) hashtable);
      if (internalId == 0)
        return false;
      if (!this._datas.ContainsKey((long) internalId))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) internalId].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdated(internalId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public ConsumableItemData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (ConsumableItemData) null;
    }

    public bool Remove(int internalId, long updateTime)
    {
      if (!this._datas.ContainsKey((long) internalId) || !this._datas[(long) internalId].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(internalId);
      this._datas.Remove((long) internalId);
      this.Publish_onDataRemoved(internalId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        Hashtable hashtable = arrayList[index] as Hashtable;
        if (PlayerData.inst.uid == long.Parse(hashtable[(object) "uid"].ToString()))
          this.Remove(int.Parse(hashtable[(object) "item_property_id"].ToString()), updateTime);
      }
    }

    public int GetQuantity(int internalId)
    {
      ConsumableItemData consumableItemData = this.Get(internalId);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }

    public int GetCooldownQuantity(int internalId)
    {
      ConsumableItemData consumableItemData = this.Get(internalId);
      if (consumableItemData == null)
        return 0;
      int cdCount = 0;
      consumableItemData.SellCooldownTimeList.ForEach((System.Action<KeyValuePair<int, int>>) (x => cdCount += x.Value));
      return cdCount;
    }

    public Dictionary<long, ConsumableItemData> Datas
    {
      get
      {
        return this._datas;
      }
    }

    public void ShowInfo()
    {
      using (Dictionary<long, ConsumableItemData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ConsumableItemData current = enumerator.Current;
        }
      }
    }
  }
}
