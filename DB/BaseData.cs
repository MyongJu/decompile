﻿// Decompiled with JetBrains decompiler
// Type: DB.BaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class BaseData
  {
    protected long _updateTime = -1;

    public bool CheckUpdateTime(long checkTime)
    {
      return checkTime >= this._updateTime;
    }

    public bool CheckAndResetUpdateTime(long checkTime)
    {
      if (!this.CheckUpdateTime(checkTime))
        return false;
      this._updateTime = checkTime;
      return true;
    }

    public static bool CheckAndResetUpdateTime(long checkTime, ref long updateTime)
    {
      if (checkTime < updateTime)
        return false;
      updateTime = checkTime;
      return true;
    }

    public static bool CheckAndParseOrgData(object orgData, out Hashtable data)
    {
      if (orgData == null)
      {
        data = (Hashtable) null;
        return false;
      }
      data = orgData as Hashtable;
      return data != null;
    }

    public static bool CheckAndParseOrgData(object orgData, out ArrayList data)
    {
      if (orgData == null)
      {
        data = (ArrayList) null;
        return false;
      }
      data = orgData as ArrayList;
      return data != null;
    }

    public bool CheckOrgDataAndUpdateTime(object orgData, long updateTime, out Hashtable dataHt)
    {
      dataHt = (Hashtable) null;
      if (this.CheckAndResetUpdateTime(updateTime))
        return BaseData.CheckAndParseOrgData(orgData, out dataHt);
      return false;
    }

    public bool CheckOrgDataAndUpdateTime(object orgData, long updateTime, out ArrayList dataArr)
    {
      dataArr = (ArrayList) null;
      if (this.CheckAndResetUpdateTime(updateTime))
        return BaseData.CheckAndParseOrgData(orgData, out dataArr);
      return false;
    }

    public virtual void CheckDependentData(bool isLoader, string action, Hashtable postData = null, System.Action<bool, object> callback = null)
    {
      if (this._updateTime != -1L)
        callback(true, (object) null);
      else if (isLoader)
        RequestManager.inst.SendLoader(action, postData, callback, true, false);
      else
        RequestManager.inst.SendRequest(action, postData, callback, true);
    }
  }
}
