﻿// Decompiled with JetBrains decompiler
// Type: DB.BenefitTransMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace DB
{
  public class BenefitTransMap
  {
    private Dictionary<string, string> dict_long2short;
    private Dictionary<string, string> dict_short2long;

    public BenefitTransMap()
    {
      this.Init();
    }

    public string GetLongKey(string key)
    {
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[key];
      if (dbProperty != null && !string.IsNullOrEmpty(dbProperty.ID))
        return dbProperty.ID;
      return ConfigManager.inst.DB_Properties.GetIDByShortKey(key);
    }

    public string GetShortKey(string key)
    {
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[key];
      if (dbProperty != null && !string.IsNullOrEmpty(dbProperty.ShortKey))
        return dbProperty.ShortKey;
      if (this.dict_long2short.ContainsKey(key))
        return this.dict_long2short[key];
      return key;
    }

    public void Init()
    {
      this.dict_long2short = new Dictionary<string, string>();
      this.dict_short2long = new Dictionary<string, string>();
      this.dict_long2short.Add("prop_infantry_health_percent", "ti1");
      this.dict_long2short.Add("prop_infantry_attack_percent", "ti2");
      this.dict_long2short.Add("prop_infantry_defense_percent", "ti3");
      this.dict_long2short.Add("prop_infantry_damage_increase_percent", "ti9");
      this.dict_long2short.Add("prop_infantry_damage_decrease_percent", "tia");
      this.dict_long2short.Add("prop_cavalry_health_percent", "tc1");
      this.dict_long2short.Add("prop_cavalry_attack_percent", "tc2");
      this.dict_long2short.Add("prop_cavalry_defense_percent", "tc3");
      this.dict_long2short.Add("prop_cavalry_damage_increase_percent", "tc9");
      this.dict_long2short.Add("prop_cavalry_damage_decrease_percent", "tca");
      this.dict_long2short.Add("prop_ranged_health_percent", "tr1");
      this.dict_long2short.Add("prop_ranged_attack_percent", "tr2");
      this.dict_long2short.Add("prop_ranged_defense_percent", "tr3");
      this.dict_long2short.Add("prop_ranged_damage_increase_percent", "tr9");
      this.dict_long2short.Add("prop_ranged_damage_decrease_percent", "tra");
      this.dict_long2short.Add("prop_siege_health_percent", "ts1");
      this.dict_long2short.Add("prop_siege_attack_percent", "ts2");
      this.dict_long2short.Add("prop_siege_defense_percent", "ts3");
      this.dict_long2short.Add("prop_siege_damage_increase_percent", "ts9");
      this.dict_long2short.Add("prop_siege_damage_decrease_percent", "tsa");
      this.dict_long2short.Add("prop_army_health_percent", "ta1");
      this.dict_long2short.Add("prop_army_attack_percent", "ta2");
      this.dict_long2short.Add("prop_army_defense_percent", "ta3");
      this.dict_long2short.Add("prop_army_upkeep_percent", "ta4");
      this.dict_long2short.Add("prop_army_training_time_percent", "ta5");
      this.dict_long2short.Add("prop_army_healing_time_percent", "ta6");
      this.dict_long2short.Add("prop_army_speed_percent", "ta7");
      this.dict_long2short.Add("prop_army_load_percent", "ta8");
      this.dict_long2short.Add("prop_army_damage_increase_percent", "ta9");
      this.dict_long2short.Add("prop_army_damage_decrease_percent", "taa");
      this.dict_long2short.Add("prop_defender_army_attack_percent", "tab");
      this.dict_long2short.Add("prop_defender_army_defense_percent", "tac");
      this.dict_long2short.Add("prop_defender_army_health_percent", "tad");
      this.dict_long2short.Add("prop_trap_health_percent", "tt1");
      this.dict_long2short.Add("prop_trap_attack_percent", "tt2");
      this.dict_long2short.Add("prop_trap_defense_percent", "tt3");
      this.dict_long2short.Add("prop_trap_capacity_value", "tt5");
      this.dict_long2short.Add("prop_trap_capacity_percent", "tt6");
      this.dict_long2short.Add("prop_trap_damage_increase_percent", "tt7");
      this.dict_long2short.Add("prop_trap_damage_decrease_percent", "tt8");
      this.dict_long2short.Add("prop_trap_training_time_percent", "tt9");
      this.dict_long2short.Add("prop_trap_training_capacity_base_value", "tta");
      this.dict_long2short.Add("prop_trap_training_capacity_value", "ttb");
      this.dict_long2short.Add("prop_trap_training_capacity_percent", "ttc");
      this.dict_long2short.Add("prop_food_generation_value", "rf2");
      this.dict_long2short.Add("prop_food_generation_percent", "rf3");
      this.dict_long2short.Add("prop_food_storage_value", "rf5");
      this.dict_long2short.Add("prop_food_storage_percent", "rf6");
      this.dict_long2short.Add("prop_food_gather_percent", "rf7");
      this.dict_long2short.Add("prop_wood_generation_value", "rw2");
      this.dict_long2short.Add("prop_wood_generation_percent", "rw3");
      this.dict_long2short.Add("prop_wood_storage_value", "rw5");
      this.dict_long2short.Add("prop_wood_storage_percent", "rw6");
      this.dict_long2short.Add("prop_wood_gather_percent", "rw7");
      this.dict_long2short.Add("prop_ore_generation_value", "ro2");
      this.dict_long2short.Add("prop_ore_generation_percent", "ro3");
      this.dict_long2short.Add("prop_ore_storage_value", "ro5");
      this.dict_long2short.Add("prop_ore_storage_percent", "ro6");
      this.dict_long2short.Add("prop_ore_gather_percent", "ro7");
      this.dict_long2short.Add("prop_silver_generation_value", "rs2");
      this.dict_long2short.Add("prop_silver_generation_percent", "rs3");
      this.dict_long2short.Add("prop_silver_storage_value", "rs5");
      this.dict_long2short.Add("prop_silver_storage_percent", "rs6");
      this.dict_long2short.Add("prop_silver_gather_percent", "rs7");
      this.dict_long2short.Add("prop_resource_generation_value", "rr2");
      this.dict_long2short.Add("prop_resource_generation_percent", "rr3");
      this.dict_long2short.Add("prop_resource_storage_value", "rr5");
      this.dict_long2short.Add("prop_resource_storage_percent", "rr6");
      this.dict_long2short.Add("prop_resource_gather_percent", "rr7");
      this.dict_long2short.Add("prop_army_training_capacity_value", "trn5");
      this.dict_long2short.Add("prop_army_training_capacity_percent", "trn6");
      this.dict_long2short.Add("prop_trap_capacity_base_value", "trn7");
      this.dict_long2short.Add("prop_army_training_capacity_base_value", "trn8");
      this.dict_long2short.Add("prop_train_army_cost_decrease_percent", "trn9");
      this.dict_long2short.Add("prop_storage_protect_base_value", "sto1");
      this.dict_long2short.Add("prop_storage_protect_value", "sto2");
      this.dict_long2short.Add("prop_storage_protect_percent", "sto3");
      this.dict_long2short.Add("prop_trade_capacity_base_value", "trd1");
      this.dict_long2short.Add("prop_trade_capacity_value", "trd2");
      this.dict_long2short.Add("prop_trade_capacity_percent", "trd3");
      this.dict_long2short.Add("prop_tradetax_value", "trd4");
      this.dict_long2short.Add("prop_healing_capacity_base_value", "hl1");
      this.dict_long2short.Add("prop_healing_capacity_value", "hl2");
      this.dict_long2short.Add("prop_healing_capacity_percent", "hl3");
      this.dict_long2short.Add("prop_wounded_percent", "hl4");
      this.dict_long2short.Add("prop_healing_army_cost_decrease_percent", "hl5");
      this.dict_long2short.Add("prop_march_capacity_base_value", "mar1");
      this.dict_long2short.Add("prop_march_capacity_value", "mar2");
      this.dict_long2short.Add("prop_march_capacity_percent", "mar3");
      this.dict_long2short.Add("prop_march_limit_value", "mar4");
      this.dict_long2short.Add("prop_march_limit_base_value", "mar5");
      this.dict_long2short.Add("prop_pve_speed_percent", "mar6");
      this.dict_long2short.Add("prop_city_reinforcement_capacity_base_value", "ren1");
      this.dict_long2short.Add("prop_city_reinforcement_capacity_value", "ren2");
      this.dict_long2short.Add("prop_city_reinforcement_capacity_percent", "ren3");
      this.dict_long2short.Add("prop_rally_capacity_base_value", "ral1");
      this.dict_long2short.Add("prop_rally_capacity_value", "ral2");
      this.dict_long2short.Add("prop_rally_capacity_percent", "ral3");
      this.dict_long2short.Add("prop_construction_time_percent", "con1");
      this.dict_long2short.Add("prop_research_time_percent", "res1");
      this.dict_long2short.Add("prop_construction_time_reduction_percent", "con2");
      this.dict_long2short.Add("prop_research_time_reduction_percent", "res2");
      this.dict_long2short.Add("prop_city_defense_value", "cit1");
      this.dict_long2short.Add("prop_speedup_time_value", "spd1");
      this.dict_long2short.Add("prop_alliance_member_value", "at1");
      this.dict_long2short.Add("prop_alliance_help_value", "at2");
      this.dict_long2short.Add("prop_alliance_help_max_value", "at3");
      this.dict_long2short.Add("prop_rally_slot_value", "at4");
      this.dict_long2short.Add("prop_alliance_storage_daily_limit_value", "at5");
      this.dict_long2short.Add("prop_alliance_storage_total_limit_value", "at6");
      this.dict_long2short.Add("prop_alliance_resource_gather_speed_percent", "at7");
      this.dict_long2short.Add("prop_alliance_hospital_capacity_value", "at8");
      this.dict_long2short.Add("prop_alliance_hospital_healing_speed_percent", "at9");
      this.dict_long2short.Add("prop_alliance_hospital_healing_cost_percent", "at10");
      this.dict_long2short.Add("prop_alliance_build_speed_percent", "at11");
      this.dict_long2short.Add("prop_alliance_demolished_speed_percent", "at12");
      this.dict_long2short.Add("prop_alliance_storage_daily_limit_percent", "at13");
      this.dict_long2short.Add("prop_alliance_storage_total_limit_percent", "at14");
      this.dict_long2short.Add("prop_alliance_hospital_capacity_percent", "at15");
      this.dict_long2short.Add("prop_spirit_recover_speed_percent", "hr1");
      this.dict_long2short.Add("prop_monster_spirit_cost_reduction_percent", "hr2");
      this.dict_long2short.Add("prop_dragon_light_reduce_percent", "dg1");
      this.dict_long2short.Add("prop_dragon_dark_reduce_percent", "dg2");
      this.dict_long2short.Add("prop_scout_level_value", "wt1");
      this.dict_long2short.Add("prop_antiscout_level_value", "wt2");
      this.dict_long2short.Add("prop_crafting_steel_cost_percent", "cs1");
      this.dict_long2short.Add("prop_crafting_time_percent", "ct1");
      this.dict_long2short.Add("prop_infantry_attack_infantry_damage_increase_percent", "di01");
      this.dict_long2short.Add("prop_infantry_attack_ranged_damage_increase_percent", "di02");
      this.dict_long2short.Add("prop_infantry_attack_cavalry_damage_increase_percent", "di03");
      this.dict_long2short.Add("prop_infantry_attack_siege_damage_increase_percent", "di04");
      this.dict_long2short.Add("prop_ranged_attack_infantry_damage_increase_percent", "di05");
      this.dict_long2short.Add("prop_ranged_attack_ranged_damage_increase_percent", "di06");
      this.dict_long2short.Add("prop_ranged_attack_cavalry_damage_increase_percent", "di07");
      this.dict_long2short.Add("prop_ranged_attack_siege_damage_increase_percent", "di08");
      this.dict_long2short.Add("prop_cavalry_attack_infantry_damage_increase_percent", "di09");
      this.dict_long2short.Add("prop_cavalry_attack_ranged_damage_increase_percent", "di10");
      this.dict_long2short.Add("prop_cavalry_attack_cavalry_damage_increase_percent", "di11");
      this.dict_long2short.Add("prop_cavalry_attack_siege_damage_increase_percent", "di12");
      this.dict_long2short.Add("prop_siege_attack_infantry_damage_increase_percent", "di13");
      this.dict_long2short.Add("prop_siege_attack_ranged_damage_increase_percent", "di14");
      this.dict_long2short.Add("prop_siege_attack_cavalry_damage_increase_percent", "di15");
      this.dict_long2short.Add("prop_siege_attack_siege_damage_increase_percent", "di16");
      this.dict_long2short.Add("prop_infantry_defend_infantry_damage_increase_percent", "di17");
      this.dict_long2short.Add("prop_infantry_defend_ranged_damage_increase_percent", "di18");
      this.dict_long2short.Add("prop_infantry_defend_cavalry_damage_increase_percent", "di19");
      this.dict_long2short.Add("prop_infantry_defend_siege_damage_increase_percent", "di20");
      this.dict_long2short.Add("prop_ranged_defend_infantry_damage_increase_percent", "di21");
      this.dict_long2short.Add("prop_ranged_defend_ranged_damage_increase_percent", "di22");
      this.dict_long2short.Add("prop_ranged_defend_cavalry_damage_increase_percent", "di23");
      this.dict_long2short.Add("prop_ranged_defend_siege_damage_increase_percent", "di24");
      this.dict_long2short.Add("prop_cavalry_defend_infantry_damage_increase_percent", "di25");
      this.dict_long2short.Add("prop_cavalry_defend_ranged_damage_increase_percent", "di26");
      this.dict_long2short.Add("prop_cavalry_defend_cavalry_damage_increase_percent", "di27");
      this.dict_long2short.Add("prop_cavalry_defend_siege_damage_increase_percent", "di28");
      this.dict_long2short.Add("prop_siege_defend_infantry_damage_increase_percent", "di29");
      this.dict_long2short.Add("prop_siege_defend_ranged_damage_increase_percent", "di30");
      this.dict_long2short.Add("prop_siege_defend_cavalry_damage_increase_percent", "di31");
      this.dict_long2short.Add("prop_siege_defend_siege_damage_increase_percent", "di32");
      this.dict_long2short.Add("prop_siege_distance_attack_traps_damage_increase_percent", "di33");
      this.dict_long2short.Add("prop_drop_attack_infantry_damage_increase_percent", "di34");
      this.dict_long2short.Add("prop_drop_attack_ranged_damage_increase_percent", "di35");
      this.dict_long2short.Add("prop_drop_attack_cavalry_damage_increase_percent", "di36");
      this.dict_long2short.Add("prop_stationary_attack_infantry_damage_increase_percent", "di37");
      this.dict_long2short.Add("prop_stationary_attack_ranged_damage_increase_percent", "di38");
      this.dict_long2short.Add("prop_stationary_attack_cavalry_damage_increase_percent", "di39");
      this.dict_long2short.Add("prop_targeted_attack_infantry_damage_increase_percent", "di40");
      this.dict_long2short.Add("prop_targeted_attack_ranged_damage_increase_percent", "di41");
      this.dict_long2short.Add("prop_targeted_attack_cavalry_damage_increase_percent", "di42");
      this.dict_long2short.Add("prop_dragon_knight_strength_base_value", "dk1");
      this.dict_long2short.Add("prop_dragon_knight_strength_percent", "dk2");
      this.dict_long2short.Add("prop_dragon_knight_intelligent_base_value", "dk3");
      this.dict_long2short.Add("prop_dragon_knight_intelligent_percent", "dk4");
      this.dict_long2short.Add("prop_dragon_knight_armor_base_value", "dk5");
      this.dict_long2short.Add("prop_dragon_knight_armor_percent", "dk6");
      this.dict_long2short.Add("prop_dragon_knight_constitution_base_value", "dk7");
      this.dict_long2short.Add("prop_dragon_knight_constitution_percent", "dk8");
      this.dict_long2short.Add("prop_dragon_knight_attack_base_value", "dk9");
      this.dict_long2short.Add("prop_dragon_knight_attack_percent", "dk10");
      this.dict_long2short.Add("prop_dragon_knight_magic_base_value", "dk11");
      this.dict_long2short.Add("prop_dragon_knight_magic_percent", "dk12");
      this.dict_long2short.Add("prop_dragon_knight_defence_base_value", "dk13");
      this.dict_long2short.Add("prop_dragon_knight_defence_percent", "dk14");
      this.dict_long2short.Add("prop_dragon_knight_health_base_value", "dk15");
      this.dict_long2short.Add("prop_dragon_knight_health_percent", "dk16");
      this.dict_long2short.Add("prop_dragon_knight_mana_base_value", "dk17");
      this.dict_long2short.Add("prop_dragon_knight_mana_percent", "dk18");
      this.dict_long2short.Add("prop_dragon_knight_health_recover_base_value", "dk19");
      this.dict_long2short.Add("prop_dragon_knight_health_recover_percent", "dk20");
      this.dict_long2short.Add("prop_dragon_knight_mana_recover_base_value", "dk21");
      this.dict_long2short.Add("prop_dragon_knight_mana_recover_percent", "dk22");
      this.dict_long2short.Add("prop_dragon_knight_fire_power_base_value", "dk23");
      this.dict_long2short.Add("prop_dragon_knight_fire_power_percent", "dk24");
      this.dict_long2short.Add("prop_dragon_knight_water_power_base_value", "dk25");
      this.dict_long2short.Add("prop_dragon_knight_water_power_percent", "dk26");
      this.dict_long2short.Add("prop_dragon_knight_nature_power_base_value", "dk27");
      this.dict_long2short.Add("prop_dragon_knight_nature_power_percent", "dk28");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_stamina_base_value", "dk29");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_stamina_value", "dk30");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_stamina_percent", "dk31");
      this.dict_long2short.Add("prop_dragon_knight_bag_capacity_base_value", "dk32");
      this.dict_long2short.Add("prop_dragon_knight_bag_capacity_value", "dk33");
      this.dict_long2short.Add("prop_dragon_knight_load_base_value", "dk34");
      this.dict_long2short.Add("prop_dragon_knight_load_value", "dk35");
      this.dict_long2short.Add("prop_dragon_knight_load_percent", "dk36");
      this.dict_long2short.Add("prop_dragon_knight_addition_stamina_count_base_value", "dk37");
      this.dict_long2short.Add("prop_dragon_knight_addition_stamina_count_value", "dk38");
      this.dict_long2short.Add("prop_dragon_knight_addition_mana_count_base_value", "dk39");
      this.dict_long2short.Add("prop_dragon_knight_addition_mana_count_value", "dk40");
      this.dict_long2short.Add("prop_dragon_knight_addition_health_count_base_value", "dk41");
      this.dict_long2short.Add("prop_dragon_knight_addition_health_count_value", "dk42");
      this.dict_long2short.Add("prop_dragon_knight_addition_stamina_effect_base_value", "dk43");
      this.dict_long2short.Add("prop_dragon_knight_addition_stamina_effect_value", "dk44");
      this.dict_long2short.Add("prop_dragon_knight_addition_stamina_effect_percent", "dk45");
      this.dict_long2short.Add("prop_dragon_knight_addition_mana_effect_base_value", "dk46");
      this.dict_long2short.Add("prop_dragon_knight_addition_mana_effect_value", "dk47");
      this.dict_long2short.Add("prop_dragon_knight_addition_mana_effect_percent", "dk48");
      this.dict_long2short.Add("prop_dragon_knight_addition_health_effect_base_value", "dk49");
      this.dict_long2short.Add("prop_dragon_knight_addition_health_effect_value", "dk50");
      this.dict_long2short.Add("prop_dragon_knight_addition_health_effect_percent", "dk51");
      this.dict_long2short.Add("prop_dragon_knight_skill_equip_base_value", "dk52");
      this.dict_long2short.Add("prop_dragon_knight_skill_equip_value", "dk53");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_strength_base_value", "dk54");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_strength_value", "dk55");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_strength_percent", "dk56");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_intelligent_base_value", "dk57");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_intelligent_value", "dk58");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_intelligent_percent", "dk59");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_armor_base_value", "dk60");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_armor_value", "dk61");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_armor_percent", "dk62");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_constitution_base_value", "dk63");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_constitution_value", "dk64");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_constitution_percent", "dk65");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_attack_base_value", "dk66");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_attack_value", "dk67");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_attack_percent", "dk68");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_magic_base_value", "dk69");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_magic_value", "dk70");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_magic_percent", "dk71");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_defence_base_value", "dk72");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_defence_value", "dk73");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_defence_percent", "dk74");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_fire_power_base_value", "dk75");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_fire_power_value", "dk76");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_fire_power_percent", "dk77");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_water_power_base_value", "dk78");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_water_power_value", "dk79");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_water_power_percent", "dk80");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_nature_power_base_value", "dk81");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_nature_power_value", "dk82");
      this.dict_long2short.Add("prop_dragon_knight_inbattle_nature_power_percent", "dk83");
      this.dict_long2short.Add("prop_dragon_knight_speed_percent", "dk84");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_cd_percent", "dk85");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_cd_value", "dk86");
      this.dict_long2short.Add("prop_dragon_knight_dungeon_cd_base_value", "dk87");
      this.dict_long2short.Add("prop_dragon_knight_speed_base_value", "dk88");
      this.dict_long2short.Add("prop_dragon_knight_speed_value", "dk89");
      this.dict_long2short.Add("prop_dragon_knight_rebound_base_value", "dk90");
      this.dict_long2short.Add("prop_dragon_knight_rebound_value", "dk91");
      this.dict_long2short.Add("prop_dragon_knight_rebound_percent", "dk92");
      this.dict_long2short.Add("prop_troop_formation_save_base_value", "tf1");
      this.dict_long2short.Add("prop_troop_formation_save_value", "tf2");
      this.dict_long2short.Add("prop_construct_food_cost_value", "bs01");
      this.dict_long2short.Add("prop_construct_wood_cost_value", "bs02");
      this.dict_long2short.Add("prop_construct_ore_cost_value", "bs03");
      this.dict_long2short.Add("prop_construct_silver_cost_value", "bs04");
      this.dict_long2short.Add("prop_research_food_cost_value", "bs05");
      this.dict_long2short.Add("prop_research_wood_cost_value", "bs06");
      this.dict_long2short.Add("prop_research_ore_cost_value", "bs07");
      this.dict_long2short.Add("prop_research_silver_cost_value", "bs08");
      this.dict_long2short.Add("prop_construct_food_cost_percent", "bs09");
      this.dict_long2short.Add("prop_construct_wood_cost_percent", "bs10");
      this.dict_long2short.Add("prop_construct_ore_cost_percent", "bs11");
      this.dict_long2short.Add("prop_construct_silver_cost_percent", "bs12");
      this.dict_long2short.Add("prop_research_food_cost_percent", "bs13");
      this.dict_long2short.Add("prop_research_wood_cost_percent", "bs14");
      this.dict_long2short.Add("prop_research_ore_cost_percent", "bs15");
      this.dict_long2short.Add("prop_research_silver_cost_percent", "bs16");
      Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.dict_long2short.Keys.GetEnumerator();
      while (enumerator.MoveNext())
        this.dict_short2long.Add(this.dict_long2short[enumerator.Current], enumerator.Current);
    }
  }
}
