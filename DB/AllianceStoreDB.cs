﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceStoreDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceStoreDB
  {
    private Dictionary<AllianceStoreDataKey, AllianceStoreData> _datas = new Dictionary<AllianceStoreDataKey, AllianceStoreData>();

    public event System.Action<AllianceStoreDataKey> onDataChanged;

    public event System.Action<AllianceStoreDataKey> onDataUpdate;

    public event System.Action<AllianceStoreDataKey> onDataRemove;

    public event System.Action<AllianceStoreDataKey> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceStoreData), (long) this._datas.Count);
    }

    public void Clear()
    {
      this._datas.Clear();
    }

    private void Publish_OnDataChanged(AllianceStoreDataKey dataKey)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(dataKey);
    }

    private void Publish_OnDataCreate(AllianceStoreDataKey dataKey)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(dataKey);
      this.Publish_OnDataChanged(dataKey);
    }

    private void Publish_OnDataUpdate(AllianceStoreDataKey dataKey)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(dataKey);
      this.Publish_OnDataChanged(dataKey);
    }

    private void Publish_OnDataRemove(AllianceStoreDataKey dataKey)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(dataKey);
      this.Publish_OnDataChanged(dataKey);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        AllianceStoreDataKey dataKey = new AllianceStoreDataKey();
        dataKey.Decode(arrayList[index]);
        this.Remove(dataKey, updateTime);
      }
    }

    public AllianceStoreData Get(AllianceStoreDataKey dataKey)
    {
      if (this._datas.ContainsKey(dataKey))
        return this._datas[dataKey];
      return (AllianceStoreData) null;
    }

    public List<AllianceStoreData> GetMyAllianceStoreDataList()
    {
      List<AllianceStoreData> allianceStoreDataList = new List<AllianceStoreData>();
      Dictionary<AllianceStoreDataKey, AllianceStoreData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceStoreData allianceStoreData = enumerator.Current.Value;
        if (allianceStoreData.dataKey.allianceId == PlayerData.inst.allianceId)
          allianceStoreDataList.Add(allianceStoreData);
      }
      return allianceStoreDataList;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceStoreData allianceStoreData = new AllianceStoreData();
      if (!allianceStoreData.Decode((object) hashtable, updateTime) || (!allianceStoreData.dataKey.IsValid || this._datas.ContainsKey(allianceStoreData.dataKey)))
        return false;
      this._datas.Add(allianceStoreData.dataKey, allianceStoreData);
      this.Publish_OnDataCreate(allianceStoreData.dataKey);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceStoreDataKey index = new AllianceStoreDataKey();
      index.Decode((object) hashtable);
      if (!index.IsValid)
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      return true;
    }

    private bool Remove(AllianceStoreDataKey dataKey, long updateTime)
    {
      if (!this._datas.ContainsKey(dataKey) || !this._datas[dataKey].CheckUpdateTime(updateTime))
        return false;
      this.Publish_OnDataRemove(dataKey);
      this._datas.Remove(dataKey);
      return true;
    }
  }
}
