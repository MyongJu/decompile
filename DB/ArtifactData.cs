﻿// Decompiled with JetBrains decompiler
// Type: DB.ArtifactData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ArtifactData : BaseData
  {
    protected int _worldId;
    protected int _artifactId;
    protected long _ownerUid;
    protected long _firstOwnerUid;
    protected int _shieldTime;
    protected int _cooldownTime;
    protected int _isEquipped;
    protected long _ctime;
    protected long _mtime;

    public int WorldId
    {
      get
      {
        return this._worldId;
      }
    }

    public int ArtifactId
    {
      get
      {
        return this._artifactId;
      }
    }

    public long OwnerUid
    {
      get
      {
        return this._ownerUid;
      }
    }

    public long FirstOwnerUid
    {
      get
      {
        return this._firstOwnerUid;
      }
    }

    public int ShieldTime
    {
      get
      {
        return this._shieldTime;
      }
    }

    public int CooldownTime
    {
      get
      {
        return this._cooldownTime;
      }
    }

    public bool IsEquipped
    {
      get
      {
        return this._isEquipped == 1;
      }
    }

    public long CTime
    {
      get
      {
        return this._ctime;
      }
    }

    public long MTime
    {
      get
      {
        return this._mtime;
      }
    }

    public int ProtectedLeftTime
    {
      get
      {
        return this._shieldTime - NetServerTime.inst.ServerTimestamp;
      }
    }

    public bool IsCooldown
    {
      get
      {
        return this.CooldownLeftTime > 0;
      }
    }

    public int CooldownLeftTime
    {
      get
      {
        return this._cooldownTime - NetServerTime.inst.ServerTimestamp;
      }
    }

    public KeyValuePair<int, int> ParamKey
    {
      get
      {
        return new KeyValuePair<int, int>(this._worldId, this._artifactId);
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData, "artifact_id", ref this._artifactId) | DatabaseTools.UpdateData(inData, "owner_uid", ref this._ownerUid) | DatabaseTools.UpdateData(inData, "first_owner_uid", ref this._firstOwnerUid) | DatabaseTools.UpdateData(inData, "shield_time", ref this._shieldTime) | DatabaseTools.UpdateData(inData, "cd_time", ref this._cooldownTime) | DatabaseTools.UpdateData(inData, "is_equipped", ref this._isEquipped) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
      return true;
    }

    public struct Params
    {
      public const string KEY = "artifact";
      public const string WORLD_ID = "world_id";
      public const string ARTIFACT_ID = "artifact_id";
      public const string OWNER_UID = "owner_uid";
      public const string FIRST_OWNER_UID = "first_owner_uid";
      public const string SHIELD_TIME = "shield_time";
      public const string COOLDOWN_TIME = "cd_time";
      public const string IS_EQUIPPED = "is_equipped";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
