﻿// Decompiled with JetBrains decompiler
// Type: DB.EmpireQuestData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class EmpireQuestData : BaseData
  {
    public const long INVALID_ID = 0;
    private int _questId;
    private int _questState;

    public int questID
    {
      get
      {
        return this._questId;
      }
    }

    public EmpireQuestData.QuestStatus questState
    {
      get
      {
        return (EmpireQuestData.QuestStatus) this._questState;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "quest_id", ref this._questId) | DatabaseTools.UpdateData(dataHt, "status", ref this._questState);
    }

    public static long GetQuestId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return long.Parse(data[(object) "quest_id"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Quest Id: " + (object) this.questID + ":" + (object) this.questState + "\n";
    }

    public struct Params
    {
      public const string KEY = "empire_quest";
      public const string QUSET_ID = "quest_id";
      public const string QUEST_STATE = "status";
    }

    public enum QuestStatus
    {
      STATUS_INACTIVE = 1,
      STATUS_ACTIVE = 2,
      STATUS_COMPLETED = 3,
      STATUS_COLLECTED = 4,
    }
  }
}
