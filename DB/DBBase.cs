﻿// Decompiled with JetBrains decompiler
// Type: DB.DBBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DBBase
  {
    protected Dictionary<long, BaseData> _datas = new Dictionary<long, BaseData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataCreated;

    public event System.Action<long> onDataUpdated;

    public event System.Action<long> onDataRemove;

    public event System.Action<long> onDataRemoved;

    protected void Publish_onDataChanged(long internalId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(internalId);
    }

    protected void Publish_onDataCreated(long internalId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(internalId);
      this.Publish_onDataChanged(internalId);
    }

    protected void Publish_OnDataUpdated(long internalId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(internalId);
      this.Publish_onDataChanged(internalId);
    }

    protected void Publish_onDataRemove(long internalId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(internalId);
      this.Publish_onDataChanged(internalId);
    }

    protected void Publish_onDataRemoved(long internalId)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(internalId);
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      BaseData data = this.CreateData();
      IDBDataDecoder dbDataDecoder = data as IDBDataDecoder;
      if (!dbDataDecoder.Decode((object) hashtable, updateTime) || dbDataDecoder.ID == 0L || this._datas.ContainsKey(dbDataDecoder.ID))
        return false;
      this._datas.Add(dbDataDecoder.ID, data);
      this.Publish_onDataCreated(dbDataDecoder.ID);
      return true;
    }

    protected virtual BaseData CreateData()
    {
      return (BaseData) null;
    }

    protected virtual bool NeedToFilter
    {
      get
      {
        return false;
      }
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable soure = orgData as Hashtable;
      if (soure == null)
        return false;
      if (this.IsCustomParse)
        return this.CustomParse(orgData);
      long dataId = this.GetDataId(soure);
      if (dataId == 0L)
        return false;
      if (!this._datas.ContainsKey(dataId))
        return this.Add(orgData, updateTime);
      if (!(this._datas[dataId] as IDBDataDecoder).Decode((object) soure, updateTime))
        return false;
      this.Publish_OnDataUpdated(dataId);
      return true;
    }

    protected virtual bool IsCustomParse
    {
      get
      {
        return false;
      }
    }

    protected virtual bool CustomParse(object orgData)
    {
      return false;
    }

    private long GetDataId(Hashtable soure)
    {
      if (soure != null)
        return long.Parse(soure[(object) this.GetDataKey()].ToString());
      return 0;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        if (!this.NeedToFilter || PlayerData.inst.uid == long.Parse((arrayList[index] as Hashtable)[(object) "uid"].ToString()))
          this.Update(arrayList[index], updateTime);
      }
    }

    protected BaseData GetData(long key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (BaseData) null;
    }

    public bool Remove(long internalId, long updateTime)
    {
      if (!this._datas.ContainsKey(internalId) || !this._datas[internalId].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(internalId);
      this._datas.Remove(internalId);
      this.Publish_onDataRemoved(internalId);
      return true;
    }

    protected virtual string GetDataKey()
    {
      return "id";
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        Hashtable hashtable = arrayList[index] as Hashtable;
        if (this.NeedToFilter)
        {
          if (PlayerData.inst.uid == long.Parse(hashtable[(object) "uid"].ToString()))
            this.OnRemove();
          else
            continue;
        }
        if (hashtable != null && hashtable.ContainsKey((object) this.GetDataKey()) && hashtable[(object) this.GetDataKey()] != null)
          this.Remove(long.Parse(hashtable[(object) this.GetDataKey()].ToString()), updateTime);
      }
    }

    protected virtual void OnRemove()
    {
    }
  }
}
