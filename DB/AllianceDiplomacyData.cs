﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceDiplomacyData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceDiplomacyData : BaseData
  {
    private long _allianceId;
    private long _oppAllianceId;
    private int _status;

    public long allianceId
    {
      get
      {
        return this._allianceId;
      }
    }

    public long oppAllianceId
    {
      get
      {
        return this._oppAllianceId;
      }
    }

    public int status
    {
      get
      {
        return this._status;
      }
    }

    public bool isFriends
    {
      get
      {
        return (this._status & 1) > 0;
      }
    }

    public bool isEnemy
    {
      get
      {
        return (this._status & 2) > 0;
      }
    }

    public bool isInvite
    {
      get
      {
        return (this._status & 4) > 0;
      }
    }

    public bool isInvited
    {
      get
      {
        return (this._status & 8) > 0;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "opposite_alliance_id", ref this._oppAllianceId) | DatabaseTools.UpdateData(inData, "status", ref this._status);
    }

    public static long GetAllianceID(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      long result = 0;
      long.TryParse(hashtable[(object) "alliance_id"].ToString(), out result);
      return result;
    }

    public static long GetOppAllianceID(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      long result = 0;
      long.TryParse(hashtable[(object) "opposite_alliance_id"].ToString(), out result);
      return result;
    }

    public enum Status
    {
      friends = 1,
      enemy = 2,
      invite = 4,
      invited = 8,
    }

    public struct Params
    {
      public const string KEY = "alliance_diplomacy";
      public const string ALLIANCE_DIPLOMACY_ALLIANCE_ID = "alliance_id";
      public const string ALLIANCE_DIPLOMACY_OPPOSITE_ALLIANCE_ID = "opposite_alliance_id";
      public const string ALLIANCE_DIPLOMACY_STATUS = "status";
    }
  }
}
