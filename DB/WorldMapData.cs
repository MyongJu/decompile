﻿// Decompiled with JetBrains decompiler
// Type: DB.WorldMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class WorldMapData : BaseData
  {
    public const int INVALID_ID = 0;
    private int _kingdomId;
    private string _languageGroup;
    private string _kingName;
    private string _kingAllianceName;
    private int _playerCount;
    private long _chatChannelId;
    private long _pushChannelId;
    private object miniWonder;
    protected string flag;
    protected string portrait;
    protected long protected_end_time;
    protected string name;
    protected string state;

    public int kingdomId
    {
      get
      {
        return this._kingdomId;
      }
    }

    public string languageGroup
    {
      get
      {
        return this._languageGroup;
      }
    }

    public string kingName
    {
      get
      {
        return this._kingName;
      }
    }

    public string kingAllianceName
    {
      get
      {
        return this._kingAllianceName;
      }
    }

    public int playerCount
    {
      get
      {
        return this._playerCount;
      }
    }

    public long chatChannelId
    {
      get
      {
        return this._chatChannelId;
      }
    }

    public long pushChannelId
    {
      get
      {
        return this._pushChannelId;
      }
    }

    public string FLAG
    {
      get
      {
        return this.flag;
      }
    }

    public string PORTRAIT
    {
      get
      {
        return this.portrait;
      }
    }

    public long PROTECTED_END_TIME
    {
      get
      {
        return this.protected_end_time;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string State
    {
      get
      {
        return this.state;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "k", ref this._kingdomId) | DatabaseTools.UpdateData(dataHt, "language_group", ref this._languageGroup) | DatabaseTools.UpdateData(dataHt, "king_username", ref this._kingName) | DatabaseTools.UpdateData(dataHt, "king_alliance_name", ref this._kingAllianceName) | DatabaseTools.UpdateData(dataHt, "player_count", ref this._playerCount) | DatabaseTools.UpdateData(dataHt, "chat_channel", ref this._chatChannelId) | DatabaseTools.UpdateData(dataHt, "push_channel", ref this._pushChannelId);
      if (dataHt.ContainsKey((object) "mini_wonder"))
        this.miniWonder = dataHt[(object) "mini_wonder"];
      return flag | DatabaseTools.UpdateData(dataHt, "name", ref this.name) | DatabaseTools.UpdateData(dataHt, "flag", ref this.flag) | DatabaseTools.UpdateData(dataHt, "portrait", ref this.portrait) | DatabaseTools.UpdateData(dataHt, "wonder_state", ref this.state) | DatabaseTools.UpdateData(dataHt, "protected_end_time", ref this.protected_end_time);
    }

    public static int GetKingdomId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return int.Parse(data[(object) "k"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Kingdom : " + (object) this.kingdomId + "\n" + str + "king's name : " + this.kingName + "\n" + str + "king's alliance name" + this.kingAllianceName + "\n" + str + "kingdom player count : " + (object) this.playerCount + "\n" + str + "Channle : " + (object) this.chatChannelId + "\n" + str + "Wonder" + Utils.Object2Json(this.miniWonder) + "\n";
    }

    public struct Params
    {
      public const string KEY = "world_map";
      public const string WORLD_MAP_LANGUAGE_GROUP = "language_group";
      public const string WORLD_MAP_KINGDOME_ID = "k";
      public const string WORLD_MAP_KING_NAME = "king_username";
      public const string WORLD_MAP_ALLIANCE_NAME = "king_alliance_name";
      public const string WORLD_MAP_PLAYER_COUNT = "player_count";
      public const string WORLD_MAP_MINI_WONDER = "mini_wonder";
      public const string WORLD_MAP_CHAT_CHANNEL = "chat_channel";
      public const string WORLD_MAP_PUSH_CHANNEL = "push_channel";
      public const string NAME = "name";
      public const string FLAG = "flag";
      public const string PORTRAIT = "portrait";
      public const string STATE = "wonder_state";
      public const string PROTECTED_END_TIME = "protected_end_time";
    }
  }
}
