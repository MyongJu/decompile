﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceHelpContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceHelpContainer
  {
    public Dictionary<AllianceJobID, AllianceHelpData> datas = new Dictionary<AllianceJobID, AllianceHelpData>();
    public bool accessible = true;

    public void Clear()
    {
      this.datas.Clear();
    }

    public static void UpdateDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceJobID allianceJobId = new AllianceJobID();
        allianceJobId.Decode(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceJobId.allianceId);
        if (allianceData != null)
          allianceData.helps.Update(arrayList[index], updateTime);
      }
    }

    public static void RemoveDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceJobID allianceJobId = new AllianceJobID();
        allianceJobId.Decode(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceJobId.allianceId);
        if (allianceData != null)
        {
          allianceData.helps.Remove(arrayList[index], updateTime);
          DBManager.inst.DB_Alliance.Publish_OnHelpRemoved();
        }
      }
    }

    public AllianceHelpData Get(long alliance_id, long user_id, long job_id)
    {
      AllianceJobID key = new AllianceJobID();
      key.Set(alliance_id, user_id, job_id);
      AllianceHelpData allianceHelpData = (AllianceHelpData) null;
      this.datas.TryGetValue(key, out allianceHelpData);
      return allianceHelpData;
    }

    public int GetValidHelpCount()
    {
      int num1 = 0;
      using (Dictionary<AllianceJobID, AllianceHelpData>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceHelpData current = enumerator.Current;
          int logCount = current.GetLogCount();
          int num2 = current.helpAvailable + logCount;
          if (current.identifier.uid != PlayerData.inst.uid && !current.FindMyLog(PlayerData.inst.uid) && logCount < num2)
            ++num1;
        }
      }
      return num1;
    }

    private bool Update(object orgData, long updateTime)
    {
      AllianceJobID key = new AllianceJobID();
      key.Decode(orgData);
      if (this.datas.ContainsKey(key))
        return this.datas[key].Decode(orgData, updateTime);
      AllianceHelpData allianceHelpData = new AllianceHelpData();
      if (!allianceHelpData.Decode(orgData, updateTime))
        return false;
      this.datas.Add(key, allianceHelpData);
      return true;
    }

    private bool Remove(object orgData, long updateTime)
    {
      AllianceJobID key = new AllianceJobID();
      key.Decode(orgData);
      if (!this.datas.ContainsKey(key) || !this.datas[key].CheckUpdateTime(updateTime))
        return false;
      this.datas.Remove(key);
      return true;
    }
  }
}
