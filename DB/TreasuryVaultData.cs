﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasuryVaultData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class TreasuryVaultData : BaseData
  {
    private long _id;
    private int _worldId;
    private long _allianceId;
    private long _senderUid;
    private string _type;
    private int _infoHidden;
    private int _itemId;
    private string _attachments;
    private string _status;
    private long _expireTime;
    private long _cTime;
    private long _mTime;

    public bool CanReceive
    {
      get
      {
        return this._status == "none";
      }
    }

    public long Id
    {
      get
      {
        return this._id;
      }
    }

    public int WorldId
    {
      get
      {
        return this._worldId;
      }
    }

    public long AllianceId
    {
      get
      {
        return this._allianceId;
      }
    }

    public long SenderUid
    {
      get
      {
        return this._senderUid;
      }
    }

    public string Type
    {
      get
      {
        return this._type;
      }
    }

    public int InfoHidden
    {
      get
      {
        return this._infoHidden;
      }
    }

    public int ItemId
    {
      get
      {
        return this._itemId;
      }
    }

    public string Attachments
    {
      get
      {
        return this._attachments;
      }
    }

    public string Status
    {
      get
      {
        return this._status;
      }
    }

    public long ExpireTime
    {
      get
      {
        return this._expireTime;
      }
    }

    public long CTime
    {
      get
      {
        return this._cTime;
      }
    }

    public long MTime
    {
      get
      {
        return this._mTime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "id", ref this._id) | DatabaseTools.UpdateData(inData, "world_id", ref this._worldId) | DatabaseTools.UpdateData(inData, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(inData, "sender_uid", ref this._senderUid) | DatabaseTools.UpdateData(inData, "type", ref this._type) | DatabaseTools.UpdateData(inData, "info_hidden", ref this._infoHidden) | DatabaseTools.UpdateData(inData, "item_id", ref this._itemId) | DatabaseTools.UpdateData(inData, "attachments", ref this._attachments) | DatabaseTools.UpdateData(inData, "status", ref this._status) | DatabaseTools.UpdateData(inData, "expire_time", ref this._expireTime) | DatabaseTools.UpdateData(inData, "ctime", ref this._cTime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mTime);
    }

    public struct Params
    {
      public const string KEY = "treasury_vault";
      public const string ID = "id";
      public const string WORLD_ID = "world_id";
      public const string ALLIANCE_ID = "alliance_id";
      public const string SENDER_UID = "sender_uid";
      public const string TYPE = "type";
      public const string INFO_HIDDEN = "info_hidden";
      public const string ITEM_ID = "item_id";
      public const string ATTACHMENTS = "attachments";
      public const string STATUS = "status";
      public const string EXPIRE_TIME = "expire_time";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
