﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonSkillData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DragonSkillData : BaseData
  {
    public const long INVALID_ID = 0;
    private long _uid;
    private long _ctime;
    private long _skill_id;

    public DragonSkillData(long uid, long skillID)
    {
      this.Uid = uid;
      this.SkillId = skillID;
    }

    public long Uid
    {
      get
      {
        return this._uid;
      }
      set
      {
        this._uid = value;
      }
    }

    public long Ctime
    {
      get
      {
        return this._ctime;
      }
      set
      {
        this._ctime = value;
      }
    }

    public long SkillId
    {
      get
      {
        return this._skill_id;
      }
      set
      {
        this._skill_id = value;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      DatabaseTools.UpdateData(inData, "uid", ref this._uid);
      DatabaseTools.UpdateData(inData, "skill_id", ref this._skill_id);
      return true;
    }

    public struct Params
    {
      public const string KEY = "dragon_skill";
      public const string UID = "uid";
      public const string SKILL_ID = "skill_id";
    }
  }
}
