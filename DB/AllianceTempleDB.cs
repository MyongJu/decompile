﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTempleDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceTempleDB
  {
    private Dictionary<long, AllianceTempleData> _datas = new Dictionary<long, AllianceTempleData>();

    public event System.Action<AllianceTempleData> onDataChanged;

    public event System.Action<AllianceTempleData> onDataUpdate;

    public event System.Action<AllianceTempleData> onDataCreate;

    public event System.Action<AllianceTempleData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceTempleData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(AllianceTempleData templeData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(templeData);
    }

    private void PublishOnDataCreateEvent(AllianceTempleData templeData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(templeData);
      this.PublishOnDataChangedEvent(templeData);
    }

    private void PublishOnDataUpdateEvent(AllianceTempleData templeData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(templeData);
      this.PublishOnDataChangedEvent(templeData);
    }

    private void PublishOnDataRemovedEvent(AllianceTempleData templeData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(templeData);
      this.PublishOnDataChangedEvent(templeData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceTempleData templeData = new AllianceTempleData();
      if (!templeData.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(templeData.templeId))
        return false;
      this._datas.Add(templeData.templeId, templeData);
      this.PublishOnDataCreateEvent(templeData);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "building_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceTempleData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long templeId, long updateTime)
    {
      if (!this._datas.ContainsKey(templeId))
        return false;
      AllianceTempleData data = this._datas[templeId];
      this._datas.Remove(templeId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "building_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AllianceTempleData GetMyAllianceTempleData()
    {
      if (this._datas.Count > 0)
      {
        using (Dictionary<long, AllianceTempleData>.Enumerator enumerator = this._datas.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<long, AllianceTempleData> current = enumerator.Current;
            if (current.Value.allianceId == PlayerData.inst.allianceId)
              return current.Value;
          }
        }
      }
      return (AllianceTempleData) null;
    }

    public AllianceTempleData Get(int x, int y)
    {
      Dictionary<long, AllianceTempleData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Location.X == x && enumerator.Current.Location.Y == y)
          return enumerator.Current;
      }
      return (AllianceTempleData) null;
    }

    public AllianceTempleData Get(long templeId)
    {
      AllianceTempleData allianceTempleData;
      this._datas.TryGetValue(templeId, out allianceTempleData);
      return allianceTempleData;
    }

    public AllianceTempleData GetDataByCoordinate(Coordinate location)
    {
      Dictionary<long, AllianceTempleData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Location.K == location.K && enumerator.Current.Location.X == location.X && enumerator.Current.Location.Y == location.Y)
          return enumerator.Current;
      }
      return (AllianceTempleData) null;
    }

    public AllianceTempleData GetByMarchId(long marchId)
    {
      Dictionary<long, AllianceTempleData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceTempleData allianceTempleData = enumerator.Current.Value;
        if (allianceTempleData.HasMarch(marchId))
          return allianceTempleData;
      }
      return (AllianceTempleData) null;
    }

    public AllianceTempleData GetAllianceTempleDataByBuildingConfigId(int allianceBuildingConfigId)
    {
      using (Dictionary<long, AllianceTempleData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceTempleData> current = enumerator.Current;
          if (current.Value.allianceId == PlayerData.inst.allianceId && current.Value.ConfigId == allianceBuildingConfigId)
            return current.Value;
        }
      }
      return (AllianceTempleData) null;
    }
  }
}
