﻿// Decompiled with JetBrains decompiler
// Type: DB.SevenDaysData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class SevenDaysData : BaseData, IDBDataDecoder
  {
    private Dictionary<int, int> _items = new Dictionary<int, int>();
    private int _status;
    private int _configId;
    private int _days;
    private long _startTime;
    private long _ctime;
    private long _mtime;

    public bool IsExpired
    {
      get
      {
        return (int) (this._startTime + 86400L) <= NetServerTime.inst.ServerTimestamp;
      }
    }

    public bool IsFinish
    {
      get
      {
        if (this.IsStart && !this.IsExpired)
          return !this.IsClaim;
        return false;
      }
    }

    public bool IsStart
    {
      get
      {
        return (int) this._startTime <= NetServerTime.inst.ServerTimestamp;
      }
    }

    public int RemainTime
    {
      get
      {
        return 86400 - (NetServerTime.inst.ServerTimestamp - (int) this._startTime);
      }
    }

    public long ID
    {
      get
      {
        return (long) this._configId;
      }
    }

    public int ConfigId
    {
      get
      {
        return this._configId;
      }
    }

    public int Days
    {
      get
      {
        return this._days;
      }
    }

    public long StartTime
    {
      get
      {
        return this._startTime;
      }
    }

    public bool IsClaim
    {
      get
      {
        return this._status == SevenDaysData.RewardStatus.CLAIM;
      }
    }

    public int Status
    {
      get
      {
        return this._status;
      }
    }

    public int GetHadBuyCount(int itemId)
    {
      if (this._items.ContainsKey(itemId))
        return this._items[itemId];
      return 0;
    }

    public int GetBoughtCount(int itemId)
    {
      if (this._items.ContainsKey(itemId))
        return this._items[itemId];
      return 0;
    }

    public bool Decode(object orgData, long updateTime)
    {
      bool flag1 = false;
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "config_id", ref this._configId) | DatabaseTools.UpdateData(inData, "rewards_status", ref this._status) | DatabaseTools.UpdateData(inData, "days", ref this._days) | DatabaseTools.UpdateData(inData, "start_time", ref this._startTime) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
      if (inData.ContainsKey((object) "items") && inData[(object) "items"] != null)
      {
        this._items.Clear();
        Hashtable hashtable = inData[(object) "items"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (enumerator.Current != null)
            {
              string s1 = enumerator.Current.ToString();
              if (hashtable[enumerator.Current] != null)
              {
                int result1 = 0;
                int.TryParse(s1, out result1);
                if (result1 > 0)
                {
                  string s2 = hashtable[enumerator.Current].ToString();
                  int result2 = 0;
                  int.TryParse(s2, out result2);
                  if (result2 > 0 && !this._items.ContainsKey(result1))
                  {
                    this._items.Add(result1, result2);
                  }
                  else
                  {
                    Dictionary<int, int> items;
                    int index;
                    (items = this._items)[index = result1] = items[index] + result2;
                  }
                }
              }
            }
          }
        }
      }
      return flag2;
    }

    public struct Params
    {
      public const string KEY = "seven_days";
      public const string CONFIG_ID = "config_id";
      public const string DAYS = "days";
      public const string START_TIME = "start_time";
      public const string REWARDS_STATUES = "rewards_status";
      public const string ITEMS = "items";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }

    public struct RewardStatus
    {
      public static int CLAIM = 1;
    }
  }
}
