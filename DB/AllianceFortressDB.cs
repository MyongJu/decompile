﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceFortressDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceFortressDB
  {
    private Dictionary<long, AllianceFortressData> _datas = new Dictionary<long, AllianceFortressData>();

    public event System.Action<AllianceFortressData> onDataChanged;

    public event System.Action<AllianceFortressData> onDataUpdate;

    public event System.Action<AllianceFortressData> onDataCreate;

    public event System.Action<AllianceFortressData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceFortressData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(AllianceFortressData fortress)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(fortress);
    }

    private void PublishOnDataCreateEvent(AllianceFortressData fortress)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(fortress);
      this.PublishOnDataChangedEvent(fortress);
    }

    private void PublishOnDataUpdateEvent(AllianceFortressData fortress)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(fortress);
      this.PublishOnDataChangedEvent(fortress);
    }

    private void PublishOnDataRemovedEvent(AllianceFortressData fortress)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(fortress);
      this.PublishOnDataChangedEvent(fortress);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceFortressData fortress = new AllianceFortressData();
      if (!fortress.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(fortress.fortressId))
        return false;
      this._datas.Add(fortress.fortressId, fortress);
      this.PublishOnDataCreateEvent(fortress);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "fortress_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceFortressData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long fortressId, long updateTime)
    {
      if (!this._datas.ContainsKey(fortressId))
        return false;
      AllianceFortressData data = this._datas[fortressId];
      this._datas.Remove(fortressId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "fortress_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public void DelMyFortressData(long fortressId)
    {
      AllianceFortressData dataByBuildingId = this.GetMyFortressDataByBuildingId(fortressId);
      if (dataByBuildingId == null)
        return;
      this.Remove(dataByBuildingId.fortressId, (long) NetServerTime.inst.ServerTimestamp);
    }

    public AllianceFortressData GetMyFortressDataByBuildingId(long buildingId)
    {
      if (this._datas.Count > 0)
      {
        using (Dictionary<long, AllianceFortressData>.Enumerator enumerator = this._datas.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<long, AllianceFortressData> current = enumerator.Current;
            if (current.Key == buildingId && current.Value.allianceId == PlayerData.inst.allianceId)
              return current.Value;
          }
        }
      }
      return (AllianceFortressData) null;
    }

    public AllianceFortressData GetMyFortressDataByBuildingConfigId(int buildingConfigId)
    {
      if (this._datas.Count > 0)
      {
        Dictionary<long, AllianceFortressData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.allianceId == PlayerData.inst.allianceId && enumerator.Current.ConfigId == buildingConfigId)
            return enumerator.Current;
        }
      }
      return (AllianceFortressData) null;
    }

    public AllianceFortressData Get(int x, int y)
    {
      Dictionary<long, AllianceFortressData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Location.X == x && enumerator.Current.Location.Y == y)
          return enumerator.Current;
      }
      return (AllianceFortressData) null;
    }

    public AllianceFortressData Get(long fortressId)
    {
      AllianceFortressData allianceFortressData;
      this._datas.TryGetValue(fortressId, out allianceFortressData);
      return allianceFortressData;
    }

    public AllianceFortressData GetDataByCoordinate(Coordinate location)
    {
      Dictionary<long, AllianceFortressData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Location.K == location.K && enumerator.Current.Location.X == location.X && enumerator.Current.Location.Y == location.Y)
          return enumerator.Current;
      }
      return (AllianceFortressData) null;
    }

    public List<AllianceFortressData> GetByAllianceId(long allianceId)
    {
      List<AllianceFortressData> allianceFortressDataList = new List<AllianceFortressData>();
      Dictionary<long, AllianceFortressData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceFortressData allianceFortressData = enumerator.Current.Value;
        if (allianceFortressData.allianceId == allianceId)
          allianceFortressDataList.Add(allianceFortressData);
      }
      return allianceFortressDataList;
    }

    public AllianceFortressData GetByMarchId(long marchId)
    {
      Dictionary<long, AllianceFortressData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        AllianceFortressData allianceFortressData = enumerator.Current.Value;
        if (allianceFortressData.HasMarch(marchId))
          return allianceFortressData;
      }
      return (AllianceFortressData) null;
    }

    public AllianceFortressData GetMyMainFortress()
    {
      using (Dictionary<long, AllianceFortressData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceFortressData> current = enumerator.Current;
          if (current.Value.allianceId == PlayerData.inst.allianceId)
          {
            AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(current.Value.ConfigId);
            if (buildingStaticInfo != null && buildingStaticInfo.type == 0)
              return current.Value;
          }
        }
      }
      return (AllianceFortressData) null;
    }
  }
}
