﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightDungeonData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightDungeonData : BaseData
  {
    private Dictionary<int, DragonKnightDungeonData.BagInfo> _bagData = new Dictionary<int, DragonKnightDungeonData.BagInfo>();
    private Dictionary<int, DragonKnightDungeonData.RaidInfo> _raidData = new Dictionary<int, DragonKnightDungeonData.RaidInfo>();
    private Dictionary<int, DragonKnightDungeonData.BagInfo> _temporaryItems = new Dictionary<int, DragonKnightDungeonData.BagInfo>();
    private long _uid;
    private long _raidJobId;
    private int _currentLevel;
    private int _levelId;
    private int _currentGrid;
    private int _previousGrid;
    private object _maze;
    protected DragonKnightAssignedSkill _assignedSkill;
    private object _attribute;
    private object _buffBenefits;
    private object _dungeonBuff;
    private object _battleBuff;
    private long _forceQuitJobId;
    private long _ctime;
    private long _mtime;

    public void InitWithArenaData(long userId, ArrayList allSkill, Hashtable talent, int levelInternalId = 20000002)
    {
      int num1 = 4;
      int num2 = 4;
      this._levelId = levelInternalId;
      this._currentLevel = 1;
      this._previousGrid = 0;
      this._currentGrid = 0;
      Hashtable hashtable1 = new Hashtable();
      this._maze = (object) hashtable1;
      hashtable1.Add((object) "width", (object) num1);
      hashtable1.Add((object) "height", (object) num2);
      hashtable1.Add((object) "start", (object) 0);
      hashtable1.Add((object) "end", (object) 1);
      ArrayList arrayList1 = new ArrayList();
      hashtable1.Add((object) "grid", (object) arrayList1);
      ArrayList arrayList2 = new ArrayList();
      hashtable1.Add((object) "crossed", (object) arrayList2);
      for (int index1 = 0; index1 < num1; ++index1)
      {
        for (int index2 = 0; index2 < num2; ++index2)
          arrayList1.Add((object) new Hashtable()
          {
            {
              (object) "access",
              (object) 0
            },
            {
              (object) "type",
              (object) string.Empty
            }
          });
      }
      Hashtable hashtable2 = arrayList1[0] as Hashtable;
      hashtable2[(object) "type"] = (object) "arena";
      hashtable2.Add((object) "id", (object) userId);
      hashtable2.Add((object) "skill", (object) allSkill);
      hashtable2.Add((object) nameof (talent), (object) talent);
    }

    public long UserId
    {
      get
      {
        return this._uid;
      }
    }

    public long RaidJobID
    {
      get
      {
        return this._raidJobId;
      }
    }

    public int CurrentLevel
    {
      get
      {
        return this._currentLevel;
      }
    }

    public int LevelId
    {
      get
      {
        return this._levelId;
      }
    }

    public int CurrentGrid
    {
      get
      {
        return this._currentGrid;
      }
    }

    public int PreviousGrid
    {
      get
      {
        return this._previousGrid;
      }
    }

    public object Maze
    {
      get
      {
        return this._maze;
      }
    }

    public DragonKnightAssignedSkill AssignedSkill
    {
      get
      {
        return this._assignedSkill;
      }
    }

    public object Attribute
    {
      get
      {
        return this._attribute;
      }
    }

    public int GetAttribute(string type)
    {
      Hashtable attribute = this._attribute as Hashtable;
      int result = 0;
      if (attribute != null && attribute.ContainsKey((object) type))
        int.TryParse(attribute[(object) type].ToString(), out result);
      return result;
    }

    public Dictionary<int, DragonKnightDungeonData.BagInfo> BagData
    {
      get
      {
        return this._bagData;
      }
    }

    public Dictionary<int, DragonKnightDungeonData.RaidInfo> RaidData
    {
      get
      {
        return this._raidData;
      }
    }

    public List<DragonKnightDungeonData.RaidInfo> RaidInfoList
    {
      get
      {
        List<DragonKnightDungeonData.RaidInfo> raidInfoList = new List<DragonKnightDungeonData.RaidInfo>((IEnumerable<DragonKnightDungeonData.RaidInfo>) this._raidData.Values);
        raidInfoList.Sort((Comparison<DragonKnightDungeonData.RaidInfo>) ((x, y) => x.level - y.level));
        return raidInfoList;
      }
    }

    public Dictionary<int, DragonKnightDungeonData.BagInfo> TemporaryItems
    {
      get
      {
        return this._temporaryItems;
      }
    }

    public object BuffBenefits
    {
      get
      {
        return this._buffBenefits;
      }
    }

    public object DungeonBuff
    {
      get
      {
        return this._dungeonBuff;
      }
    }

    public object BattleBuff
    {
      get
      {
        return this._battleBuff;
      }
    }

    public long ForceQuitJobId
    {
      get
      {
        return this._forceQuitJobId;
      }
    }

    public long CTime
    {
      get
      {
        return this._ctime;
      }
    }

    public long MTime
    {
      get
      {
        return this._mtime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "raid_job_id", ref this._raidJobId) | DatabaseTools.UpdateData(inData, "current_level", ref this._currentLevel) | DatabaseTools.UpdateData(inData, "current_grid", ref this._currentGrid) | DatabaseTools.UpdateData(inData, "previous_grid", ref this._previousGrid) | DatabaseTools.UpdateData(inData, "maze", ref this._maze) | DatabaseTools.UpdateData(inData, "dk_attribute", ref this._attribute) | this.DecodeBagInfo(inData[(object) "bag_info"]) | this.DecodeRaidInfo(inData[(object) "raid_info"]) | this.DecodeTemporaryItems(inData[(object) "dk_items"] as Hashtable) | DatabaseTools.UpdateData(inData, "buff_benefits", ref this._buffBenefits) | DatabaseTools.UpdateData(inData, "dungeon_buff", ref this._dungeonBuff) | DatabaseTools.UpdateData(inData, "battle_buff", ref this._battleBuff) | DatabaseTools.UpdateData(inData, "force_quit_job_id", ref this._forceQuitJobId) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
      Hashtable maze = this._maze as Hashtable;
      if (maze != null && maze.ContainsKey((object) "internal_id"))
        int.TryParse(maze[(object) "internal_id"].ToString(), out this._levelId);
      if (inData.ContainsKey((object) "skill_slot"))
      {
        this._assignedSkill = new DragonKnightAssignedSkill();
        this._assignedSkill.Decode(inData[(object) "skill_slot"]);
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      if (!inData.ContainsKey((object) "dk_items"))
        ;
      return flag;
    }

    public bool IsSkillAssigned(int talentSkillId)
    {
      if (this._assignedSkill != null)
      {
        Dictionary<int, int>.KeyCollection.Enumerator enumerator = this._assignedSkill.talentSkills.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (this._assignedSkill.talentSkills[enumerator.Current] == talentSkillId)
            return true;
        }
      }
      return false;
    }

    private bool DecodeBagInfo(object items)
    {
      ArrayList arrayList = items as ArrayList;
      if (arrayList != null && arrayList.Count == 0)
      {
        this._bagData = new Dictionary<int, DragonKnightDungeonData.BagInfo>();
        return true;
      }
      Hashtable hashtable = items as Hashtable;
      if (hashtable == null)
        return false;
      this._bagData = new Dictionary<int, DragonKnightDungeonData.BagInfo>();
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      while (enumerator.MoveNext())
      {
        Hashtable inData = enumerator.Value as Hashtable;
        DragonKnightDungeonData.BagInfo bagInfo = new DragonKnightDungeonData.BagInfo();
        DatabaseTools.UpdateData(inData, "id", ref bagInfo.itemId);
        DatabaseTools.UpdateData(inData, "count", ref bagInfo.count);
        this._bagData.Add(int.Parse(enumerator.Key.ToString()), bagInfo);
      }
      return true;
    }

    private bool DecodeRaidInfo(object records)
    {
      this._raidData.Clear();
      Hashtable hashtable = records as Hashtable;
      if (hashtable == null)
        return false;
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result = 0;
        if (int.TryParse(enumerator.Key.ToString(), out result))
        {
          DragonKnightDungeonData.RaidInfo raidInfo = new DragonKnightDungeonData.RaidInfo();
          raidInfo.level = result;
          raidInfo.Decode(enumerator.Value);
          this._raidData.Add(result, raidInfo);
        }
      }
      return true;
    }

    private bool DecodeTemporaryItems(Hashtable items)
    {
      if (items == null)
        return false;
      this._temporaryItems = new Dictionary<int, DragonKnightDungeonData.BagInfo>();
      IDictionaryEnumerator enumerator = items.GetEnumerator();
      while (enumerator.MoveNext())
      {
        DragonKnightDungeonData.BagInfo bagInfo = new DragonKnightDungeonData.BagInfo();
        bagInfo.itemId = int.Parse(enumerator.Key.ToString());
        bagInfo.count = int.Parse(enumerator.Value.ToString());
        this._temporaryItems.Add(bagInfo.itemId, bagInfo);
      }
      return true;
    }

    public DragonKnightDungeonData.BagInfo GetBagInfoByIndex(int index)
    {
      DragonKnightDungeonData.BagInfo bagInfo;
      this._bagData.TryGetValue(index, out bagInfo);
      return bagInfo;
    }

    public DragonKnightDungeonData.BagInfo GetTemporaryItemByInternalID(int internalId)
    {
      DragonKnightDungeonData.BagInfo bagInfo;
      this._temporaryItems.TryGetValue(internalId, out bagInfo);
      return bagInfo;
    }

    public int CurrentBagCapacity
    {
      get
      {
        int num = 0;
        using (Dictionary<int, DragonKnightDungeonData.BagInfo>.Enumerator enumerator = this._bagData.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            DragonKnightDungeonData.BagInfo bagInfo = enumerator.Current.Value;
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(bagInfo.itemId);
            num += itemStaticInfo.DungeonWeight * bagInfo.count;
          }
        }
        return num;
      }
    }

    public int CurrentBagCount
    {
      get
      {
        return this._bagData.Count;
      }
    }

    public bool HasSlotFor(int itemId)
    {
      if (this._bagData.ContainsKey(itemId))
        return true;
      return PlayerData.inst.dragonKnightData.BagAmount - this._bagData.Count > 0;
    }

    public struct Params
    {
      public const string KEY = "dragon_knight_dungeon";
      public const string USER_ID = "uid";
      public const string RAID_JOB_ID = "raid_job_id";
      public const string CURRENT_LEVEL = "current_level";
      public const string CURRENT_GRID = "current_grid";
      public const string PREVIOUS_GRID = "previous_grid";
      public const string MAZE = "maze";
      public const string SKILL_SLOT = "skill_slot";
      public const string FORCE_QUIT_JOB_ID = "force_quit_job_id";
      public const string DK_ATTRIBUTE = "dk_attribute";
      public const string DK_ITEMS = "dk_items";
      public const string BAG_INFO = "bag_info";
      public const string RAID_INFO = "raid_info";
      public const string BUFF_BENEFITS = "buff_benefits";
      public const string DUNGEON_BUFF = "dungeon_buff";
      public const string BATTLE_BUFF = "battle_buff";
      public const string LEVEL_INTERNAL_ID = "internal_id";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }

    public class BagInfo
    {
      public int itemId;
      public int count;
    }

    public class RaidInfo
    {
      public Dictionary<int, int> reward = new Dictionary<int, int>();
      public int level;
      public string pvp_time;

      public void Decode(object data)
      {
        Hashtable inData = data as Hashtable;
        if (inData == null)
          return;
        Hashtable hashtable = inData[(object) "reward"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            if (int.TryParse(enumerator.Key.ToString(), out result1))
            {
              int result2 = 0;
              int.TryParse(enumerator.Value.ToString(), out result2);
              this.reward.Add(result1, result2);
            }
          }
        }
        DatabaseTools.UpdateData(inData, "pvp_time", ref this.pvp_time);
      }
    }
  }
}
