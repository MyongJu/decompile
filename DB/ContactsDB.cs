﻿// Decompiled with JetBrains decompiler
// Type: DB.ContactsDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ContactsDB
  {
    private Dictionary<long, ContactsData> _datas = new Dictionary<long, ContactsData>();
    public List<long> friends = new List<long>();
    public List<long> inviting = new List<long>();
    public List<long> beInvited = new List<long>();
    public List<long> blocking = new List<long>();
    public List<long> beBlocked = new List<long>();
    public List<long> tempBlocked = new List<long>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ContactData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      ContactsData contact = new ContactsData();
      if (!contact.Decode((object) hashtable, updateTime) || contact.contactUid == 0L || this._datas.ContainsKey(contact.contactUid))
        return false;
      this._datas.Add(contact.contactUid, contact);
      this.UpdateStatus(contact);
      this.Publish_onDataCreated(contact.contactUid);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data) || PlayerData.inst.uid != 0L && ContactsData.GetUid(orgData) != PlayerData.inst.uid)
        return false;
      long contactUid = ContactsData.GetContactUid((object) data);
      if (contactUid == 0L)
        return false;
      if (!this._datas.ContainsKey(contactUid))
        return this.Add(orgData, updateTime);
      if (!this._datas[contactUid].Decode((object) data, updateTime))
        return false;
      this.UpdateStatus(this._datas[contactUid]);
      this.Publish_onDataUpdated(contactUid);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public ContactsData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (ContactsData) null;
    }

    public bool Remove(long contactUid, long updateTime)
    {
      if (!this._datas.ContainsKey(contactUid) || !this._datas[contactUid].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(contactUid);
      this.RemoveStatus(contactUid);
      this._datas.Remove(contactUid);
      this.Publish_onDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(ContactsData.GetContactUid(arrayList[index]), updateTime);
    }

    private void UpdateStatus(ContactsData contact)
    {
      this.UpdateStatusList(contact, this.friends, ContactsData.ContactStatus.STATUS_FRIEND);
      this.UpdateStatusList(contact, this.blocking, ContactsData.ContactStatus.STATUS_BLOCKING);
      this.UpdateStatusList(contact, this.beBlocked, ContactsData.ContactStatus.STATUS_BE_BLOCKED);
      this.UpdateStatusList(contact, this.inviting, ContactsData.ContactStatus.STATUS_INVITING);
      this.UpdateStatusList(contact, this.beInvited, ContactsData.ContactStatus.STATUS_BE_INVITED);
      this.UpdateStatusList(contact, this.tempBlocked, ContactsData.ContactStatus.STATUS_TEMP_BLOCKED);
    }

    private void RemoveStatus(long uid)
    {
      this.RemoveStatusList(uid, this.friends);
      this.RemoveStatusList(uid, this.blocking);
      this.RemoveStatusList(uid, this.beBlocked);
      this.RemoveStatusList(uid, this.inviting);
      this.RemoveStatusList(uid, this.beInvited);
      this.RemoveStatusList(uid, this.tempBlocked);
    }

    private void UpdateStatusList(ContactsData contact, List<long> list, ContactsData.ContactStatus baseStatus)
    {
      if ((contact.status & baseStatus) > (ContactsData.ContactStatus) 0)
      {
        if (list.Contains(contact.contactUid))
          return;
        list.Add(contact.contactUid);
      }
      else
      {
        if (!list.Contains(contact.contactUid))
          return;
        list.Remove(contact.contactUid);
      }
    }

    private void RemoveStatusList(long uid, List<long> list)
    {
      if (!list.Contains(uid))
        return;
      list.Remove(uid);
    }

    public void ShowInfo()
    {
      using (Dictionary<long, ContactsData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ContactsData current = enumerator.Current;
        }
      }
    }
  }
}
