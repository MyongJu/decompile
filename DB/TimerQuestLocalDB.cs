﻿// Decompiled with JetBrains decompiler
// Type: DB.TimerQuestLocalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TimerQuestLocalDB
  {
    private Dictionary<long, TimerQuestData> _datas = new Dictionary<long, TimerQuestData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;
    public System.Action onDataSetChanged;

    public Dictionary<long, TimerQuestData>.ValueCollection.Enumerator values
    {
      get
      {
        return this._datas.Values.GetEnumerator();
      }
    }

    public TimerQuestData Get(long questId)
    {
      if (this._datas.ContainsKey(questId))
        return this._datas[questId];
      return (TimerQuestData) null;
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (TimerQuestData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataSetChanged()
    {
      if (this.onDataSetChanged == null)
        return;
      this.onDataSetChanged();
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      TimerQuestData timerQuestData = new TimerQuestData();
      if (!timerQuestData.Decode((object) hashtable, updateTime) || timerQuestData.questId == 0L || this._datas.ContainsKey(timerQuestData.questId))
        return false;
      this._datas.Add(timerQuestData.questId, timerQuestData);
      this.Publish_onDataCreated(timerQuestData.questId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      int questId = TimerQuestData.GetQuestId((object) hashtable);
      if ((long) questId == 0L)
        return false;
      if (!this._datas.ContainsKey((long) questId))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) questId].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated((long) questId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
      this.Publish_onDataSetChanged();
    }

    public bool Remove(long contactUid, long updateTime)
    {
      if (!this._datas.ContainsKey(contactUid) || !this._datas[contactUid].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(contactUid);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove((long) TimerQuestData.GetQuestId(arrayList[index]), updateTime);
      this.Publish_onDataRemoved();
      this.Publish_onDataSetChanged();
    }

    public List<long> GetTotalQuests()
    {
      List<long> longList = new List<long>();
      Dictionary<long, TimerQuestData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long key = enumerator.Current.Key;
        longList.Add(key);
      }
      longList.Sort();
      return longList;
    }

    public int GetCompleteCount()
    {
      int num = 0;
      Dictionary<long, TimerQuestData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.status == TimerQuestData.QuestStatus.STATUS_COMPLETED)
          ++num;
      }
      return num;
    }

    public void CreateFakeData(int questId, TimerQuestData.QuestStatus status = TimerQuestData.QuestStatus.STATUS_ACTIVE)
    {
      TimerQuestData timerQuestData = new TimerQuestData();
      timerQuestData.questId = (long) questId;
      timerQuestData.jobId = -1L;
      timerQuestData.status = status;
      if (this._datas.ContainsKey((long) questId))
        return;
      this._datas.Add((long) questId, timerQuestData);
    }

    public void ShowInfo()
    {
    }
  }
}
