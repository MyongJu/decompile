﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceHospitalHealingData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceHospitalHealingData
  {
    public long total;
    public long healing;

    public void Decode(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      DatabaseTools.UpdateData(inData, "total", ref this.total);
      DatabaseTools.UpdateData(inData, "healing", ref this.healing);
    }
  }
}
