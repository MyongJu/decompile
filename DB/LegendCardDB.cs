﻿// Decompiled with JetBrains decompiler
// Type: DB.LegendCardDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class LegendCardDB
  {
    protected Dictionary<KeyValuePair<long, int>, LegendCardData> _tableData = new Dictionary<KeyValuePair<long, int>, LegendCardData>();

    public event System.Action<LegendCardData> onDataChanged;

    public event System.Action<LegendCardData> onDataUpdate;

    public event System.Action<LegendCardData> onDataRemove;

    public event System.Action<LegendCardData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (LegendCardData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(LegendCardData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(LegendCardData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(LegendCardData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(LegendCardData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(KeyValuePair<long, int> key, long updateTime)
    {
      if (!this._tableData.ContainsKey(key))
        return false;
      LegendCardData data = this._tableData[key];
      this._tableData.Remove(key);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData1 = 0;
      int outData2 = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "uid", ref outData1);
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "legend_id", ref outData2);
        this.Remove(new KeyValuePair<long, int>(outData1, outData2), updateTime);
      }
    }

    public LegendCardData Get(long uid, int legendId)
    {
      KeyValuePair<long, int> key = new KeyValuePair<long, int>(uid, legendId);
      if (this._tableData.ContainsKey(key))
        return this._tableData[key];
      return (LegendCardData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      LegendCardData data = new LegendCardData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.ParamKey))
        return false;
      this._tableData.Add(data.ParamKey, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData1 = 0;
      int outData2 = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData1);
      DatabaseTools.UpdateData(inData, "legend_id", ref outData2);
      KeyValuePair<long, int> key = new KeyValuePair<long, int>(outData1, outData2);
      if (!this._tableData.ContainsKey(key))
        return this.Add(orgData, updateTime);
      LegendCardData data = this._tableData[key];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    private int SortById(LegendCardData data1, LegendCardData data2)
    {
      ParliamentHeroInfo parliamentHeroInfo1 = ConfigManager.inst.DB_ParliamentHero.Get(data1.LegendId);
      ParliamentHeroInfo parliamentHeroInfo2 = ConfigManager.inst.DB_ParliamentHero.Get(data2.LegendId);
      if (data2.PositionAssigned.CompareTo(data1.PositionAssigned) != 0)
        return data2.PositionAssigned.CompareTo(data1.PositionAssigned);
      if (parliamentHeroInfo1 == null || parliamentHeroInfo2 == null)
        return 1;
      if (parliamentHeroInfo2.quality.CompareTo(parliamentHeroInfo1.quality) != 0)
        return parliamentHeroInfo2.quality.CompareTo(parliamentHeroInfo1.quality);
      if (data2.Level.CompareTo(data1.Level) != 0)
        return data2.Level.CompareTo(data1.Level);
      if (data2.Star.CompareTo(data1.Star) != 0)
        return data2.Star.CompareTo(data1.Star);
      return parliamentHeroInfo2.internalId.CompareTo(parliamentHeroInfo1.internalId);
    }

    public List<LegendCardData> GetLegendCardDataListByUid(long uid)
    {
      List<LegendCardData> legendCardDataList = new List<LegendCardData>();
      using (Dictionary<KeyValuePair<long, int>, LegendCardData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<long, int>, LegendCardData> current = enumerator.Current;
          if (current.Value.Uid == uid)
            legendCardDataList.Add(current.Value);
        }
      }
      legendCardDataList.Sort((Comparison<LegendCardData>) ((a, b) => a.LegendId.CompareTo(b.LegendId)));
      legendCardDataList.Sort(new Comparison<LegendCardData>(this.SortById));
      return legendCardDataList;
    }

    public LegendCardData GetLegendCardData(long uid, int legendId)
    {
      using (Dictionary<KeyValuePair<long, int>, LegendCardData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<long, int>, LegendCardData> current = enumerator.Current;
          if (current.Value.Uid == uid && current.Value.LegendId == legendId)
            return current.Value;
        }
      }
      return (LegendCardData) null;
    }

    public List<LegendCardData> GetLegendListByPosition(int position)
    {
      List<LegendCardData> legendCardDataList = new List<LegendCardData>();
      long uid = PlayerData.inst.uid;
      using (Dictionary<KeyValuePair<long, int>, LegendCardData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<long, int>, LegendCardData> current = enumerator.Current;
          if (current.Value.Uid == uid && current.Value.OriginPosition == position)
            legendCardDataList.Add(current.Value);
        }
      }
      return legendCardDataList;
    }
  }
}
