﻿// Decompiled with JetBrains decompiler
// Type: DB.Bookmark
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class Bookmark : BaseData
  {
    public Coordinate coordinate;
    public int type;
    public string markName;
    public int favourite;
    public long ctime;
    public long mtime;

    public Hashtable Encode()
    {
      Hashtable hashtable = new Hashtable();
      hashtable[(object) "kingdom_id"] = (object) this.coordinate.K.ToString();
      hashtable[(object) "map_x"] = (object) this.coordinate.X.ToString();
      hashtable[(object) "map_y"] = (object) this.coordinate.Y.ToString();
      hashtable[(object) "favourite"] = (object) this.favourite.ToString();
      hashtable[(object) "name"] = (object) this.markName;
      hashtable[(object) "type"] = (object) this.type.ToString();
      return hashtable;
    }

    public void Decode(object data)
    {
      Hashtable inData = data as Hashtable;
      if (inData == null)
        return;
      this.coordinate = Bookmark.DecodeLocation((object) inData);
      DatabaseTools.UpdateData(inData, "favourite", ref this.favourite);
      DatabaseTools.UpdateData(inData, "name", ref this.markName);
      DatabaseTools.UpdateData(inData, "ctime", ref this.ctime);
      DatabaseTools.UpdateData(inData, "mtime", ref this.mtime);
      DatabaseTools.UpdateData(inData, "type", ref this.type);
    }

    public static Coordinate DecodeLocation(object data)
    {
      Hashtable hashtable = data as Hashtable;
      if (hashtable == null)
        return new Coordinate();
      int result1 = 0;
      int result2 = 0;
      int result3 = 0;
      int.TryParse(hashtable[(object) "world_id"].ToString(), out result1);
      int.TryParse(hashtable[(object) "map_x"].ToString(), out result2);
      int.TryParse(hashtable[(object) "map_y"].ToString(), out result3);
      return new Coordinate(result1, result2, result3);
    }
  }
}
