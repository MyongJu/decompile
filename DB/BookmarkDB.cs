﻿// Decompiled with JetBrains decompiler
// Type: DB.BookmarkDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class BookmarkDB
  {
    private Dictionary<Coordinate, Bookmark> _datas = new Dictionary<Coordinate, Bookmark>();

    public event System.Action<Bookmark> onDataChanged;

    public event System.Action<Bookmark> onDataCreated;

    public event System.Action<Bookmark> onDataUpdated;

    public event System.Action<Bookmark> onDataReomve;

    public event System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (Bookmark), (long) this._datas.Count);
    }

    public Dictionary<Coordinate, Bookmark> data
    {
      get
      {
        return this._datas;
      }
    }

    private void Publish_onDataChanged(Bookmark bookmark)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(bookmark);
    }

    private void Publish_onDataCreated(Bookmark bookmark)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(bookmark);
      this.Publish_onDataChanged(bookmark);
    }

    private void Publish_onDataUpdated(Bookmark bookmark)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(bookmark);
      this.Publish_onDataChanged(bookmark);
    }

    private void Publish_onDataRemove(Bookmark bookmark)
    {
      if (this.onDataReomve != null)
        this.onDataReomve(bookmark);
      this.Publish_onDataChanged(bookmark);
    }

    private void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public Bookmark Get(Coordinate location)
    {
      return this.Get(location.K, location.X, location.Y);
    }

    public Bookmark Get(int k, int x, int y)
    {
      Coordinate key = new Coordinate();
      key.K = k;
      key.X = x;
      key.Y = y;
      Bookmark bookmark = (Bookmark) null;
      this._datas.TryGetValue(key, out bookmark);
      return bookmark;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int count = arrayList.Count;
      for (int index = 0; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int count = arrayList.Count;
      for (int index = 0; index < count; ++index)
        this.Remove(arrayList[index], updateTime);
    }

    private void Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      Coordinate key = Bookmark.DecodeLocation(orgData);
      Bookmark bookmark = (Bookmark) null;
      if (this._datas.TryGetValue(key, out bookmark))
      {
        bookmark.Decode(orgData);
        this.Publish_onDataUpdated(bookmark);
      }
      else
      {
        bookmark = new Bookmark();
        bookmark.Decode(orgData);
        this._datas.Add(bookmark.coordinate, bookmark);
        this.Publish_onDataCreated(bookmark);
      }
    }

    private void Remove(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      Coordinate key = Bookmark.DecodeLocation(orgData);
      Bookmark bookmark = (Bookmark) null;
      if (!this._datas.TryGetValue(key, out bookmark))
        return;
      this.Publish_onDataRemove(bookmark);
      this._datas.Remove(key);
      this.Publish_onDataRemoved();
    }
  }
}
