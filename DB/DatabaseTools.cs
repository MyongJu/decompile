﻿// Decompiled with JetBrains decompiler
// Type: DB.DatabaseTools
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class DatabaseTools
  {
    private static int _tmpStaticInt;
    private static long _tmpStaticLong;
    private static bool _tmpStaticBool;
    private static float _tmpStaticFloat;
    private static double _tmpStatcDouble;

    public static bool UpdateData(Hashtable inData, string key, ref object outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null)
        return false;
      outData = inData[(object) key];
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref int outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || (!int.TryParse(inData[(object) key].ToString(), out DatabaseTools._tmpStaticInt) || DatabaseTools._tmpStaticInt == outData))
        return false;
      outData = DatabaseTools._tmpStaticInt;
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref long outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || (!long.TryParse(inData[(object) key].ToString(), out DatabaseTools._tmpStaticLong) || DatabaseTools._tmpStaticLong == outData))
        return false;
      outData = DatabaseTools._tmpStaticLong;
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref bool outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || (!bool.TryParse(inData[(object) key].ToString(), out DatabaseTools._tmpStaticBool) || DatabaseTools._tmpStaticBool == outData))
        return false;
      outData = DatabaseTools._tmpStaticBool;
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref float outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || (!float.TryParse(inData[(object) key].ToString(), out DatabaseTools._tmpStaticFloat) || (double) DatabaseTools._tmpStaticFloat == (double) outData))
        return false;
      outData = DatabaseTools._tmpStaticFloat;
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref double outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || (!double.TryParse(inData[(object) key].ToString(), out DatabaseTools._tmpStatcDouble) || DatabaseTools._tmpStatcDouble == outData))
        return false;
      outData = DatabaseTools._tmpStatcDouble;
      return true;
    }

    public static bool UpdateData(Hashtable inData, string key, ref string outData)
    {
      if (!inData.ContainsKey((object) key) || inData[(object) key] == null || string.Compare(inData[(object) key].ToString(), outData) == 0)
        return false;
      outData = inData[(object) key].ToString();
      return true;
    }

    public static bool CheckAndParseOrgData(object orgData, out Hashtable data)
    {
      if (orgData == null)
      {
        data = (Hashtable) null;
        return false;
      }
      data = orgData as Hashtable;
      return data != null;
    }

    public static bool CheckAndParseOrgData(object orgData, out ArrayList data)
    {
      if (orgData == null)
      {
        data = (ArrayList) null;
        return false;
      }
      data = orgData as ArrayList;
      return data != null;
    }

    public static double calc1(double target, double bonus, double debuff = 0)
    {
      return target / (1.0 + bonus) * (1.0 - debuff);
    }

    public static int calc1(int target, double bonus, double debuff = 0)
    {
      return (int) ((double) target / (1.0 + bonus) * (1.0 - debuff));
    }

    public static double calc2(double target, double bonus, double debuff = 0)
    {
      return target * (1.0 + bonus) * (1.0 - debuff);
    }

    public static int calc2(int target, double bonus, double debuff = 0)
    {
      return (int) ((double) target * (1.0 + bonus) * (1.0 - debuff));
    }

    public static double calc3(double target, double bonus, double debuff = 0)
    {
      return target * (1.0 - bonus) * (1.0 + debuff);
    }

    public static int calc3(int target, double bonus, double debuff = 0)
    {
      return (int) ((double) target * (1.0 - bonus) * (1.0 + debuff));
    }
  }
}
