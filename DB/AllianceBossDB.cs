﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceBossDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceBossDB
  {
    protected Dictionary<long, AllianceBossData> _tableData = new Dictionary<long, AllianceBossData>();

    public event System.Action<AllianceBossData> onDataChanged;

    public event System.Action<AllianceBossData> onDataUpdate;

    public event System.Action<AllianceBossData> onDataRemove;

    public event System.Action<AllianceBossData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceBossData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(AllianceBossData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(AllianceBossData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(AllianceBossData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(AllianceBossData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(long allianceId, long updateTime)
    {
      if (!this._tableData.ContainsKey(allianceId))
        return false;
      AllianceBossData data = this._tableData[allianceId];
      this._tableData.Remove(allianceId);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "alliance_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AllianceBossData Get(long allianceId)
    {
      if (this._tableData.ContainsKey(allianceId))
        return this._tableData[allianceId];
      return (AllianceBossData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceBossData data = new AllianceBossData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.AllianceId))
        return false;
      this._tableData.Add(data.AllianceId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "alliance_id", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceBossData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public AllianceBossData GetAllianceBossData()
    {
      using (Dictionary<long, AllianceBossData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceBossData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceBossData) null;
    }

    public AllianceBossData GetBossDataByAllianceId(int allianceId)
    {
      using (Dictionary<long, AllianceBossData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceBossData> current = enumerator.Current;
          if (current.Value.AllianceId == (long) allianceId)
            return current.Value;
        }
      }
      return (AllianceBossData) null;
    }
  }
}
