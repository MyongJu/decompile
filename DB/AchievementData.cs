﻿// Decompiled with JetBrains decompiler
// Type: DB.AchievementData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AchievementData : BaseData
  {
    private string category = string.Empty;
    private int achievementId = -1;
    public const int INVALID_ID = 0;
    private int group_id;
    private long ctime;
    private long mtime;
    private bool isFinish;
    private int progress;
    private int uid;
    private BasePresent _present;
    private int state;

    public int GroupId
    {
      get
      {
        return this.group_id;
      }
    }

    public int AchievementId
    {
      get
      {
        return this.achievementId;
      }
    }

    public bool IsFinish
    {
      get
      {
        return this.state == 1;
      }
    }

    public bool IsClaim
    {
      get
      {
        return this.state == 2;
      }
    }

    public int UID
    {
      get
      {
        return this.uid;
      }
    }

    public BasePresent Present
    {
      get
      {
        if (this._present == null)
        {
          this._present = BasePresent.Create(ConfigManager.inst.DB_AchievementMain.GetData(ConfigManager.inst.DB_Achievement.GetData(this.achievementId).GroupID).Category, this.achievementId, (long) this.achievementId);
          this._present.Data = PresentDataFactory.Instance.CreateData(PresentData.DataType.Achievement, this.achievementId, (long) this.achievementId);
        }
        return this._present;
      }
    }

    public int State
    {
      get
      {
        return this.state;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "uid", ref this.uid) | DatabaseTools.UpdateData(dataHt, "achievement_id", ref this.achievementId) | DatabaseTools.UpdateData(dataHt, "status", ref this.state) | DatabaseTools.UpdateData(dataHt, "finish", ref this.isFinish) | DatabaseTools.UpdateData(dataHt, "group_id", ref this.group_id) | DatabaseTools.UpdateData(dataHt, "category", ref this.category) | DatabaseTools.UpdateData(dataHt, "ctime", ref this.ctime) | DatabaseTools.UpdateData(dataHt, "mtime", ref this.mtime);
    }

    public static bool CheckUID(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long result = 0;
      if (hashtable.ContainsKey((object) "uid"))
        long.TryParse(hashtable[(object) "uid"].ToString(), out result);
      return PlayerData.inst.uid == result;
    }

    public int CurrentStarCount
    {
      get
      {
        AchievementInfo data = ConfigManager.inst.DB_Achievement.GetData(this.achievementId);
        return ConfigManager.inst.DB_Achievement.GetAchievementInfos(data.GroupID).IndexOf(data);
      }
    }

    public int MaxStarCount
    {
      get
      {
        return ConfigManager.inst.DB_Achievement.GetAchievementInfos(ConfigManager.inst.DB_Achievement.GetData(this.achievementId).GroupID).Count;
      }
    }

    public static int GetInternalId(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      int result = 0;
      if (hashtable.ContainsKey((object) "achievement_id"))
        int.TryParse(hashtable[(object) "achievement_id"].ToString(), out result);
      return result;
    }

    public int GetAchievementPriority(int state)
    {
      switch (state)
      {
        case 0:
          return 2;
        case 1:
          return 1;
        case 2:
          return 3;
        default:
          return 0;
      }
    }

    public struct Params
    {
      public const string KEY = "user_achievement";
      public const string UID = "uid";
      public const string ACHIEVEMENT_ID = "achievement_id";
      public const string GROUP_ID = "group_id";
      public const string STATE = "status";
      public const string FINISH = "finish";
      public const string CATEGORY = "category";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }

    public struct AchievementState
    {
      public const int PROGRESS = 0;
      public const int FINISH = 1;
      public const int CLAIM = 2;
    }
  }
}
