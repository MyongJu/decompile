﻿// Decompiled with JetBrains decompiler
// Type: DB.StatsData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class StatsData : BaseData
  {
    private string _statsName;
    private long _count;
    private long _mtime;

    public string statsName
    {
      get
      {
        return this._statsName;
      }
    }

    public long count
    {
      get
      {
        return this._count;
      }
    }

    public long mtime
    {
      get
      {
        return this._mtime;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "name", ref this._statsName) | DatabaseTools.UpdateData(dataHt, "value", ref this._count) | DatabaseTools.UpdateData(dataHt, "mtime", ref this._mtime);
    }

    public static string GetStatasName(object orgData)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return (string) null;
      if (data[(object) "name"] != null)
        return data[(object) "name"].ToString();
      return (string) null;
    }

    public static long GetStatsUid(object orgData)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data) || data[(object) "uid"] == null)
        return 0;
      return long.Parse(data[(object) "uid"].ToString());
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "Stats : " + this.statsName + ":" + (object) this.count + "\n";
    }

    public struct Params
    {
      public const string KEY = "user_stats";
      public const string STATS_UID = "uid";
      public const string STATS_STATS = "name";
      public const string STATS_COUNT = "value";
      public const string STATS_MTIME = "mtime";
    }
  }
}
