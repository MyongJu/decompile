﻿// Decompiled with JetBrains decompiler
// Type: DB.CityDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class CityDB
  {
    private SortedDictionary<long, CityData> _datas = new SortedDictionary<long, CityData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataCreate;

    public event System.Action<long> onDataRemove;

    public event System.Action onDataRemoved;

    public event System.Action<long> onCityTroopChanged;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (CityData), (long) this._datas.Count);
    }

    public void Publish_OnDataChanged(long cityId, CityDB.ActionType actionType = CityDB.ActionType.none)
    {
      if (this.onDataChanged != null)
        this.onDataChanged(cityId);
      this.Publish_OtherActions(cityId, actionType);
    }

    public void Publish_OnDataCreate(long cityId, CityDB.ActionType actionType = CityDB.ActionType.none)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(cityId);
      this.Publish_OnDataChanged(cityId, actionType);
    }

    public void Publish_OnDataUpdate(long cityId, CityDB.ActionType actionType = CityDB.ActionType.none)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(cityId);
      this.Publish_OnDataChanged(cityId, actionType);
    }

    public void Publish_OnDataRemove(long cityId)
    {
      this.Publish_OnDataChanged(cityId, CityDB.ActionType.troop);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(cityId);
    }

    public void Publish_OnDataRemoved(CityDB.ActionType actionType = CityDB.ActionType.none)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public void Publish_OtherActions(long cityId, CityDB.ActionType actionType = CityDB.ActionType.troop)
    {
      if ((actionType & CityDB.ActionType.troop) <= CityDB.ActionType.none || this.onCityTroopChanged == null)
        return;
      this.onCityTroopChanged(cityId);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      CityData cityData = new CityData();
      if (!cityData.Decode((object) hashtable, updateTime) || cityData.cityId == -1L || this._datas.ContainsKey(cityData.cityId))
        return false;
      this._datas.Add(cityData.cityId, cityData);
      this.Publish_OnDataCreate(cityData.cityId, this.CheckActionType(orgData));
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = this.CheckKeyFromData((object) hashtable);
      if (index == -1L)
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index, this.CheckActionType(orgData));
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this.Publish_OnDataRemove(id);
      this._datas.Remove(id);
      this.Publish_OnDataRemoved(CityDB.ActionType.none);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse(arrayList[index].ToString()), updateTime);
    }

    public CityData Get(long id)
    {
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (CityData) null;
    }

    public CityData GetByUid(long uid)
    {
      SortedDictionary<long, CityData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.cityOwnerId == uid)
          return enumerator.Current.Value;
      }
      return (CityData) null;
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return -1;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return -1;
      long result = -1;
      if (!hashtable.ContainsKey((object) "city_id") || hashtable[(object) "city_id"] == null || long.TryParse(hashtable[(object) "city_id"].ToString(), out result))
        return result;
      return -1;
    }

    public CityDB.ActionType CheckActionType(object orgData)
    {
      Hashtable data = (Hashtable) null;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return CityDB.ActionType.none;
      CityDB.ActionType actionType = CityDB.ActionType.none;
      if (data.ContainsKey((object) "city_troop"))
        actionType |= CityDB.ActionType.troop;
      return actionType;
    }

    public void ShowInfo()
    {
      string str = "<color=blue>City Database Info </color>\n" + "City count : " + (object) this._datas.Count + "\n";
      using (SortedDictionary<long, CityData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str = str + "  City : " + (object) current + " : " + this._datas[current].cityName;
        }
      }
      using (SortedDictionary<long, CityData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current != (long) PlayerData.inst.cityId)
            ;
        }
      }
    }

    public enum ActionType
    {
      none = 0,
      all = 1,
      troop = 1,
    }
  }
}
