﻿// Decompiled with JetBrains decompiler
// Type: DB.TalentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TalentData : BaseData
  {
    private List<int> _groupIds = new List<int>();
    public const int INVALID_ID = 0;
    private int _uId;
    private int _skillId;

    public int uId
    {
      get
      {
        return this._uId;
      }
    }

    public List<int> groupIds
    {
      get
      {
        return this._groupIds;
      }
    }

    public bool InCurrentGroup(int groudId)
    {
      return this._groupIds.IndexOf(groudId) > -1;
    }

    public int skillId
    {
      get
      {
        return this._skillId;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag = false | DatabaseTools.UpdateData(dataHt, "uid", ref this._uId);
      this._groupIds.Clear();
      if (dataHt.ContainsKey((object) "group_id"))
      {
        ArrayList arrayList = dataHt[(object) "group_id"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            string s = arrayList[index].ToString();
            int result = -1;
            if (int.TryParse(s, out result))
              this._groupIds.Add(result);
          }
        }
      }
      return flag | DatabaseTools.UpdateData(dataHt, "skill_id", ref this._skillId);
    }

    public static int GetTalentId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data))
        return int.Parse(data[(object) "skill_id"].ToString());
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      string str = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        str = "\t";
      return str + "talent info: " + (object) this.uId + ":" + (object) this.skillId + "\n";
    }

    public struct Params
    {
      public const string KEY = "hero_skill";
      public const string USER_ID = "uid";
      public const string SKILL_ID = "skill_id";
      public const string GROUP_ID = "group_id";
    }
  }
}
