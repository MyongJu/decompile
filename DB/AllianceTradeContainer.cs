﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTradeContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceTradeContainer
  {
    private static AllianceJobID m_Identifier = new AllianceJobID();
    public Dictionary<AllianceJobID, AllianceTradeData> datas = new Dictionary<AllianceJobID, AllianceTradeData>();

    public static void UpdateDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceTradeContainer.m_Identifier.Decode(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(AllianceTradeContainer.m_Identifier.allianceId);
        if (allianceData != null)
          allianceData.trades.Update(arrayList[index], updateTime);
      }
    }

    public static void RemoveDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      ArrayList arrayList = orgData as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AllianceTradeContainer.m_Identifier.Decode(arrayList[index]);
        AllianceData allianceData = DBManager.inst.DB_Alliance.Get(AllianceTradeContainer.m_Identifier.allianceId);
        if (allianceData != null)
          allianceData.trades.Remove(arrayList[index], updateTime);
      }
    }

    public AllianceTradeData Get(long alliance_id, long user_id, long job_id)
    {
      AllianceTradeContainer.m_Identifier.Set(alliance_id, user_id, job_id);
      AllianceTradeData allianceTradeData = (AllianceTradeData) null;
      this.datas.TryGetValue(AllianceTradeContainer.m_Identifier, out allianceTradeData);
      return allianceTradeData;
    }

    private bool Update(object orgData, long updateTime)
    {
      AllianceTradeContainer.m_Identifier.Decode(orgData);
      if (this.datas.ContainsKey(AllianceTradeContainer.m_Identifier))
        return this.datas[AllianceTradeContainer.m_Identifier].Decode(orgData, updateTime);
      AllianceTradeData allianceTradeData = new AllianceTradeData();
      if (!allianceTradeData.Decode(orgData, updateTime))
        return false;
      this.datas.Add(AllianceTradeContainer.m_Identifier, allianceTradeData);
      return true;
    }

    private bool Remove(object orgData, long updateTime)
    {
      AllianceTradeContainer.m_Identifier.Decode(orgData);
      if (!this.datas.ContainsKey(AllianceTradeContainer.m_Identifier) || !this.datas[AllianceTradeContainer.m_Identifier].CheckUpdateTime(updateTime))
        return false;
      this.datas.Remove(AllianceTradeContainer.m_Identifier);
      return true;
    }
  }
}
