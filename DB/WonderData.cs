﻿// Decompiled with JetBrains decompiler
// Type: DB.WonderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class WonderData : BaseData
  {
    public const long INVALID_ID = 0;
    private long _stateChangeTime;
    protected int _kingdomColonizedBy;
    protected long king_uid;
    protected long king_alliance_id;
    protected long owner_uid;
    protected long owner_alliance_id;
    protected long state_change_job_id;
    protected long megaphone_job_id;
    protected long auto_king_job_id;
    protected long last_change_name_time;
    protected long last_change_flag_time;
    protected long last_change_portrait_time;
    protected int world_id;
    protected int flag;
    protected int portrait;
    protected long protected_end_time;
    protected string name;
    protected long mtime;
    protected Dictionary<long, long> troops;
    protected string state;

    public long MaxTroopsCount { get; set; }

    public long StateChangeTime
    {
      get
      {
        return this._stateChangeTime;
      }
    }

    public int KingdomColonizedBy
    {
      get
      {
        return this._kingdomColonizedBy;
      }
    }

    public long KING_UID
    {
      get
      {
        return this.king_uid;
      }
    }

    public long KING_ALLIANCE_ID
    {
      get
      {
        return this.king_alliance_id;
      }
    }

    public long OWNER_UID
    {
      get
      {
        return this.owner_uid;
      }
    }

    public long OWNER_ALLIANCE_ID
    {
      get
      {
        return this.owner_alliance_id;
      }
    }

    public long STATE_CHANGE_JOB_ID
    {
      get
      {
        return this.state_change_job_id;
      }
    }

    public long MEGAPHONE_JOB_ID
    {
      get
      {
        return this.megaphone_job_id;
      }
    }

    public long AUTO_KING_JOB_ID
    {
      get
      {
        return this.auto_king_job_id;
      }
    }

    public long LAST_CHANGE_NAME_TIME
    {
      get
      {
        return this.last_change_name_time;
      }
    }

    public long LAST_CHANGE_FLAG_TIME
    {
      get
      {
        return this.last_change_flag_time;
      }
    }

    public long LAST_CHANGE_PORTRAIT_TIME
    {
      get
      {
        return this.last_change_portrait_time;
      }
    }

    public long CurrentTroopsCount
    {
      get
      {
        long num = 0;
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
          if (marchData != null)
            num += (long) marchData.troopsInfo.totalCount;
        }
        return num;
      }
    }

    public long MyTroopId
    {
      get
      {
        Dictionary<long, long>.KeyCollection.Enumerator enumerator = this.Troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
            return this.Troops[enumerator.Current];
        }
        return 0;
      }
    }

    public int WORLD_ID
    {
      get
      {
        return this.world_id;
      }
    }

    public int FLAG
    {
      get
      {
        return this.flag;
      }
    }

    public string FlagIconPath
    {
      get
      {
        return Utils.GetFlagIconPath(this.flag);
      }
    }

    public int PORTRAIT
    {
      get
      {
        return this.portrait;
      }
    }

    public long PROTECTED_END_TIME
    {
      get
      {
        return this.protected_end_time;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string LabelName
    {
      get
      {
        if (!string.IsNullOrEmpty(this.name))
          return string.Format("{0} #{1}", (object) this.name, (object) this.WORLD_ID);
        return string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) this.WORLD_ID);
      }
    }

    public long ModifyTime
    {
      get
      {
        return this.mtime;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this.troops;
      }
    }

    public string State
    {
      get
      {
        return this.state;
      }
    }

    public bool CanShowPeaceShield
    {
      get
      {
        string state = this.state;
        if (state != null)
        {
          if (WonderData.\u003C\u003Ef__switch\u0024map6E == null)
            WonderData.\u003C\u003Ef__switch\u0024map6E = new Dictionary<string, int>(2)
            {
              {
                "protected",
                0
              },
              {
                "unOpen",
                0
              }
            };
          int num;
          if (WonderData.\u003C\u003Ef__switch\u0024map6E.TryGetValue(state, out num) && num == 0)
            return true;
        }
        return false;
      }
    }

    public bool CanShowGoldenSword
    {
      get
      {
        string state = this.state;
        if (state != null)
        {
          if (WonderData.\u003C\u003Ef__switch\u0024map6F == null)
            WonderData.\u003C\u003Ef__switch\u0024map6F = new Dictionary<string, int>(2)
            {
              {
                "fighting",
                0
              },
              {
                "protected",
                0
              }
            };
          int num;
          if (WonderData.\u003C\u003Ef__switch\u0024map6F.TryGetValue(state, out num) && num == 0)
            return true;
        }
        return false;
      }
    }

    public bool HasMarch(long marchId)
    {
      if (this.troops != null)
      {
        Dictionary<long, long>.Enumerator enumerator = this.troops.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value == marchId)
            return true;
        }
      }
      return false;
    }

    public virtual bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "king_uid", ref this.king_uid) | DatabaseTools.UpdateData(inData, "king_alliance_id", ref this.king_alliance_id) | DatabaseTools.UpdateData(inData, "owner_uid", ref this.owner_uid) | DatabaseTools.UpdateData(inData, "owner_alliance_id", ref this.owner_alliance_id) | DatabaseTools.UpdateData(inData, "name", ref this.name) | DatabaseTools.UpdateData(inData, "flag", ref this.flag) | DatabaseTools.UpdateData(inData, "portrait", ref this.portrait) | DatabaseTools.UpdateData(inData, "state", ref this.state) | DatabaseTools.UpdateData(inData, "protected_end_time", ref this.protected_end_time) | DatabaseTools.UpdateData(inData, "state_change_job_id", ref this.state_change_job_id) | DatabaseTools.UpdateData(inData, "megaphone_job_id", ref this.megaphone_job_id) | DatabaseTools.UpdateData(inData, "auto_king_job_id", ref this.auto_king_job_id) | DatabaseTools.UpdateData(inData, "last_change_name_time", ref this.last_change_name_time) | DatabaseTools.UpdateData(inData, "last_change_flag_time", ref this.last_change_flag_time) | DatabaseTools.UpdateData(inData, "last_change_portrait_time", ref this.last_change_portrait_time) | DatabaseTools.UpdateData(inData, "mtime", ref this.mtime) | DatabaseTools.UpdateData(inData, "state_change_time", ref this._stateChangeTime) | DatabaseTools.UpdateData(inData, "colonized_by", ref this._kingdomColonizedBy);
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public struct Params
    {
      public const string KEY = "wonder";
      public const string WORLD_ID = "world_id";
      public const string KING_UID = "king_uid";
      public const string OWNER_UID = "owner_uid";
      public const string KING_ALLIANCE_ID = "king_alliance_id";
      public const string OWNER_ALLIANCE_ID = "owner_alliance_id";
      public const string TROOPS = "troops";
      public const string TITLES = "titles";
      public const string NAME = "name";
      public const string FLAG = "flag";
      public const string PORTRAIT = "portrait";
      public const string STATE = "state";
      public const string STATE_CHANGE_TIME = "state_change_time";
      public const string PROTECTED_END_TIME = "protected_end_time";
      public const string STATE_CHANGE_JOB_ID = "state_change_job_id";
      public const string MEGAPHONE_JOB_ID = "megaphone_job_id";
      public const string AUTO_KING_JOB_ID = "auto_king_job_id";
      public const string LAST_CHANGE_NAME_TIME = "last_change_name_time";
      public const string LAST_CHANGE_FLAG_TIME = "last_change_flag_time";
      public const string LAST_CHANGE_PORTRAIT_TIME = "last_change_portrait_time";
      public const string KINGDOM_COLONIZED_BY = "colonized_by";
      public const string MTIME = "mtime";
    }

    public struct WonderState
    {
      public const string UNOPEN = "unOpen";
      public const string FIGHTING = "fighting";
      public const string PROTECTED = "protected";
    }
  }
}
