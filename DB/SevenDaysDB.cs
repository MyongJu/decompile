﻿// Decompiled with JetBrains decompiler
// Type: DB.SevenDaysDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace DB
{
  public class SevenDaysDB : DBBase
  {
    protected override BaseData CreateData()
    {
      return (BaseData) new SevenDaysData();
    }

    protected override bool NeedToFilter
    {
      get
      {
        return true;
      }
    }

    public int GetSaleItemDataRemain(int day, int itemId, int maxCount)
    {
      SevenDaysData sevenDaysData = this.GetSevenDaysData(day);
      int num = maxCount;
      if (sevenDaysData != null)
        num = maxCount - sevenDaysData.GetBoughtCount(itemId);
      return num;
    }

    protected override string GetDataKey()
    {
      return "config_id";
    }

    public SevenDaysData GetSevenDaysData(int day)
    {
      Dictionary<long, BaseData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        SevenDaysData current = enumerator.Current as SevenDaysData;
        if (current != null && current.Days == day)
          return current;
      }
      return (SevenDaysData) null;
    }

    public bool IsFinih(int day)
    {
      return this.GetSevenDaysData(day).IsFinish;
    }

    public int TotalDays
    {
      get
      {
        return this._datas.Count;
      }
    }

    public int GetCurrentDay()
    {
      int num = 1;
      Dictionary<long, BaseData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current != null)
        {
          SevenDaysData current = enumerator.Current as SevenDaysData;
          if (current.IsStart && !current.IsExpired)
            num = current.Days;
        }
      }
      return num;
    }
  }
}
