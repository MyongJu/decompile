﻿// Decompiled with JetBrains decompiler
// Type: DB.DBManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DBManager
  {
    public const string DATA_SET = "set";
    public const string DATA_DEL = "del";
    public UserDB DB_User;
    public AllianceDB DB_Alliance;
    public CityDB DB_City;
    public CityMapDB DB_CityMap;
    public MarchDB DB_March;
    public RallyDB DB_Rally;
    public ConsumableItemsDB DB_Item;
    public BookmarkDB DB_Bookmark;
    public ContactsDB DB_Contacts;
    public StatsDB DB_Stats;
    public AllianceInfoStatsDB DB_AllianceStats;
    public ResearchDB DB_Research;
    public AllianceGiftDB DB_AllianceGift;
    public UserAllianceGiftDB DB_UserAllianceGift;
    public AllianceInvitedApplyDB DB_AllianceInviteApply;
    public AllianceTechDB DB_AllianceTech;
    public WorldMapDB DB_WorldMap;
    public WorldBossDB DB_WorldBossDB;
    public KingdomBossDB DB_KingdomBossDB;
    public ChatRoomDB DB_room;
    public DragonKnightDB DB_DragonKnight;
    public DragonKnightDungeonDB DB_DragonKnightDungeon;
    public DragonKnightTalentDB DB_DragonKnightTalent;
    public BenefitLocalDB DB_Local_Benefit;
    public TimerQuestLocalDB DB_Local_TimerQuest;
    public NpcStoreLocalDB DB_Local_NpcStore;
    public MagicStoreLocalDB DB_Local_MagicStore;
    public HeroDB DB_hero;
    public Test_Database tester;
    public LegendDB DB_Legend;
    public TalentDB DB_Talent;
    public QuestDB DB_Quest;
    public UserProfileDB DB_UserProfileDB;
    public TimerChestLocalDB DB_Local_TimerChest;
    public AllianceStoreDB DB_AllianceStore;
    public DailyActiviesDB DB_DailyActives;
    public AchievementDB DB_Achievements;
    public DragonDB DB_Dragon;
    public DragonSkillDB DB_DragonSkill;
    public ZoneBorderDB DB_ZoneBorder;
    public AllianceFortressDB DB_AllianceFortress;
    public AllianceWarehouseDB DB_AllianceWarehouse;
    public AllianceSuperMineDB DB_AllianceSuperMine;
    public AllianceHospitalDB DB_AllianceHospital;
    public UserTreasureChestDB DB_UserTreasureChest;
    public AllianceBossDB DB_AllianceBoss;
    public AlliancePortalDB DB_AlliancePortal;
    public MonthCardDB DB_MonthCard;
    public SubscriptionDB DB_Subscription;
    public TreasuryDB DB_Treasury;
    public TreasureMapDB DB_Treasure;
    public WonderDB DB_Wonder;
    public WonderTowerDB DB_WonderTower;
    public ArtifactDB DB_Artifact;
    public LegendCardDB DB_LegendCard;
    public LegendTempleDB DB_LegendTemple;
    public HallOfKingDB DB_HallOfKing;
    public UserBenefitParamDB DB_UserBenefitParam;
    public AllianceTempleDB DB_AllianceTemple;
    public AllianceMagicDB DB_AllianceMagic;
    public SevenDaysDB DB_SevenDays;
    public UserWatchTowerDB DB_WatchTower;
    public UserDKArenaDB DB_DKArenaDB;
    public SevenQuestDB DB_SevenQuests;
    public DigSiteDB DB_DigSite;
    public GemDB DB_Gems;
    private bool _initialized;
    private InventoryDB DB_HeroInventory;
    private InventoryDB DB_DragonKnightInventory;
    private static DBManager _instance;

    public long GetMemorySize()
    {
      return 0L + this.DB_User.GetMemorySize() + this.DB_Alliance.GetMemorySize() + this.DB_City.GetMemorySize() + this.DB_CityMap.GetMemorySize() + this.DB_March.GetMemorySize() + this.DB_Rally.GetMemorySize() + this.DB_Item.GetMemorySize() + this.DB_Bookmark.GetMemorySize() + this.DB_Contacts.GetMemorySize() + this.DB_Stats.GetMemorySize() + this.DB_AllianceStats.GetMemorySize() + this.DB_Research.GetMemorySize() + this.DB_AllianceGift.GetMemorySize() + this.DB_UserAllianceGift.GetMemorySize() + this.DB_AllianceInviteApply.GetMemorySize() + this.DB_AllianceTech.GetMemorySize() + this.DB_WorldMap.GetMemorySize() + this.DB_WorldBossDB.GetMemorySize() + this.DB_room.GetMemorySize() + this.DB_DragonKnight.GetMemorySize() + this.DB_DragonKnightDungeon.GetMemorySize() + this.DB_DragonKnightTalent.GetMemorySize() + this.DB_Local_Benefit.GetMemorySize() + this.DB_Local_TimerQuest.GetMemorySize() + this.DB_hero.GetMemorySize() + this.DB_Legend.GetMemorySize() + this.DB_Talent.GetMemorySize() + this.DB_Quest.GetMemorySize() + this.DB_UserProfileDB.GetMemorySize() + this.DB_AllianceStore.GetMemorySize() + this.DB_DailyActives.GetMemorySize() + this.DB_Achievements.GetMemorySize() + this.DB_Dragon.GetMemorySize() + this.DB_DragonSkill.GetMemorySize() + this.DB_ZoneBorder.GetMemorySize() + this.DB_AllianceFortress.GetMemorySize() + this.DB_AllianceWarehouse.GetMemorySize() + this.DB_AllianceSuperMine.GetMemorySize() + this.DB_AllianceHospital.GetMemorySize() + this.DB_UserTreasureChest.GetMemorySize() + this.DB_AllianceBoss.GetMemorySize() + this.DB_AlliancePortal.GetMemorySize() + this.DB_MonthCard.GetMemorySize() + this.DB_Subscription.GetMemorySize() + this.DB_Treasury.GetMemorySize() + this.DB_Treasure.GetMemorySize() + this.DB_Wonder.GetMemorySize() + this.DB_WonderTower.GetMemorySize() + this.DB_Artifact.GetMemorySize() + this.DB_LegendCard.GetMemorySize() + this.DB_LegendTemple.GetMemorySize() + this.DB_HallOfKing.GetMemorySize() + this.DB_UserBenefitParam.GetMemorySize() + this.DB_AllianceTemple.GetMemorySize() + this.DB_AllianceMagic.GetMemorySize() + this.DB_DKArenaDB.GetMemorySize() + this.DB_SevenQuests.GetMemorySize() + this.DB_DigSite.GetMemorySize() + this.DB_Gems.GetMemorySize() + this.DB_HeroInventory.GetMemorySize() + this.DB_DragonKnightInventory.GetMemorySize();
    }

    public InventoryDB GetInventory(BagType type)
    {
      switch (type)
      {
        case BagType.Hero:
          return this.DB_HeroInventory;
        case BagType.DragonKnight:
          return this.DB_DragonKnightInventory;
        default:
          return (InventoryDB) null;
      }
    }

    public static DBManager inst
    {
      get
      {
        if (DBManager._instance == null)
        {
          DBManager._instance = new DBManager();
          DBManager._instance.Init();
        }
        return DBManager._instance;
      }
    }

    public bool Initialized
    {
      get
      {
        return this._initialized;
      }
    }

    private void Init()
    {
      if (this._initialized)
        return;
      this._initialized = true;
      this.DB_User = new UserDB();
      this.DB_Alliance = new AllianceDB();
      this.DB_City = new CityDB();
      this.DB_CityMap = new CityMapDB();
      this.DB_March = new MarchDB();
      this.DB_Rally = new RallyDB();
      this.DB_HeroInventory = new InventoryDB(BagType.Hero);
      this.DB_DragonKnightInventory = new InventoryDB(BagType.DragonKnight);
      this.DB_Item = new ConsumableItemsDB();
      this.DB_Bookmark = new BookmarkDB();
      this.DB_Stats = new StatsDB();
      this.DB_AllianceStats = new AllianceInfoStatsDB();
      this.DB_Research = new ResearchDB();
      this.DB_Contacts = new ContactsDB();
      this.DB_AllianceGift = new AllianceGiftDB();
      this.DB_UserAllianceGift = new UserAllianceGiftDB();
      this.DB_WorldMap = new WorldMapDB();
      this.DB_KingdomBossDB = new KingdomBossDB();
      this.DB_WorldBossDB = new WorldBossDB();
      this.DB_room = new ChatRoomDB();
      this.DB_Local_Benefit = new BenefitLocalDB();
      this.DB_Local_TimerQuest = new TimerQuestLocalDB();
      this.DB_Local_NpcStore = new NpcStoreLocalDB();
      this.DB_Local_MagicStore = new MagicStoreLocalDB();
      this.DB_DragonKnight = new DragonKnightDB();
      this.DB_DragonKnightDungeon = new DragonKnightDungeonDB();
      this.DB_DragonKnightTalent = new DragonKnightTalentDB();
      this.DB_hero = new HeroDB();
      this.DB_AllianceInviteApply = new AllianceInvitedApplyDB();
      this.DB_AllianceTech = new AllianceTechDB();
      this.DB_March.Init();
      this.DB_Rally.Init();
      this.DB_Legend = new LegendDB();
      this.DB_Talent = new TalentDB();
      this.DB_Quest = new QuestDB();
      this.DB_UserProfileDB = new UserProfileDB();
      this.DB_Local_TimerChest = new TimerChestLocalDB();
      this.DB_AllianceStore = new AllianceStoreDB();
      this.DB_DailyActives = new DailyActiviesDB();
      this.DB_Achievements = new AchievementDB();
      this.DB_Dragon = new DragonDB();
      this.DB_DragonSkill = new DragonSkillDB();
      this.DB_SevenDays = new SevenDaysDB();
      this.DB_SevenQuests = new SevenQuestDB();
      this.DB_WatchTower = new UserWatchTowerDB();
      this.DB_DKArenaDB = new UserDKArenaDB();
      this.DB_AllianceFortress = new AllianceFortressDB();
      this.DB_AllianceWarehouse = new AllianceWarehouseDB();
      this.DB_AllianceSuperMine = new AllianceSuperMineDB();
      this.DB_AllianceBoss = new AllianceBossDB();
      this.DB_UserTreasureChest = new UserTreasureChestDB();
      this.DB_AlliancePortal = new AlliancePortalDB();
      this.DB_AllianceHospital = new AllianceHospitalDB();
      this.DB_ZoneBorder = new ZoneBorderDB();
      this.DB_MonthCard = new MonthCardDB();
      this.DB_Subscription = new SubscriptionDB();
      this.DB_AllianceTemple = new AllianceTempleDB();
      this.DB_AllianceMagic = new AllianceMagicDB();
      this.DB_Treasury = new TreasuryDB();
      this.DB_Treasure = new TreasureMapDB();
      this.DB_Wonder = new WonderDB();
      this.DB_WonderTower = new WonderTowerDB();
      this.DB_Artifact = new ArtifactDB();
      this.DB_HallOfKing = new HallOfKingDB();
      this.DB_UserBenefitParam = new UserBenefitParamDB();
      this.DB_DigSite = new DigSiteDB();
      this.DB_LegendCard = new LegendCardDB();
      this.DB_LegendTemple = new LegendTempleDB();
      this.DB_Gems = new GemDB();
    }

    public void LoadDatas(object orgData, long updateTime)
    {
      this.UpdateDatas(orgData, updateTime);
    }

    public void ReciveDatas(object orgData, long updateTime)
    {
      if (orgData == null)
        return;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return;
      this.UpdateDatas(hashtable[(object) "set"], updateTime);
      this.RemoveDatas(hashtable[(object) "del"], updateTime);
    }

    public void UpdateDatas(object orgData, long updateTime = 0)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return;
      if (hashtable.ContainsKey((object) "user_info"))
        this.DB_User.UpdateDatas(hashtable[(object) "user_info"], updateTime);
      int num;
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map5F == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map5F = new Dictionary<string, int>(6)
            {
              {
                "alliance_fortress",
                0
              },
              {
                "alliance_warehouse",
                1
              },
              {
                "alliance_super_ore",
                2
              },
              {
                "alliance_portal",
                3
              },
              {
                "alliance_hospital",
                4
              },
              {
                "dig_site",
                5
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map5F.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                this.DB_AllianceFortress.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 1:
                this.DB_AllianceWarehouse.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_AllianceSuperMine.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 3:
                this.DB_AlliancePortal.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 4:
                this.DB_AllianceHospital.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 5:
                this.DB_DigSite.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map60 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map60 = new Dictionary<string, int>(55)
            {
              {
                "alliance",
                0
              },
              {
                "seven_days",
                1
              },
              {
                "user_watch_tower",
                2
              },
              {
                "user_dk_arena",
                3
              },
              {
                "seven_days_quest",
                4
              },
              {
                "march",
                5
              },
              {
                "user_consumable",
                6
              },
              {
                "user_inventory",
                7
              },
              {
                "dragon_knight_inventory",
                8
              },
              {
                "daily_quest",
                9
              },
              {
                "user_achievement",
                10
              },
              {
                "user_stats",
                11
              },
              {
                "alliance_info_stats",
                12
              },
              {
                "research",
                13
              },
              {
                "user_contacts",
                14
              },
              {
                "user_quest",
                15
              },
              {
                "world_map",
                16
              },
              {
                "world_boss",
                17
              },
              {
                "festival_boss",
                18
              },
              {
                "chat_room",
                19
              },
              {
                "user_bookmark",
                20
              },
              {
                "user_profile",
                21
              },
              {
                "alliance_gift",
                22
              },
              {
                "user_benefit",
                23
              },
              {
                "alliance_benefit",
                24
              },
              {
                "timer_quest",
                25
              },
              {
                "user_npc_store",
                26
              },
              {
                "user_mgic_store",
                27
              },
              {
                "dragon_knight",
                28
              },
              {
                "dragon_knight_dungeon",
                29
              },
              {
                "dragon_knight_talent",
                30
              },
              {
                "user_hero",
                31
              },
              {
                "alliance_invite_apply",
                32
              },
              {
                "alliance_tech",
                33
              },
              {
                "hero_skill",
                34
              },
              {
                "alliance_store",
                35
              },
              {
                "user_dragon",
                36
              },
              {
                "dragon_skill",
                37
              },
              {
                "zone_border",
                38
              },
              {
                "subscription",
                39
              },
              {
                "user_card",
                40
              },
              {
                "alliance_boss",
                41
              },
              {
                "user_treasure_chest",
                42
              },
              {
                "alliance_temple",
                43
              },
              {
                "magic",
                44
              },
              {
                "user_bank",
                45
              },
              {
                "treasure_map",
                46
              },
              {
                "wonder",
                47
              },
              {
                "wonder_tower",
                48
              },
              {
                "artifact",
                49
              },
              {
                "legend",
                50
              },
              {
                "legend_temple",
                51
              },
              {
                "hall_of_king",
                52
              },
              {
                "user_benefit_param",
                53
              },
              {
                "user_gem",
                54
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map60.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                this.DB_Alliance.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 1:
                this.DB_SevenDays.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_WatchTower.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 3:
                this.DB_DKArenaDB.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 4:
                this.DB_SevenQuests.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 5:
                this.DB_March.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 6:
                this.DB_Item.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 7:
                this.DB_HeroInventory.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 8:
                this.DB_DragonKnightInventory.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 9:
                this.DB_DailyActives.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 10:
                this.DB_Achievements.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 11:
                this.DB_Stats.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 12:
                this.DB_AllianceStats.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 13:
                this.DB_Research.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 14:
                this.DB_Contacts.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 15:
                this.DB_Quest.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 16:
                this.DB_WorldMap.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 17:
                this.DB_KingdomBossDB.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 18:
                this.DB_WorldBossDB.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 19:
                this.DB_room.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 20:
                this.DB_Bookmark.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 21:
                this.DB_UserProfileDB.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 22:
                this.DB_AllianceGift.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 23:
                this.DB_Local_Benefit.UpdateDatas(hashtable[(object) key1], updateTime, false);
                continue;
              case 24:
                this.DB_Local_Benefit.UpdateDatas(hashtable[(object) key1], updateTime, true);
                continue;
              case 25:
                this.DB_Local_TimerQuest.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 26:
                this.DB_Local_NpcStore.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 27:
                this.DB_Local_MagicStore.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 28:
                this.DB_DragonKnight.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 29:
                this.DB_DragonKnightDungeon.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 30:
                this.DB_DragonKnightTalent.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 31:
                this.DB_hero.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 32:
                this.DB_AllianceInviteApply.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 33:
                this.DB_AllianceTech.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 34:
                this.DB_Talent.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 35:
                this.DB_AllianceStore.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 36:
                this.DB_Dragon.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 37:
                this.DB_DragonSkill.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 38:
                this.DB_ZoneBorder.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 39:
                this.DB_Subscription.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 40:
                this.DB_MonthCard.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 41:
                this.DB_AllianceBoss.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 42:
                this.DB_UserTreasureChest.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 43:
                this.DB_AllianceTemple.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 44:
                this.DB_AllianceMagic.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 45:
                this.DB_Treasury.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 46:
                this.DB_Treasure.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 47:
                this.DB_Wonder.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 48:
                this.DB_WonderTower.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 49:
                this.DB_Artifact.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 50:
                this.DB_LegendCard.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 51:
                this.DB_LegendTemple.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 52:
                this.DB_HallOfKing.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 53:
                this.DB_UserBenefitParam.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 54:
                this.DB_Gems.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map61 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map61 = new Dictionary<string, int>(4)
            {
              {
                "rally",
                0
              },
              {
                "job",
                1
              },
              {
                "city",
                2
              },
              {
                "user_city",
                2
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map61.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                this.DB_Rally.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 1:
                JobManager.Instance.UpdateServerJobData(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_City.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map62 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map62 = new Dictionary<string, int>(9)
            {
              {
                "kingdom_map",
                0
              },
              {
                "city_map",
                1
              },
              {
                "chat_room_roster",
                2
              },
              {
                "alliance_roster",
                3
              },
              {
                "alliance_diplomacy",
                4
              },
              {
                "alliance_trade",
                5
              },
              {
                "alliance_help",
                6
              },
              {
                "user_alliance_gift",
                7
              },
              {
                "user_timer_chest",
                8
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map62.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                MapUtils.SetKingdomMap(hashtable[(object) "kingdom_map"]);
                continue;
              case 1:
                this.DB_CityMap.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_room.UpdateMembers(hashtable[(object) key1], updateTime);
                continue;
              case 3:
                AllianceMembersContainer.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 4:
                AllianceDiplomacyContainer.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 5:
                AllianceTradeContainer.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 6:
                AllianceHelpContainer.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 7:
                this.DB_UserAllianceGift.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              case 8:
                this.DB_Local_TimerChest.UpdateDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
    }

    public void RemoveDatas(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return;
      int num;
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map63 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map63 = new Dictionary<string, int>(14)
            {
              {
                "kingdom_map",
                0
              },
              {
                "city_map",
                1
              },
              {
                "seven_days",
                2
              },
              {
                "seven_days_quest",
                3
              },
              {
                "chat_room_roster",
                4
              },
              {
                "alliance_roster",
                5
              },
              {
                "alliance_diplomacy",
                6
              },
              {
                "alliance_trade",
                7
              },
              {
                "alliance_help",
                8
              },
              {
                "user_alliance_gift",
                9
              },
              {
                "alliance_store",
                10
              },
              {
                "dragon_knight",
                11
              },
              {
                "dragon_knight_dungeon",
                12
              },
              {
                "dragon_knight_talent",
                13
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map63.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                MapUtils.DeleteKingdomMap(hashtable[(object) "kingdom_map"]);
                continue;
              case 1:
                this.DB_CityMap.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_SevenDays.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 3:
                this.DB_SevenQuests.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 4:
                this.DB_room.RemoveMemebers(hashtable[(object) key1], updateTime);
                continue;
              case 5:
                AllianceMembersContainer.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 6:
                AllianceDiplomacyContainer.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 7:
                AllianceTradeContainer.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 8:
                AllianceHelpContainer.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 9:
                this.DB_UserAllianceGift.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 10:
                this.DB_AllianceStore.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 11:
                this.DB_DragonKnight.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 12:
                this.DB_DragonKnightDungeon.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 13:
                this.DB_DragonKnightTalent.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map64 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map64 = new Dictionary<string, int>(1)
            {
              {
                "job",
                0
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map64.TryGetValue(key2, out num) && num == 0)
            JobManager.Instance.RemoveServerJobData(hashtable[(object) key1]);
        }
      }
      foreach (string key1 in (IEnumerable) hashtable.Keys)
      {
        string key2 = key1;
        if (key2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map65 == null)
          {
            // ISSUE: reference to a compiler-generated field
            DBManager.\u003C\u003Ef__switch\u0024map65 = new Dictionary<string, int>(56)
            {
              {
                "user",
                0
              },
              {
                "user_info",
                0
              },
              {
                "city",
                1
              },
              {
                "user_city",
                1
              },
              {
                "alliance",
                2
              },
              {
                "march",
                3
              },
              {
                "user_watch_tower",
                4
              },
              {
                "user_dk_arena",
                5
              },
              {
                "rally",
                6
              },
              {
                "user_consumable",
                7
              },
              {
                "user_quest",
                8
              },
              {
                "user_inventory",
                9
              },
              {
                "dragon_knight_inventory",
                10
              },
              {
                "user_stats",
                11
              },
              {
                "user_achievement",
                12
              },
              {
                "alliance_info_stats",
                13
              },
              {
                "research",
                14
              },
              {
                "user_contacts",
                15
              },
              {
                "world_map",
                16
              },
              {
                "festival_boss",
                17
              },
              {
                "world_boss",
                18
              },
              {
                "chat_room",
                19
              },
              {
                "user_bookmark",
                20
              },
              {
                "alliance_gift",
                21
              },
              {
                "user_benefit",
                22
              },
              {
                "alliance_benefit",
                22
              },
              {
                "timer_quest",
                23
              },
              {
                "user_hero",
                24
              },
              {
                "alliance_invite_apply",
                25
              },
              {
                "alliance_tech",
                26
              },
              {
                "hero_skill",
                27
              },
              {
                "user_dragon",
                28
              },
              {
                "alliance_fortress",
                29
              },
              {
                "alliance_warehouse",
                30
              },
              {
                "alliance_super_ore",
                31
              },
              {
                "alliance_portal",
                32
              },
              {
                "alliance_boss",
                33
              },
              {
                "user_treasure_chest",
                34
              },
              {
                "alliance_hospital",
                35
              },
              {
                "dig_site",
                36
              },
              {
                "dragon_skill",
                37
              },
              {
                "zone_border",
                38
              },
              {
                "user_card",
                39
              },
              {
                "subscription",
                40
              },
              {
                "alliance_temple",
                41
              },
              {
                "magic",
                42
              },
              {
                "user_bank",
                43
              },
              {
                "treasure_map",
                44
              },
              {
                "wonder",
                45
              },
              {
                "wonder_tower",
                46
              },
              {
                "artifact",
                47
              },
              {
                "legend",
                48
              },
              {
                "legend_temple",
                49
              },
              {
                "hall_of_king",
                50
              },
              {
                "user_benefit_param",
                51
              },
              {
                "user_gem",
                52
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          if (DBManager.\u003C\u003Ef__switch\u0024map65.TryGetValue(key2, out num))
          {
            switch (num)
            {
              case 0:
                this.DB_User.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 1:
                this.DB_City.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 2:
                this.DB_Alliance.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 3:
                this.DB_March.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 4:
                this.DB_WatchTower.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 5:
                this.DB_DKArenaDB.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 6:
                this.DB_Rally.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 7:
                this.DB_Item.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 8:
                this.DB_Quest.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 9:
                this.DB_HeroInventory.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 10:
                this.DB_DragonKnightInventory.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 11:
                this.DB_Stats.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 12:
                this.DB_Achievements.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 13:
                this.DB_AllianceStats.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 14:
                this.DB_Research.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 15:
                this.DB_Contacts.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 16:
                this.DB_WorldMap.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 17:
                this.DB_WorldBossDB.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 18:
                this.DB_KingdomBossDB.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 19:
                this.DB_room.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 20:
                this.DB_Bookmark.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 21:
                this.DB_AllianceGift.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 22:
                this.DB_Local_Benefit.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 23:
                this.DB_Local_TimerQuest.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 24:
                this.DB_hero.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 25:
                this.DB_AllianceInviteApply.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 26:
                this.DB_AllianceTech.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 27:
                this.DB_Talent.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 28:
                this.DB_Dragon.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 29:
                this.DB_AllianceFortress.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 30:
                this.DB_AllianceWarehouse.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 31:
                this.DB_AllianceSuperMine.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 32:
                this.DB_AlliancePortal.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 33:
                this.DB_AllianceBoss.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 34:
                this.DB_UserTreasureChest.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 35:
                this.DB_AllianceHospital.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 36:
                this.DB_DigSite.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 37:
                this.DB_DragonSkill.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 38:
                this.DB_ZoneBorder.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 39:
                this.DB_MonthCard.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 40:
                this.DB_Subscription.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 41:
                this.DB_AllianceTemple.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 42:
                this.DB_AllianceMagic.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 43:
                this.DB_Treasury.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 44:
                this.DB_Treasure.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 45:
                this.DB_Wonder.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 46:
                this.DB_WonderTower.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 47:
                this.DB_Artifact.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 48:
                this.DB_LegendCard.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 49:
                this.DB_LegendTemple.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 50:
                this.DB_HallOfKing.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 51:
                this.DB_UserBenefitParam.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              case 52:
                this.DB_Gems.RemoveDatas(hashtable[(object) key1], updateTime);
                continue;
              default:
                continue;
            }
          }
        }
      }
    }

    public void Dispose()
    {
      this._initialized = false;
      DBManager._instance = (DBManager) null;
    }

    public void ClearAllianceData()
    {
      this.DB_AllianceTech.Clear();
      this.DB_Local_Benefit.ClearAllianceData();
    }
  }
}
