﻿// Decompiled with JetBrains decompiler
// Type: DB.UserDKArenaDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace DB
{
  public class UserDKArenaDB : DBBase
  {
    protected override string GetDataKey()
    {
      return "uid";
    }

    protected override BaseData CreateData()
    {
      return (BaseData) new UserDKArenaData();
    }

    public UserDKArenaData GetDataById(long id)
    {
      return this.GetData(id) as UserDKArenaData;
    }

    public UserDKArenaData GetCurrentData()
    {
      return this.GetDataById(PlayerData.inst.uid);
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (UserDKArenaData), (long) this._datas.Count);
    }
  }
}
