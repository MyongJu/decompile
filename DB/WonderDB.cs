﻿// Decompiled with JetBrains decompiler
// Type: DB.WonderDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class WonderDB
  {
    private Dictionary<long, WonderData> _datas = new Dictionary<long, WonderData>();

    public event System.Action<WonderData> onDataChanged;

    public event System.Action<WonderData> onDataUpdate;

    public event System.Action<WonderData> onDataCreate;

    public event System.Action<WonderData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (WonderData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(WonderData wonderData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(wonderData);
    }

    private void PublishOnDataCreateEvent(WonderData wonderData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(wonderData);
      this.PublishOnDataChangedEvent(wonderData);
    }

    private void PublishOnDataUpdateEvent(WonderData wonderData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(wonderData);
      this.PublishOnDataChangedEvent(wonderData);
    }

    private void PublishOnDataRemovedEvent(WonderData wonderData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(wonderData);
      this.PublishOnDataChangedEvent(wonderData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      WonderData wonderData = new WonderData();
      if (!wonderData.Decode((object) hashtable, updateTime) || this._datas.ContainsKey((long) wonderData.WORLD_ID))
        return false;
      this._datas.Add((long) wonderData.WORLD_ID, wonderData);
      this.PublishOnDataCreateEvent(wonderData);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "world_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      WonderData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long wonderId, long updateTime)
    {
      if (!this._datas.ContainsKey(wonderId))
        return false;
      WonderData data = this._datas[wonderId];
      this._datas.Remove(wonderId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "world_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public List<MarchData> GetMarchsByWonderID(int wonderId)
    {
      List<MarchData> marchDataList = new List<MarchData>();
      Dictionary<long, long>.ValueCollection.Enumerator enumerator = DBManager.inst.DB_Wonder.Get((long) wonderId).Troops.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
        if (marchData != null && marchData.type != MarchData.MarchType.rally_attack)
          marchDataList.Add(marchData);
      }
      return marchDataList;
    }

    public WonderData Get(long wonderid)
    {
      WonderData wonderData;
      this._datas.TryGetValue(wonderid, out wonderData);
      return wonderData;
    }
  }
}
