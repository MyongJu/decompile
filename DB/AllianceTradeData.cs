﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTradeData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceTradeData : BaseData
  {
    public AllianceJobID identifier;
    public long expireTime;
    public string resourceClass;

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | this.identifier.Decode((object) inData) | DatabaseTools.UpdateData(inData, "expire_time", ref this.expireTime) | DatabaseTools.UpdateData(inData, "class", ref this.resourceClass);
    }

    public static class Params
    {
      public const string KEY = "alliance_trade";
      public const string EXPIRE_TIME = "expire_time";
      public const string CLASS = "class";
    }
  }
}
