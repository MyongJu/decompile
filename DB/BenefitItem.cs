﻿// Decompiled with JetBrains decompiler
// Type: DB.BenefitItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace DB
{
  public struct BenefitItem
  {
    public float value;
    public long arg;

    public struct Params
    {
      public const string BENEFIT_ITEM_JOB_ID = "job_id";
      public const string BENEFIT_ITEM_VALUE = "value";
    }
  }
}
