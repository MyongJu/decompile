﻿// Decompiled with JetBrains decompiler
// Type: DB.TalentDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TalentDB
  {
    private Dictionary<int, TalentData> _datas = new Dictionary<int, TalentData>();
    public System.Action<int> onDataChanged;
    public System.Action<int> onDataCreated;
    public System.Action<int> onDataUpdated;
    public System.Action<int> onDataRemove;
    public System.Action onDataRemoved;

    public Dictionary<int, TalentData> Datas
    {
      get
      {
        return this._datas;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (TalentData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(int itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public bool CanReset()
    {
      Dictionary<int, TalentData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.InCurrentGroup(PlayerData.inst.heroData.skillGroupId))
          return true;
      }
      return false;
    }

    public void Publish_onDataCreated(int itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(int itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(int itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      TalentData talentData = new TalentData();
      if (!talentData.Decode((object) data, updateTime) || (long) talentData.uId == 0L || this._datas.ContainsKey(talentData.skillId))
        return false;
      this._datas.Add(talentData.skillId, talentData);
      this.Publish_onDataCreated(talentData.skillId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int talentId = TalentData.GetTalentId((object) data);
      if (talentId == 0)
        return false;
      if (!this._datas.ContainsKey(talentId))
        return this.Add(orgData, updateTime);
      if (!this._datas[talentId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated(talentId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public TalentData Get(int key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (TalentData) null;
    }

    public bool Remove(int talentid, long updateTime)
    {
      if (!this._datas.ContainsKey(talentid) || !this._datas[talentid].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(talentid);
      this._datas.Remove(talentid);
      this.Publish_onDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(TalentData.GetTalentId(arrayList[index]), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<int, TalentData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TalentData current = enumerator.Current;
        }
      }
    }

    public bool IsTalentOwned(int talentId)
    {
      if (this._datas.ContainsKey(talentId))
        return this._datas[talentId].InCurrentGroup(PlayerData.inst.heroData.skillGroupId);
      return false;
    }

    public List<TalentData> GetTalentListByTreeIndex(int treeIndex)
    {
      List<TalentData> talentDataList = new List<TalentData>();
      Dictionary<int, TalentData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        TalentData talentData = enumerator.Current.Value;
        PlayerTalentInfo playerTalentInfo = ConfigManager.inst.DB_PlayerTalent.GetPlayerTalentInfo(talentData.skillId);
        if (playerTalentInfo != null && ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(playerTalentInfo.talentTreeInternalId).tree == treeIndex)
          talentDataList.Add(talentData);
      }
      return talentDataList;
    }

    public int GetTalentTreeTalentCount(int treeIndex)
    {
      int num = 0;
      List<TalentData> talentListByTreeIndex = this.GetTalentListByTreeIndex(treeIndex);
      for (int index = 0; index < talentListByTreeIndex.Count; ++index)
      {
        if (DBManager.inst.DB_Talent.IsTalentOwned(talentListByTreeIndex[index].skillId))
          ++num;
      }
      return num;
    }
  }
}
