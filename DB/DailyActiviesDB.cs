﻿// Decompiled with JetBrains decompiler
// Type: DB.DailyActiviesDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DailyActiviesDB
  {
    private Dictionary<int, DailyActivyData> _datas = new Dictionary<int, DailyActivyData>();
    public System.Action onPointChanged;
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;
    public System.Action onDataSetChanged;
    private int currentPoint;
    private List<int> hadOpenChest;

    public DailyActivyData Get(int questId)
    {
      if (this._datas.ContainsKey(questId))
        return this._datas[questId];
      return (DailyActivyData) null;
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (DailyActivyData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataSetChanged()
    {
      if (this.onDataSetChanged == null)
        return;
      this.onDataSetChanged();
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      DailyActivyData dailyActivyData = new DailyActivyData();
      if (!dailyActivyData.Decode((object) hashtable, updateTime) || dailyActivyData.DailyActivyID == 0 || this._datas.ContainsKey(dailyActivyData.DailyActivyID))
        return false;
      this._datas.Add(dailyActivyData.DailyActivyID, dailyActivyData);
      this.Publish_onDataCreated((long) dailyActivyData.DailyActivyID);
      return true;
    }

    public int CurrentPoint
    {
      get
      {
        return this.currentPoint;
      }
    }

    public bool HadReadyOpenChest()
    {
      List<DailyActiveRewardInfo> totalRewardList = ConfigManager.inst.DB_DailyActivies.GetTotalRewardList();
      for (int index = 0; index < totalRewardList.Count; ++index)
      {
        if (totalRewardList[index].RequrieScore <= this.CurrentPoint && !this.IsHadOpen(totalRewardList[index].internalId))
          return true;
      }
      return false;
    }

    public bool IsHadOpen(int rewardBoxId)
    {
      if (this.hadOpenChest != null)
        return this.hadOpenChest.IndexOf(rewardBoxId) > -1;
      return false;
    }

    public void UpdateDailyScore(Hashtable data)
    {
      if (data == null)
        return;
      if (data.ContainsKey((object) "point"))
      {
        int result;
        int.TryParse(data[(object) "point"].ToString(), out result);
        if (this.currentPoint != result)
        {
          this.currentPoint = result;
          if (this.onPointChanged != null)
            this.onPointChanged();
        }
      }
      if (!data.ContainsKey((object) "chest"))
        return;
      ArrayList arrayList = data[(object) "chest"] as ArrayList;
      if (this.hadOpenChest == null)
        this.hadOpenChest = new List<int>();
      bool flag = false;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        string s = arrayList[index].ToString();
        int result = -1;
        if (int.TryParse(s, out result) && !this.hadOpenChest.Contains(result))
        {
          this.hadOpenChest.Add(result);
          flag = true;
        }
      }
      if (!flag || this.onPointChanged == null)
        return;
      this.onPointChanged();
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null || !DailyActivyData.CheckUID(orgData))
        return false;
      int internalId = DailyActivyData.GetInternalId((object) hashtable);
      if ((long) internalId == 0L)
        return false;
      if (!this._datas.ContainsKey(internalId))
        return this.Add(orgData, updateTime);
      if (!this._datas[internalId].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated((long) internalId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
      this.Publish_onDataSetChanged();
    }

    public bool Remove(int contactUid, long updateTime)
    {
      if (!this._datas.ContainsKey(contactUid) || !this._datas[contactUid].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(contactUid);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(DailyActivyData.GetInternalId(arrayList[index]), updateTime);
      this.Publish_onDataRemoved();
      this.Publish_onDataSetChanged();
    }
  }
}
