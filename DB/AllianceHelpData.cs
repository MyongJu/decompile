﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceHelpData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceHelpData : BaseData
  {
    public AllianceJobID identifier;
    public int helpAvailable;
    public Hashtable trace;
    private object helpLog;

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | this.identifier.Decode((object) inData) | DatabaseTools.UpdateData(inData, "help_available", ref this.helpAvailable);
      Hashtable hashtable = inData;
      if (hashtable.ContainsKey((object) "help_log"))
        this.helpLog = hashtable[(object) "help_log"];
      if (hashtable.ContainsKey((object) "trace"))
        this.trace = hashtable[(object) "trace"] as Hashtable;
      return flag;
    }

    public bool FindMyLog(long uid)
    {
      Hashtable helpLog = this.helpLog as Hashtable;
      if (helpLog != null)
      {
        IDictionaryEnumerator enumerator = helpLog.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (long.Parse(enumerator.Key.ToString()) == uid)
            return true;
        }
      }
      return false;
    }

    public int GetLogCount()
    {
      Hashtable helpLog = this.helpLog as Hashtable;
      if (helpLog != null)
        return helpLog.Count;
      return 0;
    }

    public static class Params
    {
      public const string KEY = "alliance_help";
      public const string HELP_AVAILABLE = "help_available";
      public const string HELP_LOG = "help_log";
      public const string TRACE = "trace";
    }
  }
}
