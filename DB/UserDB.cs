﻿// Decompiled with JetBrains decompiler
// Type: DB.UserDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class UserDB
  {
    private SortedDictionary<long, UserData> _datas = new SortedDictionary<long, UserData>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataRemove;

    public event System.Action<long> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (UserData), (long) this._datas.Count);
    }

    public void Publish_OnDataChanged(long uid)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(uid);
    }

    private void Publish_OnDataCreate(long uid)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(uid);
      this.Publish_OnDataChanged(uid);
    }

    private void Publish_OnDataUpdate(long uid)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(uid);
      this.Publish_OnDataChanged(uid);
    }

    private void Publish_OnDataRemove(long uid)
    {
      this.Publish_OnDataChanged(uid);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(uid);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      UserData userData = new UserData();
      if (!userData.Decode((object) hashtable, updateTime) || userData.uid == 0L || this._datas.ContainsKey(userData.uid))
        return false;
      this._datas.Add(userData.uid, userData);
      this.Publish_OnDataCreate(userData.uid);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long index = this.CheckKeyFromData((object) hashtable);
      if (index == 0L)
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime) || id == PlayerData.inst.uid)
        return false;
      this._datas.Remove(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse(arrayList[index].ToString()), updateTime);
    }

    public UserData Get(long id)
    {
      if (ConfigManager.inst.DB_NPC != null)
      {
        NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid(id);
        if (dataByUid != null)
        {
          UserData userData = new UserData();
          userData.Set(id, dataByUid.LOC_Name, 0, dataByUid.iconIndex, 0L);
          return userData;
        }
      }
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (UserData) null;
    }

    public UserData GetUserByChannelId(long channelId)
    {
      SortedDictionary<long, UserData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.channelId == channelId)
          return enumerator.Current;
      }
      return (UserData) null;
    }

    public void ShowInfo()
    {
      string str = "<color=blue>User Database Info </color>\n" + "User count : " + (object) this._datas.Count + "\n";
      using (SortedDictionary<long, UserData>.KeyCollection.Enumerator enumerator = this._datas.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          long current = enumerator.Current;
          str = str + string.Empty + (object) current + "\n";
        }
      }
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      if (hashtable == null)
        return 0;
      long result = 0;
      if (long.TryParse(hashtable[(object) "uid"].ToString(), out result))
        return result;
      return 0;
    }
  }
}
