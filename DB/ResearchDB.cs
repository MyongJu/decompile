﻿// Decompiled with JetBrains decompiler
// Type: DB.ResearchDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ResearchDB
  {
    private Dictionary<long, ResearchData> _datas = new Dictionary<long, ResearchData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ResearchData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      ResearchData researchData = new ResearchData();
      if (!researchData.Decode((object) data, updateTime) || researchData.researchId == 0 || this._datas.ContainsKey((long) researchData.researchId))
        return false;
      this._datas.Add((long) researchData.researchId, researchData);
      this.Publish_onDataCreated((long) researchData.researchId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int researchId = ResearchData.GetResearchId((object) data);
      if (researchId == 0)
        return false;
      if (!this._datas.ContainsKey((long) researchId))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) researchId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated((long) researchId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public ResearchData Get(int key)
    {
      if (this._datas.ContainsKey((long) key))
        return this._datas[(long) key];
      return (ResearchData) null;
    }

    public bool Remove(int researchId, long updateTime)
    {
      if (!this._datas.ContainsKey((long) researchId) || !this._datas[(long) researchId].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove((long) researchId);
      this.Publish_onDataRemove((long) researchId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(ResearchData.GetResearchId(arrayList[index]), updateTime);
    }

    public void ShowInfo()
    {
      using (Dictionary<long, ResearchData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ResearchData current = enumerator.Current;
        }
      }
    }

    public Dictionary<long, ResearchData> Datas
    {
      get
      {
        return this._datas;
      }
    }
  }
}
