﻿// Decompiled with JetBrains decompiler
// Type: DB.InlaidGems
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class InlaidGems
  {
    public Dictionary<string, GemData> slotToGemCache = new Dictionary<string, GemData>();
    private Dictionary<int, int> _slotToGems = new Dictionary<int, int>();

    public void LoadAllGems()
    {
      this.slotToGemCache.Clear();
      using (Dictionary<int, int>.KeyCollection.Enumerator enumerator = this._slotToGems.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.GetGemOnSlot(enumerator.Current);
      }
    }

    public GemData GetGemOnSlot(int slotIndex)
    {
      GemData gemData = (GemData) null;
      if (this.slotToGemCache.TryGetValue(slotIndex.ToString(), out gemData) || !this._slotToGems.ContainsKey(slotIndex))
        return gemData;
      gemData = DBManager.inst.DB_Gems.Get((long) this._slotToGems[slotIndex]);
      this.slotToGemCache[slotIndex.ToString()] = gemData;
      return gemData;
    }

    public List<GemData> GetAllGemData()
    {
      List<GemData> gemDataList = new List<GemData>();
      if (this._slotToGems != null)
      {
        using (Dictionary<int, int>.KeyCollection.Enumerator enumerator = this._slotToGems.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GemData gemOnSlot = this.GetGemOnSlot(enumerator.Current);
            if (gemOnSlot != null)
              gemDataList.Add(gemOnSlot);
          }
        }
      }
      return gemDataList;
    }

    public bool Decode(object orgData)
    {
      this._slotToGems.Clear();
      this.slotToGemCache.Clear();
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      bool flag = false;
      IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result1;
        int.TryParse(enumerator.Key.ToString(), out result1);
        int result2;
        int.TryParse(enumerator.Value.ToString(), out result2);
        this._slotToGems[result1] = result2;
        flag = true;
      }
      return flag;
    }
  }
}
