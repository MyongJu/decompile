﻿// Decompiled with JetBrains decompiler
// Type: DB.HeroDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class HeroDB
  {
    private Dictionary<long, HeroData> _datas = new Dictionary<long, HeroData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;
    private HeroKey _checkedKey;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (HeroData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(itemId);
    }

    public void Publish_onDataCreated(long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataUpdated(long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemove(long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(itemId);
      this.Publish_onDataChanged(itemId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      HeroData heroData = new HeroData();
      if (!heroData.Decode((object) data, updateTime) || heroData.uid <= 0L || this._datas.ContainsKey(heroData.uid))
        return false;
      this._datas.Add(heroData.cityId, heroData);
      this.Publish_onDataCreated(heroData.uid);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int heroId = HeroData.GetHeroId((object) data);
      if (heroId <= 0)
        return false;
      if (!this._datas.ContainsKey((long) heroId))
        return this.Add(orgData, updateTime);
      if (!this._datas[(long) heroId].Decode((object) data, updateTime))
        return false;
      this.Publish_onDataUpdated((long) heroId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public HeroData Get(long key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (HeroData) null;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(HeroData.GetHeroId(arrayList[index]), updateTime);
    }

    private bool Remove(int heroId, long updateTime)
    {
      if (!this._datas.ContainsKey((long) heroId) || !this._datas[(long) heroId].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove((long) heroId);
      this.Publish_onDataRemove((long) heroId);
      return true;
    }

    private HeroKey GetKeyFromData()
    {
      this._checkedKey.uid = 0L;
      this._checkedKey.cityId = 0L;
      return this._checkedKey;
    }
  }
}
