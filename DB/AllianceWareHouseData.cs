﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceWareHouseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceWareHouseData : BaseData
  {
    private List<long> troopsSort = new List<long>();
    protected Dictionary<long, long> troops = new Dictionary<long, long>();
    protected Dictionary<long, AllianceWareHouseData.ResourceData> resource = new Dictionary<long, AllianceWareHouseData.ResourceData>();
    protected long alliance_id;
    protected long warehouseId;
    protected long owner_uid;
    protected int world_id;
    protected int map_x;
    protected int map_y;
    protected long durability;
    protected string name;
    protected AllianceWareHouseData.State state;
    protected long build_job_id;
    protected int config_id;
    protected long timer_end;
    protected long utime;
    protected long ctime;
    protected long mtime;
    protected int clear_away;

    public Coordinate Location
    {
      get
      {
        Coordinate coordinate;
        coordinate.K = this.world_id;
        coordinate.X = this.map_x;
        coordinate.Y = this.map_y;
        return coordinate;
      }
    }

    public long AllianceId
    {
      get
      {
        return this.alliance_id;
      }
    }

    public long WarehouseId
    {
      get
      {
        return this.warehouseId;
      }
    }

    public long OwnerUid
    {
      get
      {
        return this.owner_uid;
      }
    }

    public int WorldId
    {
      get
      {
        return this.world_id;
      }
    }

    public int MapX
    {
      get
      {
        return this.map_x;
      }
    }

    public int MapY
    {
      get
      {
        return this.map_y;
      }
    }

    public long Durability
    {
      get
      {
        return this.durability;
      }
    }

    public List<long> TroopsSort
    {
      get
      {
        return this.troopsSort;
      }
    }

    public Dictionary<long, long> Troops
    {
      get
      {
        return this.troops;
      }
    }

    public Dictionary<long, AllianceWareHouseData.ResourceData> Resource
    {
      get
      {
        return this.resource;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public AllianceWareHouseData.State CurrentState
    {
      get
      {
        return this.state;
      }
    }

    public long BuildJobId
    {
      get
      {
        return this.build_job_id;
      }
    }

    public int ConfigId
    {
      get
      {
        return this.config_id;
      }
    }

    public long TimerEnd
    {
      get
      {
        return this.timer_end;
      }
    }

    public long Utime
    {
      get
      {
        return this.utime;
      }
    }

    public long Ctime
    {
      get
      {
        return this.ctime;
      }
    }

    public long Mtime
    {
      get
      {
        return this.mtime;
      }
    }

    public int ClearAway
    {
      get
      {
        return this.clear_away;
      }
    }

    public long TotalResourceWeightStored
    {
      get
      {
        long num = 0;
        using (Dictionary<long, AllianceWareHouseData.ResourceData>.Enumerator enumerator = this.resource.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<long, AllianceWareHouseData.ResourceData> current = enumerator.Current;
            num += current.Value.TotalWeight;
          }
        }
        return num;
      }
    }

    public int TotalPeopleStored
    {
      get
      {
        return this.resource.Count;
      }
    }

    public long MaxTroopsCount { get; set; }

    public long CurrentTroopsCount
    {
      get
      {
        long num = 0;
        Dictionary<long, long>.ValueCollection.Enumerator enumerator = this.Troops.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          MarchData marchData = DBManager.inst.DB_March.Get(enumerator.Current);
          if (marchData != null)
            num += (long) marchData.troopsInfo.totalCount;
        }
        return num;
      }
    }

    public long MyTroopId
    {
      get
      {
        Dictionary<long, long>.KeyCollection.Enumerator enumerator = this.Troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current == PlayerData.inst.hostPlayer.uid)
            return this.Troops[enumerator.Current];
        }
        return 0;
      }
    }

    public AllianceWareHouseData.ResourceData getResourceDataByUserId(long userId)
    {
      if (this.resource.ContainsKey(userId))
        return this.resource[userId];
      return new AllianceWareHouseData.ResourceData();
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id) | DatabaseTools.UpdateData(inData, "building_id", ref this.warehouseId) | DatabaseTools.UpdateData(inData, "owner_uid", ref this.owner_uid) | DatabaseTools.UpdateData(inData, "world_id", ref this.world_id) | DatabaseTools.UpdateData(inData, "map_x", ref this.map_x) | DatabaseTools.UpdateData(inData, "map_y", ref this.map_y) | DatabaseTools.UpdateData(inData, "durability", ref this.durability) | DatabaseTools.UpdateData(inData, "name", ref this.name) | DatabaseTools.UpdateData(inData, string.Empty, ref this.build_job_id);
      this.config_id = ConfigManager.inst.DB_AllianceBuildings.AllianceWarehouseConfigId;
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData, "timer_end", ref this.timer_end) | DatabaseTools.UpdateData(inData, "utime", ref this.utime) | DatabaseTools.UpdateData(inData, "ctime", ref this.ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this.mtime) | DatabaseTools.UpdateData(inData, "clear_away", ref this.clear_away);
      if (inData.Contains((object) "troops_sort"))
      {
        flag2 = true;
        this.troopsSort.Clear();
        ArrayList arrayList = inData[(object) "troops_sort"] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            long result = 0;
            if (long.TryParse(arrayList[index].ToString(), out result))
              this.troopsSort.Add(result);
          }
        }
      }
      if (inData.ContainsKey((object) "state"))
      {
        string key = inData[(object) "state"].ToString();
        if (key != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (AllianceWareHouseData.\u003C\u003Ef__switch\u0024map6B == null)
          {
            // ISSUE: reference to a compiler-generated field
            AllianceWareHouseData.\u003C\u003Ef__switch\u0024map6B = new Dictionary<string, int>(4)
            {
              {
                "building",
                0
              },
              {
                "complete",
                1
              },
              {
                "freeze",
                2
              },
              {
                "unComplete",
                3
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (AllianceWareHouseData.\u003C\u003Ef__switch\u0024map6B.TryGetValue(key, out num))
          {
            switch (num)
            {
              case 0:
                this.state = AllianceWareHouseData.State.Building;
                goto label_22;
              case 1:
                this.state = AllianceWareHouseData.State.Complete;
                goto label_22;
              case 2:
                this.state = AllianceWareHouseData.State.Freeze;
                goto label_22;
              case 3:
                this.state = AllianceWareHouseData.State.UnComplete;
                goto label_22;
            }
          }
        }
        D.error((object) "parse warehouse data error: invalid state");
label_22:
        flag2 = ((flag2 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "troops"))
      {
        this.troops = new Dictionary<long, long>();
        Hashtable hashtable = inData[(object) "troops"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
            this.troops.Add(long.Parse(enumerator.Current.ToString()), long.Parse(hashtable[enumerator.Current].ToString()));
        }
        flag2 = ((flag2 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "resource"))
      {
        this.resource = new Dictionary<long, AllianceWareHouseData.ResourceData>();
        Hashtable hashtable = inData[(object) "resource"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            string s = enumerator.Current.ToString();
            Hashtable data = hashtable[enumerator.Current] as Hashtable;
            if (data != null)
            {
              long key = long.Parse(s);
              AllianceWareHouseData.ResourceData resourceData = new AllianceWareHouseData.ResourceData();
              resourceData.Decode(data);
              this.resource.Add(key, resourceData);
            }
            else
              D.error((object) "parse resource in warehouse error.");
          }
        }
        bool flag3 = ((flag2 ? 1 : 0) | 1) != 0;
      }
      return true;
    }

    public struct Params
    {
      public const string KEY = "alliance_warehouse";
      public const string ALLIANCE_ID = "alliance_id";
      public const string WAREHOUSE_ID = "building_id";
      public const string OWNER_UID = "owner_uid";
      public const string WORLD_ID = "world_id";
      public const string MAP_X = "map_x";
      public const string MAP_Y = "map_y";
      public const string DURABILITY = "durability";
      public const string TROOPS = "troops";
      public const string UID = "uid";
      public const string MARCH_ID = "march_id";
      public const string RESOURCE = "resource";
      public const string WOOD = "wood";
      public const string FOOD = "food";
      public const string ORE = "ore";
      public const string SLIVER = "silver";
      public const string AMOUNT = "amount";
      public const string NAME = "name";
      public const string STATE = "state";
      public const string STATE_BUILDING = "building";
      public const string STATE_UNCOMPLELTE = "unComplete";
      public const string STATE_COMPLETE = "complete";
      public const string STATE_FREEZE = "freeze";
      public const string BUILDING_JOB_ID = "";
      public const string CONFIG_ID = "config_id";
      public const string TIMER_END = "timer_end";
      public const string UTIME = "utime";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
      public const string TROOPS_SORT = "troops_sort";
      public const string CLEAR_AWAY = "clear_away";
    }

    public enum State
    {
      UnComplete = 1,
      Building = 2,
      Complete = 3,
      Freeze = 4,
    }

    public class ResourceData
    {
      public long food;
      public long wood;
      public long ore;
      public long sliver;

      public void Decode(Hashtable data)
      {
        DatabaseTools.UpdateData(data, "food", ref this.food);
        DatabaseTools.UpdateData(data, "wood", ref this.wood);
        DatabaseTools.UpdateData(data, "ore", ref this.ore);
        DatabaseTools.UpdateData(data, "silver", ref this.sliver);
      }

      public long TotalWeight
      {
        get
        {
          return this.food * (long) ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt + this.wood * (long) ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt + this.ore * (long) ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt + this.sliver * (long) ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
        }
      }
    }
  }
}
