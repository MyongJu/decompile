﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceInfoStatsData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceInfoStatsData : BaseData
  {
    private AllianceInfoStatsData.Key _key;
    private long _value;

    public AllianceInfoStatsData.Key key
    {
      get
      {
        return this._key;
      }
    }

    public long value
    {
      get
      {
        return this._value;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | this._key.Decode(dataHt) | DatabaseTools.UpdateData(dataHt, "value", ref this._value);
    }

    public struct Params
    {
      public const string KEY = "alliance_info_stats";
      public const string ALLIANCE_ID = "alliance_id";
      public const string NAME = "name";
      public const string VALUE = "value";
    }

    public struct Key
    {
      private long _allianceId;
      private string _name;

      public Key(long allianceId, string name)
      {
        this._allianceId = allianceId;
        this._name = name;
      }

      public long allianceId
      {
        get
        {
          return this._allianceId;
        }
      }

      public string name
      {
        get
        {
          return this._name;
        }
      }

      public bool Decode(Hashtable data)
      {
        return false | DatabaseTools.UpdateData(data, "alliance_id", ref this._allianceId) | DatabaseTools.UpdateData(data, "name", ref this._name);
      }
    }
  }
}
