﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasuryData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TreasuryData : BaseData
  {
    protected long userId;
    protected int slotId;
    protected long goldCount;
    protected long jobId;
    protected int endTime;
    protected int mTime;
    protected Dictionary<int, bool> vaultLockInfo;

    public long UserId
    {
      get
      {
        return this.userId;
      }
    }

    public int SlotId
    {
      get
      {
        return this.slotId;
      }
    }

    public long GoldCount
    {
      get
      {
        return this.goldCount;
      }
    }

    public long JobId
    {
      get
      {
        return this.jobId;
      }
    }

    public int EndTime
    {
      get
      {
        return this.endTime;
      }
    }

    public int StartTime
    {
      get
      {
        if (this.jobId != 0L)
        {
          JobHandle job = JobManager.Instance.GetJob(this.jobId);
          if (job != null)
            return job.StartTime();
        }
        return -1;
      }
    }

    public int MTime
    {
      get
      {
        return this.mTime;
      }
    }

    public Dictionary<int, bool> VaultLockInfo
    {
      get
      {
        return this.vaultLockInfo;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData, "uid", ref this.userId) | DatabaseTools.UpdateData(inData, "banking_slot_id", ref this.slotId) | DatabaseTools.UpdateData(inData, "gold", ref this.goldCount) | DatabaseTools.UpdateData(inData, "job_id", ref this.jobId) | DatabaseTools.UpdateData(inData, "dead_time", ref this.endTime) | DatabaseTools.UpdateData(inData, "mtime", ref this.mTime);
      if (inData.ContainsKey((object) "lock_info"))
      {
        this.vaultLockInfo = new Dictionary<int, bool>();
        Hashtable hashtable = inData[(object) "lock_info"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            string s1 = enumerator.Current.ToString();
            string s2 = hashtable[enumerator.Current].ToString();
            int result1 = 0;
            int result2 = 0;
            int.TryParse(s1, out result1);
            int.TryParse(s2, out result2);
            if (!this.vaultLockInfo.ContainsKey(result1))
              this.vaultLockInfo.Add(result1, result2 == 1);
          }
        }
        bool flag2 = ((flag1 ? 1 : 0) | 1) != 0;
      }
      return true;
    }

    public struct Params
    {
      public const string KEY = "user_bank";
      public const string USER_ID = "uid";
      public const string LOCK_INFO = "lock_info";
      public const string SLOT_ID = "banking_slot_id";
      public const string GOLD = "gold";
      public const string JOB_ID = "job_id";
      public const string END_TIME = "dead_time";
      public const string MTIME = "mtime";
    }
  }
}
