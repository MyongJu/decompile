﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceMagicDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public class AllianceMagicDB
  {
    private Dictionary<long, AllianceMagicData> _datas = new Dictionary<long, AllianceMagicData>();

    public event System.Action<AllianceMagicData> onDataChanged;

    public event System.Action<AllianceMagicData> onDataUpdate;

    public event System.Action<AllianceMagicData> onDataCreate;

    public event System.Action<AllianceMagicData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceMagicData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(AllianceMagicData magicData)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(magicData);
    }

    private void PublishOnDataCreateEvent(AllianceMagicData magicData)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(magicData);
      this.PublishOnDataChangedEvent(magicData);
    }

    private void PublishOnDataUpdateEvent(AllianceMagicData magicData)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(magicData);
      this.PublishOnDataChangedEvent(magicData);
    }

    private void PublishOnDataRemovedEvent(AllianceMagicData magicData)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(magicData);
      this.PublishOnDataChangedEvent(magicData);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "magic_id", ref outData);
      if (!this._datas.ContainsKey(outData))
      {
        AllianceMagicData magicData = new AllianceMagicData();
        if (magicData.Decode((object) inData, updateTime))
        {
          this._datas.Add(outData, magicData);
          this.PublishOnDataCreateEvent(magicData);
          return true;
        }
      }
      return false;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "magic_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceMagicData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long magicId, long updateTime)
    {
      if (!this._datas.ContainsKey(magicId))
        return false;
      AllianceMagicData data = this._datas[magicId];
      this._datas.Remove(magicId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        Hashtable inData = arrayList[index] as Hashtable;
        if (inData != null)
        {
          long outData = 0;
          DatabaseTools.UpdateData(inData, "magic_id", ref outData);
          this.Remove(outData, updateTime);
        }
      }
    }

    public AllianceMagicData GetBySkillConfigId(int config_id)
    {
      using (Dictionary<long, AllianceMagicData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceMagicData> current = enumerator.Current;
          if (current.Value.ConfigId == config_id && current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceMagicData) null;
    }

    public AllianceMagicData GetBySkillType(string type)
    {
      using (Dictionary<long, AllianceMagicData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceMagicData> current = enumerator.Current;
          TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(current.Value.ConfigId);
          if (byId != null && byId.Type == type && current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceMagicData) null;
    }

    public AllianceMagicData Get(long magicId)
    {
      if (this._datas.ContainsKey(magicId))
        return this._datas[magicId];
      return (AllianceMagicData) null;
    }

    public AllianceMagicData GetCurrentPowerUpSkillByAllianceId(long allianceId)
    {
      using (Dictionary<long, AllianceMagicData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceMagicData> current = enumerator.Current;
          if (current.Value.AllianceId == allianceId && current.Value.CurrentState == "charging")
            return current.Value;
        }
      }
      return (AllianceMagicData) null;
    }

    public AllianceMagicData GetCurrentInterruptedSkillByAllianceId(long allianceId)
    {
      using (Dictionary<long, AllianceMagicData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceMagicData> current = enumerator.Current;
          if (current.Value.Interrupt)
            return current.Value;
        }
      }
      return (AllianceMagicData) null;
    }

    public int GetSkillCdTime(long allianceId, int magicConfigId)
    {
      int b = 0;
      AllianceMagicData allianceMagicData = (AllianceMagicData) null;
      using (Dictionary<long, AllianceMagicData>.Enumerator enumerator = this._datas.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceMagicData> current = enumerator.Current;
          if (current.Value.ConfigId == magicConfigId && current.Value.AllianceId == allianceId)
          {
            allianceMagicData = current.Value;
            break;
          }
        }
      }
      if (allianceMagicData != null)
      {
        TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(magicConfigId);
        if (byId != null)
          b = (int) ((long) byId.CdTime - ((long) NetServerTime.inst.ServerTimestamp - allianceMagicData.Ctime));
      }
      return Mathf.Max(0, b);
    }

    public AllianceMagicData GetCurrentPowerUpSkill()
    {
      if (PlayerData.inst.allianceData == null)
        return (AllianceMagicData) null;
      return this.GetCurrentPowerUpSkillByAllianceId(PlayerData.inst.allianceId);
    }

    public List<AllianceMagicData> GetAllianceWarMagicList()
    {
      List<AllianceMagicData> allianceMagicDataList = new List<AllianceMagicData>();
      if (PlayerData.inst.allianceId != 0L)
      {
        Dictionary<long, AllianceMagicData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (!(enumerator.Current.CurrentState != "charging"))
          {
            if (enumerator.Current.AllianceId == PlayerData.inst.allianceId || enumerator.Current.allTargetAlliance != null && enumerator.Current.allTargetAlliance.Contains(PlayerData.inst.allianceId))
              allianceMagicDataList.Add(enumerator.Current);
            else if (enumerator.Current.allTargetUser != null)
            {
              if (enumerator.Current.allTargetUser.Contains(PlayerData.inst.uid))
              {
                allianceMagicDataList.Add(enumerator.Current);
              }
              else
              {
                for (int index = 0; index < enumerator.Current.allTargetUser.Count; ++index)
                {
                  UserData userData = DBManager.inst.DB_User.Get(enumerator.Current.allTargetUser[index]);
                  if (userData != null && userData.allianceId == PlayerData.inst.allianceId && !allianceMagicDataList.Contains(enumerator.Current))
                    allianceMagicDataList.Add(enumerator.Current);
                }
              }
            }
          }
        }
      }
      return allianceMagicDataList;
    }
  }
}
