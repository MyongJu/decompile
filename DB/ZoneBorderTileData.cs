﻿// Decompiled with JetBrains decompiler
// Type: DB.ZoneBorderTileData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace DB
{
  public class ZoneBorderTileData
  {
    public Coordinate location;
    public Vector3 position;
    public Vector3 localPosition;
    public bool up;
    public bool down;
    public bool left;
    public bool right;

    public void Update(ZoneBorderData borderData)
    {
      Coordinate location1 = this.location;
      --location1.X;
      --location1.Y;
      Coordinate location2 = this.location;
      ++location2.X;
      ++location2.Y;
      Coordinate location3 = this.location;
      --location3.X;
      ++location3.Y;
      Coordinate location4 = this.location;
      ++location4.X;
      --location4.Y;
      this.up = !borderData.Contains(location1);
      this.down = !borderData.Contains(location2);
      this.left = !borderData.Contains(location3);
      this.right = !borderData.Contains(location4);
    }
  }
}
