﻿// Decompiled with JetBrains decompiler
// Type: DB.CityMapData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class CityMapData : BaseData
  {
    private List<InBuildingData> _inBuildingData = new List<InBuildingData>();
    public const int INVALID_ID = 0;
    private long _uid;
    private long _cityId;
    private long _buildingId;
    private int _gloryLevel;
    private int _slotId;
    private int _typeId;
    private int _level;
    private long _jobId;
    private long _taskJobId;
    private bool _inBuilding;

    public long uid
    {
      get
      {
        return this._uid;
      }
    }

    public long cityId
    {
      get
      {
        return this._cityId;
      }
    }

    public long buildingId
    {
      get
      {
        return this._buildingId;
      }
    }

    public int gloryLevel
    {
      get
      {
        return this._gloryLevel;
      }
    }

    public int slotId
    {
      get
      {
        return this._slotId;
      }
    }

    public int typeId
    {
      get
      {
        return this._typeId;
      }
    }

    public int level
    {
      get
      {
        return this._level;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
      set
      {
        this._jobId = value;
      }
    }

    public long taskJobId
    {
      get
      {
        return this._taskJobId;
      }
    }

    public bool inBuilding
    {
      get
      {
        return this._inBuildingData.Count > 0;
      }
    }

    public List<InBuildingData> inBuildingData
    {
      get
      {
        return this._inBuildingData;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null)
        return false;
      Hashtable inData1 = orgData as Hashtable;
      if (inData1 == null)
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(inData1, "uid", ref this._uid) | DatabaseTools.UpdateData(inData1, "city_id", ref this._cityId) | DatabaseTools.UpdateData(inData1, "building_id", ref this._buildingId) | DatabaseTools.UpdateData(inData1, "glory_level", ref this._gloryLevel) | DatabaseTools.UpdateData(inData1, "slot_id", ref this._slotId) | DatabaseTools.UpdateData(inData1, "type_id", ref this._typeId) | DatabaseTools.UpdateData(inData1, "level", ref this._level);
      if (inData1.ContainsKey((object) "job_id"))
      {
        long result = 0;
        if (long.TryParse(inData1[(object) "job_id"].ToString(), out result))
        {
          JobHandle job = JobManager.Instance.GetJob(this._jobId);
          if (job == null || !job.IsClientJob)
          {
            this._jobId = result;
            flag1 = ((flag1 ? 1 : 0) | 1) != 0;
          }
        }
      }
      if (this._level == 0 && this._jobId == 0L)
        D.error((object) ("building level error " + (object) this._uid + " " + (object) this._buildingId + " " + (object) this._slotId + " " + (object) this._typeId));
      bool flag2 = flag1 | DatabaseTools.UpdateData(inData1, "task_job_id", ref this._taskJobId);
      if (inData1.ContainsKey((object) "in_building"))
      {
        this._inBuildingData = new List<InBuildingData>();
        Hashtable inData2 = inData1[(object) "in_building"] as Hashtable;
        if (inData2 != null)
        {
          IDictionaryEnumerator enumerator = inData2.GetEnumerator();
          while (enumerator.MoveNext())
          {
            InBuildingData inBuildingData = new InBuildingData();
            inBuildingData.type = enumerator.Key.ToString();
            DatabaseTools.UpdateData(inData2, inBuildingData.type, ref inBuildingData.value);
            this._inBuildingData.Add(inBuildingData);
          }
        }
        flag2 = true;
      }
      return flag2;
    }

    public string ShowInfo(int tableCount)
    {
      string empty = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        empty += "\t";
      return string.Format("City Map data -- uid : {0}; cityId: {1}; buildingId: {2}", (object) this.uid, (object) this.cityId, (object) this.buildingId) + "\n" + empty + string.Format("Position : ({0})", (object) this.slotId) + "\n" + empty + "Type : " + (object) this.typeId + "\n" + empty + "Level : " + (object) this.level + "\n" + empty + "Job ID :" + (object) this.jobId + "\n" + empty + "Task Job ID :" + (object) this.taskJobId + "\n";
    }

    public struct Params
    {
      public const string KEY = "city_map";
      public const string CITY_MAP_UID = "uid";
      public const string CITY_MAP_CITY_ID = "city_id";
      public const string CITY_MAP_BUILDING_ID = "building_id";
      public const string CITY_MAP_BUILDING_GLORY_LEVEL = "glory_level";
      public const string CITY_MAP_SLOT_ID = "slot_id";
      public const string CITY_MAP_TYPE_ID = "type_id";
      public const string CITY_MAP_LEVEL = "level";
      public const string CITY_MAP_JOB_ID = "job_id";
      public const string CITY_MAP_TASK_JOB_ID = "task_job_id";
      public const string CITY_MAP_IN_BUILDING = "in_building";
    }
  }
}
