﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public class DragonKnightData : BaseData
  {
    [JsonIgnore]
    public HeroEquipments equipments = new HeroEquipments();
    private Dictionary<int, int> _uncollectedEquipments = new Dictionary<int, int>();
    private DragonKnightAttibute _attibutes = new DragonKnightAttibute();
    private Dictionary<string, BenefitInfo> _dkBenefits = new Dictionary<string, BenefitInfo>();
    private BenefitTransMap benefitTransMap = new BenefitTransMap();
    private Dictionary<string, long> _attribute = new Dictionary<string, long>();
    private Dictionary<string, float> _benefits = new Dictionary<string, float>();
    [JsonIgnore]
    public DragonKnightActiveTalent activeTalents = new DragonKnightActiveTalent();
    private long _forgeJobId;
    protected long _userId;
    protected string _name;
    protected long _xp;
    protected int _level;
    protected long _talentPoint;
    protected long _spirit;
    protected int _spiritRecoverTime;
    protected long _capacity;
    protected int _bagAmount;
    protected long _marchId;
    protected int _isInDungeon;
    protected int _lastOpenDungeonTime;
    protected int _currentDungeonEndTime;
    protected int _maxDungeonLevel;
    protected long _ctime;
    protected long _mtime;
    private string _gender;
    [JsonIgnore]
    public bool IsLevelUp;

    public Dictionary<int, int> UncollectedEquipments
    {
      get
      {
        return this._uncollectedEquipments;
      }
    }

    public long ForgeJobId
    {
      get
      {
        return this._forgeJobId;
      }
    }

    public long UserId
    {
      get
      {
        return this._userId;
      }
    }

    public Dictionary<string, float> Benefits
    {
      get
      {
        return this._benefits;
      }
      set
      {
        this._benefits = value;
      }
    }

    public float GetBenefitValue(string orgkey)
    {
      return this.Get(orgkey).total;
    }

    private BenefitInfo Get(string orgKey)
    {
      if (this._dkBenefits.ContainsKey(orgKey))
        return this._dkBenefits[orgKey];
      string shortKey = this.benefitTransMap.GetShortKey(orgKey);
      if (!this._dkBenefits.ContainsKey(shortKey))
        this._dkBenefits.Add(shortKey, new BenefitInfo());
      return this._dkBenefits[shortKey];
    }

    public int GetAttibute(DragonKnightAttibute.Type type)
    {
      return Mathf.CeilToInt(this.GetRawAttibute(type));
    }

    public float GetRawAttibute(DragonKnightAttibute.Type type)
    {
      float baseAttibute = (float) this._attibutes.GetBaseAttibute(type);
      float num1 = 0.0f;
      switch (type)
      {
        case DragonKnightAttibute.Type.Bag:
          num1 = (float) this._bagAmount;
          break;
        case DragonKnightAttibute.Type.Load:
          num1 = (float) this._capacity;
          break;
        case DragonKnightAttibute.Type.StaminaEff:
          DragonKnightItemInfo dragonKnightItemInfo1 = ConfigManager.inst.DB_DragonKnightItem.Get("stamina");
          if (dragonKnightItemInfo1 != null)
          {
            num1 = (float) dragonKnightItemInfo1.effect_base;
            break;
          }
          break;
        case DragonKnightAttibute.Type.MPEff:
          DragonKnightItemInfo dragonKnightItemInfo2 = ConfigManager.inst.DB_DragonKnightItem.Get("mana");
          if (dragonKnightItemInfo2 != null)
          {
            num1 = (float) dragonKnightItemInfo2.effect_base;
            break;
          }
          break;
        case DragonKnightAttibute.Type.HPEff:
          DragonKnightItemInfo dragonKnightItemInfo3 = ConfigManager.inst.DB_DragonKnightItem.Get("health");
          if (dragonKnightItemInfo3 != null)
          {
            num1 = (float) dragonKnightItemInfo3.effect_base;
            break;
          }
          break;
      }
      float num2 = baseAttibute + num1;
      return this.GetBenefitValue("prop_dragon_knight_" + AttributeCalcHelper.Instance.GetAttributes2Benefitkey(DragonKnightAttibute.Type2String(type)) + "_base_value") + num2;
    }

    public int GetAttibuteAndUseCalc(DragonKnightAttibute.Type type, string calcName)
    {
      return Mathf.CeilToInt(AttributeCalcHelper.Instance.GetFinalData(this.GetRawAttibute(type), calcName, this._benefits));
    }

    public int GetFinalData(string key, int baseValue, string calcName)
    {
      float benefitValue = this.GetBenefitValue(key);
      baseValue = Mathf.CeilToInt((float) baseValue + benefitValue);
      return Mathf.CeilToInt(AttributeCalcHelper.Instance.GetFinalData((float) baseValue, calcName, this._benefits));
    }

    public DragonKnightAttibute Attibutes
    {
      get
      {
        return this._attibutes;
      }
    }

    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    public long XP
    {
      get
      {
        return this._xp;
      }
    }

    public int Level
    {
      set
      {
        this._level = value;
      }
      get
      {
        return this._level;
      }
    }

    public long TalentPoint
    {
      get
      {
        return this._talentPoint;
      }
    }

    [JsonIgnore]
    public long Spirit
    {
      get
      {
        return this._spirit;
      }
      set
      {
        this._spirit = value;
      }
    }

    public int SpiritRecoverTime
    {
      get
      {
        return this._spiritRecoverTime;
      }
    }

    public long Capacity
    {
      get
      {
        return (long) this.GetFinalData("prop_dragon_knight_load_base_value", (int) this._capacity, "calc_dragon_knight_load");
      }
    }

    public int BagAmount
    {
      get
      {
        return this.GetFinalData("prop_dragon_knight_bag_capacity_base_value", this._bagAmount, "calc_dragon_knight_bag_capacity");
      }
    }

    public long MarchId
    {
      get
      {
        return this._marchId;
      }
    }

    public bool IsInDungeon
    {
      get
      {
        return this._isInDungeon != 0;
      }
    }

    public int LastOpenDungeonTime
    {
      get
      {
        return this._lastOpenDungeonTime;
      }
    }

    public int CurrentDungeonEndTime
    {
      get
      {
        return this._currentDungeonEndTime;
      }
    }

    public int MaxDungeonLevel
    {
      get
      {
        return this._maxDungeonLevel;
      }
    }

    public long CTime
    {
      get
      {
        return this._ctime;
      }
    }

    public long MTime
    {
      get
      {
        return this._mtime;
      }
    }

    public DragonKnightGender Gender
    {
      get
      {
        string gender = this._gender;
        if (gender != null)
        {
          if (DragonKnightData.\u003C\u003Ef__switch\u0024map6C == null)
            DragonKnightData.\u003C\u003Ef__switch\u0024map6C = new Dictionary<string, int>(2)
            {
              {
                "f",
                0
              },
              {
                "m",
                1
              }
            };
          int num;
          if (DragonKnightData.\u003C\u003Ef__switch\u0024map6C.TryGetValue(gender, out num))
          {
            if (num == 0)
              return DragonKnightGender.Female;
            if (num == 1)
              return DragonKnightGender.Male;
          }
        }
        return DragonKnightGender.Unknown;
      }
      set
      {
        this._gender = "m";
        switch (value)
        {
          case DragonKnightGender.Female:
            this._gender = "f";
            break;
          case DragonKnightGender.Male:
            this._gender = "m";
            break;
        }
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag1 = this._level == 0;
      bool flag2 = DatabaseTools.UpdateData(inData, "level", ref this._level);
      this.IsLevelUp = flag2 && !flag1;
      bool flag3 = flag2 | DatabaseTools.UpdateData(inData, "uid", ref this._userId) | DatabaseTools.UpdateData(inData, "name", ref this._name) | DatabaseTools.UpdateData(inData, "xp", ref this._xp) | DatabaseTools.UpdateData(inData, "talent_point", ref this._talentPoint) | DatabaseTools.UpdateData(inData, "spirit", ref this._spirit) | DatabaseTools.UpdateData(inData, "spirit_recover_time", ref this._spiritRecoverTime) | DatabaseTools.UpdateData(inData, "capacity", ref this._capacity) | DatabaseTools.UpdateData(inData, "bag_amount", ref this._bagAmount) | DatabaseTools.UpdateData(inData, "march_id", ref this._marchId) | DatabaseTools.UpdateData(inData, "is_in_dungeon", ref this._isInDungeon) | DatabaseTools.UpdateData(inData, "last_open_dungeon_time", ref this._lastOpenDungeonTime) | DatabaseTools.UpdateData(inData, "dungeon_end_time", ref this._currentDungeonEndTime) | DatabaseTools.UpdateData(inData, "max_dungeon_level", ref this._maxDungeonLevel) | DatabaseTools.UpdateData(inData, "forge_job_id", ref this._forgeJobId) | DatabaseTools.UpdateData(inData, "gender", ref this._gender) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime) | DatabaseTools.UpdateData(inData, "mtime", ref this._mtime);
      if (inData.ContainsKey((object) "active_talent"))
      {
        this.activeTalents.Decode(inData[(object) "active_talent"]);
        flag3 = ((flag3 ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "attribute"))
      {
        Hashtable sources = inData[(object) "attribute"] as Hashtable;
        flag3 |= this._attibutes.Decode(sources);
      }
      if (inData.ContainsKey((object) "benefits"))
      {
        Hashtable hashtable = inData[(object) "benefits"] as Hashtable;
        this._benefits.Clear();
        this._dkBenefits.Clear();
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (this._dkBenefits.ContainsKey(enumerator.Key.ToString()))
            {
              this._dkBenefits[enumerator.Key.ToString()].Decode(enumerator.Value, false);
              this._benefits[enumerator.Key.ToString()] = this._dkBenefits[enumerator.Key.ToString()].total;
            }
            else
            {
              BenefitInfo benefitInfo = new BenefitInfo();
              benefitInfo.Decode(enumerator.Value, false);
              this._dkBenefits.Add(enumerator.Key.ToString(), benefitInfo);
              this._benefits.Add(enumerator.Key.ToString(), benefitInfo.total);
            }
          }
          flag3 = ((flag3 ? 1 : 0) | 1) != 0;
        }
      }
      if (inData.ContainsKey((object) "equipment"))
        flag3 |= this.equipments.Decode(inData[(object) "equipment"] as Hashtable);
      if (inData.ContainsKey((object) "uncollected_equip"))
      {
        this._uncollectedEquipments = new Dictionary<int, int>();
        flag3 = ((flag3 ? 1 : 0) | 1) != 0;
        Hashtable hashtable = inData[(object) "uncollected_equip"] as Hashtable;
        if (hashtable != null)
        {
          IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
          while (enumerator.MoveNext())
            this._uncollectedEquipments.Add(int.Parse(enumerator.Key.ToString()), int.Parse(enumerator.Value.ToString()));
        }
      }
      return flag3;
    }

    public struct Params
    {
      public const string KEY = "dragon_knight";
      public const string USER_ID = "uid";
      public const string NAME = "name";
      public const string XP = "xp";
      public const string LEVEL = "level";
      public const string TALENT_POINT = "talent_point";
      public const string SPIRIT = "spirit";
      public const string SPIRIT_RECOVER_TIME = "spirit_recover_time";
      public const string EQUIPMENT = "equipment";
      public const string CAPACITY = "capacity";
      public const string BAG_AMOUNT = "bag_amount";
      public const string MARCH_ID = "march_id";
      public const string BENEFITS = "benefits";
      public const string ACTIVE_TALENT = "active_talent";
      public const string IS_IN_DUNGEON = "is_in_dungeon";
      public const string LAST_OPEN_DUNGEON_TIME = "last_open_dungeon_time";
      public const string CURRENT_DUNGEON_END_TIME = "dungeon_end_time";
      public const string MAX_DUNGEON_LEVEL = "max_dungeon_level";
      public const string SKILL_SLOT = "skill_slot";
      public const string ATTRIBUTE = "attribute";
      public const string FORGE_JOB_ID = "forge_job_id";
      public const string UNCOLLECTED_EQUIP = "uncollected_equip";
      public const string GENDER = "gender";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
