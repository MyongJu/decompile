﻿// Decompiled with JetBrains decompiler
// Type: DB.BenefitInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class BenefitInfo
  {
    public float total;
    public BenefitItem[] skills;
    public BenefitItem[] items;
    public BenefitItem hero;
    public BenefitItem research;
    public BenefitItem vip;
    public BenefitItem killStreak;
    public BenefitItem building;
    public BenefitItem title;
    public BenefitItem equipment;
    public BenefitItem alliance_tech;
    public BenefitItem alliance_temple;
    public BenefitItem artifact;
    public BenefitItem dkSkill;
    public BenefitItem dkTalent;
    public BenefitItem dkEquipment;
    public BenefitItem dkGem;
    public BenefitItem gem;
    public BenefitItem legend;
    public BenefitItem legendSuit;
    public BenefitItem equipmentSuit;
    public BenefitItem equipmentSuitEnhance;
    public BenefitItem dkEquipmentSuit;
    public BenefitItem dkEquipmentSuitEnhance;
    public BenefitItem subscription;
    public BenefitItem buildingGlory;
    public BenefitItem lordTitle;
    public BenefitItem[] miniWonders;

    public float skillTotalValue { get; private set; }

    public float itemTotalValue { get; private set; }

    public float miniWonderTotalValue { get; private set; }

    public void Decode(object orgData, bool isAllianceData)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
      {
        if (!isAllianceData)
        {
          float num1 = this.alliance_tech.value;
          float num2 = this.alliance_temple.value;
          this.Clear();
          this.alliance_tech.value = num1;
          this.alliance_temple.value = num2;
        }
        else
        {
          this.total -= this.alliance_tech.value;
          this.total -= this.alliance_temple.value;
          this.alliance_tech.value = 0.0f;
          this.alliance_temple.value = 0.0f;
        }
      }
      else
      {
        if (!isAllianceData)
        {
          float num1 = this.alliance_tech.value;
          float num2 = this.alliance_temple.value;
          this.Clear();
          this.alliance_tech.value = num1;
          this.alliance_temple.value = num2;
        }
        else
          this.total = 0.0f;
        ArrayList arrayList1 = data[(object) "item"] as ArrayList;
        if (arrayList1 != null)
        {
          this.items = new BenefitItem[arrayList1.Count];
          for (int index = 0; index < arrayList1.Count; ++index)
          {
            DatabaseTools.UpdateData(arrayList1[index] as Hashtable, "job_id", ref this.items[index].arg);
            DatabaseTools.UpdateData(arrayList1[index] as Hashtable, "value", ref this.items[index].value);
          }
        }
        ArrayList arrayList2 = data[(object) "skill"] as ArrayList;
        if (arrayList2 != null)
        {
          this.skills = new BenefitItem[arrayList2.Count];
          for (int index = 0; index < arrayList2.Count; ++index)
          {
            DatabaseTools.UpdateData(arrayList2[index] as Hashtable, "job_id", ref this.skills[index].arg);
            DatabaseTools.UpdateData(arrayList2[index] as Hashtable, "value", ref this.skills[index].value);
            this.total += this.skills[index].value;
            this.skillTotalValue += this.skills[index].value;
          }
        }
        DatabaseTools.UpdateData(data, "hero", ref this.hero.value);
        DatabaseTools.UpdateData(data, "research", ref this.research.value);
        DatabaseTools.UpdateData(data, "title", ref this.title.value);
        DatabaseTools.UpdateData(data, "vip", ref this.vip.value);
        DatabaseTools.UpdateData(data, "building", ref this.building.value);
        DatabaseTools.UpdateData(data, "kill_streak", ref this.killStreak.value);
        DatabaseTools.UpdateData(data, "equipment", ref this.equipment.value);
        DatabaseTools.UpdateData(data, "tech", ref this.alliance_tech.value);
        DatabaseTools.UpdateData(data, "alliance_temple", ref this.alliance_temple.value);
        DatabaseTools.UpdateData(data, "artifact", ref this.artifact.value);
        DatabaseTools.UpdateData(data, "dk_skill", ref this.dkSkill.value);
        DatabaseTools.UpdateData(data, "dk_talent", ref this.dkTalent.value);
        DatabaseTools.UpdateData(data, "dk_equipment", ref this.dkEquipment.value);
        DatabaseTools.UpdateData(data, "equipment_suit", ref this.equipmentSuit.value);
        DatabaseTools.UpdateData(data, "equipment_suit_enhance", ref this.equipmentSuitEnhance.value);
        DatabaseTools.UpdateData(data, "dk_equipment_suit", ref this.dkEquipmentSuit.value);
        DatabaseTools.UpdateData(data, "dk_equipment_suit_enhance", ref this.dkEquipmentSuitEnhance.value);
        DatabaseTools.UpdateData(data, "user_gem", ref this.gem.value);
        DatabaseTools.UpdateData(data, "dk_user_gem", ref this.dkGem.value);
        DatabaseTools.UpdateData(data, "legend", ref this.legend.value);
        DatabaseTools.UpdateData(data, "legend_suit", ref this.legendSuit.value);
        DatabaseTools.UpdateData(data, "subscription", ref this.subscription.value);
        DatabaseTools.UpdateData(data, "building_glory", ref this.buildingGlory.value);
        DatabaseTools.UpdateData(data, "lord_title", ref this.lordTitle.value);
        Hashtable hashtable = data[(object) "mini_wonder"] as Hashtable;
        if (hashtable != null)
        {
          this.miniWonders = new BenefitItem[hashtable.Count];
          int index = 0;
          foreach (string key in (IEnumerable) hashtable.Keys)
          {
            long.TryParse(key.ToString(), out this.miniWonders[index].arg);
            float.TryParse(hashtable[(object) key].ToString(), out this.miniWonders[index].value);
            this.total += this.miniWonders[index].value;
            this.miniWonderTotalValue += this.miniWonders[index].value;
          }
        }
        if (this.items != null)
        {
          for (int index = 0; index < this.items.Length; ++index)
            this.total += this.items[index].value;
        }
        this.total += this.hero.value;
        this.total += this.research.value;
        this.total += this.title.value;
        this.total += this.vip.value;
        this.total += this.building.value;
        this.total += this.killStreak.value;
        this.total += this.equipment.value;
        this.total += this.artifact.value;
        this.total += this.alliance_tech.value;
        this.total += this.alliance_temple.value;
        this.total += this.dkSkill.value;
        this.total += this.dkEquipment.value;
        this.total += this.dkTalent.value;
        this.total += this.equipmentSuit.value;
        this.total += this.equipmentSuitEnhance.value;
        this.total += this.dkEquipmentSuit.value;
        this.total += this.dkEquipmentSuitEnhance.value;
        this.total += this.gem.value;
        this.total += this.dkGem.value;
        this.total += this.legend.value;
        this.total += this.legendSuit.value;
        this.total += this.subscription.value;
        this.total += this.buildingGlory.value;
        this.total += this.lordTitle.value;
      }
    }

    public float GetHeroBenefit()
    {
      return this.hero.value;
    }

    public float GetEquipmentBenefit()
    {
      return this.equipment.value + this.equipmentSuit.value + this.equipmentSuitEnhance.value;
    }

    public float GetBuildingBenefit()
    {
      return this.building.value;
    }

    public float GetResearchBenefit()
    {
      return this.research.value;
    }

    public void ClearAllianceData()
    {
      this.total -= this.alliance_tech.value;
      this.total -= this.alliance_temple.value;
      this.alliance_tech.value = 0.0f;
      this.alliance_temple.value = 0.0f;
      this.legend.value = 0.0f;
      this.legendSuit.value = 0.0f;
    }

    public float GetItemBenefit()
    {
      if (this.items == null || this.items.Length == 0)
        return 0.0f;
      float num = 0.0f;
      for (int index = 0; index < this.items.Length; ++index)
        num += this.items[index].value;
      return num;
    }

    public void Clear()
    {
      this.total = this.hero.value = this.title.value = this.vip.value = this.building.value = this.killStreak.value = this.research.value = this.alliance_tech.value = this.alliance_temple.value = this.equipment.value = 0.0f;
      this.artifact.value = 0.0f;
      this.dkSkill.value = 0.0f;
      this.dkEquipment.value = 0.0f;
      this.dkEquipment.value = 0.0f;
      this.dkTalent.value = 0.0f;
      this.equipmentSuit.value = 0.0f;
      this.equipmentSuitEnhance.value = 0.0f;
      this.dkEquipmentSuit.value = 0.0f;
      this.dkEquipmentSuitEnhance.value = 0.0f;
      this.gem.value = 0.0f;
      this.dkGem.value = 0.0f;
      this.legend.value = 0.0f;
      this.legendSuit.value = 0.0f;
      this.subscription.value = 0.0f;
      this.buildingGlory.value = 0.0f;
      this.lordTitle.value = 0.0f;
      this.items = (BenefitItem[]) null;
    }

    public string ShowInfo()
    {
      string str = string.Empty + "total : " + (object) this.total + "\n" + "hero : " + (object) this.hero.value + "\n" + "research : " + (object) this.research.value + "\n" + "building : " + (object) this.building.value + "\n" + "vip : " + (object) this.vip.value + "\n" + "title : " + (object) this.title.value + "\n" + "alliance tech : " + (object) this.alliance_tech.value + "\n" + "alliance temple: " + (object) this.alliance_temple.value + "\n" + "killStreak : " + (object) this.killStreak.value + "\n";
      for (int index = 0; this.miniWonders != null && index < this.miniWonders.Length; ++index)
        str = str + "Mini " + (object) index + " : " + (object) this.miniWonders[index].arg + " -> " + (object) this.miniWonders[index].value + "\n";
      for (int index = 0; this.skills != null && index < this.skills.Length; ++index)
        str = str + "Talent " + (object) index + " : " + (object) this.skills[index].arg + " -> " + (object) this.skills[index].value + "\n";
      for (int index = 0; this.items != null && index < this.items.Length; ++index)
        str = str + "item " + (object) index + " : " + (object) this.items[index].arg + " -> " + (object) this.items[index].value + "\n";
      return str;
    }

    public struct Params
    {
      public const string BENEFIT_INFO_HERO = "hero";
      public const string BENEFIT_INFO_RESEARCH = "research";
      public const string BENEFIT_INFO_SKILL = "skill";
      public const string BENEFIT_INFO_ITEM = "item";
      public const string BENEFIT_INFO_KILL_STREAK = "kill_streak";
      public const string BENEFIT_INFO_BUILDING = "building";
      public const string BENEFIT_INFO_VIP = "vip";
      public const string BENEFIT_INFO_TITLE = "title";
      public const string BENEFIT_INFO_EQUIPMENT = "equipment";
      public const string BENEFIT_INFO_ALLIANCE_TECH = "tech";
      public const string BENEFIT_INFO_ALLIANCE_TEMPLE = "alliance_temple";
      public const string BENEFIT_INFO_MINI_WONDER = "mini_wonder";
      public const string BENEFIT_INFO_ARTIFACT = "artifact";
      public const string BENEFIT_TRACE_DK_EQUIPMENT = "dk_equipment";
      public const string BENEFIT_TRACE_DK_SKILL = "dk_skill";
      public const string BENEFIT_TRACE_DK_TALENT = "dk_talent";
      public const string BENEFIT_TRACE_EQUIPMENT_SUIT = "equipment_suit";
      public const string BENEFIT_TRACE_EQUIPMENT_SUIT_ENHANCE = "equipment_suit_enhance";
      public const string BENEFIT_TRACE_DK_EQUIPMENT_SUIT = "dk_equipment_suit";
      public const string BENEFIT_TRACE_DK_EQUIPMENT_SUIT_ENHANCE = "dk_equipment_suit_enhance";
      public const string BENEFIT_INFO_GEM = "user_gem";
      public const string BENEFIT_INFO_DK_GEM = "dk_user_gem";
      public const string BENEFIT_INFO_LEGEND = "legend";
      public const string BENEFIT_INFO_LEGEND_SUIT = "legend_suit";
      public const string BENEFIT_INFO_SUBSCRIPTION = "subscription";
      public const string BENEFIT_INFO_BUILDING_GLORY = "building_glory";
      public const string BENEFIT_INFO_LORD_TITLE = "lord_title";
    }
  }
}
