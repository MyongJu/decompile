﻿// Decompiled with JetBrains decompiler
// Type: DB.ZoneBorderDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ZoneBorderDB
  {
    private Dictionary<long, ZoneBorderData> m_FortressBorderDict = new Dictionary<long, ZoneBorderData>();

    public event System.Action<ZoneBorderData> onCreated;

    public event System.Action<ZoneBorderData> onUpdated;

    public event System.Action<ZoneBorderData> onChanged;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ZoneBorderData), (long) this.m_FortressBorderDict.Count);
    }

    private void OnChangedEvent(ZoneBorderData border)
    {
      if (this.onChanged == null)
        return;
      this.onChanged(border);
    }

    private void OnCreatedEvent(ZoneBorderData border)
    {
      if (this.onCreated != null)
        this.onCreated(border);
      this.OnChangedEvent(border);
    }

    private void OnUpdatedEvent(ZoneBorderData border)
    {
      if (this.onUpdated != null)
        this.onUpdated(border);
      this.OnChangedEvent(border);
    }

    public List<ZoneBorderData> GetZoneBordersByAllianceID(long allianceId)
    {
      List<ZoneBorderData> zoneBorderDataList = new List<ZoneBorderData>();
      Dictionary<long, ZoneBorderData>.Enumerator enumerator = this.m_FortressBorderDict.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.AllianceID == allianceId)
          zoneBorderDataList.Add(enumerator.Current.Value);
      }
      return zoneBorderDataList;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int count = arrayList.Count;
      for (int index = 0; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private void Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "fortress_id", ref outData);
      ZoneBorderData border = (ZoneBorderData) null;
      if (this.m_FortressBorderDict.TryGetValue(outData, out border))
      {
        border.Decode((object) inData);
        this.OnUpdatedEvent(border);
      }
      else
      {
        border = new ZoneBorderData();
        border.Decode((object) inData);
        this.m_FortressBorderDict.Add(outData, border);
        this.OnCreatedEvent(border);
      }
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(arrayList[index], updateTime);
    }

    private bool Remove(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "fortress_id", ref outData);
      if (!this.m_FortressBorderDict.ContainsKey(outData))
        return false;
      this.m_FortressBorderDict.Remove(outData);
      return true;
    }
  }
}
