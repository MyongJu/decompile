﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceInfoStatsDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceInfoStatsDB
  {
    private Dictionary<AllianceInfoStatsData.Key, AllianceInfoStatsData> _datas = new Dictionary<AllianceInfoStatsData.Key, AllianceInfoStatsData>();
    public System.Action<AllianceInfoStatsData> onDataChanged;
    public System.Action<AllianceInfoStatsData> onDataCreated;
    public System.Action<AllianceInfoStatsData> onDataUpdated;
    public System.Action<AllianceInfoStatsData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceInfoStatsData), (long) this._datas.Count);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        Hashtable data = arrayList[index] as Hashtable;
        AllianceInfoStatsData.Key key = new AllianceInfoStatsData.Key();
        key.Decode(data);
        this.Remove(key, updateTime);
      }
    }

    public AllianceInfoStatsData Get(AllianceInfoStatsData.Key key)
    {
      AllianceInfoStatsData allianceInfoStatsData;
      this._datas.TryGetValue(key, out allianceInfoStatsData);
      return allianceInfoStatsData;
    }

    public AllianceInfoStatsData Get(long allianceId, string name)
    {
      return this.Get(new AllianceInfoStatsData.Key(allianceId, name));
    }

    private void PublishDataChangedEvent(AllianceInfoStatsData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void PublishDataCreatedEvent(AllianceInfoStatsData data)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(data);
      this.PublishDataChangedEvent(data);
    }

    private void PublishDataUpdatedEvent(AllianceInfoStatsData data)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(data);
      this.PublishDataChangedEvent(data);
    }

    private void PublishDataRemovedEvent(AllianceInfoStatsData data)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(data);
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable data1 = orgData as Hashtable;
      if (data1 == null)
        return false;
      AllianceInfoStatsData.Key key = new AllianceInfoStatsData.Key();
      key.Decode(data1);
      AllianceInfoStatsData data2 = this.Get(key);
      if (data2 == null)
        return this.Add(orgData, updateTime);
      if (!data2.Decode((object) data1, updateTime))
        return false;
      this.PublishDataUpdatedEvent(data2);
      return true;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceInfoStatsData data = new AllianceInfoStatsData();
      if (!data.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(data.key))
        return false;
      this._datas.Add(data.key, data);
      this.PublishDataCreatedEvent(data);
      return true;
    }

    private bool Remove(AllianceInfoStatsData.Key key, long updateTime)
    {
      AllianceInfoStatsData data = this.Get(key);
      if (data == null || !data.CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(key);
      this.PublishDataRemovedEvent(data);
      return true;
    }
  }
}
