﻿// Decompiled with JetBrains decompiler
// Type: DB.BenefitLocalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class BenefitLocalDB
  {
    public Dictionary<string, BenefitInfo> benefits = new Dictionary<string, BenefitInfo>();
    public BenefitTransMap benefitTransMap = new BenefitTransMap();
    public System.Action<string> onDataChanged;
    private long _updateTime;

    public static BenefitLocalDB inst
    {
      get
      {
        return DBManager.inst.DB_Local_Benefit;
      }
    }

    public void Publish_onDataChanged(string benefitId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(benefitId);
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (BenefitInfo), (long) this.benefits.Count);
    }

    public bool Update(object orgData, long updateTime, bool isAllianceData)
    {
      if (updateTime < this._updateTime)
        return false;
      this._updateTime = updateTime;
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      if (!isAllianceData)
      {
        if (data.Contains((object) "uid"))
        {
          long result;
          long.TryParse(data[(object) "uid"].ToString(), out result);
          if (result != PlayerData.inst.uid)
            return false;
        }
      }
      else if (data.Contains((object) "alliance_id"))
      {
        long result;
        long.TryParse(data[(object) "alliance_id"].ToString(), out result);
        if (result != PlayerData.inst.allianceId)
          return false;
      }
      IDictionaryEnumerator enumerator = data.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (this.benefits.ContainsKey(enumerator.Key.ToString()))
        {
          this.benefits[enumerator.Key.ToString()].Decode(enumerator.Value, isAllianceData);
        }
        else
        {
          BenefitInfo benefitInfo = new BenefitInfo();
          benefitInfo.Decode(enumerator.Value, isAllianceData);
          this.benefits.Add(enumerator.Key.ToString(), benefitInfo);
        }
        this.Publish_onDataChanged(enumerator.Key.ToString());
      }
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime, bool isAllianceData = false)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime, isAllianceData);
    }

    public void Remove(object orgData, long updateTime)
    {
      if (updateTime < this._updateTime)
        return;
      this._updateTime = updateTime;
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return;
      IDictionaryEnumerator enumerator = data.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (this.benefits.ContainsKey(enumerator.Key.ToString()))
        {
          this.benefits[enumerator.Key.ToString()].Clear();
          this.Publish_onDataChanged(enumerator.Key.ToString());
        }
      }
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove((object) ResearchData.GetResearchId(arrayList[index]), updateTime);
    }

    public BenefitInfo Get(string orgKey)
    {
      if (this.benefits.ContainsKey(orgKey))
        return this.benefits[orgKey];
      string shortKey = this.benefitTransMap.GetShortKey(orgKey);
      if (!this.benefits.ContainsKey(shortKey))
        this.benefits.Add(shortKey, new BenefitInfo());
      return this.benefits[shortKey];
    }

    public BenefitInfo Get(int internalId)
    {
      PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[internalId];
      if (dbProperty != null)
        return this.Get(dbProperty.ID);
      return (BenefitInfo) null;
    }

    public string GetShortID(string id)
    {
      return this.benefitTransMap.GetShortKey(id);
    }

    public string GetLongID(string id)
    {
      return this.benefitTransMap.GetLongKey(id);
    }

    public void ClearAllianceData()
    {
      Dictionary<string, BenefitInfo>.ValueCollection.Enumerator enumerator = this.benefits.Values.GetEnumerator();
      while (enumerator.MoveNext())
        enumerator.Current.ClearAllianceData();
    }

    public struct Params
    {
      public const string KEY = "user_benefit";
      public const string ALLIANCE_KEY = "alliance_benefit";
      public const string prop_infantry_health_percent = "prop_infantry_health_percent";
      public const string prop_infantry_attack_percent = "prop_infantry_attack_percent";
      public const string prop_infantry_defense_percent = "prop_infantry_defense_percent";
      public const string prop_infantry_upkeep_percent = "prop_infantry_upkeep_percent";
      public const string prop_infantry_training_time_percent = "prop_infantry_training_time_percent";
      public const string prop_infantry_healing_time_percent = "prop_infantry_healing_time_percent";
      public const string prop_infantry_speed_percent = "prop_infantry_speed_percent";
      public const string prop_infantry_load_percent = "prop_infantry_load_percent";
      public const string prop_cavalry_health_percent = "prop_cavalry_health_percent";
      public const string prop_cavalry_attack_percent = "prop_cavalry_attack_percent";
      public const string prop_cavalry_defense_percent = "prop_cavalry_defense_percent";
      public const string prop_cavalry_upkeep_percent = "prop_cavalry_upkeep_percent";
      public const string prop_cavalry_training_time_percent = "prop_cavalry_training_time_percent";
      public const string prop_cavalry_healing_time_percent = "prop_cavalry_healing_time_percent";
      public const string prop_cavalry_speed_percent = "prop_cavalry_speed_percent";
      public const string prop_cavalry_load_percent = "prop_cavalry_load_percent";
      public const string prop_ranged_health_percent = "prop_ranged_health_percent";
      public const string prop_ranged_attack_percent = "prop_ranged_attack_percent";
      public const string prop_ranged_defense_percent = "prop_ranged_defense_percent";
      public const string prop_ranged_upkeep_percent = "prop_ranged_upkeep_percent";
      public const string prop_ranged_training_time_percent = "prop_ranged_training_time_percent";
      public const string prop_ranged_healing_time_percent = "prop_ranged_healing_time_percent";
      public const string prop_ranged_speed_percent = "prop_ranged_speed_percent";
      public const string prop_ranged_load_percent = "prop_ranged_load_percent";
      public const string prop_artyclose_health_percent = "prop_artyclose_health_percent";
      public const string prop_artyclose_attack_percent = "prop_artyclose_attack_percent";
      public const string prop_artyclose_defense_percent = "prop_artyclose_defense_percent";
      public const string prop_artyclose_upkeep_percent = "prop_artyclose_upkeep_percent";
      public const string prop_artyclose_training_time_percent = "prop_artyclose_training_time_percent";
      public const string prop_artyclose_healing_time_percent = "prop_artyclose_healing_time_percent";
      public const string prop_artyclose_speed_percent = "prop_artyclose_speed_percent";
      public const string prop_artyclose_load_percent = "prop_artyclose_load_percent";
      public const string prop_artyfar_health_percent = "prop_artyfar_health_percent";
      public const string prop_artyfar_attack_percent = "prop_artyfar_attack_percent";
      public const string prop_artyfar_defense_percent = "prop_artyfar_defense_percent";
      public const string prop_artyfar_upkeep_percent = "prop_artyfar_upkeep_percent";
      public const string prop_artyfar_training_time_percent = "prop_artyfar_training_time_percent";
      public const string prop_artyfar_healing_time_percent = "prop_artyfar_healing_time_percent";
      public const string prop_artyfar_speed_percent = "prop_artyfar_speed_percent";
      public const string prop_artyfar_load_percent = "prop_artyfar_load_percent";
      public const string prop_trap_health_percent = "prop_trap_health_percent";
      public const string prop_trap_attack_percent = "prop_trap_attack_percent";
      public const string prop_trap_defense_percent = "prop_trap_defense_percent";
      public const string prop_trap_training_time_percent = "prop_trap_training_time_percent";
      public const string prop_army_health_percent = "prop_army_health_percent";
      public const string prop_army_attack_percent = "prop_army_attack_percent";
      public const string prop_army_defense_percent = "prop_army_defense_percent";
      public const string prop_army_upkeep_percent = "prop_army_upkeep_percent";
      public const string prop_army_training_time_percent = "prop_army_training_time_percent";
      public const string prop_army_healing_time_percent = "prop_army_healing_time_percent";
      public const string prop_army_speed_percent = "prop_army_speed_percent";
      public const string prop_army_load_percent = "prop_army_load_percent";
      public const string prop_food_generation_value = "prop_food_generation_value";
      public const string prop_food_generation_percent = "prop_food_generation_percent";
      public const string prop_wood_generation_value = "prop_wood_generation_value";
      public const string prop_wood_generation_percent = "prop_wood_generation_percent";
      public const string prop_ore_generation_value = "prop_ore_generation_value";
      public const string prop_ore_generation_percent = "prop_ore_generation_percent";
      public const string prop_silver_generation_value = "prop_silver_generation_value";
      public const string prop_silver_generation_percent = "prop_silver_generation_percent";
      public const string prop_resource_generation_value = "prop_resource_generation_value";
      public const string prop_resource_generation_percent = "prop_resource_generation_percent";
      public const string prop_food_storage_value = "prop_food_storage_value";
      public const string prop_food_storage_percent = "prop_food_storage_percent";
      public const string prop_wood_storage_value = "prop_wood_storage_value";
      public const string prop_wood_storage_percent = "prop_wood_storage_percent";
      public const string prop_ore_storage_value = "prop_ore_storage_value";
      public const string prop_ore_storage_percent = "prop_ore_storage_percent";
      public const string prop_silver_storage_value = "prop_silver_storage_value";
      public const string prop_silver_storage_percent = "prop_silver_storage_percent";
      public const string prop_resource_storage_value = "prop_resource_storage_value";
      public const string prop_resource_storage_percent = "prop_resource_storage_percent";
      public const string prop_hero_health_value = "prop_hero_health_value";
      public const string prop_hero_health_percent = "prop_hero_health_percent";
      public const string prop_hero_attack_value = "prop_hero_attack_value";
      public const string prop_hero_attack_percent = "prop_hero_attack_percent";
      public const string prop_hero_defense_value = "prop_hero_defense_value";
      public const string prop_hero_defense_percent = "prop_hero_defense_percent";
      public const string prop_hero_speed_value = "prop_hero_speed_value";
      public const string prop_hero_speed_percent = "prop_hero_speed_percent";
      public const string prop_hero_xp_bonus_percent = "prop_hero_xp_bonus_percent";
      public const string prop_hero_xp_retention_percent = "prop_hero_xp_retention_percent";
      public const string prop_hero_execution_time_value = "prop_hero_execution_time_value";
      public const string prop_hero_execution_time_percent = "prop_hero_execution_time_percent";
      public const string prop_hero_escape_chance_percent = "prop_hero_escape_chance_percent";
      public const string prop_hero_ransom_limit_value = "prop_hero_ransom_limit_value";
      public const string prop_hero_ransom_limit_percent = "prop_hero_ransom_limit_percent";
      public const string prop_food_gather_percent = "prop_food_gather_percent";
      public const string prop_wood_gather_percent = "prop_wood_gather_percent";
      public const string prop_ore_gather_percent = "prop_ore_gather_percent";
      public const string prop_silver_gather_percent = "prop_silver_gather_percent";
      public const string prop_resource_gather_percent = "prop_resource_gather_percent";
      public const string prop_construction_time_percent = "prop_construction_time_percent";
      public const string prop_research_time_percent = "prop_research_time_percent";
      public const string prop_crafting_time_percent = "prop_crafting_time_percent";
      public const string prop_training_capacity_value = "prop_training_capacity_value";
      public const string prop_training_capacity_percent = "prop_training_capacity_percent";
      public const string prop_trade_capacity_value = "prop_trade_capacity_value";
      public const string prop_trade_capacity_percent = "prop_trade_capacity_percent";
      public const string prop_tradetax_value = "prop_tradetax_value";
      public const string prop_healing_capacity_value = "prop_healing_capacity_value";
      public const string prop_healing_capacity_percent = "prop_healing_capacity_percent";
      public const string prop_march_capacity_value = "prop_march_capacity_value";
      public const string prop_march_capacity_percent = "prop_march_capacity_percent";
      public const string prop_march_limit_value = "prop_march_limit_value";
      public const string prop_trap_capacity_value = "prop_trap_capacity_value";
      public const string prop_trap_capacity_percent = "prop_trap_capacity_percent";
      public const string prop_city_reinforcement_capacity_value = "prop_city_reinforcement_capacity_value";
      public const string prop_city_reinforcement_capacity_percent = "prop_city_reinforcement_capacity_percent";
      public const string prop_rally_capacity_value = "prop_rally_capacity_value";
      public const string prop_rally_capacity_percent = "prop_rally_capacity_percent";
      public const string prop_speedup_time_value = "prop_speedup_time_value";
      public const string prop_speedup_time_percent = "prop_speedup_time_percent";
      public const string prop_alliance_quest_limit_value = "prop_alliance_quest_limit_value";
      public const string prop_alliance_quest_autocomplete_value = "prop_alliance_quest_autocomplete_value";
      public const string prop_daily_quest_limit_value = "prop_daily_quest_limit_value";
      public const string prop_daily_quest_autocomplete_value = "prop_daily_quest_autocomplete_value";
      public const string prop_peace_protection_value = "prop_peace_protection_value";
      public const string prop_antiscout_protection_value = "prop_antiscout_protection_value";
      public const string prop_food_generation_base_value = "prop_food_generation_base_value";
      public const string prop_wood_generation_base_value = "prop_wood_generation_base_value";
      public const string prop_ore_generation_base_value = "prop_ore_generation_base_value";
      public const string prop_silver_generation_base_value = "prop_silver_generation_base_value";
      public const string prop_food_storage_base_value = "prop_food_storage_base_value";
      public const string prop_wood_storage_base_value = "prop_wood_storage_base_value";
      public const string prop_ore_storage_base_value = "prop_ore_storage_base_value";
      public const string prop_silver_storage_base_value = "prop_silver_storage_base_value";
      public const string prop_resource_storage_base_value = "prop_resource_storage_base_value";
      public const string prop_dragon_light_reduce_percent = "prop_dragon_light_reduce_percent";
      public const string prop_dragon_dark_reduce_percent = "prop_dragon_dark_reduce_percent";
      public const string prop_monster_spirit_cost_reduction_percent = "prop_monster_spirit_cost_reduction_percent";
      public const string prop_storage_protect_base_value = "prop_storage_protect_base_value";
      public const string prop_storage_protect_value = "prop_storage_protect_value";
      public const string prop_storage_protect_percent = "prop_storage_protect_percent";
      public const string prop_dragon_knight_strength_base_value = "prop_dragon_knight_strength_base_value";
      public const string prop_dragon_knight_strength_percent = "prop_dragon_knight_strength_percent";
      public const string prop_dragon_knight_intelligent_base_value = "prop_dragon_knight_intelligent_base_value";
      public const string prop_dragon_knight_intelligent_percent = "prop_dragon_knight_intelligent_percent";
      public const string prop_dragon_knight_armor_base_value = "prop_dragon_knight_armor_base_value";
      public const string prop_dragon_knight_armor_percent = "prop_dragon_knight_armor_percent";
      public const string prop_dragon_knight_constitution_base_value = "prop_dragon_knight_constitution_base_value";
      public const string prop_dragon_knight_constitution_percent = "prop_dragon_knight_constitution_percent";
      public const string prop_dragon_knight_attack_base_value = "prop_dragon_knight_attack_base_value";
      public const string prop_dragon_knight_attack_percent = "prop_dragon_knight_attack_percent";
      public const string prop_dragon_knight_magic_base_value = "prop_dragon_knight_magic_base_value";
      public const string prop_dragon_knight_magic_percent = "prop_dragon_knight_magic_percent";
      public const string prop_dragon_knight_defence_base_value = "prop_dragon_knight_defence_base_value";
      public const string prop_dragon_knight_defence_percent = "prop_dragon_knight_defence_percent";
      public const string prop_dragon_knight_health_base_value = "prop_dragon_knight_health_base_value";
      public const string prop_dragon_knight_health_percent = "prop_dragon_knight_health_percent";
      public const string prop_dragon_knight_mana_base_value = "prop_dragon_knight_mana_base_value";
      public const string prop_dragon_knight_mana_percent = "prop_dragon_knight_mana_percent";
      public const string prop_dragon_knight_health_recover_base_value = "prop_dragon_knight_health_recover_base_value";
      public const string prop_dragon_knight_health_recover_percent = "prop_dragon_knight_health_recover_percent";
      public const string prop_dragon_knight_mana_recover_base_value = "prop_dragon_knight_mana_recover_base_value";
      public const string prop_dragon_knight_mana_recover_percent = "prop_dragon_knight_mana_recover_percent";
      public const string prop_dragon_knight_fire_power_base_value = "prop_dragon_knight_fire_power_base_value";
      public const string prop_dragon_knight_fire_power_percent = "prop_dragon_knight_fire_power_percent";
      public const string prop_dragon_knight_water_power_base_value = "prop_dragon_knight_water_power_base_value";
      public const string prop_dragon_knight_water_power_percent = "prop_dragon_knight_water_power_percent";
      public const string prop_dragon_knight_nature_power_base_value = "prop_dragon_knight_nature_power_base_value";
      public const string prop_dragon_knight_nature_power_percent = "prop_dragon_knight_nature_power_percent";
      public const string prop_dragon_knight_dungeon_stamina_base_value = "prop_dragon_knight_dungeon_stamina_base_value";
      public const string prop_dragon_knight_dungeon_stamina_value = "prop_dragon_knight_dungeon_stamina_value";
      public const string prop_dragon_knight_dungeon_stamina_percent = "prop_dragon_knight_dungeon_stamina_percent";
      public const string prop_dragon_knight_bag_capacity_base_value = "prop_dragon_knight_bag_capacity_base_value";
      public const string prop_dragon_knight_bag_capacity_value = "prop_dragon_knight_bag_capacity_value";
      public const string prop_dragon_knight_load_base_value = "prop_dragon_knight_load_base_value";
      public const string prop_dragon_knight_load_value = "prop_dragon_knight_load_value";
      public const string prop_dragon_knight_load_percent = "prop_dragon_knight_load_percent";
      public const string prop_dragon_knight_addition_stamina_count_base_value = "prop_dragon_knight_addition_stamina_count_base_value";
      public const string prop_dragon_knight_addition_stamina_count_value = "prop_dragon_knight_addition_stamina_count_value";
      public const string prop_dragon_knight_addition_mana_count_base_value = "prop_dragon_knight_addition_mana_count_base_value";
      public const string prop_dragon_knight_addition_mana_count_value = "prop_dragon_knight_addition_mana_count_value";
      public const string prop_dragon_knight_addition_health_count_base_value = "prop_dragon_knight_addition_health_count_base_value";
      public const string prop_dragon_knight_addition_health_count_value = "prop_dragon_knight_addition_health_count_value";
      public const string prop_dragon_knight_addition_stamina_effect_base_value = "prop_dragon_knight_addition_stamina_effect_base_value";
      public const string prop_dragon_knight_addition_stamina_effect_value = "prop_dragon_knight_addition_stamina_effect_value";
      public const string prop_dragon_knight_addition_stamina_effect_percent = "prop_dragon_knight_addition_stamina_effect_percent";
      public const string prop_dragon_knight_addition_mana_effect_base_value = "prop_dragon_knight_addition_mana_effect_base_value";
      public const string prop_dragon_knight_addition_mana_effect_value = "prop_dragon_knight_addition_mana_effect_value";
      public const string prop_dragon_knight_addition_mana_effect_percent = "prop_dragon_knight_addition_mana_effect_percent";
      public const string prop_dragon_knight_addition_health_effect_base_value = "prop_dragon_knight_addition_health_effect_base_value";
      public const string prop_dragon_knight_addition_health_effect_value = "prop_dragon_knight_addition_health_effect_value";
      public const string prop_dragon_knight_addition_health_effect_percent = "prop_dragon_knight_addition_health_effect_percent";
      public const string prop_dragon_knight_skill_equip_base_value = "prop_dragon_knight_skill_equip_base_value";
      public const string prop_dragon_knight_skill_equip_value = "prop_dragon_knight_skill_equip_value";
      public const string prop_dragon_knight_inbattle_strength_base_value = "prop_dragon_knight_inbattle_strength_base_value";
      public const string prop_dragon_knight_inbattle_strength_value = "prop_dragon_knight_inbattle_strength_value";
      public const string prop_dragon_knight_inbattle_strength_percent = "prop_dragon_knight_inbattle_strength_percent";
      public const string prop_dragon_knight_inbattle_intelligent_base_value = "prop_dragon_knight_inbattle_intelligent_base_value";
      public const string prop_dragon_knight_inbattle_intelligent_value = "prop_dragon_knight_inbattle_intelligent_value";
      public const string prop_dragon_knight_inbattle_intelligent_percent = "prop_dragon_knight_inbattle_intelligent_percent";
      public const string prop_dragon_knight_inbattle_armor_base_value = "prop_dragon_knight_inbattle_armor_base_value";
      public const string prop_dragon_knight_inbattle_armor_value = "prop_dragon_knight_inbattle_armor_value";
      public const string prop_dragon_knight_inbattle_armor_percent = "prop_dragon_knight_inbattle_armor_percent";
      public const string prop_dragon_knight_inbattle_constitution_base_value = "prop_dragon_knight_inbattle_constitution_base_value";
      public const string prop_dragon_knight_inbattle_constitution_value = "prop_dragon_knight_inbattle_constitution_value";
      public const string prop_dragon_knight_inbattle_constitution_percent = "prop_dragon_knight_inbattle_constitution_percent";
      public const string prop_dragon_knight_inbattle_attack_base_value = "prop_dragon_knight_inbattle_attack_base_value";
      public const string prop_dragon_knight_inbattle_attack_value = "prop_dragon_knight_inbattle_attack_value";
      public const string prop_dragon_knight_inbattle_attack_percent = "prop_dragon_knight_inbattle_attack_percent";
      public const string prop_dragon_knight_inbattle_magic_base_value = "prop_dragon_knight_inbattle_magic_base_value";
      public const string prop_dragon_knight_inbattle_magic_value = "prop_dragon_knight_inbattle_magic_value";
      public const string prop_dragon_knight_inbattle_magic_percent = "prop_dragon_knight_inbattle_magic_percent";
      public const string prop_dragon_knight_inbattle_defence_base_value = "prop_dragon_knight_inbattle_defence_base_value";
      public const string prop_dragon_knight_inbattle_defence_value = "prop_dragon_knight_inbattle_defence_value";
      public const string prop_dragon_knight_inbattle_defence_percent = "prop_dragon_knight_inbattle_defence_percent";
      public const string prop_dragon_knight_inbattle_fire_power_base_value = "prop_dragon_knight_inbattle_fire_power_base_value";
      public const string prop_dragon_knight_inbattle_fire_power_value = "prop_dragon_knight_inbattle_fire_power_value";
      public const string prop_dragon_knight_inbattle_fire_power_percent = "prop_dragon_knight_inbattle_fire_power_percent";
      public const string prop_dragon_knight_inbattle_water_power_base_value = "prop_dragon_knight_inbattle_water_power_base_value";
      public const string prop_dragon_knight_inbattle_water_power_value = "prop_dragon_knight_inbattle_water_power_value";
      public const string prop_dragon_knight_inbattle_water_power_percent = "prop_dragon_knight_inbattle_water_power_percent";
      public const string prop_dragon_knight_inbattle_nature_power_base_value = "prop_dragon_knight_inbattle_nature_power_base_value";
      public const string prop_dragon_knight_inbattle_nature_power_value = "prop_dragon_knight_inbattle_nature_power_value";
      public const string prop_dragon_knight_inbattle_nature_power_percent = "prop_dragon_knight_inbattle_nature_power_percent";
      public const string prop_dragon_knight_dungeon_cd_percent = "prop_dragon_knight_dungeon_cd_percent";
      public const string prop_dragon_knight_speed_percent = "prop_dragon_knight_speed_percent";
    }
  }
}
