﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonKnightDungeonDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonKnightDungeonDB
  {
    protected Dictionary<long, DragonKnightDungeonData> _table = new Dictionary<long, DragonKnightDungeonData>();

    public event System.Action<DragonKnightDungeonData> onDataChanged;

    public event System.Action<DragonKnightDungeonData> onDataUpdate;

    public event System.Action<DragonKnightDungeonData> onDataRemove;

    public event System.Action<DragonKnightDungeonData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (DragonKnightDungeonData), (long) this._table.Count);
    }

    public void Clear()
    {
      this._table.Clear();
    }

    private void Publish_OnDataChanged(DragonKnightDungeonData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(DragonKnightDungeonData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(DragonKnightDungeonData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(DragonKnightDungeonData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(long uid, long updateTime)
    {
      if (!this._table.ContainsKey(uid))
        return false;
      DragonKnightDungeonData data = this._table[uid];
      this._table.Remove(uid);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "uid", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public DragonKnightDungeonData Get(long uid)
    {
      if (this._table.ContainsKey(uid))
        return this._table[uid];
      return (DragonKnightDungeonData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      DragonKnightDungeonData data = new DragonKnightDungeonData();
      if (!data.Decode((object) hashtable, updateTime) || this._table.ContainsKey(data.UserId))
        return false;
      this._table.Add(data.UserId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData);
      if (!this._table.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      DragonKnightDungeonData data = this._table[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public DragonKnightDungeonData GetDragonKnightDungeonData(long uid = 0)
    {
      if (uid <= 0L)
        uid = PlayerData.inst.uid;
      using (Dictionary<long, DragonKnightDungeonData>.Enumerator enumerator = this._table.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, DragonKnightDungeonData> current = enumerator.Current;
          if (current.Value.UserId == uid)
            return current.Value;
        }
      }
      return (DragonKnightDungeonData) null;
    }
  }
}
