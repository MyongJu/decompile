﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonSkillDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonSkillDB
  {
    private Dictionary<long, List<long>> _dragonSkillByUid = new Dictionary<long, List<long>>();

    public event System.Action<long> onDataChanged;

    public event System.Action<long> onDataUpdate;

    public event System.Action<long> onDataRemove;

    public event System.Action<long> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (long), (long) this._dragonSkillByUid.Count);
    }

    public List<long> GetSkillDataByGroupId(int group_id)
    {
      List<long> longList1 = new List<long>();
      List<long> longList2;
      this._dragonSkillByUid.TryGetValue(PlayerData.inst.uid, out longList2);
      if (longList2 != null)
      {
        for (int index = 0; index < longList2.Count; ++index)
        {
          ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) longList2[index]);
          if (skillMainInfo != null && skillMainInfo.group_id == group_id)
            longList1.Add(longList2[index]);
        }
      }
      return longList1;
    }

    public Dictionary<long, List<long>> Datas
    {
      get
      {
        return this._dragonSkillByUid;
      }
    }

    private void Publish_OnDataChanged(long generalID)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(generalID);
    }

    private void Publish_OnDataCreate(long generalID)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataUpdate(long generalID)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(generalID);
      this.Publish_OnDataChanged(generalID);
    }

    private void Publish_OnDataRemove(long generalID)
    {
      this.Publish_OnDataChanged(generalID);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(generalID);
    }

    private bool Add(long uid, long skillID)
    {
      DragonSkillData dragonSkillData = new DragonSkillData(uid, skillID);
      if (!this._dragonSkillByUid.ContainsKey(dragonSkillData.Uid))
        this._dragonSkillByUid.Add(dragonSkillData.Uid, new List<long>());
      bool flag;
      if (!this._dragonSkillByUid[dragonSkillData.Uid].Contains(dragonSkillData.SkillId))
      {
        this._dragonSkillByUid[dragonSkillData.Uid].Add(dragonSkillData.SkillId);
        flag = true;
      }
      else
        flag = false;
      this.Publish_OnDataCreate(dragonSkillData.Uid);
      return flag;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      return false | this.Add(long.Parse(hashtable[(object) "uid"].ToString()), long.Parse(hashtable[(object) "skill_id"].ToString()));
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      long index = long.Parse(hashtable[(object) "uid"].ToString());
      long skillID = long.Parse(hashtable[(object) "skill_id"].ToString());
      DragonSkillData dragonSkillData = new DragonSkillData(index, skillID);
      if (!this._dragonSkillByUid.ContainsKey(index) || !this._dragonSkillByUid[index].Contains(skillID))
        return false;
      this._dragonSkillByUid[index].Remove(skillID);
      this.Publish_OnDataRemove(index);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(arrayList[index], updateTime);
    }

    public bool CanResetAllSKill()
    {
      List<long> longList;
      this._dragonSkillByUid.TryGetValue(PlayerData.inst.uid, out longList);
      if (longList != null)
      {
        for (int index = 0; index < longList.Count; ++index)
        {
          if (ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) longList[index]).level > 1)
            return true;
        }
      }
      return false;
    }

    public List<ConfigDragonSkillGroupInfo> GetLearnedDragonSkillGroupsByTypeAndUsability(string type, string usability)
    {
      List<ConfigDragonSkillGroupInfo> dragonSkillGroupInfoList = new List<ConfigDragonSkillGroupInfo>();
      HashSet<int> intSet = new HashSet<int>();
      List<long> longList;
      this._dragonSkillByUid.TryGetValue(PlayerData.inst.uid, out longList);
      if (longList != null)
      {
        for (int index = 0; index < longList.Count; ++index)
        {
          ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo((int) longList[index]);
          if (!intSet.Contains(skillMainInfo.group_id))
          {
            intSet.Add(skillMainInfo.group_id);
            ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
            bool flag = ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(dragonSkillGroupInfo.internalId);
            if (dragonSkillGroupInfo.type == type && dragonSkillGroupInfo.CanBeUsedAs(usability) && flag)
              dragonSkillGroupInfoList.Add(dragonSkillGroupInfo);
          }
        }
      }
      return dragonSkillGroupInfoList;
    }
  }
}
