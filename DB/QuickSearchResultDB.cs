﻿// Decompiled with JetBrains decompiler
// Type: DB.QuickSearchResultDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class QuickSearchResultDB
  {
    public Coordinate location;
    public long owner_id;
    public long march_id;
    public long alliance_id;
    public long block_id;
    public long city_id;
    public long world_id;
    public int resource_id;
    public int resource_type;
    public int resource_level;
    public int resource_amt;

    public void Decode(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      DatabaseTools.UpdateData(inData, "world_id", ref this.location.K);
      DatabaseTools.UpdateData(inData, "map_x", ref this.location.X);
      DatabaseTools.UpdateData(inData, "map_y", ref this.location.Y);
      DatabaseTools.UpdateData(inData, "alliance_id", ref this.alliance_id);
      DatabaseTools.UpdateData(inData, "block_id", ref this.block_id);
      DatabaseTools.UpdateData(inData, "city_id", ref this.city_id);
      DatabaseTools.UpdateData(inData, "resource_id", ref this.resource_id);
      DatabaseTools.UpdateData(inData, "resource_type", ref this.resource_type);
      DatabaseTools.UpdateData(inData, "resource_level", ref this.resource_level);
      DatabaseTools.UpdateData(inData, "resource_amt", ref this.resource_amt);
      DatabaseTools.UpdateData(inData, "owner_id", ref this.owner_id);
      DatabaseTools.UpdateData(inData, "march_id", ref this.march_id);
    }
  }
}
