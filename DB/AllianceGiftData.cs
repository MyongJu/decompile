﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceGiftData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class AllianceGiftData : BaseData
  {
    public const int INVALID_ID = 0;
    public int giftId;
    public long uid;
    public int level;
    public int typeId;
    public int expire;

    public bool Decode(Hashtable orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null)
        return false;
      Hashtable inData = orgData;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "gift_id", ref this.giftId) | DatabaseTools.UpdateData(inData, "uid", ref this.uid) | DatabaseTools.UpdateData(inData, "level", ref this.level) | DatabaseTools.UpdateData(inData, "type_id", ref this.typeId) | DatabaseTools.UpdateData(inData, "ctime", ref this.expire);
      this.expire += (int) ConfigManager.inst.DB_GameConfig.GetData("alliance_gift_duration").Value;
      return flag;
    }

    public static class Params
    {
      public const string KEY = "alliance_gift";
      public const string GIFT_ID = "gift_id";
      public const string UID = "uid";
      public const string LEVEL = "level";
      public const string TYPE_ID = "type_id";
      public const string CTIME = "ctime";
      public const string ALLIANCE_GIFT_DURATION = "alliance_gift_duration";
    }
  }
}
