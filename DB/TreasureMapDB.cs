﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasureMapDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TreasureMapDB
  {
    private Dictionary<long, TreasureMapData> _datas = new Dictionary<long, TreasureMapData>();

    public event System.Action<TreasureMapData> onDataChanged;

    public event System.Action<TreasureMapData> onDataUpdate;

    public event System.Action<TreasureMapData> onDataCreate;

    public event System.Action<TreasureMapData> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (TreasureMapData), (long) this._datas.Count);
    }

    private void PublishOnDataChangedEvent(TreasureMapData treasureMap)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(treasureMap);
    }

    private void PublishOnDataCreateEvent(TreasureMapData treasureMap)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(treasureMap);
      this.PublishOnDataChangedEvent(treasureMap);
    }

    private void PublishOnDataUpdateEvent(TreasureMapData treasureMap)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(treasureMap);
      this.PublishOnDataChangedEvent(treasureMap);
    }

    private void PublishOnDataRemovedEvent(TreasureMapData treasureMap)
    {
      if (this.onDataRemoved != null)
        this.onDataRemoved(treasureMap);
      this.PublishOnDataChangedEvent(treasureMap);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      TreasureMapData treasureMap = new TreasureMapData();
      if (!treasureMap.Decode((object) hashtable, updateTime) || this._datas.ContainsKey(treasureMap.MapId))
        return false;
      this._datas.Add(treasureMap.MapId, treasureMap);
      this.PublishOnDataCreateEvent(treasureMap);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "map_id", ref outData);
      if (!this._datas.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      TreasureMapData data = this._datas[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.PublishOnDataUpdateEvent(data);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long mapId, long updateTime)
    {
      if (!this._datas.ContainsKey(mapId))
        return false;
      TreasureMapData data = this._datas[mapId];
      this._datas.Remove(mapId);
      this.PublishOnDataRemovedEvent(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "map_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public TreasureMapData Get(long mapId)
    {
      TreasureMapData treasureMapData;
      this._datas.TryGetValue(mapId, out treasureMapData);
      return treasureMapData;
    }
  }
}
