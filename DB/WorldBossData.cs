﻿// Decompiled with JetBrains decompiler
// Type: DB.WorldBossData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class WorldBossData : BaseData
  {
    public const int INVALID_ID = 0;
    private int _bossId;
    private int _kingdomId;
    private int _configId;
    private long _placeTime;
    private int _x;
    private int _y;
    private long _totalTroopCount;
    private long _troopCount;
    private int _hp;

    public int bossId
    {
      get
      {
        return this._bossId;
      }
    }

    public int kingdomId
    {
      get
      {
        return this._kingdomId;
      }
    }

    public int configId
    {
      get
      {
        return this._configId;
      }
    }

    public long placeTime
    {
      get
      {
        return this._placeTime;
      }
    }

    public int HP
    {
      get
      {
        return this._hp;
      }
    }

    public float Progress
    {
      get
      {
        return (float) this._hp / 100f;
      }
    }

    public Coordinate Location
    {
      get
      {
        return new Coordinate()
        {
          K = this.kingdomId,
          X = this._x,
          Y = this._y
        };
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      return false | DatabaseTools.UpdateData(dataHt, "world_id", ref this._kingdomId) | DatabaseTools.UpdateData(dataHt, "config_id", ref this._configId) | DatabaseTools.UpdateData(dataHt, "ctime", ref this._placeTime) | DatabaseTools.UpdateData(dataHt, "map_x", ref this._x) | DatabaseTools.UpdateData(dataHt, "map_y", ref this._y) | DatabaseTools.UpdateData(dataHt, "boss_id", ref this._bossId) | DatabaseTools.UpdateData(dataHt, "start_troops", ref this._totalTroopCount) | DatabaseTools.UpdateData(dataHt, "troops", ref this._troopCount) | DatabaseTools.UpdateData(dataHt, "hp", ref this._hp);
    }

    public struct Params
    {
      public const string KEY = "festival_boss";
      public const string WORLD_MAP_KINGDOME_ID = "world_id";
      public const string WORLD_BOSS_CONFIG_ID = "config_id";
      public const string WORLD_BOSS_ID = "boss_id";
      public const string WORLD_BOSS_PLACE_TIME = "ctime";
      public const string WORLD_BOSS_X = "map_x";
      public const string WORLD_BOSS_Y = "map_y";
      public const string WORLD_BOSS_HP = "hp";
      public const string WORLD_TOTAL_TROOPS_COUNT = "start_troops";
      public const string WORLD_TROOPS_COUNT = "troops";
    }
  }
}
