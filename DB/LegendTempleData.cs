﻿// Decompiled with JetBrains decompiler
// Type: DB.LegendTempleData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class LegendTempleData : BaseData
  {
    private Dictionary<int, int> _firstUsed = new Dictionary<int, int>();
    private Dictionary<int, long> _freeCD = new Dictionary<int, long>();
    private long _uid;
    private int _locked;
    private int _specialSummonCount;

    public long Uid
    {
      get
      {
        return this._uid;
      }
    }

    public bool Locked
    {
      get
      {
        return this._locked == 1;
      }
    }

    public int SpecialSummonCount
    {
      get
      {
        return this._specialSummonCount;
      }
    }

    public Dictionary<int, int> FirstUsed
    {
      get
      {
        return this._firstUsed;
      }
    }

    public Dictionary<int, long> FreeCD
    {
      get
      {
        return this._freeCD;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "locked", ref this._locked) | DatabaseTools.UpdateData(inData, "special_summon_count", ref this._specialSummonCount);
      if (inData.ContainsKey((object) "first_used"))
      {
        this._firstUsed.Clear();
        IDictionaryEnumerator enumerator = (inData[(object) "first_used"] as Hashtable).GetEnumerator();
        while (enumerator.MoveNext())
          this._firstUsed[int.Parse(enumerator.Key.ToString())] = int.Parse(enumerator.Value.ToString());
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      if (inData.ContainsKey((object) "free_cd"))
      {
        this._freeCD.Clear();
        IDictionaryEnumerator enumerator = (inData[(object) "free_cd"] as Hashtable).GetEnumerator();
        while (enumerator.MoveNext())
          this._freeCD[int.Parse(enumerator.Key.ToString())] = long.Parse(enumerator.Value.ToString());
        flag = ((flag ? 1 : 0) | 1) != 0;
      }
      return flag;
    }

    public struct Params
    {
      public const string KEY = "legend_temple";
      public const string UID = "uid";
      public const string LOCKED = "locked";
      public const string FIRST_USED = "first_used";
      public const string FREE_CD = "free_cd";
      public const string SPECIAL_SUMMON_COUNT = "special_summon_count";
      public const string CTIME = "ctime";
      public const string MTIME = "mtime";
    }
  }
}
