﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceTechDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceTechDB
  {
    private SortedDictionary<int, AllianceTechData> _datas = new SortedDictionary<int, AllianceTechData>();
    private Dictionary<string, AllianceTechData> _curLevelByType = new Dictionary<string, AllianceTechData>();
    private Dictionary<int, List<string>> _typesByTier = new Dictionary<int, List<string>>();
    public const string ALLIANCE_TECH_DB_KEY = "alliance_tech";

    public event System.Action<int> onDataChanged;

    public event System.Action<int> onDataUpdate;

    public event System.Action<int> onDataCreate;

    public event System.Action<int> onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceTechData), (long) this._datas.Count);
    }

    public int researchingId
    {
      get
      {
        SortedDictionary<int, AllianceTechData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.jobId != 0L)
            return enumerator.Current.researchId;
        }
        return 0;
      }
    }

    public long researchingJobId
    {
      get
      {
        SortedDictionary<int, AllianceTechData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.jobId != 0L)
            return enumerator.Current.jobId;
        }
        return 0;
      }
    }

    public int readyResearchId
    {
      get
      {
        SortedDictionary<int, AllianceTechData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int researchId = enumerator.Current.researchId;
          AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(researchId);
          if (allianceTechInfo != null && enumerator.Current.donation >= allianceTechInfo.TotalExp)
            return researchId;
        }
        return 0;
      }
    }

    public void Publish_OnDataChanged(int allianceTechId)
    {
      this.UpdateLocalData(allianceTechId);
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(allianceTechId);
    }

    public void Publish_OnDataCreate(int allianceTechId)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(allianceTechId);
      this.Publish_OnDataChanged(allianceTechId);
    }

    public void Publish_OnDataUpdate(int allianceTechId)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(allianceTechId);
      this.Publish_OnDataChanged(allianceTechId);
    }

    public void Publish_OnDataRemoved(int allianceTechId)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(allianceTechId);
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      AllianceTechData allianceTechData = new AllianceTechData();
      if (!allianceTechData.Decode((object) data, updateTime) || allianceTechData.researchId == 0 || this._datas.ContainsKey(allianceTechData.researchId))
        return false;
      this._datas.Add(allianceTechData.researchId, allianceTechData);
      this.Publish_OnDataCreate(allianceTechData.researchId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      int index = this.CheckKeyFromData((object) data);
      if (index == 0)
        return false;
      if (!this._datas.ContainsKey(index))
        return this.Add(orgData, updateTime);
      if (!this._datas[index].Decode((object) data, updateTime))
        return false;
      this.Publish_OnDataUpdate(index);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public bool Remove(int id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(id);
      this.Publish_OnDataRemoved(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(this.CheckKeyFromData(arrayList[index]), updateTime);
    }

    public AllianceTechData Get(int id)
    {
      if (id == 0)
        return (AllianceTechData) null;
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (AllianceTechData) null;
    }

    public void Clear()
    {
      this._datas.Clear();
      this._curLevelByType.Clear();
      this._typesByTier.Clear();
    }

    private int CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return 0;
      if (orgData is int)
        return int.Parse(orgData.ToString());
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return 0;
      int result = 0;
      if (int.TryParse(hashtable[(object) "research_id"].ToString(), out result))
        return result;
      return 0;
    }

    private void UpdateLocalData(int researchId)
    {
      AllianceTechData allianceTechData = this.Get(researchId);
      if (this._curLevelByType.ContainsKey(allianceTechData.info.ResearchType))
      {
        if (allianceTechData.info.Level > this._curLevelByType[allianceTechData.info.ResearchType].info.Level)
          this._curLevelByType[allianceTechData.info.ResearchType] = allianceTechData;
      }
      else
        this._curLevelByType.Add(allianceTechData.info.ResearchType, allianceTechData);
      if (!this._typesByTier.ContainsKey(allianceTechData.info.TierIndex))
        this._typesByTier.Add(allianceTechData.info.TierIndex, new List<string>());
      if (this._typesByTier[allianceTechData.info.TierIndex].Contains(allianceTechData.info.ResearchType))
        return;
      this._typesByTier[allianceTechData.info.TierIndex].Add(allianceTechData.info.ResearchType);
    }

    public AllianceTechData GetCurAllicenTechByType(string AllianceTechType)
    {
      if (this._curLevelByType.ContainsKey(AllianceTechType))
        return this._curLevelByType[AllianceTechType];
      return (AllianceTechData) null;
    }

    public List<AllianceTechData> GetAllianceTechListByTier(int tier)
    {
      if (this._typesByTier.ContainsKey(tier))
        return (List<AllianceTechData>) null;
      List<string> stringList = this._typesByTier[tier];
      List<AllianceTechData> allianceTechDataList = new List<AllianceTechData>();
      for (int index = 0; index < stringList.Count; ++index)
        allianceTechDataList.Add(this.GetCurAllicenTechByType(stringList[index]));
      allianceTechDataList.Sort((Comparison<AllianceTechData>) ((a, b) => a.info.Index.CompareTo(b.info.Index)));
      return allianceTechDataList;
    }

    public List<int> GetAllianceTechIdListByTier(int tier)
    {
      List<AllianceTechInfo> techTypeByTier = ConfigManager.inst.DB_AllianceTech.GetTechTypeByTier(tier);
      if (techTypeByTier == null)
        return (List<int>) null;
      List<int> intList = new List<int>();
      for (int index = 0; index < techTypeByTier.Count; ++index)
      {
        AllianceTechData allicenTechByType = this.GetCurAllicenTechByType(techTypeByTier[index].ResearchType);
        if (allicenTechByType != null)
          intList.Add(allicenTechByType.researchId);
        else
          intList.Add(ConfigManager.inst.DB_AllianceTech.GetMinTechInfoByType(techTypeByTier[index].ResearchType).internalId);
      }
      return intList;
    }
  }
}
