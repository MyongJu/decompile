﻿// Decompiled with JetBrains decompiler
// Type: DB.DragonData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class DragonData : BaseData
  {
    private DragonData.DragonSkillCostsInfo _skill_cost = new DragonData.DragonSkillCostsInfo();
    private DragonData.DragonBenfitsInfo _benefits = new DragonData.DragonBenfitsInfo();
    private DragonData.DragonSkillInfo _skills = new DragonData.DragonSkillInfo();
    private Dictionary<int, int> _skillStarLevel = new Dictionary<int, int>();
    private Dictionary<int, int> _skillStarLevelCost = new Dictionary<int, int>();
    public const long INVALID_ID = 0;
    public const int INVALID_SKILL_ID = 0;
    public const int MAX_SKILL_COUNT = 3;
    private long _uid;
    private string _name;
    private long _lightPoint;
    private long _darkPoint;
    private long _xp;
    private long _power;
    private long _marchId;
    private int _is_default_name;
    private long _ctime;
    private int _level;
    private DragonData.Tendency _tendency;

    public event System.Action OnTendencyChangeEvent;

    public long Uid
    {
      get
      {
        return this._uid;
      }
      set
      {
        this._uid = value;
      }
    }

    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    public long LightPoint
    {
      get
      {
        return this._lightPoint;
      }
      set
      {
        this._lightPoint = value;
      }
    }

    public long DarkPoint
    {
      get
      {
        return this._darkPoint;
      }
      set
      {
        this._darkPoint = value;
      }
    }

    public long Xp
    {
      get
      {
        return this._xp;
      }
      set
      {
        this._xp = value;
      }
    }

    public long Power
    {
      get
      {
        return this._power;
      }
      set
      {
        this._power = value;
      }
    }

    public long MarchId
    {
      get
      {
        return this._marchId;
      }
      set
      {
        this._marchId = value;
      }
    }

    public int Is_default_name
    {
      get
      {
        return this._is_default_name;
      }
      set
      {
        this._is_default_name = value;
      }
    }

    public DragonData.DragonSkillCostsInfo Skill_cost
    {
      get
      {
        return this._skill_cost;
      }
    }

    public DragonData.DragonBenfitsInfo benefits
    {
      get
      {
        return this._benefits;
      }
    }

    public DragonData.DragonSkillInfo Skills
    {
      get
      {
        return this._skills;
      }
    }

    public long Ctime
    {
      get
      {
        return this._ctime;
      }
      set
      {
        this._ctime = value;
      }
    }

    public int Level
    {
      get
      {
        return this._level;
      }
    }

    public DragonData.Tendency tendency
    {
      private set
      {
        if (this._tendency == value)
          return;
        this._tendency = value;
        if (this.OnTendencyChangeEvent == null)
          return;
        this.OnTendencyChangeEvent();
      }
      get
      {
        return this._tendency;
      }
    }

    public float TendencyBonus
    {
      get
      {
        ConfigDragonTendencyBoostInfo data = ConfigManager.inst.DB_ConfigDragonTendencyBoost.GetData(this.tendency);
        global::DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(this.Level);
        if (data != null && dragonInfoByLevel != null)
          return data.display_value * dragonInfoByLevel.tendency_skill_rate;
        return 0.0f;
      }
    }

    public int Character
    {
      get
      {
        return 0;
      }
    }

    public string modelPath
    {
      get
      {
        return string.Empty;
      }
    }

    public string imagePath
    {
      get
      {
        return string.Empty;
      }
    }

    public string iconPath
    {
      get
      {
        return string.Empty;
      }
    }

    public string DragonType
    {
      get
      {
        return "Prefab/Dragon/test_dragon";
      }
    }

    public void Init(int uid, long power, long xp)
    {
      this._uid = (long) uid;
      this._power = power;
      this._xp = xp;
    }

    public bool HaveSkillGroupEnhanced()
    {
      using (Dictionary<int, int>.Enumerator enumerator = this._skillStarLevel.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value > 0)
            return true;
        }
      }
      return false;
    }

    public int GetSkillGroupStarLevel(int skillGroupId)
    {
      if (this._skillStarLevel.ContainsKey(skillGroupId))
        return this._skillStarLevel[skillGroupId];
      return 0;
    }

    public int GetSkillGroupStarLevelBySkillId(int skillId)
    {
      ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skillId);
      if (skillMainInfo != null)
        return this.GetSkillGroupStarLevel(skillMainInfo.group_id);
      return 0;
    }

    public int GetSkillGroupSrarLevelCost(int skillGroupId)
    {
      if (this._skillStarLevelCost.ContainsKey(skillGroupId))
        return this._skillStarLevelCost[skillGroupId];
      return 0;
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (updateTime < this._updateTime || orgData == null || long.TryParse(orgData.ToString(), out this._uid))
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "uid", ref this._uid) | DatabaseTools.UpdateData(inData, "name", ref this._name) | DatabaseTools.UpdateData(inData, "xp", ref this._xp) | DatabaseTools.UpdateData(inData, "level", ref this._level) | DatabaseTools.UpdateData(inData, "power", ref this._power) | DatabaseTools.UpdateData(inData, "light_point", ref this._lightPoint) | DatabaseTools.UpdateData(inData, "dark_point", ref this._darkPoint) | DatabaseTools.UpdateData(inData, "march_id", ref this._marchId) | DatabaseTools.UpdateData(inData, "is_default_name", ref this._is_default_name) | DatabaseTools.UpdateData(inData, "ctime", ref this._ctime);
      if (inData.ContainsKey((object) "skill_star"))
      {
        this._skillStarLevel.Clear();
        Hashtable hashtable = inData[(object) "skill_star"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            int result2 = 0;
            if (int.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
              this._skillStarLevel.Add(result1, result2);
          }
        }
        flag = true;
      }
      if (inData.ContainsKey((object) "skill_star_cost"))
      {
        this._skillStarLevelCost.Clear();
        Hashtable hashtable = inData[(object) "skill_star"] as Hashtable;
        if (hashtable != null)
        {
          IEnumerator enumerator = hashtable.Keys.GetEnumerator();
          while (enumerator.MoveNext())
          {
            int result1 = 0;
            int result2 = 0;
            if (int.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
              this._skillStarLevelCost.Add(result1, result2);
          }
        }
        flag = true;
      }
      if (inData.ContainsKey((object) "skill_cost"))
        flag |= this._skill_cost.Decode(inData[(object) "skill_cost"]);
      if (inData.ContainsKey((object) "skill"))
        flag |= this._skills.Decode(inData[(object) "skill"]);
      if (inData.ContainsKey((object) "benefits"))
        flag |= this._benefits.Decode(inData[(object) "benefits"]);
      if (inData.ContainsKey((object) "tendency"))
      {
        int outData = 0;
        flag |= DatabaseTools.UpdateData(inData, "tendency", ref outData);
        this.tendency = (DragonData.Tendency) outData;
      }
      global::DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(this._level);
      if (dragonInfoByLevel != null)
      {
        float num1 = (float) (this.Skill_cost.totalLightPoint + dragonInfoByLevel.tendency_min) / (float) (this.Skill_cost.totalPoint + 2 * dragonInfoByLevel.tendency_min);
        float num2 = (float) (this.Skill_cost.totalDarkPoint + dragonInfoByLevel.tendency_min) / (float) (this.Skill_cost.totalPoint + 2 * dragonInfoByLevel.tendency_min);
        this.Skill_cost.Light_Progress = num1;
        this.Skill_cost.Dark_Progress = num2;
      }
      if (!flag)
        ;
      return flag;
    }

    public string Show()
    {
      return "Dragon Data : \n" + "Uid : " + (object) this._uid + "\n" + "Name : " + this._name + "\n" + "XP : " + (object) this._xp + "\n" + "Level : " + (object) this._level + "\n" + "Power : " + (object) this._power + "\n" + "March : " + (object) this._marchId + "\n" + "Is default name : " + (object) this.Is_default_name + "\n" + this._skill_cost.ShowInfo() + this._benefits.ShowInfo();
    }

    public struct Params
    {
      public const string KEY = "user_dragon";
      public const string UID = "uid";
      public const string NAME = "name";
      public const string XP = "xp";
      public const string LEVEL = "level";
      public const string POWER = "power";
      public const string LIGHT_POINT = "light_point";
      public const string DARK_POINT = "dark_point";
      public const string SKILL = "skill";
      public const string MARCH_ID = "march_id";
      public const string IS_DEFAULT_NAME = "is_default_name";
      public const string SKILL_COST = "skill_cost";
      public const string BENEFITS = "benefits";
      public const string C_TIME = "ctime";
      public const string M_TIME = "mtime";
      public const string SKILL_STAR_LEVEL = "skill_star";
      public const string SKILL_STAR_LEVEL_COST = "skill_star_cost";
      public const string TENDENCY = "tendency";
    }

    public enum Tendency
    {
      normal,
      ferocious,
      swift,
      pure,
      shine,
    }

    public class DragonBenfitsInfo
    {
      public Dictionary<string, Dictionary<int, float>> datas = new Dictionary<string, Dictionary<int, float>>();

      public bool Decode(object orgData)
      {
        Hashtable data;
        if (!BaseData.CheckAndParseOrgData(orgData, out data))
          return false;
        bool flag = false | this.ChangeHashToDic(data, "attack") | this.ChangeHashToDic(data, "attack_monster") | this.ChangeHashToDic(data, "gather") | this.ChangeHashToDic(data, "defend");
        return true;
      }

      public float GetBenefit(int key)
      {
        Dictionary<string, Dictionary<int, float>>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current != null && enumerator.Current.ContainsKey(key))
            return enumerator.Current[key];
        }
        return 0.0f;
      }

      public Dictionary<int, float> GetBenefits(string type)
      {
        if (this.datas.ContainsKey(type))
          return this.datas[type];
        return (Dictionary<int, float>) null;
      }

      private bool ChangeHashToDic(Hashtable data, string type)
      {
        if (!this.datas.ContainsKey(type))
          this.datas.Add(type, new Dictionary<int, float>());
        this.datas[type].Clear();
        Hashtable data1;
        if (!BaseData.CheckAndParseOrgData(data[(object) type], out data1))
          return false;
        IEnumerator enumerator = data1.Keys.GetEnumerator();
        while (enumerator.MoveNext())
          this.datas[type].Add(int.Parse(enumerator.Current.ToString()), float.Parse(data1[enumerator.Current].ToString()));
        return true;
      }

      public string ShowInfo()
      {
        string str = "Dragon Benefit : \n";
        using (Dictionary<string, Dictionary<int, float>>.KeyCollection.Enumerator enumerator1 = this.datas.Keys.GetEnumerator())
        {
          while (enumerator1.MoveNext())
          {
            string current1 = enumerator1.Current;
            str = str + "\t" + current1 + " : \n";
            using (Dictionary<int, float>.KeyCollection.Enumerator enumerator2 = this.datas[current1].Keys.GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                int current2 = enumerator2.Current;
                str = str + "\t\t" + (object) current2 + " : " + (object) this.datas[current1][current2] + "\n";
              }
            }
          }
        }
        return str;
      }

      public struct Params
      {
        public const string DRAGON_BENEFIT_GATHER = "gather";
        public const string DRAGON_BENEFIT_ATTACK_MONSTER = "attack_monster";
        public const string DRAGON_BENEFIT_ATTACK = "attack";
        public const string DRAGON_BENEFIT_DEFEND = "defend";
      }
    }

    public class DragonSkillInfo
    {
      public Dictionary<string, List<int>> datas = new Dictionary<string, List<int>>();

      public bool IsSetupSkill(int skill_id)
      {
        Dictionary<string, List<int>>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Contains(skill_id))
            return true;
        }
        return false;
      }

      public bool IsSetupSkill(string usability, int skill_id)
      {
        List<int> skillBar = this.GetSkillBar(usability);
        if (skillBar != null)
          return skillBar.Contains(skill_id);
        return false;
      }

      public List<int> GetSkillBar(string usability)
      {
        List<int> intList = (List<int>) null;
        this.datas.TryGetValue(usability, out intList);
        return intList;
      }

      public bool Decode(object orgData)
      {
        Hashtable data;
        if (!BaseData.CheckAndParseOrgData(orgData, out data))
          return false;
        this.ChangeHashToDic(data, "gather");
        this.ChangeHashToDic(data, "attack");
        this.ChangeHashToDic(data, "attack_monster");
        this.ChangeHashToDic(data, "defend");
        return true;
      }

      private void ChangeHashToDic(Hashtable ht, string type)
      {
        List<int> intList = new List<int>();
        ArrayList arrayList = ht[(object) type] as ArrayList;
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
          {
            string s = arrayList[index] == null ? "0" : arrayList[index].ToString();
            int result = 0;
            if (int.TryParse(s, out result))
              intList.Add(result);
          }
        }
        this.datas[type] = intList;
      }

      public struct Params
      {
        public const string DRAGON_BENEFIT_GATHER = "gather";
        public const string DRAGON_BENEFIT_ATTACK_MONSTER = "attack_monster";
        public const string DRAGON_BENEFIT_ATTACK = "attack";
        public const string DRAGON_BENEFIT_DEFEND = "defend";
      }
    }

    public class DragonSkillCostsInfo
    {
      public Dictionary<int, DragonData.DragonSkillCost> datas = new Dictionary<int, DragonData.DragonSkillCost>();
      private float dark_progress = 0.5f;
      private float light_progress = 0.5f;
      public int totalLightPoint;
      public int totalDarkPoint;
      public int totalPoint;

      public float Dark_Progress
      {
        get
        {
          return this.dark_progress;
        }
        set
        {
          this.dark_progress = value;
        }
      }

      public float Light_Progress
      {
        get
        {
          return this.light_progress;
        }
        set
        {
          this.light_progress = value;
        }
      }

      public bool Decode(object orgData)
      {
        this.totalLightPoint = this.totalDarkPoint = 0;
        this.totalPoint = 0;
        Hashtable data;
        if (!BaseData.CheckAndParseOrgData(orgData, out data))
          return false;
        IEnumerator enumerator = data.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result;
          if (!int.TryParse(enumerator.Current.ToString(), out result))
            return false;
          if (!this.datas.ContainsKey(result))
            this.datas.Add(result, new DragonData.DragonSkillCost());
          this.datas[result].Decode(data[enumerator.Current], result);
          this.totalLightPoint += this.datas[result].lightPoint;
          this.totalDarkPoint += this.datas[result].darkPoint;
        }
        this.totalPoint = this.totalLightPoint + this.totalDarkPoint;
        return true;
      }

      public DragonData.DragonSkillCost Get(int skillId)
      {
        if (this.datas.ContainsKey(skillId))
          return this.datas[skillId];
        return (DragonData.DragonSkillCost) null;
      }

      public string ShowInfo()
      {
        string str = "Point cost \n";
        Dictionary<int, DragonData.DragonSkillCost>.KeyCollection.Enumerator enumerator = this.datas.Keys.GetEnumerator();
        while (enumerator.MoveNext())
          str = str + enumerator.Current.ToString() + ":" + this.datas[enumerator.Current].ShowInfo() + "\n";
        return str;
      }
    }

    public class DragonSkillCost
    {
      public int skillId;
      public int lightPoint;
      public int darkPoint;

      public bool Decode(object orgData, int newSkillId)
      {
        this.skillId = newSkillId;
        Hashtable data;
        if (!BaseData.CheckAndParseOrgData(orgData, out data))
          return false;
        return false | DatabaseTools.UpdateData(data, "light_point", ref this.lightPoint) | DatabaseTools.UpdateData(data, "dark_point", ref this.darkPoint);
      }

      public string ShowInfo()
      {
        return string.Format("L : {0} <-> D : {1}", (object) this.lightPoint, (object) this.darkPoint);
      }

      public struct Params
      {
        public const string LIGHT_POINT = "light_point";
        public const string DARK_POINT = "dark_point";
      }
    }
  }
}
