﻿// Decompiled with JetBrains decompiler
// Type: DB.InventoryItemData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class InventoryItemData : BaseData
  {
    public InlaidGems inlaidGems = new InlaidGems();
    public const int INVALID_ID = 0;
    private BagType _bagType;
    private InventoryItemIdentifier _identifier;
    private int _internalId;
    private int _enhanced;
    private int _equipped;
    private int _inSale;
    private int _sellCooldownTime;

    public InventoryItemData()
    {
    }

    public InventoryItemData(BagType bagType)
    {
      this._bagType = bagType;
    }

    public event System.Action OnChanged;

    public BagType bagType
    {
      get
      {
        return this._bagType;
      }
      set
      {
        this._bagType = value;
      }
    }

    public long uid
    {
      get
      {
        return this._identifier.uid;
      }
      set
      {
        this._identifier.uid = value;
      }
    }

    public long itemId
    {
      get
      {
        return this._identifier.itemId;
      }
      set
      {
        this._identifier.itemId = value;
      }
    }

    public InventoryItemIdentifier identifier
    {
      get
      {
        return this._identifier;
      }
    }

    public int internalId
    {
      get
      {
        return this._internalId;
      }
      set
      {
        this._internalId = value;
      }
    }

    public int enhanced
    {
      get
      {
        return this._enhanced;
      }
      set
      {
        this._enhanced = value;
      }
    }

    public int equipped
    {
      get
      {
        return this._equipped;
      }
      set
      {
        this._equipped = value;
      }
    }

    public bool InSale
    {
      get
      {
        return this._inSale == 1;
      }
    }

    public int SellCooldownTime
    {
      get
      {
        return this._sellCooldownTime;
      }
    }

    public bool IsFreeze
    {
      get
      {
        return NetServerTime.inst.ServerTimestamp <= this.SellCooldownTime;
      }
    }

    public string FreezeTime
    {
      get
      {
        return string.Empty;
      }
    }

    public string FreezeHint
    {
      get
      {
        return ScriptLocalization.GetWithPara("exchange_hall_item_frozen_description", new Dictionary<string, string>()
        {
          {
            "0",
            TradingHallPayload.Instance.HowManyDays(this.SellCooldownTime - NetServerTime.inst.ServerTimestamp).ToString()
          }
        }, true);
      }
    }

    public ConfigEquipmentMainInfo equipment
    {
      get
      {
        return ConfigManager.inst.GetEquipmentMain(this._bagType).GetData(this._internalId);
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      if (!this.CheckAndResetUpdateTime(updateTime) || orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | this._identifier.Decode(orgData) | DatabaseTools.UpdateData(inData, "item_property_id", ref this._internalId) | DatabaseTools.UpdateData(inData, "enhanced", ref this._enhanced) | DatabaseTools.UpdateData(inData, "equiped", ref this._equipped) | DatabaseTools.UpdateData(inData, "in_sale", ref this._inSale) | DatabaseTools.UpdateData(inData, "sell_cd_time", ref this._sellCooldownTime);
      if (inData.ContainsKey((object) "inlaid_gem"))
        flag |= this.inlaidGems.Decode(inData[(object) "inlaid_gem"]);
      return flag;
    }

    public bool isEquipped
    {
      get
      {
        return this._equipped != 0;
      }
    }

    public struct Params
    {
      public const string KEY = "user_inventory";
      public const string DK_KEY = "dragon_knight_inventory";
      public const string INVENTORY_ITEM_INTERNAL_ID = "item_property_id";
      public const string INVENTORY_ITEM_ENHANCED = "enhanced";
      public const string INVENTORY_ITEM_EQUIPPED = "equiped";
      public const string INLAID_GEM = "inlaid_gem";
      public const string INVENTORY_ITEM_IN_SALE = "in_sale";
      public const string INVENTORY_ITEM_SELL_CD_TIME = "sell_cd_time";
    }
  }
}
