﻿// Decompiled with JetBrains decompiler
// Type: DB.DailyStreak
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct DailyStreak
  {
    public int streak;
    public DailyStreak.ChestStatus status;
    public int startTime;

    public void Decode(object orgData)
    {
      this.Clear();
      if (orgData == null)
        return;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      DatabaseTools.UpdateData(inData, "streak", ref this.streak);
      DatabaseTools.UpdateData(inData, "timestamp", ref this.startTime);
    }

    public void Clear()
    {
      this.streak = 0;
      this.status = DailyStreak.ChestStatus.unavailable;
      this.startTime = 0;
    }

    public enum ChestStatus
    {
      opened,
      unopened,
      unavailable,
    }

    public struct Params
    {
      public const string STREAK = "streak";
      public const string CHEST_STATUS = "chest_status";
      public const string START_TIME = "timestamp";
    }
  }
}
