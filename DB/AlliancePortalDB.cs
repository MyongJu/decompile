﻿// Decompiled with JetBrains decompiler
// Type: DB.AlliancePortalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AlliancePortalDB
  {
    protected Dictionary<long, AlliancePortalData> _tableData = new Dictionary<long, AlliancePortalData>();

    public event System.Action<AlliancePortalData> onDataChanged;

    public event System.Action<AlliancePortalData> onDataUpdate;

    public event System.Action<AlliancePortalData> onDataRemove;

    public event System.Action<AlliancePortalData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AlliancePortalData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(AlliancePortalData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(AlliancePortalData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(AlliancePortalData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(AlliancePortalData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long alliancePortalId, long updateTime)
    {
      if (!this._tableData.ContainsKey(alliancePortalId))
        return false;
      AlliancePortalData data = this._tableData[alliancePortalId];
      this._tableData.Remove(alliancePortalId);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "building_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AlliancePortalData Get(long alliancePortalId)
    {
      if (this._tableData.ContainsKey(alliancePortalId))
        return this._tableData[alliancePortalId];
      return (AlliancePortalData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AlliancePortalData data = new AlliancePortalData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.PortalId))
        return false;
      this._tableData.Add(data.PortalId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "building_id", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AlliancePortalData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public AlliancePortalData GetAlliancePortalData()
    {
      using (Dictionary<long, AlliancePortalData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AlliancePortalData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AlliancePortalData) null;
    }

    public AlliancePortalData GetAlliancePortalDataByBuildingConfigId(int allianceBuildingConfigId)
    {
      using (Dictionary<long, AlliancePortalData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AlliancePortalData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId && current.Value.ConfigId == allianceBuildingConfigId)
            return current.Value;
        }
      }
      return (AlliancePortalData) null;
    }

    public AlliancePortalData GetDataByCoordinate(Coordinate location)
    {
      using (Dictionary<long, AlliancePortalData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AlliancePortalData> current = enumerator.Current;
          if (current.Value.Location.K == location.K && current.Value.Location.X == location.X && current.Value.Location.Y == location.Y)
            return current.Value;
        }
      }
      return (AlliancePortalData) null;
    }
  }
}
