﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceWarehouseDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceWarehouseDB
  {
    protected Dictionary<long, AllianceWareHouseData> _tableData = new Dictionary<long, AllianceWareHouseData>();

    public event System.Action<AllianceWareHouseData> onDataChanged;

    public event System.Action<AllianceWareHouseData> onDataUpdate;

    public event System.Action<AllianceWareHouseData> onDataRemove;

    public event System.Action<AllianceWareHouseData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceWareHouseData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(AllianceWareHouseData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(AllianceWareHouseData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(AllianceWareHouseData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(AllianceWareHouseData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long warehouseId, long updateTime)
    {
      if (!this._tableData.ContainsKey(warehouseId))
        return false;
      AllianceWareHouseData data = this._tableData[warehouseId];
      this._tableData.Remove(warehouseId);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        long outData = 0;
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "building_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AllianceWareHouseData Get(long warehouesId)
    {
      if (this._tableData.ContainsKey(warehouesId))
        return this._tableData[warehouesId];
      return (AllianceWareHouseData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceWareHouseData data = new AllianceWareHouseData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.WarehouseId))
        return false;
      this._tableData.Add(data.WarehouseId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "building_id", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceWareHouseData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public AllianceWareHouseData GetMyWarehouseData()
    {
      using (Dictionary<long, AllianceWareHouseData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceWareHouseData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceWareHouseData) null;
    }

    public AllianceWareHouseData GetMyWarehouseDataByBuildingConfigId(int allianceBuildingConfigId)
    {
      using (Dictionary<long, AllianceWareHouseData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceWareHouseData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId && current.Value.ConfigId == allianceBuildingConfigId)
            return current.Value;
        }
      }
      return (AllianceWareHouseData) null;
    }

    public AllianceWareHouseData GetWarehouseDataByCoordinate(Coordinate coordinate)
    {
      using (Dictionary<long, AllianceWareHouseData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceWareHouseData> current = enumerator.Current;
          if (current.Value.MapX == coordinate.X && current.Value.MapY == coordinate.Y && current.Value.WorldId == coordinate.K)
            return current.Value;
        }
      }
      return (AllianceWareHouseData) null;
    }
  }
}
