﻿// Decompiled with JetBrains decompiler
// Type: DB.TimerChestLocalDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class TimerChestLocalDB
  {
    public const string KEY = "user_timer_chest";
    public const string UID_KEY = "uid";
    public const string UPDATE_TIME_KEY = "active_time";
    private long _updateTime;
    private int _chestUpdateTime;

    public int chestUpdateTime
    {
      get
      {
        return this._chestUpdateTime;
      }
    }

    public bool Update(object orgData, long updateTime)
    {
      if (this._updateTime >= updateTime)
        return false;
      this._updateTime = updateTime;
      Hashtable data;
      if (DatabaseTools.CheckAndParseOrgData(orgData, out data) && this.CheckUid(data))
        return DatabaseTools.UpdateData(data, "active_time", ref this._chestUpdateTime);
      return false;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool CheckUid(Hashtable data)
    {
      if (data.Contains((object) "uid"))
        return long.Parse(data[(object) "uid"].ToString()) == PlayerData.inst.uid;
      return false;
    }
  }
}
