﻿// Decompiled with JetBrains decompiler
// Type: DB.TimeDurationInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct TimeDurationInfo
  {
    public int startTime;
    public int endTime;
    private int _timeDuration;
    private float _oneOverTimeDuration;

    public TimeDurationInfo(TimeDurationInfo orgData)
    {
      this.startTime = orgData.startTime;
      this.endTime = orgData.endTime;
      this._timeDuration = orgData.duration;
      this._oneOverTimeDuration = orgData.oneOverTimeDuration;
    }

    public int duration
    {
      get
      {
        return this._timeDuration;
      }
    }

    public float oneOverTimeDuration
    {
      get
      {
        return this._oneOverTimeDuration;
      }
    }

    public bool Decode(object orgData)
    {
      if (orgData == null)
        return false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      bool flag = false | DatabaseTools.UpdateData(inData, "start_time", ref this.startTime) | DatabaseTools.UpdateData(inData, "end_time", ref this.endTime);
      this._timeDuration = this.endTime - this.startTime;
      this._oneOverTimeDuration = 1f / (float) this._timeDuration;
      return flag;
    }

    public bool Decode(object startTimeData, object endTimeData)
    {
      bool flag = false;
      if (startTimeData != null && int.TryParse(startTimeData.ToString(), out this.startTime))
        flag = true;
      if (endTimeData != null && int.TryParse(endTimeData.ToString(), out this.endTime))
        flag = true;
      if (flag)
      {
        this._timeDuration = this.endTime - this.startTime;
        this._oneOverTimeDuration = 1f / (float) this._timeDuration;
      }
      return flag;
    }

    public bool Update(int inStartTime, int inEndTime)
    {
      this.startTime = inStartTime;
      this.endTime = inEndTime;
      this._timeDuration = this.endTime - this.startTime;
      this._oneOverTimeDuration = 1f / (float) this._timeDuration;
      return true;
    }

    public struct Params
    {
      public const string START_TIME = "start_time";
      public const string END_TIME = "end_time";
    }
  }
}
