﻿// Decompiled with JetBrains decompiler
// Type: DB.InventoryDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class InventoryDB
  {
    private Dictionary<InventoryItemIdentifier, InventoryItemData> DataByIdentifier = new Dictionary<InventoryItemIdentifier, InventoryItemData>();
    public System.Action<long, long> onDataChanged;
    public System.Action<long, long> onDataCreated;
    public System.Action<long, long> onDataUpdated;
    public System.Action<long, long> onDataRemove;
    public System.Action<long, long> onDataRemoved;
    private BagType _bagType;

    public InventoryDB(BagType bagType)
    {
      this._bagType = bagType;
    }

    public BagType bagType
    {
      get
      {
        return this._bagType;
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (InventoryItemData), (long) this.DataByIdentifier.Count);
    }

    private void Publish_onDataChanged(long uid, long itemId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(uid, itemId);
    }

    private void Publish_onDataCreated(long uid, long itemId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(uid, itemId);
      this.Publish_onDataChanged(uid, itemId);
    }

    private void Publish_onDataUpdated(long uid, long itemId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(uid, itemId);
      this.Publish_onDataChanged(uid, itemId);
    }

    private void Publish_onDataRemove(long uid, long itemId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(uid, itemId);
      this.Publish_onDataChanged(uid, itemId);
    }

    private void Publish_onDataRemoved(long uid, long itemId)
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved(uid, itemId);
    }

    public int GetCount(int quality)
    {
      int num = 0;
      Dictionary<InventoryItemIdentifier, InventoryItemData>.ValueCollection.Enumerator enumerator = this.DataByIdentifier.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this._bagType).GetData(enumerator.Current.internalId);
        if (data != null && data.quality == quality)
          ++num;
      }
      return num;
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      InventoryItemData inventoryItemData = new InventoryItemData(this._bagType);
      if (!inventoryItemData.Decode((object) hashtable, updateTime) || inventoryItemData.internalId == 0)
        return false;
      this.DataByIdentifier.Add(inventoryItemData.identifier, inventoryItemData);
      this.Publish_onDataCreated(inventoryItemData.uid, inventoryItemData.itemId);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      InventoryItemIdentifier key = new InventoryItemIdentifier();
      key.Decode((object) hashtable);
      if (!key.IsValid)
        return false;
      if (!this.DataByIdentifier.ContainsKey(key))
        return this.Add(orgData, updateTime);
      if (!this.DataByIdentifier[key].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated(key.uid, key.itemId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int count = arrayList.Count;
      for (int index = 0; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(InventoryItemIdentifier identifier, long updateTime)
    {
      if (!this.DataByIdentifier.ContainsKey(identifier) || !this.DataByIdentifier[identifier].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(identifier.uid, identifier.itemId);
      this.DataByIdentifier.Remove(identifier);
      this.Publish_onDataRemoved(identifier.uid, identifier.itemId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        InventoryItemIdentifier identifier = new InventoryItemIdentifier();
        identifier.Decode(arrayList[index]);
        this.Remove(identifier, updateTime);
      }
    }

    public InventoryItemData GetByInternalID(long uid, long internalID)
    {
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.uid == uid && (long) inventoryItemData.equipment.internalId == internalID)
          return inventoryItemData;
      }
      return (InventoryItemData) null;
    }

    public InventoryItemData GetByItemID(long uid, long itemId)
    {
      InventoryItemData inventoryItemData;
      this.DataByIdentifier.TryGetValue(new InventoryItemIdentifier()
      {
        uid = uid,
        itemId = itemId
      }, out inventoryItemData);
      return inventoryItemData;
    }

    public InventoryItemData GetEquippedItemByType(long uid, int type)
    {
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.uid == uid && inventoryItemData.isEquipped && inventoryItemData.equipment.type == type)
          return inventoryItemData;
      }
      return (InventoryItemData) null;
    }

    public List<InventoryItemData> GetEquippedItemsByID(long uid)
    {
      List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.uid == uid && inventoryItemData.isEquipped)
          inventoryItemDataList.Add(inventoryItemData);
      }
      return inventoryItemDataList;
    }

    public InventoryItemData GetMyItemByItemID(long itemId)
    {
      return this.GetByItemID(PlayerData.inst.uid, itemId);
    }

    public InventoryItemData GetMyItemByInternalID(long internalID)
    {
      return this.GetByInternalID(PlayerData.inst.uid, internalID);
    }

    public bool HasMyEquipment(int equipmentID)
    {
      return this.GetMyItemByInternalID((long) equipmentID) != null;
    }

    public bool HasMyEquipmentByType(int type)
    {
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.equipment.type == type && inventoryItemData.uid == PlayerData.inst.uid && !inventoryItemData.InSale)
          return true;
      }
      return false;
    }

    public List<InventoryItemData> GetMyItemListByType(int type)
    {
      long uid = PlayerData.inst.uid;
      List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.equipment.type == type && inventoryItemData.uid == uid && !inventoryItemData.InSale)
          inventoryItemDataList.Add(inventoryItemData);
      }
      return inventoryItemDataList;
    }

    public List<InventoryItemData> GetMyItemList()
    {
      long uid = PlayerData.inst.uid;
      List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
      Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator();
      while (enumerator.MoveNext())
      {
        InventoryItemData inventoryItemData = enumerator.Current.Value;
        if (inventoryItemData.uid == uid)
          inventoryItemDataList.Add(inventoryItemData);
      }
      return inventoryItemDataList;
    }

    public void ClearThirdPartyInventory()
    {
      List<InventoryItemIdentifier> inventoryItemIdentifierList = new List<InventoryItemIdentifier>();
      using (Dictionary<InventoryItemIdentifier, InventoryItemData>.Enumerator enumerator = this.DataByIdentifier.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<InventoryItemIdentifier, InventoryItemData> current = enumerator.Current;
          if (current.Key.uid != PlayerData.inst.uid)
            inventoryItemIdentifierList.Add(current.Key);
        }
      }
      for (int index = 0; index < inventoryItemIdentifierList.Count; ++index)
        this.DataByIdentifier.Remove(inventoryItemIdentifierList[index]);
    }
  }
}
