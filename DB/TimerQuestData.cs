﻿// Decompiled with JetBrains decompiler
// Type: DB.TimerQuestData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TimerQuestData : BaseData
  {
    public const int INVALID_ID = 0;
    private long _questId;
    private int _internalId;
    private QuestRewardAgent _agent;
    private BasePresent _present;
    private int _type;
    private int _status;
    private int _randomRewardId;
    private long _jobId;

    public long questId
    {
      get
      {
        return this._questId;
      }
      set
      {
        this._questId = value;
      }
    }

    public int internalId
    {
      get
      {
        return this._internalId;
      }
    }

    public TimerQuestData.QuestType type
    {
      get
      {
        return (TimerQuestData.QuestType) this._type;
      }
    }

    public TimerQuestData.QuestStatus status
    {
      get
      {
        return (TimerQuestData.QuestStatus) this._status;
      }
      set
      {
        this._status = (int) value;
      }
    }

    public int randomRewardId
    {
      get
      {
        return this._randomRewardId;
      }
    }

    public long jobId
    {
      get
      {
        return this._jobId;
      }
      set
      {
        this._jobId = value;
      }
    }

    public bool Decode(object orgData, long updateTime)
    {
      Hashtable dataHt;
      if (!this.CheckOrgDataAndUpdateTime(orgData, updateTime, out dataHt))
        return false;
      bool flag1 = false | DatabaseTools.UpdateData(dataHt, "quest_id", ref this._questId) | DatabaseTools.UpdateData(dataHt, "internal_id", ref this._internalId) | DatabaseTools.UpdateData(dataHt, "type", ref this._type);
      int outData = 0;
      bool flag2 = flag1 | DatabaseTools.UpdateData(dataHt, "status", ref outData);
      if (outData == 3 && this._status == 2)
        PlayerData.inst.QuestManager.Public_AllianceQuestEnd(-1L);
      this._status = outData;
      return flag2 | DatabaseTools.UpdateData(dataHt, "job_id", ref this._jobId);
    }

    public static int GetQuestId(object orgData)
    {
      Hashtable data;
      if (BaseData.CheckAndParseOrgData(orgData, out data) && data[(object) "quest_id"] != null)
        return int.Parse(data[(object) "quest_id"].ToString());
      return 0;
    }

    public QuestRewardAgent RewardAgent
    {
      get
      {
        if (this._agent == null)
          this._agent = new QuestRewardAgent(this.questId);
        return this._agent;
      }
    }

    public List<QuestReward> Rewards
    {
      get
      {
        return this.RewardAgent.Rewards;
      }
    }

    public BasePresent Present
    {
      get
      {
        if (this._present == null)
          this._present = BasePresent.Create(ConfigManager.inst.DB_Quest.GetData(this.internalId).Category, this.internalId, this.questId);
        return this._present;
      }
    }

    public struct Params
    {
      public const string KEY = "timer_quest";
      public const string TIMER_QUEST_QUEST_ID = "quest_id";
      public const string TIMER_QUEST_INTERNAL_ID = "internal_id";
      public const string TIMER_QUEST_TYPE = "type";
      public const string TIMER_QUEST_STATUS = "status";
      public const string TIMER_QUEST_JOB_ID = "job_id";
      public const string UID = "uid";
    }

    public enum QuestType
    {
      TYPE_EMPIRE = 1,
      TYPE_DAILY = 2,
      TYPE_DAILY_CHEST = 3,
      TYPE_ALLIANCE = 4,
      TYPE_ALLIANCE_CHEST = 5,
      TYPE_SPECIAL = 6,
    }

    public enum QuestStatus
    {
      STATUS_INACTIVE = 1,
      STATUS_ACTIVE = 2,
      STATUS_COMPLETED = 3,
      STATUS_COLLECTED = 4,
    }
  }
}
