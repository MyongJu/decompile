﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceGiftDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceGiftDB
  {
    private Dictionary<long, AllianceGiftData> _datas = new Dictionary<long, AllianceGiftData>();

    public event System.Action<AllianceGiftData> onDataChanged;

    public event System.Action<AllianceGiftData> onDataUpdate;

    public event System.Action<AllianceGiftData> onDataRemove;

    public event System.Action<AllianceGiftData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceGiftData), (long) this._datas.Count);
    }

    private void Publish_OnDataChanged(AllianceGiftData gift)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(gift);
    }

    private void Publish_OnDataCreate(AllianceGiftData gift)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(gift);
      this.Publish_OnDataChanged(gift);
    }

    private void Publish_OnDataUpdate(AllianceGiftData gift)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(gift);
      this.Publish_OnDataChanged(gift);
    }

    private void Publish_OnDataRemove(AllianceGiftData gift)
    {
      this.Publish_OnDataChanged(gift);
      if (this.onDataRemove == null)
        return;
      this.onDataRemove(gift);
    }

    private bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable orgData1 = orgData as Hashtable;
      if (orgData1 == null)
        return false;
      AllianceGiftData gift = new AllianceGiftData();
      if (!gift.Decode(orgData1, updateTime) || gift.giftId == 0 || this._datas.ContainsKey((long) gift.giftId))
        return false;
      this._datas.Add((long) gift.giftId, gift);
      this.Publish_OnDataCreate(gift);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable orgData1 = orgData as Hashtable;
      if (orgData1 == null)
        return false;
      long key = this.CheckKeyFromData((object) orgData1);
      if (key == 0L)
        return false;
      if (!this._datas.ContainsKey(key))
        return this.Add(orgData, updateTime);
      if (!this._datas[key].Decode(orgData1, updateTime))
        return false;
      this.Publish_OnDataUpdate(this._datas[key]);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long id, long updateTime)
    {
      if (!this._datas.ContainsKey(id) || !this._datas[id].CheckUpdateTime(updateTime))
        return false;
      this._datas.Remove(id);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(long.Parse((arrayList[index] as Hashtable)[(object) "gift_id"].ToString()), updateTime);
    }

    public AllianceGiftData Get(long id)
    {
      if (this._datas.ContainsKey(id))
        return this._datas[id];
      return (AllianceGiftData) null;
    }

    private long CheckKeyFromData(object orgData)
    {
      if (orgData == null)
        return 0;
      Hashtable hashtable = orgData as Hashtable;
      if (orgData is long)
        return long.Parse(orgData.ToString());
      if (hashtable == null)
        return 0;
      long result = 0;
      if (long.TryParse(hashtable[(object) "gift_id"].ToString(), out result))
        return result;
      return 0;
    }

    public Dictionary<long, AllianceGiftData>.ValueCollection Values
    {
      get
      {
        return this._datas.Values;
      }
    }
  }
}
