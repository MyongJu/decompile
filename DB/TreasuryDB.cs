﻿// Decompiled with JetBrains decompiler
// Type: DB.TreasuryDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class TreasuryDB
  {
    protected Dictionary<long, TreasuryData> _tableData = new Dictionary<long, TreasuryData>();

    public event System.Action<TreasuryData> onDataChanged;

    public event System.Action<TreasuryData> onDataUpdate;

    public event System.Action<TreasuryData> onDataRemove;

    public event System.Action<TreasuryData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (TreasuryData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(TreasuryData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(TreasuryData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(TreasuryData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(TreasuryData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(long uid, long updateTime)
    {
      if (!this._tableData.ContainsKey(uid))
        return false;
      TreasuryData data = this._tableData[uid];
      this._tableData.Remove(uid);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "uid", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public TreasuryData Get(long uid)
    {
      if (this._tableData.ContainsKey(uid))
        return this._tableData[uid];
      return (TreasuryData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      TreasuryData data = new TreasuryData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.UserId))
        return false;
      this._tableData.Add(data.UserId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      TreasuryData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public TreasuryData GetTreasuryData()
    {
      using (Dictionary<long, TreasuryData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, TreasuryData> current = enumerator.Current;
          if (current.Value.UserId == PlayerData.inst.uid)
            return current.Value;
        }
      }
      return (TreasuryData) null;
    }

    public TreasuryData GetTreasuryDataByUserId(int uid)
    {
      using (Dictionary<long, TreasuryData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, TreasuryData> current = enumerator.Current;
          if (current.Value.UserId == (long) uid)
            return current.Value;
        }
      }
      return (TreasuryData) null;
    }
  }
}
