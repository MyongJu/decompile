﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceInvitedApplyDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceInvitedApplyDB
  {
    private Dictionary<AllianceInviteApplyKey, AllianceInvitedApplyData> _datas = new Dictionary<AllianceInviteApplyKey, AllianceInvitedApplyData>();

    public event System.Action<long, long> onDataChanged;

    public event System.Action<long, long> onDataCreate;

    public event System.Action<long, long> onDataUpdate;

    public event System.Action<long, long> onDataRemove;

    public event System.Action onDataRemoved;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceInvitedApplyData), (long) this._datas.Count);
    }

    private void Publish_OnDataChanged(long uid, long allianceId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(uid, allianceId);
    }

    private void Publish_OnDataCreate(long uid, long allianceId)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(uid, allianceId);
      this.Publish_OnDataChanged(uid, allianceId);
    }

    private void Publish_OnDataUpdate(long uid, long allianceId)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(uid, allianceId);
      this.Publish_OnDataChanged(uid, allianceId);
    }

    private void Publish_OnDataRemove(long uid, long allianceId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(uid, allianceId);
      this.Publish_OnDataChanged(uid, allianceId);
    }

    private void Publish_OnDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public void Remove(long allianceId, long uid)
    {
      AllianceInviteApplyKey key = new AllianceInviteApplyKey();
      key.allianceId = allianceId;
      key.uid = uid;
      if (this.Get(key) == null)
        return;
      this.Publish_OnDataRemove(key.uid, key.allianceId);
      this._datas.Remove(key);
      this.Publish_OnDataRemoved();
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      AllianceInviteApplyKey key = new AllianceInviteApplyKey();
      key.Decode((object) data);
      if (!key.IsValid)
        return false;
      AllianceInvitedApplyData invitedApplyData1 = this.Get(key);
      if (invitedApplyData1 != null)
      {
        if (!invitedApplyData1.Decode((object) data, updateTime))
          return false;
        this.Publish_OnDataUpdate(key.uid, key.allianceId);
        return true;
      }
      AllianceInvitedApplyData invitedApplyData2 = new AllianceInvitedApplyData();
      if (!invitedApplyData2.Decode((object) data, updateTime))
        return false;
      this._datas.Add(key, invitedApplyData2);
      this.Publish_OnDataCreate(key.uid, key.allianceId);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(arrayList[index], updateTime);
    }

    private bool Remove(object orgData, long updateTime)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return false;
      AllianceInviteApplyKey key = new AllianceInviteApplyKey();
      key.Decode((object) data);
      if (!key.IsValid)
        return false;
      AllianceInvitedApplyData invitedApplyData = this.Get(key);
      if (invitedApplyData != null && invitedApplyData.CheckUpdateTime(updateTime))
      {
        this.Publish_OnDataRemove(key.uid, key.allianceId);
        this._datas.Remove(key);
        this.Publish_OnDataRemoved();
      }
      return true;
    }

    public void RemoveByAllianceId(long allianceId)
    {
      List<AllianceInvitedApplyData> dataByAllianceId = this.GetDataByAllianceId(allianceId);
      for (int index = 0; index < dataByAllianceId.Count; ++index)
      {
        AllianceInviteApplyKey key = dataByAllianceId[index].key;
        this.Publish_OnDataRemove(key.uid, key.allianceId);
        this._datas.Remove(key);
        this.Publish_OnDataRemoved();
      }
    }

    public List<AllianceInvitedApplyData> GetDataByUid(long uid)
    {
      List<AllianceInvitedApplyData> invitedApplyDataList = new List<AllianceInvitedApplyData>();
      Dictionary<AllianceInviteApplyKey, AllianceInvitedApplyData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Key.uid == uid)
          invitedApplyDataList.Add(enumerator.Current.Value);
      }
      return invitedApplyDataList;
    }

    public List<AllianceInvitedApplyData> GetDataByAllianceId(long allianceId)
    {
      List<AllianceInvitedApplyData> invitedApplyDataList = new List<AllianceInvitedApplyData>();
      Dictionary<AllianceInviteApplyKey, AllianceInvitedApplyData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Key.allianceId == allianceId)
          invitedApplyDataList.Add(enumerator.Current.Value);
      }
      return invitedApplyDataList;
    }

    public AllianceInvitedApplyData Get(long allianceId, long uid)
    {
      return this.Get(new AllianceInviteApplyKey()
      {
        uid = uid,
        allianceId = allianceId
      });
    }

    public AllianceInvitedApplyData Get(AllianceInviteApplyKey key)
    {
      AllianceInvitedApplyData invitedApplyData;
      this._datas.TryGetValue(key, out invitedApplyData);
      return invitedApplyData;
    }

    public bool IsApplied(long allianceId)
    {
      using (List<AllianceInvitedApplyData>.Enumerator enumerator = this.GetDataByAllianceId(allianceId).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.status == AllianceInvitedApplyData.Status.asking)
            return true;
        }
      }
      return false;
    }
  }
}
