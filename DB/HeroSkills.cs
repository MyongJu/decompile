﻿// Decompiled with JetBrains decompiler
// Type: DB.HeroSkills
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class HeroSkills
  {
    public Dictionary<long, HeroSkills.Skill> skills = new Dictionary<long, HeroSkills.Skill>();

    public void Decode(object orgData)
    {
      this.skills.Clear();
      Hashtable data;
      if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
        return;
      IEnumerator enumerator = data.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        long key = long.Parse(enumerator.Current.ToString());
        HeroSkills.Skill skill = new HeroSkills.Skill();
        skill.Decode(data[enumerator.Current]);
        this.skills.Add(key, skill);
      }
    }

    public HeroSkills.Skill GetSkill(long skillId)
    {
      if (this.skills.ContainsKey(skillId))
        return this.skills[skillId];
      return (HeroSkills.Skill) null;
    }

    public class Skill
    {
      public int activeTime;
      public long jobId;

      public bool Decode(object orgData)
      {
        Hashtable data;
        if (!DatabaseTools.CheckAndParseOrgData(orgData, out data))
          return false;
        return false | DatabaseTools.UpdateData(data, "active_time", ref this.activeTime) | DatabaseTools.UpdateData(data, "job_id", ref this.jobId);
      }

      public struct Params
      {
        public const string ACTIVE_TIME = "active_time";
        public const string JOB_ID = "job_id";
      }
    }
  }
}
