﻿// Decompiled with JetBrains decompiler
// Type: DB.ChatRoomDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ChatRoomDB
  {
    public Dictionary<long, ChatRoomData> _datas = new Dictionary<long, ChatRoomData>();
    public System.Action<long> onDataChanged;
    public System.Action<long> onDataCreated;
    public System.Action<long> onDataUpdated;
    public System.Action<long> onDataRemove;
    public System.Action onDataRemoved;
    public System.Action<long, long> onMemberChanged;
    public System.Action<long, long> onMemberCreated;
    public System.Action<long, long> onMemberUpdated;
    public System.Action<long, long> onMemberRemove;
    public System.Action<long> onMemberRemoved;

    public Dictionary<long, ChatRoomData>.KeyCollection.Enumerator keysEnumerator
    {
      get
      {
        return this._datas.Keys.GetEnumerator();
      }
    }

    public Dictionary<long, ChatRoomData>.ValueCollection.Enumerator valueEnumerator
    {
      get
      {
        return this._datas.Values.GetEnumerator();
      }
    }

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ChatRoomData), (long) this._datas.Count);
    }

    public void Publish_onDataChanged(long roomId)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(roomId);
    }

    public void Publish_onDataCreated(long roomId)
    {
      if (this.onDataCreated != null)
        this.onDataCreated(roomId);
      this.Publish_onDataChanged(roomId);
    }

    public void Publish_onDataUpdated(long roomId)
    {
      if (this.onDataUpdated != null)
        this.onDataUpdated(roomId);
      this.Publish_onDataChanged(roomId);
    }

    public void Publish_onDataRemove(long roomId)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(roomId);
      this.Publish_onDataChanged(roomId);
    }

    public void Publish_onDataRemoved()
    {
      if (this.onDataRemoved == null)
        return;
      this.onDataRemoved();
    }

    public void Publish_onMemeberChanged(long roomId, long uid)
    {
      if (this.onMemberChanged == null)
        return;
      this.onMemberChanged(roomId, uid);
    }

    public void Publish_onMemeberCreated(long roomId, long uid)
    {
      if (this.onMemberCreated != null)
        this.onMemberCreated(roomId, uid);
      this.Publish_onMemeberChanged(roomId, uid);
    }

    public void Publish_onMemeberUpdated(long roomId, long uid)
    {
      if (this.onMemberUpdated != null)
        this.onMemberUpdated(roomId, uid);
      this.Publish_onMemeberChanged(roomId, uid);
    }

    public void Publish_onMemeberRemove(long roomId, long uid)
    {
      if (this.onMemberRemove != null)
        this.onMemberRemove(roomId, uid);
      this.Publish_onMemeberChanged(roomId, uid);
    }

    public void Publish_onMemeberRemoved(long roomId)
    {
      if (this.onMemberRemoved == null)
        return;
      this.onMemberRemoved(roomId);
    }

    public bool Add(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      ChatRoomData chatRoomData = new ChatRoomData();
      if (!chatRoomData.Decode((object) hashtable, updateTime) || chatRoomData.roomId == 0L || this._datas.ContainsKey(chatRoomData.roomId))
        return false;
      this._datas.Add(chatRoomData.roomId, chatRoomData);
      this.Publish_onDataCreated(chatRoomData.roomId);
      return true;
    }

    public bool Update(object orgData, long updateTime)
    {
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      long roomId = ChatRoomData.GetRoomId((object) hashtable);
      if (roomId == 0L)
        return false;
      if (!this._datas.ContainsKey(roomId))
        return this.Add(orgData, updateTime);
      if (!this._datas[roomId].Decode((object) hashtable, updateTime))
        return false;
      this.Publish_onDataUpdated(roomId);
      return true;
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateMembers(object orgDatas, long updateTime)
    {
      List<long> longList = new List<long>();
      ArrayList arrayList1 = orgDatas as ArrayList;
      if (arrayList1 == null)
        return;
      int index1 = 0;
      for (int count = arrayList1.Count; index1 < count; ++index1)
      {
        ChatRoomMember tmpMemeber = new ChatRoomMember();
        if (tmpMemeber.Decode(arrayList1[index1], updateTime))
        {
          ChatRoomData chatRoomData = this.Get(tmpMemeber.roomId);
          if (chatRoomData != null)
          {
            chatRoomData.UpdateMemeber(tmpMemeber, arrayList1[index1], updateTime);
            this.Publish_onMemeberUpdated(tmpMemeber.roomId, tmpMemeber.uid);
            if (tmpMemeber.uid == PlayerData.inst.uid && tmpMemeber.title == ChatRoomMember.Title.blackList && !longList.Contains(tmpMemeber.roomId))
              longList.Add(tmpMemeber.roomId);
          }
        }
      }
      ArrayList arrayList2 = new ArrayList();
      for (int index2 = 0; index2 < longList.Count; ++index2)
        arrayList2.Add((object) Utils.Hash((object) "room_id", (object) longList[index2], (object) "uid", (object) PlayerData.inst.uid));
      this.RemoveDatas((object) arrayList2, updateTime);
    }

    public ChatRoomData Get(long key)
    {
      if (this._datas.ContainsKey(key))
        return this._datas[key];
      return (ChatRoomData) null;
    }

    public ChatRoomData GetRoomByChannelId(long channelId)
    {
      Dictionary<long, ChatRoomData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.channelId == channelId)
          return enumerator.Current;
      }
      return (ChatRoomData) null;
    }

    public bool Remove(long roomId, long updateTime)
    {
      if (!this._datas.ContainsKey(roomId) || !this._datas[roomId].CheckUpdateTime(updateTime))
        return false;
      this.Publish_onDataRemove(roomId);
      this._datas.Remove(roomId);
      this.Publish_onDataRemoved();
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Remove(ChatRoomData.GetRoomId(arrayList[index]), updateTime);
    }

    public void RemoveMemebers(object orgDatas, long updateTime)
    {
      ArrayList arrayList1 = orgDatas as ArrayList;
      List<long> longList = new List<long>();
      if (arrayList1 == null)
        return;
      int index1 = 0;
      for (int count = arrayList1.Count; index1 < count; ++index1)
      {
        ChatRoomMember member = new ChatRoomMember();
        member.Decode(arrayList1[index1], updateTime);
        this.RemoveMember(member);
        if (member.uid == PlayerData.inst.uid && !longList.Contains(member.roomId))
          longList.Add(member.roomId);
      }
      ArrayList arrayList2 = new ArrayList();
      for (int index2 = 0; index2 < longList.Count; ++index2)
        arrayList2.Add((object) Utils.Hash((object) "room_id", (object) longList[index2], (object) "uid", (object) PlayerData.inst.uid));
      this.RemoveDatas((object) arrayList2, updateTime);
    }

    public void RemoveMember(ChatRoomMember member)
    {
      ChatRoomData chatRoomData = this.Get(member.roomId);
      if (chatRoomData == null)
        return;
      this.Publish_onMemeberRemove(chatRoomData.roomId, member.uid);
      chatRoomData.RemoveMemeber(member);
      this.Publish_onMemeberRemoved(chatRoomData.roomId);
    }

    public void ShowInfo()
    {
      using (Dictionary<long, ChatRoomData>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ChatRoomData current = enumerator.Current;
        }
      }
    }
  }
}
