﻿// Decompiled with JetBrains decompiler
// Type: DB.ResourceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public struct ResourceInfo
  {
    public const string RESOURCES_TYPE_FOOD = "food";
    public const string RESOURCES_TYPE_WOOD = "wood";
    public const string RESOURCES_TYPE_ORE = "ore";
    public const string RESOURCES_TYPE_SILVER = "silver";
    public Dictionary<string, int> resources;

    public ResourceInfo(ResourceInfo orgData)
    {
      this.resources = new Dictionary<string, int>((IDictionary<string, int>) orgData.resources);
    }

    public bool Decode(object orgData)
    {
      if (this.resources == null)
        this.resources = new Dictionary<string, int>();
      if (orgData == null)
        return false;
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
      {
        if (this.resources.Count == 0)
          return false;
        this.resources.Clear();
        return true;
      }
      bool flag = false;
      int result = 0;
      foreach (string key in (IEnumerable) hashtable.Keys)
      {
        int.TryParse(hashtable[(object) key].ToString(), out result);
        if (this.resources.ContainsKey(key))
        {
          if (this.resources[key] != result)
          {
            this.resources[key] = result;
            flag = true;
          }
        }
        else
        {
          this.resources.Add(key, result);
          flag = true;
        }
      }
      return flag;
    }

    public string ShowInfo()
    {
      string str = "Resources Info\n";
      if (this.resources != null)
      {
        using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.resources.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            str = str + "  " + current + " : " + (object) this.resources[current] + "\n";
          }
        }
      }
      return str;
    }
  }
}
