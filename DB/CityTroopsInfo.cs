﻿// Decompiled with JetBrains decompiler
// Type: DB.CityTroopsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DB
{
  public struct CityTroopsInfo
  {
    public Dictionary<string, int> troops;
    private long _totalTroopsCount;

    public long totalTrapsCount { private set; get; }

    public long totalTroopsCount
    {
      get
      {
        return this._totalTroopsCount;
      }
    }

    public long totalUpkeep
    {
      get
      {
        float num = 0.0f;
        using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.troops.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            ConfigBenefitCalc.UnitFinalStruct unitFinalData = ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(current, ConfigBenefitCalc.UnitCalcType.upkeep, ConfigBenefitCalc.CalcOption.none);
            num += Mathf.Ceil(unitFinalData.upkeep * (float) this.troops[current]);
          }
        }
        return (long) num;
      }
    }

    public long totalLoad
    {
      get
      {
        long num = 0;
        using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.troops.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            ConfigBenefitCalc.UnitFinalStruct unitFinalData = ConfigManager.inst.DB_BenefitCalc.GetUnitFinalData(current, ConfigBenefitCalc.UnitCalcType.load, ConfigBenefitCalc.CalcOption.none);
            num += (long) (int) ((double) unitFinalData.load * (double) this.troops[current]);
          }
        }
        return num;
      }
    }

    public void Decode(object orgdata)
    {
      if (this.troops == null)
        this.troops = new Dictionary<string, int>();
      this.troops.Clear();
      this._totalTroopsCount = 0L;
      this.totalTrapsCount = 0L;
      Hashtable data1;
      if (!BaseData.CheckAndParseOrgData(orgdata, out data1))
        return;
      IDictionaryEnumerator enumerator = data1.GetEnumerator();
      while (enumerator.MoveNext())
      {
        string str = enumerator.Key.ToString();
        int num = int.Parse(enumerator.Value.ToString());
        if (num != 0)
        {
          this.troops.Add(str, num);
          Unit_StatisticsInfo data2 = ConfigManager.inst.DB_Unit_Statistics.GetData(str);
          if (data2 != null && data2.Type == "trap")
            this.totalTrapsCount += (long) num;
          else
            this._totalTroopsCount += (long) num;
        }
      }
    }

    public int GetTroopsCount(string troopID)
    {
      if (this.troops.ContainsKey(troopID))
        return this.troops[troopID];
      return 0;
    }

    public string ShowInfo(int tableCount)
    {
      if (this.troops == null)
        return string.Empty;
      string empty = string.Empty;
      for (int index = 0; index < tableCount; ++index)
        empty += "\t";
      string str = string.Empty;
      using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.troops.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          str = str + empty + current + " : " + (object) this.troops[current] + "\n";
        }
      }
      return str;
    }
  }
}
