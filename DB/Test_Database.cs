﻿// Decompiled with JetBrains decompiler
// Type: DB.Test_Database
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace DB
{
  public class Test_Database
  {
    public void Init()
    {
      DBManager.inst.DB_User.onDataChanged += new Action<long>(this.userListener);
      DBManager.inst.DB_City.onDataChanged += new Action<long>(this.cityListener);
      DBManager.inst.DB_Alliance.onDataChanged += new Action<long>(this.allianceListener);
      DBManager.inst.DB_March.onDataCreate += new Action<long>(this.marchCreateListner);
      DBManager.inst.DB_March.onDataUpdate += new Action<long>(this.marchUpdateListner);
      DBManager.inst.DB_March.onDataRemove += new Action<long>(this.marchRemoveListener);
      DBManager.inst.DB_March.onDataChanged += new Action<long>(this.marchListener);
      DBManager.inst.DB_Rally.onRallyDataCreated += new Action<long>(this.rallyAddListener);
      DBManager.inst.DB_Rally.onRallyDataUpdated += new Action<long>(this.rallyUpdateListener);
      DBManager.inst.DB_Rally.onRallyDataRemove += new Action<long>(this.rallyRemoveListner);
      DBManager.inst.DB_Rally.onRallyDataChanged += new Action<long>(this.rallyListener);
    }

    private void userListener(long uid)
    {
      if (DBManager.inst.DB_User.Get(uid) != null)
        ;
    }

    private void allianceListener(long allianceId)
    {
      if (DBManager.inst.DB_Alliance.Get(allianceId) != null)
        ;
    }

    private void cityListener(long cityId)
    {
      if (DBManager.inst.DB_City.Get(cityId) != null)
        ;
    }

    private void marchListener(long marchId)
    {
      if (DBManager.inst.DB_March.Get(marchId) != null)
        ;
    }

    private void marchCreateListner(long marchId)
    {
    }

    private void marchUpdateListner(long marchId)
    {
    }

    private void marchRemoveListener(long marchId)
    {
    }

    private void rallyListener(long rallyId)
    {
      if (DBManager.inst.DB_Rally.Get(rallyId) != null)
        ;
    }

    private void rallyAddListener(long rallyId)
    {
    }

    private void rallyUpdateListener(long rallyId)
    {
    }

    private void rallyRemoveListner(long rallyId)
    {
    }
  }
}
