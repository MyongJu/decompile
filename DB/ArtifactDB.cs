﻿// Decompiled with JetBrains decompiler
// Type: DB.ArtifactDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class ArtifactDB
  {
    protected Dictionary<KeyValuePair<int, int>, ArtifactData> _tableData = new Dictionary<KeyValuePair<int, int>, ArtifactData>();

    public event System.Action<ArtifactData> onDataChanged;

    public event System.Action<ArtifactData> onDataUpdate;

    public event System.Action<ArtifactData> onDataRemove;

    public event System.Action<ArtifactData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (ArtifactData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(ArtifactData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(ArtifactData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(ArtifactData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(ArtifactData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    public void UpdateDatas(Hashtable orgDatas, long updateTime)
    {
      if (orgDatas == null)
        return;
      this.Update((object) orgDatas, updateTime);
    }

    private bool Remove(KeyValuePair<int, int> key, long updateTime)
    {
      if (!this._tableData.ContainsKey(key))
        return false;
      ArtifactData data = this._tableData[key];
      this._tableData.Remove(key);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int outData1 = 0;
      int outData2 = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "world_id", ref outData1);
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "artifact_id", ref outData2);
        this.Remove(new KeyValuePair<int, int>(outData1, outData2), updateTime);
      }
    }

    public ArtifactData Get(int worldId, int artifactId)
    {
      KeyValuePair<int, int> key = new KeyValuePair<int, int>(worldId, artifactId);
      if (this._tableData.ContainsKey(key))
        return this._tableData[key];
      return (ArtifactData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      ArtifactData data = new ArtifactData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.ParamKey))
        return false;
      this._tableData.Add(data.ParamKey, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      int outData1 = 0;
      int outData2 = 0;
      DatabaseTools.UpdateData(inData, "world_id", ref outData1);
      DatabaseTools.UpdateData(inData, "artifact_id", ref outData2);
      KeyValuePair<int, int> key = new KeyValuePair<int, int>(outData1, outData2);
      if (!this._tableData.ContainsKey(key))
        return this.Add(orgData, updateTime);
      ArtifactData data = this._tableData[key];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public List<ArtifactData> GetArtifactDataListByUid(int worldId, long ownerUid = 0)
    {
      long num = ownerUid <= 0L ? PlayerData.inst.uid : ownerUid;
      List<ArtifactData> artifactDataList = new List<ArtifactData>();
      using (Dictionary<KeyValuePair<int, int>, ArtifactData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<int, int>, ArtifactData> current = enumerator.Current;
          if (current.Value.WorldId == worldId && current.Value.OwnerUid == num)
            artifactDataList.Add(current.Value);
        }
      }
      return artifactDataList;
    }

    public ArtifactData GetArtifactData(int worldId, int artifactId)
    {
      using (Dictionary<KeyValuePair<int, int>, ArtifactData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<int, int>, ArtifactData> current = enumerator.Current;
          if (current.Value.WorldId == worldId && current.Value.ArtifactId == artifactId)
            return current.Value;
        }
      }
      return (ArtifactData) null;
    }

    private bool HasLightWand(long uid)
    {
      ArtifactInfo artifactByActiveEffect = ConfigManager.inst.DB_Artifact.GetArtifactByActiveEffect("light_wand");
      if (artifactByActiveEffect == null)
        return false;
      return this.IsUserArtifactEquiped(uid, artifactByActiveEffect.internalId);
    }

    private bool HasDarkWand(long uid)
    {
      ArtifactInfo artifactByActiveEffect = ConfigManager.inst.DB_Artifact.GetArtifactByActiveEffect("dark_wand");
      if (artifactByActiveEffect == null)
        return false;
      return this.IsUserArtifactEquiped(uid, artifactByActiveEffect.internalId);
    }

    private bool HasExtraLevel(long uid, int groupId)
    {
      ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(groupId);
      if (dragonSkillGroupInfo.no_artifact > 0)
        return false;
      if (dragonSkillGroupInfo.type == ConfigDragonSkillGroupInfo.LIGHT)
        return this.HasLightWand(uid);
      return this.HasDarkWand(uid);
    }

    public int GetExtarLevel(long uid, int groupId)
    {
      return this.HasExtraLevel(uid, groupId) ? 1 : 0;
    }

    public bool IsUserArtifactEquiped(long uid, int internalId)
    {
      using (Dictionary<KeyValuePair<int, int>, ArtifactData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<KeyValuePair<int, int>, ArtifactData> current = enumerator.Current;
          if (current.Value.IsEquipped && current.Value.OwnerUid == uid && current.Value.ArtifactId == internalId)
            return true;
        }
      }
      return false;
    }
  }
}
