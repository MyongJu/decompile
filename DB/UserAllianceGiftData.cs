﻿// Decompiled with JetBrains decompiler
// Type: DB.UserAllianceGiftData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public class UserAllianceGiftData : BaseData
  {
    public int giftId = -1;
    public const int INVALID_ID = -1;
    public int status;
    public int rewardId;
    public int rewardCount;

    public bool Decode(Hashtable orgData, long updateTime)
    {
      if (!this.CheckUpdateTime(updateTime))
        return false;
      this._updateTime = updateTime;
      if (orgData == null)
        return false;
      Hashtable inData = orgData;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "gift_id", ref this.giftId) | DatabaseTools.UpdateData(inData, "status", ref this.status) | DatabaseTools.UpdateData(inData, "reward_id", ref this.rewardId) | DatabaseTools.UpdateData(inData, "reward_count", ref this.rewardCount);
    }

    public static class Params
    {
      public const string KEY = "user_alliance_gift";
      public const string GIFT_ID = "gift_id";
      public const string STATUS = "status";
      public const string REWARD_ID = "reward_id";
      public const string REWARD_COUNT = "reward_count";
    }
  }
}
