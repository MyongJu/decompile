﻿// Decompiled with JetBrains decompiler
// Type: DB.InventoryItemIdentifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct InventoryItemIdentifier
  {
    public const string INVENTORY_ITEM_UID = "uid";
    public const string INVENTORY_ITEM_ITEM_ID = "item_id";
    public long uid;
    public long itemId;

    public bool Decode(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return false | DatabaseTools.UpdateData(inData, "uid", ref this.uid) | DatabaseTools.UpdateData(inData, "item_id", ref this.itemId);
    }

    public bool IsValid
    {
      get
      {
        if (this.uid != 0L)
          return this.itemId != 0L;
        return false;
      }
    }
  }
}
