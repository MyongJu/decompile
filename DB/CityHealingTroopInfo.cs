﻿// Decompiled with JetBrains decompiler
// Type: DB.CityHealingTroopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

namespace DB
{
  public struct CityHealingTroopInfo
  {
    public long totalCount;
    public long healingCount;

    public void Decode(object orgData)
    {
      Hashtable data;
      if (!BaseData.CheckAndParseOrgData(orgData, out data))
        return;
      DatabaseTools.UpdateData(data, "total", ref this.totalCount);
      DatabaseTools.UpdateData(data, "healing", ref this.healingCount);
    }

    public struct Params
    {
      public const string HEALING_TROOP_TOTAL = "total";
      public const string HEALING_TROOP_HEALING = "healing";
    }
  }
}
