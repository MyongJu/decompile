﻿// Decompiled with JetBrains decompiler
// Type: DB.AllianceSuperMineDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace DB
{
  public class AllianceSuperMineDB
  {
    protected Dictionary<long, AllianceSuperMineData> _tableData = new Dictionary<long, AllianceSuperMineData>();

    public event System.Action<AllianceSuperMineData> onDataChanged;

    public event System.Action<AllianceSuperMineData> onDataUpdate;

    public event System.Action<AllianceSuperMineData> onDataRemove;

    public event System.Action<AllianceSuperMineData> onDataCreate;

    public long GetMemorySize()
    {
      return DatabaseUtils.GetMemorySize(typeof (AllianceSuperMineData), (long) this._tableData.Count);
    }

    public void Clear()
    {
      this._tableData.Clear();
    }

    private void Publish_OnDataChanged(AllianceSuperMineData data)
    {
      if (this.onDataChanged == null)
        return;
      this.onDataChanged(data);
    }

    private void Publish_OnDataCreate(AllianceSuperMineData data)
    {
      if (this.onDataCreate != null)
        this.onDataCreate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataUpdate(AllianceSuperMineData data)
    {
      if (this.onDataUpdate != null)
        this.onDataUpdate(data);
      this.Publish_OnDataChanged(data);
    }

    private void Publish_OnDataRemove(AllianceSuperMineData data)
    {
      if (this.onDataRemove != null)
        this.onDataRemove(data);
      this.Publish_OnDataChanged(data);
    }

    public void UpdateDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
        this.Update(arrayList[index], updateTime);
    }

    private bool Remove(long superMineId, long updateTime)
    {
      if (!this._tableData.ContainsKey(superMineId))
        return false;
      AllianceSuperMineData data = this._tableData[superMineId];
      this._tableData.Remove(superMineId);
      this.Publish_OnDataRemove(data);
      return true;
    }

    public void RemoveDatas(object orgDatas, long updateTime)
    {
      ArrayList arrayList = orgDatas as ArrayList;
      if (arrayList == null)
        return;
      long outData = 0;
      int index = 0;
      for (int count = arrayList.Count; index < count; ++index)
      {
        DatabaseTools.UpdateData(arrayList[index] as Hashtable, "building_id", ref outData);
        this.Remove(outData, updateTime);
      }
    }

    public AllianceSuperMineData Get(long superMineId)
    {
      if (this._tableData.ContainsKey(superMineId))
        return this._tableData[superMineId];
      return (AllianceSuperMineData) null;
    }

    private bool Add(object orgData, long updateTime)
    {
      Hashtable hashtable = orgData as Hashtable;
      if (hashtable == null)
        return false;
      AllianceSuperMineData data = new AllianceSuperMineData();
      if (!data.Decode((object) hashtable, updateTime) || this._tableData.ContainsKey(data.SuperMineId))
        return false;
      this._tableData.Add(data.SuperMineId, data);
      this.Publish_OnDataCreate(data);
      return true;
    }

    private bool Update(object orgData, long updateTime)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "building_id", ref outData);
      if (!this._tableData.ContainsKey(outData))
        return this.Add(orgData, updateTime);
      AllianceSuperMineData data = this._tableData[outData];
      if (!data.Decode((object) inData, updateTime))
        return false;
      this.Publish_OnDataUpdate(data);
      return true;
    }

    public AllianceSuperMineData GetSuperMineData()
    {
      using (Dictionary<long, AllianceSuperMineData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceSuperMineData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId)
            return current.Value;
        }
      }
      return (AllianceSuperMineData) null;
    }

    public AllianceSuperMineData GetSuperMineDataByBuildingConfigId(int allianceBuildingConfigId)
    {
      using (Dictionary<long, AllianceSuperMineData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceSuperMineData> current = enumerator.Current;
          if (current.Value.AllianceId == PlayerData.inst.allianceId && current.Value.ConfigId == allianceBuildingConfigId)
            return current.Value;
        }
      }
      return (AllianceSuperMineData) null;
    }

    public AllianceSuperMineData GetDataByCoordinate(Coordinate location)
    {
      using (Dictionary<long, AllianceSuperMineData>.Enumerator enumerator = this._tableData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, AllianceSuperMineData> current = enumerator.Current;
          if (current.Value.Location.K == location.K && current.Value.Location.X == location.X && current.Value.Location.Y == location.Y)
            return current.Value;
        }
      }
      return (AllianceSuperMineData) null;
    }
  }
}
