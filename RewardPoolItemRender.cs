﻿// Decompiled with JetBrains decompiler
// Type: RewardPoolItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class RewardPoolItemRender : MonoBehaviour
{
  private const string quality_fre = "tuintableframe";
  public UILabel itemCount;
  public UILabel itemName;
  public UITexture itemImage;
  public UISprite defaultImage;
  public UISprite colorImage;
  public GameObject selected;
  public GameObject newLabel;
  public System.Action<RewardPoolItemRender> OnItemClickedHandler;
  private RewardPoolItemData data;
  private int itemId;
  private int count;
  private int color;
  private int grade;
  private bool noData;

  public bool Select
  {
    set
    {
      NGUITools.SetActive(this.selected, value);
    }
  }

  public RewardPoolItemData Data
  {
    get
    {
      return this.data;
    }
  }

  public void FeedData(RewardPoolItemData data)
  {
    if (data == null)
    {
      this.noData = true;
    }
    else
    {
      this.noData = false;
      this.data = data;
      this.itemId = data.itemId;
      this.count = data.itemCount;
      this.color = data.color;
      this.grade = data.grade;
    }
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.noData)
    {
      this.itemCount.text = "X0";
      this.itemName.text = Utils.XLAT("BLANK");
      this.defaultImage.gameObject.SetActive(true);
    }
    else
    {
      this.defaultImage.gameObject.SetActive(false);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
      if (itemStaticInfo != null)
      {
        this.itemName.text = itemStaticInfo.LocName;
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemImage, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        this.colorImage.spriteName = "tuintableframe" + (object) this.color;
      }
      this.itemCount.text = "X" + this.count.ToString();
      if (this.grade == 3)
      {
        this.newLabel.gameObject.SetActive(true);
        this.newLabel.GetComponentInChildren<UILabel>().text = ScriptLocalization.Get("id_uppercase_rare", true);
      }
      else
        this.newLabel.gameObject.SetActive(false);
    }
  }

  public void OnIteClicked()
  {
    if (this.OnItemClickedHandler == null)
      return;
    this.OnItemClickedHandler(this);
  }
}
