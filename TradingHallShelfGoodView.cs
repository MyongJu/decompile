﻿// Decompiled with JetBrains decompiler
// Type: TradingHallShelfGoodView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class TradingHallShelfGoodView : MonoBehaviour
{
  private List<TradingHallData.SellGoodData> _able2ShelfGoodList = new List<TradingHallData.SellGoodData>();
  private Dictionary<int, TradingHallShelfGoodSlot> _shelfItemDict = new Dictionary<int, TradingHallShelfGoodSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int MAX_SHELF_COUNT_SHOW = 24;
  [SerializeField]
  private GameObject _noShelfGoodNode;
  [SerializeField]
  private UIScrollView _shelfScrollView;
  [SerializeField]
  private UITable _shelfTable;
  [SerializeField]
  private GameObject _shelfItemPrefab;
  [SerializeField]
  private GameObject _shelfItemRoot;
  private UIWrapContentEx _wc;
  public System.Action OnShelfGoodSuccessed;

  public void UpdateUI(bool resetScrollView = true)
  {
    this.PrepareShelfGoods();
    this.SortShelfGoods();
    this.GenerateShelfGoods(resetScrollView);
  }

  public void ClearShelfGoodData()
  {
    if ((UnityEngine.Object) this._wc != (UnityEngine.Object) null)
    {
      this._wc.SortBasedOnScrollMovement();
      UIWrapContentEx wc = this._wc;
      wc.onInitializeItem = wc.onInitializeItem - new UIWrapContent.OnInitializeItem(this.OnInitItem);
    }
    using (Dictionary<int, TradingHallShelfGoodSlot>.Enumerator enumerator = this._shelfItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TradingHallShelfGoodSlot> current = enumerator.Current;
        current.Value.OnShelfGoodSuccess -= new System.Action(this.OnShelfGoodSuccess);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._shelfItemDict.Clear();
    this._itemPool.Clear();
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    DBManager.inst.GetInventory(BagType.Hero).onDataUpdated -= new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.GetInventory(BagType.DragonKnight).onDataUpdated -= new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
  }

  private void RepositionShelfGood(bool resetScrollView = true)
  {
    this._shelfTable.repositionNow = true;
    this._shelfTable.Reposition();
    if (!resetScrollView)
      return;
    this._shelfScrollView.enabled = true;
    this._shelfScrollView.ResetPosition();
  }

  private TradingHallShelfGoodSlot GenerateShelfGoodSlot(TradingHallData.SellGoodData sellGoodData)
  {
    this._itemPool.Initialize(this._shelfItemPrefab, this._shelfItemRoot);
    GameObject go = this._itemPool.AddChild(this._shelfTable.gameObject);
    NGUITools.SetActive(go, true);
    TradingHallShelfGoodSlot component = go.GetComponent<TradingHallShelfGoodSlot>();
    component.SetData(sellGoodData);
    component.OnShelfGoodSuccess += new System.Action(this.OnShelfGoodSuccess);
    return component;
  }

  private void OnShelfGoodSuccess()
  {
    if (this.OnShelfGoodSuccessed == null)
      return;
    this.OnShelfGoodSuccessed();
  }

  private void PrepareShelfGoods()
  {
    this._able2ShelfGoodList.Clear();
    List<InventoryItemData> inventoryItemDataList1 = (List<InventoryItemData>) null;
    List<InventoryItemData> inventoryItemDataList2 = (List<InventoryItemData>) null;
    InventoryDB inventory1 = DBManager.inst.GetInventory(BagType.Hero);
    if (inventory1 != null)
      inventoryItemDataList1 = inventory1.GetMyItemList();
    InventoryDB inventory2 = DBManager.inst.GetInventory(BagType.DragonKnight);
    if (inventory2 != null)
      inventoryItemDataList2 = inventory2.GetMyItemList();
    List<ExchangeHallInfo> infoList = ConfigManager.inst.DB_ExchangeHall.GetExchangeHallInfoList();
    int equipmentEnhanced = 0;
    if (infoList != null)
    {
      for (int i = 0; i < infoList.Count; ++i)
      {
        equipmentEnhanced = 0;
        if (infoList[i].itemType == "equipment" && inventoryItemDataList1 != null || infoList[i].itemType == "dk_equipment" && inventoryItemDataList2 != null)
        {
          int.TryParse(infoList[i].param1, out equipmentEnhanced);
          List<InventoryItemData> inventoryItemDataList3 = !(infoList[i].itemType == "equipment") ? inventoryItemDataList2 : inventoryItemDataList1;
          BagType bagType = TradingHallPayload.Instance.GetBagType(infoList[i].internalId);
          List<InventoryItemData> all = inventoryItemDataList3.FindAll((Predicate<InventoryItemData>) (x =>
          {
            ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(x.internalId);
            if (data != null && data.itemID == infoList[i].itemId && x.enhanced == equipmentEnhanced)
              return !x.InSale;
            return false;
          }));
          if (all != null)
          {
            for (int index = 0; index < all.Count; ++index)
              this.GenerateItem(infoList[i].internalId, infoList[i].itemId, (int) infoList[i].itemPrice, 1, (int) all[index].itemId, all[index].SellCooldownTime);
          }
        }
        else
        {
          int itemCount = DBManager.inst.DB_Item.GetQuantity(infoList[i].itemId) - DBManager.inst.DB_Item.GetCooldownQuantity(infoList[i].itemId);
          this.GenerateItem(infoList[i].internalId, infoList[i].itemId, (int) infoList[i].itemPrice, itemCount, 0, 0);
          ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(infoList[i].itemId);
          if (consumableItemData != null && consumableItemData.SellCooldownTimeList.Count > 0)
          {
            for (int index = 0; index < consumableItemData.SellCooldownTimeList.Count; ++index)
            {
              if (NetServerTime.inst.ServerTimestamp <= consumableItemData.SellCooldownTimeList[index].Key && consumableItemData.SellCooldownTimeList[index].Value > 0)
                this.GenerateItem(infoList[i].internalId, infoList[i].itemId, (int) infoList[i].itemPrice, consumableItemData.SellCooldownTimeList[index].Value, 0, consumableItemData.SellCooldownTimeList[index].Key);
            }
          }
        }
      }
    }
    NGUITools.SetActive(this._noShelfGoodNode, this._able2ShelfGoodList.Count <= 0);
  }

  private void GenerateItem(int exchangeId, int itemId, int itemPrice, int itemCount, int equipmentId, int cooldownTime)
  {
    if (itemCount <= 0)
      return;
    this._able2ShelfGoodList.Add(new TradingHallData.SellGoodData(exchangeId, itemId, itemPrice, itemCount, equipmentId, cooldownTime));
  }

  private void SortShelfGoods()
  {
    if (this._able2ShelfGoodList == null)
      return;
    this._able2ShelfGoodList.Sort((Comparison<TradingHallData.SellGoodData>) ((x, y) =>
    {
      ExchangeHallInfo exchangeHallInfo1 = ConfigManager.inst.DB_ExchangeHall.Get(x.ExchangeId);
      ExchangeHallInfo exchangeHallInfo2 = ConfigManager.inst.DB_ExchangeHall.Get(y.ExchangeId);
      if (exchangeHallInfo1 == null || exchangeHallInfo2 == null)
        return -1;
      if (exchangeHallInfo1.IsEquipment != exchangeHallInfo2.IsEquipment)
        return exchangeHallInfo2.IsEquipment.CompareTo(exchangeHallInfo1.IsEquipment);
      if (exchangeHallInfo1.Quality != exchangeHallInfo2.Quality)
        return exchangeHallInfo2.Quality.CompareTo(exchangeHallInfo1.Quality);
      if (exchangeHallInfo1.Enhanced != exchangeHallInfo2.Enhanced)
        return exchangeHallInfo2.Enhanced.CompareTo(exchangeHallInfo1.Enhanced);
      if (exchangeHallInfo1.RequireLevel != exchangeHallInfo2.RequireLevel)
        return exchangeHallInfo2.RequireLevel.CompareTo(exchangeHallInfo1.RequireLevel);
      if (x.IsCooldown != y.IsCooldown)
        return x.IsCooldown.CompareTo(y.IsCooldown);
      return exchangeHallInfo1.Prority.CompareTo(exchangeHallInfo2.Prority);
    }));
  }

  private void GenerateShelfGoods(bool resetScrollView = true)
  {
    this.ClearShelfGoodData();
    TradingHallPayload.Instance.OnSellDataUpdated -= new System.Action(this.OnSellDataUpdated);
    TradingHallPayload.Instance.OnSellDataUpdated += new System.Action(this.OnSellDataUpdated);
    DBManager.inst.GetInventory(BagType.Hero).onDataUpdated -= new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.GetInventory(BagType.Hero).onDataUpdated += new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.GetInventory(BagType.DragonKnight).onDataUpdated -= new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.GetInventory(BagType.DragonKnight).onDataUpdated += new System.Action<long, long>(this.OnInventoryDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdated);
    int count = this._able2ShelfGoodList.Count;
    for (int key = 0; key < count; ++key)
    {
      if (key < this._able2ShelfGoodList.Count)
      {
        TradingHallShelfGoodSlot shelfGoodSlot = this.GenerateShelfGoodSlot(this._able2ShelfGoodList[key]);
        this._shelfItemDict.Add(key, shelfGoodSlot);
      }
    }
    this.RepositionShelfGood(resetScrollView);
  }

  private void UpdateShelfGoods(TradingHallShelfGoodSlot shelfSlot, int index)
  {
    if ((UnityEngine.Object) shelfSlot == (UnityEngine.Object) null || this._able2ShelfGoodList == null || index >= this._able2ShelfGoodList.Count)
      return;
    shelfSlot.SetData(this._able2ShelfGoodList[index]);
  }

  private void OnInitItem(GameObject obj, int wrapIndex, int realIndex)
  {
    if (realIndex <= -this._wc.minIndex && realIndex > -1)
    {
      NGUITools.SetActive(obj, true);
      this.UpdateShelfGoods(obj.GetComponent<TradingHallShelfGoodSlot>(), realIndex);
    }
    else
      NGUITools.SetActive(obj, false);
  }

  private void UpdateWC(int minIndex, int maxIndex)
  {
    if (this._able2ShelfGoodList != null && this._able2ShelfGoodList.Count <= 24)
      return;
    this._wc = this._shelfItemRoot.GetComponent<UIWrapContentEx>();
    if ((UnityEngine.Object) this._wc == (UnityEngine.Object) null)
      this._wc = this._shelfItemRoot.AddComponent<UIWrapContentEx>();
    if (!((UnityEngine.Object) this._wc != (UnityEngine.Object) null))
      return;
    this._wc.rowNum = 4;
    this._wc.minIndex = minIndex;
    this._wc.maxIndex = maxIndex;
    this._wc.itemWidth = 280;
    this._wc.itemHeight = 270;
    this._wc.cullContent = false;
    UIWrapContentEx wc1 = this._wc;
    wc1.onInitializeItem = wc1.onInitializeItem - new UIWrapContent.OnInitializeItem(this.OnInitItem);
    UIWrapContentEx wc2 = this._wc;
    wc2.onInitializeItem = wc2.onInitializeItem + new UIWrapContent.OnInitializeItem(this.OnInitItem);
    this._wc.SortBasedOnScrollMovement();
    this._wc.WrapContent();
  }

  private void OnSellDataUpdated()
  {
    this.UpdateUI(false);
  }

  private void OnInventoryDataUpdated(long uid, long equipmentItemId)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateUI(false);
  }

  private void OnItemDataUpdated(int itemId)
  {
    this.UpdateUI(false);
  }
}
