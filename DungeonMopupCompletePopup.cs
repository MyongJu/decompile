﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupCompletePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DungeonMopupCompletePopup : Popup
{
  public GameObject _levelPrefab;
  public UITable _levelTable;
  public UIScrollView _levelScrollView;
  public bool NeedReposition;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DragonKnightUtils.ClearMopupHistory();
    MessageHub.inst.GetPortByAction("DragonKnight:tagReadRaidInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    List<DragonKnightDungeonData.RaidInfo> raidInfoList = DragonKnightUtils.GetDungeonData().RaidInfoList;
    raidInfoList.Sort((Comparison<DragonKnightDungeonData.RaidInfo>) ((x, y) => y.level - x.level));
    for (int index = 0; index < raidInfoList.Count; ++index)
    {
      GameObject gameObject = Utils.DuplicateGOB(this._levelPrefab, this._levelTable.transform);
      gameObject.SetActive(true);
      gameObject.GetComponent<DungeonMopupLevelResultRenderer>().SetData(raidInfoList[index], new System.Action(this.OnReposition));
    }
    this._levelTable.Reposition();
    this._levelScrollView.ResetPosition();
  }

  private void OnReposition()
  {
    this.NeedReposition = true;
  }

  private void LateUpdate()
  {
    if (!this.NeedReposition)
      return;
    this.NeedReposition = false;
    this._levelTable.repositionNow = true;
  }
}
