﻿// Decompiled with JetBrains decompiler
// Type: KingdomBuffSubItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class KingdomBuffSubItemRenderer : MonoBehaviour
{
  public UITexture buffTexture;
  public UILabel buffName;
  public UILabel buffValue;
  public UILabel buffDesc;
  public UILabel buffFinishedTime;
  public UILabel buffExtInfo;
  public System.Action onKingdomBuffFinished;
  private KingdomBuffPayload.KingdomBuffData _buffData;

  public void SetData(KingdomBuffPayload.KingdomBuffData buffData)
  {
    this._buffData = buffData;
    this.UpdateUI();
    this.AddEventHandler();
    this.OnSecondEvent(0);
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  private void OnSecondEvent(int time)
  {
    if (this._buffData == null || !((UnityEngine.Object) this.buffFinishedTime != (UnityEngine.Object) null))
      return;
    this.buffFinishedTime.text = Utils.FormatTime(this._buffData.finishedTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    if (NetServerTime.inst.ServerTimestamp <= this._buffData.finishedTime || this.onKingdomBuffFinished == null)
      return;
    this.onKingdomBuffFinished();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void UpdateUI()
  {
    if ((UnityEngine.Object) this.buffValue != (UnityEngine.Object) null)
      NGUITools.SetActive(this.buffValue.gameObject, false);
    if ((UnityEngine.Object) this.buffDesc != (UnityEngine.Object) null)
      NGUITools.SetActive(this.buffDesc.gameObject, false);
    if (this._buffData == null)
      return;
    switch (this._buffData.buffType)
    {
      case KingdomBuffPayload.KingdomBuffType.KINGDOM:
        this.ShowKingdomBuffDetail();
        break;
      case KingdomBuffPayload.KingdomBuffType.ALLIANCE:
        this.ShowAllianceBuffDetail();
        break;
    }
    if ((UnityEngine.Object) this.buffFinishedTime != (UnityEngine.Object) null)
      this.buffFinishedTime.text = Utils.FormatTime(this._buffData.finishedTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    if (!((UnityEngine.Object) this.buffExtInfo != (UnityEngine.Object) null))
      return;
    this.buffExtInfo.supportEncoding = true;
    this.buffExtInfo.text = this.GetBuffExtInfo();
  }

  private string GetBuffExtInfo()
  {
    string str = string.Empty;
    if (this._buffData != null)
    {
      KingSkillInfo byType = ConfigManager.inst.DB_KingSkill.GetByType("after_war_training");
      if (byType != null && byType.KingdomBuffId == this._buffData.buffId)
      {
        UserBenefitParamData benefitParamData = DBManager.inst.DB_UserBenefitParam.Get(PlayerData.inst.uid);
        if (benefitParamData != null)
        {
          int num = benefitParamData.KingdomTrainTotal - benefitParamData.KingdomTrainLeft;
          int kingdomTrainTotal = benefitParamData.KingdomTrainTotal;
          Dictionary<string, string> para = new Dictionary<string, string>();
          if (num >= kingdomTrainTotal)
            para.Add("0", string.Format("[ff0000]{0}/{1}[-]", (object) num, (object) kingdomTrainTotal));
          else
            para.Add("0", string.Format("{0}/{1}", (object) num, (object) kingdomTrainTotal));
          str = ScriptLocalization.GetWithPara("throne_king_skill_2_limit", para, true);
        }
      }
    }
    return str;
  }

  private void ShowKingdomBuffDetail()
  {
    KingdomBuffInfo kingdomBuffInfo = ConfigManager.inst.DB_KingdomBuff.Get(this._buffData.buffId);
    if (kingdomBuffInfo == null || kingdomBuffInfo.benefitId <= 0 || !ConfigManager.inst.DB_Properties.Contains(kingdomBuffInfo.benefitId))
      return;
    this.buffName.text = ConfigManager.inst.DB_Properties[kingdomBuffInfo.benefitId].Name;
    this.buffValue.text = ((double) this._buffData.benefitValue < 0.0 ? (object) "-" : (object) "+").ToString() + (object) (float) ((double) this._buffData.benefitValue * 100.0) + "%";
    BuilderFactory.Instance.HandyBuild((UIWidget) this.buffTexture, kingdomBuffInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.buffValue.gameObject, true);
  }

  private void ShowAllianceBuffDetail()
  {
    TempleSkillInfo buff = this._buffData.buff as TempleSkillInfo;
    if (buff == null)
      return;
    this.buffName.text = buff.Name;
    this.buffDesc.text = ConfigManager.inst.DB_TempleSkill.GetSkillDescription(buff.internalId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.buffTexture, buff.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.buffDesc.gameObject, true);
  }
}
