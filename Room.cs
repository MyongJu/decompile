﻿// Decompiled with JetBrains decompiler
// Type: Room
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Room
{
  protected List<RoomMonsterGroup> _allMonster = new List<RoomMonsterGroup>();
  protected List<RoomChest> _allChest = new List<RoomChest>();
  protected List<int> _allBuff = new List<int>();
  protected List<long> _allPlayer = new List<long>();
  protected List<List<int>> _allPlayerSkill = new List<List<int>>();
  protected List<RoomRaidChest> _allRaidChest = new List<RoomRaidChest>();
  public Room ForwardRoom;
  public Room BackwardRoom;
  public Room LeftRoom;
  public Room RightRoom;
  public int RoomX;
  public int RoomY;
  public int Floor;
  public int FloorInternalId;
  public bool IsExit;
  public bool IsSearched;
  protected Dictionary<string, int> _playerTalent;

  public void ClearData()
  {
    this._allMonster.Clear();
    this._allChest.Clear();
    this._allBuff.Clear();
    this._allPlayer.Clear();
  }

  public string RoomType { get; set; }

  public void SetPlayerTalent(string talent, int value)
  {
    if (this._playerTalent == null)
      this._playerTalent = new Dictionary<string, int>();
    if (this._playerTalent.ContainsKey(talent))
      this._playerTalent.Remove(talent);
    this._playerTalent.Add(talent, value);
  }

  public int GetPlayerTalent(string talent)
  {
    if (this._playerTalent != null && this._playerTalent.ContainsKey(talent))
      return this._playerTalent[talent];
    return 0;
  }

  public List<long> AllPlayer
  {
    get
    {
      return this._allPlayer;
    }
  }

  public List<List<int>> AllPlayerSkill
  {
    get
    {
      return this._allPlayerSkill;
    }
  }

  public List<RoomMonsterGroup> AllMonster
  {
    get
    {
      return this._allMonster;
    }
  }

  public void AddMonster(RoomMonsterGroup monster)
  {
    this._allMonster.Add(monster);
  }

  public void ClearAllMonster()
  {
    this._allMonster.Clear();
  }

  public List<RoomChest> AllChest
  {
    get
    {
      return this._allChest;
    }
  }

  public List<RoomRaidChest> AllRaidChest
  {
    get
    {
      return this._allRaidChest;
    }
  }

  public void AddChest(RoomChest chest)
  {
    this._allChest.Add(chest);
  }

  public void ClearAllChest()
  {
    this._allChest.Clear();
  }

  public List<int> AllBuff
  {
    get
    {
      return this._allBuff;
    }
  }

  public void AddBuff(int buffConfigId)
  {
    this._allBuff.Add(buffConfigId);
  }

  public void ClearAllBuff()
  {
    this._allBuff.Clear();
  }

  public void AddPlayer(long playerId, List<int> allSkill)
  {
    this._allPlayer.Add(playerId);
    this._allPlayerSkill.Add(allSkill);
  }

  public void ClearAllPlayer()
  {
    this._allPlayer.Clear();
    this._allPlayerSkill.Clear();
  }

  public bool ContainsBoss()
  {
    using (List<RoomMonsterGroup>.Enumerator enumerator = this._allMonster.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Type == "boss")
          return true;
      }
    }
    return false;
  }

  public bool ContainsPlayer()
  {
    return this._allPlayer.Count > 0;
  }

  public bool ContainsMonsters()
  {
    return this._allMonster.Count > 0;
  }

  public bool ContainsChest()
  {
    return this._allChest.Count > 0;
  }

  public bool ContainsBuff()
  {
    return this._allBuff.Count > 0;
  }
}
