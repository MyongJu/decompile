﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightAssetLoader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class DragonKnightAssetLoader
{
  private Dictionary<string, bool> _bundleStates = new Dictionary<string, bool>();
  private Queue<string> _bundleQueue = new Queue<string>();
  private Queue<string> _doneQueue = new Queue<string>();
  private const int RETRY_MAX_TIMES = 20;
  private const string EXTENSION = ".assetbundle";
  private int _doneCount;
  private System.Action _callback;
  private int _retryCount;
  private static DragonKnightAssetLoader _instance;

  public static DragonKnightAssetLoader Instance
  {
    get
    {
      if (DragonKnightAssetLoader._instance == null)
        DragonKnightAssetLoader._instance = new DragonKnightAssetLoader();
      return DragonKnightAssetLoader._instance;
    }
  }

  public void Initialize(string[] bundleNames, System.Action callback)
  {
    this.Reset();
    this._callback = callback;
    this._retryCount = 20;
    this.AddListener();
    if (!AssetManager.IsLoadAssetFromBundle)
    {
      foreach (string bundleName in bundleNames)
        LoadingScreenDivide.Instance.Add();
      this.Finish();
    }
    else
    {
      foreach (string bundleName in bundleNames)
      {
        this._bundleStates[bundleName] = false;
        this._bundleQueue.Enqueue(bundleName);
      }
      if (this._bundleStates.Count <= 0)
        return;
      LoadingScreenDivide.Instance.ShowTip(Utils.XLAT("load_cache_bundle") + string.Format(" {0}/{1}", (object) this._doneCount, (object) this._bundleStates.Count));
    }
  }

  private void Reset()
  {
    this._bundleStates.Clear();
    this._bundleQueue.Clear();
    this._doneQueue.Clear();
    this._doneCount = 0;
  }

  private void Finish()
  {
    if (this._callback != null)
      this._callback();
    this.RemoveListener();
    this.Reset();
  }

  private void AddListener()
  {
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdate);
    BundleManager.Instance.onBundleLoaded += new System.Action<string, bool>(this.OnBundleLoadFinished);
    BundleManager.Instance.onBundleBeginToDownload += new System.Action<string, LoaderBatch>(this.OnBunldeStartDownload);
  }

  private void RemoveListener()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdate);
    BundleManager.Instance.onBundleLoaded -= new System.Action<string, bool>(this.OnBundleLoadFinished);
    BundleManager.Instance.onBundleBeginToDownload -= new System.Action<string, LoaderBatch>(this.OnBunldeStartDownload);
  }

  private void OnUpdate(double t)
  {
    while (this._bundleQueue.Count > 0)
      BundleManager.Instance.CacheBundle(this._bundleQueue.Dequeue());
    if (this._doneQueue.Count > 0)
    {
      this._doneQueue.Dequeue();
      ++this._doneCount;
      LoadingScreenDivide.Instance.Add();
      LoadingScreenDivide.Instance.ShowTip(Utils.XLAT("load_cache_bundle") + string.Format(" {0}/{1}", (object) this._doneCount, (object) this._bundleStates.Count));
    }
    else
    {
      if (!this.Done)
        return;
      this.Finish();
    }
  }

  private void OnBunldeStartDownload(string name, LoaderBatch batch)
  {
  }

  private void OnBundleLoadFinished(string name, bool success)
  {
    if (name.EndsWith(".assetbundle"))
      name = name.TrimEnd(".assetbundle".ToCharArray());
    if (!this._bundleStates.ContainsKey(name))
      return;
    if (!success)
    {
      if (this._retryCount == 0)
      {
        this.RemoveListener();
        NetWorkDetector.Instance.RetryTip(new System.Action(this.Retry), ScriptLocalization.Get("data_error_title", true), ScriptLocalization.Get("data_error_loading2_description", true));
      }
      else
      {
        --this._retryCount;
        this._bundleQueue.Enqueue(name);
      }
    }
    else
    {
      if (this._bundleStates[name])
        return;
      this._bundleStates[name] = success;
      this._doneQueue.Enqueue(name);
    }
  }

  private bool Done
  {
    get
    {
      Dictionary<string, bool>.Enumerator enumerator = this._bundleStates.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (!enumerator.Current.Value)
          return false;
      }
      return true;
    }
  }

  private void Retry()
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, bool>.Enumerator enumerator = this._bundleStates.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, bool> current = enumerator.Current;
        if (current.Value)
          stringList.Add(current.Key);
      }
    }
    this.Initialize(stringList.ToArray(), this._callback);
  }
}
