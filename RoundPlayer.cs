﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RoundPlayer
{
  private string _model = string.Empty;
  private StateMachine _stateMachine = new StateMachine();
  private Dictionary<long, BattleBuff> _buffs = new Dictionary<long, BattleBuff>();
  private Dictionary<int, int> _skillCds = new Dictionary<int, int>();
  private List<int> _allSkillIds = new List<int>();
  private long _playerId;
  private int _skillId;
  private int _health;
  private int _healthMax;
  private int _mana;
  private int _manaMax;
  private int _rebund;
  private int _speed;
  private int _defense;
  private int _damge;
  private int _magicDamge;
  private int _strong;
  private int _intelligent;
  private int _aromor;
  private int _constitution;
  private int _fireDamge;
  private int _waterDamge;
  private int _natureDamge;
  private int _hpRecover;
  private int _mpRecover;
  private int _orginHealth;
  private UnityEngine.Animation _animation;
  private IRoundPlayerHud _playerHud;
  private PlayerDamgeAttribute _damgeAttribute;
  private PlayerMagicDamgeAttribute _magicAttribute;
  private PlayerHealthAttribute _healthAttribute;
  private PlayerDefenseAttribute _defenseAttribute;
  private PlayerHealthRecoverAttribute _healthRecoverAttribute;
  private PlayerManaRecoverAttribute _manaRecoverAttribute;
  public GuardEventReceiver Receiver;
  private RoundPlayerAction _action;
  private long _uid;
  public UserData UserData;

  public long Uid
  {
    get
    {
      return this._uid;
    }
    set
    {
      this._uid = value;
    }
  }

  public void AutoAction()
  {
    if (this._action == null)
      this._action = new RoundPlayerAction(this);
    this._action.Play();
    this.SkillId = this._action.SkillId;
  }

  public int TotalHeal
  {
    get
    {
      return this._healthAttribute.TotalHeal;
    }
  }

  public int TotalHurt
  {
    get
    {
      return this._healthAttribute.TotalHurt;
    }
  }

  public virtual RoundPlayer.Camp PlayerCamp
  {
    get
    {
      return RoundPlayer.Camp.None;
    }
  }

  public void SetSkills(params int[] skillIds)
  {
    this._allSkillIds.Clear();
    for (int index = 0; index < skillIds.Length; ++index)
      this._allSkillIds.Add(skillIds[index]);
    this._allSkillIds.Sort(new Comparison<int>(this.CompareSkill));
    this._allSkillIds.Reverse();
  }

  private int CompareSkill(int a, int b)
  {
    ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(a);
    ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(b);
    DragonKnightTalentActiveInfo itemBySkillId1 = ConfigManager.inst.DB_DragonKnightTalentActive.GetItemBySkillId(a);
    DragonKnightTalentActiveInfo itemBySkillId2 = ConfigManager.inst.DB_DragonKnightTalentActive.GetItemBySkillId(b);
    if (itemBySkillId1.skillPriority == itemBySkillId2.skillPriority)
      return a.CompareTo(b);
    return itemBySkillId1.skillPriority.CompareTo(itemBySkillId2.skillPriority);
  }

  public List<int> GetAllSkills()
  {
    return this._allSkillIds;
  }

  public bool HasCd(int skillId)
  {
    return this._skillCds.ContainsKey(skillId);
  }

  public void Startup(UnityEngine.Animation animation, IRoundPlayerHud playerHud)
  {
    this._animation = animation;
    this._playerHud = playerHud;
    this.InitializeStateMachine();
  }

  public abstract void Shutdown();

  public abstract void InitializeStateMachine();

  public void Receive(DragonKnightPacket packet)
  {
    switch (packet.Type)
    {
      case DragonKnightPacket.PacketType.Attack:
        this.ResetBuffs();
        this.UpdateBuffs();
        this._stateMachine.SetState("guard_attack", (Hashtable) null);
        this.UpdateManaHud();
        this.UpdateCD();
        break;
      case DragonKnightPacket.PacketType.Spell:
        this.UpdateSkillCD();
        this.AddSkillCD();
        this.ResetBuffs();
        this.UpdateBuffs();
        this._stateMachine.SetState("guard_spell", (Hashtable) null);
        this.UpdateManaHud();
        this.UpdateHealthHud();
        this.UpdateCD();
        break;
      case DragonKnightPacket.PacketType.UnderAttack:
        DragonKnightPacketUnderAttack packetUnderAttack = packet as DragonKnightPacketUnderAttack;
        this.AddBuffs(packetUnderAttack.Result.Buffs);
        this.UpdateBuffs();
        this.UpdateSkillCD();
        this._stateMachine.SetState("guard_under_attack", (Hashtable) null);
        this.UpdateManaHud();
        this.UpdateHealthHud();
        this.UpdateHealthAnimation(packetUnderAttack.Result);
        break;
      case DragonKnightPacket.PacketType.Enhance:
        DragonKnightPacketEnhance knightPacketEnhance = packet as DragonKnightPacketEnhance;
        this.AddBuffs(knightPacketEnhance.Result.Buffs);
        this.UpdateBuffs();
        this._stateMachine.SetState("guard_enhance", (Hashtable) null);
        this.UpdateManaHud();
        this.UpdateHealthHud();
        this.UpdateHealthAnimation(knightPacketEnhance.Result);
        break;
      case DragonKnightPacket.PacketType.Rebound:
        if (this.Health <= 0)
          this._stateMachine.SetState("guard_death", (Hashtable) null);
        this.UpdateManaHud();
        this.UpdateHealthHud();
        this.UpdateHealthAnimation((packet as DragonKnightPacketRebound).Result);
        break;
    }
  }

  public void ResetRoundPlayerState()
  {
    this.UpdateSkillCD();
    this.AddSkillCD();
    this.ResetBuffs();
  }

  private void OnTutorialFinished()
  {
    BattleManager.Instance.Start();
    TutorialManager.Instance.onTutotialFinished -= new System.Action(this.OnTutorialFinished);
  }

  private void UpdateCD()
  {
    if (this._playerHud == null)
      return;
    this._playerHud.DecreaseCDTime();
  }

  private void UpdateBuffs()
  {
    if (this._playerHud == null)
      return;
    this._playerHud.SetBuffs(this.GetAllBuffs());
  }

  private void UpdateHealthHud()
  {
    if (this._playerHud == null)
      return;
    this._playerHud.SetHealthProgress((float) this.Health / (float) this.HealthMax);
    this._playerHud.SetHealthNumber(this.Health, this.HealthMax);
  }

  private void UpdateHealthAnimation(RoundResult result)
  {
    if (result == null || this._playerHud == null)
      return;
    Color color = Color.white;
    switch (result.Type)
    {
      case RoundResult.ResultType.NormalDamage:
        color = Color.white;
        break;
      case RoundResult.ResultType.SkillDamage:
        color = Color.yellow;
        break;
      case RoundResult.ResultType.Rebound:
        color = Color.yellow;
        break;
      case RoundResult.ResultType.Heal:
        color = Color.green;
        break;
    }
    int number = result.Value;
    if (number > 0)
      this._playerHud.SetIncreaseHealth(number, color);
    else
      this._playerHud.SetDecreaseHealth(number, color);
  }

  private void UpdateManaHud()
  {
    if (this._playerHud == null)
      return;
    this._playerHud.SetManaProgress((float) this.Mana / (float) this.ManaMax);
    this._playerHud.SetManaNumber(this.Mana, this.ManaMax);
  }

  public void AddBuffs(List<BattleBuff> buffs)
  {
    for (int index = 0; index < buffs.Count; ++index)
      this.AddBuff(buffs[index]);
  }

  public bool CanAttack(int skillId)
  {
    return true;
  }

  public void SetHurt(int damge)
  {
    this._healthAttribute.AddValue(damge);
  }

  public int HPRecover
  {
    get
    {
      return this._hpRecover;
    }
    set
    {
      this._hpRecover = value;
    }
  }

  public int MPRecover
  {
    get
    {
      return this._mpRecover;
    }
    set
    {
      this._mpRecover = value;
    }
  }

  public int SkillId
  {
    get
    {
      return this._skillId;
    }
    set
    {
      this._skillId = value;
    }
  }

  public int OrignHealth
  {
    get
    {
      return this._orginHealth;
    }
    set
    {
      this._orginHealth = value;
    }
  }

  public int Strong
  {
    get
    {
      return this._strong;
    }
    set
    {
      this._strong = value;
    }
  }

  public int Aromor
  {
    get
    {
      return this._aromor;
    }
    set
    {
      this._aromor = value;
    }
  }

  public int FireDamge
  {
    get
    {
      return this._fireDamge;
    }
    set
    {
      this._fireDamge = value;
    }
  }

  public int WaterDamge
  {
    get
    {
      return this._waterDamge;
    }
    set
    {
      this._waterDamge = value;
    }
  }

  public int NatureDamge
  {
    get
    {
      return this._natureDamge;
    }
    set
    {
      this._natureDamge = value;
    }
  }

  public int Constitution
  {
    get
    {
      return this._constitution;
    }
    set
    {
      this._constitution = value;
    }
  }

  public int Intelligent
  {
    get
    {
      return this._intelligent;
    }
    set
    {
      this._intelligent = value;
    }
  }

  public long PlayerId
  {
    get
    {
      return this._playerId;
    }
    set
    {
      this._playerId = value;
    }
  }

  public int Health
  {
    get
    {
      return this._healthAttribute.CurrentValue;
    }
    set
    {
      this._health = value;
    }
  }

  public int HealthMax
  {
    get
    {
      return this._healthAttribute.MaxValue;
    }
    set
    {
      this._healthMax = value;
    }
  }

  public bool IsDead
  {
    get
    {
      return this.Health <= 0;
    }
  }

  public bool IsAlive
  {
    get
    {
      return !this.IsDead;
    }
  }

  public int Damge
  {
    get
    {
      return this._damgeAttribute.CurrentValue;
    }
    set
    {
      this._damge = value;
    }
  }

  public int MagicDamge
  {
    get
    {
      return this._magicAttribute.CurrentValue;
    }
    set
    {
      this._magicDamge = value;
    }
  }

  public int Defense
  {
    get
    {
      return this._defenseAttribute.CurrentValue;
    }
    set
    {
      this._defense = value;
    }
  }

  public int Rebund
  {
    get
    {
      bool isEvil = this.PlayerCamp == RoundPlayer.Camp.Evil;
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Uid);
      if (dragonKnightData != null)
        return AttributeCalcHelper.Instance.CalcAttributeOutBattle((float) this._rebund, DragonKnightAttibute.Type.Rebund, isEvil, (List<BattleBuff>) null, dragonKnightData);
      return this._rebund;
    }
    set
    {
      this._rebund = value;
    }
  }

  public int Speed
  {
    get
    {
      bool isEvil = this.PlayerCamp == RoundPlayer.Camp.Evil;
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(this.Uid);
      if (dragonKnightData != null)
        return AttributeCalcHelper.Instance.CalcAttributeOutBattle((float) this._speed, DragonKnightAttibute.Type.Speed, isEvil, (List<BattleBuff>) null, dragonKnightData);
      return this._speed;
    }
    set
    {
      this._speed = value;
    }
  }

  public int Mana
  {
    get
    {
      return this._mana;
    }
    set
    {
      this._mana = value;
      if (this.ManaMax == 0 || this._mana <= this.ManaMax)
        return;
      this._mana = this.ManaMax;
    }
  }

  public int ManaMax
  {
    get
    {
      return this._manaMax;
    }
    set
    {
      this._manaMax = value;
    }
  }

  public string Model
  {
    get
    {
      return this._model;
    }
    set
    {
      this._model = value;
    }
  }

  public UnityEngine.Animation Animation
  {
    get
    {
      return this._animation;
    }
  }

  public IRoundPlayerHud PlayerHud
  {
    get
    {
      return this._playerHud;
    }
  }

  public StateMachine StateMacine
  {
    get
    {
      return this._stateMachine;
    }
  }

  public override string ToString()
  {
    return string.Format("[RoundPlayer: RoundPlayer={0}, SkillId={1}, PlayerId={2}, Health={3}, Damge={4}, Defense={5}, Rebund={6}, Speed={7}, Mana={8}]", (object) this, (object) this.SkillId, (object) this.PlayerId, (object) this.Health, (object) this.Damge, (object) this.Defense, (object) this.Rebund, (object) this.Speed, (object) this.Mana);
  }

  public void AddBuff(BattleBuff buff)
  {
    if (this._buffs.ContainsKey((long) buff.BuffId))
      this._buffs[(long) buff.BuffId].Reset(buff);
    else
      this._buffs.Add((long) buff.BuffId, buff);
    this.ResetAttributes();
  }

  public void ClearBuff()
  {
    this._buffs.Clear();
  }

  public List<BattleBuff> GetAllBuffs()
  {
    List<BattleBuff> battleBuffList = new List<BattleBuff>();
    Dictionary<long, BattleBuff>.ValueCollection.Enumerator enumerator = this._buffs.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null)
        battleBuffList.Add(enumerator.Current);
    }
    battleBuffList.Sort(new Comparison<BattleBuff>(this.Compare));
    return battleBuffList;
  }

  private int Compare(BattleBuff a, BattleBuff b)
  {
    return a.CTime.CompareTo(b.CTime);
  }

  public virtual void SmallRoundStart()
  {
  }

  private void AddSkillCD()
  {
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this.SkillId);
    if (knightTalentInfo == null || knightTalentInfo.cdTime <= 0)
      return;
    if (this._skillCds.ContainsKey(this.SkillId))
      this._skillCds[this.SkillId] = knightTalentInfo.cdTime;
    else
      this._skillCds.Add(this.SkillId, knightTalentInfo.cdTime);
  }

  public bool CheckMp(int skillId)
  {
    DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(skillId);
    if (knightTalentInfo == null)
      return false;
    return knightTalentInfo.manaCost <= this.Mana;
  }

  protected void UpdateSkillCD()
  {
    if (this._skillCds.Count <= 0)
      return;
    List<int> intList = new List<int>();
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = this._skillCds.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this._skillCds[enumerator.Current] - 1 <= 0)
        intList.Add(enumerator.Current);
    }
    for (int index = 0; index < intList.Count; ++index)
      this._skillCds.Remove(intList[index]);
  }

  protected void ResetBuffs()
  {
    List<long> longList = new List<long>();
    Dictionary<long, BattleBuff>.ValueCollection.Enumerator enumerator = this._buffs.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null)
        enumerator.Current.Attenuation();
      if (enumerator.Current.IsFinish())
        longList.Add((long) enumerator.Current.BuffId);
    }
    for (int index = 0; index < longList.Count; ++index)
      this._buffs.Remove(longList[index]);
    if (longList.Count <= 0)
      return;
    this.ResetAttributes();
  }

  public void RoundFinish()
  {
    this.OnRoundFinish();
  }

  protected void ResetAttributes()
  {
    this._damgeAttribute.Reset();
    this._magicAttribute.Reset();
    this._healthAttribute.Reset();
    this._defenseAttribute.Reset();
    this._manaRecoverAttribute.Reset();
    this._healthRecoverAttribute.Reset();
    if (this.PlayerCamp == RoundPlayer.Camp.Moral && (DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "monster" || DragonKnightSystem.Instance.RoomManager.CurrentRoom.RoomType == "boss"))
      this._healthAttribute.SetCurrentValue(this._orginHealth);
    BattleManager.Instance.SetPlayerLog(string.Empty, this.PlayerId);
  }

  protected virtual void OnRoundFinish()
  {
  }

  public void Init()
  {
    this._damgeAttribute = new PlayerDamgeAttribute(this);
    this._damgeAttribute.Setup(this._damge, 0);
    this._magicAttribute = new PlayerMagicDamgeAttribute(this);
    this._magicAttribute.Setup(this._magicDamge, 0);
    this._healthAttribute = new PlayerHealthAttribute(this);
    this._healthAttribute.Setup(this._health, this._healthMax);
    this._defenseAttribute = new PlayerDefenseAttribute(this);
    this._defenseAttribute.Setup(this._defense, 0);
    this._manaRecoverAttribute = new PlayerManaRecoverAttribute(this);
    this._manaRecoverAttribute.Setup(this._mpRecover, 0);
    this._healthRecoverAttribute = new PlayerHealthRecoverAttribute(this);
    this._healthRecoverAttribute.Setup(this._hpRecover, 0);
  }

  public void SetCurrentValue(DragonKnightAttibute.Type type, int value)
  {
    if (type != DragonKnightAttibute.Type.Health && type != DragonKnightAttibute.Type.MP)
      return;
    if (type == DragonKnightAttibute.Type.Health)
    {
      this._orginHealth = value;
      this._healthAttribute.SetCurrentValue(value);
    }
    if (type != DragonKnightAttibute.Type.MP)
      return;
    this._mana = value;
  }

  public enum Camp
  {
    None,
    Evil,
    Moral,
  }
}
