﻿// Decompiled with JetBrains decompiler
// Type: CityTroopsAnimControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class CityTroopsAnimControler : MonoBehaviour
{
  private static Vector3[] troopDeltaPosition1 = new Vector3[1]
  {
    new Vector3(0.0f, 0.0f, 0.0f)
  };
  private static Vector3[] troopDeltaPosition2 = new Vector3[4]
  {
    new Vector3(0.5f, 0.0f, 0.0f),
    new Vector3(0.0f, 0.5f, 0.1f),
    new Vector3(-0.5f, 0.0f, 0.05f),
    new Vector3(0.0f, -0.5f, -0.1f)
  };
  private static Vector3[] troopDeltaPosition4 = new Vector3[4]
  {
    new Vector3(0.4f, 0.0f, 0.0f),
    new Vector3(0.0f, 0.4f, 0.1f),
    new Vector3(-0.4f, 0.0f, 0.05f),
    new Vector3(0.0f, -0.4f, -0.1f)
  };
  private int MIN_POINTS_COUNT = 2;
  private string[] TroopViewPath = new string[5]
  {
    "Prefab/TroopModels/Infantry",
    "Prefab/TroopModels/Archer",
    "Prefab/TroopModels/Knight",
    "Prefab/TroopModels/Mage",
    "Prefab/TroopModels/Catapult"
  };
  public const float ONE_OVER_CITY_TROOPS_VELOCITY = 0.01f;
  public const float CITY_TROOPS_VELOCITY = 100f;
  private Vector3[] _points;
  private float[] _timePoints;
  private float[] _oneOverTimeLength;
  private float _startTime;
  private bool _startFlag;
  private int _troopsCount;
  private Vector3[] _troopDeltaPosition;
  private Vector3 _troopScale;
  private TroopType _troopsType;
  private int _troopNumber;
  private Vector3 stepStartPoint;
  private Vector3 stepEndPoint;
  private float stepStartTime;
  private float stepOneOverTime;
  private int stepIndex;
  private bool isSetuped;
  private Animator[] animators;
  public int ID2;

  public event System.Action<TroopType, int> troopReachDes;

  public int ID { private set; get; }

  public void StartMoving(List<Vector3> points, CityTroopsAnimControler.Parameter param)
  {
    this.ID = param.ID;
    if (points.Count < this.MIN_POINTS_COUNT)
      return;
    this._points = points.ToArray();
    this._timePoints = new float[points.Count];
    this._oneOverTimeLength = new float[points.Count - 1];
    this._timePoints[0] = this._startTime = param.startTime;
    int index = 1;
    for (int length = this._points.Length; index < length; ++index)
    {
      this._timePoints[index] = this._timePoints[index - 1] + (this._points[index] - points[index - 1]).magnitude * 0.01f;
      this._oneOverTimeLength[index - 1] = (float) (1.0 / ((double) this._timePoints[index] - (double) this._timePoints[index - 1]));
    }
    this._troopsType = param.troopType;
    this._troopNumber = param.troopsCount;
    switch (this._troopsType)
    {
      case TroopType.class_infantry:
        this._troopsCount = 4;
        this._troopScale = new Vector3(0.3f, 0.3f, 0.3f);
        this._troopDeltaPosition = CityTroopsAnimControler.troopDeltaPosition4;
        break;
      case TroopType.class_ranged:
        this._troopsCount = 4;
        this._troopScale = new Vector3(0.27f, 0.27f, 0.27f);
        this._troopDeltaPosition = CityTroopsAnimControler.troopDeltaPosition4;
        break;
      case TroopType.class_cavalry:
        this._troopsCount = 4;
        this._troopScale = new Vector3(0.26f, 0.26f, 0.26f);
        this._troopDeltaPosition = CityTroopsAnimControler.troopDeltaPosition2;
        break;
      case TroopType.class_artyfar:
        this._troopsCount = 4;
        this._troopScale = new Vector3(0.19f, 0.19f, 0.19f);
        this._troopDeltaPosition = CityTroopsAnimControler.troopDeltaPosition4;
        break;
    }
    this.CreateTroopsModel(param.troopType);
  }

  private bool CalcStep()
  {
    if (this.stepIndex >= this._points.Length - 1)
      return false;
    this.stepStartPoint = this._points[this.stepIndex];
    this.stepEndPoint = this._points[this.stepIndex + 1];
    Vector3 vector3_1 = this.stepEndPoint - this.stepStartPoint;
    vector3_1.z = 0.0f;
    vector3_1.Normalize();
    this.stepStartTime = Time.time;
    Vector3 vector3_2 = this.stepEndPoint - this.stepStartPoint;
    vector3_2.z = 0.0f;
    this.stepOneOverTime = 100f / vector3_2.magnitude;
    for (int index = 0; index < this._troopsCount; ++index)
    {
      this.animators[index].transform.localScale = new Vector3(Mathf.Abs(this.animators[index].transform.localScale.x) * ((double) vector3_1.x <= 0.379999995231628 ? 1f : -1f), this.animators[index].transform.localScale.y, this.animators[index].transform.localScale.z);
      this.animators[index].SetInteger("x", (double) vector3_1.x >= -0.379999995231628 ? ((double) vector3_1.x >= 0.379999995231628 ? 1 : 0) : -1);
      this.animators[index].SetInteger("y", (double) vector3_1.y >= -0.379999995231628 ? ((double) vector3_1.y >= 0.379999995231628 ? 1 : 0) : -1);
      this.animators[index].SetBool("attack", false);
    }
    return true;
  }

  public void FixedUpdate()
  {
    if (!this.isSetuped)
      return;
    if (!this._startFlag)
    {
      if ((double) Time.time >= (double) this._startTime)
      {
        this._startFlag = true;
        this.stepIndex = 0;
        this.CalcStep();
      }
    }
    else
    {
      float t = (Time.time - this.stepStartTime) * this.stepOneOverTime;
      if ((double) t < 1.0)
      {
        this.transform.position = Vector3.Lerp(this.stepStartPoint, this.stepEndPoint, t);
      }
      else
      {
        ++this.stepIndex;
        if (this.CalcStep())
        {
          this.transform.position = this.stepStartPoint;
        }
        else
        {
          if (this.troopReachDes != null)
            this.troopReachDes(this._troopsType, this._troopNumber);
          CityTroopsAnimManager.inst.DestoryTroops(this.ID);
        }
      }
    }
    this.ID2 = this.ID;
  }

  public void CreateTroopsModel(TroopType troopType)
  {
    switch (troopType)
    {
      case TroopType.class_infantry:
      case TroopType.class_ranged:
      case TroopType.class_cavalry:
      case TroopType.class_artyclose:
      case TroopType.class_artyfar:
        AssetManager.Instance.LoadAsync(this.TroopViewPath[(int) troopType], (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          if (!ret)
            return;
          this.OnTroopLoadFinished(obj);
          AssetManager.Instance.UnLoadAsset(this.TroopViewPath[(int) troopType], (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        break;
      default:
        AssetManager.Instance.LoadAsync(this.TroopViewPath[0], (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
        {
          if (!ret)
            return;
          this.OnTroopLoadFinished(obj);
          AssetManager.Instance.UnLoadAsset(this.TroopViewPath[0], (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        break;
    }
  }

  private void OnTroopLoadFinished(UnityEngine.Object orgTroops)
  {
    this.animators = new Animator[this._troopsCount];
    for (int index = 0; index < this._troopsCount; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate(orgTroops) as GameObject;
      gameObject.name = "Troop";
      gameObject.transform.parent = this.transform;
      gameObject.transform.localPosition = this._troopDeltaPosition[index];
      gameObject.transform.localScale = this._troopScale;
      SpriteRenderer component = gameObject.GetComponent<SpriteRenderer>();
      Color color = component.color;
      color.a = 1f;
      component.color = color;
      component.sortingLayerName = "Default";
      component.sortingOrder = 0;
      this.animators[index] = gameObject.GetComponent<Animator>();
      this.animators[index].SetInteger("x", 1);
    }
    this._startFlag = false;
    this.isSetuped = true;
  }

  public struct Parameter
  {
    public int troopsCount;
    public TroopType troopType;
    public float startTime;
    public int ID;
  }
}
