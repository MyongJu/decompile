﻿// Decompiled with JetBrains decompiler
// Type: AlliancMemeberScoreItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AlliancMemeberScoreItem : MonoBehaviour
{
  public UILabel userName;
  public UILabel score;
  public UILabel index;

  public void SetData(AlliancePlayerScoreData data)
  {
    this.userName.text = data.Name;
    this.score.text = Utils.FormatThousands(data.Score.ToString());
    this.index.text = data.Rank.ToString();
  }
}
