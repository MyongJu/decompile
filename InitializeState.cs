﻿// Decompiled with JetBrains decompiler
// Type: InitializeState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using I2.Loc;
using System.Collections;
using System.Text;
using UI;
using UnityEngine;

public class InitializeState : LoadBaseState
{
  private bool redirected;
  private static int update_time;

  public InitializeState(int step)
    : base(step)
  {
  }

  public override string Key
  {
    get
    {
      return nameof (InitializeState);
    }
  }

  protected override void Prepare()
  {
    base.Prepare();
    this.PreSetting();
    this.LoadRemote();
  }

  private void LoadRemote()
  {
    Hashtable hashtable1 = new Hashtable();
    hashtable1[(object) "class"] = (object) "call";
    hashtable1[(object) "method"] = (object) "manifest";
    Hashtable hashtable2 = new Hashtable();
    hashtable2.Add((object) "os", (object) NativeManager.inst.GetOS());
    string severChannel = CustomDefine.GetSeverChannel();
    if (!string.IsNullOrEmpty(severChannel))
      hashtable2.Add((object) "channel", (object) severChannel);
    hashtable2.Add((object) "client_version", (object) NativeManager.inst.AppMajorVersion);
    hashtable2.Add((object) "client_full_version", (object) NativeManager.inst.AppVersion);
    hashtable2.Add((object) "client_lang", (object) LocalizationManager.CurrentLanguageCode);
    string sysLanguage = NativeManager.inst.SysLanguage;
    hashtable2.Add((object) "sys_lang", !string.IsNullOrEmpty(sysLanguage) ? (object) sysLanguage : (object) "en-US");
    hashtable1.Add((object) "params", (object) hashtable2);
    if (!this.redirected)
      NetApi.inst.BuildFinalServerURL();
    LoaderInfo loaderInfo = this.GetLoaderInfo("manifest", NetApi.inst.BuildApiUrl(string.Empty));
    loaderInfo.PostData = hashtable1;
    this.batch.Add(loaderInfo);
    LoaderManager.inst.Add(this.batch, false, false);
  }

  private void PreSetting()
  {
    if (Application.isEditor)
      return;
    FunplusRum.Instance.SetRUMDefaultConfig("koa.global.prod", "d3b7c2e58b97b29faa637dc31af8feb6", "devops.rum.us", "1.0", "https://logagent.infra.funplus.net/log", 1);
    string[] adjustInfo = CustomDefine.GetAdjustInfo();
    if (adjustInfo.Length > 2)
      AdjustProxy.Instance.SetConfig(adjustInfo[0], adjustInfo[1], adjustInfo[2]);
    string channel = CustomDefine.GetChannel();
    if (!string.IsNullOrEmpty(channel))
      FunplusSdk.Instance.setGamePackageChannel(channel);
    if (CustomDefine.IsiOSCNDefine())
    {
      ReYunTrack.Instance.Track_Init("f42eb0d2a4aca2e7d063466f58c61724", "unknow");
    }
    else
    {
      if (!CustomDefine.IsOfficialDefine())
        return;
      ReYunTrack.Instance.Track_Init("2b1a4c6459a931c862af1ad1be7572c9", "unknow");
    }
  }

  protected override void OnAllFileFinish(LoaderBatch batch)
  {
    base.OnAllFileFinish(batch);
    LoaderInfo resultInfo = batch.GetResultInfo("manifest");
    if (resultInfo.ResData == null || !this.CheckResult(resultInfo))
      return;
    Hashtable hashtable = Utils.Json2Object(Utils.ByteArray2String(resultInfo.ResData), true) as Hashtable;
    string empty1 = string.Empty;
    if (hashtable.Contains((object) "ok"))
      empty1 = hashtable[(object) "ok"].ToString();
    if (string.IsNullOrEmpty(empty1) || empty1 != "1")
    {
      string empty2 = string.Empty;
      if (hashtable.ContainsKey((object) "msg"))
        empty2 = hashtable[(object) "msg"].ToString();
      string title = ScriptLocalization.Get("data_error_title", true);
      string tip = ScriptLocalization.Get("data_error_file_parse_description", true);
      if (!NetWorkDetector.Instance.IsReadyRestart())
        NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Loading, "Manifest", empty2);
      NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
    }
    else
    {
      string empty2 = string.Empty;
      if (hashtable.ContainsKey((object) "time") && hashtable[(object) "time"] != null)
      {
        empty2 = hashtable[(object) "time"].ToString();
        NetServerTime.inst.SetServerTime((double) long.Parse(empty2) / 1000.0);
      }
      if (hashtable.ContainsKey((object) "payload") && hashtable[(object) "payload"] != null)
      {
        NetApi.inst.Manifest = hashtable[(object) "payload"] as Hashtable;
        if (this.CheckRedirect())
        {
          this.ResetBatch();
          this.Prepare();
          return;
        }
        if (NetApi.inst.Manifest.ContainsKey((object) "payment"))
          PaymentManager.Instance.PaymentServerId = NetApi.inst.Manifest[(object) "payment"].ToString();
        PlayerPrefsEx.DeleteKey("server_ip");
        if (NetApi.inst.Manifest.ContainsKey((object) "server_ip"))
        {
          ArrayList arrayList = NetApi.inst.Manifest[(object) "server_ip"] as ArrayList;
          StringBuilder stringBuilder = new StringBuilder();
          foreach (object obj in arrayList)
          {
            stringBuilder.Append("http://");
            stringBuilder.Append(obj);
            stringBuilder.Append("/api/");
            stringBuilder.Append("|");
          }
          PlayerPrefsEx.SetString("server_ip", stringBuilder.ToString());
        }
        GameLoadingAnnouncementPayload.Instance.AnnouncementContent = !NetApi.inst.Manifest.ContainsKey((object) "loadingAnnounce") || NetApi.inst.Manifest[(object) "loadingAnnounce"] == null ? (string) null : NetApi.inst.Manifest[(object) "loadingAnnounce"].ToString();
        ABTestManager.Instance.Init(NetApi.inst.Manifest[(object) "abtest"] as Hashtable);
        this.CheckGateWay();
        this.CheckSDK();
        if (NetApi.inst.Manifest != null && NetApi.inst.Manifest.ContainsKey((object) "app_id") && NetApi.inst.Manifest[(object) "app_id"] != null)
          PaymentManager.Instance.PaymentAppId = NetApi.inst.Manifest[(object) "app_id"].ToString();
        if (NetApi.inst.Manifest != null && NetApi.inst.Manifest.ContainsKey((object) "sdk"))
          FunplusSdk.Instance.FunplusSDKInit(Utils.Object2Json(NetApi.inst.Manifest[(object) "sdk"]), empty2);
      }
      if (NetApi.inst.Encrypted)
        Encryptor.Open(string.Empty);
      this.CheckVersion();
    }
  }

  private void CheckSDK()
  {
    if (Application.isEditor)
      return;
    if (ABTestManager.Instance.IsTestRunning("config_server"))
    {
      StringABCondtion condition = ABTestManager.Instance.GetCondition<StringABCondtion>("config_server");
      if (!string.IsNullOrEmpty(condition.ConditionValue()))
        FunplusSdk.Instance.setConfigServerEndpoint(condition.ConditionValue());
    }
    if (ABTestManager.Instance.IsTestRunning("payment_server"))
    {
      StringABCondtion condition = ABTestManager.Instance.GetCondition<StringABCondtion>("payment_server");
      string empty = string.Empty;
      if (condition != null)
      {
        string endpointDomain = condition.ConditionValue();
        if (!string.IsNullOrEmpty(endpointDomain))
        {
          FunplusSdk.Instance.setPaymentServerEndpoint(endpointDomain);
          PaymentManager.Instance.PaymentUrl = endpointDomain;
        }
      }
    }
    if (!ABTestManager.Instance.IsTestRunning("passport_server"))
      return;
    StringABCondtion condition1 = ABTestManager.Instance.GetCondition<StringABCondtion>("passport_server");
    string empty1 = string.Empty;
    if (condition1 == null)
      return;
    string endpointDomain1 = condition1.ConditionValue();
    if (string.IsNullOrEmpty(endpointDomain1))
      return;
    FunplusSdk.Instance.setPassportServerEndpoint(endpointDomain1);
  }

  private void CheckGateWay()
  {
    if (ABTestManager.Instance.IsTestRunning("ios_gateway"))
      NetApi.DefaultServerURL = NetApi.inst.BuildDefaultServerURL(ABTestManager.Instance.GetCondition<StringABCondtion>("ios_gateway").ConditionValue());
    if (!ABTestManager.Instance.IsTestRunning("android_gateway"))
      return;
    NetApi.DefaultServerURL = NetApi.inst.BuildDefaultServerURL(ABTestManager.Instance.GetCondition<StringABCondtion>("android_gateway").ConditionValue());
  }

  private void CheckVersion()
  {
    if (Application.isEditor)
    {
      this.Finish();
    }
    else
    {
      switch (NetApi.inst.State + 1)
      {
        case NetApi.AppVersioState.Update:
          InitializeState.update_time = PlayerPrefs.GetInt("update_time", 0);
          this.UpgradeApp();
          break;
        case NetApi.AppVersioState.Normal:
          PlayerPrefs.DeleteKey("update_time");
          this.ChooseUpdate();
          break;
        case (NetApi.AppVersioState) 2:
          PlayerPrefs.DeleteKey("update_time");
          this.Finish();
          break;
        default:
          string title = ScriptLocalization.Get("data_error_title", true);
          string tip = ScriptLocalization.Get("data_error_file_parse_description", true);
          if (!NetWorkDetector.Instance.IsReadyRestart())
            NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Loading, "Manifest", "no state found in app_ver");
          NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
          break;
      }
    }
  }

  private bool CheckRedirect()
  {
    if (!NetApi.inst.Manifest.ContainsKey((object) "redirect") || !(NetApi.DefaultServerURL != NetApi.inst.Manifest[(object) "redirect"].ToString()))
      return false;
    this.redirected = true;
    NetApi.DefaultServerURL = NetApi.inst.Manifest[(object) "redirect"].ToString();
    return true;
  }

  private void ChooseUpdate()
  {
    UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("update_title", true), ScriptLocalization.Get("update_choice_description", true), ScriptLocalization.Get("update_choice_yes", true), ScriptLocalization.Get("update_choice_no", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmUpdateCallBack), new System.Action(((LoadBaseState) this).Finish), new System.Action(((LoadBaseState) this).Finish));
  }

  private void UpgradeApp()
  {
    PlayerPrefs.SetInt("update_time", InitializeState.update_time + 1);
    ChooseConfirmationBox.ButtonState states = ChooseConfirmationBox.ButtonState.OK_CENTER;
    string right = ScriptLocalization.Get("update_official_site_button", true);
    if (NetApi.inst.NeedOfficialUpdate == NetApi.OfficialUpdateState.Open)
    {
      if (Application.platform == RuntimePlatform.Android)
        states = ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT;
    }
    else if (NetApi.inst.NeedOfficialUpdate == NetApi.OfficialUpdateState.Normal && InitializeState.update_time >= 1 && Application.platform == RuntimePlatform.Android)
      states = ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT;
    UIManager.inst.ShowConfirmationBox(ScriptLocalization.Get("update_title", true), ScriptLocalization.Get("update_no_choice_description", true), ScriptLocalization.Get("update_choice_yes", true), right, states, new System.Action(this.ConfirmUpdateCallBack), new System.Action(this.ConfirmUpdateFromOfficial), new System.Action(this.ConfirmUpdateCallBack));
  }

  private void ConfirmUpdateFromOfficial()
  {
    if (Application.platform != RuntimePlatform.Android)
      this.ConfirmUpdateCallBack();
    else if (!string.IsNullOrEmpty(NetApi.inst.OfficialUpdateURL))
    {
      Application.OpenURL(NetApi.inst.OfficialUpdateURL);
      Application.Quit();
    }
    else
      this.ConfirmUpdateCallBack();
  }

  private void ConfirmUpdateCallBack()
  {
    if (string.IsNullOrEmpty(NetApi.inst.UpdateURL))
      return;
    Application.OpenURL(NetApi.inst.UpdateURL);
    Application.Quit();
  }

  protected override void Retry()
  {
    this.ResetBatch();
    NetApi.DefaultServerURL = NetApi.inst.BuildDefaultServerURL("http://dw-us.funplusgame.com");
    this.redirected = true;
    this.LoadRemote();
  }

  protected override void Finish()
  {
    if (this.IsDestroy)
      return;
    base.Finish();
    this.RefreshProgress();
    this.Preloader.OnInitFinish();
  }

  protected override SplashDataConfig.Phase CurrentPhase
  {
    get
    {
      return SplashDataConfig.Phase.Init;
    }
  }
}
