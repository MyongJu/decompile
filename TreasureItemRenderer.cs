﻿// Decompiled with JetBrains decompiler
// Type: TreasureItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TreasureItemRenderer : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemName;
  public UILabel m_ItemCount;
  public Transform border;
  public UITexture background;
  private ItemStaticInfo m_ItemInfo;

  public void SetData(DropMainData dropMainData)
  {
    this.SetData(dropMainData.ItemId, dropMainData.MinValue);
  }

  public void SetData(int itemId, int count)
  {
    this.m_ItemInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this.SetItemData(this.m_ItemInfo, count);
  }

  public void OnItemClick()
  {
    if (this.m_ItemInfo == null)
      return;
    Utils.ShowItemTip(this.m_ItemInfo.internalId, this.border, 0L, 0L, 0);
  }

  public void OnItemPress()
  {
    if (this.m_ItemInfo == null)
      return;
    Utils.DelayShowTip(this.m_ItemInfo.internalId, this.border, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public ItemStaticInfo ItemInfo
  {
    get
    {
      return this.m_ItemInfo;
    }
  }

  private void SetItemData(ItemStaticInfo itemInfo, int count)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, itemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemName(this.m_ItemName, itemInfo.internalId);
    this.m_ItemCount.text = "x " + count.ToString();
    Utils.SetItemBackground(this.background, itemInfo.internalId);
  }
}
