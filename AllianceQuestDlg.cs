﻿// Decompiled with JetBrains decompiler
// Type: AllianceQuestDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceQuestDlg : MonoBehaviour
{
  private GameObjectPool pool = new GameObjectPool();
  private List<AllianceQuestItemRenderer> items = new List<AllianceQuestItemRenderer>();
  public UILabel refreshTimeLabel;
  public UIButton refresh;
  public UIScrollView scroll;
  public UITable container;
  public AllianceQuestItemRenderer itemPrefab;
  public GameObject itemRoot;
  private bool init;

  public void Clear()
  {
    List<AllianceQuestItemRenderer>.Enumerator enumerator = this.items.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.Clear();
      this.pool.Release(enumerator.Current.gameObject);
    }
    this.items.Clear();
    this.RemoveEventHandler();
  }

  public void OnClickRefresh()
  {
    string str1 = "Alliance";
    string str2 = "shopitem_alliance_quest_refresh";
    UIManager.inst.OpenPopup("QuestRefreshPopup", (Popup.PopupParameter) new QuestRefreshDlg.Parameter()
    {
      titleText = str1,
      itemNeed = str2
    });
  }

  public void StartLoadAllainceQuest()
  {
    MessageHub.inst.GetPortByAction("Quest:loadAllianceQuest").SendRequest((Hashtable) null, new System.Action<bool, object>(this.OnReceivedAllianceQuests), true);
  }

  private void OnReceivedAllianceQuests(bool success, object result)
  {
    if (!success)
      return;
    DBManager.inst.DB_Local_TimerQuest.UpdateDatas(result, (long) NetServerTime.inst.ServerTimestamp);
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    if (!this.init)
    {
      this.init = true;
      this.pool.Initialize(this.itemPrefab.gameObject, this.itemRoot);
    }
    this.Clear();
    this.AddEventHandler();
    long num = -1;
    List<long> totalQuests = DBManager.inst.DB_Local_TimerQuest.GetTotalQuests();
    for (int index = 0; index < totalQuests.Count; ++index)
    {
      long questId = totalQuests[index];
      if (DBManager.inst.DB_Local_TimerQuest.Get(questId).jobId > 0L)
        num = questId;
      GameObject gameObject = this.pool.AddChild(this.container.gameObject);
      gameObject.SetActive(true);
      AllianceQuestItemRenderer component = gameObject.GetComponent<AllianceQuestItemRenderer>();
      component.SetData(questId);
      this.items.Add(component);
    }
    this.container.repositionNow = true;
    this.container.Reposition();
    this.scroll.ResetPosition();
    NGUITools.SetActive(this.scroll.gameObject, false);
    NGUITools.SetActive(this.scroll.gameObject, true);
    this.Refresh();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.RefreshTime);
    DBManager.inst.DB_Local_TimerQuest.onDataRemoved += new System.Action(this.RefreshQuestDataOnRemove);
    DBManager.inst.DB_Local_TimerQuest.onDataCreated += new System.Action<long>(this.RefreshQuestData);
  }

  private void RefreshQuestDataOnRemove()
  {
    this.UpdateUI();
  }

  private void RefreshQuestData(long id)
  {
    this.UpdateUI();
  }

  private void RefreshTime(int time)
  {
    this.Refresh();
  }

  private void Refresh()
  {
    int num = 28800;
    this.refreshTimeLabel.text = Utils.FormatTime((NetServerTime.inst.ServerTimestamp / num + 1) * num - NetServerTime.inst.ServerTimestamp - 14400, false, false, true);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshTime);
    DBManager.inst.DB_Local_TimerQuest.onDataRemoved -= new System.Action(this.RefreshQuestDataOnRemove);
    DBManager.inst.DB_Local_TimerQuest.onDataCreated -= new System.Action<long>(this.RefreshQuestData);
  }
}
