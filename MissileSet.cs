﻿// Decompiled with JetBrains decompiler
// Type: MissileSet
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MissileSet
{
  private LinkedList<Missile> _waitList = new LinkedList<Missile>();
  private List<Missile> _missiles = new List<Missile>();
  private float _stopTimestamp = -1f;
  private MissileSystem _system;
  private Animator _animation;
  private AnimationEventHandler _animationEventHandler;
  private string _shootStateName;
  private float _shootInterval;
  private int _maxShoot;
  private bool _isPlaying;

  public MissileSet(MissileSystem system)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) system)
      Debug.LogError((object) "Create Missile Set Error:Missile System must exist.");
    this._system = system;
    this._animation = this._system.CreateAnimation();
    this._animationEventHandler = Utils.EnsureComponent<AnimationEventHandler>(this._animation.gameObject);
    if ((UnityEngine.Object) this._animationEventHandler != (UnityEngine.Object) null)
      this._animationEventHandler.OnAnimationEvent += new System.Action<AnimationEvent>(this.OnAnimationEvent);
    this._isPlaying = false;
  }

  public float ShootInterval
  {
    set
    {
      this._shootInterval = value;
    }
    get
    {
      return this._shootInterval;
    }
  }

  public string ShootStateName
  {
    set
    {
      this._shootStateName = value;
    }
    get
    {
      return this._shootStateName;
    }
  }

  public int MaxShoot
  {
    set
    {
      this._maxShoot = value;
    }
    get
    {
      return this._maxShoot;
    }
  }

  public void Update()
  {
    this.UpdateAnimation();
  }

  public void Clear()
  {
    this._waitList.Clear();
    this.OnAnimationEnd();
  }

  public void Dispose()
  {
    this.Clear();
    if ((UnityEngine.Object) this._animationEventHandler != (UnityEngine.Object) null)
    {
      this._animationEventHandler.OnAnimationEvent -= new System.Action<AnimationEvent>(this.OnAnimationEvent);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._animationEventHandler);
    }
    this._system.DestoryAnimation(this._animation);
    this._system = (MissileSystem) null;
    this._animation = (Animator) null;
  }

  public void Shoot(Vector3 source, Vector3 target, int tag = 0)
  {
    Missile missile = this._system.CreateMissile();
    if ((UnityEngine.Object) missile == (UnityEngine.Object) null)
      return;
    missile.normlizedAnimation = this._animation;
    missile.PrepareShoot(source, target);
    missile.tag = tag;
    this._waitList.AddLast(missile);
  }

  public void Stop(int tag = 0)
  {
    LinkedListNode<Missile> node = this._waitList.First;
    LinkedListNode<Missile> next;
    for (; node != null; node = next)
    {
      next = node.Next;
      if (node.Value.tag == tag)
      {
        this._system.DestoryMissile(node.Value);
        this._waitList.Remove(node);
      }
    }
  }

  public void PlayMissleAnimation(string name, float time = 0)
  {
    for (int index = 0; index < this._missiles.Count; ++index)
      this._missiles[index].PlayAnimation(name, time);
  }

  private void UpdateMissiles()
  {
    for (int index = 0; index < this._missiles.Count; ++index)
      this._missiles[index].UpdateAnimation();
  }

  private void ClearMissiles()
  {
    for (int index = 0; index < this._missiles.Count; ++index)
      this._system.DestoryMissile(this._missiles[index]);
    this._missiles.Clear();
  }

  public void PlayAnimation()
  {
    while (this._waitList.Count > 0 && this._missiles.Count < this.MaxShoot)
    {
      Missile missile = this._waitList.First.Value;
      this._waitList.RemoveFirst();
      missile.OnStartShoot();
      this._missiles.Add(missile);
    }
    this._isPlaying = true;
    this._animation.enabled = true;
    this._animation.Play(this._shootStateName, -1, 0.0f);
    this._animation.speed = this._system.GenerateRandomAnimationSpeed();
    this._animation.Update(0.1f);
  }

  public void UpdateAnimation()
  {
    if (this._isPlaying)
    {
      this.UpdateMissiles();
      if ((double) this._animation.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0)
        return;
      this.OnAnimationEnd();
    }
    else
    {
      if (this._waitList.Count <= 0 || (double) this._stopTimestamp > 0.0 && (double) Time.time - (double) this._stopTimestamp <= (double) this._shootInterval)
        return;
      this.PlayAnimation();
    }
  }

  public void OnAnimationEnd()
  {
    this._isPlaying = false;
    this.ClearMissiles();
    this._animation.enabled = false;
    this._animation.speed = 1f;
    this._stopTimestamp = Time.time;
  }

  private void OnAnimationEvent(AnimationEvent e)
  {
    if (e == null && this._isPlaying)
      return;
    this.PlayMissleAnimation(e.stringParameter, e.floatParameter);
  }
}
