﻿// Decompiled with JetBrains decompiler
// Type: JackpotPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JackpotPayload
{
  private float delayTimeFactor = 20f;
  private float rollingDownUpdateTime = 4.5f;
  private float rollingDownUpdateRate = 0.1f;
  private Hashtable cacheJackpotData = new Hashtable();
  private float intervalRequestTime = 600f;
  private List<JackpotRankInfo> jackpotRankInfoList = new List<JackpotRankInfo>();
  private List<JackpotRankInfo> lastJackpotRankInfoList = new List<JackpotRankInfo>();
  public System.Action<Hashtable> onJackpotDataUpdated;
  private Coroutine coroutine;
  private Coroutine jackpotCounterCoroutine;
  private long jackpotCounter;
  private int updateTime;
  private int intervalUpdateTime;
  private long updateJackpot;
  private long intervalUpdateJackpot;
  private long currentJackpot;
  private long baseJackpot;
  private long rewardJackpot;
  private static JackpotPayload instance;

  public float IntervalRequestTime
  {
    get
    {
      return this.intervalRequestTime;
    }
  }

  public int IntervalUpdateTime
  {
    get
    {
      return this.intervalUpdateTime;
    }
  }

  public long IntervalUpdateJackpot
  {
    get
    {
      return this.intervalUpdateJackpot;
    }
  }

  public long CurrentJackpot
  {
    get
    {
      return this.currentJackpot;
    }
    set
    {
      this.currentJackpot = value;
    }
  }

  public long BaseJackpot
  {
    get
    {
      return this.baseJackpot;
    }
    set
    {
      this.baseJackpot = value;
    }
  }

  public long RewardJackpot
  {
    get
    {
      return this.rewardJackpot;
    }
    set
    {
      this.rewardJackpot = value;
    }
  }

  public List<JackpotRankInfo> JackpotRankInfoList
  {
    get
    {
      this.jackpotRankInfoList.Sort((Comparison<JackpotRankInfo>) ((a, b) => b.winTime.CompareTo(a.winTime)));
      return this.jackpotRankInfoList;
    }
  }

  public List<JackpotRankInfo> LastJackpotRankInfoList
  {
    get
    {
      return this.lastJackpotRankInfoList;
    }
    set
    {
      this.lastJackpotRankInfoList = value;
    }
  }

  public bool NeedRefreshRankContent
  {
    get
    {
      if (this.HasJackpotRankInfo && this.lastJackpotRankInfoList != null)
      {
        for (int i = 0; i < this.jackpotRankInfoList.Count; ++i)
        {
          if (this.lastJackpotRankInfoList.Find((Predicate<JackpotRankInfo>) (x =>
          {
            if (x.winnerUid == this.jackpotRankInfoList[i].winnerUid)
              return x.winTime == this.jackpotRankInfoList[i].winTime;
            return false;
          })) == null)
            return true;
        }
      }
      return false;
    }
  }

  public bool HasJackpotRankInfo
  {
    get
    {
      return this.jackpotRankInfoList != null && this.jackpotRankInfoList.Count > 0;
    }
  }

  public ItemStaticInfo BaseRewardInfo
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("jackpot_base_reward_id");
      if (data != null)
        return ConfigManager.inst.DB_Items.GetItem(data.ValueString);
      return (ItemStaticInfo) null;
    }
  }

  public int BaseRewardCount
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("jackpot_base_reward_value");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public static JackpotPayload Instance
  {
    get
    {
      if (JackpotPayload.instance == null)
        JackpotPayload.instance = new JackpotPayload();
      return JackpotPayload.instance;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
      return;
    this.cacheJackpotData = data;
    if (data[(object) "base_gold"] != null)
      DatabaseTools.UpdateData(data, "base_gold", ref this.baseJackpot);
    if (data[(object) "jackpot_gold"] != null)
      DatabaseTools.UpdateData(data, "jackpot_gold", ref this.currentJackpot);
    if (data[(object) "reward_gold"] != null)
      DatabaseTools.UpdateData(data, "reward_gold", ref this.rewardJackpot);
    else
      this.rewardJackpot = 0L;
    int outData1 = -1;
    if (data[(object) "type"] != null)
    {
      DatabaseTools.UpdateData(data, "type", ref outData1);
      if (outData1 == 3)
        this.currentJackpot = this.rewardJackpot;
    }
    this.lastJackpotRankInfoList = new List<JackpotRankInfo>((IEnumerable<JackpotRankInfo>) this.jackpotRankInfoList.ToArray());
    this.jackpotRankInfoList.Clear();
    ArrayList data1 = new ArrayList();
    if (data[(object) "top1_list"] != null && outData1 != 3)
    {
      DatabaseTools.CheckAndParseOrgData(data[(object) "top1_list"], out data1);
      if (data1 != null && data1.Count > 0)
      {
        for (int index = 0; index < data1.Count; ++index)
        {
          Hashtable inData = data1[index] as Hashtable;
          if (inData != null)
          {
            string empty1 = string.Empty;
            string empty2 = string.Empty;
            string outData2 = "0";
            string empty3 = string.Empty;
            int outData3 = 0;
            int outData4 = 0;
            int outData5 = 0;
            int outData6 = 0;
            long outData7 = 0;
            DatabaseTools.UpdateData(inData, "name", ref empty1);
            DatabaseTools.UpdateData(inData, "acronym", ref empty2);
            DatabaseTools.UpdateData(inData, "portrait", ref outData2);
            DatabaseTools.UpdateData(inData, "icon", ref empty3);
            DatabaseTools.UpdateData(inData, "lord_title", ref outData3);
            DatabaseTools.UpdateData(inData, "uid", ref outData4);
            DatabaseTools.UpdateData(inData, "world_id", ref outData6);
            DatabaseTools.UpdateData(inData, "gold", ref outData7);
            DatabaseTools.UpdateData(inData, "jackpot_time", ref outData5);
            this.jackpotRankInfoList.Add(new JackpotRankInfo()
            {
              winner = empty1,
              winnerAllianceAcronym = empty2,
              winnerPortrait = outData2,
              winnerLordTitleId = outData3,
              winnerImage = empty3,
              winnerUid = outData4,
              winnerKingdom = outData6,
              winGoldAmount = outData7,
              winTime = outData5
            });
          }
        }
      }
    }
    this.intervalUpdateTime = this.updateTime != 0 ? NetServerTime.inst.ServerTimestamp - this.updateTime : (int) this.intervalRequestTime;
    this.intervalUpdateJackpot = this.updateJackpot != 0L ? this.currentJackpot - this.updateJackpot : this.updateJackpot;
    if (this.intervalUpdateJackpot < 0L)
      this.intervalUpdateJackpot = 0L;
    this.updateTime = NetServerTime.inst.ServerTimestamp;
    this.updateJackpot = this.currentJackpot;
    if (this.onJackpotDataUpdated == null)
      return;
    this.onJackpotDataUpdated(data);
  }

  public void Initialize()
  {
    this.RequestServerData((System.Action<bool, object>) null);
    this.StopCoroutine(this.coroutine);
  }

  public void Dispose()
  {
    this.StopCoroutine(this.coroutine);
    this.StopCoroutine(this.jackpotCounterCoroutine);
  }

  public void RequestServerData(System.Action<bool, object> callback)
  {
    if (callback != null)
      MessageHub.inst.GetPortByAction("jackpot:getJackpotInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
          this.Decode(data as Hashtable);
        if (callback == null)
          return;
        callback(ret, data);
      }), true);
    else
      MessageHub.inst.GetPortByAction("jackpot:getJackpotInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.Decode(data as Hashtable);
      }), false, false);
  }

  public void ShowCurrentJackpot(UILabel jackpotText, bool slam = true, float rollupTime = 0.0f)
  {
    float num = (float) (this.currentJackpot - this.jackpotCounter);
    if (this.intervalUpdateTime <= 0 || (double) num <= 0.0 || slam)
    {
      this.jackpotCounter = this.currentJackpot;
      jackpotText.text = this.currentJackpot.ToString();
    }
    else
    {
      if (this.intervalUpdateTime <= 0)
        return;
      float timeInterval = (float) this.intervalUpdateTime / this.delayTimeFactor / num;
      int deltaJackpot = 1;
      if ((double) rollupTime > 0.0)
      {
        timeInterval = 0.1f;
        deltaJackpot = (int) Mathf.Floor(num / (rollupTime / timeInterval));
      }
      jackpotText.text = this.jackpotCounter.ToString();
      this.StopCoroutine(this.jackpotCounterCoroutine);
      this.jackpotCounterCoroutine = Utils.ExecuteInIntervalTime(timeInterval, (System.Action) (() =>
      {
        this.jackpotCounter += (long) deltaJackpot;
        if (this.jackpotCounter > this.currentJackpot)
          this.jackpotCounter = this.currentJackpot;
        if (!((UnityEngine.Object) jackpotText != (UnityEngine.Object) null))
          return;
        jackpotText.text = this.jackpotCounter.ToString();
      }));
    }
  }

  public void ShowJackpotRollDown(UILabel jackpotText, System.Action onFinished)
  {
    if (this.rewardJackpot <= 0L)
    {
      if (onFinished == null)
        return;
      onFinished();
    }
    else
    {
      this.jackpotCounter = this.rewardJackpot;
      jackpotText.text = this.jackpotCounter.ToString();
      int deltaJackpot = (int) ((double) this.rewardJackpot / ((double) this.rollingDownUpdateTime / (double) this.rollingDownUpdateRate));
      this.StopCoroutine(this.jackpotCounterCoroutine);
      this.jackpotCounterCoroutine = Utils.ExecuteInIntervalTime(this.rollingDownUpdateRate, (System.Action) (() =>
      {
        this.jackpotCounter -= (long) deltaJackpot;
        if (this.jackpotCounter <= 0L)
        {
          this.jackpotCounter = 0L;
          if (onFinished == null)
            return;
          onFinished();
        }
        else
        {
          if (!((UnityEngine.Object) jackpotText != (UnityEngine.Object) null))
            return;
          jackpotText.text = this.jackpotCounter.ToString();
        }
      }));
    }
  }

  public void UpdateDataByCache()
  {
    int outData = -1;
    if (this.cacheJackpotData == null)
      return;
    DatabaseTools.UpdateData(this.cacheJackpotData, "type", ref outData);
    if (outData != 3)
      return;
    this.cacheJackpotData[(object) "type"] = (object) -1;
    this.Decode(this.cacheJackpotData);
  }

  public void StopUpdateJackpotMeter()
  {
    this.StopCoroutine(this.jackpotCounterCoroutine);
  }

  private void StopCoroutine(Coroutine cor)
  {
    if (cor == null)
      return;
    Utils.StopCoroutine(cor);
    cor = (Coroutine) null;
  }
}
