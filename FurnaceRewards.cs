﻿// Decompiled with JetBrains decompiler
// Type: FurnaceRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnaceRewards : MonoBehaviour
{
  private Dictionary<int, FurnaceReward> totals = new Dictionary<int, FurnaceReward>();
  private Dictionary<int, int> item2count = new Dictionary<int, int>();
  public UIGrid grid;
  public UIScrollView scroll;
  public FurnaceReward itemPrefab;
  public System.Action OnSelterDelegate;
  public System.Action<int> OnItemUnSelected;
  public UIButton selterBt;
  public GameObject effectA;
  public GameObject effectB;
  private bool _isDestroy;

  private void Start()
  {
    this.CheckButton();
  }

  public void Dispose()
  {
    Dictionary<int, FurnaceReward>.ValueCollection.Enumerator enumerator = this.totals.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((UnityEngine.Object) enumerator.Current != (UnityEngine.Object) null)
        enumerator.Current.Dispose();
    }
    this.totals.Clear();
  }

  public void OnSelter()
  {
    Utils.ShowWarning(ScriptLocalization.Get("exchange_magic_store_cauldron_process_confirm_description", true), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.StartPlayEffectB), ScriptLocalization.Get("exchange_magic_store_cauldron_process_confirm_title", true));
  }

  public void CheckButton()
  {
    bool flag = false;
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = this.item2count.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this.item2count[enumerator.Current] > 0)
        flag = true;
    }
    this.selterBt.isEnabled = flag;
  }

  private void OnCommmit()
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    Hashtable hashtable = new Hashtable();
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = this.item2count.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (this.item2count[enumerator.Current] > 0)
        hashtable.Add((object) enumerator.Current, (object) this.item2count[enumerator.Current]);
    }
    if (hashtable.Count <= 0)
      return;
    postData.Add((object) "goods", (object) hashtable);
    RequestManager.inst.SendRequest("magicStore:smeltingGoods", postData, new System.Action<bool, object>(this.OnCallBack), true);
  }

  private void OnDestroy()
  {
    this._isDestroy = true;
  }

  private void StartPlayEffectB()
  {
    if (this._isDestroy)
      return;
    this.selterBt.isEnabled = false;
    NGUITools.SetActive(this.effectA, true);
    Hashtable args = new Hashtable();
    args.Add((object) "x", (object) -1.549f);
    args.Add((object) "z", (object) 0.437f);
    args.Add((object) "time", (object) 0.5f);
    args.Add((object) "easetype", (object) "Linear");
    args.Add((object) "isLocal", (object) true);
    args.Add((object) "delay", (object) 0.2f);
    args.Add((object) "oncompletetarget", (object) this.gameObject);
    args.Add((object) "oncomplete", (object) "OnCompleteHandler");
    NGUITools.SetActive(this.effectB, true);
    iTween.MoveTo(this.effectB, args);
  }

  public void OnCompleteHandler()
  {
    if (this._isDestroy)
      return;
    this.effectB.transform.localPosition = Vector3.zero;
    NGUITools.SetActive(this.effectB, false);
    NGUITools.SetActive(this.effectA, false);
    this.OnCommmit();
  }

  private void OnCallBack(bool success, object result)
  {
    if (!success)
      return;
    if (this.OnSelterDelegate != null)
      this.OnSelterDelegate();
    RewardsCollectionAnimator.Instance.Clear();
    Dictionary<int, FurnaceReward>.ValueCollection.Enumerator enumerator = this.totals.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.rewardCount > 0)
      {
        ItemRewardInfo.Data data = new ItemRewardInfo.Data();
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(enumerator.Current.rewardItemId);
        data.count = (float) enumerator.Current.rewardCount;
        data.icon = itemStaticInfo.ImagePath;
        RewardsCollectionAnimator.Instance.items.Add(data);
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(true);
    AudioManager.Instance.PlaySound("sfx_alliance_altar_cast_spell", false);
    this.Clear();
  }

  private void Clear()
  {
    this.item2count.Clear();
    Dictionary<int, FurnaceReward>.ValueCollection.Enumerator enumerator = this.totals.Values.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Refresh(0);
  }

  public void Add(int rewardItemId, int itemId, int count)
  {
    FurnaceReward furnaceReward = this.GetItem(rewardItemId);
    this.item2count[itemId] = count;
    int count1 = 0;
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = this.item2count.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (ConfigManager.inst.DB_MagicSelter.GetDataByDisbandItemId(enumerator.Current).RewardItemId == rewardItemId)
        count1 += ConfigManager.inst.DB_MagicSelter.GetRewardCount(enumerator.Current, this.item2count[enumerator.Current]);
    }
    furnaceReward.SetData(rewardItemId, count1);
    furnaceReward.OnItemUnSelected = this.OnItemUnSelected;
    this.grid.Reposition();
    this.scroll.ResetPosition();
    this.CheckButton();
  }

  private FurnaceReward GetItem(int itemId)
  {
    FurnaceReward furnaceReward;
    if (!this.totals.ContainsKey(itemId))
    {
      furnaceReward = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject).GetComponent<FurnaceReward>();
      this.totals.Add(itemId, furnaceReward);
    }
    else
      furnaceReward = this.totals[itemId];
    return furnaceReward;
  }
}
