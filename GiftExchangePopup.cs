﻿// Decompiled with JetBrains decompiler
// Type: GiftExchangePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;

public class GiftExchangePopup : Popup
{
  private const string CODE_EXCHANGE_COOLDOWN_TIME = "code_exchange_cooldown_time";
  private const string CODE_EXCHANGE_ERROR_TIMES = "code_exchange_error_times";
  private const int CODE_EXCHANGE_ERROR_LIMIT_TIMES = 5;
  private const int CODE_EXCHANGE_LIMIT_TIME = 7200;
  public UIInput inputExchangeCode;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnExchangeBtnPressed()
  {
    string str = this.inputExchangeCode.value;
    if (!this.CanCodeExchange())
      UIManager.inst.toast.Show(Utils.XLAT("toast_gift_codes_too_many_attempts"), (System.Action) null, 4f, true);
    else
      MessageHub.inst.GetPortByAction("player:exchangeCode").SendRequest(new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "code",
          (object) str
        },
        {
          (object) "package_type",
          (object) this.GetPackageType()
        }
      }, (System.Action<bool, object>) ((ret, data) =>
      {
        string empty = string.Empty;
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null)
          empty = hashtable[(object) "errno"] as string;
        if (ret && string.IsNullOrEmpty(empty))
        {
          UIManager.inst.toast.Show(Utils.XLAT("toast_gift_codes_success"), (System.Action) null, 4f, false);
          this.ResetErrorStatus();
          this.OnCloseBtnPressed();
        }
        else
          this.HandleExchangeError(empty);
      }), true);
  }

  private string GetPackageType()
  {
    return PaymentManager.Instance.GetPackageType();
  }

  private void HandleExchangeError(string errno)
  {
    int num1 = PlayerPrefsEx.GetInt("code_exchange_error_times", 0);
    if (string.IsNullOrEmpty(errno))
      return;
    int result = 0;
    int.TryParse(errno, out result);
    switch (result)
    {
      case 1100030:
      case 1100040:
      case 1100050:
      case 1100060:
        int num2;
        PlayerPrefsEx.SetInt("code_exchange_error_times", num2 = num1 + 1);
        if (num2 < 5)
          break;
        PlayerPrefsEx.SetInt("code_exchange_cooldown_time", NetServerTime.inst.ServerTimestamp);
        break;
    }
  }

  private bool CanCodeExchange()
  {
    int num = NetServerTime.inst.ServerTimestamp - PlayerPrefsEx.GetInt("code_exchange_cooldown_time", NetServerTime.inst.ServerTimestamp);
    if (PlayerPrefsEx.GetInt("code_exchange_error_times", 0) >= 5 && num <= 7200)
      return false;
    if (num >= 7200)
      this.ResetErrorStatus();
    return true;
  }

  private void ResetErrorStatus()
  {
    PlayerPrefsEx.SetInt("code_exchange_error_times", 0);
    PlayerPrefsEx.DeleteKey("code_exchange_cooldown_time");
  }
}
