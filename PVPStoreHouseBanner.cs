﻿// Decompiled with JetBrains decompiler
// Type: PVPStoreHouseBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPStoreHouseBanner : SpriteBanner
{
  public TextMesh m_Status;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    AllianceWareHouseData wareHouseData = tile.WareHouseData;
    string empty = string.Empty;
    string str = string.Empty;
    if (allianceData != null)
      empty += string.Format("({0})", (object) allianceData.allianceAcronym);
    if (wareHouseData != null)
    {
      empty += string.IsNullOrEmpty(wareHouseData.Name) ? Utils.XLAT("alliance_storehouse_title") : wareHouseData.Name;
      str = string.Format("({0})", (object) PVPStoreHouseBanner.GetStateString(wareHouseData.CurrentState));
    }
    this.Name = empty;
    this.m_Status.text = str;
  }

  public static string GetStateString(AllianceWareHouseData.State state)
  {
    switch (state)
    {
      case AllianceWareHouseData.State.UnComplete:
        return Utils.XLAT("alliance_fort_status_unfinished");
      case AllianceWareHouseData.State.Building:
        return Utils.XLAT("alliance_fort_status_under_construction");
      case AllianceWareHouseData.State.Complete:
        return Utils.XLAT("alliance_storehouse_status_storing");
      case AllianceWareHouseData.State.Freeze:
        return Utils.XLAT("alliance_storehouse_status_frozen");
      default:
        return string.Empty;
    }
  }
}
