﻿// Decompiled with JetBrains decompiler
// Type: AllianceActivityChestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceActivityChestInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "alliance_level")]
  public int AllianceLevel;
  [Config(Name = "chest_id")]
  public int ChestId;
  [Config(Name = "req_score_1")]
  public int ReqScore1;
  [Config(Name = "chest_value_1")]
  public int ChestValue1;
  [Config(Name = "req_score_2")]
  public int ReqScore2;
  [Config(Name = "chest_value_2")]
  public int ChestValue2;
  [Config(Name = "req_score_3")]
  public int ReqScore3;
  [Config(Name = "chest_value_3")]
  public int ChestValue3;
  [Config(Name = "req_score_4")]
  public int ReqScore4;
  [Config(Name = "chest_value_4")]
  public int ChestValue4;
  [Config(Name = "req_score_5")]
  public int ReqScore5;
  [Config(Name = "chest_value_5")]
  public int ChestValue5;
  [Config(Name = "req_score_6")]
  public int ReqScore6;
  [Config(Name = "chest_value_6")]
  public int ChestValue6;
  private int[] _allRequireScore;
  private int[] _allChestValue;

  public int[] AllRequireScore
  {
    get
    {
      if (this._allRequireScore == null)
        this._allRequireScore = new int[6]
        {
          this.ReqScore1,
          this.ReqScore2,
          this.ReqScore3,
          this.ReqScore4,
          this.ReqScore5,
          this.ReqScore6
        };
      return this._allRequireScore;
    }
  }

  public int[] AllChestValue
  {
    get
    {
      if (this._allChestValue == null)
        this._allChestValue = new int[6]
        {
          this.ChestValue1,
          this.ChestValue2,
          this.ChestValue3,
          this.ChestValue4,
          this.ChestValue5,
          this.ChestValue6
        };
      return this._allChestValue;
    }
  }

  public int MaxRequireScore
  {
    get
    {
      return this.AllRequireScore[this.AllRequireScore.Length - 1];
    }
  }

  public int GetAllChestCanGet(int score)
  {
    int num = 0;
    for (int index = 0; index < this.AllRequireScore.Length && score >= this.AllRequireScore[index]; ++index)
      num = this.AllChestValue[index];
    return num;
  }
}
