﻿// Decompiled with JetBrains decompiler
// Type: GuardStateSpellMoral
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardStateSpellMoral : AbstractGuardState
{
  public GuardStateSpellMoral(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_spell";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    DragonKnightSystem.Instance.Controller.BattleAudio.Play("Prefab/DragonKnight/Objects/SFX/dragon_knight_activate_skill", false);
    DragonKnightSystem.Instance.Controller.BattleHud.TriggerCoolDownTimer(this.Player.SkillId);
    DragonKnightSystem.Instance.Controller.BattleHud.AddKillStackItem(this.Player.SkillId, (System.Action) (() =>
    {
      if (!DragonKnightSystem.Instance.IsReady || !DragonKnightSystem.Instance.RoomManager.CurrentRoom.ContainsMonsters() && !DragonKnightSystem.Instance.RoomManager.CurrentRoom.ContainsPlayer())
        return;
      RoundPlayer player = BattleManager.Instance.GetPlayer(BattleManager.Instance.TargetId);
      if (player == null || !(bool) ((UnityEngine.Object) player.Animation) || !(bool) ((UnityEngine.Object) player.Animation.gameObject))
        return;
      DragonKnightTalentInfo knightTalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(this.Player.SkillId);
      if (knightTalentInfo != null)
      {
        TextAsset script = AssetManager.Instance.HandyLoad("Prefab/DragonKnight/Objects/Skills/" + knightTalentInfo.ID, (System.Type) null) as TextAsset;
        if ((bool) ((UnityEngine.Object) script))
        {
          Dictionary<string, GuardAction>.Enumerator enumerator = (!knightTalentInfo.IsMyself() ? GuardEventDataParser.ParseActions(player.Animation.gameObject, script) : GuardEventDataParser.ParseActions(DragonKnightSystem.Instance.Controller.BattleScene.gameObject, script)).GetEnumerator();
          while (enumerator.MoveNext())
            enumerator.Current.Value.Process(this.Player);
        }
      }
      BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketSpellHit(this.Player.PlayerId));
      this.Player.StateMacine.SetState("guard_idle", (Hashtable) null);
    }));
  }

  public override void OnProcess()
  {
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
  }
}
