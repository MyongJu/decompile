﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_RallyList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class AllianceWar_MainDlg_RallyList : MonoBehaviour
{
  public static AllianceWar_MainDlg_RallyList.Params param = new AllianceWar_MainDlg_RallyList.Params();
  public const int RALLY_LIST_ATTACKER_PORTRAIT_INDEX = -1;
  public const int RALLY_LIST_DEFENDER_PORTRAIT_INDEX = -2;
  [SerializeField]
  private AllianceWar_MainDlg_RallyList.Panel panel;

  public void Setup(AllianceWar_MainDlg_RallyList.Params param)
  {
    this.panel.targetName.text = param.ownerName;
    this.panel.totalCount.text = param.memberCount.ToString();
    if (param.memberCount > 0 && param.rallyType != AllianceWar_MainDlg_RallyList.Params.RallyListType.BOSS)
      this.BuildPortrait((UIWidget) this.panel.portraits[0], param.ownerPortrait);
    if (param.rallyType == AllianceWar_MainDlg_RallyList.Params.RallyListType.BOSS)
    {
      this.panel.portraits[0].gameObject.SetActive(true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.portraits[0], param.bossIcon, (System.Action<bool>) null, true, false, string.Empty);
    }
    else if (param.rallyType != AllianceWar_MainDlg_RallyList.Params.RallyListType.FALLEN_KNIGHT)
    {
      for (int index = 1; index < param.memberCount && index < this.panel.portraits.Length; ++index)
      {
        this.panel.portraits[index].gameObject.SetActive(true);
        this.BuildPortrait((UIWidget) this.panel.portraits[index], !param.isAttack ? -2 : -1);
      }
    }
    else
    {
      for (int index = 1; index < param.memberCount && index < this.panel.portraits.Length; ++index)
      {
        this.panel.portraits[index].gameObject.SetActive(true);
        this.BuildPortrait((UIWidget) this.panel.portraits[index], param.ownerPortrait);
      }
    }
    if (param.memberCount < 6)
    {
      if (param.canJoin)
      {
        for (int memberCount = param.memberCount; memberCount < this.panel.portraits.Length; ++memberCount)
        {
          this.panel.portraits[memberCount].gameObject.SetActive(true);
          this.panel.portraits[memberCount].mainTexture = (Texture) null;
        }
      }
      else
      {
        for (int memberCount = param.memberCount; memberCount < this.panel.portraits.Length; ++memberCount)
          this.panel.portraits[memberCount].gameObject.SetActive(false);
      }
      this.panel.colon.gameObject.SetActive(false);
    }
    else if (param.memberCount == 6)
    {
      this.panel.portraits[5].gameObject.SetActive(true);
      if (param.canJoin)
      {
        this.panel.portraits[5].mainTexture = (Texture) null;
        this.panel.colon.gameObject.SetActive(true);
      }
      else
        this.panel.colon.gameObject.SetActive(false);
    }
    else
      this.panel.colon.gameObject.SetActive(true);
  }

  private void BuildPortrait(UIWidget widget, int iconIndex)
  {
    if (iconIndex == -2)
      BuilderFactory.Instance.HandyBuild(widget, "Texture/Hero/Portrait_Icon/player_portrait_icon_defender", (System.Action<bool>) null, true, false, string.Empty);
    else if (iconIndex == -1)
    {
      BuilderFactory.Instance.HandyBuild(widget, "Texture/Hero/Portrait_Icon/player_portrait_icon_attacker", (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      CustomIconLoader.Instance.requestCustomIcon(widget as UITexture, string.Format("Texture/Hero/Portrait_Icon/player_portrait_icon_{0}", (object) iconIndex), AllianceWar_MainDlg_RallyList.param.ownerCustomIconUrl, false);
      LordTitlePayload.Instance.ApplyUserAvator(widget as UITexture, AllianceWar_MainDlg_RallyList.param.ownerLordTitleId, 2);
    }
  }

  public void OnDisable()
  {
    for (int index = 0; index < this.panel.portraits.Length; ++index)
      BuilderFactory.Instance.Release((UIWidget) this.panel.portraits[index]);
  }

  public void Reset()
  {
    this.panel.targetName = this.transform.Find("Name").gameObject.GetComponent<UILabel>();
    this.panel.totalCount = this.transform.Find("MemberIcon/MemberCount").gameObject.GetComponent<UILabel>();
    this.panel.colon = this.transform.Find("MoreMember").gameObject.GetComponent<Transform>();
    this.panel.portraits = new UITexture[6];
    for (int index = 0; index < 6; ++index)
    {
      GameObject gameObject = this.transform.Find("Portrait_" + (object) index).gameObject;
      this.panel.portraits[index] = gameObject.GetComponent<UITexture>();
      this.panel.portraits[index].mainTexture = (Texture) null;
    }
  }

  public class Params
  {
    public string bossIcon = string.Empty;
    public AllianceWar_MainDlg_RallyList.Params.RallyListType rallyType;
    public bool isAttack;
    public bool canJoin;
    public string ownerName;
    public int ownerPortrait;
    public string ownerCustomIconUrl;
    public int ownerLordTitleId;
    public int memberCount;

    public enum RallyListType
    {
      MEMBER,
      BOSS,
      FORTRESS,
      FALLEN_KNIGHT,
    }
  }

  [Serializable]
  protected class Panel
  {
    public const int PORTRAIT_COUNT = 6;
    public UILabel targetName;
    public UILabel totalCount;
    public Transform colon;
    public UITexture[] portraits;
  }
}
