﻿// Decompiled with JetBrains decompiler
// Type: TutorialDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using UI;
using UnityEngine;

public class TutorialDialog : MonoBehaviour, ITutorial
{
  public int scalesize = 30;
  public float scaletime = 0.5f;
  private Vector3 cacheHeadPos = Vector3.zero;
  private Vector3 cacheBottomPos = Vector3.zero;
  public float npcmovetime = 1f;
  private const string path = "Prefab/UI/Dialogs/Tutorial/";
  private const string targetPath = "CityMap/MapRoot/buildings/activity";
  private const string earlyJoinAlliancePath = "UIManager/UI Root/UI2DCamera/PublicHUD(Clone)/PART_COMMON/BottomButtonBar/Grid/AllianceBtn";
  private const string npcImagePath = "Texture/STATIC_TEXTURE/tutorial_npc";
  public UIWidget clickTriger;
  public GameObject arrow;
  public UI2DSprite mask;
  public GameObject npcGroup;
  public GameObject clickGroup;
  public UITexture npcImage;
  private TutorialDialog.Mode curMode;
  private Transform _curTarget;
  private string _curTargetName;
  private int timer;
  private TutorialData _data;

  public Transform CurTarget
  {
    get
    {
      return this._curTarget;
    }
    set
    {
      this._curTarget = value;
      this._curTargetName = !((UnityEngine.Object) null == (UnityEngine.Object) this._curTarget) ? this._curTarget.name : (string) null;
    }
  }

  private void Awake()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnUpdate);
    this.CurTarget = this.transform;
  }

  private void OnDestroy()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnUpdate);
    string[] strArray = this.Data.name.Split('.');
    if (strArray.Length != 2)
      return;
    if (strArray[0] == "Tutorial_activity_center")
      this.ShowDragonSoulHint();
    if (strArray[0] == "Tutorial_merlin_tower")
      this.ShowMerlinTowerHint(this.Data.parent);
    if (strArray[0] == "testb_early_tutorial_join_alliance")
      this.ShowJoinAllianceGuide();
    if (!(strArray[0] == "Tutorial_dragon_skill") || TutorialManager.Instance.EarlyGoalType != TutorialManager.ABTestType.Second)
      return;
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightGuidePopup", (Popup.PopupParameter) null);
  }

  private void Init(float time)
  {
    this.transform.FindChild("bg").GetComponent<BoxCollider>().enabled = false;
    this.curMode = (TutorialDialog.Mode) this.Data.mode;
    if (this.curMode == TutorialDialog.Mode.NPC)
    {
      this.npcGroup.SetActive(true);
      this.clickGroup.SetActive(false);
      if (!string.IsNullOrEmpty(this.Data.npcImage))
        BuilderFactory.Instance.HandyBuild((UIWidget) this.npcImage, "Texture/STATIC_TEXTURE/tutorial_npc_" + this.Data.npcImage, (System.Action<bool>) null, true, false, string.Empty);
      else
        BuilderFactory.Instance.HandyBuild((UIWidget) this.npcImage, "Texture/STATIC_TEXTURE/tutorial_npc", (System.Action<bool>) null, true, false, string.Empty);
      this.npcGroup.GetComponentInChildren<INPCMode>().SeedData((object) this.Data.npcText);
      this.NPCGroupFadeout(1f);
    }
    else
    {
      if (this.curMode != TutorialDialog.Mode.CLICK)
        return;
      this.npcGroup.SetActive(false);
      this.clickGroup.SetActive(true);
      this.arrow.transform.localPosition = this.Data.arrowPos;
      this.arrow.transform.localRotation = this.Data.arrowRot;
      this.arrow.transform.localScale = this.Data.arrowScale;
      this.mask.width = (int) this.Data.maskSize.x;
      this.mask.height = (int) this.Data.maskSize.y;
      iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0, (object) "to", (object) this.scalesize, (object) nameof (time), (object) this.scaletime, (object) "easetype", (object) iTween.EaseType.linear, (object) "looptype", (object) iTween.LoopType.pingPong, (object) "onupdate", (object) "ScaleAnimation"));
      this.clickGroup.transform.OverlayPosition(this.CurTarget);
      Vector3 localPosition = this.clickGroup.transform.localPosition;
      localPosition.z = -1f;
      this.clickGroup.transform.localPosition = localPosition;
      this.clickTriger.transform.localPosition = this.Data.ctPos;
      this.clickTriger.width = (int) this.Data.ctSize.x;
      this.clickTriger.height = (int) this.Data.ctSize.y;
      this.clickTriger.ResizeCollider();
    }
  }

  private void ShowDragonSoulHint()
  {
    GameObject gameObject = GameObject.Find("CityMap/MapRoot/buildings/activity");
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    LinkerHub.Instance.MountHint(gameObject.transform, 0, (object) null);
    LinkerHub.Instance.StartTipCount();
  }

  private void ShowMerlinTowerHint(string parent)
  {
    GameObject gameObject = GameObject.Find(parent);
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    LinkerHub.Instance.MountHint(gameObject.transform, 0, (object) null);
  }

  private void ShowJoinAllianceGuide()
  {
    GameObject gameObject = GameObject.Find("UIManager/UI Root/UI2DCamera/PublicHUD(Clone)/PART_COMMON/BottomButtonBar/Grid/AllianceBtn");
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    LinkerHub.Instance.MountHint(gameObject.transform, 3, (object) null);
    LinkerHub.Instance.StartTipCount();
  }

  private void OnUpdate(int delta)
  {
    if (this._curTargetName != null)
      return;
    GameObject gameObject = GameObject.Find(this.Data.parent);
    bool flag = false;
    if ((UnityEngine.Object) null != (UnityEngine.Object) gameObject)
      flag = !this.Data.parent.Contains("PublicHUD") ? gameObject.activeInHierarchy : UIManager.inst.publicHUD.IsShow;
    if (flag)
    {
      this.timer = 0;
      this.CurTarget = gameObject.transform;
      this.BeginAnimations();
    }
    else
    {
      ++this.timer;
      if (this.timer <= 7)
        return;
      this.ForceSkipProcess();
      TutorialManager.Instance.Terminate();
    }
  }

  private void Update()
  {
    if (this._curTargetName == null || this.curMode != TutorialDialog.Mode.CLICK)
      return;
    this.clickGroup.transform.OverlayPosition(this.CurTarget);
    Vector3 localPosition = this.clickGroup.transform.localPosition;
    localPosition.z = -1f;
    this.clickGroup.transform.localPosition = localPosition;
  }

  private void ScaleAnimation(float value)
  {
    this.mask.width = (int) ((double) this.Data.maskSize.x + (double) value);
    this.mask.height = (int) ((double) this.Data.maskSize.y + (double) value);
  }

  private void UglyHardCode()
  {
    if ("Tutorial_kingdom.3" == this.Data.name)
      this.StartCoroutine(this.BeginFindFarm());
    if (!("testb_Tutorial_monster.3" == this.Data.name))
      return;
    this.StartCoroutine(this.BeginFindMonster());
  }

  [DebuggerHidden]
  private IEnumerator BeginFindFarm()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialDialog.\u003CBeginFindFarm\u003Ec__IteratorA3()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator BeginFindMonster()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TutorialDialog.\u003CBeginFindMonster\u003Ec__IteratorA4()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void RemoveHardCodeDelegate()
  {
  }

  public void OnTrigerClicked()
  {
    this.SendRay(UICamera.currentTouch.pos);
  }

  private void SendRay(Vector2 pos)
  {
    bool flag1 = false;
    TutorialManager.Instance.ActiveTutorialLayer(false);
    if (UICamera.Raycast((Vector3) pos))
    {
      UICamera.hoveredObject.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
      UIEventTrigger component1 = UICamera.hoveredObject.GetComponent<UIEventTrigger>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component1 && component1.onClick != null)
        EventDelegate.Execute(component1.onClick);
      BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
      bool flag2 = false;
      foreach (object component2 in UICamera.hoveredObject.GetComponents<MonoBehaviour>())
      {
        foreach (MemberInfo method in component2.GetType().GetMethods(bindingAttr))
        {
          if ("OnClick" == method.Name)
            flag2 = true;
        }
      }
      flag1 = flag2 || (UnityEngine.Object) null != (UnityEngine.Object) component1 && component1.onClick != null || this.ForceSkip();
    }
    TutorialManager.Instance.ActiveTutorialLayer(true);
    if (!flag1)
      return;
    this.CloseDialogAnim();
  }

  private bool ForceSkip()
  {
    if (!("Tutorial_kingdom.5" == this.Data.name))
      return false;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    return true;
  }

  public void OnNPCGroupClicked()
  {
    if ("Tutorial_kingdom.2" == this.Data.name)
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode || !PVPSystem.Instance.IsReady)
        return;
      this.CloseDialogAnim();
    }
    else
      this.CloseDialogAnim();
  }

  public void OnClickGroupClicked()
  {
    this.SendRay((Vector2) UIManager.inst.ui2DOverlayCamera.WorldToScreenPoint(this.clickTriger.transform.position));
  }

  public void BeginAnimations()
  {
    if (this.Data.ignoreCamera || this.Data.mode == 0)
      this.Init(0.0f);
    else if (this.Data.isMiniMapCamera)
    {
      TweenPosition move = TweenPosition.Begin(MiniMapSystem.Instance.TheCamera.GetComponent<Camera>().gameObject, this.Data.smoothTime, this.Data.cameraPos, false);
      move.onFinished.Clear();
      move.onFinished.Add(new EventDelegate((EventDelegate.Callback) (() =>
      {
        if (!((UnityEngine.Object) null != (UnityEngine.Object) this.gameObject))
          return;
        this.Init(0.0f);
        move.onFinished.Clear();
      })));
    }
    else
    {
      CityCamera cityCamera = UIManager.inst.cityCamera;
      UICamera component = cityCamera.GetComponent<UICamera>();
      cityCamera.zoomSmoothFactor = (float) ((double) this.Data.smoothTime * 121.0 / 19.0 / 19.0);
      cityCamera.TargetZoom = this.Data.cityCameraZoom;
      TweenPosition move = TweenPosition.Begin(component.gameObject, this.Data.smoothTime, this.Data.cameraPos, false);
      move.onFinished.Clear();
      move.onFinished.Add(new EventDelegate((EventDelegate.Callback) (() =>
      {
        if (!((UnityEngine.Object) null != (UnityEngine.Object) this.gameObject))
          return;
        this.Init(0.0f);
        move.onFinished.Clear();
      })));
    }
  }

  private void ForceSkipProcess()
  {
    string[] strArray = this.Data.name.Split('.');
    if (strArray.Length != 2)
      return;
    using (Dictionary<string, List<string>>.Enumerator enumerator = NewTutorial.Instance.DataSource.pipeline.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator.Current;
        for (int index = 0; index < current.Value.Count; ++index)
        {
          if (current.Value[index].Contains(strArray[0]))
          {
            string str = "finished";
            NewTutorialRecord newRecord = NewTutorial.Instance.GetNewRecord();
            if (newRecord.currentProcess.ContainsKey(current.Key))
              newRecord.currentProcess[current.Key] = str;
            else
              newRecord.currentProcess.Add(current.Key, str);
            newRecord.DebugLog();
            NewTutorial.Instance.SetTutorialProgress(newRecord);
            return;
          }
        }
      }
    }
  }

  private void NewRecordProgress()
  {
    using (Dictionary<string, List<string>>.Enumerator enumerator = NewTutorial.Instance.DataSource.pipeline.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator.Current;
        if (current.Value.Contains(this.Data.name))
        {
          int num = current.Value.IndexOf(this.Data.name);
          string str = num != current.Value.Count - 1 ? current.Value[num + 1] : "finished";
          NewTutorialRecord newRecord = NewTutorial.Instance.GetNewRecord();
          if (newRecord.currentProcess.ContainsKey(current.Key))
            newRecord.currentProcess[current.Key] = str;
          else
            newRecord.currentProcess.Add(current.Key, str);
          newRecord.DebugLog();
          NewTutorial.Instance.SetTutorialProgress(newRecord);
        }
      }
    }
  }

  public void CloseDialogAnim()
  {
    if (this.curMode == TutorialDialog.Mode.NPC)
    {
      float num = (float) (UIManager.inst.uiRoot.manualHeight / 2);
      iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 1, (object) "to", (object) 0, (object) "time", (object) this.npcmovetime, (object) "easetype", (object) "Linear", (object) "onupdate", (object) "NPCGroupFadeout", (object) "oncomplete", (object) "OnFadeoutComplete"));
    }
    else
    {
      if (this.curMode != TutorialDialog.Mode.CLICK)
        return;
      this.Close();
    }
  }

  private void ShiftNpcGroupBoxCollider(bool v)
  {
    foreach (Collider componentsInChild in this.npcGroup.GetComponentsInChildren<BoxCollider>())
      componentsInChild.enabled = v;
  }

  private void ShiftClickGroupBoxCollider(bool v)
  {
    foreach (Collider componentsInChild in this.clickGroup.GetComponentsInChildren<BoxCollider>())
      componentsInChild.enabled = v;
  }

  private void NPCGroupFadeout(float value)
  {
    foreach (UIWidget componentsInChild in this.npcGroup.GetComponentsInChildren<UIWidget>())
      componentsInChild.alpha = value;
  }

  private void OnFadeoutComplete()
  {
    this.Close();
  }

  public void Open(TutorialDialog.Parameter orgParam)
  {
    this.Data = orgParam.td;
  }

  public void Close()
  {
    this.transform.FindChild("bg").GetComponent<BoxCollider>().enabled = true;
    this.npcGroup.SetActive(false);
    this.clickGroup.SetActive(false);
    iTween.Stop(this.gameObject);
    this.NewRecordProgress();
    if (this.onFinished == null)
      return;
    this.onFinished();
  }

  public TutorialData Data
  {
    get
    {
      return this._data;
    }
    set
    {
      this._data = value;
      if (this.Data.mode == 0)
      {
        this.BeginAnimations();
      }
      else
      {
        this.CurTarget = (Transform) null;
        this.UglyHardCode();
      }
    }
  }

  public System.Action onFinished { get; set; }

  public void ResetSelf()
  {
  }

  public enum Mode
  {
    NPC,
    CLICK,
  }

  public class Parameter
  {
    public TutorialData td;
  }
}
