﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarSignUpState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AllianceWarSignUpState : AllianceWarBaseState
{
  private const string CONDITION_DESCRIPTION = "alliance_warfare_conditions_description";
  private const string ONLY_LEADER_TOAST = "toast_alliance_warfare_only_leader";
  private const string SIGN_UP_SUCESS_TOAST = "toast_alliance_warfare_registration_success";
  private const string SIGN_UP_CANCELED_TOAST = "toast_alliance_warfare_registration_cancelled";
  private const string ALLIANCE_RANK_TOAST = "toast_alliance_warfare_kingdom_rank_low";
  private const string ACTIVITY_IN_PROGRESS_TOAST = "toast_alliance_warfare_kingdom_invasion_open";
  private const string ALLIANCE_LEVEL_TOAST = "toast_alliance_warfare_alliance_level_join";
  private const string ALLIANCE_POWER_TOAST = "toast_alliance_warfare_alliance_power_join";
  private const string ALLIANCE_7_DAT_TOAST = "toast_alliance_warfare_alliance_members_join";
  private const string ALLIANCE_LEVEL_MIN_KEY = "alliance_warfare_sign_up_alliance_level_min";
  private const string ALLIANCE_POWER_MIN_KEY = "alliance_warfare_sign_up_alliance_power_min";
  private const string ALLIANCE_ACTIVITY_MEMBER_MIN_KEY = "alliance_warfare_sign_up_alliance_active_member_min";
  private const string ALLIANCE_RANK_MAX_KEY = "alliance_warfare_sign_up_alliance_rank_max";
  private const string ALLIANCE_SIGN_UP_CD = "alliance_warfare_sign_up_cd";
  private const int SEVEN_DAY_TIME = 604800;
  public const string REGISTER = "alliance_warfare_register";
  public const string UNREGISTER = "alliance_warfare_register_cancel";
  public UIButton signUpButton;
  public UIButton unRegisterButton;
  public UILabel signUpLabel;
  public UILabel unregisterLabel;
  public UILabel signUpCondition;
  public UILabel allianceWarStartTimeLabel;
  private int allianceLevel;
  private long alliancePower;
  private int sevenDayActivityCount;
  private int allianceRank;
  private int signUpCd;
  private bool registerSuccess;
  private bool isAllianceMemberDataReady;
  private bool isWonderDataReady;

  public override void Show(bool requestData)
  {
    this.InitData();
    base.Show(false);
  }

  private void InitData()
  {
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_sign_up_alliance_level_min");
    if (data1 != null)
      this.allianceLevel = data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_sign_up_alliance_power_min");
    if (data2 != null)
      this.alliancePower = data2.ValueLong;
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_sign_up_alliance_active_member_min");
    if (data3 != null)
      this.sevenDayActivityCount = data3.ValueInt;
    GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_sign_up_alliance_rank_max");
    if (data4 != null)
      this.allianceRank = data4.ValueInt;
    GameConfigInfo data5 = ConfigManager.inst.DB_GameConfig.GetData("alliance_warfare_sign_up_cd");
    if (data5 != null)
      this.signUpCd = data5.ValueInt;
    if (this.allianceWarData.IsRegistered)
      this.registerSuccess = true;
    else
      this.registerSuccess = false;
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    int time1 = this.allianceWarData.RegisterEndTime - NetServerTime.inst.ServerTimestamp;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", Utils.FormatTime(time1, true, true, true));
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_enrollment_status", para, true);
    this.signUpCondition.text = ScriptLocalization.GetWithPara("alliance_warfare_conditions_description", new Dictionary<string, string>()
    {
      {
        "0",
        this.allianceLevel.ToString()
      },
      {
        "1",
        this.alliancePower.ToString()
      },
      {
        "2",
        this.sevenDayActivityCount.ToString()
      },
      {
        "3",
        this.allianceRank.ToString()
      }
    }, true);
    if (this.registerSuccess)
    {
      this.ShowRegisterButton(false);
      this.ShowUnRegisterButton(true);
    }
    else
    {
      this.ShowUnRegisterButton(false);
      this.ShowRegisterButton(true);
      if (this.allianceWarData.RegisterCDTime != 0)
      {
        int time2 = this.allianceWarData.RegisterCDTime - NetServerTime.inst.ServerTimestamp;
        if (time2 >= 0)
        {
          this.signUpButton.isEnabled = false;
          this.signUpLabel.text = Utils.XLAT("alliance_warfare_register") + string.Format("({0})", (object) Utils.FormatTime(time2, false, true, true));
        }
        else
        {
          this.signUpButton.isEnabled = true;
          this.signUpLabel.text = Utils.XLAT("alliance_warfare_register");
          this.allianceWarData.RegisterCDTime = 0;
        }
      }
      else
        this.signUpLabel.text = Utils.XLAT("alliance_warfare_register");
    }
    if (this.isAllianceMemberDataReady && this.isWonderDataReady)
    {
      this.isAllianceMemberDataReady = false;
      this.isWonderDataReady = false;
      this.ExecuteRegisterRequest();
    }
    para.Clear();
    para.Add("0", Utils.FormatTimeYYYYMMDDHHMMSS((long) ActivityManager.Intance.allianceWar.FightStartTime, "-"));
    this.allianceWarStartTimeLabel.text = ScriptLocalization.GetWithPara("alliance_warfare_this_round_opens_description", para, true);
  }

  public override void Hide()
  {
    base.Hide();
  }

  private bool IsConditionSatisfied()
  {
    bool flag = false;
    if (PlayerData.inst.allianceData != null && !this.IsKingdomWarInProgress() && (PlayerData.inst.allianceData.allianceLevel >= this.allianceLevel && PlayerData.inst.allianceData.power >= this.alliancePower) && (this.allianceWarData.AllianceRank > 0 && this.allianceWarData.AllianceRank <= this.allianceRank && this.Satisify7DayActivityCondition()))
      flag = true;
    return flag;
  }

  private bool IsKingdomWarInProgress()
  {
    bool flag = false;
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) PlayerData.inst.userData.world_id);
    if (wonderData != null)
      flag = wonderData.State == "fighting";
    else
      D.error((object) "can't find wonder data.");
    return flag;
  }

  private bool Satisify7DayActivityCondition()
  {
    int num = 0;
    bool flag = false;
    if (PlayerData.inst.allianceData != null)
    {
      using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = PlayerData.inst.allianceData.members.datas.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UserData userData = DBManager.inst.DB_User.Get(enumerator.Current.uid);
          if (userData != null && (long) NetServerTime.inst.ServerTimestamp - userData.lastLogin < 604800L)
            ++num;
        }
      }
    }
    if (num >= this.sevenDayActivityCount)
      flag = true;
    return flag;
  }

  private void ShowRegisterButton(bool show)
  {
    this.signUpButton.gameObject.SetActive(show);
  }

  private void ShowUnRegisterButton(bool show)
  {
    this.unregisterLabel.text = Utils.XLAT("alliance_warfare_register_cancel");
    this.unRegisterButton.gameObject.SetActive(show);
  }

  public void OnSignUpButtonPressed()
  {
    if (PlayerData.inst.allianceData != null)
    {
      if (PlayerData.inst.allianceData.creatorId == PlayerData.inst.uid)
      {
        if (this.registerSuccess)
          return;
        this.signUpButton.isEnabled = false;
        this.SendRegisterRequest();
      }
      else
        UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_only_leader"), (System.Action) null, 4f, false);
    }
    else
      Logger.Error((object) "There is no alliance data!");
  }

  public void OnUnregisterBtnClicked()
  {
    if (PlayerData.inst.allianceData != null)
    {
      if (PlayerData.inst.allianceData.creatorId == PlayerData.inst.uid)
      {
        if (!this.registerSuccess)
          return;
        this.SendUnregisterRequest(new System.Action<bool, object>(this.OnUnRegisterCompleteCallback), true);
      }
      else
        UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_only_leader"), (System.Action) null, 4f, false);
    }
    else
      Logger.Error((object) "There is no alliance data!");
  }

  private void SendRegisterRequest()
  {
    this.SendGetAllianceMemberRequest(new System.Action<bool, object>(this.OnRequestAllianceMemberCallback), false);
    this.SendGetWonderDataRequest(new System.Action<bool, object>(this.OnWonderDataCallback), false);
  }

  private void SendRegisterRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("AC:register").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void SendUnregisterRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("AC:cancelRegister").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void SendGetAllianceMemberRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void SendGetWonderDataRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "world_ids"] = (object) new ArrayList()
    {
      (object) PlayerData.inst.userData.world_id
    };
    MessageHub.inst.GetPortByAction("wonder:loadWonderMapData").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void ExecuteRegisterRequest()
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (this.IsConditionSatisfied())
    {
      this.SendRegisterRequest(new System.Action<bool, object>(this.OnRegisterCompleteCallback), true);
    }
    else
    {
      if (PlayerData.inst.allianceData != null)
      {
        if (PlayerData.inst.allianceData.allianceLevel < this.allianceLevel)
        {
          para.Add("0", this.allianceLevel.ToString());
          UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_warfare_alliance_level_join", para, true), (System.Action) null, 4f, false);
        }
        else if (PlayerData.inst.allianceData.power < this.alliancePower)
        {
          para.Add("0", this.alliancePower.ToString());
          UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_warfare_alliance_power_join", para, true), (System.Action) null, 4f, false);
        }
        else if (this.allianceWarData.AllianceRank == 0 || this.allianceWarData.AllianceRank > this.allianceRank)
        {
          para.Add("0", this.allianceRank.ToString());
          UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_warfare_kingdom_rank_low", para, true), (System.Action) null, 4f, false);
        }
        else if (!this.Satisify7DayActivityCondition())
        {
          para.Add("0", this.sevenDayActivityCount.ToString());
          UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_warfare_alliance_members_join", para, true), (System.Action) null, 4f, false);
        }
        else if (this.IsKingdomWarInProgress())
          UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_kingdom_invasion_open"), (System.Action) null, 4f, false);
      }
      this.signUpButton.isEnabled = true;
    }
  }

  private void OnRequestAllianceMemberCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.isAllianceMemberDataReady = true;
  }

  private void OnWonderDataCallback(bool ret, object data)
  {
    if (!ret)
      return;
    this.isWonderDataReady = true;
  }

  private void OnRegisterCompleteCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_registration_success"), (System.Action) null, 4f, false);
    this.registerSuccess = true;
    this.UpdateUI();
    this.RequestData(false);
  }

  private void OnUnRegisterCompleteCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_registration_cancelled"), (System.Action) null, 4f, false);
    this.registerSuccess = false;
    this.UpdateUI();
    this.RequestData(false);
  }
}
