﻿// Decompiled with JetBrains decompiler
// Type: MonsterDisplayInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MonsterDisplayInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "monster_type")]
  public string monsterType;
  [Config(Name = "offset")]
  public string offsetOrgValue;
  public Vector3 offsetValue;
  [Config(Name = "scaling")]
  public string scalingOrgValue;
  public Vector3 scalingValue;
  [Config(Name = "rotation")]
  public string rotationOrgValue;
  public Quaternion rotationValue;
}
