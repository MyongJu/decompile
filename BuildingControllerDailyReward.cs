﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerDailyReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerDailyReward : BuildingControllerNew
{
  public GameObject chest;
  public GameObject freeCasino;
  public GameObject freeMarksman;

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChange);
    DBManager.inst.DB_Stats.onDataChanged += new System.Action<string>(this.OnUserStatusDataChange);
    MarksmanPayload.Instance.onMarksmanDataUpdated += new System.Action(this.OnMarksmanDataUpdated);
    this.ShowBuildingTipInfo();
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChange);
    DBManager.inst.DB_Stats.onDataChanged -= new System.Action<string>(this.OnUserStatusDataChange);
    MarksmanPayload.Instance.onMarksmanDataUpdated -= new System.Action(this.OnMarksmanDataUpdated);
  }

  private void ShowBuildingTipInfo()
  {
    if (DBManager.inst.DB_DailyActives.HadReadyOpenChest())
    {
      NGUITools.SetActive(this.chest, true);
      NGUITools.SetActive(this.freeCasino, false);
      NGUITools.SetActive(this.freeMarksman, false);
    }
    else if (!DBManager.inst.DB_DailyActives.HadReadyOpenChest() && CasinoFunHelper.Instance.isFreeSpinRoulette())
    {
      NGUITools.SetActive(this.chest, false);
      NGUITools.SetActive(this.freeCasino, true);
      NGUITools.SetActive(this.freeMarksman, false);
    }
    else if (MarksmanPayload.Instance.IsRefreshFree)
    {
      NGUITools.SetActive(this.chest, false);
      NGUITools.SetActive(this.freeCasino, false);
      NGUITools.SetActive(this.freeMarksman, true);
    }
    else
    {
      NGUITools.SetActive(this.chest, false);
      NGUITools.SetActive(this.freeCasino, false);
      NGUITools.SetActive(this.freeMarksman, false);
    }
  }

  private void OnUserStatusDataChange(string statsName)
  {
    if (!statsName.Contains("casino_play"))
      return;
    this.ShowBuildingTipInfo();
  }

  private void OnUserDataChange(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.ShowBuildingTipInfo();
  }

  private void OnMarksmanDataUpdated()
  {
    this.ShowBuildingTipInfo();
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    AudioManager.Instance.PlaySound("sfx_city_click_tavern", false);
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_dally_rewards", "id_daily_reward", new EventDelegate((EventDelegate.Callback) (() => this.OnDailyRewardClickHandler())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara1);
    CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_casino", "id_fortune_teller", new EventDelegate((EventDelegate.Callback) (() => CasinoFunHelper.Instance.ShowFirstCasinoScene())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara2);
    CityCircleBtnPara cityCircleBtnPara3 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_jackpot", "id_crystal_ball", new EventDelegate((EventDelegate.Callback) (() => CasinoFunHelper.Instance.ShowJackpotScene())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("jackpot_min_level");
    if (data == null || CityManager.inst.mStronghold.mLevel >= data.ValueInt)
      ftl.Add(cityCircleBtnPara3);
    CityCircleBtnPara cityCircleBtnPara4 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_artifact", "id_artifact_map", new EventDelegate((EventDelegate.Callback) (() => this.OnArtifactBoardClickHandler())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara4);
    CityCircleBtnPara cityCircleBtnPara5 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_marksman", "id_lucky_archer", new EventDelegate((EventDelegate.Callback) (() => MarksmanPayload.Instance.ShowFirstMarksmanScene())), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara5);
    CityTouchCircle.Instance.Title = ScriptLocalization.Get("tavern_name", true);
  }

  public void OnDailyRewardClickHandler()
  {
    RequestManager.inst.SendRequest("Quest:refreshDailyQuest", (Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) => UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null)), true);
  }

  public void OnArtifactBoardClickHandler()
  {
    RequestManager.inst.SendRequest("Artifact:loadArtifact", new Hashtable()
    {
      {
        (object) "world_id",
        (object) PlayerData.inst.userData.world_id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Artifact/ArtifactDlg", (UI.Dialog.DialogParameter) new ArtifactDlg.Parameter()
      {
        artifactData = (data as ArrayList)
      }, true, true, true);
    }), true);
  }

  public void OnIconClickHandler()
  {
    AudioManager.Instance.PlaySound("sfx_city_click_tavern", false);
    if (DBManager.inst.DB_DailyActives.HadReadyOpenChest())
    {
      RequestManager.inst.SendRequest("Quest:refreshDailyQuest", (Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) => UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null)), true);
      OperationTrace.TraceClick(ClickArea.DailyRewardOutSide);
    }
    else if (!DBManager.inst.DB_DailyActives.HadReadyOpenChest() && CasinoFunHelper.Instance.isFreeSpinRoulette())
    {
      CasinoFunHelper.Instance.ShowFirstCasinoScene();
      OperationTrace.TraceClick(ClickArea.FortuneTellerOutSide);
    }
    else
    {
      if (!MarksmanPayload.Instance.IsRefreshFree)
        return;
      MarksmanPayload.Instance.ShowFirstMarksmanScene();
      OperationTrace.TraceClick(ClickArea.FirstMarksmanOutSide);
    }
  }

  private void OnBuildingSelected()
  {
    CitadelSystem.inst.OpenTouchCircle();
  }
}
