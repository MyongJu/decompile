﻿// Decompiled with JetBrains decompiler
// Type: CasinoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class CasinoDlg : UI.Dialog
{
  public float spinDuration = 5.5f;
  private float spinAngleOffset = 180f;
  public GameObject turntable;
  public UIButton spinButton;
  public UILabel throwTalusLabel;
  public UILabel spinCostLabel;
  public UITexture[] itemTextures;
  private TweenRotation currentTweenRotation;
  private int rewardHitIndex;
  private long throwTalusCount;
  private bool isSpinning;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    RouletteRewardInfo.Instance.Initialize();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.throwTalusCount = PlayerData.inst.userData.copper_coin;
    this.isSpinning = false;
    this.AddEventHandler();
    this.ShowCasinoDetails();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    AudioManager.Instance.StopSound("sfx_city_casino_spin");
    this.RemoveEventHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (!this.isSpinning)
      return;
    this.ShowCollectionEffects(true);
  }

  public void ShowCasinoDetails()
  {
    if (this.throwTalusCount < RouletteRewardInfo.Instance.NeedTalusToSpinOnce && !CasinoFunHelper.Instance.isFreeSpinRoulette())
      UIManager.inst.OpenPopup("Casino/GetMorePopup", (Popup.PopupParameter) new CasinoGetMorePopup.Parameter()
      {
        type = "casino1"
      });
    this.throwTalusLabel.text = PlayerData.inst.userData.copper_coin.ToString();
    this.ShowMainContent();
    this.FillItemDataOnRoulette();
    this.UpdateTextInfo();
    this.UpdataNPC();
  }

  public void OnCurrencyBtnPressed()
  {
    UIManager.inst.OpenDlg("Casino/CasinoStoreDlg", (UI.Dialog.DialogParameter) new CasinoStoreDlg.Parameter()
    {
      displayType = 0
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void SpinButtonPressed()
  {
    if (!this.CheckPropsCount())
      return;
    this.spinButton.enabled = false;
    this.UpdateRewardHitIndex();
    this.StartCoroutine("StartSpinRoulette");
    this.throwTalusLabel.text = PlayerData.inst.userData.copper_coin.ToString();
  }

  public void OnBackBtnPressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void ShowMainContent()
  {
    Transform transform = this.transform.Find("CasinoEntrance/SpinRoulette/FrontPanel/PremiumCurrencyBtn/icon");
    if (!((UnityEngine.Object) transform != (UnityEngine.Object) null))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    MarksmanPayload.Instance.FeedMarksmanTexture(component, "Texture/ItemIcons/item_casino_knucklebone_tiny");
  }

  private void GiveupLastChestInfo()
  {
    MessageHub.inst.GetPortByAction("casino:closeChest").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUerDataChange);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUerDataChange);
  }

  private void OnUerDataChange(long user_id)
  {
    if (user_id != PlayerData.inst.uid)
      return;
    this.throwTalusLabel.text = PlayerData.inst.userData.copper_coin.ToString();
  }

  [DebuggerHidden]
  private IEnumerator StartSpinRoulette()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CasinoDlg.\u003CStartSpinRoulette\u003Ec__Iterator2E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void SpinRoulette()
  {
    float rouletteItemAngle = this.GetRouletteItemAngle(this.rewardHitIndex);
    TweenRotation tweenRotation = this.turntable.AddComponent<TweenRotation>();
    tweenRotation.from = new Vector3(0.0f, 0.0f, this.turntable.transform.localEulerAngles.z);
    tweenRotation.to = new Vector3(0.0f, 0.0f, this.spinDuration * 360f + this.spinAngleOffset + rouletteItemAngle);
    tweenRotation.duration = this.spinDuration;
    tweenRotation.callWhenFinished = "OnSpinFinished";
    tweenRotation.onFinished.Add(new EventDelegate(new EventDelegate.Callback(this.OnSpinFinished)));
    tweenRotation.animationCurve = this.turntable.GetComponent<TweenRotation>().animationCurve;
    tweenRotation.Play(true);
    AudioManager.Instance.PlaySound("sfx_city_casino_spin", true);
    this.currentTweenRotation = tweenRotation;
    this.isSpinning = true;
    this.throwTalusLabel.text = PlayerData.inst.userData.copper_coin.ToString();
  }

  private bool UpdateRewardHitIndex()
  {
    this.rewardHitIndex = -1;
    MessageHub.inst.GetPortByAction("casino:play").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      int outData = -1;
      if (inData == null || inData[(object) "group_id"] == null)
        return;
      DatabaseTools.UpdateData(inData, "group_id", ref outData);
      if (outData == -1)
        return;
      this.rewardHitIndex = outData - 1;
    }), true);
    return this.rewardHitIndex != -1;
  }

  private void UpdateTextInfo()
  {
    if (CasinoFunHelper.Instance.isFreeSpinRoulette())
    {
      this.spinCostLabel.color = Color.white;
      this.spinCostLabel.text = Utils.XLAT("id_uppercase_free");
    }
    else
    {
      if (PlayerData.inst.userData.copper_coin < RouletteRewardInfo.Instance.NeedTalusToSpinOnce)
        this.spinCostLabel.color = Color.red;
      else
        this.spinCostLabel.color = Color.white;
      this.spinCostLabel.text = RouletteRewardInfo.Instance.NeedTalusToSpinOnce.ToString();
    }
  }

  private void UpdataNPC()
  {
    Transform transform = this.transform.Find("CasinoEntrance/SpinRoulette/npc");
    if (!((UnityEngine.Object) transform != (UnityEngine.Object) null))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    Utils.SetPortrait(component, "Texture/STATIC_TEXTURE/tutorial_npc_03");
  }

  private RouletteRewardItem GetRouletteRewardItem(int index)
  {
    List<RouletteRewardItem> rouletteRewardItemList = RouletteRewardInfo.Instance.GetRouletteRewardItemList();
    if (rouletteRewardItemList != null && index < rouletteRewardItemList.Count)
    {
      for (int index1 = 0; index1 < rouletteRewardItemList.Count; ++index1)
      {
        if (rouletteRewardItemList[index1].groupId == index + 1)
          return rouletteRewardItemList[index1];
      }
    }
    return (RouletteRewardItem) null;
  }

  private float GetRouletteItemAngle(int index)
  {
    List<RouletteRewardItem> rouletteRewardItemList = RouletteRewardInfo.Instance.GetRouletteRewardItemList();
    if (rouletteRewardItemList != null && index < rouletteRewardItemList.Count)
    {
      for (int index1 = 0; index1 < rouletteRewardItemList.Count; ++index1)
      {
        if (rouletteRewardItemList[index1].groupId == index + 1)
          return rouletteRewardItemList[index1].stopAngle;
      }
    }
    return 0.0f;
  }

  private void OnSpinFinished()
  {
    this.isSpinning = false;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.currentTweenRotation);
    AudioManager.Instance.StopSound("sfx_city_casino_spin");
    this.ShowCollectionEffects(true);
    this.UpdateTextInfo();
  }

  private void ShowCollectionEffects(bool showCardWin = true)
  {
    if (this.rewardHitIndex == -1)
      return;
    RouletteRewardItem rouletteRewardItem = this.GetRouletteRewardItem(this.rewardHitIndex);
    if (rouletteRewardItem == null)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    ItemRewardInfo.Data data = new ItemRewardInfo.Data();
    ResRewardsInfo.Data resReward = new ResRewardsInfo.Data();
    DailyRewardFactor currentRewardFactor = ConfigManager.inst.DB_DailyActivies.GetCurrentRewardFactor();
    string rewardItemType = rouletteRewardItem.rewardItemType;
    if (rewardItemType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CasinoDlg.\u003C\u003Ef__switch\u0024map2E == null)
      {
        // ISSUE: reference to a compiler-generated field
        CasinoDlg.\u003C\u003Ef__switch\u0024map2E = new Dictionary<string, int>(6)
        {
          {
            "chest",
            0
          },
          {
            "item",
            1
          },
          {
            "wood",
            2
          },
          {
            "food",
            3
          },
          {
            "ore",
            4
          },
          {
            "silver",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CasinoDlg.\u003C\u003Ef__switch\u0024map2E.TryGetValue(rewardItemType, out num))
      {
        switch (num)
        {
          case 0:
            if (showCardWin)
            {
              data.icon = "Texture/ItemIcons/icon_casino_card";
              data.count = 1f;
              this.Invoke("DelayShowCardPanel", 1f);
              break;
            }
            break;
          case 1:
            int internalId = !string.IsNullOrEmpty(rouletteRewardItem.rewardItemName) ? int.Parse(rouletteRewardItem.rewardItemName) : -1;
            int rewardItemCount = rouletteRewardItem.rewardItemCount;
            ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
            if (itemStaticInfo != null)
              data.icon = itemStaticInfo.ImagePath;
            data.count = (float) rewardItemCount;
            this.spinButton.enabled = true;
            RewardsCollectionAnimator.Instance.items.Add(data);
            RewardsCollectionAnimator.Instance.CollectItems(false);
            break;
          case 2:
            this.AddResourceRewardCollection(rouletteRewardItem, resReward, currentRewardFactor.CasinoWoodRatio);
            this.spinButton.enabled = true;
            RewardsCollectionAnimator.Instance.CollectResource(false);
            break;
          case 3:
            this.AddResourceRewardCollection(rouletteRewardItem, resReward, currentRewardFactor.CasinoFoodRatio);
            this.spinButton.enabled = true;
            RewardsCollectionAnimator.Instance.CollectResource(false);
            break;
          case 4:
            this.AddResourceRewardCollection(rouletteRewardItem, resReward, currentRewardFactor.CasinoOreRatio);
            this.spinButton.enabled = true;
            RewardsCollectionAnimator.Instance.CollectResource(false);
            break;
          case 5:
            this.AddResourceRewardCollection(rouletteRewardItem, resReward, currentRewardFactor.CasinoSilverRatio);
            this.spinButton.enabled = true;
            RewardsCollectionAnimator.Instance.CollectResource(false);
            break;
        }
      }
    }
    this.PlayCollectionSound(rouletteRewardItem.rewardItemType);
  }

  private void PlayCollectionSound(string rewardItemType)
  {
    if (rewardItemType.Contains("chest"))
      return;
    AudioManager.Instance.StopAndPlaySound("sfx_item_buy");
  }

  private void AddResourceRewardCollection(RouletteRewardItem rouletteRewardItem, ResRewardsInfo.Data resReward, float ratio)
  {
    string lower = rouletteRewardItem.rewardItemType.ToLower();
    if (lower != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CasinoDlg.\u003C\u003Ef__switch\u0024map2F == null)
      {
        // ISSUE: reference to a compiler-generated field
        CasinoDlg.\u003C\u003Ef__switch\u0024map2F = new Dictionary<string, int>(4)
        {
          {
            "wood",
            0
          },
          {
            "food",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CasinoDlg.\u003C\u003Ef__switch\u0024map2F.TryGetValue(lower, out num))
      {
        switch (num)
        {
          case 0:
            resReward.rt = ResRewardsInfo.ResType.Wood;
            break;
          case 1:
            resReward.rt = ResRewardsInfo.ResType.Food;
            break;
          case 2:
            resReward.rt = ResRewardsInfo.ResType.Ore;
            break;
          case 3:
            resReward.rt = ResRewardsInfo.ResType.Silver;
            break;
        }
      }
    }
    resReward.count = rouletteRewardItem.rewardItemCount * (int) ratio;
    RewardsCollectionAnimator.Instance.Ress.Add(resReward);
  }

  private void DelayShowCardPanel()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_harvest_timed_chest");
    UIManager.inst.OpenDlg("Casino/CasinoPickCardDlg", (UI.Dialog.DialogParameter) new CasinoPickCardDlg.Parameter()
    {
      hitChestId = -1,
      canShuffle = true
    }, 1 != 0, 1 != 0, 1 != 0);
    this.spinButton.enabled = true;
  }

  private bool CheckPropsCount()
  {
    if (CasinoFunHelper.Instance.isFreeSpinRoulette())
    {
      CasinoFunHelper.Instance.isFreeOnceTime = true;
      return true;
    }
    if (PlayerData.inst.userData.copper_coin >= RouletteRewardInfo.Instance.NeedTalusToSpinOnce)
      return true;
    UIManager.inst.OpenPopup("Casino/GetMorePopup", (Popup.PopupParameter) new CasinoGetMorePopup.Parameter()
    {
      type = "casino1"
    });
    return false;
  }

  private void FillItemDataOnRoulette()
  {
    List<RouletteRewardItem> rouletteRewardItemList = RouletteRewardInfo.Instance.GetRouletteRewardItemList();
    for (int index = 0; index < rouletteRewardItemList.Count; ++index)
    {
      RouletteRewardItem rouletteRewardItem = rouletteRewardItemList[index];
      string path = string.Empty;
      string rewardItemType = rouletteRewardItem.rewardItemType;
      if (rewardItemType != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (CasinoDlg.\u003C\u003Ef__switch\u0024map30 == null)
        {
          // ISSUE: reference to a compiler-generated field
          CasinoDlg.\u003C\u003Ef__switch\u0024map30 = new Dictionary<string, int>(2)
          {
            {
              "chest",
              0
            },
            {
              "item",
              1
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (CasinoDlg.\u003C\u003Ef__switch\u0024map30.TryGetValue(rewardItemType, out num))
        {
          switch (num)
          {
            case 0:
              path = "Texture/ItemIcons/icon_casino_card";
              goto label_11;
            case 1:
              ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(!string.IsNullOrEmpty(rouletteRewardItem.rewardItemName) ? int.Parse(rouletteRewardItem.rewardItemName) : -1);
              if (itemStaticInfo != null)
              {
                path = itemStaticInfo.ImagePath;
                goto label_11;
              }
              else
                goto label_11;
          }
        }
      }
      path = "Texture/BuildingConstruction/" + Utils.GetResoureSpriteName(rouletteRewardItem.rewardItemType);
label_11:
      if (!string.IsNullOrEmpty(path))
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemTextures[index], path, (System.Action<bool>) null, true, false, string.Empty);
    }
  }
}
