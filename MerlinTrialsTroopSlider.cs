﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsTroopSlider
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class MerlinTrialsTroopSlider : TroopSlider
{
  public string tip = "toast_trial_num_troops_no_change";

  public void OnAreaClick()
  {
    UIManager.inst.toast.Show(Utils.XLAT(this.tip), (System.Action) null, 4f, false);
  }
}
