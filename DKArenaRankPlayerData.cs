﻿// Decompiled with JetBrains decompiler
// Type: DKArenaRankPlayerData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class DKArenaRankPlayerData
{
  public long score = 1000;
  public int rank;
  public long uid;
  public string name;
  public string icon;
  public int portrait;
  public string alliance;
  public int lordTitleId;

  public string UserName
  {
    get
    {
      if (!string.IsNullOrEmpty(this.alliance))
        return "[" + this.alliance + "]" + this.name;
      return this.name;
    }
  }

  public void Decode(object orgs)
  {
    string empty = string.Empty;
    Hashtable hashtable = orgs as Hashtable;
    if (hashtable.ContainsKey((object) "uid") && hashtable[(object) "uid"] != null)
      long.TryParse(hashtable[(object) "uid"].ToString(), out this.uid);
    if (hashtable.ContainsKey((object) "portrait") && hashtable[(object) "portrait"] != null)
      int.TryParse(hashtable[(object) "portrait"].ToString(), out this.portrait);
    if (hashtable.ContainsKey((object) "lord_title") && hashtable[(object) "lord_title"] != null)
      int.TryParse(hashtable[(object) "lord_title"].ToString(), out this.lordTitleId);
    if (hashtable.ContainsKey((object) "icon") && hashtable[(object) "icon"] != null)
      this.icon = hashtable[(object) "icon"].ToString();
    if (hashtable.ContainsKey((object) "name") && hashtable[(object) "name"] != null)
      this.name = hashtable[(object) "name"].ToString();
    if (hashtable.ContainsKey((object) "alliance") && hashtable[(object) "alliance"] != null)
      this.alliance = hashtable[(object) "alliance"].ToString();
    if (!hashtable.ContainsKey((object) "score") || hashtable[(object) "score"] == null)
      return;
    long.TryParse(hashtable[(object) "score"].ToString(), out this.score);
  }
}
