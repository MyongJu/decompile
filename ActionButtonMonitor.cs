﻿// Decompiled with JetBrains decompiler
// Type: ActionButtonMonitor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ActionButtonMonitor : MonoBehaviour
{
  private readonly float[] fixedPos = new float[4]
  {
    405f,
    140f,
    -125f,
    -390f
  };
  public GameObject[] items;

  public void UpdateButtons()
  {
    int index1 = 0;
    for (int index2 = 0; index2 < this.items.Length; ++index2)
    {
      if (this.items[index2].activeSelf)
      {
        Utils.SetLPX(this.items[index2], this.fixedPos[index1]);
        ++index1;
      }
    }
  }

  public void ResetButtons()
  {
    for (int index = 0; index < this.items.Length; ++index)
      this.items[index].SetActive(true);
  }
}
