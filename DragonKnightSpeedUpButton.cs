﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSpeedUpButton
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragonKnightSpeedUpButton : MonoBehaviour
{
  private readonly int[] m_Speeds = new int[3]{ 1, 2, 4 };
  private readonly string[] m_Icons = new string[3]
  {
    "icon_dungeon_battle_original_speed",
    "icon_dungeon_battle_double_speed",
    "icon_dungeon_battle_quadruple_speed"
  };
  public bool Restore = true;
  private const string SPEED_KEY = "dungeon_speedup_index";
  public UISprite m_SpeedIcon;
  private int m_SpeedIndex;
  private bool m_Start;
  public System.Action callback;

  private void Start()
  {
    this.m_Start = true;
    this.m_SpeedIndex = PlayerPrefsEx.GetIntByUid("dungeon_speedup_index", 0);
    this.UpdateSpeed();
  }

  private void OnEnable()
  {
    if (!this.m_Start)
      return;
    this.m_SpeedIndex = PlayerPrefsEx.GetIntByUid("dungeon_speedup_index", 0);
    this.UpdateSpeed();
  }

  private void OnDisable()
  {
    if (!this.Restore)
      return;
    this.m_SpeedIndex = 0;
    this.UpdateSpeed();
  }

  public void OnSpeedup()
  {
    this.m_SpeedIndex = (this.m_SpeedIndex + 1) % this.Length;
    PlayerPrefsEx.SetIntByUid("dungeon_speedup_index", this.m_SpeedIndex);
    this.UpdateSpeed();
    if (this.callback == null)
      return;
    this.callback();
  }

  public void Reset()
  {
    if (!this.m_Start)
      return;
    this.m_SpeedIndex = PlayerPrefsEx.GetIntByUid("dungeon_speedup_index", 0);
    this.UpdateSpeed();
  }

  private void UpdateSpeed()
  {
    Time.timeScale = (float) this.m_Speeds[this.m_SpeedIndex];
    this.m_SpeedIcon.spriteName = this.m_Icons[this.m_SpeedIndex];
  }

  private int Length
  {
    get
    {
      if (PlayerData.inst.hostPlayer.VIPActive)
      {
        VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
        if (vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_4"))
          return 3;
        if (vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_2"))
          return 2;
      }
      return 1;
    }
  }
}
