﻿// Decompiled with JetBrains decompiler
// Type: PVPTempleBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class PVPTempleBanner : SpriteBanner
{
  protected SpriteCreator m_spriteCreator = new SpriteCreator();
  public TextMesh m_Status;
  public GameObject m_RootPowerUp;
  public SpriteRenderer m_SkillIcon;
  public UISpriteMeshProgressBar m_TimeProgress;
  public UISpriteMeshProgressBar m_PowerProgress;
  public TextMesh m_TimeText;
  public TextMesh m_PowerText;
  protected TileData m_tileData;

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
  }

  public void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    if ((bool) ((UnityEngine.Object) this.m_SkillIcon))
      this.m_SkillIcon.sprite = (UnityEngine.Sprite) null;
    this.m_spriteCreator.DestroyAllCreated();
  }

  protected void SecondTick(int delta)
  {
    this.UpdateUI(this.m_tileData);
  }

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    this.m_tileData = tile;
    string empty = string.Empty;
    string str = string.Empty;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    if (allianceData != null)
      empty += string.Format("({0})", (object) allianceData.allianceAcronym);
    AllianceTempleData templeData = tile.TempleData;
    if (templeData != null)
    {
      empty += AllianceBuildUtils.GetBuildName(templeData.ConfigId);
      str = string.Format("({0})", (object) PVPTempleBanner.GetStateString(templeData.State));
    }
    this.Name = empty;
    this.m_Status.text = str;
    bool flag = false;
    AllianceMagicData skillByAllianceId = DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkillByAllianceId(templeData.allianceId);
    if (skillByAllianceId != null)
    {
      TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(skillByAllianceId.ConfigId);
      if (byId != null)
      {
        flag = true;
        this.m_SkillIcon.sprite = this.m_spriteCreator.CreateSprite(byId.IconPath);
        long num = skillByAllianceId.TimerEnd - (long) NetServerTime.inst.ServerTimestamp;
        this.m_TimeText.text = Utils.FormatTime((int) num, false, false, true);
        this.m_TimeProgress.SetProgress((float) num / (float) byId.DonationTime);
        float progress = (float) skillByAllianceId.Donation / (float) byId.MaxDonation;
        this.m_PowerText.text = string.Format("{0} {1:0.00%}", (object) ScriptLocalization.Get("id_charge", true), (object) progress);
        this.m_PowerProgress.SetProgress(progress);
      }
    }
    this.m_RootPowerUp.SetActive(flag);
  }

  private static string GetStateString(string state)
  {
    string key = state;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (PVPTempleBanner.\u003C\u003Ef__switch\u0024map9F == null)
      {
        // ISSUE: reference to a compiler-generated field
        PVPTempleBanner.\u003C\u003Ef__switch\u0024map9F = new Dictionary<string, int>(7)
        {
          {
            "unComplete",
            0
          },
          {
            "building",
            1
          },
          {
            "unDefend",
            2
          },
          {
            "defending",
            3
          },
          {
            "broken",
            4
          },
          {
            "repairing",
            5
          },
          {
            "demolishing",
            6
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (PVPTempleBanner.\u003C\u003Ef__switch\u0024map9F.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return Utils.XLAT("alliance_fort_status_unfinished");
          case 1:
            return Utils.XLAT("alliance_fort_status_under_construction");
          case 2:
            return Utils.XLAT("alliance_fort_status_not_garrisoned");
          case 3:
            return Utils.XLAT("alliance_fort_status_garrisoned");
          case 4:
            return Utils.XLAT("alliance_fort_status_damaged");
          case 5:
            return Utils.XLAT("alliance_fort_status_repair");
          case 6:
            return Utils.XLAT("alliance_fort_status_demolishing");
        }
      }
    }
    return string.Empty;
  }
}
