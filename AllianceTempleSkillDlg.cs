﻿// Decompiled with JetBrains decompiler
// Type: AllianceTempleSkillDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTempleSkillDlg : UI.Dialog
{
  protected List<AllianceTempleSkillGroupItem> m_allAllianceTempleSkillGroupItem = new List<AllianceTempleSkillGroupItem>();
  protected string DRAGON_ALTAR_TECH_TYPE = "alliance_temple_level_1";
  [SerializeField]
  protected UILabel m_labelExp;
  [SerializeField]
  protected UISlider m_sliderExp;
  [SerializeField]
  protected UILabel m_labelManaPoint;
  [SerializeField]
  protected UISlider m_sliderManaPoint;
  [SerializeField]
  protected UILabel m_labelElixirLimit;
  [SerializeField]
  protected UISlider m_sliderElixirLimit;
  [SerializeField]
  protected AllianceTempleSkillGroupItem m_skillGroupTemplate;
  [SerializeField]
  protected UITable m_tableSkillGroup;
  [SerializeField]
  protected UIScrollView m_scrollViewSkillGroup;
  [SerializeField]
  protected UIButton m_buttonUseElixir;
  [SerializeField]
  protected UIButton m_buttonDonate;
  [SerializeField]
  protected UILabel m_labelDonate;
  protected bool m_canResearch;
  protected int m_researchId;
  protected int m_previousElixirLimit;
  protected bool m_listDirty;

  protected int MaxAllianceTempleLevel
  {
    get
    {
      AllianceTechInfo maxTechInfoByType = ConfigManager.inst.DB_AllianceTech.GetMaxTechInfoByType(this.DRAGON_ALTAR_TECH_TYPE);
      if (maxTechInfoByType != null)
        return maxTechInfoByType.Level;
      return 0;
    }
  }

  protected int CurrentAllianceTempleLevel
  {
    get
    {
      return Utils.GetPlayerAllianceTempleLevel();
    }
  }

  protected int MaxMana
  {
    get
    {
      AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.GetMyAllianceTempleData();
      if (allianceTempleData != null)
      {
        AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceTempleData.ConfigId);
        if (buildingStaticInfo != null)
          return (int) buildingStaticInfo.Param1;
      }
      return 100;
    }
  }

  protected int CurrentMana
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.manaPoint;
      return 0;
    }
  }

  protected int MaxElixirLimit
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("alliance_temple_donation_repletion_max");
      if (data != null)
        return data.ValueInt;
      return 100;
    }
  }

  protected int CurrentElixirLimit
  {
    get
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
        return allianceData.elixirLimit;
      return 0;
    }
  }

  protected int CurrentTechId
  {
    get
    {
      AllianceTechData allicenTechByType = DBManager.inst.DB_AllianceTech.GetCurAllicenTechByType(this.DRAGON_ALTAR_TECH_TYPE);
      if (allicenTechByType != null)
        return allicenTechByType.researchId;
      return ConfigManager.inst.DB_AllianceTech.GetMinTechInfoByType(this.DRAGON_ALTAR_TECH_TYPE).internalId;
    }
  }

  protected int CurrentTechReqAllianceLevel
  {
    get
    {
      AllianceTechInfo minTechInfoByType = ConfigManager.inst.DB_AllianceTech.GetMinTechInfoByType(this.DRAGON_ALTAR_TECH_TYPE);
      AllianceTechData allicenTechByType = DBManager.inst.DB_AllianceTech.GetCurAllicenTechByType(this.DRAGON_ALTAR_TECH_TYPE);
      if (allicenTechByType != null)
        minTechInfoByType = ConfigManager.inst.DB_AllianceTech.GetItem(allicenTechByType.researchId);
      if (minTechInfoByType != null)
      {
        AllianceTechTier allianceTechTier = ConfigManager.inst.DB_AllianceTech.GetAllianceTechTier(minTechInfoByType.TierId);
        if (allianceTechTier != null)
        {
          Alliance_LevelInfo data = ConfigManager.inst.DB_Alliance_Level.GetData(allianceTechTier.ReqAllianceLevel);
          if (data != null)
            return data.Alliance_Level;
        }
      }
      return 0;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_AllianceTech.onDataChanged += new System.Action<int>(this.OnAllianceTechDataChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
    Hashtable postData = new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      }
    };
    RequestManager.inst.SendRequest("AllianceTemple:loadTempleInfo", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      PlayerData.inst.allianceTechManager.Update(data);
      this.UpdateUI();
    }), true);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_AllianceTech.onDataChanged -= new System.Action<int>(this.OnAllianceTechDataChanged);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  protected void OnSecondTick(int delta)
  {
    int currentElixirLimit = this.CurrentElixirLimit;
    if (currentElixirLimit != this.m_previousElixirLimit)
    {
      this.UpdateStateUI();
      this.m_previousElixirLimit = currentElixirLimit;
    }
    this.UpdateTempleSkillList();
    this.UpdateExp();
  }

  protected void OnAllianceDataChanged(long allianceId)
  {
    if (PlayerData.inst.allianceId != allianceId)
      return;
    this.UpdateUI();
  }

  protected void OnAllianceTechDataChanged(int techId)
  {
    AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(techId);
    if (allianceTechInfo == null || !(allianceTechInfo.ResearchType == this.DRAGON_ALTAR_TECH_TYPE))
      return;
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    this.UpdateExp();
    this.UpdateStateUI();
    this.UpdateTempleSkillList();
  }

  protected void UpdateStateUI()
  {
    this.m_labelManaPoint.text = string.Format("{0}/{1}", (object) this.CurrentMana, (object) this.MaxMana);
    this.m_sliderManaPoint.value = (float) this.CurrentMana / (float) this.MaxMana;
    this.m_labelElixirLimit.text = string.Format("{0}/{1}", (object) this.CurrentElixirLimit, (object) this.MaxElixirLimit);
    this.m_sliderElixirLimit.value = (float) this.CurrentElixirLimit / (float) this.MaxElixirLimit;
    this.m_buttonUseElixir.isEnabled = this.CurrentElixirLimit < this.MaxElixirLimit && this.CurrentMana < this.MaxMana;
  }

  protected void UpdateExp()
  {
    this.m_canResearch = false;
    this.m_buttonDonate.gameObject.SetActive(true);
    this.m_buttonDonate.isEnabled = true;
    this.m_labelDonate.text = ScriptLocalization.Get("alliance_altar_go_to_donations", true);
    AllianceTechData allicenTechByType = DBManager.inst.DB_AllianceTech.GetCurAllicenTechByType(this.DRAGON_ALTAR_TECH_TYPE);
    int num1 = 0;
    AllianceTechInfo infoByTypeAndLevel;
    if (allicenTechByType != null)
    {
      this.m_researchId = allicenTechByType.researchId;
      num1 = allicenTechByType.donation;
      infoByTypeAndLevel = ConfigManager.inst.DB_AllianceTech.GetItem(allicenTechByType.researchId);
    }
    else
      infoByTypeAndLevel = ConfigManager.inst.DB_AllianceTech.GetTechInfoByTypeAndLevel(this.DRAGON_ALTAR_TECH_TYPE, 0);
    if (infoByTypeAndLevel == null)
      D.error((object) "can not found alliance tech config with type {0}", (object) this.DRAGON_ALTAR_TECH_TYPE);
    else if (infoByTypeAndLevel.Level < this.MaxAllianceTempleLevel)
    {
      if (allicenTechByType != null && allicenTechByType.jobId != 0L)
      {
        string str = ScriptLocalization.Get("alliance_knowledge_research_in_progress", true);
        JobHandle job = JobManager.Instance.GetJob(allicenTechByType.jobId);
        if (job != null)
        {
          str += string.Format("({0})", (object) Utils.FormatTime(job.LeftTime(), false, false, true));
          int num2 = job.EndTime() - job.StartTime();
          this.m_sliderExp.value = (float) (1.0 - (double) job.LeftTime() / (double) num2);
        }
        this.m_labelExp.text = str;
        this.m_buttonDonate.isEnabled = false;
      }
      else
      {
        this.m_labelExp.text = string.Format("{0} ({1})", (object) string.Format("{0}/{1}", (object) num1, (object) infoByTypeAndLevel.TotalExp), (object) ScriptLocalization.GetWithPara("alliance_altar_unlocks_next_level", new Dictionary<string, string>()
        {
          {
            "0",
            (infoByTypeAndLevel.Level + 1).ToString()
          }
        }, true));
        this.m_sliderExp.value = (float) num1 / (float) infoByTypeAndLevel.TotalExp;
        if (num1 < infoByTypeAndLevel.TotalExp)
          return;
        this.m_canResearch = true;
        if (Utils.IsR4Member() || Utils.IsR5Member())
          this.m_labelDonate.text = ScriptLocalization.Get("alliance_knowledge_research_button", true);
        else
          this.m_buttonDonate.gameObject.SetActive(false);
      }
    }
    else
    {
      this.m_buttonDonate.gameObject.SetActive(false);
      this.m_labelExp.text = ScriptLocalization.Get("id_uppercase_maxed", true);
      this.m_sliderExp.value = 1f;
    }
  }

  protected void UpdateTempleSkillList()
  {
    if (this.m_allAllianceTempleSkillGroupItem.Count <= 0)
    {
      for (int index = 0; index <= this.MaxAllianceTempleLevel; ++index)
      {
        if (ConfigManager.inst.DB_TempleSkill.IsHaveSkillUnlockAtLevel(index))
        {
          AllianceTempleSkillGroupItem templeSkillGroupItem = this.CreateTempleSkillGroupItem();
          templeSkillGroupItem.SetAllianceTempleLevel(index);
          this.m_allAllianceTempleSkillGroupItem.Add(templeSkillGroupItem);
        }
      }
      this.m_listDirty = true;
    }
    else
    {
      using (List<AllianceTempleSkillGroupItem>.Enumerator enumerator = this.m_allAllianceTempleSkillGroupItem.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.UpdateUI();
      }
    }
  }

  protected void LateUpdate()
  {
    if (!this.m_listDirty)
      return;
    this.m_listDirty = false;
    this.m_tableSkillGroup.repositionNow = true;
    this.m_tableSkillGroup.Reposition();
    this.m_scrollViewSkillGroup.movement = UIScrollView.Movement.Unrestricted;
    this.m_scrollViewSkillGroup.ResetPosition();
    this.m_scrollViewSkillGroup.movement = UIScrollView.Movement.Horizontal;
    this.m_scrollViewSkillGroup.currentMomentum = Vector3.zero;
  }

  protected AllianceTempleSkillGroupItem CreateTempleSkillGroupItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_skillGroupTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this.m_tableSkillGroup.transform);
    gameObject.transform.localScale = Vector3.one;
    return gameObject.GetComponent<AllianceTempleSkillGroupItem>();
  }

  public void OnDonateButtonClicked()
  {
    if (PlayerData.inst.allianceData == null)
      return;
    if (this.m_canResearch)
    {
      if (DBManager.inst.DB_AllianceTech.researchingId != 0)
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_tech_already_researching", true), (System.Action) null, 4f, false);
      else
        RequestManager.inst.SendRequest("AllianceTech:research", Utils.Hash((object) "research_id", (object) this.m_researchId), (System.Action<bool, object>) ((result, orgData) => this.UpdateUI()), true);
    }
    else
    {
      int reqAllianceLevel = this.CurrentTechReqAllianceLevel;
      if (PlayerData.inst.allianceData.allianceLevel < reqAllianceLevel)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_alliance_level_needed_num", new Dictionary<string, string>()
        {
          {
            "0",
            reqAllianceLevel.ToString()
          }
        }, true), (System.Action) null, 4f, true);
      else
        UIManager.inst.OpenPopup("AllianceTech/AllianceTechDonatePopUp", (Popup.PopupParameter) new AllianceTechDonateDlg.AllianceTechDonateParam()
        {
          researchId = this.CurrentTechId
        });
    }
  }

  public void OnUseElixirButtonClicked()
  {
    UIManager.inst.OpenPopup("Alliance/DragonAltarRestoreManaPopup", (Popup.PopupParameter) null);
  }
}
