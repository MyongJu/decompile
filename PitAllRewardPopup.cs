﻿// Decompiled with JetBrains decompiler
// Type: PitAllRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitAllRewardPopup : Popup
{
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _tableContainer;
  [SerializeField]
  private PitRewardItem _rewardItemTemplate;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._rewardItemTemplate.gameObject.SetActive(false);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    using (List<AbyssRankRewardInfo>.Enumerator enumerator = ConfigManager.inst.DB_AbyssRankReward.AllRankReward.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreatePitRewardItem().SetData(enumerator.Current, new System.Action<Icon>(this.OnIconClick), new System.Action<Icon>(this.OnIconPress), new System.Action<Icon>(this.OnIconRelease));
    }
    this._tableContainer.Reposition();
    this._scrollView.ResetPosition();
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private PitRewardItem CreatePitRewardItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._rewardItemTemplate.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._tableContainer.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    return gameObject.GetComponent<PitRewardItem>();
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }
}
