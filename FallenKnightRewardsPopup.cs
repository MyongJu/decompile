﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class FallenKnightRewardsPopup : Popup
{
  private int currentSelectedIndex = -1;
  private Dictionary<string, FallenKnightRankRewardItemRenderer> itemDict = new Dictionary<string, FallenKnightRankRewardItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    FallenKnightRewardsPopup.Parameter parameter = orgParam as FallenKnightRewardsPopup.Parameter;
    if (parameter != null)
      this.currentSelectedIndex = parameter.selectedIndex;
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowRankRewardContent(this.currentSelectedIndex);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.currentSelectedIndex = -1;
  }

  private void ShowRankRewardContent(int currentSelectedIndex)
  {
    this.ClearData();
    List<FallenKnightRankRewardInfo> rankRewardList = ConfigManager.inst.DB_FallenKnightRankReward.GetRankRewardList();
    for (int index = 0; index < rankRewardList.Count; ++index)
    {
      if (currentSelectedIndex == 1 && rankRewardList[index].Type == 0 || currentSelectedIndex == 2 && rankRewardList[index].Type == 1)
      {
        FallenKnightRankRewardItemRenderer itemRenderer = this.CreateItemRenderer(rankRewardList[index]);
        this.itemDict.Add(rankRewardList[index].ID, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<string, FallenKnightRankRewardItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private FallenKnightRankRewardItemRenderer CreateItemRenderer(FallenKnightRankRewardInfo info)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    FallenKnightRankRewardItemRenderer component = gameObject.GetComponent<FallenKnightRankRewardItemRenderer>();
    component.SetData(info);
    return component;
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int selectedIndex;
  }
}
