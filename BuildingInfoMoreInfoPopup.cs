﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoMoreInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoMoreInfoPopup : Popup
{
  public UILabel BigTitle;
  public UILabel StatsDescription;
  public UILabel LevelLabel;
  public GameObject mStatsPage;
  public GameObject StatsRow;
  public GameObject[] BennifitTitles;
  private BuildingInfo buildingInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    BuildingInfoMoreInfoPopup.Parameter parameter = orgParam as BuildingInfoMoreInfoPopup.Parameter;
    base.OnShow(orgParam);
    this.InitUI(parameter.info);
  }

  private void InitUI(BuildingInfo info)
  {
    this.buildingInfo = info;
    this.BigTitle.text = Utils.XLAT(info.Type + "_name");
    this.showAllBennifitInfo();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void showAllBennifitInfo()
  {
    this.StatsDescription.text = Utils.XLAT(this.buildingInfo.Type + "_description");
    this.LevelLabel.text = Utils.XLAT("walls_info_more_level");
    Transform transform = this.mStatsPage.GetComponentInChildren<UITable>().transform;
    for (int index = 0; index < transform.childCount; ++index)
      Object.Destroy((Object) transform.GetChild(index).gameObject);
    List<string> stringList = new List<string>();
    if (this.buildingInfo.Benefit_ID_1.Length > 0)
      stringList.Add(this.buildingInfo.Benefit_ID_1);
    if (this.buildingInfo.Benefit_ID_2.Length > 0)
      stringList.Add(this.buildingInfo.Benefit_ID_2);
    if (this.buildingInfo.Benefit_ID_3.Length > 0)
      stringList.Add(this.buildingInfo.Benefit_ID_3);
    for (int index = 0; index < 2; ++index)
    {
      if (index < stringList.Count)
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(stringList[index])];
        this.BennifitTitles[index].SetActive(true);
        this.BennifitTitles[index].GetComponent<UILabel>().text = dbProperty.Name;
      }
      else
        this.BennifitTitles[index].SetActive(false);
    }
    this.StatsRow.SetActive(false);
    List<BuildingInfo> buildingInfoList = new List<BuildingInfo>();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType(this.buildingInfo.Type);
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.buildingInfo.Type + "_" + (object) index);
      if (data != null)
        buildingInfoList.Add(data);
    }
    for (int index1 = 0; index1 < buildingInfoList.Count; ++index1)
    {
      BuildingInfo buildingInfo = buildingInfoList[index1];
      GameObject gameObject = Object.Instantiate<GameObject>(this.StatsRow);
      gameObject.SetActive(true);
      gameObject.transform.parent = transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.Find("Level").GetComponent<UILabel>().text = buildingInfo.Building_Lvl.ToString();
      List<double> doubleList = new List<double>();
      if (buildingInfo.Benefit_ID_1.Length > 0)
        doubleList.Add(buildingInfo.Benefit_Value_1);
      if (buildingInfo.Benefit_ID_2.Length > 0)
        doubleList.Add(buildingInfo.Benefit_Value_2);
      if (buildingInfo.Benefit_ID_3.Length > 0)
        doubleList.Add(buildingInfo.Benefit_Value_3);
      for (int index2 = 0; index2 < 2; ++index2)
      {
        if (index2 < stringList.Count)
        {
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(stringList[index2])];
          if (this.buildingInfo.Type == "watchtower")
          {
            UILabel component = gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>();
            component.text = Utils.XLAT("watchtower_ability_level" + (object) buildingInfo.Building_Lvl);
            component.alignment = NGUIText.Alignment.Left;
          }
          else if (this.IsTrainingBuilding())
          {
            UILabel component = gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>();
            component.text = Utils.XLAT(this.buildingInfo.Type + "_effect_level" + (object) buildingInfo.Building_Lvl);
            component.alignment = NGUIText.Alignment.Left;
          }
          else
            gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>().text = dbProperty.ConvertToDisplayString(doubleList[index2], false, true);
        }
        else
          gameObject.transform.Find("Value_" + (object) index2).GetComponent<UILabel>().text = string.Empty;
      }
      if (index1 + 1 == this.buildingInfo.Building_Lvl)
        gameObject.transform.Find("CurrentItemBackGround").GetComponent<UISprite>().enabled = true;
      else
        gameObject.transform.Find("CurrentItemBackGround").GetComponent<UISprite>().enabled = false;
    }
    transform.GetComponent<UITable>().repositionNow = true;
  }

  private bool IsTrainingBuilding()
  {
    if (!(this.buildingInfo.Type == "barracks") && !(this.buildingInfo.Type == "range") && (!(this.buildingInfo.Type == "stables") && !(this.buildingInfo.Type == "workshop")))
      return this.buildingInfo.Type == "fortress";
    return true;
  }

  public class Parameter : Popup.PopupParameter
  {
    public BuildingInfo info;
  }
}
