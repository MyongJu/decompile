﻿// Decompiled with JetBrains decompiler
// Type: DungeonRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DungeonRewardItemRenderer : MonoBehaviour
{
  private Dictionary<int, DungeonRewardSubItemRenderer> _itemDict = new Dictionary<int, DungeonRewardSubItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel rewardItemTitle;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public UITable table;
  private DungeonRankingRewardInfo _rewardInfo;

  public void SetData(DungeonRankingRewardInfo rewardInfo)
  {
    this._rewardInfo = rewardInfo;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.UpdateUI();
  }

  public void ClearData()
  {
    using (Dictionary<int, DungeonRewardSubItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
  }

  private DungeonRewardSubItemRenderer CreateItemRenderer(ItemStaticInfo itemInfo, int itemCount, int itemIndex)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    DungeonRewardSubItemRenderer component = gameObject.GetComponent<DungeonRewardSubItemRenderer>();
    component.SetData(itemInfo, itemCount, itemIndex);
    return component;
  }

  private void UpdateUI()
  {
    this.ClearData();
    this.rewardItemTitle.text = string.Format(Utils.XLAT("event_rank_num"), (object) this._rewardInfo.ranking);
    if (this._rewardInfo != null && this._rewardInfo.Rewards != null)
    {
      int key = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this._rewardInfo.Rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int.TryParse(current.Key, out result);
          ItemStaticInfo itemInfo = ConfigManager.inst.DB_Items.GetItem(result);
          if (itemInfo != null)
          {
            DungeonRewardSubItemRenderer itemRenderer = this.CreateItemRenderer(itemInfo, current.Value, key++);
            this._itemDict.Add(key, itemRenderer);
          }
        }
      }
    }
    this.Reposition();
  }
}
