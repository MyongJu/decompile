﻿// Decompiled with JetBrains decompiler
// Type: ReinforcePlayerWithTierComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ReinforcePlayerWithTierComponent : ComponentRenderBase
{
  public GameObject parent;
  public GameObject container;
  public DefaultPlayerComponent user;
  public TierListComponent tierList;
  public GameObject dragon;

  public override void Init()
  {
    base.Init();
    ReinforcePlayerWithTierComponentData data = this.data as ReinforcePlayerWithTierComponentData;
    this.user.GetComponent<DefaultPlayerComponent>().FeedData((IComponentData) new DefaultPlayerComponentData()
    {
      portrait = data.portrait,
      icon = data.icon,
      lordTitleId = data.lordTitleId,
      user_name = data.name,
      acronym = data.acronym,
      troopAmount = data.amount,
      level = ("Lv." + data.level)
    });
    if (data.dragon != null)
    {
      this.dragon.SetActive(true);
      this.dragon.GetComponent<DragonWithSkillComponent>().FeedData<DragonWithSkillComponentData>(data.dragon);
    }
    else
      this.dragon.SetActive(false);
    for (int index = 0; index < data.tiers.Count; ++index)
      NGUITools.AddChild(this.container, this.tierList.gameObject).GetComponent<TierListComponent>().FeedData((IComponentData) new TierComponentData(data.tiers[index].tier, data.tiers[index].list));
    this.container.GetComponent<TableContainer>().ResetPosition();
  }
}
