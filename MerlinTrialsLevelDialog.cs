﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsLevelDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsLevelDialog : Dialog
{
  private readonly Stack<MerlinTrialCityItem> _usedCityItems = new Stack<MerlinTrialCityItem>();
  private readonly Stack<MerlinTrialCityItem> _unusedCityItems = new Stack<MerlinTrialCityItem>();
  private const string cityTName = "castleItem_{0}_{1}";
  private const string lineTName = "line_{0}_{1}";
  private const string chestTName = "chestItem_{0}";
  public UILabel _title;
  public UILabel _floorValue;
  public GameObject _passIcon;
  public GameObject _mysteryRoot;
  public GameObject _cityRoot;
  public GameObject _chestRoot;
  public GameObject _lineRoot;
  public MerlinTrialCityItem _cityTemplate;
  public UIScrollView _scrollView;
  public UITexture _bgTexture;
  public UIButton _leftArrow;
  public UIButton _rightArrow;
  public GameObject _leftHint;
  public GameObject _rightHint;
  public GameObject _leftEffect;
  public GameObject _rightEffect;
  public UILabel _labelMysteryShopEndTime;
  private int _day;
  private int _curLevel;
  private int _groupId;
  private int _minLevel;
  private int _maxLevel;
  private int _curBattleId;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    MerlinTrialsLevelDialog.Parameter parameter = orgParam as MerlinTrialsLevelDialog.Parameter;
    if (parameter == null)
      return;
    this._day = parameter.day;
    this._groupId = ConfigManager.inst.DB_MerlinTrialsGroup.Day2Group(this._day);
    this._minLevel = ConfigManager.inst.DB_MerlinTrialsLayer.GetMinLevel(this._groupId);
    this._maxLevel = ConfigManager.inst.DB_MerlinTrialsLayer.GetMaxLevel(this._groupId);
    this._curLevel = MerlinTrialsHelper.inst.LatestLevelConfig(this._groupId).level;
    this._curBattleId = MerlinTrialsHelper.inst.LatestAvaliableTrialConfig(this._groupId).internalId;
    this._cityTemplate.gameObject.SetActive(false);
    this.FixScrollView();
    MerlinTrialsHelper.inst.RequestBattleStatus(this._groupId, new System.Action<object>(this.OnReqeustBattleStatusCallback));
  }

  private void FixScrollView()
  {
    Vector2 windowSize = this._scrollView.panel.GetWindowSize();
    this._scrollView.panel.SetRect(0.0f, 50f, windowSize.x, windowSize.y);
    this._bgTexture.transform.localPosition = new Vector3((float) (-(double) windowSize.x / 2.0), 0.0f, 0.0f);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI(this._curLevel);
    this.UpdateArrow();
    this.UpdateMysteryShopTime();
    this.UpdateMysteryShopStatus();
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
  }

  public override void OnBackButtonPress()
  {
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
      base.OnBackButtonPress();
    }));
  }

  public override void OnCloseButtonPress()
  {
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
      base.OnCloseButtonPress();
    }));
  }

  private void OnReqeustBattleStatusCallback(object orgData)
  {
    this.UpdateUI(this._curLevel);
    this.UpdateArrow();
    this.ScrollToCity(MerlinTrialsHelper.inst.LatestAvaliableTrialConfig(this._groupId).internalId, (System.Action<int>) null, false);
  }

  private void RecycleCities()
  {
    while (this._usedCityItems.Count > 0)
    {
      MerlinTrialCityItem t = this._usedCityItems.Pop();
      t.transform.parent = (Transform) null;
      t.gameObject.SetActive(false);
      this._unusedCityItems.Push(t);
    }
  }

  private MerlinTrialCityItem AllocateItem(GameObject root)
  {
    MerlinTrialCityItem t;
    if (this._unusedCityItems.Count == 0)
    {
      t = NGUITools.AddChild(root, this._cityTemplate.gameObject).GetComponent<MerlinTrialCityItem>();
    }
    else
    {
      t = this._unusedCityItems.Pop();
      t.transform.parent = root.transform;
    }
    t.gameObject.SetActive(true);
    t.transform.localPosition = Vector3.zero;
    t.transform.localScale = Vector3.one;
    this._usedCityItems.Push(t);
    return t;
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    AttackShowManager.Instance.onAttackShowTerminate += new System.Action(this.OnAttackShowTerminate);
    MerlinTrialsHelper.inst.onGroupComplete += new System.Action<int>(this.OnGroupComplete);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    AttackShowManager.Instance.onAttackShowTerminate -= new System.Action(this.OnAttackShowTerminate);
    MerlinTrialsHelper.inst.onGroupComplete -= new System.Action<int>(this.OnGroupComplete);
  }

  private void OnSecond(int time)
  {
    this.UpdateMysteryShopTime();
  }

  private void OnAttackShowTerminate()
  {
    this.UpdateMysteryShopStatus();
  }

  private void OnGroupComplete(int group)
  {
    this.UpdateUI(this._curLevel);
  }

  private void UpdateUI(int level)
  {
    this._title.text = ConfigManager.inst.DB_MerlinTrialsGroup.Get(this._groupId).LocName;
    this._floorValue.text = Utils.XLAT("trial_current_stage_level", (object) "0", (object) level);
    this._passIcon.SetActive(MerlinTrialsHelper.inst.IsGroupComplete(this._groupId));
    int layerId = ConfigManager.inst.DB_MerlinTrialsLayer.GetLayerId(this._groupId, level);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._bgTexture, ConfigManager.inst.DB_MerlinTrialsLayer.Get(layerId).BackgroundPath, (System.Action<bool>) (b =>
    {
      if (!b)
        return;
      this._bgTexture.height = 1550;
    }), true, false, "Texture/STATIC_TEXTURE/bg_MelinTiralLevel_0");
    List<MerlinTrialsMainInfo> allByLayerId = ConfigManager.inst.DB_MerlinTrialsMain.GetAllByLayerId(this._groupId, layerId);
    this.RecycleCities();
    int index = 0;
    for (int count = allByLayerId.Count; index < count; ++index)
    {
      MerlinTrialsMainInfo merlinTrialsMainInfo1 = allByLayerId[index];
      int internalId = merlinTrialsMainInfo1.internalId;
      string cityName = string.Format("castleItem_{0}_{1}", (object) merlinTrialsMainInfo1.coordinateX, (object) merlinTrialsMainInfo1.coordinateY);
      this.UpdateCityItem(internalId, cityName);
      string chestName = string.Format("chestItem_{0}", (object) (index + 1));
      this.UpdateChest(internalId, chestName);
      this.UpdateLine(string.Format("line_{0}_{1}", (object) (index + 1), (object) 0), merlinTrialsMainInfo1.coordinateY != 0 ? 45f : -45f);
      if (index < allByLayerId.Count - 1)
      {
        MerlinTrialsMainInfo merlinTrialsMainInfo2 = allByLayerId[index + 1];
        this.UpdateLine(string.Format("line_{0}_{1}", (object) (index + 1), (object) 1), merlinTrialsMainInfo2.coordinateY != 0 ? -45f : 45f);
      }
    }
  }

  private void UpdateCityItem(int battleId, string cityName)
  {
    this.AllocateItem(this._cityRoot.transform.FindChild(cityName).gameObject).FeedData((IComponentData) new MerlinTrialCityItemData()
    {
      battleId = battleId,
      onSelectAction = new System.Action<int>(this.OnSelectHandler),
      onFocusAction = new System.Action<int>(this.OnFocusHandler),
      onBattleFinishAction = new System.Action<int>(this.OnBattleFinished)
    });
  }

  private void UpdateMysteryShopTime()
  {
    int time = MerlinTrialsHelper.inst.GetMysteryShopEndTime() - NetServerTime.inst.ServerTimestamp;
    this._labelMysteryShopEndTime.text = Utils.FormatTime(time, true, false, true);
    if (time > 0)
      return;
    NGUITools.SetActive(this._mysteryRoot, false);
  }

  private void UpdateMysteryShopStatus()
  {
    NGUITools.SetActive(this._mysteryRoot, MerlinTrialsHelper.inst.ShouldShowMysteryShop());
  }

  private void OnSelectHandler(int battleId)
  {
    this.SelectCityItem(battleId);
  }

  private void SelectCityItem(int battleId)
  {
    Stack<MerlinTrialCityItem>.Enumerator enumerator = this._usedCityItems.GetEnumerator();
    while (enumerator.MoveNext())
    {
      MerlinTrialCityItem current = enumerator.Current;
      current.ShowCircleMenu(current.BattleId == battleId);
    }
  }

  private void OnFocusHandler(int battleId)
  {
    this.SelectCityItem(-1);
    this.ScrollToCity(battleId, new System.Action<int>(this.SelectCityItem), true);
  }

  private void ScrollToCity(int battleId, System.Action<int> finishCallback = null, bool smooth = false)
  {
    float x = this._scrollView.panel.GetWindowSize().x;
    float width = (float) this._bgTexture.width;
    float min = 0.0f;
    float max = width - x;
    float num1 = x / 2f;
    float num2 = num1;
    Stack<MerlinTrialCityItem>.Enumerator enumerator = this._usedCityItems.GetEnumerator();
    while (enumerator.MoveNext())
    {
      MerlinTrialCityItem current = enumerator.Current;
      if (current.BattleId == battleId)
      {
        float num3 = this._scrollView.transform.InverseTransformPoint(current.transform.position).x - num1;
        float num4 = 0.0f - this._scrollView.transform.localPosition.x;
        float num5 = Mathf.Clamp(num3 + num2, min, max);
        if (smooth)
        {
          SpringPanel.Begin(this._scrollView.gameObject, new Vector3(-num5, 0.0f, 0.0f), 15f).onFinished = (SpringPanel.OnFinished) (() =>
          {
            if (finishCallback == null)
              return;
            finishCallback(battleId);
          });
        }
        else
        {
          this._scrollView.MoveRelative(new Vector3(num4 - num5, 0.0f, 0.0f));
          if (finishCallback != null)
            finishCallback(battleId);
        }
      }
    }
  }

  private void OnBattleFinished(int battleId)
  {
    if (!MerlinTrialsHelper.inst.IsLastBattleOfLevel(battleId))
      return;
    this.UpdateArrow();
  }

  private void UpdateLine(string lineName, float angle)
  {
    this._lineRoot.transform.FindChild(lineName).eulerAngles = new Vector3(0.0f, 0.0f, angle);
  }

  private void UpdateChest(int battleId, string chestName)
  {
    this._chestRoot.transform.FindChild(chestName).gameObject.GetComponent<MerlinTrialChestItem>().FeedData((IComponentData) new MerlinTrialChestItemData()
    {
      battleId = battleId
    });
  }

  public void OnDetailClick()
  {
    UIManager.inst.OpenPopup("MerlinTrials/TrialDetailsPopup", (Popup.PopupParameter) new MerlinTrialsDetailPopup.Parameter()
    {
      index = this._day
    });
  }

  public void OnHistoryClick()
  {
    UIManager.inst.OpenPopup("MerlinTrials/MerlinTrialsHistoryPopup", (Popup.PopupParameter) null);
  }

  public void OnLeftClick()
  {
    if (this._curLevel <= this._minLevel)
      return;
    this.SwitchLevel(this._curLevel - 1);
  }

  public void OnRightClick()
  {
    if (this._curLevel >= this._maxLevel)
      return;
    if (this._curLevel >= MerlinTrialsHelper.inst.LatestLevelConfig(this._groupId).level)
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_complete_final_challenge"), (System.Action) null, 4f, false);
    else
      this.SwitchLevel(this._curLevel + 1);
  }

  private void SwitchLevel(int targetLevel)
  {
    if (this._curLevel == targetLevel)
      return;
    this._curLevel = targetLevel;
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
      this._scrollView.ResetPosition();
      this.UpdateArrow();
      this.UpdateUI(this._curLevel);
      this.ScrollToCity(MerlinTrialsHelper.inst.MaxAvaliableTrialConfig(this._groupId, this._curLevel).internalId, (System.Action<int>) null, false);
    }));
  }

  private void UpdateArrow()
  {
    this._leftArrow.gameObject.SetActive(this._curLevel > this._minLevel);
    this._rightArrow.gameObject.SetActive(this._curLevel < this._maxLevel);
    MerlinTrialsLayerInfo merlinTrialsLayerInfo = MerlinTrialsHelper.inst.LatestLevelConfig(this._groupId);
    this._rightArrow.isEnabled = this._curLevel < merlinTrialsLayerInfo.level;
    this._rightEffect.SetActive(this._curLevel < merlinTrialsLayerInfo.level);
    this._leftHint.SetActive(this._curLevel != this._minLevel && MerlinTrialsHelper.inst.HasUnclaimedChest(this._groupId, this._curLevel - 1));
    this._rightHint.SetActive(this._curLevel != this._maxLevel && MerlinTrialsHelper.inst.HasUnclaimedChest(this._groupId, this._curLevel + 1));
  }

  public void OnMysteryRewardClick()
  {
    MerlinTrialsHelper.inst.LatestAvaliableTrialConfig(this._groupId);
    UIManager.inst.OpenPopup("MerlinTrials/MerlinMysteryStorePopup", (Popup.PopupParameter) new MerlinMysteryStorePopup.Parameter()
    {
      merlinTrialsMainId = MerlinTrialsHelper.inst.GetMysteryShopBattleId()
    });
  }

  public void OnOtherAreaClick()
  {
    this.SelectCityItem(0);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int day;
  }
}
