﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarTargetListSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class AllianceWarTargetListSlot : MonoBehaviour
{
  private long uid;
  private Coordinate location;
  [SerializeField]
  private AllianceWarTargetListSlot.Panel panel;

  public void Setup(long uid)
  {
    UserData userData = DBManager.inst.DB_User.Get(uid);
    CityData cityData = DBManager.inst.DB_City.Get(uid);
    if (userData != null)
    {
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      this.panel.userName.text = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
      CustomIconLoader.Instance.requestCustomIcon(this.panel.portrait, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.portrait, userData.LordTitle, 1);
    }
    if (cityData == null)
      return;
    this.location = cityData.cityLocation;
    this.panel.location.text = this.location.ToString();
  }

  public void OnTargetSlotClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this.location.K,
      x = this.location.X,
      y = this.location.Y
    });
  }

  public void Reset()
  {
    this.panel.portrait = this.transform.Find("Portrait").GetComponent<UITexture>();
    this.panel.userName = this.transform.Find("name").GetComponent<UILabel>();
    this.panel.location = this.transform.Find("Coord/LocationText").GetComponent<UILabel>();
  }

  [Serializable]
  public class Panel
  {
    public UITexture portrait;
    public UILabel userName;
    public UILabel location;
  }
}
