﻿// Decompiled with JetBrains decompiler
// Type: ArtifactDetailsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ArtifactDetailsPopup : Popup
{
  public UITexture artifactFrame;
  public UITexture artifactTexture;
  public UILabel artifactName;
  public UILabel artifactSpecificDesc;
  public UILabel artifactTimesLimitedText;
  public UILabel artifactProtectedTime;
  public GameObject artifactProtectedTimeNode;
  public UILabel artifactAvaliableTime;
  public GameObject artifactAvaliableTimeNode;
  public GameObject specialEffectNode;
  public UIScrollView scrollView;
  public UITable table;
  public EquipmentBenefits benefitContent;
  private int _artifactId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ArtifactDetailsPopup.Parameter parameter = orgParam as ArtifactDetailsPopup.Parameter;
    if (parameter != null)
      this._artifactId = parameter.artifactId;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ShowArtifactDetails();
    this.ShowArtifactProtectedTime();
  }

  private void ShowArtifactDetails()
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactId);
    if (artifactInfo == null)
      return;
    if (artifactInfo.benefits != null)
      this.benefitContent.Init(artifactInfo.benefits);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.artifactTimesLimitedText.gameObject, false);
    if (artifactInfo.activeEffect == "magic_source_stone")
    {
      this.artifactSpecificDesc.text = artifactInfo.GetSpecialDescription((object) artifactInfo.deleteTypeParam, (object) (int) artifactInfo.activeEffectValue);
      this.artifactTimesLimitedText.text = ScriptLocalization.GetWithPara("artifact_temp_peace_shields_remaining", new Dictionary<string, string>()
      {
        {
          "0",
          string.Format("{0}/{1}", (object) (int) artifactInfo.activeEffectValue2, (object) (int) artifactInfo.activeEffectValue2)
        }
      }, true);
      NGUITools.SetActive(this.artifactTimesLimitedText.gameObject, true);
    }
    else
      this.artifactSpecificDesc.text = artifactInfo.Description;
    if (string.IsNullOrEmpty(artifactInfo.description))
      NGUITools.SetActive(this.specialEffectNode, false);
    if (artifactInfo.IsTimeLimitArtifact)
    {
      this.artifactName.text = "[98339E]" + artifactInfo.Name + "[-]";
      this.artifactAvaliableTime.text = ScriptLocalization.GetWithPara("artifact_temp_time_num", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatTime(artifactInfo.ArtifactLimitTime, false, false, true)
        }
      }, true);
      NGUITools.SetActive(this.artifactAvaliableTimeNode, true);
      NGUITools.SetActive(this.artifactProtectedTimeNode, false);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      this.artifactName.text = "[E1A31D]" + artifactInfo.Name + "[-]";
      ArtifactData artifactData = DBManager.inst.DB_Artifact.GetArtifactData(PlayerData.inst.userData.world_id, this._artifactId);
      if (artifactData == null || artifactData.ProtectedLeftTime <= 0)
        this.artifactName.transform.localPosition = new Vector3(this.artifactName.transform.localPosition.x, this.artifactName.transform.localPosition.y - 50f, 0.0f);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.NormalArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    Utils.ExecuteInSecs(0.05f, (System.Action) (() =>
    {
      this.table.repositionNow = true;
      this.table.Reposition();
      this.scrollView.ResetPosition();
    }));
  }

  private void ShowArtifactProtectedTime()
  {
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactId);
    if (artifactInfo == null || artifactInfo.IsTimeLimitArtifact)
      return;
    ArtifactData artifactData = DBManager.inst.DB_Artifact.GetArtifactData(PlayerData.inst.userData.world_id, this._artifactId);
    if (artifactData != null && artifactData.ProtectedLeftTime > 0)
    {
      this.artifactProtectedTime.text = Utils.FormatTime(artifactData.ProtectedLeftTime, true, false, true);
      NGUITools.SetActive(this.artifactProtectedTimeNode, true);
    }
    else
      NGUITools.SetActive(this.artifactProtectedTimeNode, false);
  }

  private void OnSecondHandler(int time)
  {
    this.ShowArtifactProtectedTime();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int artifactId;
  }
}
