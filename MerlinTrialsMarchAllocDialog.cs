﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsMarchAllocDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsMarchAllocDialog : Dialog
{
  private readonly Hashtable _troops = new Hashtable();
  public GameObject _mask;
  public GameObject _dragonCheckImage;
  public UILabel _dragonDescription;
  public GameObject _noDragonGo;
  public GameObject _dragonGo;
  public UITexture _dragonIcon;
  public UILabel _dragonLevel;
  public IconGroup _skillGroup;
  public UITable _troopTable;
  public MerlinTrialsTroopSlider _troopTemplate;
  public UILabel _troopAmount;
  public UILabel _troopPower;
  private long _totalCapacity;
  private long _totalPower;
  private int _battleId;
  private System.Action<int> onBattleFinishCallback;
  private Hashtable _battleResult;
  private bool _isMysteryShopExist;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    MerlinTrialsMarchAllocDialog.Parameter parameter = orgParam as MerlinTrialsMarchAllocDialog.Parameter;
    if (parameter == null)
      return;
    this._battleId = parameter.battleId;
    this.onBattleFinishCallback = parameter.onBattleFinishCallback;
    this._troopTemplate.gameObject.SetActive(false);
    MerlinTrialsHelper.inst.RequestCapacityInfo(this._battleId, new System.Action<object>(this.OnReqeustCapacityCallback));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    this._skillGroup.Clear();
  }

  public override void OnCloseButtonPress()
  {
    this.OnBackButtonPress();
  }

  public void OnMarchClick()
  {
    this._isMysteryShopExist = MerlinTrialsHelper.inst.ShouldShowMysteryShop();
    MerlinTrialsHelper.inst.RequestCombat(this._battleId, new System.Action<object>(this.OnBattleResult));
  }

  private void OnBattleResult(object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "status"))
    {
      int result;
      int.TryParse(hashtable[(object) "status"].ToString(), out result);
      if (result > 0)
      {
        UIManager.inst.toast.Show(Utils.XLAT("toast_trial_not_open"), (System.Action) null, 4f, false);
        return;
      }
    }
    if (hashtable.ContainsKey((object) "winOrLost") && (bool) hashtable[(object) "winOrLost"])
    {
      MerlinTrialsHelper.inst.UpdateLastNumber(this._battleId);
      if (this.onBattleFinishCallback != null)
        this.onBattleFinishCallback(this._battleId);
    }
    this._battleResult = hashtable;
    this.ShowAnimation();
  }

  private void ShowAnimation()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    DragonData dragonData = PlayerData.inst.dragonData;
    int num = 0;
    DragonData.Tendency tendency = DragonData.Tendency.normal;
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
    if (ConfigManager.inst.DB_MerlinTrialsGroup.Get(merlinTrialsMainInfo.trialId).useDragon > 0 && dragonData != null)
    {
      num = dragonData.Level;
      tendency = dragonData.tendency;
    }
    AttackShowPara attackShowPara = new AttackShowPara()
    {
      dragon = new DB.DragonInfo()
      {
        level = num,
        tendency = tendency
      },
      targetAllianceCode = -1,
      targetLevel = merlinTrialsMainInfo.cityLevel,
      targetName = merlinTrialsMainInfo.LocName,
      troops = this._troops
    };
    AttackShowManager.Instance.onAttackShowTerminate -= new System.Action(this.ShowResult);
    AttackShowManager.Instance.onAttackShowTerminate += new System.Action(this.ShowResult);
    UIManager.inst.OpenPopup("Report/AnniversaryAttackShowPopup", (Popup.PopupParameter) new AnniversaryAttackShowPopup.Parameter()
    {
      para = attackShowPara
    });
  }

  private void ShowResult()
  {
    this.ShowWarReport();
  }

  private void ShowWarReport()
  {
    if (this._battleResult == null)
      return;
    bool result = false;
    if (this._battleResult.ContainsKey((object) "winOrLost"))
      bool.TryParse(this._battleResult[(object) "winOrLost"].ToString(), out result);
    if (this._battleResult.ContainsKey((object) "detail") && !UIManager.inst.IsPopupExist("MerlinTrials/MerlinTrialsWarReportPopup"))
      UIManager.inst.OpenPopup("MerlinTrials/MerlinTrialsWarReportPopup", (Popup.PopupParameter) new MerlinTrialsWarReportPopup.Parameter()
      {
        winWar = result,
        battleId = this._battleId,
        openAfterBattle = true,
        isMysteryShopExist = this._isMysteryShopExist,
        mailData = (this._battleResult[(object) "detail"] as Hashtable)
      });
    this._battleResult = (Hashtable) null;
  }

  private void OnReqeustCapacityCallback(object orgData)
  {
    Hashtable hashtable1 = orgData as Hashtable;
    if (hashtable1 == null)
      return;
    if (hashtable1.ContainsKey((object) "troops"))
    {
      Hashtable hashtable2 = hashtable1[(object) "troops"] as Hashtable;
      this._totalCapacity = 0L;
      IDictionaryEnumerator enumerator = hashtable2.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result1;
        int result2;
        if (int.TryParse(enumerator.Key as string, out result1) && int.TryParse(enumerator.Value as string, out result2))
        {
          this._troops.Add((object) ConfigManager.inst.DB_Unit_Statistics.GetData(result1).ID, (object) result2);
          this._totalCapacity += (long) result2;
        }
      }
    }
    if (hashtable1.ContainsKey((object) "fight"))
      long.TryParse(hashtable1[(object) "fight"] as string, out this._totalPower);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this._troopAmount.text = this._totalCapacity.ToString();
    this._troopPower.text = this._totalPower.ToString();
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
    if (merlinTrialsMainInfo == null)
      return;
    MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get(merlinTrialsMainInfo.trialId);
    if (merlinTrialsGroupInfo == null)
      return;
    this.UpdateDragon(merlinTrialsGroupInfo.useDragon > 0);
    this.UpdateTroops(merlinTrialsMainInfo.playerUnitId);
  }

  private void UpdateDragon(bool canUseDragon)
  {
    this._mask.SetActive(!canUseDragon);
    this._dragonCheckImage.SetActive(canUseDragon);
    DragonData dragonData = PlayerData.inst.dragonData;
    this._dragonGo.SetActive(dragonData != null);
    this._noDragonGo.SetActive(dragonData == null);
    this._dragonDescription.text = !canUseDragon ? Utils.XLAT("trial_challenge_no_dragon_today") : Utils.XLAT("march_dragon_power_name");
    int character = 0;
    int level = 1;
    long uid = PlayerData.inst.uid;
    if (dragonData != null)
    {
      character = (int) dragonData.tendency;
      level = dragonData.Level;
      uid = dragonData.Uid;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this._dragonIcon, DragonUtils.GetDragonIconPath(character, level), (System.Action<bool>) null, true, false, string.Empty);
    this._dragonLevel.text = level.ToString();
    this._skillGroup.CreateIcon(DragonUtils.GetSkillIconData("attack", uid, false));
  }

  private void UpdateTroops(int playerUnitId)
  {
    MerlinTrialsPlayerUnitInfo trialsPlayerUnitInfo = ConfigManager.inst.DB_MerlinTrialsPlayerUnit.Get(playerUnitId);
    if (trialsPlayerUnitInfo == null)
      return;
    List<UnitGroup.UnitRecord> unitList = trialsPlayerUnitInfo.units.GetUnitList();
    UIUtils.ClearTable(this._troopTable);
    int index = 0;
    for (int count = unitList.Count; index < count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this._troopTable.gameObject, this._troopTemplate.gameObject);
      gameObject.SetActive(true);
      MerlinTrialsTroopSlider component = gameObject.GetComponent<MerlinTrialsTroopSlider>();
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(unitList[index].internalId);
      int troop = (int) this._troops[(object) data.ID];
      this.UpdateTroopItem(component, data, troop);
    }
    this._troopTable.Reposition();
  }

  private void UpdateTroopItem(MerlinTrialsTroopSlider troopItem, Unit_StatisticsInfo unitInfo, int amount)
  {
    troopItem.isEnable = false;
    troopItem.troopMaxCount = amount;
    troopItem.troopCount = amount;
    troopItem.troopInfo = unitInfo;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int battleId;
    public System.Action<int> onBattleFinishCallback;
  }
}
