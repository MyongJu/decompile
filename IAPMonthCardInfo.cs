﻿// Decompiled with JetBrains decompiler
// Type: IAPMonthCardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPMonthCardInfo
{
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "days")]
  public int days;
  [Config(Name = "prosperity")]
  public int prosperity;
  [Config(Name = "description")]
  public string description;

  public string productId
  {
    get
    {
      IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, this.id);
      if (iapPlatformInfo != null)
        return iapPlatformInfo.product_id;
      return string.Empty;
    }
  }
}
