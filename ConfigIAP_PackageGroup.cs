﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_PackageGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_PackageGroup
{
  private Dictionary<string, IAPPackageGroupInfo> m_DataByID;
  private Dictionary<int, IAPPackageGroupInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPPackageGroupInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPPackageGroupInfo Get(string id)
  {
    IAPPackageGroupInfo packageGroupInfo;
    this.m_DataByID.TryGetValue(id, out packageGroupInfo);
    return packageGroupInfo;
  }

  public IAPPackageGroupInfo Get(int internalID)
  {
    IAPPackageGroupInfo packageGroupInfo;
    this.m_DataByInternalID.TryGetValue(internalID, out packageGroupInfo);
    return packageGroupInfo;
  }

  public void Clear()
  {
    this.m_DataByID = (Dictionary<string, IAPPackageGroupInfo>) null;
    this.m_DataByInternalID = (Dictionary<int, IAPPackageGroupInfo>) null;
  }
}
