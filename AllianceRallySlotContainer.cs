﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallySlotContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceRallySlotContainer : MonoBehaviour
{
  private List<AllianceRallySlotItem> items = new List<AllianceRallySlotItem>();
  private Dictionary<long, AllianceRallySlotItem> itemsDic = new Dictionary<long, AllianceRallySlotItem>();
  private Queue<AllianceRallySlotItem> emptyItems = new Queue<AllianceRallySlotItem>();
  public AllianceRallySlotItem itemPrefab;
  public UIScrollView scroll;
  public UITable tabel;
  private bool isDestroy;
  private bool needRefresh;

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  private RallyInfoData Data { get; set; }

  public void SetData(RallyInfoData data)
  {
    this.Data = data;
    this.needRefresh = false;
    if (this.isDestroy)
      return;
    this.Clear();
    List<MarchData> marchList = this.Data.MarchList;
    if (!this.Data.IsDefense)
    {
      int index1 = 0;
      for (int index2 = 0; index2 < marchList.Count; ++index2)
      {
        ++index1;
        AllianceRallySlotItem allianceRallySlotItem = this.GetItem();
        allianceRallySlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
        allianceRallySlotItem.SetData(index1, marchList[index2].marchId, this.Data);
        this.items.Add(allianceRallySlotItem);
        this.itemsDic.Add(marchList[index2].marchId, allianceRallySlotItem);
      }
      int emptySlot = this.Data.EmptySlot;
      for (int index2 = 0; index2 < emptySlot; ++index2)
      {
        ++index1;
        AllianceRallySlotItem allianceRallySlotItem = this.GetItem();
        allianceRallySlotItem.Empty(index1, this.Data);
        this.items.Add(allianceRallySlotItem);
        this.emptyItems.Enqueue(allianceRallySlotItem);
      }
      AllianceRallySlotItem allianceRallySlotItem1 = this.GetItem();
      int index3 = index1 + 1;
      allianceRallySlotItem1.Lock(index3, this.Data);
      this.items.Add(allianceRallySlotItem1);
    }
    else
    {
      marchList.Sort(new Comparison<MarchData>(this.CompareByTime));
      for (int index = 0; index < marchList.Count; ++index)
      {
        AllianceRallySlotItem allianceRallySlotItem = this.GetItem();
        this.items.Add(allianceRallySlotItem);
        allianceRallySlotItem.OnOpenCallBackDelegate = new System.Action(this.OnOpenHandler);
        allianceRallySlotItem.SetData(index + 1, marchList[index].marchId, this.Data);
      }
    }
    this.tabel.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scroll.ResetPosition()));
    this.AddEventHandler();
  }

  private int CompareByTime(MarchData a, MarchData b)
  {
    return a.startTime.CompareTo(b.startTime);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.RefreshUI);
    DBManager.inst.DB_Rally.onRallyDataChanged += new System.Action<long>(this.OnMarchDataChanged);
    DBManager.inst.DB_Rally.onRallyTargetDataRemove += new System.Action<long>(this.OnRallyDataRemove);
  }

  private void OnRallyDataRemove(long rallyId)
  {
    if (rallyId != this.Data.DataID)
      return;
    this.RemoveEventHandler();
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void Update()
  {
    if (!this.needRefresh)
      return;
    this.Data.Reset();
    this.SetData(this.Data);
  }

  private void RallyDataChanged(long rallyId)
  {
    if (rallyId != this.Data.DataID)
      return;
    this.Data.Refresh();
    this.SetData(this.Data);
  }

  private void OnMarchDataChanged(long rally_id)
  {
    if (this.Data.DataID != rally_id)
      return;
    this.needRefresh = true;
  }

  private void RefreshUI(int time)
  {
    for (int index = 0; index < this.items.Count; ++index)
      this.items[index].Refresh();
  }

  private void OnDisable()
  {
    this.RemoveEventHandler();
  }

  private void RemoveEventHandler()
  {
    if (!GameEngine.IsReady())
      return;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshUI);
    DBManager.inst.DB_Rally.onRallyDataChanged -= new System.Action<long>(this.OnMarchDataChanged);
    DBManager.inst.DB_Rally.onRallyTargetDataRemove -= new System.Action<long>(this.OnRallyDataRemove);
  }

  private void OnOpenHandler()
  {
    this.tabel.repositionNow = true;
    this.tabel.Reposition();
  }

  private AllianceRallySlotItem GetEmptyItem()
  {
    if (this.emptyItems.Count > 0)
      return this.emptyItems.Dequeue();
    return (AllianceRallySlotItem) null;
  }

  private AllianceRallySlotItem GetItem()
  {
    GameObject go = Utils.DuplicateGOB(this.itemPrefab.gameObject, this.tabel.transform);
    AllianceRallySlotItem component = go.GetComponent<AllianceRallySlotItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public void Clear()
  {
    this.itemsDic.Clear();
    this.RemoveEventHandler();
    for (int index = 0; index < this.items.Count; ++index)
    {
      this.items[index].Clear();
      NGUITools.SetActive(this.items[index].gameObject, false);
      this.items[index].transform.parent = this.transform;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.items[index]);
    }
    this.items.Clear();
  }
}
