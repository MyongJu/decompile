﻿// Decompiled with JetBrains decompiler
// Type: GuardVFXHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GuardVFXHelper
{
  public static Vector3 ParseVector3(object orgData)
  {
    Vector3 zero = Vector3.zero;
    ArrayList arrayList = orgData as ArrayList;
    float.TryParse(arrayList[0].ToString(), out zero.x);
    float.TryParse(arrayList[1].ToString(), out zero.y);
    float.TryParse(arrayList[2].ToString(), out zero.z);
    return zero;
  }

  public static GameObject GetAttachPoint(GameObject go, string xpath)
  {
    if (string.IsNullOrEmpty(xpath))
      return go;
    string[] strArray = xpath.Split('/');
    Queue<string> queue = new Queue<string>();
    for (int index = 0; index < strArray.Length; ++index)
      queue.Enqueue(strArray[index]);
    return GuardVFXHelper.GetAttachPoint(go, queue);
  }

  public static void PlayVFX(string URI, Vector3 position, Vector3 rotation, Vector3 scale, Transform transform)
  {
    SimpleVfx simpleVfx = VfxManager.Instance.Create(URI) as SimpleVfx;
    if ((bool) ((Object) simpleVfx))
    {
      simpleVfx.offset = position;
      simpleVfx.rotation = rotation;
      simpleVfx.scale = scale;
      simpleVfx.Play(transform);
      simpleVfx.Attach(transform, position, rotation, scale);
    }
    else
      D.warn((object) "[GuardVFXHelper]PlayVFX: a SimpleVfx should be attached. uri = {0}", (object) URI);
  }

  private static GameObject GetAttachPoint(GameObject go, Queue<string> queue)
  {
    if (queue.Count == 0)
      return go;
    string str = queue.Dequeue();
    Transform transform = go.transform;
    int childCount = transform.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      Transform child = transform.GetChild(index);
      if (child.gameObject.name.Equals(str))
        return GuardVFXHelper.GetAttachPoint(child.gameObject, queue);
    }
    return (GameObject) null;
  }
}
