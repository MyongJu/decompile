﻿// Decompiled with JetBrains decompiler
// Type: LookupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class LookupInfo
{
  public const string RESOURCE_FOOD = "food";
  public const string RESOURCE_WOOD = "wood";
  public const string RESOURCE_ORE = "ore";
  public const string RESOURCE_SILVER = "silver";
  public const string HERO_XP = "xp";
  public const string PLAYER_CURRENCY = "currency";
  private int _internalID;
  private string _ID;
  private string _name;
  private string _image;
  private LookupInfo.Type _type;

  public LookupInfo(int internalID, string ID, LookupInfo.Type type, string locID, string image)
  {
    this._internalID = internalID;
    this._ID = ID;
    this._type = type;
    this._name = locID == null || locID.Length == 0 ? string.Empty : ScriptLocalization.Get(locID + nameof (_name), true);
    this._image = image == null || image.Length == 0 ? "icon_missing" : image;
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._ID;
    }
  }

  public LookupInfo.Type type
  {
    get
    {
      return this._type;
    }
  }

  public string Name
  {
    get
    {
      return this._name;
    }
  }

  public string Image
  {
    get
    {
      return this._image;
    }
  }

  public static LookupInfo.Type TryParseLookupType(string type)
  {
    if (type == null || type.Length <= 0)
      return LookupInfo.Type.Invalid;
    string key = type;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (LookupInfo.\u003C\u003Ef__switch\u0024map46 == null)
      {
        // ISSUE: reference to a compiler-generated field
        LookupInfo.\u003C\u003Ef__switch\u0024map46 = new Dictionary<string, int>(5)
        {
          {
            "none",
            0
          },
          {
            "resource",
            1
          },
          {
            "unit_class",
            2
          },
          {
            "building_class",
            3
          },
          {
            "property",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (LookupInfo.\u003C\u003Ef__switch\u0024map46.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return LookupInfo.Type.None;
          case 1:
            return LookupInfo.Type.Resource;
          case 2:
            return LookupInfo.Type.UnitClass;
          case 3:
            return LookupInfo.Type.BuildingClass;
          case 4:
            return LookupInfo.Type.Property;
        }
      }
    }
    return LookupInfo.Type.Invalid;
  }

  public enum Type
  {
    Invalid = -1,
    None = 0,
    Resource = 1,
    UnitClass = 2,
    BuildingClass = 3,
    Property = 4,
  }
}
