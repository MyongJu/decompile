﻿// Decompiled with JetBrains decompiler
// Type: DungeonFloorPickItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class DungeonFloorPickItemRenderer : MonoBehaviour
{
  public UILabel[] dungeonFloorDescs;
  public UISprite[] dungeonFloorFrames;
  public UIToggle dungeonFloorPickToggle;
  public UILabel canPlunder;
  public UILabel cannotPlunder;
  public System.Action<int, bool> onDungeonFloorPicked;
  private int _dungeonFloor;

  public void SetData(int dungeonFloor)
  {
    this._dungeonFloor = dungeonFloor;
    this.UpdateUI();
  }

  public void UpdateFloorPickStatus(bool floorChecked)
  {
    this.dungeonFloorPickToggle.value = floorChecked;
    this.UpdateUI();
  }

  public void OnFloorPickToggleChanged()
  {
    this.UpdateUI();
    if (this.onDungeonFloorPicked == null)
      return;
    this.onDungeonFloorPicked(this._dungeonFloor, this.dungeonFloorPickToggle.value);
  }

  private void UpdateUI()
  {
    string withPara = ScriptLocalization.GetWithPara("dragon_knight_dungeon_floor_num", new Dictionary<string, string>()
    {
      {
        "0",
        this._dungeonFloor.ToString()
      }
    }, true);
    for (int index = 0; index < this.dungeonFloorDescs.Length; ++index)
    {
      this.dungeonFloorDescs[index].text = withPara;
      NGUITools.SetActive(this.dungeonFloorDescs[index].gameObject, false);
      NGUITools.SetActive(this.dungeonFloorFrames[index].gameObject, false);
    }
    int index1 = !this.dungeonFloorPickToggle.value ? 0 : 1;
    NGUITools.SetActive(this.dungeonFloorDescs[index1].gameObject, true);
    NGUITools.SetActive(this.dungeonFloorFrames[index1].gameObject, true);
    bool flag = this._dungeonFloor <= DragonKnightUtils.MaxRaidLevel;
    this.canPlunder.gameObject.SetActive(flag);
    this.cannotPlunder.gameObject.SetActive(!flag);
  }
}
