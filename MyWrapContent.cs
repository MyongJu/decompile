﻿// Decompiled with JetBrains decompiler
// Type: MyWrapContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MyWrapContent : UIWrapContent
{
  public override void WrapContent()
  {
    float num1 = (float) (this.itemSize * this.mChildren.Count) * 0.5f;
    Vector3[] worldCorners = this.mPanel.worldCorners;
    for (int index = 0; index < 4; ++index)
    {
      Vector3 vector3 = this.mTrans.InverseTransformPoint(worldCorners[index]);
      worldCorners[index] = vector3;
    }
    Vector3 vector3_1 = Vector3.Lerp(worldCorners[0], worldCorners[2], 0.5f);
    bool flag = true;
    float num2 = num1 * 2f;
    if (this.mHorizontal)
    {
      float num3 = worldCorners[0].x - (float) this.itemSize;
      float num4 = worldCorners[2].x + (float) this.itemSize;
      int index = 0;
      for (int count = this.mChildren.Count; index < count; ++index)
      {
        Transform mChild = this.mChildren[index];
        float num5 = mChild.localPosition.x - vector3_1.x;
        if ((double) num5 < -(double) num1)
        {
          Vector3 localPosition = mChild.localPosition;
          localPosition.x += num2;
          num5 = localPosition.x - vector3_1.x;
          int num6 = Mathf.RoundToInt(localPosition.x / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
          {
            mChild.localPosition = localPosition;
            this.UpdateItem(mChild, index);
          }
          else
            flag = false;
        }
        else if ((double) num5 > (double) num1)
        {
          Vector3 localPosition = mChild.localPosition;
          localPosition.x -= num2;
          num5 = localPosition.x - vector3_1.x;
          int num6 = Mathf.RoundToInt(localPosition.x / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
          {
            mChild.localPosition = localPosition;
            this.UpdateItem(mChild, index);
          }
          else
            flag = false;
        }
        else if (this.mFirstTime)
          this.UpdateItem(mChild, index);
        if (this.cullContent)
        {
          float num7 = num5 + (this.mPanel.clipOffset.x - this.mTrans.localPosition.x);
        }
      }
    }
    else
    {
      float num3 = worldCorners[0].y - (float) this.itemSize;
      float num4 = worldCorners[2].y + (float) this.itemSize;
      int index = 0;
      for (int count = this.mChildren.Count; index < count; ++index)
      {
        Transform mChild = this.mChildren[index];
        float num5 = mChild.localPosition.y - vector3_1.y;
        if ((double) num5 < -(double) num1)
        {
          Vector3 localPosition = mChild.localPosition;
          localPosition.y += num2;
          num5 = localPosition.y - vector3_1.y;
          int num6 = Mathf.RoundToInt(localPosition.y / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
          {
            mChild.localPosition = localPosition;
            this.UpdateItem(mChild, index);
          }
          else
            flag = false;
        }
        else if ((double) num5 > (double) num1)
        {
          Vector3 localPosition = mChild.localPosition;
          localPosition.y -= num2;
          num5 = localPosition.y - vector3_1.y;
          int num6 = Mathf.RoundToInt(localPosition.y / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
          {
            mChild.localPosition = localPosition;
            this.UpdateItem(mChild, index);
          }
          else
            flag = false;
        }
        else if (this.mFirstTime)
          this.UpdateItem(mChild, index);
        if (this.cullContent)
        {
          float num7 = num5 + (this.mPanel.clipOffset.y - this.mTrans.localPosition.y);
        }
      }
    }
    this.mScroll.restrictWithinPanel = !flag;
  }
}
