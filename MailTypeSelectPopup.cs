﻿// Decompiled with JetBrains decompiler
// Type: MailTypeSelectPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MailTypeSelectPopup : Popup
{
  public UIButton sendMail;
  public UIButton sendModMail;
  public UIButton close;
  [Header("Localize")]
  public UILabel titleTxt;
  public UILabel helpTxt;
  public UILabel modTxt;
  private bool inited;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    EventDelegate.Add(this.close.onClick, new EventDelegate.Callback(this.OnClosePopup));
    EventDelegate.Add(this.sendMail.onClick, new EventDelegate.Callback(this.OnSendMail));
    EventDelegate.Add(this.sendModMail.onClick, new EventDelegate.Callback(this.OnSendModMail));
    if (this.inited)
      return;
    this._OnceInit();
    this.inited = true;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    EventDelegate.Remove(this.close.onClick, new EventDelegate.Callback(this.OnClosePopup));
    EventDelegate.Remove(this.sendMail.onClick, new EventDelegate.Callback(this.OnSendMail));
    EventDelegate.Remove(this.sendModMail.onClick, new EventDelegate.Callback(this.OnSendModMail));
  }

  private void OnSendModMail()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = (List<string>) null,
      allianceID = 0L,
      isModMail = true
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnSendMail()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnClosePopup()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  private void _OnceInit()
  {
  }
}
