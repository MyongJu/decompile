﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRewardsGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceBossRewardsGroup : MonoBehaviour
{
  public Icon icon;
  public IconListComponent ilc;

  public void SeedData(AllianceBossRewardsGroup.Data d)
  {
    this.icon.SetData((string) null, d.header);
    this.ilc.FeedData((IComponentData) d.ilcd);
  }

  public class Data
  {
    public string[] header;
    public IconListComponentData ilcd;
  }
}
