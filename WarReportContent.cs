﻿// Decompiled with JetBrains decompiler
// Type: WarReportContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class WarReportContent
{
  public const string iconpath = "Texture/BuildingConstruction/";
  public string k;
  public string x;
  public string y;
  public bool disastrous;
  public bool is_attacker;
  public int artifact;
  public Dictionary<string, string> resources;
  public DragonXP dragon_attr;
  public string winner_uid;
  public TroopLeader attacker;
  public TroopLeader defender;
  public List<Hashtable> legend;
  public List<TroopDetail> attacker_troop_detail;
  public List<TroopDetail> defender_troop_detail;

  public bool IsAttacker()
  {
    return this.is_attacker;
  }

  public bool IsWinner()
  {
    return !this.disastrous && this.winner_uid == (!this.IsAttacker() ? this.defender : this.attacker).uid;
  }

  public bool TargetIsMonster()
  {
    return "monster" == this.defender.name;
  }

  public void SummarizeResourceReward(ref List<IconData> res)
  {
    bool flag = this.IsWinner();
    string str1 = "+";
    if (!flag)
      str1 = "-";
    if (this.resources == null)
    {
      IconData iconData1 = new IconData("Texture/BuildingConstruction/food_icon", new string[1]
      {
        str1 + "0"
      });
      res.Add(iconData1);
      IconData iconData2 = new IconData("Texture/BuildingConstruction/wood_icon", new string[1]
      {
        str1 + "0"
      });
      res.Add(iconData2);
      IconData iconData3 = new IconData("Texture/BuildingConstruction/ore_icon", new string[1]
      {
        str1 + "0"
      });
      res.Add(iconData3);
      IconData iconData4 = new IconData("Texture/BuildingConstruction/silver_icon", new string[1]
      {
        str1 + "0"
      });
      res.Add(iconData4);
    }
    else
    {
      string s = "0";
      this.resources.TryGetValue("food", out s);
      string str2 = s != null ? Utils.FormatShortThousands(int.Parse(s)) : "0";
      IconData iconData1 = new IconData("Texture/BuildingConstruction/food_icon", new string[1]
      {
        str1 + str2
      });
      res.Add(iconData1);
      this.resources.TryGetValue("wood", out s);
      string str3 = s != null ? Utils.FormatShortThousands(int.Parse(s)) : "0";
      IconData iconData2 = new IconData("Texture/BuildingConstruction/wood_icon", new string[1]
      {
        str1 + str3
      });
      res.Add(iconData2);
      this.resources.TryGetValue("ore", out s);
      string str4 = s != null ? Utils.FormatShortThousands(int.Parse(s)) : "0";
      IconData iconData3 = new IconData("Texture/BuildingConstruction/ore_icon", new string[1]
      {
        str1 + str4
      });
      res.Add(iconData3);
      this.resources.TryGetValue("silver", out s);
      string str5 = s != null ? Utils.FormatShortThousands(int.Parse(s)) : "0";
      IconData iconData4 = new IconData("Texture/BuildingConstruction/silver_icon", new string[1]
      {
        str1 + str5
      });
      res.Add(iconData4);
    }
  }

  public void SummarizeAllTroopDetail(List<TroopDetail> tds, ref Dictionary<string, Dictionary<string, List<SoldierInfo.Data>>> allmytroopdetail)
  {
    if (tds == null)
      return;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        Dictionary<string, List<SoldierInfo.Data>> troopdetail = new Dictionary<string, List<SoldierInfo.Data>>();
        current.SummarizeTroopDetail(ref troopdetail);
        string key = current.name;
        if (current.lord_level != 0)
          key = key + " Lv." + (object) current.lord_level;
        allmytroopdetail.Add(key, troopdetail);
      }
    }
  }

  public void SummarizeAllDragonDetail(List<TroopDetail> tds, ref Dictionary<string, Hashtable> alldragondetail)
  {
    if (tds == null)
      return;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        string key = current.name;
        if (current.lord_level != 0)
          key = key + " Lv." + (object) current.lord_level;
        alldragondetail.Add(key, current.dragon);
      }
    }
  }

  public void SummarizeAllLegendDetail(List<TroopDetail> tds, ref Dictionary<string, List<Hashtable>> alllegenddetail)
  {
    if (tds == null)
      return;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        string key = current.name;
        if (current.lord_level != 0)
          key = key + " Lv." + (object) current.lord_level;
        alllegenddetail.Add(key, current.legends);
      }
    }
  }

  public long CalculateAllTroop(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroop();
      }
    }
    return num;
  }

  public long CalculateAllTroopSurvive(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopSurvived();
      }
    }
    return num;
  }

  public long CalculateAllTroopWound(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopWounded();
      }
    }
    return num;
  }

  public long CalculateAllTroopNotGood(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopLost();
        num += current.CalculateTroopWounded();
      }
    }
    return num;
  }

  public long CalculateAllTroopLost(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopLost();
      }
    }
    return num;
  }

  public long CalculateAllTroopKill(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculateTroopKill();
      }
    }
    return num;
  }

  public long CalculateAllPowerLost(List<TroopDetail> tds)
  {
    long num = 0;
    using (List<TroopDetail>.Enumerator enumerator = tds.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TroopDetail current = enumerator.Current;
        num += current.CalculatePowerLost();
      }
    }
    return num;
  }
}
