﻿// Decompiled with JetBrains decompiler
// Type: SpiritDecrease
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpiritDecrease : MonoBehaviour
{
  [SerializeField]
  private UILabel m_SpiritValue;

  public string spirit
  {
    set
    {
      if (!((Object) this.m_SpiritValue != (Object) null))
        return;
      this.m_SpiritValue.text = value;
    }
  }

  public void OnFinished()
  {
    Object.Destroy((Object) this.gameObject);
  }
}
