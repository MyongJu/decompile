﻿// Decompiled with JetBrains decompiler
// Type: ConfigActivityPriority
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigActivityPriority
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, ActivityPriorityInfo> datas;
  private Dictionary<int, ActivityPriorityInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ActivityPriorityInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public int GetPriorityStart(string key)
  {
    if (this.datas == null || this.datas.Count == 0)
      return 0;
    int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
    ActivityPriorityInfo activityPriorityInfo1 = (ActivityPriorityInfo) null;
    ActivityPriorityInfo activityPriorityInfo2 = (ActivityPriorityInfo) null;
    int num = 0;
    Dictionary<string, ActivityPriorityInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.internalId > num)
      {
        num = enumerator.Current.internalId;
        activityPriorityInfo2 = enumerator.Current;
      }
      if (mLevel >= enumerator.Current.MinLevel && mLevel <= enumerator.Current.MaxLevel)
      {
        activityPriorityInfo1 = enumerator.Current;
        break;
      }
    }
    if (activityPriorityInfo1 == null)
      activityPriorityInfo1 = activityPriorityInfo2;
    return activityPriorityInfo1.GetPriorityStart(key);
  }

  public int GetPriority(string key)
  {
    if (this.datas == null || this.datas.Count == 0)
      return 0;
    int mLevel = PlayerData.inst.CityData.mStronghold.mLevel;
    ActivityPriorityInfo activityPriorityInfo1 = (ActivityPriorityInfo) null;
    ActivityPriorityInfo activityPriorityInfo2 = (ActivityPriorityInfo) null;
    int num = 0;
    Dictionary<string, ActivityPriorityInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.internalId > num)
      {
        num = enumerator.Current.internalId;
        activityPriorityInfo2 = enumerator.Current;
      }
      if (mLevel >= enumerator.Current.MinLevel && mLevel <= enumerator.Current.MaxLevel)
      {
        activityPriorityInfo1 = enumerator.Current;
        break;
      }
    }
    if (activityPriorityInfo1 == null)
      activityPriorityInfo1 = activityPriorityInfo2;
    return activityPriorityInfo1.GetPriority(key);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ActivityPriorityInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ActivityPriorityInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public ActivityPriorityInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (ActivityPriorityInfo) null;
  }

  public ActivityPriorityInfo GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ActivityPriorityInfo) null;
  }
}
