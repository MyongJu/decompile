﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Fortress_Rally_Attack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class AllianceWar_MainDlg_Fortress_Rally_Attack : AllianceWar_MainDlg_Rally_Slot
{
  protected override void SetupBelowList(RallyData rallyData, long uid, long allianceId, int memberCount)
  {
    base.SetupBelowList(rallyData, uid, allianceId, memberCount);
    if (rallyData != null)
    {
      if (DBManager.inst.DB_AllianceFortress.Get((long) rallyData.fortressId) != null)
      {
        AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get((long) rallyData.fortressId);
        if (allianceFortressData != null)
        {
          AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(allianceFortressData.ConfigId);
          AllianceWar_MainDlg_RallyList.param.ownerName = allianceFortressData == null || string.IsNullOrEmpty(allianceFortressData.Name) ? buildingStaticInfo.LocalName : allianceFortressData.Name;
        }
      }
      else
        AllianceWar_MainDlg_RallyList.param.ownerName = Utils.XLAT("alliance_altar_name");
    }
    AllianceWar_MainDlg_RallyList.param.memberCount = memberCount;
    AllianceWar_MainDlg_RallyList.param.canJoin = false;
    AllianceWar_MainDlg_RallyList.param.isAttack = false;
    AllianceWar_MainDlg_RallyList.param.rallyType = AllianceWar_MainDlg_RallyList.Params.RallyListType.FORTRESS;
    this.panel.belowList.Setup(AllianceWar_MainDlg_RallyList.param);
  }

  protected override void SetupTargetSlot(RallyData rallyData)
  {
    if (rallyData == null)
      return;
    this.panel.targetSlot.Setup(rallyData.rallyId, 80f, true);
  }
}
