﻿// Decompiled with JetBrains decompiler
// Type: InvAttachmentPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Item Attachment Point")]
public class InvAttachmentPoint : MonoBehaviour
{
  public InvBaseItem.Slot slot;
  private GameObject mPrefab;
  private GameObject mChild;

  public GameObject Attach(GameObject prefab)
  {
    if ((Object) this.mPrefab != (Object) prefab)
    {
      this.mPrefab = prefab;
      if ((Object) this.mChild != (Object) null)
        Object.Destroy((Object) this.mChild);
      if ((Object) this.mPrefab != (Object) null)
      {
        Transform transform1 = this.transform;
        this.mChild = Object.Instantiate((Object) this.mPrefab, transform1.position, transform1.rotation) as GameObject;
        Transform transform2 = this.mChild.transform;
        transform2.parent = transform1;
        transform2.localPosition = Vector3.zero;
        transform2.localRotation = Quaternion.identity;
        transform2.localScale = Vector3.one;
      }
    }
    return this.mChild;
  }
}
