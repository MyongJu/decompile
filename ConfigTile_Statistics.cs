﻿// Decompiled with JetBrains decompiler
// Type: ConfigTile_Statistics
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTile_Statistics
{
  public Dictionary<int, Tile_StatisticsInfo> datas;
  private Dictionary<string, int> dicByUniqueId;

  public ConfigTile_Statistics()
  {
    this.datas = new Dictionary<int, Tile_StatisticsInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "tile_id");
        int index3 = ConfigManager.GetIndex(inHeader, "loc_name_id");
        int index4 = ConfigManager.GetIndex(inHeader, "loc_description_id");
        int index5 = ConfigManager.GetIndex(inHeader, "image");
        int index6 = ConfigManager.GetIndex(inHeader, "level");
        int index7 = ConfigManager.GetIndex(inHeader, "resource_type");
        int index8 = ConfigManager.GetIndex(inHeader, "amount_stored");
        int index9 = ConfigManager.GetIndex(inHeader, "gather_rate");
        int index10 = ConfigManager.GetIndex(inHeader, "reward_id_1");
        int index11 = ConfigManager.GetIndex(inHeader, "reward_id_2");
        int index12 = ConfigManager.GetIndex(inHeader, "reward_id_3");
        int index13 = ConfigManager.GetIndex(inHeader, "reward_id_4");
        int index14 = ConfigManager.GetIndex(inHeader, "reward_id_5");
        int index15 = ConfigManager.GetIndex(inHeader, "reward_id_6");
        int index16 = ConfigManager.GetIndex(inHeader, "reward_id_7");
        int index17 = ConfigManager.GetIndex(inHeader, "reward_id_8");
        int index18 = ConfigManager.GetIndex(inHeader, "reward_id_9");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          Tile_StatisticsInfo data = new Tile_StatisticsInfo();
          if (int.TryParse(key.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              data.ID = ConfigManager.GetString(arr, index1);
              data.Tile_ID = ConfigManager.GetString(arr, index2);
              data.Tile_Name_LOC_ID = ConfigManager.GetString(arr, index3);
              data.Tile_Description_LOC_ID = ConfigManager.GetString(arr, index4);
              data.Tile_Image_ID = ConfigManager.GetString(arr, index5);
              data.Level = ConfigManager.GetInt(arr, index6);
              data.Resource_Type = ConfigManager.GetString(arr, index7);
              data.Amount_Stored = ConfigManager.GetInt(arr, index8);
              data.Gathering_Rate = ConfigManager.GetFloat(arr, index9);
              data.Reward_ID_1 = ConfigManager.GetString(arr, index10);
              data.Reward_ID_2 = ConfigManager.GetString(arr, index11);
              data.Reward_ID_3 = ConfigManager.GetString(arr, index12);
              data.Reward_ID_4 = ConfigManager.GetString(arr, index13);
              data.Reward_ID_5 = ConfigManager.GetString(arr, index14);
              data.Reward_ID_6 = ConfigManager.GetString(arr, index15);
              data.Reward_ID_7 = ConfigManager.GetString(arr, index16);
              data.Reward_ID_8 = ConfigManager.GetString(arr, index17);
              data.Reward_ID_9 = ConfigManager.GetString(arr, index18);
              this.PushData(data.internalId, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, Tile_StatisticsInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, Tile_StatisticsInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public Tile_StatisticsInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (Tile_StatisticsInfo) null;
  }

  public Tile_StatisticsInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (Tile_StatisticsInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }
}
