﻿// Decompiled with JetBrains decompiler
// Type: PVPResourceBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PVPResourceBanner : SpriteBanner
{
  private int m_Level;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    int level = tile.Level;
    if (this.m_Level == level)
      return;
    this.m_Level = level;
    this.Level = level.ToString();
  }
}
