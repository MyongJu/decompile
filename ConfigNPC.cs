﻿// Decompiled with JetBrains decompiler
// Type: ConfigNPC
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigNPC
{
  public Dictionary<long, NPC_Info> _uid2info = new Dictionary<long, NPC_Info>();
  public Dictionary<int, NPC_Info> _datas;

  public void BuildDB(object res)
  {
    ConfigParse configParse = new ConfigParse();
    configParse.Clear();
    configParse.Parse<NPC_Info, int>(res as Hashtable, "internalId", out this._datas);
    Dictionary<int, NPC_Info>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this._uid2info.ContainsKey(enumerator.Current.uid))
      {
        this._uid2info.Add(enumerator.Current.uid, enumerator.Current);
        int.TryParse(enumerator.Current.icon.Replace("player_portrait_icon_", string.Empty), out enumerator.Current.iconIndex);
      }
    }
  }

  public NPC_Info GetData(int internalId)
  {
    if (this._datas.ContainsKey(internalId))
      return this._datas[internalId];
    return (NPC_Info) null;
  }

  public NPC_Info GetDataByUid(long uid)
  {
    if (this._uid2info.ContainsKey(uid))
      return this._uid2info[uid];
    return (NPC_Info) null;
  }
}
