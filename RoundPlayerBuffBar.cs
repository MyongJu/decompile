﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerBuffBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RoundPlayerBuffBar : MonoBehaviour
{
  public const int MAX_BUFFS = 4;
  public RoundPlayerBuffSlot[] m_Slots;
  public GameObject m_Etc;

  public void SetBuffs(List<BattleBuff> buffs)
  {
    this.HideAll();
    for (int index = 0; index < buffs.Count && index < 4; ++index)
    {
      DragonKnightTalentBuffInfo data = ConfigManager.inst.DBDragonKnightTalentBuff.GetData(buffs[index].BuffId);
      this.m_Slots[index].SetData(data);
      this.m_Slots[index].SetActive(true);
    }
    this.m_Etc.SetActive(buffs.Count > 4);
  }

  public void HideAll()
  {
    for (int index = 0; index < 4; ++index)
      this.m_Slots[index].SetActive(false);
    this.m_Etc.SetActive(false);
  }
}
