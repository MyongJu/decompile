﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsRankSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTrialsRankSlot : MonoBehaviour
{
  private Color RANK_FIRST_COLOR = new Color(0.2509804f, 0.2117647f, 0.08627451f);
  private Color RANK_SECOND_COLOR = new Color(0.2666667f, 0.2666667f, 0.2666667f);
  private Color RANK_THIRD_COLOR = new Color(0.2509804f, 0.2313726f, 0.2f);
  private const string PREFIX_RANK_ICON = "icon_no";
  [SerializeField]
  private UITexture _playerIcon;
  [SerializeField]
  private UILabel _labelPlayerName;
  [SerializeField]
  private UILabel _labelCompleteNumber;
  [SerializeField]
  private UILabel _labelLostNumber;
  [SerializeField]
  private UISprite _spriteBkgFrame;
  [SerializeField]
  private UILabel _labelRank;
  [SerializeField]
  private UITexture _textureRank;
  private MerlinTrialsPayload.RankData _rankData;

  public void SetData(MerlinTrialsPayload.RankData rankData)
  {
    this._rankData = rankData;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._rankData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this._playerIcon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + this._rankData.portrait, this._rankData.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this._playerIcon, this._rankData.lordTitleId, 2);
    this._labelPlayerName.text = !string.IsNullOrEmpty(this._rankData.allianceTag) ? string.Format("({0}){1}.K{2}", (object) this._rankData.allianceTag, (object) this._rankData.name, (object) this._rankData.worldId) : string.Format("{0}.K{1}", (object) this._rankData.name, (object) this._rankData.worldId);
    this._labelPlayerName.text = this._rankData.FullName;
    this._labelCompleteNumber.text = Utils.FormatThousands(this._rankData.number.ToString());
    this._labelLostNumber.text = Utils.FormatThousands(this._rankData.lostedNumber.ToString());
    this._labelRank.text = this._rankData.rank.ToString();
    if (this._rankData.rank >= 1 && this._rankData.rank <= 3)
    {
      NGUITools.SetActive(this._textureRank.gameObject, true);
      NGUITools.SetActive(this._labelRank.gameObject, false);
      switch (this._rankData.rank)
      {
        case 1:
          this._spriteBkgFrame.color = this.RANK_FIRST_COLOR;
          break;
        case 2:
          this._spriteBkgFrame.color = this.RANK_SECOND_COLOR;
          break;
        case 3:
          this._spriteBkgFrame.color = this.RANK_THIRD_COLOR;
          break;
      }
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureRank, "Texture/LeaderboardIcons/icon_no" + (object) this._rankData.rank, (System.Action<bool>) null, true, true, string.Empty);
    }
    else
    {
      NGUITools.SetActive(this._textureRank.gameObject, false);
      NGUITools.SetActive(this._labelRank.gameObject, true);
    }
  }

  public void OnPlayerIconClick()
  {
    UIManager.inst.OpenPopup("LeaderBoard/LeaderBoardPlayerInfo", (Popup.PopupParameter) new LeaderBoardPlayerInfo.LeaderBoardPlayerInfoParameter()
    {
      data = this.ConstructRankData(this._rankData),
      playerID = this._rankData.uid,
      showKingdom = true
    });
  }

  private RankData ConstructRankData(MerlinTrialsPayload.RankData merlinRankData)
  {
    PlayerRankData playerRankData = new PlayerRankData();
    playerRankData.AllianceName = merlinRankData.allianceName;
    playerRankData.FullName = merlinRankData.FullName;
    playerRankData.KingdomId = (int) merlinRankData.worldId;
    playerRankData.Name = merlinRankData.Name;
    playerRankData.OrginName = merlinRankData.OriginName;
    playerRankData.Rank = merlinRankData.rank;
    playerRankData.Score = merlinRankData.lostedNumber;
    int result;
    if (int.TryParse(merlinRankData.allianceSymbolCode, out result))
      playerRankData.Symbol = result;
    playerRankData.Tag = merlinRankData.allianceTag;
    playerRankData.UID = merlinRankData.uid.ToString();
    playerRankData.Image = "Texture/Hero/Portrait_Icon/player_portrait_icon_" + merlinRankData.portrait;
    playerRankData.CustomIconUrl = merlinRankData.icon;
    playerRankData.LordTitleId = merlinRankData.lordTitleId;
    playerRankData.Power = merlinRankData.power;
    return (RankData) playerRankData;
  }
}
