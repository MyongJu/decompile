﻿// Decompiled with JetBrains decompiler
// Type: IapError.Error
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace IapError
{
  [Serializable]
  public class Error
  {
    public string requestId;
    public string errorCode;
    public string errorMessage;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Error] ");
      stringBuilder.Append("requestId: " + this.requestId + " ");
      stringBuilder.Append("errorCode: " + this.errorCode + " ");
      stringBuilder.Append("errorMessage: " + this.errorMessage + " ");
      return stringBuilder.ToString();
    }
  }
}
