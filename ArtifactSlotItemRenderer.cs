﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSlotItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class ArtifactSlotItemRenderer : MonoBehaviour
{
  public UITexture artifactTexture;
  public UILabel artifactName;
  public UILabel ownerName;
  public UILabel ownerLocation;
  public UILabel getMethodHint;
  public UISprite nameLineSprite;
  public UISprite viewIconSprite;
  public UISprite getMethodLineSprite;
  public UILabel ownerStatus;
  public GameObject noOwnerHintNode;
  public GameObject ownerDetailNode;
  private ArtifactDlg.ArtifactItemInfo _artifactItemInfo;
  private float _originNameWidth;
  private Vector3 _originViewIconPosition;

  public void SetData(ArtifactDlg.ArtifactItemInfo artifactItemInfo)
  {
    this._artifactItemInfo = artifactItemInfo;
    this._originNameWidth = this.artifactName.printedSize.x;
    this._originViewIconPosition = this.viewIconSprite.transform.localPosition;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._artifactItemInfo == null)
      return;
    ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._artifactItemInfo.artifactId);
    if (artifactInfo != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.artifactName.text = artifactInfo.Name;
    }
    if (this._artifactItemInfo.ownerUid > 0L)
    {
      this.ownerName.text = Utils.XLAT("id_bearer") + this._artifactItemInfo.ownerName;
      if (this._artifactItemInfo.mapX >= 0 && this._artifactItemInfo.mapY >= 0 && this._artifactItemInfo.isInHomeKingdom)
      {
        this.ownerLocation.text = string.Format("X: {0} Y: {1}", (object) this._artifactItemInfo.mapX, (object) this._artifactItemInfo.mapY);
        NGUITools.SetActive(this.ownerLocation.transform.parent.gameObject, true);
      }
      else
        NGUITools.SetActive(this.ownerLocation.transform.parent.gameObject, false);
    }
    else
    {
      this.getMethodHint.text = Utils.XLAT("artifact_how_to_obtain");
      this.getMethodLineSprite.width = this.getMethodHint.width;
    }
    float num = (float) this.artifactName.width - this._originNameWidth;
    this.nameLineSprite.width = this.artifactName.width * 2;
    this.viewIconSprite.transform.localPosition = new Vector3(this._originViewIconPosition.x + num, this._originViewIconPosition.y, this._originViewIconPosition.z);
    NGUITools.SetActive(this.noOwnerHintNode, this._artifactItemInfo.ownerUid <= 0L);
    NGUITools.SetActive(this.ownerDetailNode, this._artifactItemInfo.ownerUid > 0L);
    NGUITools.SetActive(this.ownerStatus.gameObject, !this._artifactItemInfo.isInHomeKingdom);
  }

  public void OnMoreDescriptionClickHandler()
  {
    UIManager.inst.OpenPopup("Artifact/ArtifactDescriptionPopup", (Popup.PopupParameter) new ArtifactDescriptionPopup.Parameter()
    {
      artifactId = this._artifactItemInfo.artifactId
    });
  }

  public void OnOwnerLocationClickHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = PlayerData.inst.userData.world_id,
      x = this._artifactItemInfo.mapX,
      y = this._artifactItemInfo.mapY
    });
  }

  public void OnArtifactNameClickHandler()
  {
    UIManager.inst.OpenPopup("Artifact/ArtifactDetailsPopup", (Popup.PopupParameter) new ArtifactDetailsPopup.Parameter()
    {
      artifactId = this._artifactItemInfo.artifactId
    });
  }
}
