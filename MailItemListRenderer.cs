﻿// Decompiled with JetBrains decompiler
// Type: MailItemListRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class MailItemListRenderer : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemCount;
  private System.Action<ConsumableItemData> m_Callback;
  private ItemStaticInfo m_ItemInfo;
  private ConsumableItemData m_ItemData;

  public void SetData(ConsumableItemData itemData, ItemStaticInfo itemInfo, System.Action<ConsumableItemData> callback)
  {
    this.m_ItemData = itemData;
    this.m_ItemInfo = itemInfo;
    this.m_Callback = callback;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ItemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemCount.text = itemData.quantity.ToString();
  }

  public ItemStaticInfo ItemInfo
  {
    get
    {
      return this.m_ItemInfo;
    }
  }

  public void OnClicked()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_ItemData);
  }
}
