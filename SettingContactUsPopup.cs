﻿// Decompiled with JetBrains decompiler
// Type: SettingContactUsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using UI;
using UnityEngine;

public class SettingContactUsPopup : Popup
{
  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOpenHelpShift()
  {
    if (Application.isEditor)
      return;
    FunplusHelpshift.Instance.ShowConversation();
  }
}
