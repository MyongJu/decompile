﻿// Decompiled with JetBrains decompiler
// Type: WorldMap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class WorldMap : MonoBehaviour
{
  public float tilegap = 400f;
  public float mapscale = 3f / 1000f;
  public float ratioW = 80f;
  public float ratioH = 40f;
  [HideInInspector]
  public List<WonderTile> tiles = new List<WonderTile>();
  public Dictionary<Vector2, WonderTile> tilescor = new Dictionary<Vector2, WonderTile>();
  public HashSet<string> linename = new HashSet<string>();
  private const int edge = 3;
  private const string pathpath = "Prefab/World/";
  private const string tilepath = "Prefab/World/WonderTile";
  private const string SIGNAL = "#";
  public Transform background;
  private TileCamera m_TileCamera;

  public void Init()
  {
    Vector3 vector3 = new Vector3(this.mapscale, this.mapscale, this.mapscale);
    this.transform.parent = (Transform) null;
    this.transform.localScale = vector3;
    WorldMapData.Instance.onDataInitFinished += new System.Action(this.InitWonderTiles);
  }

  public void InitCamera()
  {
    if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && (UnityEngine.Object) UIManager.inst.tileCamera != (UnityEngine.Object) null)
    {
      this.m_TileCamera = UIManager.inst.tileCamera;
      if ((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null)
      {
        this.m_TileCamera.ActiveKingdomOnly = false;
        Bounds bounds = this.background.GetComponent<Renderer>().bounds;
        float magnitude = bounds.extents.magnitude;
        this.m_TileCamera.Boundary = new Rect(new Vector2(bounds.center.x - magnitude * 4f, bounds.center.y - magnitude * 4f), new Vector2(magnitude * 8f, magnitude * 8f));
      }
    }
    if (MapUtils.IsPitWorld(PlayerData.inst.playerCityData.cityLocation.K))
      this.GotoK(PlayerData.inst.userData.world_id, false);
    else
      this.GotoK(PlayerData.inst.playerCityData.cityLocation.K, false);
  }

  private void OnDrawGizmos()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.m_TileCamera))
      return;
    Gizmos.color = Color.red;
    Gizmos.DrawLine((Vector3) this.m_TileCamera.Boundary.min, (Vector3) this.m_TileCamera.Boundary.max);
    Renderer component = this.background.GetComponent<Renderer>();
    Gizmos.DrawWireSphere(component.bounds.center, component.bounds.extents.magnitude);
  }

  public void InitWonderTiles()
  {
    this.tiles.Clear();
    this.tilescor.Clear();
    this.linename.Clear();
    this.GenerateTiles();
    float num = Mathf.Sqrt((float) WorldMapData.Instance.Wonderlist.Count) * (this.tilegap / 4f);
    this.background.localScale = new Vector3(num * 2f, 1f, num * 2f);
    this.background.GetComponent<MeshRenderer>().materials[0].mainTextureScale = new Vector2(-num / this.ratioW, -num / this.ratioH);
  }

  private void GenerateTiles()
  {
    for (int index1 = 0; index1 < WorldMapData.Instance.Wonderlist.Count; ++index1)
    {
      Vector2 wonderPos = WorldMapData.Instance.WonderPosList[index1];
      int num1 = (int) wonderPos.x % 3;
      if (num1 > 1)
        num1 -= 3;
      else if (num1 < -1)
        num1 += 3;
      int num2 = (int) wonderPos.y % 3;
      if (num2 > 1)
        num2 -= 3;
      else if (num2 < -1)
        num2 += 3;
      WonderTile component1 = (UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/World/WonderTile" + (object) num1 + "_" + (object) num2, (System.Type) null)) as GameObject).GetComponent<WonderTile>();
      component1.transform.parent = this.transform;
      component1.gameObject.SetActive(true);
      Vector2 vector2_1 = wonderPos * this.tilegap;
      component1.transform.localPosition = new Vector3(vector2_1.x, vector2_1.y);
      component1.transform.localScale = Vector3.one;
      component1.NormalIdInVector = new Vector2((float) num1, (float) num2);
      component1.Coord = wonderPos;
      WonderTileRender component2 = (UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/World/WonderTileRender", (System.Type) null)) as GameObject).GetComponent<WonderTileRender>();
      component2.seedData(WorldMapData.Instance.Wonderlist[index1]);
      GameObject child1 = Utils.FindChild(component1.gameObject, "RenderRoot");
      component2.transform.parent = child1.transform;
      component2.transform.localScale = Vector3.one;
      component2.transform.localPosition = Vector3.back;
      component2.onClicked += new System.Action<int>(this.OnWonderTileClicked);
      if (this.tiles.Count > 0)
      {
        Vector2 index2 = component1.Coord - this.tiles[this.tiles.Count - 1].Coord;
        string pathname1 = "Prefab/World/" + ((double) index2.x + (double) index2.y >= 0.0 ? this.tiles[this.tiles.Count - 1].NormalIdInString + "#" + component1.NormalIdInString : component1.NormalIdInString + "#" + this.tiles[this.tiles.Count - 1].NormalIdInString);
        string name1 = component1.NormalIdInString + "+" + this.tiles[this.tiles.Count - 1].NormalIdInString;
        GameObject child2 = Utils.FindChild(component1.gameObject, name1);
        this.GeneratePath(pathname1, child2);
        Vector2 dir = WorldMapData.Instance.dirMap[index2];
        Vector2 key = wonderPos + dir;
        if (this.tilescor.ContainsKey(key))
        {
          Vector2 vector2_2 = component1.Coord - key;
          string pathname2 = "Prefab/World/" + ((double) vector2_2.x + (double) vector2_2.y >= 0.0 ? this.tilescor[key].NormalIdInString + "#" + component1.NormalIdInString : component1.NormalIdInString + "#" + this.tilescor[key].NormalIdInString);
          string name2 = component1.NormalIdInString + "+" + this.tilescor[key].NormalIdInString;
          GameObject child3 = Utils.FindChild(component1.gameObject, name2);
          this.GeneratePath(pathname2, child3);
        }
      }
      this.tiles.Add(component1);
      this.tilescor.Add(wonderPos, component1);
    }
    this.InitCamera();
  }

  private void GeneratePath(string pathname, GameObject pathroot)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) pathroot)
      return;
    UnityEngine.Object @object = AssetManager.Instance.HandyLoad(pathname, (System.Type) null);
    if (!((UnityEngine.Object) null != @object))
      return;
    PrefabManagerEx.Instance.Spawn(@object as GameObject, pathroot.transform);
  }

  public void GotoK(int k, bool needsmooth = false)
  {
    this.m_TileCamera.SetTargetLookPosition(Utils.FindChild(this.tiles[k - 1].gameObject, "RenderRoot").transform.position, needsmooth);
  }

  private void OnDestroy()
  {
    WorldMapData.Instance.onDataInitFinished -= new System.Action(this.InitWonderTiles);
    WorldMapData.Instance.Dispose();
  }

  private void OnTap(TapGesture gesture)
  {
    RaycastHit hitInfo;
    if ((UnityEngine.Object) this.m_TileCamera == (UnityEngine.Object) null || (UnityEngine.Object) this.m_TileCamera.GetComponent<Camera>() == (UnityEngine.Object) null || !Physics.Raycast(this.m_TileCamera.GetComponent<Camera>().ScreenPointToRay((Vector3) gesture.Position), out hitInfo, float.PositiveInfinity, 1 << GameEngine.Instance.TileLayer))
      return;
    WonderTileRender component = hitInfo.collider.transform.GetComponent<WonderTileRender>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.OnClicked();
  }

  public void OnWonderTileClicked(int kid)
  {
    this.GotoK(kid, true);
    WorldSystem.Instance.ExitWorldSystem();
    PVPSystem.Instance.Show(true);
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.KINGDOM);
    PVPSystem.Instance.Map.GotoLocation(new Coordinate(kid, 640, 1274), false);
  }
}
