﻿// Decompiled with JetBrains decompiler
// Type: ActivityItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityItemRenderer : Icon
{
  public UILabel timeLabel;
  private int remainTime;
  public UILabel desc;
  public System.Action OnStartHandler;
  private ActivityBaseUIData basedata;
  public GameObject[] starts;
  public GameObject newFlag;

  public override void FeedData(IComponentData data)
  {
    base.FeedData(data);
    this.basedata = (this.data as IconData).Data as ActivityBaseUIData;
    if (this.starts != null)
    {
      int star = this.basedata.GetStar();
      for (int index = 0; index < this.starts.Length; ++index)
        this.starts[index].SetActive(false);
      for (int index = 0; index < star; ++index)
      {
        if (index < this.starts.Length)
          this.starts[index].SetActive(true);
      }
    }
    if ((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null)
      this.newFlag.SetActive(this.basedata.HadRedPoint());
    this.Process(0);
    this.RemoveEventHandler();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    if (this.basedata.Type != ActivityBaseUIData.ActivityType.AlianceWar)
      return;
    MessageHub.inst.GetPortByAction("ac_grouping_finished").AddEvent(new System.Action<object>(this.OnGroupingFinished));
  }

  private void OnGroupingFinished(object data)
  {
    if (this.basedata.Type != ActivityBaseUIData.ActivityType.AlianceWar)
      return;
    this.RequestData(new System.Action<bool, object>(this.OnRequestDataCallback), false);
  }

  private void RequestData(System.Action<bool, object> callback, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("AC:getActivityInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void OnRequestDataCallback(bool ret, object data)
  {
    if (!ret)
      return;
    AllianceWarActivityUIData basedata = this.basedata as AllianceWarActivityUIData;
    if (basedata == null)
      return;
    basedata.AllianceWarData.Decode(data as Hashtable);
  }

  private void Process(int time = 0)
  {
    Color color = Color.green;
    if (!this.basedata.IsStart())
      color = new Color(0.9921569f, 0.5372549f, 0.0f);
    switch (this.basedata.Type)
    {
      case ActivityBaseUIData.ActivityType.AllianceBoss:
        this.timeLabel.text = ActivityManager.Intance.GetPortalTimeStr();
        break;
      case ActivityBaseUIData.ActivityType.AlianceWar:
        AllianceWarActivityUIData basedata1 = this.basedata as AllianceWarActivityUIData;
        if (basedata1 == null)
          return;
        string locDescription = basedata1.GetLocDescription();
        int remainTime1 = this.basedata.GetRemainTime();
        if (remainTime1 < 0)
        {
          this.timeLabel.text = Utils.XLAT(locDescription);
          break;
        }
        this.timeLabel.text = ScriptLocalization.GetWithPara(locDescription, new Dictionary<string, string>()
        {
          {
            "0",
            Utils.FormatTime(remainTime1, true, true, true)
          }
        }, true);
        if (basedata1.AllianceWarData.GetAllianceWarState() == AllianceWarActivityData.AllianceWarState.RegisterFail)
        {
          color = new Color(0.9921569f, 0.5372549f, 0.0f);
          break;
        }
        break;
      case ActivityBaseUIData.ActivityType.Festival:
        this.timeLabel.text = Utils.XLAT("event_in_progress_notice");
        break;
      case ActivityBaseUIData.ActivityType.Pit:
        if (this.basedata.IsStart() && !this.basedata.IsFinish())
        {
          this.timeLabel.text = Utils.XLAT("event_fire_kingdom_in_progress");
          break;
        }
        this.timeLabel.text = ScriptLocalization.GetWithPara(this.basedata.TimePre, new Dictionary<string, string>()
        {
          {
            "0",
            Utils.FormatTime(this.basedata.GetRemainTime(), true, false, true)
          }
        }, true);
        break;
      default:
        int remainTime2 = this.basedata.GetRemainTime();
        if (this.basedata is RoundActivityUIData)
        {
          RoundActivityUIData basedata2 = this.basedata as RoundActivityUIData;
          if ((UnityEngine.Object) this.desc != (UnityEngine.Object) null)
            this.desc.text = basedata2.GetNormalDesc();
        }
        if (remainTime2 > 0)
        {
          this.timeLabel.text = ScriptLocalization.GetWithPara(this.basedata.TimePre, new Dictionary<string, string>()
          {
            {
              "1",
              Utils.FormatTime(remainTime2, true, true, true)
            }
          }, true);
          break;
        }
        if (this.basedata.IsFinish())
        {
          this.RemoveEventHandler();
          break;
        }
        break;
    }
    this.timeLabel.color = color;
    if (!((UnityEngine.Object) this.newFlag != (UnityEngine.Object) null))
      return;
    this.newFlag.SetActive(this.basedata.HadRedPoint());
  }

  public override void Dispose()
  {
    base.Dispose();
    this.RemoveEventHandler();
  }

  public new void OnClickHandler()
  {
    this.basedata.DisplayActivityDetail();
    this.basedata.ClearRedPont();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    if (this.basedata.Type != ActivityBaseUIData.ActivityType.AlianceWar)
      return;
    MessageHub.inst.GetPortByAction("ac_grouping_finished").RemoveEvent(new System.Action<object>(this.OnGroupingFinished));
  }
}
