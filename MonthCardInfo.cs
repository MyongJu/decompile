﻿// Decompiled with JetBrains decompiler
// Type: MonthCardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MonthCardInfo
{
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "level_req_min")]
  public int level_req_min;
  [Config(Name = "level_req_max")]
  public int level_req_max;
  [Config(Name = "reward_multi")]
  public int reward_multi;
  [Config(Name = "base_gold")]
  public int base_gold;
  [Config(CustomParse = true, Name = "BaseRewards", ParseFuncKey = "ParseComposeItems")]
  public Dictionary<long, int> rewards;
  [Config(Name = "prosperity_down")]
  public int prosperity_down;
  [Config(Name = "prosperity_down_no_card")]
  public int prosperity_down_no_card;
}
