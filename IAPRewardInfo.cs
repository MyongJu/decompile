﻿// Decompiled with JetBrains decompiler
// Type: IAPRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class IAPRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;
  [Config(Name = "lucky_archer_coin")]
  public int lucky_archer_coin;

  public List<Reward.RewardsValuePair> GetRewards()
  {
    List<Reward.RewardsValuePair> rewards = this.Rewards.GetRewards();
    if (this.lucky_archer_coin > 0)
    {
      int propsId = MarksmanPayload.Instance.TempMarksmanData.PropsId;
      if (propsId > 0)
        rewards.Add(new Reward.RewardsValuePair(propsId, this.lucky_archer_coin));
    }
    return rewards;
  }
}
