﻿// Decompiled with JetBrains decompiler
// Type: CollectionUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionUI : CityTouchCircleBtn, IRecycle
{
  protected CityCircleBtnPara mPara;
  private CollectionUI.CollectionType ct;

  public override void Open(Transform parent)
  {
    base.Open(parent);
    Vector3 localPosition = this.transform.localPosition;
    localPosition.z += -5f;
    this.transform.localPosition = localPosition;
    this.ChangeParentBCSts(false);
  }

  public override void Close()
  {
    base.Close();
    this.ChangeParentBCSts(true);
    this.gameObject.SetActive(false);
    this.transform.parent = (Transform) null;
    Object.Destroy((Object) this.gameObject);
  }

  public virtual void InitCollectionUI(CityCircleBtnPara para, CollectionUI.CollectionType ct)
  {
    this.mPara = para;
    this.AdjustAtlas(this.esPair[para.at]);
    this.useUS.spriteName = para.spritename;
    this.et.onClick.Clear();
    this.et.onClick.Add(para.ed);
    if (para.closeAfterClick)
      this.et.onClick.Add(new EventDelegate(new EventDelegate.Callback(((SceneUI) this).Close)));
    this.ct = ct;
    if (!((Object) null != (Object) para.attachment))
      return;
    GameObject gameObject = Utils.DuplicateGOB(para.attachment, this.transform);
    gameObject.name = "attatchGO";
    CityTouchCircleBtnAttachment component = gameObject.GetComponent<CityTouchCircleBtnAttachment>();
    if (!((Object) null != (Object) component))
      return;
    component.SeedData(para.attachmentpara);
  }

  public void SetSmallCollider()
  {
    BoxCollider component = this.GetComponent<BoxCollider>();
    component.center = new Vector3(0.0f, 260f, 0.0f);
    component.size = new Vector3(200f, 200f, 0.0f);
  }

  public CollectionUI.CollectionType CT
  {
    get
    {
      return this.ct;
    }
  }

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
  }

  protected override void Awake()
  {
    base.Awake();
  }

  private void Update()
  {
    this.ScaleInCity();
  }

  public enum CollectionType
  {
    TrainSoldier,
    TrainDefence,
    CollectionRes,
    IdleHint,
  }
}
