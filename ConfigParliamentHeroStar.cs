﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroStar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroStar
{
  private List<ParliamentHeroStarInfo> _parliamentHeroStarInfoList = new List<ParliamentHeroStarInfo>();
  private Dictionary<string, ParliamentHeroStarInfo> _datas;
  private Dictionary<int, ParliamentHeroStarInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroStarInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroStarInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentHeroStarInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroStarInfoList.Sort((Comparison<ParliamentHeroStarInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public List<ParliamentHeroStarInfo> GetParliamentHeroStarInfoList()
  {
    return this._parliamentHeroStarInfoList;
  }

  public ParliamentHeroStarInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroStarInfo) null;
  }

  public ParliamentHeroStarInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroStarInfo) null;
  }
}
