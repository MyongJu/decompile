﻿// Decompiled with JetBrains decompiler
// Type: TeleportManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class TeleportManager
{
  public const float TELEPORT_DELAY = 3f;
  private const string TELEPORT_SOURCE_EFFECT = "Prefab/VFX/fx_teleport_source";
  private const string TELEPORT_TARGET_EFFECT = "Prefab/VFX/fx_teleport_target";
  private const string TELEPORT_SMOKE_EFFECT = "Prefab/VFX/fx_teleport_smoke";
  private static TeleportManager m_Instance;
  private TeleportMode m_TeleportMode;
  private Coordinate m_TeleportLocation;
  private bool m_IgnoreWonderArea;

  private TeleportManager()
  {
  }

  public static TeleportManager Instance
  {
    get
    {
      if (TeleportManager.m_Instance == null)
        TeleportManager.m_Instance = new TeleportManager();
      return TeleportManager.m_Instance;
    }
  }

  public bool CheckCityState()
  {
    CityData playerCityData = PlayerData.inst.playerCityData;
    if (!playerCityData.triggerClearAway)
      return false;
    switch (playerCityData.clearAway)
    {
      case 1:
        UIManager.inst.priorityPopupQueue.OpenPopup("Teleport/RebuildPopup", (Popup.PopupParameter) null, 4);
        break;
      case 2:
        UIManager.inst.priorityPopupQueue.OpenPopup("Teleport/SendBackPopup", (Popup.PopupParameter) null, 4);
        break;
      case 3:
        UIManager.inst.priorityPopupQueue.OpenPopup("Teleport/ForceBackPopup", (Popup.PopupParameter) null, 4);
        break;
      case 8:
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("account_banned_midgame_title"),
          Content = Utils.XLAT("account_banned_midgame_description"),
          Okay = Utils.XLAT("id_uppercase_okay"),
          OkayCallback = (System.Action) (() => GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep)),
          OnCloseCallback = (System.Action) (() => GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep))
        });
        break;
    }
    return true;
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("teleport").AddEvent(new System.Action<object>(this.OnPushTeleport));
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityDataChanged);
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("teleport").RemoveEvent(new System.Action<object>(this.OnPushTeleport));
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.OnCityDataChanged);
  }

  private void OnPushTeleport(object data)
  {
    WatchtowerUtilities.Update();
  }

  private void OnCityDataChanged(long cityid)
  {
    if (cityid == PlayerData.inst.playerCityData.cityId && this.CheckCityState())
      return;
    CityData cityData = DBManager.inst.DB_City.Get(cityid);
    if (cityData == null || !cityData.isLocationChanged)
      return;
    cityData.isLocationChanged = false;
    if (cityData.cityId == (long) PlayerData.inst.cityId)
    {
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      GameEngine.GameMode targetMode = !PlayerData.inst.IsInPit ? GameEngine.GameMode.PVPMode : GameEngine.GameMode.PitMode;
      if (GameEngine.Instance.CurrentGameMode != targetMode)
      {
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          GameEngine.Instance.CurrentGameMode = targetMode;
          PVPSystem.Instance.Map.CurrentKXY = cityData.cityLocation;
          PVPSystem.Instance.Map.GotoLocation(cityData.cityLocation, false);
          this.PlayTeleprotMovie(cityData);
        }));
      }
      else
      {
        PVPSystem.Instance.Map.CurrentKXY = cityData.cityLocation;
        PVPSystem.Instance.Map.GotoLocation(cityData.cityLocation, false);
        this.PlayTeleprotMovie(cityData);
      }
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        return;
      this.PlayTeleprotMovie(cityData);
    }
  }

  private static DynamicMapData GetMapData(int K)
  {
    if (MapUtils.IsPitWorld(K))
      return PitMapData.MapData;
    return PVPMapData.MapData;
  }

  public void PlayAllianceWarTeleportMovie(Coordinate location)
  {
    long targetId = this.PlayVisualEffectAtLocation("Prefab/VFX/fx_teleport_target", location);
    PVPTile pvpTile = TeleportManager.GetMapData(location.K).GetPVPTile(location);
    if (pvpTile != null)
      pvpTile.Hide();
    Utils.ExecuteInSecs(3f, (System.Action) (() =>
    {
      VfxManager.Instance.Delete(targetId);
      Coordinate location1 = location;
      Coordinate location2 = new Coordinate(location1.K, location1.X - 1, location1.Y + 1);
      Coordinate location3 = new Coordinate(location1.K, location1.X + 1, location1.Y + 1);
      Coordinate location4 = new Coordinate(location1.K, location1.X, location1.Y + 2);
      this.ClearWasCityFlag(location1);
      this.ClearWasCityFlag(location2);
      this.ClearWasCityFlag(location3);
      this.ClearWasCityFlag(location4);
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        return;
      this.PlayVisualEffectAtLocation("Prefab/VFX/fx_teleport_smoke", location);
      TeleportManager.PlaySoundEffectAtLocation("sfx_teleport", location);
    }));
  }

  private void PlayTeleprotMovie(CityData cityData)
  {
    cityData.teleporting = true;
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_ENTER_TELEPORT || this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_TELEPORT || this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_BACK_TELEPORT)
    {
      cityData.teleporting = false;
    }
    else
    {
      long sourceId = 0;
      if (!cityData.triggerAntiClearAway)
        sourceId = this.PlayVisualEffectAtLocation("Prefab/VFX/fx_teleport_source", cityData.lastCityLocation);
      long targetId = this.PlayVisualEffectAtLocation("Prefab/VFX/fx_teleport_target", cityData.cityLocation);
      PVPTile pvpTile = TeleportManager.GetMapData(cityData.cityLocation.K).GetPVPTile(cityData.cityLocation);
      if (pvpTile != null)
        pvpTile.Hide();
      Utils.ExecuteInSecs(3f, (System.Action) (() =>
      {
        VfxManager.Instance.Delete(sourceId);
        VfxManager.Instance.Delete(targetId);
        cityData.teleporting = false;
        Coordinate cityLocation = cityData.cityLocation;
        Coordinate location1 = new Coordinate(cityLocation.K, cityLocation.X - 1, cityLocation.Y + 1);
        Coordinate location2 = new Coordinate(cityLocation.K, cityLocation.X + 1, cityLocation.Y + 1);
        Coordinate location3 = new Coordinate(cityLocation.K, cityLocation.X, cityLocation.Y + 2);
        this.ClearWasCityFlag(cityLocation);
        this.ClearWasCityFlag(location1);
        this.ClearWasCityFlag(location2);
        this.ClearWasCityFlag(location3);
        if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
          return;
        this.PlayVisualEffectAtLocation("Prefab/VFX/fx_teleport_smoke", cityData.cityLocation);
        TeleportManager.PlaySoundEffectAtLocation("sfx_teleport", cityData.cityLocation);
      }));
    }
  }

  private void ClearWasCityFlag(Coordinate location)
  {
    TileData tileAt = TeleportManager.GetMapData(location.K).GetTileAt(location);
    if (tileAt == null)
      return;
    tileAt.WasCity = false;
    tileAt.Show();
  }

  private static void PlaySoundEffectAtLocation(string sfxName, Coordinate location)
  {
    if (!PVPSystem.Instance.Map.GetClipViewport().Contains(TeleportManager.GetMapData(location.K).ConvertTileKXYToPixelPosition(location), true))
      return;
    AudioManager.Instance.PlaySound(sfxName, false);
  }

  private long PlayVisualEffectAtLocation(string vfxName, Coordinate location)
  {
    long uid = 0;
    Rect clipViewport = PVPSystem.Instance.Map.GetClipViewport();
    Vector3 pixelPosition = TeleportManager.GetMapData(location.K).ConvertTileKXYToPixelPosition(location);
    if (clipViewport.Contains(pixelPosition, true))
    {
      uid = VfxManager.Instance.CreateAndPlay(vfxName, PVPSystem.Instance.Map.transform);
      VfxManager.Instance.Get(uid).gameObject.transform.localPosition = pixelPosition;
    }
    return uid;
  }

  public void OnInviteTeleport(TeleportMode teleportMode)
  {
    this.m_TeleportMode = teleportMode;
    if (PlayerData.inst.allianceData != null)
    {
      this.m_IgnoreWonderArea = teleportMode == TeleportMode.ALLIANCE_TELEPORT;
      Hashtable postData = new Hashtable();
      postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
      MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param1, _param2) =>
      {
        this.m_TeleportLocation = DBManager.inst.DB_City.GetByUid(PlayerData.inst.allianceData.creatorId).cityLocation;
        this.SwitchPvPUI(true);
      }), true);
    }
    else
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("id_uppercase_warning"),
        Content = Utils.XLAT("alliance_teleport_not_in_alliance_warning"),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
  }

  public void OnFortressTeleport(TeleportMode teleportMode)
  {
    this.m_TeleportMode = teleportMode;
    AllianceFortressData buildingConfigId = DBManager.inst.DB_AllianceFortress.GetMyFortressDataByBuildingConfigId(ConfigManager.inst.DB_AllianceBuildings.Get("alliance_fortress").internalId);
    if (buildingConfigId == null)
      return;
    this.m_IgnoreWonderArea = true;
    this.m_TeleportLocation = buildingConfigId.Location;
    this.SwitchPvPUI(true);
  }

  public void RequestTeleport_FirstEnterPit(TeleportMode teleportMode, bool cloud = true)
  {
    this.m_TeleportMode = teleportMode;
    MessageHub.inst.GetPortByAction("City:replace").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.m_TeleportLocation = PlayerData.inst.playerCityData.cityLocation;
      this.SwitchUI_EnterPit(true, cloud);
    }), true);
  }

  public void EnterPit(TeleportMode teleportMode, bool cloud = true)
  {
    this.m_TeleportMode = teleportMode;
    this.m_TeleportLocation = PlayerData.inst.playerCityData.cityLocation;
    this.SwitchUI_EnterPit(false, cloud);
  }

  public void LeavePit(TeleportMode teleportMode, bool cloud = true)
  {
    this.m_TeleportMode = teleportMode;
    this.m_TeleportLocation = PlayerData.inst.playerCityData.cityLocation;
    this.SwitchUI_LeavePit(cloud);
  }

  private void SwitchUI_EnterPit(bool firstTime, bool cloud = true)
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
    {
      if (cloud)
      {
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          UIManager.inst.ShowManualBlocker();
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PitMode;
          if (firstTime)
            PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnFirstEnterPitBlock);
          else
            PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterPitBlock);
          PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
        }));
      }
      else
      {
        UIManager.inst.ShowManualBlocker();
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PitMode;
        if (firstTime)
          PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnFirstEnterPitBlock);
        else
          PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterPitBlock);
        PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
      }
    }
    else if (PitMapData.MapData.NeedRequestEnterKingdom(this.m_TeleportLocation))
    {
      UIManager.inst.ShowManualBlocker();
      if (firstTime)
        PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnFirstEnterPitBlock);
      else
        PitMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterPitBlock);
      PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
    }
    else if (firstTime)
      this.OnFirstEnterPitBlock();
    else
      this.OnEnterPitBlock();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void EnterAC(TeleportMode teleportMode, bool cloud = true)
  {
    AllianceWarPayload.Instance.IsInAllianceWarTeleport = true;
    GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
    this.m_TeleportMode = teleportMode;
    this.m_TeleportLocation = PlayerData.inst.playerCityData.cityLocation;
    this.m_IgnoreWonderArea = false;
    this.SwitchPvPUI(cloud);
  }

  public void LeaveAC(TeleportMode teleportMode, bool cloud = true)
  {
    this.m_TeleportMode = teleportMode;
    this.m_TeleportLocation = PlayerData.inst.playerCityData.cityLocation;
    this.SwitchPvPUI(cloud);
  }

  private void OnFirstEnterPitBlock()
  {
    UIManager.inst.HideManualBlocker();
    PitMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.OnFirstEnterPitBlock);
    UIManager.inst.tileCamera.StartAutoZoom();
  }

  private void OnEnterPitBlock()
  {
    UIManager.inst.HideManualBlocker();
    PitMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.OnEnterPitBlock);
    UIManager.inst.tileCamera.StartAutoZoom();
    TileData emptyTileForCity = PitMapData.MapData.GetNearEmptyTileForCity(this.m_TeleportLocation, true);
    if (emptyTileForCity != null)
      MessageHub.inst.GetPortByAction("city:placeForAbyss").SendRequest(Utils.Hash((object) "x", (object) emptyTileForCity.Location.X, (object) "y", (object) emptyTileForCity.Location.Y), (System.Action<bool, object>) ((ret, data) =>
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "errno"))
        {
          int result = 0;
          int.TryParse(hashtable[(object) "errno"].ToString(), out result);
          if (result != 1400040)
            return;
          this.HanldeTileNotFoundInPitMap();
        }
        else
        {
          if (!ret)
            return;
          Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
          ++cityLocation.Y;
          PVPSystem.Instance.Map.GotoLocation(cityLocation, false);
        }
      }), true);
    else
      this.HanldeTileNotFoundInPitMap();
  }

  public void OnKingdomTeleport(TeleportMode teleportMode, Coordinate targetLocation, bool cloud = true)
  {
    this.m_TeleportMode = teleportMode;
    this.m_TeleportLocation = targetLocation;
    this.m_IgnoreWonderArea = false;
    this.SwitchPvPUI(cloud);
  }

  public void CityReplace()
  {
    MessageHub.inst.GetPortByAction("City:replace").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
      ++cityLocation.Y;
      PVPSystem.Instance.Map.GotoLocation(cityLocation, false);
    }), true);
  }

  public void AllianceWarTeleport(System.Action<bool, object> callback = null)
  {
    TileData emptyTileForCity = PVPMapData.MapData.GetNearEmptyTileForCity(PlayerData.inst.playerCityData.cityLocation, false);
    if (emptyTileForCity == null)
      return;
    TeleportManager.Instance.PlayAllianceWarTeleportMovie(emptyTileForCity.Location);
    AllianceWarPayload.Instance.PlaceCity(PlayerData.inst.uid, emptyTileForCity.Location.X, emptyTileForCity.Location.Y, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        AllianceWarPayload.Instance.IsInAllianceWarTeleport = false;
        if (callback == null)
          return;
        callback(ret, data);
      }
      else
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
          return;
        int result = 0;
        int.TryParse(hashtable[(object) "errno"].ToString(), out result);
        if (result != 1400040)
          return;
        TeleportManager.Instance.CityReplace();
      }
    }));
  }

  public void SetTeleportMode(TeleportMode teleportMode)
  {
    this.m_TeleportMode = teleportMode;
  }

  private void SwitchPvPUI(bool cloud = true)
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
    {
      if (cloud)
      {
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          UIManager.inst.ShowManualBlocker();
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
          PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlock);
          PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
        }));
      }
      else
      {
        UIManager.inst.ShowManualBlocker();
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlock);
        PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
      }
    }
    else if (PVPMapData.MapData.NeedRequestEnterKingdom(this.m_TeleportLocation))
    {
      UIManager.inst.ShowManualBlocker();
      PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlock);
      PVPSystem.Instance.Map.CurrentKXY = this.m_TeleportLocation;
      PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
    }
    else
      this.OnEnterKingdomBlock();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void OnEnterKingdomBlock()
  {
    UIManager.inst.HideManualBlocker();
    PVPMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.OnEnterKingdomBlock);
    UIManager.inst.tileCamera.StartAutoZoom();
    if (this.m_TeleportMode == TeleportMode.ALLIANCE_WAR_BACK_TELEPORT)
    {
      TeleportManager.Instance.AllianceWarTeleport((System.Action<bool, object>) null);
    }
    else
    {
      TileData emptyTileForCity = PVPMapData.MapData.GetNearEmptyTileForCity(this.m_TeleportLocation, this.m_IgnoreWonderArea);
      if (emptyTileForCity == null)
        return;
      PVPSystem.Instance.Map.EnterTeleportMode(emptyTileForCity.Location, this.m_TeleportMode, 0, false);
      Coordinate location = emptyTileForCity.Location;
      ++location.Y;
      PVPSystem.Instance.Map.GotoLocation(location, false);
    }
  }

  private void OnEnterKingdomBlockOutFromPit()
  {
    UIManager.inst.HideManualBlocker();
    PVPMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.OnEnterKingdomBlockOutFromPit);
    UIManager.inst.tileCamera.StartAutoZoom();
    TileData emptyTileForCity = PVPMapData.MapData.GetNearEmptyTileForCity(this.m_TeleportLocation, false);
    if (emptyTileForCity != null)
      MessageHub.inst.GetPortByAction("city:placeForAbyss").SendRequest(Utils.Hash((object) "x", (object) emptyTileForCity.Location.X, (object) "y", (object) emptyTileForCity.Location.Y), (System.Action<bool, object>) ((ret, data) =>
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "errno"))
        {
          int result = 0;
          int.TryParse(hashtable[(object) "errno"].ToString(), out result);
          if (result != 1400040)
            return;
          this.HandleTileNotFoundInPVPMap();
        }
        else
        {
          if (!ret)
            return;
          Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
          ++cityLocation.Y;
          PVPSystem.Instance.Map.GotoLocation(cityLocation, false);
        }
      }), true);
    else
      this.HandleTileNotFoundInPVPMap();
  }

  private void HandleTileNotFoundInPVPMap()
  {
    MessageHub.inst.GetPortByAction("City:replace").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
      ++cityLocation.Y;
      PVPSystem.Instance.Map.GotoLocation(cityLocation, false);
    }), true);
  }

  private void HanldeTileNotFoundInPitMap()
  {
    MessageHub.inst.GetPortByAction("City:replace").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      Coordinate cityLocation = PlayerData.inst.playerCityData.cityLocation;
      ++cityLocation.Y;
      PVPSystem.Instance.Map.GotoLocation(cityLocation, false);
    }), true);
  }

  private void SwitchUI_LeavePit(bool cloud)
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
    {
      if (cloud)
      {
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          UIManager.inst.ShowManualBlocker();
          GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
          PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlockOutFromPit);
          PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
        }));
      }
      else
      {
        UIManager.inst.ShowManualBlocker();
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlockOutFromPit);
        PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
      }
    }
    else if (PVPMapData.MapData.NeedRequestEnterKingdom(this.m_TeleportLocation))
    {
      UIManager.inst.ShowManualBlocker();
      PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.OnEnterKingdomBlockOutFromPit);
      PVPSystem.Instance.Map.CurrentKXY = this.m_TeleportLocation;
      PVPSystem.Instance.Map.GotoLocation(this.m_TeleportLocation, false);
    }
    else
      this.OnEnterKingdomBlockOutFromPit();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }
}
