﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroLevel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroLevel
{
  private List<ParliamentHeroLevelInfo> _parliamentHeroLevelInfoList = new List<ParliamentHeroLevelInfo>();
  private Dictionary<string, ParliamentHeroLevelInfo> _datas;
  private Dictionary<int, ParliamentHeroLevelInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroLevelInfo, string>(res as Hashtable, "level", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroLevelInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.level))
        this._parliamentHeroLevelInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroLevelInfoList.Sort((Comparison<ParliamentHeroLevelInfo>) ((a, b) => a.Level.CompareTo(b.Level)));
  }

  public List<ParliamentHeroLevelInfo> GetParliamentHeroLevelInfoList()
  {
    return this._parliamentHeroLevelInfoList;
  }

  public ParliamentHeroLevelInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroLevelInfo) null;
  }

  public ParliamentHeroLevelInfo GetByLevel(string level)
  {
    if (this._datas.ContainsKey(level))
      return this._datas[level];
    return (ParliamentHeroLevelInfo) null;
  }

  public ParliamentHeroLevelInfo GetByLevel(int level)
  {
    return this.GetByLevel(level.ToString());
  }
}
