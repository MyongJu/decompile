﻿// Decompiled with JetBrains decompiler
// Type: Messenger
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

internal static class Messenger
{
  private static MessengerHelper messengerHelper = (MessengerHelper) null;
  public static Dictionary<string, Delegate> eventTable = new Dictionary<string, Delegate>();
  public static List<string> permanentMessages = new List<string>();

  public static void MarkAsPermanent(string eventType)
  {
    Messenger.permanentMessages.Add(eventType);
  }

  public static void Init()
  {
    Messenger.messengerHelper = new GameObject("MessengerHelper").AddComponent<MessengerHelper>();
  }

  public static void Cleanup()
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, Delegate>.Enumerator enumerator1 = Messenger.eventTable.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<string, Delegate> current1 = enumerator1.Current;
        bool flag = false;
        using (List<string>.Enumerator enumerator2 = Messenger.permanentMessages.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            string current2 = enumerator2.Current;
            if (current1.Key == current2)
            {
              flag = true;
              break;
            }
          }
        }
        if (!flag)
          stringList.Add(current1.Key);
      }
    }
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Messenger.eventTable.Remove(current);
      }
    }
    UnityEngine.Object.DestroyObject((UnityEngine.Object) GameObject.Find("MessengerHelper"));
  }

  public static void PrintEventTable()
  {
    using (Dictionary<string, Delegate>.Enumerator enumerator = Messenger.eventTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, Delegate> current = enumerator.Current;
      }
    }
  }

  public static void OnListenerAdding(string eventType, Delegate listenerBeingAdded)
  {
    if (!Messenger.eventTable.ContainsKey(eventType))
      Messenger.eventTable.Add(eventType, (Delegate) null);
    else
      D.error((object) "Can't have two keys the same!");
    Delegate @delegate = Messenger.eventTable[eventType];
    if ((object) @delegate != null && @delegate.GetType() != listenerBeingAdded.GetType())
      throw new Messenger.ListenerException(string.Format("Attempting to add listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being added has type {2}", (object) eventType, (object) @delegate.GetType().Name, (object) listenerBeingAdded.GetType().Name));
  }

  public static void OnListenerRemoving(string eventType, Delegate listenerBeingRemoved)
  {
    if (!Messenger.eventTable.ContainsKey(eventType))
      throw new Messenger.ListenerException(string.Format("Attempting to remove listener for type \"{0}\" but Messenger doesn't know about this event type.", (object) eventType));
    Delegate @delegate = Messenger.eventTable[eventType];
    if ((object) @delegate == null)
      throw new Messenger.ListenerException(string.Format("Attempting to remove listener with for event type \"{0}\" but current listener is null.", (object) eventType));
    if (@delegate.GetType() != listenerBeingRemoved.GetType())
      throw new Messenger.ListenerException(string.Format("Attempting to remove listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being removed has type {2}", (object) eventType, (object) @delegate.GetType().Name, (object) listenerBeingRemoved.GetType().Name));
  }

  public static void OnListenerRemoved(string eventType)
  {
    if ((object) Messenger.eventTable[eventType] != null)
      return;
    Messenger.eventTable.Remove(eventType);
  }

  public static void OnBroadcasting(string eventType)
  {
    if (!Messenger.eventTable.ContainsKey(eventType))
      ;
  }

  public static Messenger.BroadcastException CreateBroadcastSignatureException(string eventType)
  {
    return new Messenger.BroadcastException(string.Format("Broadcasting message \"{0}\" but listeners have a different signature than the broadcaster.", (object) eventType));
  }

  public static void AddListener(string eventType, Callback handler)
  {
    Messenger.OnListenerAdding(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Combine(Messenger.eventTable[eventType], (Delegate) handler);
  }

  public static void AddListener<T>(string eventType, Callback<T> handler)
  {
    Messenger.OnListenerAdding(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Combine(Messenger.eventTable[eventType], (Delegate) handler);
  }

  public static void AddListener<T, U>(string eventType, Callback<T, U> handler)
  {
    Messenger.OnListenerAdding(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Combine(Messenger.eventTable[eventType], (Delegate) handler);
  }

  public static void AddListener<T, U, V>(string eventType, Callback<T, U, V> handler)
  {
    Messenger.OnListenerAdding(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Combine(Messenger.eventTable[eventType], (Delegate) handler);
  }

  public static void ShowEvents()
  {
    using (Dictionary<string, Delegate>.Enumerator enumerator = Messenger.eventTable.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, Delegate> current = enumerator.Current;
      }
    }
  }

  public static void RemoveListener(string eventType)
  {
    Callback callback = (Callback) Messenger.eventTable[eventType];
    Messenger.OnListenerRemoving(eventType, (Delegate) callback);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) callback);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void RemoveListener(string eventType, Callback handler)
  {
    Messenger.OnListenerRemoving(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) handler);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void RemoveListener<T>(string eventType)
  {
    Callback<T> callback = (Callback<T>) Messenger.eventTable[eventType];
    Messenger.OnListenerRemoving(eventType, (Delegate) callback);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) callback);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void RemoveListener<T>(string eventType, Callback<T> handler)
  {
    Messenger.OnListenerRemoving(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) handler);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void RemoveListener<T, U>(string eventType, Callback<T, U> handler)
  {
    Messenger.OnListenerRemoving(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) handler);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void RemoveListener<T, U, V>(string eventType, Callback<T, U, V> handler)
  {
    Messenger.OnListenerRemoving(eventType, (Delegate) handler);
    Messenger.eventTable[eventType] = Delegate.Remove(Messenger.eventTable[eventType], (Delegate) handler);
    Messenger.OnListenerRemoved(eventType);
  }

  public static void Broadcast(string eventType)
  {
    Messenger.OnBroadcasting(eventType);
    Delegate @delegate;
    if (!Messenger.eventTable.TryGetValue(eventType, out @delegate))
      return;
    Callback callback = @delegate as Callback;
    if (callback == null)
      throw Messenger.CreateBroadcastSignatureException(eventType);
    callback();
  }

  public static void Broadcast<T>(string eventType, T arg1)
  {
    Messenger.OnBroadcasting(eventType);
    Delegate @delegate;
    if (!Messenger.eventTable.TryGetValue(eventType, out @delegate))
      return;
    Callback<T> callback = @delegate as Callback<T>;
    if (callback == null)
      throw Messenger.CreateBroadcastSignatureException(eventType);
    callback(arg1);
  }

  public static void Broadcast<T, U>(string eventType, T arg1, U arg2)
  {
    Messenger.OnBroadcasting(eventType);
    Delegate @delegate;
    if (!Messenger.eventTable.TryGetValue(eventType, out @delegate))
      return;
    Callback<T, U> callback = @delegate as Callback<T, U>;
    if (callback == null)
      throw Messenger.CreateBroadcastSignatureException(eventType);
    callback(arg1, arg2);
  }

  public static void Broadcast<T, U, V>(string eventType, T arg1, U arg2, V arg3)
  {
    Messenger.OnBroadcasting(eventType);
    Delegate @delegate;
    if (!Messenger.eventTable.TryGetValue(eventType, out @delegate))
      return;
    Callback<T, U, V> callback = @delegate as Callback<T, U, V>;
    if (callback == null)
      throw Messenger.CreateBroadcastSignatureException(eventType);
    callback(arg1, arg2, arg3);
  }

  public class BroadcastException : Exception
  {
    public BroadcastException(string msg)
      : base(msg)
    {
    }
  }

  public class ListenerException : Exception
  {
    public ListenerException(string msg)
      : base(msg)
    {
    }
  }
}
