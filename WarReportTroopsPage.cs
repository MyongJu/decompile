﻿// Decompiled with JetBrains decompiler
// Type: WarReportTroopsPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarReportTroopsPage : MonoBehaviour, WarReportDetailPage.IReportSubPage
{
  private readonly List<GameObject> _warReportItems = new List<GameObject>();
  public TroopsPagePlayerInfo playerData;
  public TroopsPagePlayerInfo opponentData;
  public UIPanel panel;
  public UIScrollView scrollView;
  public UILabel oreLoss;
  public UILabel woodLoss;
  public UILabel foodLoss;
  public UILabel silverLoss;
  public UILabel oreGain;
  public UILabel woodGain;
  public UILabel foodGain;
  public UILabel silverGain;
  public UITable resourceTable;
  public WarReportResItem itemTemplate;
  private Hashtable _data;
  private bool _isAttacker;

  private Hashtable _attackerData
  {
    get
    {
      return this._data[(object) "attacker"] as Hashtable;
    }
  }

  private Hashtable _defenderData
  {
    get
    {
      return this._data[(object) "defender"] as Hashtable;
    }
  }

  public void SetData(Hashtable data)
  {
    this._data = data;
    this._isAttacker = long.Parse(this._attackerData[(object) "uid"].ToString()) == PlayerData.inst.uid;
    this.Clear();
    this.playerData.Clear();
    this.opponentData.Clear();
    this.ProcessResourceData(data);
    if (this._isAttacker)
    {
      this.ProcessTroopData(this._attackerData, this.playerData);
      this.ProcessTroopData(this._defenderData, this.opponentData);
    }
    else
    {
      this.ProcessTroopData(this._defenderData, this.playerData);
      this.ProcessTroopData(this._attackerData, this.opponentData);
    }
    this.playerData.SetPlayerInfo(data, this._isAttacker);
    this.opponentData.SetPlayerInfo(data, !this._isAttacker);
    this.SetItems(data[(object) "rewards"] as Hashtable);
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.scrollView.ResetPosition();
    this.panel.alpha = 1f;
  }

  public void Hide()
  {
    this.panel.alpha = 0.01f;
  }

  public void SetItems(Hashtable itemList)
  {
    if (itemList == null)
      return;
    for (int index = 0; index < this._warReportItems.Count; ++index)
      Object.Destroy((Object) this._warReportItems[index]);
    this._warReportItems.Clear();
    this._warReportItems.TrimExcess();
    foreach (DictionaryEntry dictionaryEntry in itemList)
    {
      GameObject gameObject = NGUITools.AddChild(this.resourceTable.gameObject, this.itemTemplate.gameObject);
      if ((Object) gameObject != (Object) null)
      {
        gameObject.GetComponent<WarReportResItem>().SetItem(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
        gameObject.SetActive(true);
        this._warReportItems.Add(gameObject);
      }
    }
    this.gameObject.SetActive(true);
    this.resourceTable.repositionNow = true;
  }

  private void Clear()
  {
    List<GameObject>.Enumerator enumerator = this._warReportItems.GetEnumerator();
    while (enumerator.MoveNext())
      Object.Destroy((Object) enumerator.Current);
    this._warReportItems.Clear();
  }

  private void ProcessResourceData(Hashtable dataBlock)
  {
    Hashtable hashtable = dataBlock[(object) "resources"] as Hashtable;
    if (hashtable == null)
    {
      this.SetResourceData(0, 0, 0, 0);
    }
    else
    {
      int result1;
      int.TryParse(hashtable[(object) "ore"].ToString(), out result1);
      int result2;
      int.TryParse(hashtable[(object) "wood"].ToString(), out result2);
      int result3;
      int.TryParse(hashtable[(object) "food"].ToString(), out result3);
      int result4;
      int.TryParse(hashtable[(object) "silver"].ToString(), out result4);
      if (this._isAttacker)
        this.SetResourceData(result1, result2, result3, result4);
      else
        this.SetResourceData(-1 * result1, -1 * result2, -1 * result3, -1 * result4);
    }
  }

  private void SetResourceData(int oreChange, int woodChange, int foodChange, int silverChange)
  {
    UILabel oreGain = this.oreGain;
    string str1 = oreChange.ToString();
    this.oreLoss.text = str1;
    string str2 = str1;
    oreGain.text = str2;
    this.oreGain.gameObject.SetActive(oreChange >= 0);
    this.oreLoss.gameObject.SetActive(oreChange < 0);
    UILabel woodGain = this.woodGain;
    string str3 = woodChange.ToString();
    this.woodLoss.text = str3;
    string str4 = str3;
    woodGain.text = str4;
    this.woodGain.gameObject.SetActive(woodChange >= 0);
    this.woodLoss.gameObject.SetActive(woodChange < 0);
    UILabel foodGain = this.foodGain;
    string str5 = foodChange.ToString();
    this.foodLoss.text = str5;
    string str6 = str5;
    foodGain.text = str6;
    this.foodGain.gameObject.SetActive(foodChange >= 0);
    this.foodLoss.gameObject.SetActive(foodChange < 0);
    UILabel silverGain = this.silverGain;
    string str7 = silverChange.ToString();
    this.silverLoss.text = str7;
    string str8 = str7;
    silverGain.text = str8;
    this.silverGain.gameObject.SetActive(silverChange >= 0);
    this.silverLoss.gameObject.SetActive(silverChange < 0);
  }

  private void ProcessTroopData(Hashtable dataBlock, TroopsPagePlayerInfo targetUI)
  {
    targetUI.Clear();
    Hashtable hashtable1 = dataBlock[(object) "start_troops"] as Hashtable;
    Hashtable hashtable2 = dataBlock[(object) "end_troops"] as Hashtable;
    Dictionary<TroopInfo, KeyValuePair<int, int>> unitMap = new Dictionary<TroopInfo, KeyValuePair<int, int>>();
    if (hashtable1 != null)
    {
      IEnumerator enumerator = hashtable1.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        TroopInfo troopInfo = TroopInfoManager.Instance.GetTroopInfo(enumerator.Current.ToString());
        int key = int.Parse(hashtable1[enumerator.Current].ToString());
        int num = 0;
        if (hashtable2 != null && hashtable2.ContainsKey(enumerator.Current))
          num = int.Parse(hashtable2[enumerator.Current].ToString());
        unitMap.Add(troopInfo, new KeyValuePair<int, int>(key, key - num));
      }
    }
    targetUI.SetTroopData(unitMap);
  }
}
