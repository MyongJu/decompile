﻿// Decompiled with JetBrains decompiler
// Type: Utils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using ICSharpCode.SharpZipLib.GZip;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UI;
using UnityEngine;

public class Utils
{
  private static bool stop = false;
  private static Coroutine _coroutinue = (Coroutine) null;
  public static readonly DateTime time19700101 = new DateTime(1970, 1, 1);
  public static bool IsIAPStoreEnabled = true;
  private static JsonReaderSettings _rSetting = (JsonReaderSettings) null;
  private static JsonWriterSettings _wSetting = (JsonWriterSettings) null;
  private static bool _jsonInited = false;
  public const int DRAGON_LEVELUP_PRIORITY = 0;
  public const int LORD_LEVELUP_PRIORITY = 1;
  public const int SIGN_IN_PRIORITY = 2;
  public const int EVENT_IN_PRIORITY = 3;
  public const int REBUILD_CITY_PRIORITY = 4;
  public const int RECOMMEND_PRIORITY = -1;
  public const int CONNECTION_PRIORITY = 100;
  public const long NPC_UID = 6000000000;
  public const int SIGN_COOL_DOWN = 7;
  public const float DOUBLE_CLICK_TIMEOUT = 0.3f;
  public const int ONE_DAY = 86400;
  public const int TWO_DAYS = 172800;

  public static void ForceUnloadAssets()
  {
  }

  public static string GetFlagIconPath(int flag)
  {
    flag = WonderFlagManager.Instance.GetMappedFlag(flag);
    return "icon_flag_" + (object) flag;
  }

  public static bool IsDragonHatchingJob(JobHandle job)
  {
    if (job != null)
    {
      Hashtable data = job.Data as Hashtable;
      if (job.GetJobEvent() == JobEvent.JOB_TRAIN_TROOP && data != null && data.ContainsKey((object) "troop_class"))
        return data[(object) "troop_class"].ToString() == "dragon";
    }
    return false;
  }

  public static int TryParseInt(Hashtable data, object key)
  {
    int result = 0;
    if (data != null && data.ContainsKey(key) && data[key] != null)
      int.TryParse(data[key].ToString(), out result);
    return result;
  }

  public static long TryParseLong(Hashtable data, object key)
  {
    long result = 0;
    if (data != null && data.ContainsKey(key) && data[key] != null)
      long.TryParse(data[key].ToString(), out result);
    return result;
  }

  public static bool IsHelpedMarchInEmbassy()
  {
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    if (targetListByType != null)
    {
      for (int index = 0; index < targetListByType.Count; ++index)
      {
        MarchData marchData = DBManager.inst.DB_March.Get(targetListByType[index]);
        if (marchData != null && (marchData.troopsInfo.troops != null && marchData.state == MarchData.MarchState.reinforcing))
          return true;
      }
    }
    return false;
  }

  public static bool IsMarchInCurrentKingdom()
  {
    List<MarchData> marches = DBManager.inst.DB_March.Marches;
    for (int index = 0; index < marches.Count; ++index)
    {
      if (marches[index].ownerUid == PlayerData.inst.uid)
        return true;
    }
    List<RallyData> inCurrentKingdom = DBManager.inst.DB_Rally.GetRallyesByAllianceIdInCurrentKingdom(PlayerData.inst.allianceId);
    for (int index = 0; index < inCurrentKingdom.Count; ++index)
    {
      if (inCurrentKingdom[index].ownerAllianceId != PlayerData.inst.allianceId && inCurrentKingdom[index].targetAllianceId == PlayerData.inst.allianceId && inCurrentKingdom[index].ownerUid == PlayerData.inst.uid)
        return true;
    }
    return false;
  }

  public static bool IsSomeoneAttackMe()
  {
    List<MarchData> marches = DBManager.inst.DB_March.Marches;
    for (int index = 0; index < marches.Count; ++index)
    {
      if (marches[index].IsAttackMe)
        return true;
    }
    return false;
  }

  public static bool IsSomeoneAttackingMyFortress(long fortressId)
  {
    if (fortressId == 0L)
      return false;
    AllianceFortressData allianceFortressData = DBManager.inst.DB_AllianceFortress.Get(fortressId);
    if (allianceFortressData == null)
      return false;
    if (allianceFortressData.State == "demolishing")
      return true;
    List<MarchData> marches = DBManager.inst.DB_March.Marches;
    for (int index = 0; index < marches.Count; ++index)
    {
      if (marches[index].ownerAllianceId != PlayerData.inst.allianceId && (marches[index].IsAttackingFortress(fortressId) || marches[index].IsScoutingFortress(fortressId)))
        return true;
    }
    List<RallyData> inCurrentKingdom = DBManager.inst.DB_Rally.GetRallyesByAllianceIdInCurrentKingdom(PlayerData.inst.allianceId);
    for (int index = 0; index < inCurrentKingdom.Count; ++index)
    {
      if (inCurrentKingdom[index].ownerAllianceId != PlayerData.inst.allianceId && inCurrentKingdom[index].targetAllianceId == PlayerData.inst.allianceId && (long) inCurrentKingdom[index].fortressId == fortressId)
        return true;
    }
    return false;
  }

  public static void ShowException(int errno)
  {
    UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
    {
      type = CommonBlocker.CommonBlockerType.confirmation,
      displayType = 11,
      title = Utils.XLAT("exception_title"),
      buttonEventText = Utils.XLAT("exception_button"),
      descriptionText = ScriptLocalization.Get(string.Format("exception_{0}", (object) errno), true),
      buttonEvent = (System.Action) null
    });
  }

  public static char AllianceNameValidator(string allianceName, int charIndex, char addedChar)
  {
    allianceName = allianceName.Trim();
    if (Encoding.UTF8.GetBytes(allianceName).Length >= 20)
      return char.MinValue;
    return addedChar;
  }

  public static void StopShowItemTip()
  {
    Utils.stop = true;
    if (Utils._coroutinue == null)
      return;
    Utils.StopCoroutine(Utils._coroutinue);
    Utils._coroutinue = (Coroutine) null;
  }

  public static void SetItemNormalName(UILabel label, int quailty)
  {
    if (!((UnityEngine.Object) label != (UnityEngine.Object) null))
      return;
    EquipmentManager.Instance.ConfigQualityLabelWithColor(label, quailty);
  }

  public static void SetItemNormalBackground(UITexture background, int quailty)
  {
    if (!((UnityEngine.Object) background != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) background, Utils.GetQualityImagePath(quailty), (System.Action<bool>) null, true, false, string.Empty);
  }

  public static void SetItemBackground(UITexture background, int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (!((UnityEngine.Object) background != (UnityEngine.Object) null) || itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) background, Utils.GetQualityImagePath(itemStaticInfo.Quality), (System.Action<bool>) null, true, false, string.Empty);
  }

  public static void SetItemName(UILabel label, int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (!((UnityEngine.Object) label != (UnityEngine.Object) null) || itemStaticInfo == null)
      return;
    label.text = itemStaticInfo.LocName;
    EquipmentManager.Instance.ConfigQualityLabelWithColor(label, itemStaticInfo.Quality);
  }

  public static void DelayShowTip(int itemId, Transform focous, long equipmentid = 0, long userId = 0, int enhanced = 0)
  {
    if (ConfigManager.inst.DB_Items.GetItem(itemId) == null)
      return;
    Utils.stop = false;
    UIScrollView componentInParent = focous.GetComponentInParent<UIScrollView>();
    if ((UnityEngine.Object) componentInParent != (UnityEngine.Object) null)
    {
      componentInParent.onDragStarted -= new UIScrollView.OnDragNotification(Utils.OnItemTipStartDragging);
      componentInParent.onDragStarted += new UIScrollView.OnDragNotification(Utils.OnItemTipStartDragging);
    }
    Utils._coroutinue = Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      if (Utils.stop)
        return;
      Utils.ShowItemTip(itemId, focous, equipmentid, userId, enhanced);
    }));
  }

  private static void OnItemTipStartDragging()
  {
    Utils.StopShowItemTip();
  }

  public static void ShowItemTip(int itemId, Transform focous, long equipmentid = 0, long userId = 0, int enhanced = 0)
  {
    Utils.StopShowItemTip();
    if (ConfigManager.inst.DB_Items.GetItem(itemId) == null || !((UnityEngine.Object) focous != (UnityEngine.Object) null) || UIManager.inst.IsPopupExist("ItemTipDetailPopup"))
      return;
    UIManager.inst.OpenPopup("ItemTipDetailPopup", (Popup.PopupParameter) new ItemTipPopup.Parameter()
    {
      itemId = itemId,
      focus = focous,
      equipmentId = equipmentid,
      userId = userId,
      enhanced = enhanced
    });
  }

  public static void ShowItemTip(InventoryItemData itemData, Transform focous)
  {
    Utils.StopShowItemTip();
    if (itemData == null || ConfigManager.inst.DB_Items.GetItem(itemData.equipment.itemID) == null || (!((UnityEngine.Object) focous != (UnityEngine.Object) null) || UIManager.inst.IsPopupExist("ItemTipDetailPopup")))
      return;
    UIManager.inst.OpenPopup("ItemTipDetailPopup", (Popup.PopupParameter) new ItemTipPopup.Parameter()
    {
      itemId = itemData.equipment.itemID,
      focus = focous,
      equipmentId = itemData.itemId,
      userId = itemData.uid,
      inventoryItemData = itemData
    });
  }

  public static void DelayShowCustomTip(ItemTipCustomInfo.ItemTipData itemTipData, Transform focus)
  {
    Utils.stop = false;
    Utils._coroutinue = Utils.ExecuteInSecs(1f, (System.Action) (() =>
    {
      if (Utils.stop)
        return;
      Utils.ShowCustomItemTip(itemTipData, focus);
    }));
  }

  public static void ShowCustomItemTip(ItemTipCustomInfo.ItemTipData itemTipData, Transform focus)
  {
    Utils.StopShowItemTip();
    if (UIManager.inst.IsPopupExist("ItemTipDetailPopup"))
      return;
    UIManager.inst.OpenPopup("ItemTipDetailPopup", (Popup.PopupParameter) new ItemTipPopup.Parameter()
    {
      customTip = true,
      customItemTipData = itemTipData,
      focus = focus
    });
  }

  public static string GetQualityImagePath(int quality)
  {
    return "Texture/Equipment/frame_equipment_" + (object) quality;
  }

  public static string Join<T>(List<T> sources, string separator = ",")
  {
    if (sources == null || sources.Count == 0)
      return string.Empty;
    string empty = string.Empty;
    int count = sources.Count;
    for (int index = 0; index < sources.Count; ++index)
    {
      empty += sources[index].ToString();
      if (index != count - 1)
        empty += ",";
    }
    return empty;
  }

  public static void RefreshBlock(Coordinate location, System.Action<bool, object> callback = null)
  {
    BlockData blockAt = PVPMapData.MapData.GetBlockAt(location);
    if (blockAt == null)
      return;
    blockAt.ResetToTerrain();
    Hashtable postData = new Hashtable();
    postData[(object) "leave"] = (object) new ArrayList();
    postData[(object) "enter"] = (object) new ArrayList()
    {
      (object) new ArrayList()
      {
        (object) location.K,
        (object) blockAt.BlockID
      }
    };
    postData[(object) "city_id"] = (object) PlayerData.inst.cityId;
    MessageHub.inst.GetPortByAction("Map:enterKingdomBlock").SendLoader(postData, callback, true, true);
  }

  public static bool IsR4Member()
  {
    int num = 1;
    if (PlayerData.inst.allianceData == null)
      return false;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.userData.uid);
    if (allianceMemberData != null)
      num = allianceMemberData.title;
    return num >= 4;
  }

  public static void ShowWarning(string content, ChooseConfirmationBox.ButtonState state, System.Action callBack, string title = null)
  {
    if (string.IsNullOrEmpty(title))
      title = ScriptLocalization.Get("account_warning_title", true);
    string str1 = ScriptLocalization.Get("id_uppercase_cancel", true);
    string str2 = ScriptLocalization.Get("id_uppercase_confirm", true);
    ChooseConfirmationBox.Parameter parameter = new ChooseConfirmationBox.Parameter();
    parameter.title = title;
    parameter.message = content;
    parameter.yesOrOklabel_left = str1;
    parameter.nolabel_right = str2;
    parameter.buttonState = state;
    parameter.yesOrOkHandler_right = (System.Action) null;
    parameter.noHandler_left = callBack;
    parameter.closeHandler = (System.Action) null;
    try
    {
      UIManager.inst.OpenPopup("ChooseConfirmationBox", (Popup.PopupParameter) parameter);
    }
    catch (Exception ex)
    {
      D.error((object) "Can not found ChooseConfirmationBox!");
      throw;
    }
  }

  public static bool IsR5Member()
  {
    int num = 1;
    if (PlayerData.inst.allianceData == null)
      return false;
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.userData.uid);
    if (allianceMemberData != null)
      num = allianceMemberData.title;
    return num == 6;
  }

  public static int GetPlayerAllianceTempleLevel()
  {
    AllianceTechData allicenTechByType = DBManager.inst.DB_AllianceTech.GetCurAllicenTechByType("alliance_temple_level_1");
    if (allicenTechByType != null)
    {
      AllianceTechInfo allianceTechInfo = ConfigManager.inst.DB_AllianceTech.GetItem(allicenTechByType.researchId);
      if (allianceTechInfo != null)
        return allianceTechInfo.Level;
    }
    return 0;
  }

  public static string GetUserTitleIconPath(int title)
  {
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(title.ToString());
    if (wonderTitleInfo != null)
      return "Texture/Kingdom/" + wonderTitleInfo.icon;
    return string.Empty;
  }

  public static string GetUserTitleBgPath(int title)
  {
    WonderTitleInfo wonderTitleInfo = ConfigManager.inst.DB_WonderTitle.Get(title.ToString());
    if (wonderTitleInfo == null)
      return string.Empty;
    return wonderTitleInfo.titleType == 0 ? "Texture/Kingdom/kingdom_map_title_base_positive" : "Texture/Kingdom/kingdom_map_title_base_negative";
  }

  public static double GetWallDefenceLimit()
  {
    return (double) ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_city_defense");
  }

  public static void ShowNotEnoughGoldTip()
  {
    if (Utils.ShowIAPStore((UI.Dialog.DialogParameter) null))
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_insufficient_gold_tip", true), (System.Action) null, 4f, false);
  }

  public static void SetPriceToLabel(UILabel label, int gold)
  {
    Color color = new Color(0.9686275f, 0.8f, 0.003921569f, 1f);
    label.text = Utils.FormatThousands(gold.ToString());
    if (gold > PlayerData.inst.hostPlayer.Currency)
      label.color = Color.red;
    else
      label.color = color;
  }

  public static string FormatTimeAgo(long _sendTime)
  {
    int num1 = (NetServerTime.inst.ServerTimestamp - (int) _sendTime) / 60;
    int num2 = 43200;
    int num3 = 1440;
    int num4 = 60;
    int num5;
    bool flag1 = (num5 = 0) != 0;
    bool flag2 = num5 != 0;
    bool flag3 = num5 != 0;
    bool flag4 = num5 != 0;
    int num6 = num1 / num2;
    int num7 = num1 - num6 * num2;
    bool flag5;
    bool flag6;
    bool flag7;
    bool flag8 = flag4 | (flag7 = flag3 | (flag6 = flag2 | (flag5 = flag1 | num6 > 0)));
    int num8 = num7 / num3;
    int num9 = num7 - num8 * num3;
    bool flag9;
    bool flag10;
    bool flag11 = flag7 | (flag10 = flag6 | (flag9 = flag5 | num8 > 0));
    int num10 = num9 / num4;
    int num11 = num9 - num10 * num4;
    bool flag12;
    bool flag13 = flag10 | (flag12 = flag9 | num10 > 0);
    int num12 = num11;
    bool flag14 = flag12 | num12 > 0;
    if (!flag14)
      return "less than 1 min";
    return string.Format("{0}{1}{2}{3} ago", !flag8 ? (object) string.Empty : (object) string.Format("{0} Month", (object) num6), !flag11 ? (object) string.Empty : (object) string.Format("{0} Day", (object) num8), !flag13 ? (object) string.Empty : (object) string.Format("{0} Hour", (object) num10), !flag14 ? (object) string.Empty : (object) string.Format("{0} Min", (object) num12));
  }

  public static string FormatOfflineTime(long duration)
  {
    long num1 = duration / 60L;
    long num2 = num1 / 525600L;
    if (num2 >= 1L)
      return string.Format(ScriptLocalization.Get("alliance_members_online_time_years", true), (object) num2);
    long num3 = num1 / 1440L;
    if (num3 >= 1L)
      return string.Format(ScriptLocalization.Get("alliance_members_online_time_days", true), (object) num3);
    long num4 = num1 / 60L;
    if (num4 >= 1L)
      return string.Format(ScriptLocalization.Get("alliance_members_online_time_hours", true), (object) num4);
    if (num1 >= 1L)
      return string.Format(ScriptLocalization.Get("alliance_members_online_time_minutes", true), (object) num1);
    return string.Format(ScriptLocalization.Get("alliance_members_online_time_minutes", true), (object) 1);
  }

  public static string UnixTimeStmapToFormattedTime(int unixTimeStamp)
  {
    return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddSeconds((double) unixTimeStamp).ToString("yyyy/MM/dd HH:mm:ss");
  }

  public static DateTime UnixTimeStampToLocalDateTime(int unixTimeStamp)
  {
    return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddSeconds((double) unixTimeStamp);
  }

  public static string GetDragonTendencyDesc(DragonData dragonData)
  {
    DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(dragonData.Level);
    string empty = string.Empty;
    string Term1 = string.Empty;
    switch (dragonData.tendency)
    {
      case DragonData.Tendency.normal:
        Term1 = "dragon_character_neutral_0_name";
        break;
      case DragonData.Tendency.ferocious:
        Term1 = "dragon_character_dark_10_name";
        break;
      case DragonData.Tendency.swift:
        Term1 = "dragon_character_dark_5_name";
        break;
      case DragonData.Tendency.pure:
        Term1 = "dragon_character_light_5_name";
        break;
      case DragonData.Tendency.shine:
        Term1 = "dragon_character_light_10_name";
        break;
    }
    string Term2 = string.Format("dragon_stage_{0}_name", (object) dragonInfoByLevel.age);
    return ScriptLocalization.Get(Term1, true) + " " + ScriptLocalization.Get(Term2, true);
  }

  public static float GetScrollViewSize(UIScrollView scroll)
  {
    Bounds bounds = scroll.bounds;
    Vector2 min = (Vector2) bounds.min;
    Vector2 max = (Vector2) bounds.max;
    Vector2 zero = Vector2.zero;
    int num1 = Mathf.RoundToInt(scroll.panel.finalClipRegion.z);
    if ((num1 & 1) != 0)
      --num1;
    float num2 = Mathf.Round((float) num1 * 0.5f);
    if (scroll.panel.clipping == UIDrawCall.Clipping.SoftClip)
      num2 -= scroll.panel.clipSoftness.x;
    float num3 = max.x - min.x;
    return num2 * 2f;
  }

  public static Vector2 GetScrollViewCurrentContentSize(UIScrollView scroll)
  {
    Bounds bounds = scroll.bounds;
    Vector2 min = (Vector2) bounds.min;
    Vector2 max = (Vector2) bounds.max;
    Vector2 zero = Vector2.zero;
    Vector4 finalClipRegion = scroll.panel.finalClipRegion;
    int num1 = Mathf.RoundToInt(finalClipRegion.z);
    if ((num1 & 1) != 0)
      --num1;
    float num2 = Mathf.Round((float) num1 * 0.5f);
    if (scroll.panel.clipping == UIDrawCall.Clipping.SoftClip)
      num2 -= scroll.panel.clipSoftness.x;
    float num3 = max.x - min.x;
    float num4 = num2 * 2f;
    float x1 = min.x;
    float x2 = max.x;
    float num5 = finalClipRegion.x - num2;
    float num6 = finalClipRegion.x + num2;
    float num7 = num5 - x1;
    float num8 = x2 - num6;
    zero.x = num7;
    zero.y = num8;
    return zero;
  }

  public static List<T> Merge<T>(List<T> source, List<T> target)
  {
    List<T> objList = new List<T>((IEnumerable<T>) source);
    for (int index = 0; index < target.Count; ++index)
    {
      if (!objList.Contains(target[index]))
        objList.Add(target[index]);
    }
    return objList;
  }

  public static string GetResoureSpriteName(string resourName)
  {
    string str = string.Empty;
    string key = resourName;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAD == null)
      {
        // ISSUE: reference to a compiler-generated field
        Utils.\u003C\u003Ef__switch\u0024mapAD = new Dictionary<string, int>(4)
        {
          {
            "food",
            0
          },
          {
            "wood",
            1
          },
          {
            "ore",
            2
          },
          {
            "silver",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAD.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            str = "food_icon";
            break;
          case 1:
            str = "wood_icon";
            break;
          case 2:
            str = "ore_icon";
            break;
          case 3:
            str = "silver_icon";
            break;
        }
      }
    }
    return str;
  }

  public static int GetSignInDays()
  {
    StatsData statsData = DBManager.inst.DB_Stats.Get("sign_in");
    if (statsData != null)
    {
      int num = NetServerTime.inst.ServerTimestamp - (int) statsData.mtime / 86400 * 86400;
      if (num <= 172800 && (num <= 86400 || statsData.count < 7L))
        return (int) statsData.count;
    }
    return 0;
  }

  public static bool IsSignedIn()
  {
    StatsData statsData = DBManager.inst.DB_Stats.Get("sign_in");
    if (statsData != null)
      return NetServerTime.inst.ServerTimestamp - (int) statsData.mtime / 86400 * 86400 <= 86400;
    return false;
  }

  public static bool IsShowSevenDay()
  {
    if (DBManager.inst.DB_SevenDays.TotalDays <= 0)
      return false;
    int intByUid = PlayerPrefsEx.GetIntByUid("seven_days_show_time");
    bool flag = false;
    if (intByUid <= 0)
      flag = true;
    else if (NetServerTime.inst.ServerTimestamp - intByUid / 86400 * 86400 > 86400)
      flag = true;
    if (flag)
    {
      SevenDaysData sevenDaysData = DBManager.inst.DB_SevenDays.GetSevenDaysData(7);
      if (sevenDaysData != null)
        flag = !sevenDaysData.IsExpired;
    }
    return flag;
  }

  public static string GetIconSpriteName(int type)
  {
    switch (type)
    {
      case 0:
        return "icon_bookmark_marked";
      case 1:
        return "icon_bookmark_friend";
      case 2:
        return "icon_bookmark_enemy";
      default:
        return string.Empty;
    }
  }

  public static Vector3 Dampen(ref Vector3 velocity, float damping, float strength, float dt)
  {
    Vector3 zero = Vector3.zero;
    int num = Math.Max(1, (int) ((double) dt / 0.0166666675359011 + 0.5));
    for (int index = 0; index < num; ++index)
    {
      zero += velocity * 0.01666667f;
      velocity = (float) Math.Pow((double) damping, (double) strength * (double) dt) * velocity;
    }
    return zero;
  }

  public static FileMode GetReplaceFileModel(string path)
  {
    return File.Exists(path) ? FileMode.Truncate : FileMode.OpenOrCreate;
  }

  public static void WriteFile(string path, byte[] bytes)
  {
    FileStream fileStream = new FileStream(path, Utils.GetReplaceFileModel(path), FileAccess.Write);
    fileStream.Write(bytes, 0, bytes.Length);
    fileStream.Close();
  }

  public static void SetLabelOutFlowText(UILabel uiLabel)
  {
    string text = uiLabel.text;
    if (string.IsNullOrEmpty(text))
      return;
    string final = string.Empty;
    if (!uiLabel.Wrap(text, out final, uiLabel.height) && !string.IsNullOrEmpty(final))
      final = final.Substring(0, final.Length - 1) + "...";
    uiLabel.text = final;
  }

  public static QuestRewardAgent GetRewardAgent(long questId)
  {
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(questId);
    if (questData2 != null)
      return questData2.RewardAgent;
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(questId);
    if (timerQuestData != null)
      return timerQuestData.RewardAgent;
    return (QuestRewardAgent) null;
  }

  public static string GetBenefitsOneDesc(Dictionary<int, float> Benefits)
  {
    StringBuilder stringBuilder = new StringBuilder();
    if (Benefits != null)
    {
      Dictionary<int, float>.KeyCollection.Enumerator enumerator = Benefits.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current];
        float benefit = Benefits[enumerator.Current];
        if (dbProperty != null)
        {
          string str = "+";
          if (dbProperty.IsNegative)
            str = "-";
          stringBuilder.Append("[99ff00] ");
          stringBuilder.Append(str);
          switch (dbProperty.Format)
          {
            case PropertyDefinition.FormatType.Integer:
              stringBuilder.Append(benefit);
              break;
            case PropertyDefinition.FormatType.Percent:
              stringBuilder.Append(benefit * 100f);
              stringBuilder.Append("%");
              break;
            case PropertyDefinition.FormatType.Seconds:
              stringBuilder.Append(benefit);
              stringBuilder.Append("S");
              break;
            case PropertyDefinition.FormatType.Boolean:
              stringBuilder.Append(((double) benefit == 1.0).ToString());
              break;
            default:
              stringBuilder.Append(benefit);
              break;
          }
          stringBuilder.Append("[-] ");
          break;
        }
      }
    }
    return stringBuilder.ToString();
  }

  public static string GetBenefitsDesc(Dictionary<int, float> Benefits)
  {
    StringBuilder stringBuilder = new StringBuilder();
    if (Benefits != null)
    {
      Dictionary<int, float>.KeyCollection.Enumerator enumerator = Benefits.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current];
        float benefit = Benefits[enumerator.Current];
        if (dbProperty != null)
        {
          string str = "+";
          if (dbProperty.IsNegative)
            str = "-";
          stringBuilder.Append(dbProperty.Name);
          stringBuilder.Append("[99ff00] ");
          stringBuilder.Append(str);
          switch (dbProperty.Format)
          {
            case PropertyDefinition.FormatType.Integer:
              stringBuilder.Append(benefit);
              break;
            case PropertyDefinition.FormatType.Percent:
              stringBuilder.Append(benefit * 100f);
              stringBuilder.Append("%");
              break;
            case PropertyDefinition.FormatType.Seconds:
              stringBuilder.Append(benefit);
              stringBuilder.Append("S");
              break;
            case PropertyDefinition.FormatType.Boolean:
              stringBuilder.Append(((double) benefit == 1.0).ToString());
              break;
            default:
              stringBuilder.Append(benefit);
              break;
          }
          stringBuilder.Append("[-] ");
        }
      }
    }
    return stringBuilder.ToString();
  }

  public static string GetSkillValueDesc(LegendSkillInfo skill)
  {
    StringBuilder stringBuilder = new StringBuilder();
    bool flag = false;
    if (skill.Benefits != null)
    {
      Dictionary<int, float>.KeyCollection.Enumerator enumerator = skill.Benefits.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current];
        float benefit = skill.Benefits[enumerator.Current];
        if (dbProperty != null)
        {
          string str = "+";
          if ((double) benefit < 0.0)
            str = "-";
          stringBuilder.Append(dbProperty.Name);
          stringBuilder.Append("[99ff00] ");
          stringBuilder.Append(str);
          stringBuilder.Append(benefit * 100f);
          stringBuilder.Append("%[-] ");
          flag = true;
        }
      }
    }
    if (skill.OppBenefits != null && skill.OppBenefits != null)
    {
      Dictionary<int, float>.KeyCollection.Enumerator enumerator = skill.OppBenefits.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[enumerator.Current];
        float oppBenefit = skill.OppBenefits[enumerator.Current];
        if (dbProperty != null)
        {
          string str = "+";
          if ((double) oppBenefit < 0.0)
            str = string.Empty;
          stringBuilder.Append(dbProperty.Name);
          stringBuilder.Append("[99ff00] ");
          stringBuilder.Append(str);
          stringBuilder.Append(oppBenefit * 100f);
          stringBuilder.Append("%[-] ");
          flag = true;
        }
      }
    }
    if (!flag)
      stringBuilder.Append(skill.Loc_Effect);
    return stringBuilder.ToString();
  }

  public static T ParseEnum<T>(string value, bool ignoreCase = true)
  {
    try
    {
      return (T) Enum.Parse(typeof (T), value, ignoreCase);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Failed to parse enum for " + typeof (T).ToString() + " / " + value + " " + ex.ToString()));
      return (T) Enum.GetValues(typeof (T)).GetValue(0);
    }
  }

  public static bool CheckBoostIsStart(BoostType type)
  {
    return JobManager.Instance.GetSingleJobByClass(Utils.BoostType2JobEvent(type)) != null;
  }

  public static JobEvent BoostType2JobEvent(BoostType type)
  {
    JobEvent jobEvent = JobEvent.JOB_UNKNOWN;
    switch (type)
    {
      case BoostType.marchsize:
        jobEvent = JobEvent.JOB_MARCH_SIZE;
        break;
      case BoostType.attack:
        jobEvent = JobEvent.JOB_ATTACK_BOOST;
        break;
      case BoostType.defense:
        jobEvent = JobEvent.JOB_DEFENSE_BOOST;
        break;
      case BoostType.foodboost:
        jobEvent = JobEvent.JOB_RESOURCE_FOOD_BOOST;
        break;
      case BoostType.woodboost:
        jobEvent = JobEvent.JOB_RESOURCE_WOOD_BOOST;
        break;
      case BoostType.oreboost:
        jobEvent = JobEvent.JOB_RESOURCE_ORE_BOOST;
        break;
      case BoostType.silverboost:
        jobEvent = JobEvent.JOB_RESOURCE_SILVER_BOOST;
        break;
      case BoostType.gatherspeed:
        jobEvent = JobEvent.JOB_GATHER_SPEED_UP;
        break;
      case BoostType.upkeepreduction:
        jobEvent = JobEvent.JOB_UPKEEP_REDUCE;
        break;
      case BoostType.marchspeed:
        jobEvent = JobEvent.JOB_PVP_MARCHING;
        break;
      case BoostType.peaceshield:
        jobEvent = JobEvent.JOB_PEACE_SHIELD;
        break;
      case BoostType.antiscout:
        jobEvent = JobEvent.JOB_ANTI_SCOUT;
        break;
    }
    return jobEvent;
  }

  public static void EnableNGUIControlInteraction(GameObject ob, bool enable)
  {
    Collider componentInChildren = ob.GetComponentInChildren<Collider>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.enabled = enable;
  }

  public static BasePresent GetBasePresentByID(long questId)
  {
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(questId);
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(questId);
    AchievementData achievementData = DBManager.inst.DB_Achievements.Get((int) questId);
    BasePresent basePresent = (BasePresent) null;
    if (questData2 != null)
      basePresent = questData2.Present;
    else if (timerQuestData != null)
      basePresent = timerQuestData.Present;
    else if (achievementData != null)
      basePresent = achievementData.Present;
    return basePresent;
  }

  public static string GetUITextPath()
  {
    return "Texture/ItemIcons/";
  }

  public static void DeleteFile(string path)
  {
    if (!File.Exists(path))
      return;
    File.Delete(path);
  }

  public static bool CheckFile(string path)
  {
    return File.Exists(path);
  }

  public static string XLAT(string source)
  {
    return ScriptLocalization.Get(source, true);
  }

  public static string XLAT(string source, params object[] kvs)
  {
    if (kvs.Length % 2 != 0)
    {
      D.error((object) "Error: kvs pair should be an even number");
      return source;
    }
    Dictionary<string, string> para = new Dictionary<string, string>();
    int index = 0;
    while (index < kvs.Length)
    {
      string key = kvs[index].ToString();
      string str = kvs[index + 1].ToString();
      para.Add(key, str);
      index += 2;
    }
    return ScriptLocalization.GetWithPara(source, para, true);
  }

  public static Color GetColorFromHex(uint hex)
  {
    uint num1 = hex >> 16;
    uint num2 = hex - (num1 << 16);
    uint num3 = num2 >> 8;
    uint num4 = num2 - (num3 << 8);
    return new Color((float) num1 / (float) byte.MaxValue, (float) num3 / (float) byte.MaxValue, (float) num4 / (float) byte.MaxValue);
  }

  public static uint GetARGBFromColor32(Color32 color)
  {
    return (uint) (0 | (int) color.a << 24) | (uint) color.r << 16 | (uint) color.g << 8 | (uint) color.b;
  }

  public static uint GetRGBFromColor32(Color32 color)
  {
    return (uint) (0 | (int) color.r << 16) | (uint) color.g << 8 | (uint) color.b;
  }

  public static string GetRGBAFromColor(Color c)
  {
    return c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2") + c.a.ToString("X2");
  }

  public static string GetBenefitDescription(PropertyDefinition.FormatType format, double value, bool negativeIsGood = false, [Optional] Color32 goodColor, [Optional] Color32 badColor)
  {
    bool flag = value < 0.0;
    value = Math.Abs(value);
    string str = string.Empty;
    switch (format)
    {
      case PropertyDefinition.FormatType.Integer:
        str = value.ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        str = string.Format("{0:P}", (object) value);
        break;
    }
    if (!negativeIsGood)
    {
      if (flag)
        return Utils.GetColoredString("-" + str, badColor);
      return Utils.GetColoredString("+" + str, goodColor);
    }
    if (flag)
      return Utils.GetColoredString("-" + str, goodColor);
    return Utils.GetColoredString("+" + str, badColor);
  }

  public static string GetBenefitValueString(PropertyDefinition.FormatType format, double value)
  {
    bool flag = value < 0.0;
    value = Math.Abs(value);
    string str = string.Empty;
    switch (format)
    {
      case PropertyDefinition.FormatType.Integer:
        str = value.ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        value = Math.Round(value * 100.0, 2);
        str = value.ToString() + "%";
        break;
    }
    if (flag)
      return "-" + str;
    return "+" + str;
  }

  public static string GetColoredString(string content, [Optional] Color32 color)
  {
    return string.Format("[{0}]{1}[-]", (object) Utils.GetTintString(color), (object) content);
  }

  public static string GetTintString(Color32 color)
  {
    return Utils.GetRGBFromColor32(color).ToString("X2");
  }

  public static string GetRomaNumeralsBelowTen(int i)
  {
    switch (i)
    {
      case 1:
        return "I";
      case 2:
        return "II";
      case 3:
        return "III";
      case 4:
        return "IV";
      case 5:
        return "V";
      case 6:
        return "VI";
      case 7:
        return "VII";
      case 8:
        return "VIII";
      case 9:
        return "IX";
      case 10:
        return "X";
      case 11:
        return "XI";
      case 12:
        return "XII";
      case 13:
        return "XIII";
      case 14:
        return "XIV";
      case 15:
        return "XV";
      case 16:
        return "XVI";
      case 17:
        return "XVII";
      case 18:
        return "XVIII";
      case 19:
        return "XIX";
      case 20:
        return "XX";
      default:
        return string.Empty;
    }
  }

  public static string ConvertNumberToNormalString(long num)
  {
    return string.Format("{0:N0}", (object) num);
  }

  public static string ConvertNumberToNormalString2(double num)
  {
    return string.Format("{0:N1}", (object) num);
  }

  public static string ConvertNumberToNormalString3(double num)
  {
    num += 9.99999974737875E-05;
    if ((int) (num * 10.0) % 10 != 0)
      return string.Format("{0:N1}", (object) num);
    return ((int) num).ToString();
  }

  public static string ConvertNumberToNormalString(int num)
  {
    return string.Format("{0:N0}", (object) num);
  }

  public static string ConvertSecsToString(double seconds)
  {
    DateTime dateTime1 = new DateTime();
    if (seconds < 0.0)
      seconds = 0.0;
    DateTime dateTime2 = dateTime1.AddSeconds(seconds);
    int days = new TimeSpan(dateTime2.Ticks).Days;
    string empty = string.Empty;
    string str;
    if (days > 0)
      str = string.Format("{0:0}d {1:00}:{2:00}:{3:00}", (object) days, (object) dateTime2.Hour, (object) dateTime2.Minute, (object) dateTime2.Second);
    else
      str = dateTime2.Hour <= 0 ? string.Format("{0:0}:{1:00}", (object) dateTime2.Minute, (object) dateTime2.Second) : string.Format("{0:0}:{1:00}:{2:00}", (object) dateTime2.Hour, (object) dateTime2.Minute, (object) dateTime2.Second);
    return str;
  }

  public static void SnapAndScale(UITexture tex, float newScale)
  {
    tex.MakePixelPerfect();
    tex.gameObject.transform.localScale = new Vector3(newScale, newScale, newScale);
  }

  public static string FormatTime1(int time)
  {
    int num1 = time / 3600;
    int num2 = num1 / 24;
    int num3 = num1 % 24;
    string str1 = num2 <= 0 ? string.Empty : num2.ToString() + "d ";
    string str2 = num3 < 10 ? string.Format("0{0}:", (object) num3) : num3.ToString() + ":";
    int num4 = time % 3600 / 60;
    string str3 = num4 < 10 ? string.Format("0{0}:", (object) num4) : num4.ToString() + ":";
    int num5 = time % 60;
    string str4 = num5 < 10 ? string.Format("0{0}", (object) num5) : num5.ToString();
    return string.Format("{0}{1}{2}{3}", (object) str1, (object) str2, (object) str3, (object) str4);
  }

  public static string FormatTime(int time, bool hadDay = false, bool translate = false, bool hadSecond = true)
  {
    if (time < 0)
      time = 0;
    int num1 = time / 3600;
    int num2 = num1 / 24;
    if (hadDay)
      num1 %= 24;
    string str1 = "d ";
    if (translate)
      str1 = ScriptLocalization.Get("id_day_abbrev", true) + " ";
    string str2 = num2 <= 0 ? string.Empty : num2.ToString() + str1;
    string str3 = num1 <= 0 ? (!hadSecond ? "0:" : string.Empty) : num1.ToString() + ":";
    int num3 = time % 3600 / 60;
    string str4 = !hadSecond ? string.Empty : ":";
    string str5 = num3 < 10 ? (num1 <= 0 ? (num3 < 0 ? string.Empty : num3.ToString() + str4) : "0" + num3.ToString() + str4) : num3.ToString() + str4;
    int num4 = time % 60;
    string str6 = num4 < 10 ? string.Format("0{0}", (object) num4) : num4.ToString();
    string str7 = !hadSecond ? string.Format("{0}{1}", (object) str3, (object) str5) : string.Format("{0}{1}{2}", (object) str3, (object) str5, (object) str6);
    if (hadDay)
    {
      string str8;
      if (hadSecond)
        str8 = string.Format("{0}{1}{2}{3}", (object) str2, (object) str3, (object) str5, (object) str6);
      else
        str8 = string.Format("{0}{1}{2}", (object) str2, (object) str3, (object) str5);
      str7 = str8;
    }
    return str7;
  }

  public static void SafeDestroy(ref GameObject obj)
  {
    if (!((UnityEngine.Object) obj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) obj);
    obj = (GameObject) null;
  }

  public static void SafeDestroy(string gobName)
  {
    GameObject gameObject = GameObject.Find(gobName);
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) gameObject);
  }

  public static GameObject DuplicateGOB(GameObject src)
  {
    return Utils.DuplicateGOB(src, src.transform.parent);
  }

  public static GameObject DuplicateGOB(GameObject src, Transform parent)
  {
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(src);
    if ((UnityEngine.Object) null != (UnityEngine.Object) parent)
      NGUITools.SetLayer(go, parent.gameObject.layer);
    go.transform.parent = parent;
    go.transform.localPosition = src.transform.localPosition;
    go.transform.localScale = src.transform.localScale;
    return go;
  }

  public static string GetMD5HashString(string input)
  {
    byte[] hash = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < hash.Length; ++index)
      stringBuilder.Append(hash[index].ToString("x2"));
    return stringBuilder.ToString();
  }

  public static void DestroyNonNull(GameObject obj)
  {
    if (!((UnityEngine.Object) obj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) obj);
  }

  public static void TraceGOB(GameObject gob)
  {
    GameObject gameObject = gob;
    string str = gameObject.name;
    while ((UnityEngine.Object) gameObject.transform.parent != (UnityEngine.Object) null)
    {
      gameObject = gameObject.transform.parent.gameObject;
      str = gameObject.name + "/" + str;
    }
  }

  public static string GetGOBPath(GameObject gob)
  {
    GameObject gameObject = gob;
    string str = gameObject.name;
    while ((UnityEngine.Object) gameObject.transform.parent != (UnityEngine.Object) null)
    {
      gameObject = gameObject.transform.parent.gameObject;
      str = gameObject.name + "/" + str;
    }
    return str;
  }

  public static void SetText(string gobName, string newText)
  {
    TextMesh component = GameObject.Find(gobName).GetComponent<TextMesh>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.text = newText;
  }

  public static T FindClass<T>() where T : UnityEngine.Object
  {
    return UnityEngine.Object.FindObjectOfType(typeof (T)) as T;
  }

  public static T FindChildClass<T>(GameObject unityObject, string childName) where T : UnityEngine.Object
  {
    return (object) GameObject.Find(Utils.GetGOBPath(unityObject) + childName).GetComponent(typeof (T)) as T;
  }

  public static void ShowChildRenderers(GameObject parent, bool bShow)
  {
    if ((UnityEngine.Object) parent.GetComponent<Renderer>() != (UnityEngine.Object) null)
      parent.GetComponent<Renderer>().enabled = bShow;
    for (int index = 0; index < parent.transform.childCount; ++index)
      Utils.ShowChildRenderers(parent.transform.GetChild(index).gameObject, bShow);
  }

  public static void ColorFamily(GameObject parent, float colMultiplier)
  {
    ColorMultiplier component = parent.GetComponent<ColorMultiplier>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.MultiplyColor(colMultiplier);
    for (int index = 0; index < parent.transform.childCount; ++index)
      Utils.ColorFamily(parent.transform.GetChild(index).gameObject, colMultiplier);
  }

  public static void RecursiveAlpha(GameObject parent, float alpha)
  {
    UITexture component = parent.GetComponent<UITexture>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.alpha = alpha;
    for (int index = 0; index < parent.transform.childCount; ++index)
      Utils.RecursiveAlpha(parent.transform.GetChild(index).gameObject, alpha);
  }

  public static float GraphLerp(float sourceA, float sourceB, float destA, float destB, float val)
  {
    float num = (float) (((double) val - (double) sourceA) / ((double) sourceB - (double) sourceA));
    return destA + (destB - destA) * num;
  }

  public static void SetX(GameObject gob, float newX)
  {
    Vector3 position = gob.transform.position;
    position.x = newX;
    gob.transform.position = position;
  }

  public static void SetY(GameObject gob, float newY)
  {
    Vector3 position = gob.transform.position;
    position.y = newY;
    gob.transform.position = position;
  }

  public static void SetZ(GameObject gob, float newZ)
  {
    Vector3 position = gob.transform.position;
    position.z = newZ;
    gob.transform.position = position;
  }

  public static void SetLPX(GameObject gob, float newX)
  {
    Vector3 localPosition = gob.transform.localPosition;
    localPosition.x = newX;
    gob.transform.localPosition = localPosition;
  }

  public static void SetLPY(GameObject gob, float newY)
  {
    if ((UnityEngine.Object) gob == (UnityEngine.Object) null)
      return;
    Vector3 localPosition = gob.transform.localPosition;
    localPosition.y = newY;
    gob.transform.localPosition = localPosition;
  }

  public static void SetLPZ(GameObject gob, float newZ)
  {
    Vector3 localPosition = gob.transform.localPosition;
    localPosition.z = newZ;
    gob.transform.localPosition = localPosition;
  }

  public static void SetLSX(GameObject gob, float newX)
  {
    Vector3 localScale = gob.transform.localScale;
    localScale.x = newX;
    gob.transform.localScale = localScale;
  }

  public static void SetLSY(GameObject gob, float newY)
  {
    Vector3 localScale = gob.transform.localScale;
    localScale.y = newY;
    gob.transform.localScale = localScale;
  }

  public static void SetLSZ(GameObject gob, float newZ)
  {
    Vector3 localScale = gob.transform.localScale;
    localScale.z = newZ;
    gob.transform.localScale = localScale;
  }

  public static Coroutine ExecuteInSecs(float timeDelay, System.Action onFinished)
  {
    if ((bool) ((UnityEngine.Object) GameDataManager.inst))
      return GameDataManager.inst.StartCoroutine(Utils._ExecuteInSecs(timeDelay, onFinished));
    return (Coroutine) null;
  }

  public static Coroutine ExecuteInIntervalTime(float timeInterval, System.Action onFinished)
  {
    if ((bool) ((UnityEngine.Object) GameDataManager.inst))
      return GameDataManager.inst.StartCoroutine(Utils._ExecuteInIntervalTime(timeInterval, onFinished));
    return (Coroutine) null;
  }

  public static void StopCoroutine(Coroutine coroutine)
  {
    if (coroutine == null || !(bool) ((UnityEngine.Object) GameDataManager.inst))
      return;
    GameDataManager.inst.StopCoroutine(coroutine);
  }

  [DebuggerHidden]
  private static IEnumerator _ExecuteInSecs(float timeDelay, System.Action onFinished)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Utils.\u003C_ExecuteInSecs\u003Ec__IteratorB8()
    {
      timeDelay = timeDelay,
      onFinished = onFinished,
      \u003C\u0024\u003EtimeDelay = timeDelay,
      \u003C\u0024\u003EonFinished = onFinished
    };
  }

  [DebuggerHidden]
  private static IEnumerator _ExecuteInIntervalTime(float timeInterval, System.Action onFinished)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Utils.\u003C_ExecuteInIntervalTime\u003Ec__IteratorB9()
    {
      timeInterval = timeInterval,
      onFinished = onFinished,
      \u003C\u0024\u003EtimeInterval = timeInterval,
      \u003C\u0024\u003EonFinished = onFinished
    };
  }

  public static void ExecuteAtTheEndOfFrame(System.Action finish)
  {
    if (!(bool) ((UnityEngine.Object) GameDataManager.inst))
      return;
    GameDataManager.inst.StartCoroutine(Utils._ExecuteAtTheEndOfFrame(finish));
  }

  [DebuggerHidden]
  private static IEnumerator _ExecuteAtTheEndOfFrame(System.Action finish)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Utils.\u003C_ExecuteAtTheEndOfFrame\u003Ec__IteratorBA()
    {
      finish = finish,
      \u003C\u0024\u003Efinish = finish
    };
  }

  public static float RndRange(float min, float max)
  {
    return UnityEngine.Random.Range(min, max);
  }

  public static int RndRange(int min, int max)
  {
    return UnityEngine.Random.Range(min, max);
  }

  public static void Clamp<T>(ref T value, T low, T high) where T : IComparable
  {
    if (value.CompareTo((object) low) < 0)
    {
      value = low;
    }
    else
    {
      if (value.CompareTo((object) high) <= 0)
        return;
      value = high;
    }
  }

  public static string ZeroPad(int src, int padSize)
  {
    string str = src.ToString();
    while (str.Length < padSize)
      str = "0" + str;
    return str;
  }

  public static Hashtable Hash(params object[] args)
  {
    Hashtable hashtable = new Hashtable(args.Length / 2);
    if (args.Length % 2 != 0)
    {
      Debug.LogError((object) "Error: Hash requires an even number of arguments!");
      return (Hashtable) null;
    }
    int index = 0;
    while (index < args.Length - 1)
    {
      hashtable.Add(args[index], args[index + 1]);
      index += 2;
    }
    return hashtable;
  }

  public static void ReflectFromHash(Hashtable ht, object obj)
  {
    System.Type type = obj.GetType();
    foreach (DictionaryEntry dictionaryEntry in ht)
    {
      if (dictionaryEntry.Value != null)
      {
        FieldInfo field = type.GetField(dictionaryEntry.Key.ToString());
        if (field != null)
        {
          if (field.FieldType == typeof (int))
            field.SetValue(obj, (object) int.Parse(dictionaryEntry.Value.ToString()));
          else if (field.FieldType == typeof (float))
            field.SetValue(obj, (object) float.Parse(dictionaryEntry.Value.ToString()));
          else if (field.FieldType == typeof (string))
            field.SetValue(obj, dictionaryEntry.Value);
        }
      }
    }
  }

  public static void TransformAAPoints(float rot, float w, float h, float xscale, float yscale, bool bGlobalScale, float offx, float offy, out AAPoints temp)
  {
    temp = new AAPoints();
    float num1 = Mathf.Sin((float) (-(double) rot * 3.14159274101257 / 180.0));
    float num2 = Mathf.Cos((float) (-(double) rot * 3.14159274101257 / 180.0));
    float num3 = xscale;
    float num4 = yscale;
    if (bGlobalScale)
    {
      num3 = 1f;
      num4 = 1f;
    }
    float num5 = (float) (-(double) w / 2.0) + offx;
    float num6 = (float) (-(double) h / 2.0) + offy;
    float num7 = (float) ((double) num5 * (double) num2 * (double) num3 - (double) num6 * (double) num1 * (double) num4);
    float num8 = (float) ((double) num5 * (double) num1 * (double) num3 + (double) num6 * (double) num2 * (double) num4);
    temp.x1 = num7;
    temp.y1 = num8;
    float num9 = w / 2f + offx;
    float num10 = (float) (-(double) h / 2.0) + offy;
    float num11 = (float) ((double) num9 * (double) num2 * (double) num3 - (double) num10 * (double) num1 * (double) num4);
    float num12 = (float) ((double) num9 * (double) num1 * (double) num3 + (double) num10 * (double) num2 * (double) num4);
    temp.x2 = num11;
    temp.y2 = num12;
    float num13 = w / 2f + offx;
    float num14 = h / 2f + offy;
    float num15 = (float) ((double) num13 * (double) num2 * (double) num3 - (double) num14 * (double) num1 * (double) num4);
    float num16 = (float) ((double) num13 * (double) num1 * (double) num3 + (double) num14 * (double) num2 * (double) num4);
    temp.x3 = num15;
    temp.y3 = num16;
    float num17 = (float) (-(double) w / 2.0) + offx;
    float num18 = h / 2f + offy;
    float num19 = (float) ((double) num17 * (double) num2 * (double) num3 - (double) num18 * (double) num1 * (double) num4);
    float num20 = (float) ((double) num17 * (double) num1 * (double) num3 + (double) num18 * (double) num2 * (double) num4);
    temp.x4 = num19;
    temp.y4 = num20;
    if (!bGlobalScale)
      return;
    temp.x1 *= xscale;
    temp.y1 *= yscale;
    temp.x2 *= xscale;
    temp.y2 *= yscale;
    temp.x3 *= xscale;
    temp.y3 *= yscale;
    temp.x4 *= xscale;
    temp.y4 *= yscale;
  }

  public static string FormatThousands(string src)
  {
    string str = string.Empty;
    if (src.IndexOf(".") > 0)
    {
      src = src.Substring(0, src.IndexOf(".") + 2);
      int num1 = 0;
      bool flag = false;
      int num2 = 0;
      for (int startIndex = src.Length - 1; startIndex >= 0; --startIndex)
      {
        str = src.Substring(startIndex, 1) + str;
        if (flag)
          ++num1;
        if (src.Substring(startIndex, 1) == ".")
          flag = true;
        if (num1 % 3 == 0 && num2 > 0 && (startIndex > 0 && src.Substring(startIndex - 1, 1) != "-"))
          str = "," + str;
        if (flag)
          ++num2;
      }
    }
    else
    {
      int num1 = 0;
      int num2 = 0;
      for (int startIndex = src.Length - 1; startIndex >= 0; --startIndex)
      {
        str = src.Substring(startIndex, 1) + str;
        ++num1;
        if (num1 % 3 == 0 && num2 > 0 && (startIndex > 0 && src.Substring(startIndex - 1, 1) != "-"))
          str = "," + str;
        ++num2;
      }
    }
    return str;
  }

  public static string FormatShortThousands(int src)
  {
    string empty = string.Empty;
    return src < 1000000 ? (src < 1000 ? src.ToString() : string.Format("{0:0.#}K", (object) ((float) src / 1000f))) : Utils.FormatThousands(((float) src / 1000000f).ToString()) + "M";
  }

  public static string FormatShortThousandsLong(long src)
  {
    string empty = string.Empty;
    return src < 1000000000L ? (src < 1000000L ? (src < 1000L ? src.ToString() : string.Format("{0:0.#}K", (object) ((float) src / 1000f))) : Utils.FormatThousands(((float) src / 1000000f).ToString()) + "M") : Utils.FormatThousands(((float) src / 1E+09f).ToString()) + "B";
  }

  public static void SetScreen()
  {
    int width1 = 1024;
    int height1 = 768;
    if (Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.OSXWebPlayer || (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor))
      return;
    Resolution resolution = Screen.resolutions[Screen.resolutions.Length - 1];
    float num = (float) resolution.width / (float) resolution.height;
    for (int index = 0; index < Screen.resolutions.Length; ++index)
    {
      if (Screen.resolutions[index].width >= 1024 && (double) Mathf.Abs((float) Screen.resolutions[index].width / (float) Screen.resolutions[index].height - num) < 0.00999999977648258)
      {
        width1 = Screen.resolutions[index].width;
        height1 = Screen.resolutions[index].height;
      }
    }
    int width2 = (int) ((double) width1 * 0.75);
    int height2 = (int) ((double) width1 / 1.33333337306976);
    if (PlayerPrefsEx.GetInt("Fullscreen", 1) > 0)
      Screen.SetResolution(width1, height1, true);
    else
      Screen.SetResolution(width2, height2, false);
  }

  public static Transform SearchHierarchyForBone(Transform inRoot, string inName, bool ignoreDisabled = false)
  {
    if ((UnityEngine.Object) inRoot == (UnityEngine.Object) null || inName.Length <= 0)
      return (Transform) null;
    if (inRoot.name.Equals(inName))
      return inRoot;
    Queue<Transform> transformQueue = new Queue<Transform>(16);
    transformQueue.Enqueue(inRoot);
    while (transformQueue.Count > 0)
    {
      Transform transform1 = transformQueue.Dequeue();
      Transform transform2 = transform1.Find(inName);
      if ((bool) ((UnityEngine.Object) transform2) && (!ignoreDisabled || transform2.gameObject.activeInHierarchy))
        return transform2;
      int childCount = transform1.childCount;
      for (int index = 0; index < childCount; ++index)
        transformQueue.Enqueue(transform1.GetChild(index));
    }
    return (Transform) null;
  }

  public static double ClientSideUnixUTCSeconds
  {
    get
    {
      return DateTime.UtcNow.Subtract(Utils.time19700101).TotalSeconds;
    }
  }

  public static double ClientSideUnixUTCMillSeconds
  {
    get
    {
      return DateTime.UtcNow.Subtract(Utils.time19700101).TotalMilliseconds;
    }
  }

  public static DateTime ServerTime2DateTime(long systemTime)
  {
    return new DateTime(Utils.time19700101.Ticks + systemTime * 10000000L).ToLocalTime();
  }

  public static long DateTime2ServerTime(DateTime dateTime)
  {
    return (long) (dateTime - Utils.time19700101).TotalSeconds;
  }

  public static long DateTime2ServerTime(string yymmdd, string divideSymbol)
  {
    if (string.IsNullOrEmpty(yymmdd))
      return 0;
    int result1 = 0;
    int result2 = 0;
    int result3 = 0;
    if (string.IsNullOrEmpty(divideSymbol))
    {
      int.TryParse(yymmdd.Substring(0, 4), out result1);
      int.TryParse(yymmdd.Substring(4, 2), out result2);
      int.TryParse(yymmdd.Substring(6, 2), out result3);
    }
    else
    {
      string[] strArray = yymmdd.Split(divideSymbol.ToCharArray());
      if (strArray == null || strArray.Length != 3)
        return 0;
      int.TryParse(strArray[0], out result1);
      int.TryParse(strArray[1], out result2);
      int.TryParse(strArray[2], out result3);
    }
    return Utils.DateTime2ServerTime(new DateTime(result1, result2, result3));
  }

  public static long DateTime2ServerTime(string yymmddhhmmss)
  {
    if (string.IsNullOrEmpty(yymmddhhmmss))
      return 0;
    int result1 = 0;
    int result2 = 0;
    int result3 = 0;
    int result4 = 0;
    int result5 = 0;
    int result6 = 0;
    string[] strArray1 = yymmddhhmmss.Split(' ');
    if (strArray1 != null && strArray1.Length > 0)
    {
      string[] strArray2 = strArray1[0].Split('-');
      if (strArray2 != null && strArray2.Length == 3)
      {
        int.TryParse(strArray2[0], out result1);
        int.TryParse(strArray2[1], out result2);
        int.TryParse(strArray2[2], out result3);
      }
    }
    if (strArray1 != null && strArray1.Length > 1)
    {
      string[] strArray2 = strArray1[1].Split(':');
      if (strArray2 != null)
      {
        if (strArray2.Length == 3)
        {
          int.TryParse(strArray2[0], out result4);
          int.TryParse(strArray2[1], out result5);
          int.TryParse(strArray2[2], out result6);
        }
        else if (strArray2.Length == 2)
        {
          int.TryParse(strArray2[0], out result4);
          int.TryParse(strArray2[1], out result5);
        }
        else if (strArray2.Length == 1)
          int.TryParse(strArray2[0], out result4);
      }
    }
    return Utils.DateTime2ServerTime(new DateTime(result1, result2, result3, result4, result5, result6));
  }

  public static string GetTimeHHMM(string timeStr)
  {
    string[] strArray1 = timeStr.Split(':');
    string[] strArray2 = timeStr.Split(' ');
    if (strArray2 != null && strArray2.Length == 2)
      strArray1 = strArray2[1].Split(':');
    if (strArray1 != null && strArray1.Length >= 2)
      return string.Format("{0}:{1}", (object) strArray1[0], (object) strArray1[1]);
    return string.Empty;
  }

  public static string FormatTimeYYYYMMDD(long systemTime, string divideSymbol = "/")
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    return string.Format("{2}{3}{0}{3}{1}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Year.ToString("00"), (object) divideSymbol);
  }

  public static string FormatTimeForMail(long systemTime)
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    return string.Format("{4}/{0}/{1} {2}:{3}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Year.ToString("00"));
  }

  public static string FormatTimeForSystemSetting(long systemTime)
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    return string.Format("{4}-{0}-{1} {2}:{3}:{5}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Year.ToString("00"), (object) dateTime.Second.ToString("00"));
  }

  public static string FormatTimeMMDDHHMMSS(long systemTime, string separator = "/")
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    return string.Format("{0}{5}{1} {2}:{3}:{4}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Second.ToString("00"), (object) separator);
  }

  public static string FormatTimeYYYYMMDDHHMMSS(long systemTime, string separator = "/")
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    return string.Format("{5}{6}{0}{6}{1} {2}:{3}:{4}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Second.ToString("00"), (object) dateTime.Year.ToString("0000"), (object) separator);
  }

  public static string FormatTimeToUTC(long systemTime)
  {
    DateTime dateTime = Utils.ServerTime2DateTime(systemTime);
    dateTime = dateTime.ToUniversalTime();
    return string.Format("{4}-{0}-{1} {2}:{3}:{5}", (object) dateTime.Month.ToString("00"), (object) dateTime.Day.ToString("00"), (object) dateTime.Hour.ToString("00"), (object) dateTime.Minute.ToString("00"), (object) dateTime.Year.ToString("00"), (object) dateTime.Second.ToString("00"));
  }

  public static float AngleAroundAxis(Vector3 inDirA, Vector3 inDirB, Vector3 inAxis)
  {
    inDirA -= Vector3.Project(inDirA, inAxis);
    inDirB -= Vector3.Project(inDirB, inAxis);
    return Vector3.Angle(inDirA, inDirB) * ((double) Vector3.Dot(inAxis, Vector3.Cross(inDirA, inDirB)) >= 0.0 ? 1f : -1f);
  }

  public static float LineToSphereTest(Vector3 inLineStart, Vector3 inLineNormal, float inDistance, Vector3 inSphereCenter, float inSphereRadius)
  {
    Vector3 rhs = inSphereCenter - inLineStart;
    float sqrMagnitude1 = rhs.sqrMagnitude;
    float num1 = inSphereRadius * inSphereRadius;
    if ((double) num1 >= (double) sqrMagnitude1)
      return 0.0f;
    float num2 = Vector3.Dot(inLineNormal, rhs);
    if ((double) num2 <= 0.0)
      return -1f;
    float sqrMagnitude2 = (inLineStart + inLineNormal * num2 - inSphereCenter).sqrMagnitude;
    if ((double) num1 < (double) sqrMagnitude2)
      return -1f;
    if ((double) num1 > (double) sqrMagnitude2)
      num2 -= Mathf.Sqrt(num1 - sqrMagnitude2);
    if ((double) num2 > (double) inDistance)
      return -1f;
    return num2;
  }

  [DebuggerHidden]
  public static IEnumerator DoUILabelInput(UILabel targetLabel, string placeholder, int maxChars, TouchScreenKeyboardType kbdType, System.Action onChange, System.Action onFinished)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Utils.\u003CDoUILabelInput\u003Ec__IteratorBB()
    {
      targetLabel = targetLabel,
      placeholder = placeholder,
      kbdType = kbdType,
      maxChars = maxChars,
      onChange = onChange,
      onFinished = onFinished,
      \u003C\u0024\u003EtargetLabel = targetLabel,
      \u003C\u0024\u003Eplaceholder = placeholder,
      \u003C\u0024\u003EkbdType = kbdType,
      \u003C\u0024\u003EmaxChars = maxChars,
      \u003C\u0024\u003EonChange = onChange,
      \u003C\u0024\u003EonFinished = onFinished
    };
  }

  public static string PrintObjectMembers(object obj)
  {
    string str1 = string.Empty;
    System.Type type = obj.GetType();
    foreach (PropertyInfo property in type.GetProperties())
      str1 = str1 + property.Name + ": " + property.GetValue(obj, new object[0]) + ", ";
    string str2 = string.Empty;
    foreach (FieldInfo field in type.GetFields())
      str2 = str2 + field.Name + ": " + field.GetValue(obj) + ", ";
    return string.Format("[{0} fields=[{1}], properties=[{2}]]", (object) type, (object) str2, (object) str1);
  }

  public static bool HasUpperCase(string str)
  {
    if (string.IsNullOrEmpty(str))
      return false;
    for (int index = 0; index < str.Length; ++index)
    {
      if (char.IsUpper(str[index]))
        return true;
    }
    return false;
  }

  public static byte[] EncodeByGzip(byte[] data)
  {
    if (data == null || Utils.IsGzipData(data))
      return data;
    MemoryStream memoryStream = new MemoryStream();
    Stream stream = (Stream) new GZipOutputStream((Stream) memoryStream);
    stream.Write(data, 0, data.Length);
    stream.Close();
    byte[] array = memoryStream.ToArray();
    memoryStream.Flush();
    memoryStream.Close();
    return array;
  }

  public static byte[] DecodeByGzip(byte[] data)
  {
    if (data == null || !Utils.IsGzipData(data))
      return data;
    GZipInputStream gzipInputStream = (GZipInputStream) null;
    MemoryStream memoryStream = (MemoryStream) null;
    try
    {
      gzipInputStream = new GZipInputStream((Stream) new MemoryStream(data));
      memoryStream = new MemoryStream();
      byte[] buffer = new byte[4096];
      int count;
      while ((count = gzipInputStream.Read(buffer, 0, buffer.Length)) != 0)
        memoryStream.Write(buffer, 0, count);
      memoryStream.Flush();
      return memoryStream.ToArray();
    }
    catch (Exception ex)
    {
      D.error((object) ("[DecodeByGzip]" + ex.Message));
    }
    finally
    {
      if (gzipInputStream != null)
        gzipInputStream.Close();
      if (memoryStream != null)
        memoryStream.Close();
    }
    return data;
  }

  public static bool IsGzipData(byte[] data)
  {
    if (data == null || data.Length < 2)
      return false;
    return ((int) data[0] << 8 | (int) data[1] & (int) byte.MaxValue) == 8075;
  }

  public static string ByteArray2String(byte[] bytes)
  {
    if (bytes != null)
      return Encoding.UTF8.GetString(bytes);
    return (string) null;
  }

  public static string GetParentFullName(Transform target)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) target)
      return (string) null;
    List<string> stringList = new List<string>();
    stringList.Add(target.name);
    for (; (UnityEngine.Object) null != (UnityEngine.Object) target.parent; target = target.parent)
      stringList.Add(target.parent.name);
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = stringList.Count - 1; index >= 0; --index)
    {
      stringBuilder.Append(stringList[index]);
      if (index != 0)
        stringBuilder.Append("/");
    }
    return stringBuilder.ToString();
  }

  public static void ArrangePositionMode1(Vector3 oripos, int count, float gapX, float gapY, ref List<Vector3> tarpos, int maxinoneline)
  {
    if (maxinoneline != 0)
    {
      if (count >= maxinoneline)
      {
        List<Vector3> tarpos1 = new List<Vector3>(maxinoneline);
        Utils.ArrangePosition(oripos, maxinoneline, gapX, 0.0f, ref tarpos1);
        for (int index1 = 0; index1 < count; ++index1)
        {
          int index2 = index1 % maxinoneline;
          int num = index1 / maxinoneline;
          tarpos.Add(new Vector3()
          {
            x = tarpos1[index2].x,
            y = tarpos1[index2].y + (float) num * gapY
          });
        }
      }
      else
        Utils.ArrangePosition(oripos, count, gapX, 0.0f, ref tarpos);
    }
    else
      Utils.ArrangePosition(oripos, count, gapX, gapY, ref tarpos);
  }

  public static void ArrangePositionMode2(Vector3 oripos, int count, float gapX, float gapY, ref List<Vector3> tarpos, int maxinoneline)
  {
    if (maxinoneline != 0)
    {
      if (count >= maxinoneline)
      {
        int num1 = count / 2;
        if (2 * num1 < count)
          ++num1;
        int num2 = count - num1;
        List<Vector3> tarpos1 = new List<Vector3>(num1);
        List<Vector3> tarpos2 = new List<Vector3>(num2);
        Utils.ArrangePosition(oripos, num1, gapX, 0.0f, ref tarpos1);
        oripos.y += gapY;
        Utils.ArrangePosition(oripos, num2, gapX, 0.0f, ref tarpos2);
        tarpos.AddRange((IEnumerable<Vector3>) tarpos1);
        tarpos.AddRange((IEnumerable<Vector3>) tarpos2);
      }
      else
        Utils.ArrangePosition(oripos, count, gapX, 0.0f, ref tarpos);
    }
    else
      Utils.ArrangePosition(oripos, count, gapX, gapY, ref tarpos);
  }

  public static void ArrangePosition(Vector3 oripos, int count, float gapX, float gapY, ref List<Vector3> tarpos)
  {
    for (int index = 0; index < count; ++index)
      tarpos.Add(oripos);
    if (count % 2 == 0)
    {
      for (int index = count / 2 - 1; index >= 0; --index)
      {
        if (count / 2 - 1 == index)
        {
          tarpos[index] = new Vector3(oripos.x - gapX / 2f, oripos.y, oripos.z);
          tarpos[count - 1 - index] = new Vector3(oripos.x + gapX / 2f, oripos.y, oripos.z);
        }
        else
        {
          Vector3 vector3_1 = tarpos[index + 1];
          Vector3 vector3_2 = tarpos[count - 1 - index - 1];
          tarpos[index] = new Vector3(vector3_1.x - gapX, vector3_1.y + gapY, vector3_1.z);
          tarpos[count - 1 - index] = new Vector3(vector3_2.x + gapX, vector3_2.y + gapY, vector3_2.z);
        }
      }
    }
    else
    {
      tarpos[count / 2] = oripos;
      for (int index = count / 2 - 1; index >= 0; --index)
      {
        Vector3 vector3_1 = tarpos[index + 1];
        Vector3 vector3_2 = tarpos[count - 1 - index - 1];
        tarpos[index] = new Vector3(vector3_1.x - gapX, vector3_1.y + gapY, vector3_1.z);
        tarpos[count - 1 - index] = new Vector3(vector3_2.x + gapX, vector3_2.y + gapY, vector3_2.z);
      }
    }
  }

  public static void PopUpCommingSoon()
  {
    UIManager.inst.OpenPopup("SingleButtonPopup", (Popup.PopupParameter) new SingleButtonPopup.Parameter()
    {
      setType = (SingleButtonPopup.SetType.title | SingleButtonPopup.SetType.description | SingleButtonPopup.SetType.buttonEvent | SingleButtonPopup.SetType.buttonText),
      title = Utils.XLAT("coming_soon_popup_title"),
      description = Utils.XLAT("coming_soon_popup_description"),
      buttonText = Utils.XLAT("coming_soon_popup_button"),
      buttonClickEvent = (System.Action) null
    });
  }

  public static void PlayParticles(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    obj.SetActive(true);
    ParticleSystem[] componentsInChildren = obj.GetComponentsInChildren<ParticleSystem>();
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      componentsInChildren[index].enableEmission = true;
      componentsInChildren[index].Play();
    }
  }

  public static void StopParticles(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    foreach (ParticleSystem componentsInChild in obj.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
    obj.SetActive(false);
  }

  public static bool IsIAPStoreComingSoon
  {
    get
    {
      return (Application.isEditor || Application.platform == RuntimePlatform.Android ? 1 : (Application.platform == RuntimePlatform.IPhonePlayer ? 1 : 0)) == 0;
    }
  }

  public static void ShowMonthCardPopup()
  {
    if (!Utils.IsIAPStoreEnabled)
      return;
    if (PlayerData.inst.MonthCardValue == PlayerData.MonthCardType.NORMAL)
      UIManager.inst.OpenPopup("MonthCardPopup", (Popup.PopupParameter) null);
    else
      UIManager.inst.OpenPopup("MonthCardPopupB", (Popup.PopupParameter) null);
  }

  public static void ShowArtifactBuyInventory()
  {
    if (!Utils.IsIAPStoreEnabled)
      return;
    UIManager.inst.OpenDlg("Artifact/ArtifactBuyInventoryDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public static bool ShowIAPStore(UI.Dialog.DialogParameter param = null)
  {
    if (Utils.IsIAPStoreComingSoon)
    {
      Utils.PopUpCommingSoon();
      return true;
    }
    if (!Utils.IsIAPStoreEnabled)
      return false;
    UIManager.inst.OpenDlg(UIManager.DialogType.IAPStoreDialog, param, true, true, true);
    return true;
  }

  public static void ShowMonthlySpecialStore(UI.Dialog.DialogParameter param = null)
  {
    if (!Utils.IsIAPStoreEnabled)
      return;
    UIManager.inst.OpenDlg("IAP/IAPStoreMonthlyDisountDlg", param, true, true, true);
  }

  public static IAPStorePackage GetRecommendedIAPStorePackageByPolicty()
  {
    List<IAPStorePackage> sameTypePackages = new List<IAPStorePackage>();
    IAPStorePackage iapStorePackage1 = (IAPStorePackage) null;
    IAPStorePackage iapStorePackage2 = (IAPStorePackage) null;
    bool flag = true;
    if (!TutorialManager.Instance.IsRunning && Utils.IsIAPStoreEnabled)
    {
      List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
      if (IAPStorePackagePayload.Instance.RecommendedPackage != null)
        iapStorePackage1 = IAPStorePackagePayload.Instance.RecommendedPackage;
      if (availablePackages.Count > 0)
      {
        if (iapStorePackage1 != null)
        {
          for (int index = 0; index < availablePackages.Count; ++index)
          {
            if (availablePackages[index].id == iapStorePackage1.id)
            {
              if (availablePackages[index].LeftPurchaseCount != iapStorePackage1.LeftPurchaseCount)
                iapStorePackage1 = availablePackages[index];
              flag = false;
              break;
            }
          }
        }
        if (!flag)
        {
          iapStorePackage2 = iapStorePackage1;
        }
        else
        {
          if (IAPStorePackagePayload.Instance.Policy == 0)
          {
            string topSeven = IAPStorePackagePayload.Instance.TopSeven;
            int type = availablePackages[0].group.type;
            sameTypePackages.Add(availablePackages[0]);
            for (int index = 1; index < availablePackages.Count; ++index)
            {
              if (availablePackages[index].group.type == type)
                sameTypePackages.Add(availablePackages[index]);
            }
            iapStorePackage2 = sameTypePackages.Count <= 1 ? availablePackages[0] : Utils.SelectRecommendedIAPPackageByTopSevenLevel(sameTypePackages, topSeven);
          }
          else
            iapStorePackage2 = availablePackages[0];
          IAPStorePackagePayload.Instance.RecommendedPackage = iapStorePackage2;
        }
      }
    }
    return iapStorePackage2;
  }

  private static IAPStorePackage SelectRecommendedIAPPackageByTopSevenLevel(List<IAPStorePackage> sameTypePackages, string topSeven)
  {
    List<IAPStorePackage> iapStorePackageList = new List<IAPStorePackage>();
    IAPStorePackage iapStorePackage = (IAPStorePackage) null;
    List<string> ids = ConfigManager.inst.DB_IAPPaymentLevel.GetIds();
    int num1 = ids.IndexOf(topSeven);
    if (num1 < 0)
      return (IAPStorePackage) null;
    int num2 = num1 - 1;
    int num3 = num1 + 1;
    if (sameTypePackages.Count > 0)
    {
      for (int index1 = 0; index1 < 1000; ++index1)
      {
        for (int index2 = 0; index2 < sameTypePackages.Count; ++index2)
        {
          if (topSeven != null && sameTypePackages[index2].package.pay_id == topSeven)
            iapStorePackageList.Add(sameTypePackages[index2]);
        }
        if (iapStorePackageList.Count == 1)
        {
          iapStorePackage = iapStorePackageList[0];
          break;
        }
        if (iapStorePackageList.Count > 1)
        {
          int index2 = UnityEngine.Random.Range(0, iapStorePackageList.Count);
          iapStorePackage = iapStorePackageList[index2];
          break;
        }
        if (ids != null && ids.Count > 0)
        {
          if (num3 < ids.Count)
            topSeven = ids[num3++];
          else if (num2 >= 0)
            topSeven = ids[num2--];
        }
      }
    }
    return iapStorePackage;
  }

  public static void ShowRecommendedRandomIAPPackagePopup()
  {
    if (TutorialManager.Instance.IsRunning || !Utils.IsIAPStoreEnabled)
      return;
    List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
    if (availablePackages.Count <= 0)
      return;
    int index = UnityEngine.Random.Range(0, availablePackages.Count);
    UIManager.inst.OpenDlg("IAP/IAPRecommendedPackageDialog", (UI.Dialog.DialogParameter) new IAPRecommendedPackageDialog.Parameter()
    {
      package = availablePackages[index]
    }, true, true, true);
  }

  public static void ShowRecommendIAPPackagePopupByPolicy()
  {
    if (TutorialManager.Instance.IsRunning || !Utils.IsIAPStoreEnabled)
      return;
    if (new IAPRecommendedPackageDialog.Parameter()
    {
      package = Utils.GetRecommendedIAPStorePackageByPolicty()
    }.package == null)
      return;
    bool autoOpen = UIManager.inst.AutoOpen;
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      bool autoOpen1 = UIManager.inst.AutoOpen;
      UIManager.inst.AutoOpen = autoOpen;
      UIManager.inst.OpenDlg("IAP/IAPRecommendedPackageDialog", (UI.Dialog.DialogParameter) parameter, true, true, true);
      UIManager.inst.AutoOpen = autoOpen1;
    }));
  }

  public static void SetPopupLevel(int id)
  {
    List<int> popupIds = IAPDailyRechargePackagePayload.Instance.popupIDs;
    if (popupIds != null && popupIds.Count == 1)
    {
      if (id != popupIds[0])
        return;
      popupIds.RemoveAt(0);
    }
    else
    {
      if (popupIds.Count <= 1)
        return;
      if (id == popupIds[0])
        popupIds.RemoveAt(0);
      if (id != popupIds[popupIds.Count - 1])
        return;
      popupIds.RemoveAt(popupIds.Count - 1);
    }
  }

  public static void RecordPopupId(int id)
  {
    IAPDailyRechargePackagePayload.Instance.popupIDs.Add(id);
  }

  public static void HideNetErrorWindowBlocker()
  {
    if (GameLoadingAnnouncementPayload.Instance.AnnouncementContent == null)
      return;
    NetErrorBlocker objectOfType = UnityEngine.Object.FindObjectOfType(typeof (NetErrorBlocker)) as NetErrorBlocker;
    if (!((UnityEngine.Object) objectOfType != (UnityEngine.Object) null) || !objectOfType.gameObject.activeInHierarchy)
      return;
    Transform child = objectOfType.gameObject.transform.FindChild("zBlocker_Common");
    if (!((UnityEngine.Object) child != (UnityEngine.Object) null))
      return;
    child.gameObject.SetActive(false);
  }

  public static System.Type GetType(string typeName)
  {
    System.Type type1 = System.Type.GetType(typeName);
    if (type1 != null)
      return type1;
    foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
    {
      System.Type type2 = assembly.GetType(typeName);
      if (type2 != null)
        return type2;
    }
    return (System.Type) null;
  }

  public static bool IsZimu(char c)
  {
    if ((int) c <= 122 && (int) c >= 97)
      return true;
    if ((int) c <= 90)
      return (int) c >= 65;
    return false;
  }

  public static bool IsShuzi(char c)
  {
    if ((int) c <= 57)
      return (int) c >= 48;
    return false;
  }

  private static void CheckJsonLib()
  {
    if (!Utils._jsonInited)
    {
      Utils._rSetting = new JsonReaderSettings();
      Utils._rSetting.UseStringInsteadOfNumber = true;
      Utils._wSetting = new JsonWriterSettings();
    }
    Utils._jsonInited = true;
  }

  public static object Json2Object(string json, bool useStringInsteadOfNumber = true)
  {
    if (json == null)
      return (object) null;
    bool flag = true;
    Utils.CheckJsonLib();
    Utils._rSetting.UseStringInsteadOfNumber = useStringInsteadOfNumber;
    object obj = new JsonReader(json, Utils._rSetting).Deserialize();
    if (!flag)
    {
      obj = (object) null;
      Debug.LogError((object) ("json decode error with following json:\n" + json));
    }
    return obj;
  }

  public static string GetDataFromJobByKey(JobHandle job, string key)
  {
    if (job.Data != null)
    {
      Hashtable data = job.Data as Hashtable;
      if (data.Contains((object) key))
        return data[(object) key].ToString();
    }
    return string.Empty;
  }

  public static string Object2Json(object obj)
  {
    Utils.CheckJsonLib();
    StringBuilder output = new StringBuilder();
    using (JsonWriter jsonWriter = new JsonWriter(output, Utils._wSetting))
      jsonWriter.Write(obj);
    return output.ToString();
  }

  public static byte[] Json2ByteArray(string json)
  {
    return Encoding.UTF8.GetBytes(json);
  }

  public static bool SerializeString(string aString, StringBuilder builder)
  {
    foreach (char ch in aString.ToCharArray())
    {
      switch (ch)
      {
        case '\b':
          builder.Append("\\b");
          break;
        case '\t':
          builder.Append("\\t");
          break;
        case '\n':
          builder.Append("\\n");
          break;
        case '\f':
          builder.Append("\\f");
          break;
        case '\r':
          builder.Append("\\r");
          break;
        case '"':
          builder.Append("\\\"");
          break;
        case '\\':
          builder.Append("\\\\");
          break;
        default:
          int int32 = Convert.ToInt32(ch);
          if (int32 >= 32 && int32 <= 126)
          {
            builder.Append(ch);
            break;
          }
          builder.Append("\\u" + Convert.ToString(int32, 16).PadLeft(4, '0'));
          break;
      }
    }
    return true;
  }

  public static string RemoveBBCode(string source)
  {
    return Regex.Replace(source, "\\[[^\\]]+]", string.Empty);
  }

  public static void SetClipboardText(string str)
  {
    UniCopipe.Value = str;
  }

  public static void SetLayer(GameObject go, int layer)
  {
    go.layer = layer;
    Transform transform = go.transform;
    int index = 0;
    for (int childCount = transform.childCount; index < childCount; ++index)
      Utils.SetLayer(transform.GetChild(index).gameObject, layer);
  }

  public static T EnsureComponent<T>(GameObject go) where T : Component
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) go)
      return (T) null;
    T obj = go.GetComponent<T>();
    if ((UnityEngine.Object) null == (UnityEngine.Object) obj)
      obj = go.AddComponent<T>();
    return obj;
  }

  public static GameObject FindChild(GameObject go, string name)
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null || string.IsNullOrEmpty(name))
      return (GameObject) null;
    if (name.Equals(go.name))
      return go;
    Transform transform = go.transform;
    int childCount = transform.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      GameObject child = Utils.FindChild(transform.GetChild(index).gameObject, name);
      if ((UnityEngine.Object) child != (UnityEngine.Object) null)
        return child;
    }
    return (GameObject) null;
  }

  public static string GetLimitStr(string orgStr, int byteLimit)
  {
    if (string.IsNullOrEmpty(orgStr))
      return orgStr;
    byte[] bytes1 = Encoding.UTF8.GetBytes(orgStr);
    if (bytes1 == null || bytes1.Length < byteLimit)
      return orgStr;
    string s = orgStr;
    for (byte[] bytes2 = Encoding.UTF8.GetBytes(s); bytes2 != null && bytes2.Length > byteLimit; bytes2 = Encoding.UTF8.GetBytes(s))
      s = s.Substring(0, s.Length - 1);
    return s;
  }

  public static bool CanSpellToTargetTile(TempleSkillInfo templeSkillInfo, TileData tileData, ref string errorMsg)
  {
    if (tileData.Location.K != PlayerData.inst.userData.world_id)
    {
      errorMsg = ScriptLocalization.Get("toast_spell_other_kingdom_forbidden", true);
      return false;
    }
    string targetType = templeSkillInfo.TargetType;
    if (targetType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAE == null)
      {
        // ISSUE: reference to a compiler-generated field
        Utils.\u003C\u003Ef__switch\u0024mapAE = new Dictionary<string, int>(6)
        {
          {
            "self_alliance",
            0
          },
          {
            "target_alliance",
            1
          },
          {
            "ops_alliance_temple",
            2
          },
          {
            "ops_city",
            3
          },
          {
            "area",
            4
          },
          {
            "self_hive_ops_city",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAE.TryGetValue(targetType, out num))
      {
        switch (num)
        {
          case 0:
            return true;
          case 1:
            if (tileData.TileType == TileType.City && tileData.AllianceID != 0L && tileData.AllianceID != PlayerData.inst.allianceId)
            {
              if (tileData.CityData == null || tileData.CityData.peaceShieldJobId == 0L)
                return true;
              errorMsg = "toast_spell_single_peace_shield";
              break;
            }
            errorMsg = "toast_spell_single_requires_enemy";
            break;
          case 2:
            if (tileData.TileType == TileType.AllianceTemple && tileData.AllianceID != PlayerData.inst.allianceId && DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkillByAllianceId(tileData.AllianceID) != null)
              return true;
            break;
          case 3:
            if (tileData.TileType == TileType.City && tileData.AllianceID != PlayerData.inst.allianceId)
            {
              if (tileData.CityData == null || tileData.CityData.peaceShieldJobId == 0L)
                return true;
              errorMsg = "toast_spell_single_peace_shield";
              break;
            }
            errorMsg = "toast_spell_single_requires_enemy";
            break;
          case 4:
            if (tileData.TileType == TileType.AllianceFortress || tileData.TileType == TileType.AllianceTemple || (tileData.TileType == TileType.Resource1 || tileData.TileType == TileType.Resource2) || (tileData.TileType == TileType.Resource3 || tileData.TileType == TileType.Resource4 || tileData.TileType == TileType.Digsite))
              return true;
            if (tileData.TileType == TileType.City && tileData.AllianceID != PlayerData.inst.allianceId)
            {
              if (tileData.CityData == null || tileData.CityData.peaceShieldJobId == 0L)
                return true;
              errorMsg = "toast_spell_single_peace_shield";
              break;
            }
            if (tileData.TileType == TileType.Wonder)
            {
              if (tileData.WonderData != null && tileData.WonderData.State == "fighting")
                return true;
              break;
            }
            if (tileData.TileType == TileType.WonderTower)
            {
              if (tileData.WonderTowerData != null && tileData.WonderTowerData.State == "fighting")
                return true;
              break;
            }
            if (tileData.TileType == TileType.Camp && PlayerData.inst.allianceData.members.Get(tileData.OwnerID) == null)
              return true;
            break;
          case 5:
            if (tileData.TileType == TileType.City && tileData.AllianceID != PlayerData.inst.allianceId)
            {
              if (tileData.CityData != null && tileData.CityData.peaceShieldJobId != 0L)
              {
                errorMsg = "toast_spell_single_peace_shield";
                break;
              }
              List<ZoneBorderData> bordersByAllianceId = DBManager.inst.DB_ZoneBorder.GetZoneBordersByAllianceID(PlayerData.inst.allianceId);
              for (int index = 0; index < bordersByAllianceId.Count; ++index)
              {
                if (bordersByAllianceId[index].Contains(tileData.Location))
                  return true;
              }
              break;
            }
            break;
        }
      }
    }
    if (string.IsNullOrEmpty(errorMsg))
      errorMsg = "toast_spell_single_target_forbidden";
    return false;
  }

  public static bool CanSpellToTargetTile(KingSkillInfo kingSkillInfo, TileData tileData, ref string errorMsg)
  {
    if (tileData.Location.K != PlayerData.inst.userData.world_id)
    {
      errorMsg = ScriptLocalization.Get("toast_spell_other_kingdom_forbidden", true);
      return false;
    }
    string skillType = kingSkillInfo.SkillType;
    if (skillType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAF == null)
      {
        // ISSUE: reference to a compiler-generated field
        Utils.\u003C\u003Ef__switch\u0024mapAF = new Dictionary<string, int>(2)
        {
          {
            "force_teleport",
            0
          },
          {
            "send_troop_forbidden",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (Utils.\u003C\u003Ef__switch\u0024mapAF.TryGetValue(skillType, out num) && num == 0)
      {
        if (tileData.TileType == TileType.City)
        {
          if (tileData.CityData == null || tileData.CityData.peaceShieldJobId == 0L)
            return true;
          errorMsg = "toast_spell_single_peace_shield";
        }
        else
          errorMsg = "toast_spell_single_requires_enemy";
      }
    }
    return false;
  }

  public static void ShowKingdomTitleDetail(int kingdomId, long nominateUserId = 0)
  {
    DB.WonderData wd = DBManager.inst.DB_Wonder.Get((long) kingdomId);
    if (wd == null || !(wd.State == "protected"))
      return;
    MessageHub.inst.GetPortByAction("wonder:loadTitleInfo").SendRequest(new Hashtable()
    {
      {
        (object) "target_world_id",
        (object) kingdomId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      if (data != null)
        ConfigManager.inst.DB_WonderTitle.UpdateWonderTitleData(data as Hashtable, true);
      UIManager.inst.OpenDlg("Wonder/KingdomTitleDlg", (UI.Dialog.DialogParameter) new KingdomTitleDlg.Parameter()
      {
        kingdomId = kingdomId,
        needNominateUserId = (wd == null || wd.KING_UID != PlayerData.inst.uid || wd.KING_UID == nominateUserId ? 0L : nominateUserId)
      }, 1 != 0, 1 != 0, 1 != 0);
    }), true);
  }

  public static int GetEquipmentCountWithEquipmentSuit(BagType bagType, int equipmentSuitId)
  {
    int num = 0;
    List<int> allEquipments = (bagType != BagType.DragonKnight ? PlayerData.inst.heroData.equipments : PlayerData.inst.dragonKnightData.equipments).GetAllEquipments();
    List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
    using (List<int>.Enumerator enumerator = allEquipments.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        InventoryItemData myItemByItemId = DBManager.inst.GetInventory(bagType).GetMyItemByItemID((long) current);
        inventoryItemDataList.Add(myItemByItemId);
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(myItemByItemId.internalId);
        if (data != null && data.suitGroup == equipmentSuitId)
          ++num;
      }
    }
    return num;
  }

  public static List<Benefits.BenefitValuePair> GetAllBenefitInEquipmentSuit(int equipmentSuitGroupId, int equipmentCount)
  {
    List<Benefits.BenefitValuePair> benefitValuePairList = new List<Benefits.BenefitValuePair>();
    ConfigEquipmentSuitMainInfo equipmentSuitMainInfo = ConfigManager.inst.DB_EquipmentSuitMain.GetEquipmentSuitEffectMap(equipmentSuitGroupId).Find((Predicate<ConfigEquipmentSuitMainInfo>) (p => p.numberRequired == equipmentCount));
    if (equipmentSuitMainInfo != null)
    {
      using (List<Benefits.BenefitValuePair>.Enumerator enumerator = equipmentSuitMainInfo.benefits.GetBenefitsPairs().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Benefits.BenefitValuePair current = enumerator.Current;
          benefitValuePairList.Add(current);
        }
      }
      benefitValuePairList.Sort((Comparison<Benefits.BenefitValuePair>) ((x, y) => x.propertyDefinition.Priority.CompareTo(y.propertyDefinition.Priority)));
    }
    return benefitValuePairList;
  }

  public static void GetEquipmentBenefitChangeInfoAfterReequipAll(BagType bagType, List<InventoryItemData> allBefore, List<InventoryItemData> allNow, out List<Benefits.BenefitValuePair> allDelProperty, out List<Benefits.BenefitValuePair> allAddProperty, out ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance, out ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance)
  {
    allDelProperty = Utils.GetAllEquipmentSuitBenefit(bagType, allBefore);
    allAddProperty = Utils.GetAllEquipmentSuitBenefit(bagType, allNow);
    addEquipmentSuitEnhance = delEquipmentSuitEnhance = (ConfigEquipmentSuitEnhanceInfo) null;
    int num1 = int.MaxValue;
    if (allBefore.Count < 6)
    {
      num1 = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allBefore.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num1 = Mathf.Min(enumerator.Current.enhanced, num1);
      }
    }
    int num2 = int.MaxValue;
    if (allNow.Count < 6)
    {
      num2 = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allNow.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num2 = Mathf.Min(enumerator.Current.enhanced, num2);
      }
    }
    ConfigEquipmentSuitEnhanceInfo lessEqual1 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num2, bagType);
    ConfigEquipmentSuitEnhanceInfo lessEqual2 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num1, bagType);
    delEquipmentSuitEnhance = lessEqual2;
    addEquipmentSuitEnhance = lessEqual1;
  }

  public static List<InventoryItemData> GetAllEquipedItemData(BagType bagType)
  {
    List<InventoryItemData> inventoryItemDataList = new List<InventoryItemData>();
    if (bagType == BagType.Hero)
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
      if (heroData != null)
      {
        using (List<int>.Enumerator enumerator = heroData.equipments.GetAllEquipments().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            InventoryItemData myItemByItemId = DBManager.inst.GetInventory(BagType.Hero).GetMyItemByItemID((long) enumerator.Current);
            if (myItemByItemId != null)
              inventoryItemDataList.Add(myItemByItemId);
          }
        }
      }
    }
    else
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      if (dragonKnightData != null)
      {
        using (List<int>.Enumerator enumerator = dragonKnightData.equipments.GetAllEquipments().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            InventoryItemData myItemByItemId = DBManager.inst.GetInventory(BagType.DragonKnight).GetMyItemByItemID((long) enumerator.Current);
            if (myItemByItemId != null)
              inventoryItemDataList.Add(myItemByItemId);
          }
        }
      }
    }
    return inventoryItemDataList;
  }

  public static void GetEquipmentBenefitChangeInfoAfterEnhancement(BagType bagType, InventoryItemData enhancedItem, out ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance, out ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance)
  {
    addEquipmentSuitEnhance = delEquipmentSuitEnhance = (ConfigEquipmentSuitEnhanceInfo) null;
    List<InventoryItemData> allEquipedItemData = Utils.GetAllEquipedItemData(bagType);
    int num = int.MaxValue;
    if (allEquipedItemData.Count < 6)
    {
      num = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allEquipedItemData.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num = Mathf.Min(enumerator.Current.enhanced, num);
      }
    }
    int level = enhancedItem.enhanced - 1;
    if (level >= num)
      level = num;
    if (num == level)
      return;
    ConfigEquipmentSuitEnhanceInfo lessEqual1 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num, bagType);
    ConfigEquipmentSuitEnhanceInfo lessEqual2 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(level, bagType);
    if (lessEqual1 == lessEqual2)
      return;
    delEquipmentSuitEnhance = lessEqual2;
    addEquipmentSuitEnhance = lessEqual1;
  }

  public static List<Benefits.BenefitValuePair> GetAllEquipmentSuitBenefit(BagType bagType, List<InventoryItemData> allEquiped)
  {
    List<Benefits.BenefitValuePair> benefitValuePairList = new List<Benefits.BenefitValuePair>();
    List<ConfigEquipmentMainInfo> equipmentMainInfoList = new List<ConfigEquipmentMainInfo>();
    using (List<InventoryItemData>.Enumerator enumerator = allEquiped.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InventoryItemData current = enumerator.Current;
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(current.internalId);
        if (data != null)
          equipmentMainInfoList.Add(data);
      }
    }
    Dictionary<int, int> dictionary1 = new Dictionary<int, int>();
    using (List<ConfigEquipmentMainInfo>.Enumerator enumerator = equipmentMainInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigEquipmentMainInfo current = enumerator.Current;
        if (current.suitGroup != 0)
        {
          if (!dictionary1.ContainsKey(current.suitGroup))
            dictionary1.Add(current.suitGroup, 0);
          Dictionary<int, int> dictionary2;
          int suitGroup;
          (dictionary2 = dictionary1)[suitGroup = current.suitGroup] = dictionary2[suitGroup] + 1;
        }
      }
    }
    using (Dictionary<int, int>.KeyCollection.Enumerator enumerator1 = dictionary1.Keys.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        int current1 = enumerator1.Current;
        if (ConfigManager.inst.DB_EquipmentSuitGroup.GetData(current1) == null)
        {
          D.error((object) string.Format("cannot find equipment suit group info: {0}", (object) current1));
        }
        else
        {
          List<ConfigEquipmentSuitMainInfo> equipmentSuitEffectMap = ConfigManager.inst.DB_EquipmentSuitMain.GetEquipmentSuitEffectMap(current1);
          if (equipmentSuitEffectMap != null)
          {
            int num = dictionary1[current1];
            using (List<ConfigEquipmentSuitMainInfo>.Enumerator enumerator2 = equipmentSuitEffectMap.GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                ConfigEquipmentSuitMainInfo current2 = enumerator2.Current;
                if (current2.numberRequired <= num)
                {
                  List<Benefits.BenefitValuePair> benefitsPairs = current2.benefits.GetBenefitsPairs();
                  if (benefitsPairs != null)
                    benefitValuePairList.AddRange((IEnumerable<Benefits.BenefitValuePair>) benefitsPairs);
                }
              }
            }
          }
        }
      }
    }
    return benefitValuePairList;
  }

  public static void GetEquipmentBenefitChangedInfo(BagType bagType, InventoryItemData added, InventoryItemData removed, out List<Benefits.BenefitValuePair> allDelProperty, out List<Benefits.BenefitValuePair> allAddProperty, out ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance, out ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance)
  {
    int internalID1 = added != null ? added.internalId : 0;
    int internalID2 = removed != null ? removed.internalId : 0;
    allDelProperty = new List<Benefits.BenefitValuePair>();
    allAddProperty = new List<Benefits.BenefitValuePair>();
    delEquipmentSuitEnhance = addEquipmentSuitEnhance = (ConfigEquipmentSuitEnhanceInfo) null;
    ConfigEquipmentMainInfo equipmentMainInfo1 = internalID1 != 0 ? ConfigManager.inst.GetEquipmentMain(bagType).GetData(internalID1) : (ConfigEquipmentMainInfo) null;
    ConfigEquipmentMainInfo equipmentMainInfo2 = internalID2 != 0 ? ConfigManager.inst.GetEquipmentMain(bagType).GetData(internalID2) : (ConfigEquipmentMainInfo) null;
    if (equipmentMainInfo1 == null || equipmentMainInfo2 == null || equipmentMainInfo1.suitGroup != equipmentMainInfo2.suitGroup)
    {
      if (equipmentMainInfo1 != null && equipmentMainInfo1.suitGroup != 0)
      {
        int withEquipmentSuit = Utils.GetEquipmentCountWithEquipmentSuit(bagType, equipmentMainInfo1.suitGroup);
        allAddProperty = Utils.GetAllBenefitInEquipmentSuit(equipmentMainInfo1.suitGroup, withEquipmentSuit + 1);
      }
      if (equipmentMainInfo2 != null && equipmentMainInfo2.suitGroup != 0)
      {
        int withEquipmentSuit = Utils.GetEquipmentCountWithEquipmentSuit(bagType, equipmentMainInfo2.suitGroup);
        allDelProperty = Utils.GetAllBenefitInEquipmentSuit(equipmentMainInfo2.suitGroup, withEquipmentSuit);
      }
    }
    List<InventoryItemData> allEquipedItemData = Utils.GetAllEquipedItemData(bagType);
    int num1 = int.MaxValue;
    if (allEquipedItemData.Count < 6)
    {
      num1 = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allEquipedItemData.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num1 = Mathf.Min(enumerator.Current.enhanced, num1);
      }
    }
    int num2 = int.MaxValue;
    if (removed != null)
      allEquipedItemData.RemoveAll((Predicate<InventoryItemData>) (p => p.itemId == removed.itemId));
    if (added != null)
      allEquipedItemData.Add(added);
    if (allEquipedItemData.Count < 6)
    {
      num2 = 0;
    }
    else
    {
      using (List<InventoryItemData>.Enumerator enumerator = allEquipedItemData.GetEnumerator())
      {
        while (enumerator.MoveNext())
          num2 = Mathf.Min(enumerator.Current.enhanced, num2);
      }
    }
    ConfigEquipmentSuitEnhanceInfo lessEqual1 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num1, bagType);
    ConfigEquipmentSuitEnhanceInfo lessEqual2 = ConfigManager.inst.DB_EquipmentSuitEnhance.GetLessEqual(num2, bagType);
    if (lessEqual1 == lessEqual2)
      return;
    delEquipmentSuitEnhance = lessEqual1;
    addEquipmentSuitEnhance = lessEqual2;
  }

  public static bool IsPlayerReinforcedBySelf(long targetUid)
  {
    List<MarchData> reinforceByUid = DBManager.inst.DB_March.GetReinforceByUid(targetUid);
    if (reinforceByUid != null)
    {
      MarchData marchData = reinforceByUid.Find((Predicate<MarchData>) (x => x.ownerUid == PlayerData.inst.uid));
      if (marchData != null && marchData.type == MarchData.MarchType.reinforce && marchData.state == MarchData.MarchState.reinforcing)
        return true;
    }
    return false;
  }

  public static bool CanOpenAllianceChest()
  {
    if (PlayerData.inst.allianceId != 0L)
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null && allianceData.members != null)
      {
        AllianceMemberData allianceMemberData = allianceData.members.Get(PlayerData.inst.uid);
        if (allianceMemberData != null)
          return !NetServerTime.inst.IsToday((double) allianceMemberData.joinTime);
      }
    }
    return false;
  }

  public static void SetPortrait(UITexture texture, string portraitPath)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) texture, portraitPath, (System.Action<bool>) null, true, true, "Texture/Hero/player_portrait_missing");
  }

  public static long GetSpecificTime(string dayOfWeek, string time)
  {
    DateTime dateTime = Utils.ServerTime2DateTime((long) NetServerTime.inst.ServerTimestamp);
    int result = 0;
    int.TryParse(dayOfWeek, out result);
    int dayOfWeek1 = Utils.GetDayOfWeek(dateTime.DayOfWeek);
    return Utils.DateTime2ServerTime(string.Format("{0}-{1}-{2} {3}", (object) dateTime.Year.ToString("D4"), (object) dateTime.Month.ToString("D2"), (object) dateTime.Day.ToString("D2"), (object) time)) + (long) ((result - dayOfWeek1) * 24 * 3600);
  }

  public static int GetDayOfWeek(DayOfWeek dayOfWeek)
  {
    switch (dayOfWeek)
    {
      case DayOfWeek.Sunday:
        return 0;
      case DayOfWeek.Monday:
        return 1;
      case DayOfWeek.Tuesday:
        return 2;
      case DayOfWeek.Wednesday:
        return 3;
      case DayOfWeek.Thursday:
        return 4;
      case DayOfWeek.Friday:
        return 5;
      case DayOfWeek.Saturday:
        return 6;
      default:
        return 0;
    }
  }
}
