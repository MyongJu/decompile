﻿// Decompiled with JetBrains decompiler
// Type: ChatCreateRoomPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Text;
using UI;

public class ChatCreateRoomPopup : Popup
{
  public const string POPUP_TYPE = "Chat/ChatCreateRoomPopup";
  public UIInput input;
  public CreateRoomStepDlg1 stemp1;
  public UILabel titleLabelCount;
  private string newRoomName;
  private bool newRoomIsPrivate;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.stemp1.Show();
    this.titleLabelCount.text = string.Format("{0}/99", (object) this.input.value.Length);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void Show(string roomName, bool isPrivate)
  {
    this.gameObject.SetActive(true);
    this.newRoomName = roomName;
    this.newRoomIsPrivate = isPrivate;
    this.input.value = string.Empty;
  }

  public void OnTextChanged()
  {
    this.input.value = Utils.GetLimitStr(this.input.value, 99);
    this.titleLabelCount.text = string.Format("{0}/99", (object) Encoding.UTF8.GetBytes(this.input.value).Length);
  }

  public void OnDoneButtonClick()
  {
    HubPort portByAction = MessageHub.inst.GetPortByAction("Chat:createRoom");
    object[] objArray = new object[8]
    {
      (object) "name",
      (object) IllegalWordsUtils.Filter(this.stemp1.panel.input.value),
      (object) "is_open",
      !this.stemp1.isPrivate ? (object) "1" : (object) "2",
      (object) "is_search",
      (object) "1",
      (object) "title",
      null
    };
    int index = 7;
    string str;
    if (string.IsNullOrEmpty(this.input.value))
      str = ScriptLocalization.GetWithMultyContents("chat_room_welcome_description", true, IllegalWordsUtils.Filter(this.stemp1.panel.input.value));
    else
      str = IllegalWordsUtils.Filter(this.input.value);
    objArray[index] = (object) str;
    Hashtable postData = Utils.Hash(objArray);
    System.Action<bool, object> callback = (System.Action<bool, object>) ((result, orgSrc) =>
    {
      if (!result)
        return;
      MessageHub.inst.GetPortByAction("Chat:getInitChatRooms").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      }), true);
    });
    int num = 1;
    portByAction.SendRequest(postData, callback, num != 0);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long roomId;
  }
}
