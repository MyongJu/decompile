﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryHUD : MonoBehaviour
{
  public bool setStatus = true;
  private string _vfxName = string.Empty;
  private const string VFX_PATH = "Prefab/Anniversary/";
  public GameObject rootNode;
  public UILabel remainTime;
  public string anniversaryTheme;
  private GameObject _vfxObj;

  private void Start()
  {
    this.OnTournamentPointChanged(0);
  }

  private void OnEnable()
  {
    this.OnTournamentPointChanged(0);
    if (!this.setStatus)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    AnniversaryManager.Instance.OnPointChange -= new System.Action<int>(this.OnTournamentPointChanged);
    AnniversaryManager.Instance.OnPointChange += new System.Action<int>(this.OnTournamentPointChanged);
  }

  private void OnSecondEvent(int time)
  {
    if (!(bool) ((UnityEngine.Object) this.gameObject))
      return;
    this.UpdateUI();
  }

  private void OnTournamentPointChanged(int count)
  {
    if (!(this.anniversaryTheme == "anniversary_tournament") || !((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
      return;
    Transform child = this.transform.FindChild("AnnvTournamentBtn/BadgeBkg/Counter");
    if (!(bool) ((UnityEngine.Object) child))
      return;
    UILabel component = child.GetComponent<UILabel>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    component.text = count.ToString();
    NGUITools.SetActive(component.transform.parent.gameObject, count > 0);
  }

  private void UpdateUI()
  {
    if (!this.setStatus)
      return;
    if ((UnityEngine.Object) this.remainTime != (UnityEngine.Object) null)
      this.remainTime.text = Utils.FormatTime((int) (AnniversaryIapPayload.Instance.EndTime - (long) NetServerTime.inst.ServerTimestamp), true, false, true);
    this.CheckShowTime();
    if (this.ShouldShowHUD())
    {
      NGUITools.SetActive(this.rootNode, true);
      this.SetVFX(this.GetThemeName());
    }
    else
    {
      if (this.rootNode.activeInHierarchy)
        NGUITools.SetActive(this.rootNode, false);
      this.DeleteVFX();
    }
  }

  private void CheckShowTime()
  {
    if (!(this.anniversaryTheme == "anniversary_tournament") || !((UnityEngine.Object) this.remainTime != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this.remainTime.transform.parent.gameObject, (long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.TournamentStartTime && (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime);
  }

  private bool ShouldShowHUD()
  {
    bool flag = false;
    string anniversaryTheme = this.anniversaryTheme;
    if (anniversaryTheme != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AnniversaryHUD.\u003C\u003Ef__switch\u0024map24 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AnniversaryHUD.\u003C\u003Ef__switch\u0024map24 = new Dictionary<string, int>(2)
        {
          {
            "anniversary_iap",
            0
          },
          {
            "anniversary_tournament",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AnniversaryHUD.\u003C\u003Ef__switch\u0024map24.TryGetValue(anniversaryTheme, out num))
      {
        switch (num)
        {
          case 0:
            flag = (long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.StartTime & (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime;
            break;
          case 1:
            flag = (long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.TournamentShowTime & (long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime;
            break;
        }
      }
    }
    return flag & AnniversaryIapPayload.Instance.IsOpen;
  }

  private string GetThemeName()
  {
    string str = string.Empty;
    string anniversaryTheme = this.anniversaryTheme;
    if (anniversaryTheme != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AnniversaryHUD.\u003C\u003Ef__switch\u0024map25 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AnniversaryHUD.\u003C\u003Ef__switch\u0024map25 = new Dictionary<string, int>(2)
        {
          {
            "anniversary_iap",
            0
          },
          {
            "anniversary_tournament",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AnniversaryHUD.\u003C\u003Ef__switch\u0024map25.TryGetValue(anniversaryTheme, out num))
      {
        switch (num)
        {
          case 0:
            str = "icon_first_countdown";
            break;
          case 1:
            str = "icon_first_year";
            break;
        }
      }
    }
    return str;
  }

  private void SetVFX(string vfxUsedName)
  {
    if (!(this._vfxName != vfxUsedName))
      return;
    this.DeleteVFX();
    this._vfxName = vfxUsedName;
    this._vfxObj = AssetManager.Instance.HandyLoad("Prefab/Anniversary/" + this._vfxName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    this._vfxObj = UnityEngine.Object.Instantiate<GameObject>(this._vfxObj);
    this._vfxObj.transform.parent = this.rootNode.transform;
    this._vfxObj.transform.localPosition = Vector3.zero;
    this._vfxObj.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this._vfxName = string.Empty;
    if (!((UnityEngine.Object) this._vfxObj != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._vfxObj);
    this._vfxObj = (GameObject) null;
  }

  public void OnEntranceClicked()
  {
    if (!AnniversaryIapPayload.Instance.IsOpen)
      return;
    string anniversaryTheme = this.anniversaryTheme;
    if (anniversaryTheme == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (AnniversaryHUD.\u003C\u003Ef__switch\u0024map26 == null)
    {
      // ISSUE: reference to a compiler-generated field
      AnniversaryHUD.\u003C\u003Ef__switch\u0024map26 = new Dictionary<string, int>(2)
      {
        {
          "anniversary_iap",
          0
        },
        {
          "anniversary_tournament",
          1
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!AnniversaryHUD.\u003C\u003Ef__switch\u0024map26.TryGetValue(anniversaryTheme, out num))
      return;
    switch (num)
    {
      case 0:
        OperationTrace.TraceClick(ClickArea.IAPAnniversary);
        if (this.setStatus)
        {
          AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
          {
            if (!ret)
              return;
            UIManager.inst.OpenDlg("Anniversary/AnniversaryIapMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
          }));
          break;
        }
        List<SuperLoginMainInfo> mainInfoList = ConfigManager.inst.DB_SuperLoginMain.GetSuperLoginMainInfoListByGroup("super_log_in_1_year_celebration");
        if (mainInfoList != null)
          mainInfoList.Sort((Comparison<SuperLoginMainInfo>) ((a, b) => a.day.CompareTo(b.day)));
        if (mainInfoList != null && SuperLoginPayload.Instance.CurrentDay > 0 && SuperLoginPayload.Instance.CurrentDay <= mainInfoList.Count)
        {
          SuperLoginPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
          {
            if (!ret)
              return;
            if (SuperLoginPayload.Instance.CanActivityShow("super_log_in_1_year_celebration") && mainInfoList[SuperLoginPayload.Instance.CurrentDay - 1].RewardState != SuperLoginPayload.DailyRewardState.HAS_GOT)
              UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryPopup", (Popup.PopupParameter) null);
            else
              this.CheckGoldenMan(true);
          }), true);
          break;
        }
        this.CheckGoldenMan(false);
        break;
      case 1:
        OperationTrace.TraceClick(ClickArea.AnniversaryTournament);
        if (this.setStatus)
        {
          UIManager.inst.OpenDlg("Anniversary/AnniversaryMainDlg", (UI.Dialog.DialogParameter) new AnniversaryMainDlg.Parameter()
          {
            catalogType = AnniversaryCatalog.CatalogType.TOURNAMENT
          }, 1 != 0, 1 != 0, 1 != 0);
          break;
        }
        if (AnniversaryManager.Instance.EndTime <= NetServerTime.inst.ServerTimestamp)
        {
          UIManager.inst.OpenDlg("Anniversary/AnniversaryOverDlg", (UI.Dialog.DialogParameter) null, true, true, true);
          break;
        }
        if ((long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.TournamentShowTime || (long) NetServerTime.inst.ServerTimestamp >= AnniversaryIapPayload.Instance.EndTime)
          break;
        UIManager.inst.OpenDlg("Anniversary/AnniversaryMainDlg", (UI.Dialog.DialogParameter) new AnniversaryMainDlg.Parameter()
        {
          catalogType = AnniversaryCatalog.CatalogType.TOURNAMENT
        }, 1 != 0, 1 != 0, 1 != 0);
        break;
    }
  }

  private void CheckGoldenMan(bool firstAnniversaryStart = false)
  {
    if (AnniversaryIapPayload.Instance.ShouldShowAnnvIap)
      UIManager.inst.OpenDlg("Anniversary/AnniversaryMainDlg", (UI.Dialog.DialogParameter) new AnniversaryMainDlg.Parameter()
      {
        catalogType = AnniversaryCatalog.CatalogType.IAP_PACKAGE
      }, 1 != 0, 1 != 0, 1 != 0);
    else if ((long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.StartTime && firstAnniversaryStart)
    {
      SuperLoginPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret || !SuperLoginPayload.Instance.CanActivityShow("super_log_in_1_year_celebration"))
          return;
        UIManager.inst.OpenPopup("SuperLogin/FirstAnniversaryPopup", (Popup.PopupParameter) null);
      }), true);
    }
    else
    {
      if ((long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime)
        return;
      UIManager.inst.toast.Show(Utils.XLAT("toast_1_year_tournament_close"), (System.Action) null, 4f, true);
    }
  }
}
