﻿// Decompiled with JetBrains decompiler
// Type: ConfigSevenDayRewards
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigSevenDayRewards
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, SevenDayRewardInfo> _datas;
  private Dictionary<int, SevenDayRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<SevenDayRewardInfo, string>(res as Hashtable, "ID", out this._datas, out this.dicByUniqueId);
  }

  public SevenDayRewardInfo GetRewardInfo(int day)
  {
    Dictionary<string, SevenDayRewardInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.Day == day)
        return enumerator.Current;
    }
    return (SevenDayRewardInfo) null;
  }

  public void Clear()
  {
    if (this._datas != null)
    {
      this._datas.Clear();
      this._datas = (Dictionary<string, SevenDayRewardInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, SevenDayRewardInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this._datas.ContainsKey(id);
  }

  public SevenDayRewardInfo GetData(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (SevenDayRewardInfo) null;
  }

  public SevenDayRewardInfo GetData(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (SevenDayRewardInfo) null;
  }
}
