﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitTenLuckyDrawDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroRecruitTenLuckyDrawDialog : UI.Dialog
{
  public HeroCardRotator[] m_HeroCardRotators;
  public HeroCardResult[] m_HeroCardResults;
  public UIButton m_Back;
  public UIButton m_Close;
  public UIButton m_Button;
  public UILabel m_Price;
  public UILabel m_CallAgain;
  public Color m_CurrencyColor;
  public Animator m_FlyOut;
  public UnityEngine.Animation m_Shake;
  public ParticleSystem m_ParticleSystem;
  public AnimationEventReceiver m_AnimationEventReceiver;
  public UISprite m_ItemIcon;
  public GameObject m_Trigger;
  public GameObject m_Content;
  public GameObject m_Direct;
  private HeroRecruitTenLuckyDrawDialog.Parameter m_Parameter;
  private int m_CurrentIndex;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_FlyOut.enabled = false;
    if (this.m_Parameter == null)
    {
      this.m_Parameter = orgParam as HeroRecruitTenLuckyDrawDialog.Parameter;
      this.PlayAnimation();
    }
    this.UpdatePrice();
  }

  public void OnRecruit()
  {
    if (this.Price > this.m_Parameter.heroRecruitData.GetCurrency())
      this.m_Parameter.heroRecruitData.ShowStore((int) this.Price);
    else
      HeroRecruitUtils.Summon(this.SummonType, false, (System.Action<bool, HeroRecruitPayload>) ((ret, payload) =>
      {
        if (!ret)
          return;
        this.m_Parameter.heroPayloads = payload.GetPayloadData();
        this.PlayAnimation();
      }));
  }

  private int SummonType
  {
    get
    {
      if (this.m_Parameter.customSummonType == 0)
        return this.m_Parameter.heroRecruitData.GetSummonType();
      return this.m_Parameter.customSummonType;
    }
  }

  public void OnDirectResult()
  {
    this.m_Content.SetActive(false);
    this.m_Direct.SetActive(true);
    for (int index = 0; index < this.m_Parameter.heroPayloads.Count; ++index)
    {
      this.m_HeroCardResults[index].SetData(this.m_Parameter.heroPayloads[index]);
      ParliamentHeroInfo heroInfo = this.m_HeroCardResults[index].GetHeroInfo();
      if (heroInfo != null && heroInfo.quality == 3)
      {
        this.m_Shake.enabled = true;
        this.m_Shake.Play(AnimationPlayMode.Stop);
      }
    }
    this.EnableButtons();
  }

  private void PlayAnimation()
  {
    this.m_Content.SetActive(true);
    this.m_Direct.SetActive(false);
    for (int index = 0; index < this.m_HeroCardRotators.Length; ++index)
      this.m_HeroCardRotators[index].Reset();
    this.DisableButtons();
    this.m_Button.gameObject.SetActive(this.m_Parameter.showButton);
    this.m_CallAgain.gameObject.SetActive(this.m_Parameter.showButton);
    this.m_ItemIcon.spriteName = this.m_Parameter.heroRecruitData.GetIcon();
    this.UpdatePrice();
    this.Play();
  }

  private void UpdatePrice()
  {
    this.m_Price.text = Utils.FormatThousands(this.Price.ToString());
    this.m_Price.color = this.Price > this.m_Parameter.heroRecruitData.GetCurrency() ? Color.red : this.m_CurrencyColor;
  }

  private long Price
  {
    get
    {
      if (this.m_Parameter.customSummonType == 0)
        return this.m_Parameter.heroRecruitData.GetPrice();
      return (long) this.m_Parameter.customPrice;
    }
  }

  private void EnableButtons()
  {
    this.m_Back.isEnabled = true;
    this.m_Close.isEnabled = true;
    this.m_Button.isEnabled = true;
    this.m_CallAgain.gameObject.SetActive(this.m_Parameter.showButton);
    this.m_Trigger.SetActive(false);
    this.m_CallAgain.text = HeroRecruitUtils.GetRecruitText(this.m_Parameter.heroRecruitData.GetSummonType());
  }

  private void DisableButtons()
  {
    this.m_Back.isEnabled = false;
    this.m_Close.isEnabled = false;
    this.m_Button.isEnabled = false;
    this.m_CallAgain.gameObject.SetActive(false);
    this.m_Trigger.SetActive(true);
  }

  private void Play()
  {
    this.m_AnimationEventReceiver.onAnimationEnd = new System.Action(this.OnAnimationDone);
    this.m_ParticleSystem.Stop();
    this.m_ParticleSystem.Clear();
    this.m_ParticleSystem.Play(true);
    AudioManager.Instance.StopAndPlaySound("sfx_city_hero_summon");
    this.m_FlyOut.enabled = true;
    this.m_FlyOut.Rebind();
  }

  private void OnAnimationDone()
  {
    this.m_CurrentIndex = 0;
    this.m_HeroCardRotators[this.m_CurrentIndex].Play(this.m_Parameter.heroPayloads[this.m_CurrentIndex], new System.Action(this.OnFlipCardFinished));
  }

  private void OnFlipCardFinished()
  {
    ParliamentHeroInfo heroInfo = this.m_HeroCardRotators[this.m_CurrentIndex].GetHeroInfo();
    if (heroInfo != null && heroInfo.quality == 3)
    {
      this.m_Shake.enabled = true;
      this.m_Shake.Play(AnimationPlayMode.Stop);
    }
    ++this.m_CurrentIndex;
    if (this.m_CurrentIndex < this.m_Parameter.heroPayloads.Count)
      this.m_HeroCardRotators[this.m_CurrentIndex].Play(this.m_Parameter.heroPayloads[this.m_CurrentIndex], new System.Action(this.OnFlipCardFinished));
    else
      this.EnableButtons();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool showButton;
    public HeroRecruitData heroRecruitData;
    public List<HeroRecruitPayloadData> heroPayloads;
    public int customSummonType;
    public int customPrice;
  }
}
