﻿// Decompiled with JetBrains decompiler
// Type: TroopsPagePlayerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopsPagePlayerInfo : MonoBehaviour
{
  private List<HorizontalTroopList> _troopTableEntries = new List<HorizontalTroopList>();
  private List<UILabel> _troopLabelEntries = new List<UILabel>();
  private char _currentChar = 'a';
  public UIProgressBar troopProgress;
  public UITable troopTable;
  public UILabel labelAlliance;
  public UILabel labelName;
  public UILabel cityName;
  public UISprite spritePlayerPortrait;
  public UILabel currentPower;
  public UILabel labelK;
  public UILabel labelX;
  public UILabel labelY;
  public GameObject allianceDataContainer;
  public UILabel troopTypeLabelTemplate;
  public HorizontalTroopList troopInfoTableTemplate;
  private string _troopTableEntryName;
  private string _troopLabelEntryName;

  public void Start()
  {
    this._troopTableEntryName = this.troopInfoTableTemplate.gameObject.name;
    this._troopLabelEntryName = this.troopTypeLabelTemplate.gameObject.name;
  }

  public void SetTroopData(Dictionary<TroopInfo, KeyValuePair<int, int>> unitMap)
  {
    this.Clear();
    int num1 = 0;
    int num2 = 0;
    Dictionary<string, List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>>> dictionary = new Dictionary<string, List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>>>();
    Dictionary<TroopInfo, KeyValuePair<int, int>>.Enumerator enumerator1 = unitMap.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      TroopInfo key = enumerator1.Current.Key;
      if (!dictionary.ContainsKey(key.m_Name))
        dictionary.Add(key.m_Name, new List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>>());
      dictionary[key.m_Name].Add(enumerator1.Current);
    }
    Dictionary<string, List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>>>.KeyCollection.Enumerator enumerator2 = dictionary.Keys.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      GameObject gameObject1 = PrefabManagerEx.Instance.Spawn(this.troopTypeLabelTemplate.gameObject, this.troopTable.gameObject.transform);
      UILabel component1 = gameObject1.GetComponent<UILabel>();
      component1.text = ScriptLocalization.Get(enumerator2.Current, true);
      gameObject1.name = this._currentChar.ToString();
      ++this._currentChar;
      gameObject1.SetActive(true);
      this._troopLabelEntries.Add(component1);
      GameObject gameObject2 = PrefabManagerEx.Instance.Spawn(this.troopInfoTableTemplate.gameObject, this.troopTable.gameObject.transform);
      HorizontalTroopList component2 = gameObject2.GetComponent<HorizontalTroopList>();
      gameObject2.name = this._currentChar.ToString();
      ++this._currentChar;
      gameObject2.SetActive(true);
      component2.OnTablePositioned += new System.Action<HorizontalTroopList>(this.OnTablePositioned);
      component2.SetData(dictionary[enumerator2.Current]);
      this._troopTableEntries.Add(component2);
    }
    Dictionary<TroopInfo, KeyValuePair<int, int>>.KeyCollection.Enumerator enumerator3 = unitMap.Keys.GetEnumerator();
    while (enumerator3.MoveNext())
    {
      int key = unitMap[enumerator3.Current].Key;
      num1 += key;
      int num3 = unitMap[enumerator3.Current].Value;
      num2 += num3;
    }
    if (num1 == 0)
      this.troopProgress.value = 1f;
    else if (num2 == num1)
      this.troopProgress.value = 1f;
    else if (num1 - num2 == 0)
      this.troopProgress.value = 0.0f;
    else
      this.troopProgress.value = (float) num2 / (float) num1;
  }

  public void SetPlayerInfo(Hashtable ht, bool isAttacker)
  {
    if (ht == null)
      return;
    this.SetPlayerData(!isAttacker ? ht[(object) "defender"] as Hashtable : ht[(object) "attacker"] as Hashtable);
    Coordinate coordinate = !isAttacker ? new Coordinate(1, 101, 100) : new Coordinate(1, 100, 100);
    this.labelK.text = coordinate.K.ToString();
    this.labelX.text = coordinate.X.ToString();
    this.labelY.text = coordinate.Y.ToString();
  }

  public void Clear()
  {
    this._currentChar = 'a';
    List<HorizontalTroopList>.Enumerator enumerator1 = this._troopTableEntries.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      HorizontalTroopList current = enumerator1.Current;
      current.Clear();
      GameObject gameObject = current.gameObject;
      gameObject.SetActive(false);
      gameObject.name = this._troopTableEntryName;
      PrefabManagerEx.Instance.Destroy(gameObject);
    }
    this._troopTableEntries.Clear();
    List<UILabel>.Enumerator enumerator2 = this._troopLabelEntries.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      GameObject gameObject = enumerator2.Current.gameObject;
      gameObject.SetActive(false);
      gameObject.name = this._troopLabelEntryName;
      PrefabManagerEx.Instance.Destroy(gameObject);
    }
    this._troopLabelEntries.Clear();
  }

  private void SetPlayerData(Hashtable hashtable)
  {
    this.labelName.text = !hashtable.ContainsKey((object) "name") ? ScriptLocalization.Get("Monster", true) : hashtable[(object) "name"].ToString();
    if (hashtable.ContainsKey((object) "end_power"))
    {
      this.currentPower.text = int.Parse(hashtable[(object) "end_power"].ToString()).ToString();
      this.currentPower.gameObject.SetActive(true);
    }
    else
      this.currentPower.gameObject.SetActive(false);
    if (hashtable.ContainsKey((object) "city_name"))
    {
      this.cityName.text = hashtable[(object) "city_name"] as string;
      this.cityName.gameObject.SetActive(true);
    }
    else
      this.cityName.gameObject.SetActive(false);
    Hashtable hashtable1 = hashtable[(object) "alliance"] as Hashtable;
    if (hashtable1 != null)
    {
      string str = hashtable1[(object) "name"] as string;
      this.labelAlliance.text = "(" + hashtable1[(object) "acronym"] + ") " + str;
      this.allianceDataContainer.SetActive(true);
      this.allianceDataContainer.SetActive(true);
    }
    else
      this.allianceDataContainer.SetActive(false);
  }

  private void OnTablePositioned(HorizontalTroopList obj)
  {
    obj.OnTablePositioned -= new System.Action<HorizontalTroopList>(this.OnTablePositioned);
    bool flag = true;
    List<HorizontalTroopList>.Enumerator enumerator = this._troopTableEntries.GetEnumerator();
    while (enumerator.MoveNext())
      flag &= enumerator.Current.positioned;
    this.troopTable.repositionNow = flag;
  }
}
