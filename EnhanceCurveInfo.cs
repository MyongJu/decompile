﻿// Decompiled with JetBrains decompiler
// Type: EnhanceCurveInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EnhanceCurveInfo
{
  public const int InvalidID = 0;
  public int InternalId;
  public string ID;
  public int Curve;
  public int Level;
  public int EssenceTotal;
  public int EssenceDiff;
  public int EssenceSell;
  public double BenefitIncreasePercentage;
  public double CriticalChance;
  public int CriticalValue;
}
