﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentSuitEnhanceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentSuitEnhanceInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "enhance_level_req")]
  public int enhanceLevelReq;
  [Config(Name = "enhance_value")]
  public float enhanceValue;
  [Config(Name = "type")]
  public string type;
}
