﻿// Decompiled with JetBrains decompiler
// Type: AutoRetryTaskModule
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AutoRetryTaskModule
{
  private Dictionary<int, AutoRetryTask> _tasks = new Dictionary<int, AutoRetryTask>();
  private Queue<AutoRetryTask> _retryList = new Queue<AutoRetryTask>();
  private List<AutoRetryTask> _waits = new List<AutoRetryTask>();

  private int Now
  {
    get
    {
      return (int) Time.time;
    }
  }

  public void SetSuccess(int taskId, bool isSuccess)
  {
    if (!this._tasks.ContainsKey(taskId))
      return;
    AutoRetryTask task = this._tasks[taskId];
    task.Success = isSuccess;
    if (isSuccess)
      task.IsFinish = true;
    else if (this.Now - task.StartTime >= task.RetryMaxTime)
      task.IsFinish = true;
    else
      this._retryList.Enqueue(task);
  }

  public void Initialize()
  {
  }

  public void Remove(int taskId)
  {
    if (!this._tasks.ContainsKey(taskId))
      return;
    this._tasks.Remove(taskId);
  }

  public void Process()
  {
    this.Check();
  }

  public void Execute(AutoRetryTask task)
  {
    task.StartTime = this.Now;
    this._tasks.Add(task.TaskId, task);
    this._retryList.Enqueue(task);
  }

  private void Check()
  {
    if (this._waits.Count > 0)
    {
      for (int index = 0; index < this._waits.Count; ++index)
        this._retryList.Enqueue(this._waits[index]);
      this._waits.Clear();
    }
    if (this._retryList.Count == 0)
      return;
    while (this._retryList.Count > 0)
    {
      AutoRetryTask autoRetryTask = this._retryList.Dequeue();
      if (autoRetryTask.CanRetry)
      {
        autoRetryTask.ExecuteTime = this.Now;
        autoRetryTask.Execute();
      }
      else
      {
        autoRetryTask.Wait();
        this._waits.Add(autoRetryTask);
      }
    }
    this._retryList.Clear();
  }

  public bool IsFinish(int taskId)
  {
    if (this._tasks.ContainsKey(taskId))
      return this._tasks[taskId].IsFinish;
    return false;
  }

  public void Dispose()
  {
    this._tasks.Clear();
  }
}
