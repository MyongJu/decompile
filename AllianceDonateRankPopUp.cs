﻿// Decompiled with JetBrains decompiler
// Type: AllianceDonateRankPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceDonateRankPopUp : Popup
{
  private Stack<AllianceDonateRankItem> pools = new Stack<AllianceDonateRankItem>();
  private List<AllianceDonateRankItemData> totals = new List<AllianceDonateRankItemData>();
  private List<GameObject> items = new List<GameObject>();
  private const int DAY_SECOND = 86400;
  public GameObject rankItemTemplate;
  public UIWrapContent container;
  public UITable tableContainer;
  public UIScrollView scrollView;
  public GameObject refreshContent;
  public UILabel remainTime;
  public UIButtonBar bar;
  private int time;
  public UILabel title;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ToLoadData(0);
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  private void CreateFakeData()
  {
    this.totals.Clear();
    for (int index = 0; index < 10; ++index)
      this.totals.Add(new AllianceDonateRankItemData()
      {
        Rank = index + 1,
        Name = "Test" + (object) index,
        Alliance_EXP = (long) index,
        Donation = (long) index,
        Honor = (long) index
      });
  }

  private void InitItem(GameObject go, int wrapIndex, int realIndex)
  {
    realIndex = Mathf.Abs(realIndex);
    AllianceDonateRankItem component = go.GetComponent<AllianceDonateRankItem>();
    AllianceDonateRankItemData data = (AllianceDonateRankItemData) null;
    if (this.totals.Count > realIndex)
      data = this.totals[realIndex];
    if (data == null)
      return;
    component.SetData(data, realIndex);
  }

  public void ToLoadData(int index = 0)
  {
    this.Clear();
    RequestManager.inst.SendRequest("Alliance:getRank", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (index),
        (object) index
      }
    }, new System.Action<bool, object>(this.LoadDataCallBack), true);
    this.bar.SelectedIndex = index;
  }

  private void Refresh()
  {
    this.totals.Sort(new Comparison<AllianceDonateRankItemData>(this.CompareItem));
    if (this.totals.Count <= 0)
      return;
    this.UpdateUI();
  }

  private void LoadDataCallBack(bool success, object result)
  {
    if (!success)
      return;
    Hashtable hashtable = result as Hashtable;
    if (hashtable == null)
      return;
    if (hashtable.ContainsKey((object) "countdown"))
    {
      string s = hashtable[(object) "countdown"].ToString();
      int result1 = 0;
      if (int.TryParse(s, out result1))
        this.time = result1;
    }
    if (!hashtable.ContainsKey((object) "rank"))
      return;
    ArrayList arrayList = hashtable[(object) "rank"] as ArrayList;
    if (arrayList == null)
      return;
    this.totals.Clear();
    for (int index = 0; index < arrayList.Count; ++index)
    {
      object orgData = arrayList[index];
      AllianceDonateRankItemData donateRankItemData = new AllianceDonateRankItemData();
      donateRankItemData.Decode(orgData);
      donateRankItemData.Rank = index + 1;
      this.totals.Add(donateRankItemData);
    }
    this.Refresh();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private int CompareItem(AllianceDonateRankItemData itemA, AllianceDonateRankItemData itemB)
  {
    return itemA.Rank.CompareTo(itemB.Rank);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("alliance_knowledge_donation_rankings_title", true);
    this.scrollView.gameObject.SetActive(false);
    NGUITools.SetActive(this.refreshContent, this.bar.SelectedIndex != 2);
    this.remainTime.text = Utils.FormatTime(this.time, this.bar.SelectedIndex > 0, false, true);
    this.GenerateCard();
    this.scrollView.gameObject.SetActive(true);
    this.tableContainer.repositionNow = true;
    this.tableContainer.Reposition();
    this.scrollView.ResetPosition();
  }

  private void GenerateCard()
  {
    for (int index = 0; index < this.totals.Count; ++index)
    {
      AllianceDonateRankItem allianceDonateRankItem = this.GetItem();
      allianceDonateRankItem.transform.parent = this.tableContainer.transform;
      allianceDonateRankItem.SetData(this.totals[index], index + 1);
      NGUITools.SetActive(allianceDonateRankItem.gameObject, true);
      this.items.Add(allianceDonateRankItem.gameObject);
    }
    this.scrollView.disableDragIfFits = this.totals.Count < 6;
  }

  private void Process(int timed)
  {
    --this.time;
    this.remainTime.text = Utils.FormatTime(this.time, this.bar.SelectedIndex > 0, false, true);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
    this.bar.OnSelectedHandler += new System.Action<int>(this.ButtonBarClickHandler);
  }

  private void ButtonBarClickHandler(int index)
  {
    this.ToLoadData(index);
  }

  private void RemoveEventHandler()
  {
    this.bar.OnSelectedHandler -= new System.Action<int>(this.ButtonBarClickHandler);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
  }

  private AllianceDonateRankItem GetItem()
  {
    if (this.pools.Count > 0)
      return this.pools.Pop();
    return Utils.DuplicateGOB(this.rankItemTemplate.gameObject).GetComponent<AllianceDonateRankItem>();
  }

  private void Clear()
  {
    this.container.gameObject.SetActive(false);
    for (int index = 0; index < this.items.Count; ++index)
    {
      AllianceDonateRankItem component = this.items[index].GetComponent<AllianceDonateRankItem>();
      component.Clear();
      NGUITools.SetActive(component.gameObject, false);
      component.transform.parent = this.transform;
      this.pools.Push(component);
    }
    this.items.Clear();
  }
}
