﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerMerlinTrials
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerMerlinTrials : BuildingControllerNew
{
  [SerializeField]
  private GameObject _objHasDailyReward;
  [SerializeField]
  private GameObject _objCheckInTip;

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    MerlinTrialsHelper.inst.onMerlinTrialsDataChanged += new System.Action(this.UpdateDailyRewardStatus);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    MerlinTrialsHelper.inst.onMerlinTrialsDataChanged -= new System.Action(this.UpdateDailyRewardStatus);
  }

  public void OnBuildingSelected()
  {
    if (!PlayerData.inst.MerlinTrialsOpen)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_merlin_trail_temporarily_closed"), (System.Action) null, 4f, true);
    }
    else
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("merlin_trials_stronghold_level_min");
      if (data == null || data.ValueInt <= PlayerData.inst.CityData.mStronghold.mLevel)
      {
        UIManager.inst.OpenDlg("MerlinTrials/MelinTrialDlg", (UI.Dialog.DialogParameter) null, true, true, true);
        AudioManager.Instance.PlaySound("sfx_city_click_merlin_trial", false);
      }
      else
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_trial_locked", new Dictionary<string, string>()
        {
          {
            "0",
            data.ValueInt.ToString()
          }
        }, true), (System.Action) null, 4f, true);
    }
  }

  public void OnDailyRewardClicked()
  {
    UIManager.inst.OpenDlg("MerlinTrials/MelinTrialDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClickCityBuilding(this.name);
  }

  public void OnCheckInTipClicked()
  {
    UIManager.inst.OpenDlg("MerlinTrials/MelinTrialDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnSecond(int time)
  {
    this.UpdateDailyRewardStatus();
  }

  private void UpdateDailyRewardStatus()
  {
    bool hasDailyReward = MerlinTrialsHelper.inst.HasDailyReward;
    NGUITools.SetActive(this._objHasDailyReward, hasDailyReward);
    if (hasDailyReward)
      return;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("merlin_trials_stronghold_level_min");
    if (data == null || PlayerData.inst.CityData.mStronghold.mLevel < data.ValueInt)
      return;
    NGUITools.SetActive(this._objCheckInTip, !MerlinTrialsHelper.inst.IsTodayCheckedIn());
  }
}
