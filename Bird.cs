﻿// Decompiled with JetBrains decompiler
// Type: Bird
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class Bird : MonoBehaviour
{
  public float m_Speed = 2000f;
  public GameObject m_RootNode;
  private Camera m_Camera;
  private Vector3 m_Start;
  private Vector3 m_End;
  private Vector3 m_Direction;
  private Vector3 m_Position;

  public void Initialize(Vector3 start, Vector3 end)
  {
    this.m_Camera = UIManager.inst.tileCamera.GetComponent<Camera>();
    this.m_Start = this.m_Camera.transform.InverseTransformPoint(start);
    this.m_End = this.m_Camera.transform.InverseTransformPoint(end);
    this.m_Direction = Vector3.Normalize(this.m_End - this.m_Start);
    this.m_Position = this.m_Start;
    this.UpdateRotation(start, end);
    this.UpdatePosition();
  }

  private void Update()
  {
    this.UpdatePosition();
    this.m_Position += this.m_Direction * this.m_Speed * Time.deltaTime;
  }

  private void UpdatePosition()
  {
    this.transform.localPosition = this.m_Position;
  }

  private void UpdateRotation(Vector3 start, Vector3 end)
  {
    Vector3 forward1 = this.m_Camera.transform.forward;
    Ray ray1 = new Ray(start, forward1);
    Ray ray2 = new Ray(end, forward1);
    Plane plane = new Plane(this.m_RootNode.transform.TransformDirection(Vector3.up), this.m_RootNode.transform.TransformPoint(Vector3.left));
    float enter = 0.0f;
    plane.Raycast(ray1, out enter);
    start = ray1.GetPoint(enter);
    plane.Raycast(ray2, out enter);
    end = ray2.GetPoint(enter);
    Vector3 forward2 = start - end;
    forward2.y = 0.0f;
    Vector3 eulerAngles = Quaternion.LookRotation(forward2).eulerAngles;
    eulerAngles.y -= 45f;
    this.m_RootNode.transform.localRotation = Quaternion.Euler(eulerAngles);
  }
}
