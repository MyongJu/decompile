﻿// Decompiled with JetBrains decompiler
// Type: VIPEntryRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class VIPEntryRender : MonoBehaviour
{
  public UITexture iconTexture;
  public UILabel descriptionLabel;
  public UILabel valueLabel;
  public GameObject increaseGameobject;

  public void Init(Effect effect, bool showIncrease)
  {
    if (effect.Property != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, effect.Property.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.descriptionLabel.text = effect.Property.Name;
      this.valueLabel.text = effect.FormattedModifier;
    }
    else if (effect.Privilege != null)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.iconTexture, effect.Privilege.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.descriptionLabel.text = effect.Privilege.Name;
      this.valueLabel.text = (double) effect.Modifier <= 0.0 ? Utils.XLAT("id_locked") : Utils.XLAT("id_unlocked");
    }
    if (!(bool) ((UnityEngine.Object) this.increaseGameobject))
      return;
    this.increaseGameobject.SetActive(showIncrease);
  }

  public void OnDispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.iconTexture);
  }
}
