﻿// Decompiled with JetBrains decompiler
// Type: KingdomTreasuryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomTreasuryPopup : Popup
{
  private Dictionary<int, KingdomTreasuryItemRenderer> _itemDict = new Dictionary<int, KingdomTreasuryItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UILabel kingdomSilverAmount;
  public UITexture kingdomTreasuryFrame;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private Hashtable _kingdomTreasuryData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomTreasuryPopup.Parameter parameter = orgParam as KingdomTreasuryPopup.Parameter;
    if (parameter != null)
      this._kingdomTreasuryData = parameter.kingdomTreasuryData;
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowKingdomTreasury();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnViewAllotBtnPressed()
  {
    UIManager.inst.OpenPopup("Wonder/KingdomTreasuryRecordPopup", (Popup.PopupParameter) new KingdomTreasuryRecordPopup.Parameter()
    {
      recordType = "assign_log",
      recordData = (this._kingdomTreasuryData == null ? (ArrayList) null : this._kingdomTreasuryData[(object) "assign_log"] as ArrayList)
    });
  }

  public void OnViewTollageBtnPressed()
  {
    UIManager.inst.OpenPopup("Wonder/KingdomTreasuryRecordPopup", (Popup.PopupParameter) new KingdomTreasuryRecordPopup.Parameter()
    {
      recordType = "tax_log",
      recordData = (this._kingdomTreasuryData == null ? (ArrayList) null : this._kingdomTreasuryData[(object) "tax_log"] as ArrayList)
    });
  }

  private void ShowKingdomTreasury()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.kingdomTreasuryFrame, "Texture/GUI_Textures/treasury_top_banner", (System.Action<bool>) null, true, false, string.Empty);
    this.ClearData();
    long outData1 = 0;
    if (this._kingdomTreasuryData != null)
    {
      DatabaseTools.UpdateData(this._kingdomTreasuryData, "kingdom_coin", ref outData1);
      Hashtable inData = this._kingdomTreasuryData[(object) "resource"] as Hashtable;
      long outData2 = 0;
      long outData3 = 0;
      long outData4 = 0;
      long outData5 = 0;
      long outData6 = 0;
      if (inData != null)
      {
        DatabaseTools.UpdateData(inData, "food", ref outData2);
        DatabaseTools.UpdateData(inData, "wood", ref outData3);
        DatabaseTools.UpdateData(inData, "ore", ref outData4);
        DatabaseTools.UpdateData(inData, "silver", ref outData5);
        DatabaseTools.UpdateData(inData, "steel", ref outData6);
      }
      Dictionary<string, long> dictionary = new Dictionary<string, long>();
      dictionary.Add("food", outData2);
      dictionary.Add("wood", outData3);
      dictionary.Add("ore", outData4);
      dictionary.Add("silver", outData5);
      int num = 0;
      Dictionary<string, long>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (dictionary[enumerator.Current] >= 0L)
        {
          KingdomTreasuryItemRenderer itemRenderer = this.CreateItemRenderer(enumerator.Current, dictionary[enumerator.Current]);
          this._itemDict.Add(num++, itemRenderer);
        }
      }
    }
    this.kingdomSilverAmount.text = Utils.FormatThousands(outData1.ToString());
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomTreasuryItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private KingdomTreasuryItemRenderer CreateItemRenderer(string resType, long resAmount)
  {
    GameObject gameObject = this._itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    KingdomTreasuryItemRenderer component = gameObject.GetComponent<KingdomTreasuryItemRenderer>();
    component.SetData(resType, resAmount);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public Hashtable kingdomTreasuryData;
  }
}
