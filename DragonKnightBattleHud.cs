﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightBattleHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightBattleHud : MonoBehaviour
{
  private bool m_Changed = true;
  private const int ASSIGNED_SKILL_COUNT = 3;
  public Camera SceneCamera;
  public Transform ScenePivot;
  public DragonKnightTalentSkillSlot[] TalentSlots;
  public UIGrid KillStackGrid;
  public GameObject KillStackItemPrefab;
  public UIGrid OppKillStackGrid;
  public GameObject OppKillStackItemPrefab;
  public MyPlayerHUD MyHUD;
  public UILabel SpiritValue;
  public UnityEngine.Animation SpiritAnimation;
  public UIGrid ButtonGrid;
  public UIButton BagButton;
  public UIButton ItemButotn;
  public DragonKnightSpeedUpButton SpeedButton;
  public UIButton SettingButton;
  public UIButton QuickBattleButton;
  public UIButton MopupButton;
  public UISprite BlueSpirit;
  public UISprite RedSpirit;

  private void Start()
  {
    this.InitiSkillBar();
    this.OnKnightChanged(PlayerData.inst.dragonKnightData);
  }

  private void OnEnable()
  {
    DBManager.inst.DB_DragonKnight.onDataChanged += new System.Action<DragonKnightData>(this.OnKnightChanged);
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged += new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_DragonKnight.onDataChanged -= new System.Action<DragonKnightData>(this.OnKnightChanged);
    DBManager.inst.DB_DragonKnightDungeon.onDataChanged -= new System.Action<DragonKnightDungeonData>(this.OnDungeonChanged);
  }

  public void OnSkip()
  {
    BattleManager.Instance.AutoAttack();
  }

  public void ShowButtons()
  {
    this.ButtonGrid.gameObject.SetActive(true);
    this.ButtonGrid.Reposition();
  }

  public void HideButtons()
  {
    this.ButtonGrid.gameObject.SetActive(false);
  }

  private void OnKnightChanged(DragonKnightData dragonKnightData)
  {
    if (dragonKnightData == null || dragonKnightData.UserId != PlayerData.inst.uid)
      return;
    DragonKnightLevelInfo byLevel = ConfigManager.inst.DB_DragonKnightLevel.GetByLevel(dragonKnightData.Level);
    string key = "prop_dragon_knight_dungeon_stamina_base_value";
    int dungeonStamina = byLevel.dungeonStamina;
    int finalData = dragonKnightData.GetFinalData(key, dungeonStamina, "calc_dragon_knight_dungeon_stamina");
    this.SpiritValue.text = string.Format("{0}/{1}", (object) dragonKnightData.Spirit.ToString(), (object) finalData);
    this.UpdateMopupColor();
  }

  private void OnDungeonChanged(DragonKnightDungeonData dungeonData)
  {
    if (dungeonData == null || dungeonData.UserId != PlayerData.inst.uid)
      return;
    this.InitiSkillBar();
    this.MyHUD.Reset();
    RoundPlayer myPlayer = BattleManager.Instance.GetMyPlayer();
    if (myPlayer == null)
      return;
    myPlayer.SetCurrentValue(DragonKnightAttibute.Type.Health, AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.Health, 0L));
    myPlayer.SetCurrentValue(DragonKnightAttibute.Type.MP, AttributeCalcHelper.Instance.GetCurrentAttibuteValueInDungen(DragonKnightAttibute.Type.MP, 0L));
  }

  public void ShowBagAndItems()
  {
    this.BagButton.gameObject.SetActive(true);
    this.ItemButotn.gameObject.SetActive(true);
  }

  public void HideBagAndItems()
  {
    this.BagButton.gameObject.SetActive(false);
    this.ItemButotn.gameObject.SetActive(false);
  }

  public void ShowSpeedAndQuickBattle()
  {
    VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
    bool vipActive = PlayerData.inst.hostPlayer.VIPActive;
    this.SpeedButton.gameObject.SetActive(vipActive && (vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_2") || vipLevelInfo.ContainsPrivilege("dk_dungeon_speedup_4")));
    this.QuickBattleButton.gameObject.SetActive(vipActive && vipLevelInfo.ContainsPrivilege("dk_dungeon_skip"));
    this.MopupButton.gameObject.SetActive(false);
    this.ButtonGrid.Reposition();
    this.UpdateMopupColor();
  }

  public void HideSpeedAndQuickBattle()
  {
    this.SpeedButton.gameObject.SetActive(false);
    this.QuickBattleButton.gameObject.SetActive(false);
    this.MopupButton.gameObject.SetActive(DragonKnightUtils.IsMopupButtonEnabled);
    this.ButtonGrid.Reposition();
    this.UpdateMopupColor();
  }

  private void UpdateMopupColor()
  {
    this.BlueSpirit.gameObject.SetActive(DragonKnightUtils.IsSpiritEnough);
    this.RedSpirit.gameObject.SetActive(!DragonKnightUtils.IsSpiritEnough);
  }

  private void OnTalentSkillChanged()
  {
    this.m_Changed = true;
  }

  public void DecreaseCDTime()
  {
    for (int index = 0; index < 3; ++index)
      this.TalentSlots[index].DecreaseCDTime();
  }

  private void Update()
  {
    if (this.m_Changed)
    {
      this.m_Changed = false;
      bool flag = false;
      for (int index = 0; index < 3; ++index)
      {
        if (this.TalentSlots[index].IsActive && this.TalentSlots[index].IsNotCooling)
        {
          this.TalentSlots[index].Blink();
          BattleManager.Instance.UserSkill(this.TalentSlots[index].TalentInfo.internalId, 1L);
          AudioManager.Instance.PlaySound("sfx_ui_click", false);
          flag = true;
        }
        else
          this.TalentSlots[index].Unblink();
      }
      if (!flag)
        BattleManager.Instance.UserSkill(0, 1L);
    }
    this.AlignScene();
  }

  private void AlignScene()
  {
    if (!DragonKnightSystem.Instance.IsReady)
      return;
    Camera roomCamera = DragonKnightSystem.Instance.Controller.RoomHud.RoomCamera;
    Transform roomGroundTransform = DragonKnightSystem.Instance.Controller.RoomHud.RoomGroundTransform;
    if (!(bool) ((UnityEngine.Object) roomCamera) || !(bool) ((UnityEngine.Object) roomGroundTransform) || (!(bool) ((UnityEngine.Object) this.SceneCamera) || !(bool) ((UnityEngine.Object) this.ScenePivot)))
      return;
    Vector3 worldPoint = this.SceneCamera.ScreenToWorldPoint(roomCamera.WorldToScreenPoint(roomGroundTransform.position));
    Vector3 position = roomGroundTransform.TransformPoint(Vector3.up);
    Vector3 lhs = this.SceneCamera.ScreenToWorldPoint(roomCamera.WorldToScreenPoint(position)) - worldPoint;
    lhs.z = 0.0f;
    lhs.Normalize();
    Vector3 forward = this.ScenePivot.forward;
    Vector3 vector3 = Vector3.Cross(lhs, forward);
    this.ScenePivot.up = lhs;
    this.ScenePivot.forward = forward;
    this.ScenePivot.right = vector3;
    worldPoint.z = 0.0f;
    this.ScenePivot.position = worldPoint;
  }

  private void InitiSkillBar()
  {
    int activeSkillSlotCount = DragonKnightUtils.GetActiveSkillSlotCount();
    for (int index = 0; index < 3; ++index)
    {
      this.TalentSlots[index].OnChanged = new System.Action(this.OnTalentSkillChanged);
      this.TalentSlots[index].Unlock(index < activeSkillSlotCount);
    }
    DragonKnightDungeonData knightDungeonData = DBManager.inst.DB_DragonKnightDungeon.Get(PlayerData.inst.uid);
    if (knightDungeonData == null)
      return;
    DragonKnightAssignedSkill assignedSkill = knightDungeonData.AssignedSkill;
    for (int index = 0; index < 3; ++index)
    {
      int talentSkill = assignedSkill.GetTalentSkill(index + 1);
      this.TalentSlots[index].SetData(talentSkill);
    }
  }

  public void PlayerNoSpiritVFX()
  {
    this.SpiritValue.color = Color.red;
    this.SpiritAnimation.Play(AnimationPlayMode.Stop);
    this.SpiritValue.GetComponent<AnimationEventReceiver>().onAnimationEnd = new System.Action(this.OnAnimationEnd);
  }

  private void OnAnimationEnd()
  {
    this.SpiritValue.color = Color.white;
  }

  public void EnableSkillBar()
  {
    DragonKnightUtils.GetActiveSkillSlotCount();
    for (int index = 0; index < 3; ++index)
      this.TalentSlots[index].SetEnable(true);
  }

  public void DisableSkillBar()
  {
    DragonKnightUtils.GetActiveSkillSlotCount();
    for (int index = 0; index < 3; ++index)
      this.TalentSlots[index].SetEnable(false);
  }

  public void AddKillStackItem(int talentSkillId, System.Action callback)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.KillStackItemPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.KillStackGrid.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    KillStackItem component = gameObject.GetComponent<KillStackItem>();
    component.SetData(talentSkillId);
    component.OnDestroy = callback;
  }

  public void AddOppKillStackItem(int talentSkillId, System.Action callback)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.OppKillStackItemPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.OppKillStackGrid.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    KillStackItem component = gameObject.GetComponent<KillStackItem>();
    component.SetData(talentSkillId);
    component.OnDestroy = callback;
  }

  public void TriggerCoolDownTimer(int talentSkillId)
  {
    DragonKnightTalentSkillSlot skillSlot = this.FindSkillSlot(talentSkillId);
    if (!((UnityEngine.Object) skillSlot != (UnityEngine.Object) null))
      return;
    skillSlot.Trigger();
  }

  public void OnInventoryBtnClick()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonInventoryPopup", (Popup.PopupParameter) null);
  }

  public void OnTemporaryItemClicked()
  {
    UIManager.inst.OpenPopup("DungeonTemporaryItemsPopup", (Popup.PopupParameter) null);
  }

  private DragonKnightTalentSkillSlot FindSkillSlot(int talentSkillId)
  {
    for (int index = 0; index < 3; ++index)
    {
      if ((UnityEngine.Object) this.TalentSlots[index] != (UnityEngine.Object) null && this.TalentSlots[index].TalentInfo != null && this.TalentSlots[index].TalentInfo.internalId == talentSkillId)
        return this.TalentSlots[index];
    }
    return (DragonKnightTalentSkillSlot) null;
  }

  public void OnQuitBattle()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightDungeonSettingPopup", (Popup.PopupParameter) null);
  }

  public void OnMopupClick()
  {
    VIP_Level_Info limitForPrivilege1 = ConfigManager.inst.DB_VIP_Level.GetMinLevelLimitForPrivilege("dk_dungeon_layer_complete_1");
    VIP_Level_Info vipLevelInfo = PlayerData.inst.hostPlayer.VIPLevelInfo;
    bool vipActive = PlayerData.inst.hostPlayer.VIPActive;
    if (limitForPrivilege1 == null)
      D.error((object) "[DragonKnightBattleHud]OnMopupClick: vip privilege for mopup is not configured.");
    else if (vipLevelInfo.Level < limitForPrivilege1.Level)
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = Utils.XLAT("dragon_knight_raid_vip_low_title"),
        content = ScriptLocalization.GetWithPara("dragon_knight_raid_must_activate_vip_description", new Dictionary<string, string>()
        {
          {
            "0",
            limitForPrivilege1.Level.ToString()
          }
        }, 1 != 0),
        no = Utils.XLAT("id_uppercase_cancel"),
        yes = Utils.XLAT("vip_upgrade_button"),
        yesCallback = (System.Action) (() => UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true))
      });
    else if (!vipActive)
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = Utils.XLAT("dragon_knight_vip_not_open_title"),
        content = ScriptLocalization.GetWithPara("dragon_knight_raid_must_activate_vip_description", new Dictionary<string, string>()
        {
          {
            "0",
            limitForPrivilege1.Level.ToString()
          }
        }, 1 != 0),
        no = Utils.XLAT("id_uppercase_cancel"),
        yes = Utils.XLAT("vip_activate_button"),
        yesCallback = (System.Action) (() => UIManager.inst.OpenDlg("VIP/VIPItemsDlg", (UI.Dialog.DialogParameter) null, true, true, true))
      });
    else if (!DragonKnightUtils.IsSpiritEnough)
      UIManager.inst.toast.Show(Utils.XLAT("dragon_knight_raid_stamina_low_description"), (System.Action) null, 4f, false);
    else if (DragonKnightUtils.IsCurrentLevelDone)
      UIManager.inst.toast.Show(Utils.XLAT("dragon_knight_floor_explored_description"), (System.Action) null, 4f, false);
    else if (!DragonKnightUtils.CanMopupFurther)
      UIManager.inst.toast.Show(Utils.XLAT("dragon_knight_floor_explored_description"), (System.Action) null, 4f, false);
    else if (vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_3"))
      UIManager.inst.OpenPopup("DragonKnight/DungeonMopupFloorSelectionPopup", (Popup.PopupParameter) null);
    else if (vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_2"))
    {
      VIP_Level_Info limitForPrivilege2 = ConfigManager.inst.DB_VIP_Level.GetMinLevelLimitForPrivilege("dk_dungeon_layer_complete_3");
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("dragon_knight_raid_remaining_title"),
        Content = (ScriptLocalization.GetWithPara("dragon_knight_raid_stamina_floor_spend_description", new Dictionary<string, string>()
        {
          {
            "0",
            DragonKnightUtils.GetMopupStaminaNeeded(1).ToString()
          }
        }, 1 != 0) + "\n" + ScriptLocalization.GetWithPara("dragon_knight_raid_vip_floors_description", new Dictionary<string, string>()
        {
          {
            "0",
            limitForPrivilege2.Level.ToString()
          }
        }, 1 != 0)),
        Okay = Utils.XLAT("id_uppercase_plunder"),
        OkayCallback = (System.Action) (() => DragonKnightUtils.MopupOneLevelWithJob(DragonKnightUtils.CurrentDungeonLevel, (System.Action<bool>) (ret =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenPopup("DragonKnight/DungeonMopupCompletePopup", (Popup.PopupParameter) null);
        })))
      });
    }
    else
    {
      if (!vipLevelInfo.ContainsPrivilege("dk_dungeon_layer_complete_1"))
        return;
      VIP_Level_Info limitForPrivilege2 = ConfigManager.inst.DB_VIP_Level.GetMinLevelLimitForPrivilege("dk_dungeon_layer_complete_2");
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = Utils.XLAT("dragon_knight_raid_remaining_title"),
        content = (ScriptLocalization.GetWithPara("dragon_knight_raid_remaining_description", new Dictionary<string, string>()
        {
          {
            "0",
            DragonKnightUtils.GetMopupStaminaNeeded(1).ToString()
          }
        }, 1 != 0) + "\n" + ScriptLocalization.GetWithPara("dragon_knight_raid_vip_instant_description", new Dictionary<string, string>()
        {
          {
            "0",
            limitForPrivilege2.Level.ToString()
          }
        }, 1 != 0)),
        no = Utils.XLAT("id_uppercase_cancel"),
        yes = Utils.XLAT("id_uppercase_confirm"),
        yesCallback = (System.Action) (() => DragonKnightUtils.MopupOneLevelWithJob(DragonKnightUtils.CurrentDungeonLevel, (System.Action<bool>) (ret =>
        {
          if (!ret)
            return;
          UIManager.inst.OpenPopup("DragonKnight/DungeonCanelPopup", (Popup.PopupParameter) null);
        })))
      });
    }
  }

  private void OnTap(TapGesture gesture)
  {
    RaycastHit hitInfo;
    if (!Physics.Raycast(this.SceneCamera.ScreenPointToRay((Vector3) gesture.Position), out hitInfo, float.PositiveInfinity, 1 << LayerMask.NameToLayer("DragonBattle")))
      return;
    GuardEventReceiver componentInChildren = hitInfo.collider.gameObject.GetComponentInChildren<GuardEventReceiver>();
    if (!componentInChildren.Player.IsAlive)
      return;
    BattleManager.Instance.ChangeTarget(componentInChildren.Player.PlayerId);
  }
}
