﻿// Decompiled with JetBrains decompiler
// Type: RuralBlockInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RuralBlockInfo
{
  public List<int> slots = new List<int>(5);
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int reqLevel;
  [Config(Name = "food")]
  public long reqFood;
  [Config(Name = "wood")]
  public long reqWood;
  [Config(Name = "ore")]
  public long reqOre;
  [Config(Name = "silver")]
  public long reqSilver;
  [Config(Name = "building_slot")]
  public string bSlot;
}
