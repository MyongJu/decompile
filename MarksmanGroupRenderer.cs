﻿// Decompiled with JetBrains decompiler
// Type: MarksmanGroupRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MarksmanGroupRenderer : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelGroupName;
  [SerializeField]
  private UISprite _spriteArrow;
  [SerializeField]
  private UISprite _spritePicked;
  private LuckyArcherGroupInfo _groupInfo;
  public System.Action<LuckyArcherGroupInfo> OnGroupNormalClick;

  public LuckyArcherGroupInfo GroupInfo
  {
    get
    {
      return this._groupInfo;
    }
  }

  public void SetData(LuckyArcherGroupInfo groupInfo)
  {
    this._groupInfo = groupInfo;
    this.UpdateUI();
  }

  public void ShowArrow(bool show)
  {
    NGUITools.SetActive(this._spriteArrow.gameObject, show);
  }

  public void ShowPickedStatus(bool show)
  {
    if (!(bool) ((UnityEngine.Object) this._spritePicked))
      return;
    NGUITools.SetActive(this._spritePicked.gameObject, show);
  }

  public void OnGroupClick()
  {
    if (this.OnGroupNormalClick == null)
      return;
    this.OnGroupNormalClick(this._groupInfo);
  }

  private void UpdateUI()
  {
    if (this._groupInfo == null)
      return;
    this._labelGroupName.text = this._groupInfo.GroupName;
  }
}
