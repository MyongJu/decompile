﻿// Decompiled with JetBrains decompiler
// Type: CityCircleBtnPara
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CityCircleBtnPara
{
  public bool closeAfterClick = true;
  public AtlasType at;
  public string spritename;
  public string titleid;
  public EventDelegate ed;
  public bool isActive;
  public string warnTextId;
  public GameObject attachment;
  public CityTouchCircleBtnAttachment.AttachmentData attachmentpara;
  public float refreshTime;
  public CityCircleBtnPara refreshPara;
  public bool needEffect;
  public bool showRedPoint;

  public CityCircleBtnPara(AtlasType pta, string psname, string ptid, EventDelegate ped = null, bool active = true, string textId = null, GameObject attachment = null, CityTouchCircleBtnAttachment.AttachmentData attachmentpara = null, bool showRedPoint = false)
  {
    this.at = pta;
    this.spritename = psname;
    this.titleid = ptid;
    this.ed = ped;
    this.isActive = active;
    this.warnTextId = textId;
    this.attachment = attachment;
    this.attachmentpara = attachmentpara;
    this.showRedPoint = showRedPoint;
  }

  internal void Reset()
  {
    this.needEffect = false;
  }
}
