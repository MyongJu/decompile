﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuyAndUseDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UnityEngine;

public class AllianceBuyAndUseDialog : MonoBehaviour
{
  private Color32 GOLD_COLOR = new Color32((byte) 246, (byte) 203, (byte) 1, byte.MaxValue);
  public UILabel m_ItemName;
  public UILabel m_Description;
  public UILabel m_Loyalty;
  public UILabel m_OwnCount;
  public UITexture m_Icon;
  public Texture2D m_MissingIcon;
  public UIButton m_UseButton;
  private ShopStaticInfo info;

  private void OnEnable()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnConsumableCountChanged);
  }

  private void OnDisable()
  {
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnConsumableCountChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnConsumableCountChanged);
  }

  private void OnConsumableCountChanged(int itemId)
  {
    this.UpdateNewUI();
  }

  public void Show(ShopStaticInfo info)
  {
    this.gameObject.SetActive(true);
    this.info = info;
    this.UpdateNewUI();
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnClose()
  {
    this.Hide();
  }

  public void OnBuy()
  {
    DBManager.inst.DB_User.Get(PlayerData.inst.uid);
    if (!ItemBag.Instance.CheckCanBuyShopItem(this.info.ID, true))
      return;
    ItemBag.Instance.BuyAllianceGift(this.info.ID, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  public void OnUse()
  {
    ItemBag.Instance.UseItem(this.info.Item_InternalId, 1, (Hashtable) null, (System.Action<bool, object>) null);
  }

  private void UpdateNewUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.info.Item_InternalId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, string.Format("Texture/ItemIcons/ui_{0}", (object) itemStaticInfo.ID), (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemName.text = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
    this.m_Description.text = ScriptLocalization.Get(itemStaticInfo.Loc_Description_Id, true);
    uint rgbFromColor32 = Utils.GetRGBFromColor32(this.GOLD_COLOR);
    int shopStaticQuanlity = ItemBag.Instance.GetShopStaticQuanlity(this.info.ID);
    this.m_OwnCount.text = string.Format("Own: [{0}]{1}[-]", (object) rgbFromColor32.ToString("X2"), (object) shopStaticQuanlity);
    if (shopStaticQuanlity > 0 && ItemBag.CanShopItemBeUsedInStore(this.info))
      this.m_UseButton.isEnabled = true;
    else
      this.m_UseButton.isEnabled = false;
  }
}
