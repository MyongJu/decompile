﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarLogSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UnityEngine;

public class AllianceWarLogSlot : MonoBehaviour
{
  [SerializeField]
  private AllianceWarLogSlot.Panel panel;

  public void Setup(AllianceWarManager.LogData log)
  {
    if (log == null)
      return;
    this.panel.icon.spriteName = !log.isStarter ? "alliance_level_icon" : "icon_attack";
    Color color;
    if (log.isAllyWin)
    {
      this.panel.background.color = this.panel.winColor;
      color = this.panel.winTextColor;
    }
    else
    {
      this.panel.background.color = this.panel.loseColor;
      color = this.panel.loseTextColor;
    }
    this.panel.allyResult.color = color;
    if ((UnityEngine.Object) this.panel.allyName != (UnityEngine.Object) null)
      this.panel.allyName.color = color;
    this.panel.enemyResult.color = color;
    this.panel.enemyName.color = color;
    this.panel.time.color = color;
    this.panel.allyResult.text = !log.isAllyWin ? ScriptLocalization.Get("alliance_battles_uppercase_defeat", true) : ScriptLocalization.Get("alliance_battles_uppercase_victory", true);
    if ((UnityEngine.Object) this.panel.allyName != (UnityEngine.Object) null)
      this.panel.allyName.text = string.IsNullOrEmpty(log.ally.allianceTag) ? log.ally.name : string.Format("[{0}]{1}", (object) log.ally.allianceTag, (object) log.ally.name);
    this.panel.enemyResult.text = !log.isAllyWin ? ScriptLocalization.Get("alliance_battles_uppercase_victory", true) : ScriptLocalization.Get("alliance_battles_uppercase_defeat", true);
    this.panel.enemyName.text = string.IsNullOrEmpty(log.enemy.allianceTag) ? log.enemy.name : string.Format("[{0}]{1}", (object) log.enemy.allianceTag, (object) log.enemy.name);
    if (log.enemy.k > 0)
      this.panel.enemyName.text = this.panel.enemyName.text + ".K" + (object) log.enemy.k;
    this.panel.time.text = this.FormatTime(NetServerTime.inst.ServerTimestamp - log.endTime);
  }

  private string FormatTime(int time)
  {
    if (time < 60)
      return string.Format("{0}sec {1}", (object) time, (object) ScriptLocalization.Get("id_ago", true));
    if (time < 3600)
      return string.Format("{0}min {1}sec {2}", (object) (time / 60), (object) (time % 60), (object) ScriptLocalization.Get("id_ago", true));
    if (time < 86400)
      return string.Format("{0}h {1}min {2}", (object) (time / 3600), (object) (time % 3600 / 60), (object) ScriptLocalization.Get("id_ago", true));
    return string.Format("{0}d {1}h {2}", (object) (time / 86400), (object) (time % 86400 / 3600), (object) ScriptLocalization.Get("id_ago", true));
  }

  public void Reset()
  {
    this.panel.icon = this.transform.Find("Icon").gameObject.gameObject.GetComponent<UISprite>();
    this.panel.background = this.transform.Find("BackgroundColor").gameObject.gameObject.GetComponent<UISprite>();
    this.panel.allyResult = this.transform.Find("AllyResult").gameObject.gameObject.GetComponent<UILabel>();
    this.panel.allyName = this.transform.Find("AllyName").gameObject.gameObject.GetComponent<UILabel>();
    this.panel.enemyResult = this.transform.Find("EnemyResult").gameObject.gameObject.GetComponent<UILabel>();
    this.panel.enemyName = this.transform.Find("EnemyName").gameObject.gameObject.GetComponent<UILabel>();
    this.panel.time = this.transform.Find("Time").gameObject.gameObject.GetComponent<UILabel>();
  }

  [Serializable]
  public class Panel
  {
    public Color winColor;
    public Color loseColor;
    public Color winTextColor;
    public Color loseTextColor;
    public UISprite icon;
    public UISprite background;
    public UILabel allyResult;
    public UILabel allyName;
    public UILabel enemyResult;
    public UILabel enemyName;
    public UILabel time;
  }
}
