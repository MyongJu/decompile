﻿// Decompiled with JetBrains decompiler
// Type: RequestManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

[ExecuteInEditMode]
public class RequestManager : MonoBehaviour
{
  public static List<string> preloadLoader = new List<string>()
  {
    "player:loadUserBank",
    "gift:getBigTimeChest",
    "Wishing:loadData",
    "Mail:getUnReadCount",
    "Player:getPaymentLevel",
    "Alliance:loadWarList",
    "legend:getHeroList"
  };
  public static List<string> backendLoader = new List<string>()
  {
    "Alliance:checkFirstJoinAlliance",
    "casino:getDiyInfo",
    "Wonder:getKingdomBuffList",
    "Activity:getSuperLoginState",
    "PaymentReturn:getPaymentReturnInfo",
    "casino:getLuckyInfo",
    "player:paymentPackage",
    "activity:loginCheckSingleActivityState",
    "AllianceTech:getResearches",
    "Chat:getInitChatRooms",
    "PVP:getWatchTowerEvents",
    "player:checkShieldingPay",
    "anniversary:getKingsConflictList"
  };
  private Dictionary<string, List<System.Action<string, bool>>> _observers = new Dictionary<string, List<System.Action<string, bool>>>();
  private List<string> _preloadLoaderCaches = new List<string>();
  private List<string> _backendLoaderList = new List<string>();
  private HashSet<string> _backendLoaderSet = new HashSet<string>();
  private const int MAX_REQUESGT = 3;
  public const int MAX_TIME_OUT = 5;
  public const int EXCEPTION_0_MAX_COUNT = 10;
  public const int EXCEPTION_0_DEFAULT = 0;
  public const int EXCEPTION_0_WITHOUT_POPUP = 1;
  private int _counter;
  private bool _isRestart;
  private Queue<LoaderBatch> requestQueue;
  private Dictionary<string, LoaderBatch> backendLoaderDict;
  public static RequestManager inst;
  private bool startBlock;
  private RequestAssistant assistant;
  private bool waitRequest;
  private bool waitLoader;
  private LoaderInfo info;

  public void AddObserver(string action, System.Action<string, bool> handler)
  {
    List<System.Action<string, bool>> actionList;
    if (this._observers.ContainsKey(action))
    {
      actionList = this._observers[action];
    }
    else
    {
      actionList = new List<System.Action<string, bool>>();
      this._observers.Add(action, actionList);
    }
    if (actionList.Contains(handler))
      return;
    actionList.Add(handler);
  }

  public void RemoveObserver(string action, System.Action<string, bool> handler)
  {
    if (!this._observers.ContainsKey(action))
      return;
    List<System.Action<string, bool>> observer = this._observers[action];
    if (observer.Contains(handler))
      observer.Remove(handler);
    if (observer.Count != 0)
      return;
    this._observers.Remove(action);
  }

  private void CheckObserver(string action, bool success)
  {
    if (!this._observers.ContainsKey(action))
      return;
    List<System.Action<string, bool>> observer = this._observers[action];
    for (int index = 0; index < observer.Count; ++index)
      observer[index](action, success);
  }

  private void Awake()
  {
    if ((UnityEngine.Object) RequestManager.inst != (UnityEngine.Object) null)
    {
      Debug.LogError((object) "There is should be only one RequestManager object");
    }
    else
    {
      RequestManager.inst = this;
      this.Init();
    }
  }

  private void Init()
  {
    this.requestQueue = new Queue<LoaderBatch>();
    this.backendLoaderDict = new Dictionary<string, LoaderBatch>();
    NetApi.inst.SeqNum = 1;
    this.assistant = new RequestAssistant();
    this._preloadLoaderCaches.Clear();
    this._backendLoaderList.Clear();
    this._backendLoaderSet.Clear();
  }

  private void OnDestroy()
  {
    RequestManager.inst = (RequestManager) null;
  }

  public void Dispose()
  {
    NetApi.inst.SeqNum = 1;
    this._isRestart = false;
    this._observers.Clear();
    this.assistant = new RequestAssistant();
  }

  public void SendLoader(string action, Hashtable postData = null, System.Action<bool, object> callback = null, bool autoRetry = true, bool useStack = false)
  {
    string[] strArray = action.Split(':');
    if (strArray == null || strArray.Length != 2)
    {
      MessageHub.inst.OnError(action, "is not server action");
    }
    else
    {
      if (RequestManager.preloadLoader.Contains(action) && !this._preloadLoaderCaches.Contains(action))
        this._preloadLoaderCaches.Add(action);
      if (postData == null)
        postData = new Hashtable();
      postData[(object) "token"] = (object) NetApi.inst.Token;
      postData[(object) "client_version"] = (object) NativeManager.inst.AppMajorVersion;
      Hashtable hashtable = new Hashtable();
      hashtable[(object) "class"] = (object) strArray[0];
      hashtable[(object) "method"] = (object) strArray[1];
      hashtable[(object) "params"] = (object) postData;
      LoaderInfo info = LoaderManager.inst.PopInfo();
      info.Action = action;
      info.Type = 2;
      info.TimeOut = 5;
      info.AutoRetry = autoRetry;
      info.URL = NetApi.inst.BuildApiUrl(string.Empty);
      info.PostData = hashtable;
      info.OnCallback = callback;
      LoaderBatch batch = LoaderManager.inst.PopBatch();
      batch.Type = info.Type;
      batch.Add(info);
      batch.processEvent += new System.Action<LoaderInfo>(this.OnCallback);
      if (RequestManager.backendLoader.Contains(action))
      {
        if (this._backendLoaderSet.Contains(action))
        {
          if (this._backendLoaderList.Contains(action))
          {
            if (!(this._backendLoaderList[0] != action))
              return;
            this._backendLoaderList.Remove(action);
            this.backendLoaderDict.Remove(action);
            LoaderManager.inst.Add(batch, useStack, false);
          }
          else
            LoaderManager.inst.Add(batch, useStack, false);
        }
        else
        {
          this._backendLoaderSet.Add(action);
          if (this._backendLoaderList.Count == 0)
            LoaderManager.inst.Add(batch, useStack, false);
          this._backendLoaderList.Add(action);
          this.backendLoaderDict.Add(action, batch);
        }
      }
      else
        LoaderManager.inst.Add(batch, useStack, false);
    }
  }

  public void SendRequest(string action, Hashtable postData = null, System.Action<bool, object> callback = null, bool blockScreen = true)
  {
    string[] strArray = action.Split(':');
    if (strArray == null || strArray.Length != 2)
    {
      MessageHub.inst.OnError(action, "not server action");
    }
    else
    {
      LoaderInfo info1 = LoaderManager.inst.PopInfo();
      Hashtable hashtable1 = new Hashtable();
      hashtable1[(object) "op"] = (object) action;
      hashtable1[(object) "args"] = (object) postData;
      hashtable1[(object) "token"] = (object) NetApi.inst.Token;
      hashtable1[(object) "cv"] = (object) NetApi.inst.ConfigFilesVersion;
      hashtable1[(object) "bvg"] = (object) NetApi.inst.BundlesVersionG;
      hashtable1[(object) "bvr"] = (object) NetApi.inst.BundlesVersionR;
      hashtable1[(object) "os"] = (object) NativeManager.inst.GetOS();
      hashtable1[(object) "fpid"] = (object) AccountManager.Instance.FunplusID;
      hashtable1[(object) "session_key"] = (object) AccountManager.Instance.SessionKey;
      hashtable1[(object) "client_version"] = (object) NativeManager.inst.AppMajorVersion;
      Hashtable hashtable2 = new Hashtable();
      hashtable2[(object) "class"] = (object) "call";
      hashtable2[(object) "method"] = (object) "commit";
      hashtable2[(object) "params"] = (object) hashtable1;
      hashtable2[(object) "race_mode"] = (object) PlayerPrefsEx.GetInt("RaceMode", 0);
      info1.Action = action;
      info1.Type = 1;
      if (info1.TimeOut <= 0)
        info1.TimeOut = 5;
      info1.AutoRetry = true;
      info1.URL = NetApi.inst.BuildApiUrl(string.Empty);
      info1.PostData = hashtable2;
      info1.OnCallback = callback;
      info1.BlockScreen = blockScreen;
      LoaderBatch info2 = LoaderManager.inst.PopBatch();
      info2.Type = info1.Type;
      info2.BlockScreen = blockScreen;
      info2.Add(info1);
      info2.processEvent += new System.Action<LoaderInfo>(this.OnCallback);
      info2.finishedEvent += new System.Action<LoaderBatch>(this.OnRequestFinished);
      this.assistant.ShowSystemBlocker(info2);
      this.requestQueue.Enqueue(info2);
      if (this.requestQueue.Count == 3)
      {
        this.startBlock = true;
        this.assistant.Block();
      }
      if (NetApi.inst.SeqNum == 1)
      {
        if (!this.LoaderFinish)
        {
          if (TutorialManager.Instance.CheckTutorialIsFinish())
            this.waitRequest = true;
          else
            this.SendRequest();
        }
        else
          this.SendRequest();
      }
      else
        this.SendRequest();
    }
  }

  private bool LoaderFinish
  {
    get
    {
      return this._preloadLoaderCaches.Count == 0;
    }
  }

  private void SendRequest()
  {
    this.waitRequest = false;
    if (!LoaderManager.inst.RequestEnAble || this.requestQueue.Count == 0)
      return;
    LoaderManager.inst.RequestStart(this.requestQueue.Dequeue(), true);
  }

  private void PushRequestBatch(LoaderInfo info)
  {
  }

  private void OnCallback(LoaderInfo info)
  {
    this.CheckObserver(info.Action, info.ResData != null);
    if (info.ResData != null)
      this.OnResult(info);
    else
      this.OnFailure(info);
    if (this._preloadLoaderCaches.Contains(info.Action))
      this._preloadLoaderCaches.Remove(info.Action);
    if (this.waitRequest && this.LoaderFinish)
      this.SendRequest();
    if (!this._backendLoaderList.Contains(info.Action))
      return;
    this._backendLoaderList.Remove(info.Action);
    this.backendLoaderDict.Remove(info.Action);
    if (this._backendLoaderList.Count <= 0)
      return;
    LoaderManager.inst.Add(this.backendLoaderDict[this._backendLoaderList[0]], false, false);
  }

  private void OnRequestFinished(LoaderBatch obj)
  {
    if (obj.Type != 1)
      return;
    LoaderManager.inst.ReuqestDone();
    if (this.requestQueue.Count < 3 && this.startBlock)
    {
      this.startBlock = false;
      this.assistant.CancelBlock();
    }
    this.assistant.HideSystemBlocker(obj);
    if (!LoaderManager.inst.RequestEnAble || this.requestQueue.Count == 0)
      return;
    LoaderManager.inst.RequestStart(this.requestQueue.Dequeue(), true);
  }

  private void OnResult(LoaderInfo info)
  {
    string json = Encryptor.LoaderDecode(info);
    Hashtable ht = (Hashtable) null;
    if (string.IsNullOrEmpty(json))
      D.error((object) "[Illegal JSON]Json content is null.");
    else if ((int) json[0] != 123)
      D.error((object) string.Format("[Illegal JSON]{0}[Action]{1}[Type]{2}", (object) json.Substring(0, Math.Min(json.Length, 20)), (object) info.Action, (object) info.Type));
    else
      ht = Utils.Json2Object(json, true) as Hashtable;
    bool needToHandler = true;
    if (this.CheckResult(ht, info, ref needToHandler))
    {
      MessageInfo msgInfo = MessageHub.inst.PopMsgInfo();
      msgInfo.Action = info.Action;
      msgInfo.Callback = info.OnCallback;
      msgInfo.Ret = true;
      if (ht.ContainsKey((object) "time") && ht[(object) "time"] != null)
        msgInfo.TimeStamp = long.Parse(ht[(object) "time"].ToString());
      if (ht.ContainsKey((object) "data"))
        msgInfo.Data = ht[(object) "data"];
      if (ht.ContainsKey((object) "payload"))
        msgInfo.Payload = ht[(object) "payload"];
      if (ht.ContainsKey((object) "ok") && ht[(object) "ok"].ToString() == "0")
      {
        msgInfo.Ret = false;
        Hashtable hashtable = ht[(object) "payload"] as Hashtable;
        if (hashtable != null)
        {
          if (hashtable.ContainsKey((object) "errmsg"))
            this.ShowError(info.Action, string.Format("{0} {1}", (object) hashtable[(object) "errno"].ToString(), (object) hashtable[(object) "errmsg"].ToString()));
        }
        else
          this.ShowError(info.Action, "unknown error");
      }
      MessageHub.inst.OnMessage(msgInfo);
    }
    else
    {
      if (!needToHandler)
        return;
      if (info.ErrorMsg == "ok = 4")
      {
        this.info = info.Clone();
        NetWorkDetector.Instance.RetryTip(new System.Action(this.RetryFail), (string) null, (string) null);
      }
      else
      {
        --NetApi.inst.SeqNum;
        this.OnFailure(info);
      }
    }
  }

  private void RetryFail()
  {
    if (this.info == null)
      return;
    --NetApi.inst.SeqNum;
    if (this.info.Type == 2)
      this.SendLoader(this.info.Action, this.info.PostData, this.info.OnCallback, true, false);
    else
      this.SendRequest(this.info.Action, this.info.PostData, this.info.OnCallback, true);
    this.info = (LoaderInfo) null;
  }

  private void OnFailure(LoaderInfo info)
  {
    if (!GameEngine.IsReady())
      return;
    if (info.Type == 1)
    {
      if (!info.IsTimeOut && info.ErrorMsg == null)
        return;
      this.ShowRetry(info.Action, info.ErrorMsg);
    }
    else
    {
      MessageInfo msgInfo = MessageHub.inst.PopMsgInfo();
      msgInfo.Action = info.Action;
      msgInfo.Callback = info.OnCallback;
      msgInfo.Ret = false;
      MessageHub.inst.OnMessage(msgInfo);
    }
  }

  private void ReSendRequest()
  {
    LoaderManager.inst.RequestRetry();
  }

  private void ShowRetry(string action, string errmsg)
  {
    if (this._isRestart)
      return;
    string tip = ScriptLocalization.Get("network_error_timeout_description", true);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Request, action, errmsg);
    NetWorkDetector.Instance.RestartGame(tip, true);
  }

  private void ShowError(string action, string errmsg)
  {
    if (this._isRestart)
      return;
    MessageHub.inst.OnError(action, errmsg);
  }

  private bool CheckResult(Hashtable ht, LoaderInfo info, ref bool needToHandler)
  {
    needToHandler = true;
    if (ht != null && ht.ContainsKey((object) "ok"))
    {
      string key = ht[(object) "ok"].ToString();
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (RequestManager.\u003C\u003Ef__switch\u0024map97 == null)
        {
          // ISSUE: reference to a compiler-generated field
          RequestManager.\u003C\u003Ef__switch\u0024map97 = new Dictionary<string, int>(5)
          {
            {
              "0",
              0
            },
            {
              "1",
              1
            },
            {
              "2",
              2
            },
            {
              "3",
              3
            },
            {
              "4",
              4
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (RequestManager.\u003C\u003Ef__switch\u0024map97.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              return this.HandleException0(ht);
            case 1:
              return this.HandleException1(ht);
            case 2:
              needToHandler = false;
              info.ErrorMsg = "server_maintenance";
              return this.HandleException2(ht);
            case 3:
              needToHandler = false;
              return this.HandleException3(info.Action, ht);
            case 4:
              info.ErrorMsg = "ok = 4";
              return this.HandleException4(ht);
          }
        }
      }
      info.ErrorMsg = "ok = " + ht[(object) "ok"].ToString();
    }
    return false;
  }

  private bool HandleException0(Hashtable ht)
  {
    Hashtable hashtable = ht[(object) "payload"] as Hashtable;
    if ((UnityEngine.Object) UIManager.inst != (UnityEngine.Object) null && hashtable != null && hashtable.ContainsKey((object) "errno"))
    {
      switch (int.Parse(hashtable[(object) "errno"].ToString()) % 10)
      {
        case 0:
          TutorialManager.Instance.Pause();
          UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
          {
            type = CommonBlocker.CommonBlockerType.confirmation,
            displayType = 11,
            title = Utils.XLAT("exception_title"),
            buttonEventText = Utils.XLAT("exception_button"),
            descriptionText = ScriptLocalization.Get(string.Format("exception_{0}", (object) hashtable[(object) "errno"].ToString()), true),
            buttonEvent = (System.Action) null
          });
          break;
        case 1:
          break;
        default:
          UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
          {
            type = CommonBlocker.CommonBlockerType.confirmation,
            displayType = 11,
            title = Utils.XLAT("exception_title"),
            buttonEventText = Utils.XLAT("exception_button"),
            descriptionText = ScriptLocalization.Get(string.Format("exception_{0}", (object) hashtable[(object) "errno"].ToString()), true),
            buttonEvent = (System.Action) null
          });
          break;
      }
    }
    return true;
  }

  private bool HandleException1(Hashtable ht)
  {
    return true;
  }

  private bool HandleException2(Hashtable ht)
  {
    Dictionary<string, string> para = new Dictionary<string, string>();
    int result = 0;
    if (ht.ContainsKey((object) "msg"))
      int.TryParse(ht[(object) "msg"].ToString(), out result);
    int num1 = 0;
    if (result > 0)
    {
      num1 = (int) ((double) result - Utils.ClientSideUnixUTCSeconds);
      if (num1 > 0)
      {
        int num2 = num1 / 60;
        if (num1 % 60 > 0)
          ++num2;
        para.Add("0", num2.ToString());
      }
    }
    string title = ScriptLocalization.Get("server_maintenance_time_title", true);
    string withPara = ScriptLocalization.GetWithPara("server_maintenance_time_description", para, true);
    string leftBt = (string) null;
    if (num1 <= 0)
    {
      int num2 = Mathf.Abs(num1);
      int num3 = num2 / 60;
      if (num2 % 60 > 0)
        ++num3;
      para.Clear();
      para.Add("0", num3.ToString());
      withPara = ScriptLocalization.GetWithPara("toast_server_not_open_yet", para, true);
      leftBt = ScriptLocalization.Get("event_server_ready_login", true);
    }
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Maintenance, string.Empty, string.Empty);
    NetWorkDetector.Instance.RestartOrQuitGame(withPara, title, leftBt);
    return false;
  }

  private bool HandleException3(string action, Hashtable ht)
  {
    string errorDetail = "server msg = ";
    if (ht.ContainsKey((object) "msg"))
      errorDetail = ht[(object) "msg"].ToString();
    string title = ScriptLocalization.Get("data_error_title", true);
    string tip = ScriptLocalization.Get("data_error_sync_description", true);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.ServerError, action, errorDetail);
    NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
    return false;
  }

  private bool HandleException4(Hashtable ht)
  {
    return false;
  }

  public void ShowFullScreenBlock()
  {
    this.assistant.Block();
  }

  public void HideFullScreenBlock()
  {
    this.assistant.CancelBlock();
  }

  public void ShowRetryTip()
  {
    this.assistant.ShowRetry();
  }

  private void RestartGame()
  {
    NetServerTime.inst.StopSyncTime();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    GameEngine.Instance.RestartGame();
  }

  private void RestartGame(string error)
  {
    this._isRestart = true;
    NetServerTime.inst.StopSyncTime();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void BeginCoroutine(IEnumerator enu)
  {
    if (enu == null)
      return;
    this.StartCoroutine(enu);
  }
}
