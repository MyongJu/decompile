﻿// Decompiled with JetBrains decompiler
// Type: SQChatService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SQChatService
{
  private static readonly SQChatService _instance = new SQChatService();
  public const int CHAT_TYPE_WORLD = 1;
  public const int CHAT_TYPE_ALLIANCE = 2;
  public const int CHAT_TYPE_ROOM = 3;
  public const int CHAT_TYPE_PRIVATE = 5;
  public const int CHAT_TYPE_MAIL = 8;
  public const string KEY = "#d1lv8UTT56L!Szy";
  public const string URL_CHECK_CONTENT = "http://cm3.api.37.com.cn/Content/_checkContent";
  public const int MOTHER_GID = 1002671;

  private SQChatService()
  {
  }

  public static SQChatService Instance
  {
    get
    {
      return SQChatService._instance;
    }
  }

  public void RemoteCheckChatContent(string content, int chatType, SQHttpPostProcessor.SQPostCallback callback)
  {
    GameObject gameObject = new GameObject();
    Object.DontDestroyOnLoad((Object) gameObject);
    SQHttpPostProcessor httpPostProcessor = gameObject.AddComponent<SQHttpPostProcessor>();
    int serverTimestamp = NetServerTime.inst.ServerTimestamp;
    int gid = 1002671;
    string uid = PlayerData.inst.uid.ToString();
    string dsid = PlayerData.inst.userData.world_id.ToString();
    string sign = SQChatChecker.GetSign(uid, gid, dsid, serverTimestamp, chatType);
    string userName = PlayerData.inst.userData.userName;
    SQChatChecker postData = new SQChatChecker()
    {
      time = serverTimestamp,
      uid = uid,
      gid = gid,
      dsid = dsid,
      type = chatType,
      sign = sign,
      chat_time = serverTimestamp.ToString(),
      content = WWW.EscapeURL(content),
      actor_name = WWW.EscapeURL(userName)
    };
    httpPostProcessor.DoPost("http://cm3.api.37.com.cn/Content/_checkContent", postData, callback);
  }
}
