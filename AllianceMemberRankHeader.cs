﻿// Decompiled with JetBrains decompiler
// Type: AllianceMemberRankHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceMemberRankHeader : MonoBehaviour, IAllianceRankHeader, IOnlineUpdateable
{
  private Dictionary<long, IMemberItemRenderer> m_ItemDict = new Dictionary<long, IMemberItemRenderer>();
  public UILabel m_RankName;
  public UILabel m_RankLevel;
  public UILabel m_Members;
  public UISprite m_Collapsed;
  public UISprite m_Expanded;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  private AllianceData m_AllianceData;
  private int m_Title;
  private bool m_IsExpanded;
  private System.Action m_OnChanged;
  private bool m_Initialized;
  private bool m_Nominate;

  public void SetData(AllianceData allianceData, int title, bool expand, System.Action onChanged)
  {
    this.m_AllianceData = allianceData;
    this.m_Title = title;
    this.m_IsExpanded = expand;
    this.m_OnChanged = onChanged;
    this.RegisterListeners();
    this.UpdateUI();
  }

  public void SetNominate(bool nominate)
  {
    this.m_Nominate = nominate;
  }

  public void OnHeaderClick()
  {
    this.m_IsExpanded = !this.m_IsExpanded;
    this.UpdateUI();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  public void UpdateOnlineStatus(List<long> status)
  {
    Dictionary<long, IMemberItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateOnlineStatus(status);
  }

  private void UpdateUI()
  {
    this.InitOnce();
    this.m_Grid.gameObject.SetActive(this.m_IsExpanded);
    this.m_Grid.Reposition();
    this.m_RankName.text = this.m_AllianceData.GetTitleName(this.m_Title);
    this.m_RankLevel.text = AllianceTitle.TitleToRank(this.m_Title);
    this.m_Collapsed.gameObject.SetActive(!this.m_IsExpanded);
    this.m_Expanded.gameObject.SetActive(this.m_IsExpanded);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void ClearList()
  {
    using (Dictionary<long, IMemberItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IMemberItemRenderer current = enumerator.Current;
        current.gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemDict.Clear();
  }

  private void InitOnce()
  {
    if (this.m_Initialized)
      return;
    this.m_Initialized = true;
    this.ClearList();
    using (Dictionary<long, AllianceMemberData>.ValueCollection.Enumerator enumerator = this.m_AllianceData.members.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceMemberData current = enumerator.Current;
        if (current.status == 0 && current.title == this.m_Title)
          this.AddItem(DBManager.inst.DB_User.Get(current.uid));
      }
    }
  }

  private void AddItem(UserData user)
  {
    GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab);
    gameObject.SetActive(true);
    IMemberItemRenderer component = gameObject.GetComponent<IMemberItemRenderer>();
    component.SetData(this.m_AllianceData, user, this.m_Title);
    component.SetNominate(this.m_Nominate);
    this.m_ItemDict.Add(user.uid, component);
  }

  private void OnEnable()
  {
    this.RegisterListeners();
  }

  private void OnDisable()
  {
    this.UnregisterListeners();
  }

  private void RegisterListeners()
  {
    if (this.m_AllianceData == null)
      return;
    this.m_AllianceData.members.onDataCreate += new System.Action<AllianceMemberData>(this.OnMemberCreate);
    this.m_AllianceData.members.onDataUpdate += new System.Action<AllianceMemberData>(this.OnMemberUpdate);
    this.m_AllianceData.members.onDataRemoved += new System.Action<AllianceMemberData>(this.OnMemberRemoved);
  }

  private void UnregisterListeners()
  {
    if (this.m_AllianceData == null)
      return;
    this.m_AllianceData.members.onDataCreate -= new System.Action<AllianceMemberData>(this.OnMemberCreate);
    this.m_AllianceData.members.onDataUpdate -= new System.Action<AllianceMemberData>(this.OnMemberUpdate);
    this.m_AllianceData.members.onDataRemoved -= new System.Action<AllianceMemberData>(this.OnMemberRemoved);
  }

  private void OnMemberCreate(AllianceMemberData member)
  {
    if (member.title == this.m_Title)
      this.CreateMember(member);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void OnMemberUpdate(AllianceMemberData member)
  {
    if (member.title == this.m_Title)
      this.CreateMember(member);
    else
      this.RemoveMember(member);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void OnMemberRemoved(AllianceMemberData member)
  {
    this.RemoveMember(member);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void CreateMember(AllianceMemberData member)
  {
    if (this.m_ItemDict.ContainsKey(member.uid))
      return;
    this.AddItem(DBManager.inst.DB_User.Get(member.uid));
    this.m_Grid.Reposition();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  private void RemoveMember(AllianceMemberData member)
  {
    if (!this.m_ItemDict.ContainsKey(member.uid))
      return;
    IMemberItemRenderer memberItemRenderer = this.m_ItemDict[member.uid];
    memberItemRenderer.gameObject.SetActive(false);
    memberItemRenderer.gameObject.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) memberItemRenderer.gameObject);
    this.m_ItemDict.Remove(member.uid);
    this.m_Grid.Reposition();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
