﻿// Decompiled with JetBrains decompiler
// Type: RallySlotBarDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class RallySlotBarDetail : MonoBehaviour
{
  [SerializeField]
  private RallySlotBarDetail.Panel panel;

  public void Setup(Unit unit)
  {
    this.panel.troopIcon.spriteName = this._GetTroopSpriteName(unit);
    this.panel.troopName.text = this._GetTroopName(unit);
    this.panel.troopCount.text = unit.Count.ToString();
  }

  private string _GetTroopSpriteName(Unit unit)
  {
    if (unit.Definition.Troop_Class_ID.Contains("infantry"))
      return "pic_infantry";
    if (unit.Definition.Troop_Class_ID.Contains("ranged"))
      return "pic_ranged";
    if (unit.Definition.Troop_Class_ID.Contains("cavalry"))
      return "pic_cavalry";
    if (unit.Definition.Troop_Class_ID.Contains("mage"))
      return "pic_artillery_close";
    if (unit.Definition.Troop_Class_ID.Contains("seige"))
      return "pic_artillery_distance";
    return string.Empty;
  }

  private string _GetTroopName(Unit unit)
  {
    return unit.Class;
  }

  [Serializable]
  protected class Panel
  {
    public UISprite troopIcon;
    public UILabel troopName;
    public UILabel troopCount;
  }
}
