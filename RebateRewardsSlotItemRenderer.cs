﻿// Decompiled with JetBrains decompiler
// Type: RebateRewardsSlotItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class RebateRewardsSlotItemRenderer : MonoBehaviour
{
  private const float DEFAULT_DESIGN_RATIO = 1.777778f;
  private const float tipPositionOffset = 282f;
  public UILabel slotItemDesc;
  public UIButton rechargeBtn;
  public UIButton collectBtn;
  public UIButton collectedBtn;
  public UILabel tipName;
  public UILabel tipDetails;
  public RebateRewardsSlotItemRenderer.ItemInfo[] rewardsInfo;
  public System.Action OnCollectHandler;
  public System.Action OnItemClickHandler;
  private RebateByRechargeRewards rebateRewards;

  public void SetData(RebateByRechargeRewards rebateRewards)
  {
    this.rebateRewards = rebateRewards;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.rebateRewards == null)
      return;
    if (this.rebateRewards.Rebated)
    {
      NGUITools.SetActive(this.rechargeBtn.gameObject, false);
      NGUITools.SetActive(this.collectBtn.gameObject, false);
      NGUITools.SetActive(this.collectedBtn.gameObject, true);
    }
    else if (RebateByRechargePayload.Instance.RechargedAmount >= (long) this.rebateRewards.TargetRechargeAmount)
    {
      NGUITools.SetActive(this.rechargeBtn.gameObject, false);
      NGUITools.SetActive(this.collectBtn.gameObject, true);
      NGUITools.SetActive(this.collectedBtn.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.rechargeBtn.gameObject, true);
      NGUITools.SetActive(this.collectBtn.gameObject, false);
      NGUITools.SetActive(this.collectedBtn.gameObject, false);
    }
    for (int index = 0; index < this.rewardsInfo.Length; ++index)
    {
      NGUITools.SetActive(this.rewardsInfo[index].itemTexture.transform.parent.gameObject, false);
      if (index < this.rebateRewards.rewardItemInfoList.Count)
      {
        RebateByRechargeRewards.RewardItemInfo rewardItemInfo = this.rebateRewards.rewardItemInfoList[index];
        if (rewardItemInfo != null)
        {
          this.rewardsInfo[index].itemCount.text = rewardItemInfo.rewardItemCount.ToString();
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardItemInfo.rewardItemId);
          if (itemStaticInfo != null)
          {
            BuilderFactory.Instance.HandyBuild((UIWidget) this.rewardsInfo[index].itemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
            Utils.SetItemBackground(this.rewardsInfo[index].backgroud, itemStaticInfo.internalId);
          }
          else
            Utils.SetItemNormalBackground(this.rewardsInfo[index].backgroud, 0);
          NGUITools.SetActive(this.rewardsInfo[index].itemTexture.transform.parent.gameObject, true);
          if (rewardItemInfo.hasSpecialEffect)
          {
            NGUITools.SetActive(this.rewardsInfo[index].itemEffect, true);
            this.UpdateParticleScale(this.rewardsInfo[index].itemEffect, (UIWidget) this.rewardsInfo[index].itemTexture);
          }
        }
      }
    }
    this.slotItemDesc.text = string.Format(Utils.XLAT("event_topup_rewards_reward_description"), (object) this.rebateRewards.Step, (object) this.rebateRewards.TargetRechargeAmount);
  }

  public void OnCollectBtnPressed()
  {
    Hashtable postData = new Hashtable();
    if (this.rebateRewards != null)
      postData.Add((object) "step", (object) this.rebateRewards.Step);
    MessageHub.inst.GetPortByAction("PaymentReturn:gainPaymentReturnRewards").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      RebateByRechargePayload.Instance.Decode(orgData as Hashtable);
      if (this.OnCollectHandler != null)
        this.OnCollectHandler();
      this.ShowCollectEffect();
      this.UpdateUI();
    }), true);
  }

  public void OnRechargeBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnRewardItemPressDown(int clickedIndex)
  {
    if (this.rebateRewards == null || this.rebateRewards.rewardItemInfoList == null)
      return;
    RebateByRechargeRewards.RewardItemInfo rewardItemInfo = this.rebateRewards.rewardItemInfoList[clickedIndex];
    if (rewardItemInfo == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardItemInfo.rewardItemId);
    if (itemStaticInfo == null)
      return;
    Utils.DelayShowTip(itemStaticInfo.internalId, this.rewardsInfo[clickedIndex].border, 0L, 0L, 0);
  }

  public void OnRewardItemRelese()
  {
    Utils.StopShowItemTip();
  }

  public void OnRewardItemPressed(int clickedIndex)
  {
    if (this.OnItemClickHandler != null)
      this.OnItemClickHandler();
    if (this.rebateRewards == null || this.rebateRewards.rewardItemInfoList == null)
      return;
    RebateByRechargeRewards.RewardItemInfo rewardItemInfo = this.rebateRewards.rewardItemInfoList[clickedIndex];
    if (rewardItemInfo == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardItemInfo.rewardItemId);
    if (itemStaticInfo == null)
      return;
    Utils.ShowItemTip(itemStaticInfo.internalId, this.rewardsInfo[clickedIndex].border, 0L, 0L, 0);
  }

  private void ShowItemTips(int index)
  {
    Vector3 localPosition = this.rewardsInfo[index].itemTexture.transform.parent.localPosition;
    this.tipName.transform.parent.localPosition = new Vector3(localPosition.x + 282f, localPosition.y, localPosition.z);
    NGUITools.SetActive(this.tipName.transform.parent.gameObject, true);
    this.StopCoroutine("HideItemTips");
    this.StartCoroutine("HideItemTips");
  }

  [DebuggerHidden]
  private IEnumerator HideItemTips()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RebateRewardsSlotItemRenderer.\u003CHideItemTips\u003Ec__Iterator93()
    {
      \u003C\u003Ef__this = this
    };
  }

  private float AdjustResolution()
  {
    float num1 = (float) Screen.width * 1f / (float) Screen.height;
    float num2 = num1 / 1.777778f;
    if ((double) num1 >= 1.77777779102325)
      num2 = 1f;
    return num2;
  }

  private void UpdateParticleScale(GameObject obj, UIWidget refer)
  {
    float num = this.AdjustResolution();
    obj.transform.localPosition = new Vector3((float) ((double) refer.width * ((double) num - 1.0) / 1.77777779102325), obj.transform.localPosition.y, obj.transform.localPosition.z);
    obj.transform.localRotation = Quaternion.identity;
    obj.transform.localScale = new Vector3(num, num, num);
    ParticleScaler component = obj.GetComponent<ParticleScaler>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.particleScale = num;
    component.alsoScaleGameobject = true;
  }

  private void ShowCollectEffect()
  {
    if (this.rebateRewards == null)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    for (int index = 0; index < this.rebateRewards.rewardItemInfoList.Count; ++index)
    {
      RebateByRechargeRewards.RewardItemInfo rewardItemInfo = this.rebateRewards.rewardItemInfoList[index];
      if (rewardItemInfo != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardItemInfo.rewardItemId);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = (float) rewardItemInfo.rewardItemCount,
          icon = itemStaticInfo.ImagePath
        });
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(true);
  }

  [Serializable]
  public class ItemInfo
  {
    public UITexture backgroud;
    public Transform border;
    public UITexture itemTexture;
    public UILabel itemCount;
    public GameObject itemEffect;
  }
}
