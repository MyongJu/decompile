﻿// Decompiled with JetBrains decompiler
// Type: UISpriteMeshProgressBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UISpriteMeshProgressBar : MonoBehaviour
{
  [SerializeField]
  protected int m_minWidth = 20;
  [SerializeField]
  protected int m_maxWidth = 100;
  protected float m_progress = 1f;
  [SerializeField]
  protected UISpriteMesh m_spriteMesh;
  protected bool m_dirty;

  private void Start()
  {
    this.m_dirty = true;
  }

  private void Update()
  {
    if (!this.m_dirty)
      return;
    this.m_dirty = false;
    if (!(bool) ((Object) this.m_spriteMesh))
      return;
    this.m_spriteMesh.width = Mathf.Max((int) ((double) this.m_progress * (double) this.m_maxWidth), this.m_minWidth);
  }

  public void SetProgress(float progress)
  {
    progress = Mathf.Clamp(progress, 0.0f, 1f);
    if ((double) this.m_progress == (double) progress)
      return;
    this.m_progress = progress;
    this.m_dirty = true;
  }
}
