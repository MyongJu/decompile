﻿// Decompiled with JetBrains decompiler
// Type: MailMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MailMainDlg : UI.Dialog
{
  private Dictionary<string, MailCategory> _toggleToTypeMap = new Dictionary<string, MailCategory>();
  private List<AbstractMailEntry> displayList = new List<AbstractMailEntry>();
  private List<int> selectIndex = new List<int>();
  private List<MailListRender> allMailListRenders = new List<MailListRender>();
  public UILabel titleLabel;
  public UIToggle[] toggles;
  public UIButton composeBtn;
  public List<MailTabEntry> tabList;
  public UIWrapContent wc;
  public UIScrollView scrollView;
  public PageTabsMonitor pageTabsMonitor;
  public GameObject loadMoreSymbol;
  public UILabel loadingTip;
  public MailNoItemDisplay noItem;
  private MailList _currentMailList;
  private MailController controller;
  private bool shouldLoadMore;
  private MailCategory loadingMailCategory;
  private Coroutine loadMoreCoroutine;
  private bool isLoadingMore;
  private bool noMoreMail;
  public GameObject editor;
  public UISprite selectAllIcon;
  public UILabel editorSelectedCoutLabel1;
  public UILabel editorSelectedCoutLabel2;
  public UILabel editorSelectedCoutLabel3;
  private MailMainDlg.Parameter _parameter;

  private void InitUI()
  {
    this.wc.SortBasedOnScrollMovement();
    this.wc.WrapContent();
  }

  private void InitTopToggoles()
  {
    this._toggleToTypeMap.Add("Message", MailCategory.Message);
    this._toggleToTypeMap.Add("BattleReport", MailCategory.BattleReport);
    this._toggleToTypeMap.Add("Alliance", MailCategory.Alliance);
    this._toggleToTypeMap.Add("System", MailCategory.System);
    this._toggleToTypeMap.Add("Report", MailCategory.Report);
    this._toggleToTypeMap.Add("Favorite", MailCategory.Favorite);
    this._toggleToTypeMap.Add("Mod", MailCategory.Mod);
  }

  private void InitEvts()
  {
    this.composeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnComposeBtnClicked)));
    for (int index = 0; index < this.toggles.Length; ++index)
      this.toggles[index].onChange.Add(new EventDelegate(new EventDelegate.Callback(this.SwitchType)));
    this.wc.onInitializeItem += new UIWrapContent.OnInitializeItem(this.InitItem);
    if (this._currentMailList == null)
      return;
    this._currentMailList.mailAdded += new System.Action<AbstractMailEntry, bool>(this.OnMailAdded);
    this._currentMailList.mailRemoved += new System.Action<AbstractMailEntry>(this.OnMailRemoved);
    this._currentMailList.mailUpdate += new System.Action<AbstractMailEntry>(this.OnMailUpdate);
  }

  private void RemoveEvts()
  {
    this.composeBtn.onClick.Clear();
    for (int index = 0; index < this.toggles.Length; ++index)
      this.toggles[index].onChange.Clear();
    this.ClearCurrentMailList();
    this.wc.onInitializeItem -= new UIWrapContent.OnInitializeItem(this.InitItem);
    if (this.loadMoreCoroutine == null)
      return;
    Utils.StopCoroutine(this.loadMoreCoroutine);
  }

  private void InitItem(GameObject go, int wrapIndex, int realIndex)
  {
    realIndex = -realIndex;
    if (this.displayList != null && this.displayList.Count > 0 && (realIndex < this.displayList.Count && realIndex <= -this.wc.minIndex) && realIndex > -1)
    {
      go.SetActive(true);
      MailListRender component = go.GetComponent<MailListRender>();
      AbstractMailEntry display = this.displayList[realIndex];
      component.controller = this.controller;
      component.SetMailEntry(display);
      MailRenderData mailRenderData = new MailRenderData();
      mailRenderData.mail = this.displayList[realIndex];
      mailRenderData.realIndex = realIndex;
      component.data = mailRenderData;
      mailRenderData.isSelected = this.selectIndex.Contains(realIndex);
      component.Show();
      if (component.EditorClick == null)
        component.EditorClick += new System.Action<MailRenderData>(this.OnRenderClick);
      if (this.allMailListRenders.Contains(component))
        return;
      this.allMailListRenders.Add(component);
    }
    else
      go.SetActive(false);
  }

  private void OnRenderClick(MailRenderData data)
  {
    if (data.isSelected)
      this.selectIndex.Add(data.realIndex);
    else
      this.selectIndex.Remove(data.realIndex);
    this.ShowEditor();
  }

  public void AddMail()
  {
    if (this.displayList.Count > 1)
    {
      this.wc.maxIndex = 0;
      this.wc.minIndex = -this.displayList.Count + 1;
    }
    else
    {
      this.wc.minIndex = -1;
      this.wc.maxIndex = 0;
    }
    this.wc.WrapContent();
  }

  private void OnComposeBtnClicked()
  {
    if (PlayerData.inst.moderatorInfo.IsModerator)
      UIManager.inst.OpenPopup("mail/MailTypePopup", (Popup.PopupParameter) null);
    else
      UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnBackBtnClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnMailCategoryChanged(MailCategory mailC)
  {
    if (mailC == MailCategory.Mod)
      ;
  }

  public void SwitchType()
  {
    this.HideSubViews((System.Action) (() =>
    {
      UIToggle activeToggle = UIToggle.GetActiveToggle(2);
      if (!((UnityEngine.Object) activeToggle != (UnityEngine.Object) null) || !this._toggleToTypeMap.ContainsKey(activeToggle.name) || this._currentMailList != null && (this._currentMailList == null || this._toggleToTypeMap[activeToggle.name] == this._currentMailList.MailCategory))
        return;
      this.shouldLoadMore = false;
      this.noMoreMail = false;
      this.loadMoreSymbol.SetActive(false);
      this.loadingTip.gameObject.SetActive(false);
      this.controller.SetCurrentList(this._toggleToTypeMap[activeToggle.name]);
      this.pageTabsMonitor.SetCurrentTab((int) this._currentMailList.MailCategory, true);
      this.OnMailCategoryChanged(this._toggleToTypeMap[activeToggle.name]);
    }));
  }

  public void SetMailList(MailList list)
  {
    if (list == this._currentMailList)
      return;
    this.shouldLoadMore = false;
    this.noMoreMail = false;
    this.isLoadingMore = false;
    this.loadMoreSymbol.SetActive(false);
    this.loadingTip.gameObject.SetActive(false);
    this.scrollView.enabled = true;
    this.scrollView.RestrictWithinBounds(true);
    this.ClearCurrentMailList();
    this._currentMailList = list;
    list.Init(new System.Action(this.MailListInitCallBack));
  }

  private void CheckNoItem()
  {
    if (this._currentMailList == null || this._currentMailList.LoadedCount == 0)
    {
      MailCategory catgory = MailCategory.Message;
      if (this._currentMailList != null)
        catgory = this._currentMailList.MailCategory;
      this.noItem.Show(catgory);
    }
    else
      this.noItem.Hide();
  }

  private void MailListInitCallBack()
  {
    if (this._currentMailList != null)
    {
      this._currentMailList.mailAdded += new System.Action<AbstractMailEntry, bool>(this.OnMailAdded);
      this._currentMailList.mailRemoved += new System.Action<AbstractMailEntry>(this.OnMailRemoved);
      this._currentMailList.mailUpdate += new System.Action<AbstractMailEntry>(this.OnMailUpdate);
      this.displayList = this._currentMailList.GetMail();
    }
    this.UpdateMailList();
    this.UpdateMessageCount();
    this.CheckNoItem();
  }

  private void OnMailAdded(AbstractMailEntry mail, bool fromLoad)
  {
    if (fromLoad)
    {
      this.CancelInvoke();
      this.Invoke("AfterMailAdded", 0.0f);
    }
    else
      this.Refresh();
  }

  private void AfterMailAdded()
  {
    if (this._currentMailList != null)
    {
      this.displayList = this._currentMailList.GetMail();
      if (this.displayList.Count > 1)
      {
        this.wc.maxIndex = 0;
        this.wc.minIndex = -this.displayList.Count + 1;
      }
      else
      {
        this.wc.minIndex = -1;
        this.wc.maxIndex = 0;
      }
      this.wc.WrapContent();
    }
    this.UpdateMessageCount();
  }

  private void OnNewContent()
  {
    this.Refresh();
  }

  private void OnMailRemoved(AbstractMailEntry mail)
  {
    this.Refresh();
    this._currentMailList.GetMoreMail((System.Action) null, false, true);
  }

  private void OnMailUpdate(AbstractMailEntry mail)
  {
    using (List<MailListRender>.Enumerator enumerator = this.allMailListRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Refresh();
    }
    this.UpdateMessageCount();
  }

  private void HideSubViews(System.Action callback)
  {
    this.OnHideEditorBtnClicked();
    callback();
  }

  private List<AbstractMailEntry> GetSelectedMailEntries()
  {
    List<AbstractMailEntry> abstractMailEntryList = new List<AbstractMailEntry>();
    for (int index = 0; index < this.selectIndex.Count; ++index)
    {
      if (this.selectIndex[index] < this.displayList.Count)
        abstractMailEntryList.Add(this.displayList[this.selectIndex[index]]);
    }
    return abstractMailEntryList;
  }

  private List<AbstractMailEntry> GetBatchGiftMailEntries()
  {
    List<AbstractMailEntry> abstractMailEntryList = new List<AbstractMailEntry>();
    for (int index = 0; index < this.selectIndex.Count; ++index)
    {
      if (this.selectIndex[index] < this.displayList.Count)
        abstractMailEntryList.Add(this.displayList[this.selectIndex[index]]);
    }
    return abstractMailEntryList;
  }

  private List<string> GetSelectedSubjects()
  {
    List<string> stringList = new List<string>();
    for (int index = 0; index < this.selectIndex.Count; ++index)
      stringList.Add(this.displayList[this.selectIndex[index]].subject);
    return stringList;
  }

  public void AddSelect(MailListRender select)
  {
    if (!this.selectIndex.Contains(select.data.realIndex))
      this.selectIndex.Add(select.data.realIndex);
    this.UpdateMessageCount();
  }

  public void RemoveSelect(MailListRender select)
  {
    if (this.selectIndex.Contains(select.data.realIndex))
      this.selectIndex.Remove(select.data.realIndex);
    this.UpdateMessageCount();
  }

  private void Update()
  {
    if (!this.scrollView.isDragging)
      return;
    this.loadingTip.text = !this.noMoreMail ? Utils.XLAT("mail_scroll_load_instruction") : Utils.XLAT("mail_scroll_no_more");
    Bounds bounds = this.scrollView.bounds;
    Vector3 constrainOffset = this.scrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.scrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.y < -200.0)
    {
      this.loadingTip.gameObject.SetActive(true);
      this.shouldLoadMore = true;
    }
    else if ((double) constrainOffset.y < 0.0)
    {
      this.loadingTip.gameObject.SetActive(true);
    }
    else
    {
      this.loadMoreSymbol.SetActive(false);
      this.loadingTip.gameObject.SetActive(false);
      this.shouldLoadMore = false;
    }
    if (!this.shouldLoadMore || this.isLoadingMore || (this.noMoreMail || this._currentMailList == null))
      return;
    this.shouldLoadMore = false;
    this.loadMoreSymbol.SetActive(this.shouldLoadMore);
    this.isLoadingMore = true;
    this.loadMoreSymbol.SetActive(true);
    int currentCount = this._currentMailList.LoadedCount;
    this.scrollView.enabled = false;
    this.loadingMailCategory = this._currentMailList.MailCategory;
    try
    {
      this.loadMoreCoroutine = Utils.ExecuteInSecs(1f, (System.Action) (() => this._currentMailList.GetMoreMail((System.Action) (() =>
      {
        try
        {
          if (this.loadingMailCategory != this._currentMailList.MailCategory)
            return;
          this.isLoadingMore = false;
          this.loadMoreSymbol.SetActive(false);
          if (this._currentMailList.LoadedCount != currentCount)
          {
            this.noMoreMail = false;
          }
          else
          {
            this.noMoreMail = true;
            this.loadingTip.text = Utils.XLAT("mail_scroll_no_more");
            this.scrollView.enabled = true;
            this.scrollView.RestrictWithinBounds(true);
          }
          this.scrollView.enabled = true;
        }
        catch
        {
        }
      }), false, true)));
    }
    catch
    {
    }
  }

  private void UpdateMailList()
  {
    this.scrollView.ResetPosition();
    this.Refresh();
  }

  private void UpdateMessageCount()
  {
    using (List<MailTabEntry>.Enumerator enumerator = this.tabList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MailTabEntry current = enumerator.Current;
        if (current.type != MailCategory.Favorite)
        {
          int unreadedCount = this.controller.GetMailListOfType(current.type).GetUnreadedCount();
          current.UpdateUI(unreadedCount);
        }
      }
    }
    this.controller.UpdateUnReadCount();
  }

  private int SortOnMailId(Transform x, Transform y)
  {
    return -(int) (x.gameObject.GetComponent<MailListRender>().mail.mailID - y.gameObject.GetComponent<MailListRender>().mail.mailID);
  }

  public void ShowEditor()
  {
    this.editor.SetActive(true);
    this.UpdateEditor();
  }

  public void UpdateEditor()
  {
    this.selectAllIcon.gameObject.SetActive(this.selectIndex.Count == this.displayList.Count);
    this.editorSelectedCoutLabel1.text = string.Format("({0})", (object) this.selectIndex.Count);
    this.editorSelectedCoutLabel2.text = string.Format("({0})", (object) this.selectIndex.Count);
    this.editorSelectedCoutLabel3.text = string.Format("({0})", (object) this.selectIndex.Count);
  }

  public void OnHideEditorBtnClicked()
  {
    this.selectIndex.Clear();
    using (List<MailListRender>.Enumerator enumerator = this.allMailListRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Selected = false;
    }
    this.editor.SetActive(false);
  }

  public void OnReceiveAllBtnClicked()
  {
    this.ShowRewardAnimation(this.GetSelectedMailEntries());
    this.controller.BatchGainAttachments((int) this._currentMailList.MailCategory, this.GetSelectedMailEntries());
    this.SwitchType();
    this.UpdateEditor();
    this.UpdateMessageCount();
  }

  private void ShowRewardAnimation(List<AbstractMailEntry> enties)
  {
    Dictionary<int, int> dictionary1 = new Dictionary<int, int>();
    for (int index1 = 0; index1 < enties.Count; ++index1)
    {
      Hashtable hashtable = enties[index1].data[(object) "attachment"] as Hashtable;
      if (enties[index1].CanReceiveAttachment())
      {
        RewardData rewardData = JsonReader.Deserialize<RewardData>(Utils.Object2Json((object) hashtable));
        List<IconData> iconDataList = new List<IconData>();
        if (rewardData.item != null)
        {
          RewardsCollectionAnimator.Instance.Clear();
          using (Dictionary<string, int>.Enumerator enumerator = rewardData.item.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<string, int> current = enumerator.Current;
              int key = int.Parse(current.Key);
              int num = current.Value;
              if (dictionary1.ContainsKey(key))
              {
                Dictionary<int, int> dictionary2;
                int index2;
                (dictionary2 = dictionary1)[index2 = key] = dictionary2[index2] + num;
              }
              else
                dictionary1.Add(key, num);
            }
          }
        }
      }
    }
    List<IconData> iconDataList1 = new List<IconData>();
    if (dictionary1.Count > 0)
    {
      RewardsCollectionAnimator.Instance.Clear();
      using (Dictionary<int, int>.Enumerator enumerator = dictionary1.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, int> current = enumerator.Current;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(current.Key);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            icon = itemStaticInfo.ImagePath,
            count = (float) current.Value
          });
        }
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  public void OnSelectAllBtnClicked()
  {
    if (this.selectIndex.Count < this.displayList.Count)
    {
      this.selectIndex.Clear();
      for (int index = 0; index < this.displayList.Count; ++index)
        this.selectIndex.Add(index);
    }
    else
      this.selectIndex.Clear();
    if (this._currentMailList != null)
    {
      this.displayList = this._currentMailList.GetMail();
      if (this.displayList.Count > 1)
      {
        this.wc.maxIndex = 0;
        this.wc.minIndex = -this.displayList.Count + 1;
      }
      else
      {
        this.wc.minIndex = -1;
        this.wc.maxIndex = 0;
      }
      using (List<MailListRender>.Enumerator enumerator = this.allMailListRenders.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MailListRender current = enumerator.Current;
          current.data.isSelected = this.selectIndex.Contains(current.data.realIndex);
          current.Refresh();
        }
      }
    }
    this.UpdateEditor();
  }

  public void OnDeleteBtnClicked()
  {
    if (this._currentMailList.MailCategory == MailCategory.Favorite)
    {
      this.controller.MarkFavToMails(this.GetSelectedMailEntries(), false, (int) this._currentMailList.MailCategory, new System.Action(this.OnDeleteFavCallBack));
    }
    else
    {
      string str1 = ScriptLocalization.Get("mail_delete_confirm", true);
      string str2 = ScriptLocalization.Get("delete_mail_choice_yes", true);
      string str3 = ScriptLocalization.Get("delete_mail_choice_no", true);
      string str4 = ScriptLocalization.Get("id_uppercase_confirm", true);
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
        title = str4,
        leftButtonText = str3,
        leftButtonClickEvent = (System.Action) null,
        closeButtonCallbackEvent = (System.Action) null,
        rightButtonText = str2,
        rightButtonClickEvent = new System.Action(this.ConfirmDeleteCallBack),
        description = str1
      });
    }
  }

  private void ConfirmDeleteCallBack()
  {
    this.controller.DeleteMailList((int) this._currentMailList.MailCategory, this.GetSelectedMailEntries(), new System.Action<bool>(this.OnMailDeleteCallback));
  }

  private void OnDeleteFavCallBack()
  {
    this.Refresh();
  }

  public void OnMailDeleteCallback(bool notDeleteFully)
  {
    if (notDeleteFully)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("embassy_uppercase_notice"),
        Content = Utils.XLAT("mail_delete_attachments_not_deleted_notice"),
        Okay = "OKAY",
        OkayCallback = (System.Action) (() => this._currentMailList.GetMoreMail(new System.Action(this.Refresh), true, true))
      });
    else
      this._currentMailList.GetMoreMail(new System.Action(this.Refresh), true, false);
  }

  public void OnMarkAsReadClicked()
  {
    this.controller.MarkMailAsRead((int) this._currentMailList.MailCategory, this.GetSelectedMailEntries(), true, true);
    this.SwitchType();
    this.UpdateEditor();
    this.UpdateMessageCount();
  }

  public void Refresh()
  {
    if (this._currentMailList == null)
      return;
    this.displayList = this._currentMailList.GetMail();
    if (this.displayList.Count > 1)
    {
      this.wc.maxIndex = 0;
      this.wc.minIndex = -this.displayList.Count + 1;
    }
    else
    {
      this.wc.minIndex = -1;
      this.wc.maxIndex = 0;
    }
    this.wc.SortBasedOnScrollMovement();
    this.wc.WrapContent();
    this.scrollView.ResetPosition();
    this.OnHideEditorBtnClicked();
    this.UpdateMessageCount();
    this.CheckNoItem();
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.InitTopToggoles();
    this.titleLabel.text = Utils.XLAT("mail_uppercase_title");
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.InitEvts();
    this.InitUI();
    this.controller = PlayerData.inst.mail;
    this.controller.mailPanel = this;
    this._parameter = orgParam as MailMainDlg.Parameter;
    this.CheckDefault();
    this.SwitchType();
  }

  private void CheckDefault()
  {
    if (this._parameter == null)
      return;
    for (int index = 0; index < this.toggles.Length; ++index)
    {
      UIToggle toggle = this.toggles[index];
      if (this._toggleToTypeMap.ContainsKey(toggle.name) && this._toggleToTypeMap[toggle.name] == this._parameter.category)
      {
        toggle.startsActive = true;
        break;
      }
      toggle.startsActive = false;
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    using (List<MailListRender>.Enumerator enumerator = this.allMailListRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Refresh();
    }
    this.UpdateMessageCount();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEvts();
  }

  private void ClearCurrentMailList()
  {
    if (this._currentMailList != null)
    {
      this._currentMailList.mailAdded -= new System.Action<AbstractMailEntry, bool>(this.OnMailAdded);
      this._currentMailList.mailRemoved -= new System.Action<AbstractMailEntry>(this.OnMailRemoved);
      this._currentMailList.mailUpdate -= new System.Action<AbstractMailEntry>(this.OnMailUpdate);
      this._currentMailList = (MailList) null;
    }
    using (List<MailListRender>.Enumerator enumerator = this.allMailListRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(false);
    }
    this.scrollView.ResetPosition();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public MailCategory category;
  }
}
