﻿// Decompiled with JetBrains decompiler
// Type: ChatContactBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ChatContactBar : MonoBehaviour
{
  private UserData userData;
  private bool _isOnline;
  private int _unreadMsg;
  public System.Action<long> onBarClick;
  [SerializeField]
  private ChatContactBar.Panel panel;

  public long uid { private set; get; }

  public bool isOnline
  {
    get
    {
      return this._isOnline;
    }
    set
    {
      this._isOnline = value;
      if (this._isOnline)
        this.panel.onlineState.color = this.panel.onlineColor;
      else
        this.panel.onlineState.color = this.panel.offlineColor;
    }
  }

  public string userName
  {
    set
    {
      this.panel.playerName.text = value;
    }
  }

  public int unreadMsg
  {
    get
    {
      return this._unreadMsg;
    }
    set
    {
      this.panel.unreadMsgContainer.gameObject.SetActive(value != 0);
      this.panel.unreadMsgCount.text = value.ToString();
      this._unreadMsg = value;
    }
  }

  public void Setup(long inUid, bool onlineStatus)
  {
    this.uid = inUid;
    this.userData = DBManager.inst.DB_User.Get(this.uid);
    if (this.userData == null)
      return;
    this.userName = this.userData.userName_Kingdom_Alliance_Name;
    this.isOnline = onlineStatus;
    this.unreadMsg = 0;
    CustomIconLoader.Instance.requestCustomIcon(this.panel.playerIcon, this.userData.PortraitIconPath, this.userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.panel.playerIcon, this.userData.LordTitle, 1);
    this.unreadMsg = GameEngine.Instance.ChatManager.GetChannelUnreadCount(this.userData.channelId);
    this.isMute = this.userData.IsShutUp;
  }

  public void OnBarClick()
  {
    if (this.onBarClick == null)
      return;
    this.onBarClick(this.uid);
  }

  public void UpdateOnlineStatus(List<long> onlineStatus)
  {
    if (this.userData == null)
      return;
    this.isOnline = onlineStatus.Contains(this.userData.channelId);
  }

  public bool isMute
  {
    set
    {
      this.panel.muteIcon.gameObject.SetActive(value);
    }
  }

  [Serializable]
  protected class Panel
  {
    public Transform unreadMsgContainer;
    public UILabel unreadMsgCount;
    public UISprite onlineState;
    public Color onlineColor;
    public Color offlineColor;
    public UITexture playerIcon;
    public UILabel playerName;
    public UISprite muteIcon;
  }
}
