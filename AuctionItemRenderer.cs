﻿// Decompiled with JetBrains decompiler
// Type: AuctionItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class AuctionItemRenderer : MonoBehaviour
{
  public UILabel curItemTitle;
  public UILabel curPriceText;
  public UILabel curLeftTimeText;
  public UILabel curMinAddAmount;
  public UILabel curItemCount;
  public UILabel curBidButtonText;
  public UILabel curFeeText;
  public UILabel yourBidPriceText;
  public UISprite selectedHighlight;
  public UITexture curItemTexture;
  public UIInput curBiddingInput;
  public UILabel curBiddingInputText;
  public UIButton curBiddingButton;
  public UISprite normalItemBkg;
  public UISprite equipmentScrollItemBkg;
  public UITexture equipmentItemBkg;
  public UITexture artifactItemBkg;
  public System.Action onAuctionBiddingSuccessed;
  public System.Action<AuctionItemInfo> onAuctionItemPressed;
  public System.Action onAuctionBiddingFinished;
  private AuctionHelper.AuctionItemType _auctionItemType;
  private AuctionHelper.AuctionType _auctionType;
  private AuctionItemInfo _auctionItemInfo;
  private bool _isPublicAuction;
  private long _minBiddingAmount;
  private long _yourBiddingAmount;

  public void SetData(AuctionHelper.AuctionType auctionType, AuctionItemInfo itemInfo)
  {
    this._auctionType = auctionType;
    this._auctionItemInfo = itemInfo;
    this._auctionItemType = AuctionHelper.Instance.GetAuctionItemType(this._auctionItemInfo);
    this._isPublicAuction = this._auctionType == AuctionHelper.AuctionType.PUBLIC_AUCTION || this._auctionType == AuctionHelper.AuctionType.WORLD_AUCTION;
    this._minBiddingAmount = this._auctionItemInfo == null ? 0L : this._auctionItemInfo.itemCurrentPrice + this._auctionItemInfo.itemMinAddAmount;
    this.AddEventHandler();
    this.ShowAuctionItemInfo();
    this.UpdateUI();
  }

  public void OnBidButtonPressed()
  {
    if (this._yourBiddingAmount == 0L || this._yourBiddingAmount < this._minBiddingAmount)
    {
      if (this._auctionType == AuctionHelper.AuctionType.PUBLIC_AUCTION || this._auctionType == AuctionHelper.AuctionType.WORLD_AUCTION)
        UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_exchange_auction_bid_too_low", new Dictionary<string, string>()
        {
          {
            "0",
            this._minBiddingAmount.ToString()
          }
        }, true), (System.Action) null, 4f, true);
      else
        UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_black_market_bid_too_low"), (System.Action) null, 4f, true);
    }
    else
      UIManager.inst.ShowConfirmationBox(Utils.XLAT("exchange_auction_uppercase_bid_confirmation_title"), ScriptLocalization.GetWithPara(!this._isPublicAuction ? "exchange_black_market_bid_confirmation" : "exchange_auction_bid_confirmation", new Dictionary<string, string>()
      {
        {
          "0",
          this.curItemTitle.text
        },
        {
          "1",
          Utils.FormatThousands(this._yourBiddingAmount.ToString())
        },
        {
          "2",
          Utils.FormatThousands(this._auctionItemInfo.itemFeePrice.ToString())
        }
      }, true), Utils.XLAT("id_uppercase_confirm"), Utils.XLAT("id_uppercase_cancel"), ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, (System.Action) (() => this.AuctionBidding()), (System.Action) null, (System.Action) null);
  }

  public void OnItemTexturePressed()
  {
    if (this.onAuctionItemPressed != null)
      this.onAuctionItemPressed(this._auctionItemInfo);
    NGUITools.SetActive(this.selectedHighlight.gameObject, true);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this._auctionItemInfo.itemId, this.curItemTexture.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void HideItemSelectedEffects()
  {
    NGUITools.SetActive(this.selectedHighlight.gameObject, false);
  }

  public void OnBiddingAmountInputChange()
  {
    long num1 = 0;
    long num2 = this._minBiddingAmount = this._auctionItemInfo == null ? 0L : this._auctionItemInfo.itemCurrentPrice + this._auctionItemInfo.itemMinAddAmount;
    bool flag = false;
    try
    {
      num1 = (long) Convert.ToInt32(this.curBiddingInput.value);
      if (!(this.curBiddingInput.value == "0"))
        return;
      flag = true;
      num1 = num2;
    }
    catch (FormatException ex)
    {
      flag = !string.IsNullOrEmpty(this.curBiddingInput.value);
      num1 = num2;
    }
    catch (OverflowException ex)
    {
      flag = true;
      num1 = PlayerData.inst.userData.currency.gold;
    }
    finally
    {
      this._yourBiddingAmount = num1;
      if (flag)
      {
        this.curBiddingInput.value = this._yourBiddingAmount.ToString();
        this.curBiddingInputText.text = this._yourBiddingAmount.ToString();
      }
      this.UpdateBiddingBtnStatus();
    }
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  private void UpdateBiddingBtnStatus()
  {
  }

  [DebuggerHidden]
  private IEnumerator HideItemSelectedHighlight()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AuctionItemRenderer.\u003CHideItemSelectedHighlight\u003Ec__Iterator2C()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void AuctionBidding()
  {
    MessageHub.inst.GetPortByAction("auction:bid").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "auction_id",
        (object) this._auctionItemInfo.itemIndex
      },
      {
        (object) "price",
        (object) this._yourBiddingAmount
      },
      {
        (object) "type",
        (object) this._auctionType
      },
      {
        (object) "server_type",
        (object) this._auctionItemInfo.itemServerType
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        AuctionHelper.Instance.UpdateData(this._auctionType, data as Hashtable, true, false);
        if (this.onAuctionBiddingSuccessed == null)
          return;
        this.onAuctionBiddingSuccessed();
      }
      else
      {
        int outData = 0;
        Hashtable inData = data as Hashtable;
        if (inData != null)
          DatabaseTools.UpdateData(inData, "errno", ref outData);
        if (outData != 2030050)
          return;
        AuctionHelper.Instance.LoadAuctionData(this._auctionType, (System.Action<bool, object>) null, false);
      }
    }), true);
  }

  private void UpdateUI()
  {
    if (this._auctionItemInfo == null)
      return;
    if (this._isPublicAuction)
    {
      this.curPriceText.text = Utils.FormatThousands(this._auctionItemInfo.itemCurrentPrice.ToString());
      if (this._auctionItemInfo.isYourBiddingPrice)
        this.curPriceText.text = string.Format("{0}[3FC526] ({1})[-]", (object) this.curPriceText.text, (object) Utils.XLAT("exchange_auction_bided_description"));
      this.curMinAddAmount.text = Utils.FormatThousands(this._auctionItemInfo.itemMinAddAmount.ToString());
    }
    else
    {
      this.curFeeText.text = Utils.FormatThousands(this._auctionItemInfo.itemFeePrice.ToString());
      if (this._auctionItemInfo.itemYourBidPrice > 0L)
        this.yourBidPriceText.text = Utils.FormatThousands(this._auctionItemInfo.itemYourBidPrice.ToString());
      NGUITools.SetActive(this.yourBidPriceText.transform.parent.gameObject, this._auctionItemInfo.itemYourBidPrice > 0L);
      NGUITools.SetActive(this.curBidButtonText.transform.parent.gameObject, this._auctionItemInfo.itemYourBidPrice <= 0L);
      NGUITools.SetActive(this.curBiddingInput.transform.parent.gameObject, this._auctionItemInfo.itemYourBidPrice <= 0L);
    }
    this.curItemCount.text = "x" + this._auctionItemInfo.itemCount.ToString();
    this.curLeftTimeText.text = Utils.FormatTime(this._auctionItemInfo.itemRemainedTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    this.curBidButtonText.text = Utils.XLAT("id_uppercase_bid");
    this.UpdateBiddingBtnStatus();
    if (this._auctionItemInfo.itemRemainedTime - NetServerTime.inst.ServerTimestamp >= 0 || this.onAuctionBiddingFinished == null)
      return;
    this.onAuctionBiddingFinished();
  }

  private void ShowAuctionItemInfo()
  {
    NGUITools.SetActive(this.equipmentScrollItemBkg.gameObject, false);
    ItemStaticInfo itemStaticInfo = (ItemStaticInfo) null;
    switch (this._auctionItemType)
    {
      case AuctionHelper.AuctionItemType.PROPS:
      case AuctionHelper.AuctionItemType.EQUIPMENT:
      case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
      case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
        itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._auctionItemInfo.itemId);
        break;
    }
    if (itemStaticInfo != null)
    {
      if ((UnityEngine.Object) this.curItemTexture.mainTexture == (UnityEngine.Object) null || (UnityEngine.Object) this.curItemTexture.mainTexture != (UnityEngine.Object) null && this.curItemTexture.mainTexture.name != itemStaticInfo.Image)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.curItemTexture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      Utils.SetItemBackground(this.equipmentItemBkg, itemStaticInfo.internalId);
      BagType bagType = BagType.Hero;
      switch (this._auctionItemType)
      {
        case AuctionHelper.AuctionItemType.EQUIPMENT:
        case AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL:
          bagType = BagType.Hero;
          break;
        case AuctionHelper.AuctionItemType.DK_EQUIPMENT:
        case AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL:
          bagType = BagType.DragonKnight;
          break;
      }
      if (this._auctionItemType == AuctionHelper.AuctionItemType.EQUIPMENT || this._auctionItemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT)
        this.FeedEquipmentData(itemStaticInfo.ID + "_0", bagType);
      else if (this._auctionItemType == AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL || this._auctionItemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL)
      {
        ConfigEquipmentScrollInfo dataByItemId = ConfigManager.inst.GetEquipmentScroll(bagType).GetDataByItemID(itemStaticInfo.internalId);
        if (dataByItemId != null)
        {
          ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(dataByItemId.equipmenID);
          if (data != null)
          {
            this.FeedEquipmentData(data.ID + "_0", bagType);
            NGUITools.SetActive(this.equipmentScrollItemBkg.gameObject, true);
            BuilderFactory.Instance.HandyBuild((UIWidget) this.curItemTexture, dataByItemId.ImagePath(bagType), (System.Action<bool>) null, true, false, string.Empty);
            this.curItemTexture.transform.localScale = new Vector3(0.72f, 0.72f, 1f);
          }
        }
      }
      this.curItemTitle.text = string.Format("{0} [FFFFFF]x {1}[-]", (object) itemStaticInfo.LocName, (object) this._auctionItemInfo.itemCount.ToString());
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this.curItemTitle, itemStaticInfo.Quality);
    }
    if (!this._auctionItemInfo.IsArtifact)
    {
      bool flag = this._auctionItemType == AuctionHelper.AuctionItemType.EQUIPMENT || this._auctionItemType == AuctionHelper.AuctionItemType.EQUIPMENT_SCROLL || this._auctionItemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT || this._auctionItemType == AuctionHelper.AuctionItemType.DK_EQUIPMENT_SCROLL;
      NGUITools.SetActive(this.normalItemBkg.gameObject, false);
      NGUITools.SetActive(this.equipmentItemBkg.gameObject, true);
      NGUITools.SetActive(this.artifactItemBkg.gameObject, false);
    }
    else
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactItemBkg, "Texture/Equipment/frame_equipment_5", (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this.normalItemBkg.gameObject, false);
      NGUITools.SetActive(this.equipmentItemBkg.gameObject, false);
      NGUITools.SetActive(this.artifactItemBkg.gameObject, true);
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(this._auctionItemInfo.itemArtifactId);
      if (artifactInfo != null)
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this.curItemTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        this.curItemTitle.text = string.Format("{0} [FFFFFF]x {1}[-]", (object) artifactInfo.Name, (object) this._auctionItemInfo.itemCount.ToString());
        EquipmentManager.Instance.ConfigQualityLabelWithColor(this.curItemTitle, 5);
      }
    }
    this.UpdateBiddingInputText();
  }

  private void FeedEquipmentData(string equipmentId, BagType bagType)
  {
    ConfigEquipmentPropertyInfo data = ConfigManager.inst.GetEquipmentProperty(bagType).GetData(equipmentId);
    if (data == null)
      return;
    EquipmentComponentData equipmentComponentData = new EquipmentComponentData(data.equipID, 0, bagType, 0L);
    if (equipmentComponentData == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.equipmentItemBkg, equipmentComponentData.equipmentInfo.QualityImagePath, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateBiddingInputText()
  {
    if (this._isPublicAuction)
    {
      this._yourBiddingAmount = this._auctionItemInfo.itemCurrentPrice + this._auctionItemInfo.itemMinAddAmount;
      this.curBiddingInput.value = this._yourBiddingAmount.ToString();
      UIInput curBiddingInput = this.curBiddingInput;
      string str1 = this._yourBiddingAmount.ToString();
      this.curBiddingInputText.text = str1;
      string str2 = str1;
      curBiddingInput.defaultText = str2;
    }
    else
    {
      UIInput curBiddingInput = this.curBiddingInput;
      string str1 = Utils.XLAT("exchange_enter_bid_placeholder");
      this.curBiddingInputText.text = str1;
      string str2 = str1;
      curBiddingInput.defaultText = str2;
    }
  }

  private void OnSecondEventHandler(int time)
  {
    this.UpdateUI();
  }

  private void OnAuctionItemDataUpdated(bool refresh)
  {
    if (!refresh)
      return;
    List<AuctionItemInfo> auctionItemInfoList = AuctionHelper.Instance.GetAuctionItemInfoList(this._auctionType);
    if (auctionItemInfoList == null)
      return;
    AuctionItemInfo auctionItemInfo = auctionItemInfoList.Find((Predicate<AuctionItemInfo>) (x => x.itemIndex == this._auctionItemInfo.itemIndex));
    if (auctionItemInfo == null)
      return;
    this._auctionItemInfo = auctionItemInfo;
    this.UpdateBiddingInputText();
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEventHandler);
    AuctionHelper.Instance.onAuctionItemDataUpdated += new System.Action<bool>(this.OnAuctionItemDataUpdated);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEventHandler);
    AuctionHelper.Instance.onAuctionItemDataUpdated -= new System.Action<bool>(this.OnAuctionItemDataUpdated);
  }
}
