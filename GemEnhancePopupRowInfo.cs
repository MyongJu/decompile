﻿// Decompiled with JetBrains decompiler
// Type: GemEnhancePopupRowInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GemEnhancePopupRowInfo : MonoBehaviour
{
  private readonly Color ColorNextLevel = new Color(0.43f, 0.82f, 0.13f, 1f);
  private readonly Color ColorCurLevel = new Color(0.65f, 0.65f, 0.65f, 1f);
  public UILabel lblCurName;
  public UILabel lblCurValue;
  public UILabel lblNextValue;
  public Transform arrow;

  public void Setup(string name, string value1, string value2, bool canLevelUp, bool showNextLevel = true)
  {
    this.lblCurName.text = name;
    this.lblCurValue.text = value1;
    NGUITools.SetActive(this.arrow.gameObject, showNextLevel);
    NGUITools.SetActive(this.lblNextValue.gameObject, showNextLevel);
    if (!showNextLevel)
      return;
    this.lblNextValue.text = value2;
    if (canLevelUp)
      this.lblNextValue.color = this.ColorNextLevel;
    else
      this.lblNextValue.color = this.ColorCurLevel;
  }
}
