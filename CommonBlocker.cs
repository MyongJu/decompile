﻿// Decompiled with JetBrains decompiler
// Type: CommonBlocker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class CommonBlocker : SystemBlocker
{
  private System.Action onButtonClick;
  private System.Action onLeftButtonClick;
  private System.Action onRightButtonClick;
  private System.Action onCloseButtonClick;
  [SerializeField]
  private CommonBlocker.Panel panel;

  public void OnCloseClick()
  {
    UIManager.inst.HideSystemBlocker(nameof (CommonBlocker), (SystemBlocker.SystemBlockerParameter) null);
    if (this.onCloseButtonClick == null)
      return;
    this.onCloseButtonClick();
  }

  public void OnButtonClick()
  {
    UIManager.inst.HideSystemBlocker(nameof (CommonBlocker), (SystemBlocker.SystemBlockerParameter) null);
    if (this.onButtonClick == null)
      return;
    this.onButtonClick();
  }

  public void OnLeftButtonClick()
  {
    UIManager.inst.HideSystemBlocker(nameof (CommonBlocker), (SystemBlocker.SystemBlockerParameter) null);
    if (this.onLeftButtonClick == null)
      return;
    this.onLeftButtonClick();
  }

  public void OnRightButtonClick()
  {
    UIManager.inst.HideSystemBlocker(nameof (CommonBlocker), (SystemBlocker.SystemBlockerParameter) null);
    if (this.onRightButtonClick == null)
      return;
    this.onRightButtonClick();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CommonBlocker.Parameter parameter = orgParam as CommonBlocker.Parameter;
    if (parameter == null)
      return;
    this.panel.BT_Close.gameObject.SetActive(parameter.hideCloseButton);
    this.onCloseButtonClick = parameter.closeButtonCallbackEvent;
    this.panel.ErrorWarningContainer.gameObject.SetActive(parameter.type == CommonBlocker.CommonBlockerType.error);
    this.panel.ConfirmationContainer.gameObject.SetActive(parameter.type == CommonBlocker.CommonBlockerType.confirmation);
    this.panel.iconConfirmationContainer.gameObject.SetActive(parameter.type == CommonBlocker.CommonBlockerType.iconConfirmation);
    this.panel.ChoiceContainer.gameObject.SetActive(parameter.type == CommonBlocker.CommonBlockerType.choice);
    if (parameter.type == CommonBlocker.CommonBlockerType.error)
    {
      this.panel.title.text = "Error";
      if ((parameter.displayType & 2) > 0)
        this.panel.ErrorWarning_Icon.spriteName = parameter.iconName;
      if ((parameter.displayType & 4) > 0)
        this.panel.ErrorWarning_Title.text = parameter.errorTitleText;
      if ((parameter.displayType & 8) > 0)
        this.panel.ErrorWarning_Description.text = parameter.descriptionText;
    }
    if (parameter.type == CommonBlocker.CommonBlockerType.confirmation)
    {
      this.panel.title.text = "Confirmation";
      if ((parameter.displayType & 2) > 0)
        this.panel.Confirmation_Description.text = parameter.descriptionText;
      if ((parameter.displayType & 4) > 0)
        this.onButtonClick = parameter.buttonEvent;
      if ((parameter.displayType & 8) > 0)
        this.panel.Confirmation_ButtonText.text = parameter.buttonEventText;
    }
    if (parameter.type == CommonBlocker.CommonBlockerType.iconConfirmation)
    {
      this.panel.title.text = "Confirmation";
      if ((parameter.displayType & 2) > 0)
        this.panel.iconConfirmation_Icon.spriteName = parameter.iconName;
      if ((parameter.displayType & 4) > 0)
        this.panel.iconConfirmation_Description.text = parameter.descriptionText;
      if ((parameter.displayType & 8) > 0)
        this.onButtonClick = parameter.buttonEvent;
      if ((parameter.displayType & 16) > 0)
        this.panel.iconConfirmation_ButtonText.text = parameter.buttonEventText;
    }
    if (parameter.type == CommonBlocker.CommonBlockerType.choice)
    {
      this.panel.title.text = "Confirmation";
      if ((parameter.displayType & 2) > 0)
        this.panel.Choice_Icon.spriteName = parameter.iconName;
      if ((parameter.displayType & 4) > 0)
        this.panel.Choice_Description.text = parameter.descriptionText;
      if ((parameter.displayType & 8) > 0)
        this.panel.Choice_Warning.text = parameter.warningText;
      else
        this.panel.Choice_Warning.gameObject.SetActive(false);
      if ((parameter.displayType & 16) > 0)
        this.onLeftButtonClick = parameter.leftButtonEvent;
      if ((parameter.displayType & 32) > 0)
        this.panel.Choice_LeftButtonText.text = parameter.leftButtonEventText;
      if ((parameter.displayType & 64) > 0)
        this.onRightButtonClick = parameter.rightButtonEvent;
      if ((parameter.displayType & 128) > 0)
        this.panel.Choice_RightButtonText.text = parameter.rightButtonEventText;
    }
    if ((parameter.displayType & 1) <= 0)
      return;
    this.panel.title.text = parameter.title;
  }

  public enum CommonBlockerType
  {
    error,
    confirmation,
    iconConfirmation,
    choice,
  }

  public struct PanelChangeType
  {
    public const int title = 1;
    public const int errorWarningIcon = 2;
    public const int errorWarningTitle = 4;
    public const int errorWarningDescription = 8;
    public const int confirmationDescription = 2;
    public const int confirmationButton = 4;
    public const int confirmationButtonText = 8;
    public const int iconConfirmationIcon = 2;
    public const int iconConfirmationDescription = 4;
    public const int iconConfirmationButton = 8;
    public const int iconConfirmationButtonText = 16;
    public const int ChoiceIcon = 2;
    public const int ChoiceDescription = 4;
    public const int ChoiceWarning = 8;
    public const int ChoiceLeftButton = 16;
    public const int ChoiceLeftButtonText = 32;
    public const int ChoiceRightButton = 64;
    public const int ChoiceRightButtonText = 128;
    public const string defaultTitle_Error = "Error";
    public const string defaultTitle_Confirmation = "Confirmation";
    public const string defaultTitle_IconConfirmation = "Confirmation";
    public const string defaultTitle_Choice = "Confirmation";
  }

  public class Parameter : SystemBlocker.SystemBlockerParameter
  {
    public int displayType = 128;
    public const int DEFUALT_DISPLAY_TYPE_ALL_DISPLAY = 128;
    public const int DEFUALT_ERROR_WARNING_TYPE = 6;
    public const int DEFUALT_CONFIRMATION = 6;
    public const int DEFUALT_CONFIRMATION_ICON = 14;
    public const int DEFUALT_CHOICE = 86;
    private CommonBlocker.CommonBlockerType _type;
    public string title;
    public string iconName;
    public string errorTitleText;
    public string descriptionText;
    public string warningText;
    public System.Action buttonEvent;
    public System.Action leftButtonEvent;
    public System.Action rightButtonEvent;
    public string buttonEventText;
    public string leftButtonEventText;
    public string rightButtonEventText;
    public bool hideCloseButton;
    public System.Action closeButtonCallbackEvent;

    public CommonBlocker.CommonBlockerType type
    {
      get
      {
        return this._type;
      }
      set
      {
        this._type = value;
        switch (value)
        {
          case CommonBlocker.CommonBlockerType.error:
            this.displayType = 6;
            break;
          case CommonBlocker.CommonBlockerType.confirmation:
            this.displayType = 6;
            break;
          case CommonBlocker.CommonBlockerType.iconConfirmation:
            this.displayType = 14;
            break;
          case CommonBlocker.CommonBlockerType.choice:
            this.displayType = 86;
            break;
        }
      }
    }
  }

  [Serializable]
  protected class Panel
  {
    public UILabel title;
    public Transform ErrorWarningContainer;
    public UISprite ErrorWarning_Icon;
    public UILabel ErrorWarning_Title;
    public UILabel ErrorWarning_Description;
    public Transform ConfirmationContainer;
    public UILabel Confirmation_Description;
    public UIButton Confirmation_Button;
    public UILabel Confirmation_ButtonText;
    public Transform iconConfirmationContainer;
    public UISprite iconConfirmation_Icon;
    public UILabel iconConfirmation_Description;
    public UIButton iconConfirmation_Button;
    public UILabel iconConfirmation_ButtonText;
    public Transform ChoiceContainer;
    public UISprite Choice_Icon;
    public UILabel Choice_Description;
    public UILabel Choice_Warning;
    public UIButton Choice_LeftButton;
    public UILabel Choice_LeftButtonText;
    public UIButton Choice_RightButton;
    public UILabel Choice_RightButtonText;
    public UIButton BT_Close;
  }
}
