﻿// Decompiled with JetBrains decompiler
// Type: DefendInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DefendInfo : MonoBehaviour
{
  private List<GameObject> pool = new List<GameObject>();
  public GameObject content;
  public GameObject type;
  public GameObject tier;
  public UILabel name;
  public UILabel count;
  public UISprite togglemark;

  public event System.Action toggleDefendInfo;

  public void SeedData(string name, DefendInfo.Data d)
  {
    this.name.text = name;
    if (d.amount == 0L)
    {
      this.count.gameObject.SetActive(false);
    }
    else
    {
      this.count.gameObject.SetActive(true);
      this.count.text = d.amount.ToString();
    }
    if (d.type != null)
    {
      float height1 = (float) this.type.GetComponent<UIWidget>().height;
      float height2 = (float) this.tier.GetComponent<UIWidget>().height;
      float num1 = this.type.transform.localPosition.y + height1 / 2f;
      if (d.tier != null)
      {
        using (Dictionary<string, List<ScoutBar.Data>>.Enumerator enumerator1 = d.tier.GetEnumerator())
        {
          while (enumerator1.MoveNext())
          {
            KeyValuePair<string, List<ScoutBar.Data>> current1 = enumerator1.Current;
            float num2 = num1 - height1 / 2f;
            GameObject gameObject1 = PrefabManagerEx.Instance.Spawn(this.type, this.type.transform.parent);
            this.pool.Add(gameObject1);
            Vector3 localPosition1 = this.type.transform.localPosition;
            localPosition1.y = num2;
            gameObject1.transform.localPosition = localPosition1;
            gameObject1.GetComponent<TypeInfo>().SeedData(new TypeInfo.Data()
            {
              name = current1.Key,
              count = long.Parse(d.type[current1.Key])
            });
            num1 = num2 - height1 / 2f;
            using (List<ScoutBar.Data>.Enumerator enumerator2 = current1.Value.GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                ScoutBar.Data current2 = enumerator2.Current;
                num1 -= height2 / 2f;
                GameObject gameObject2 = PrefabManagerEx.Instance.Spawn(this.tier, this.tier.transform.parent);
                this.pool.Add(gameObject2);
                Vector3 localPosition2 = this.tier.transform.localPosition;
                localPosition2.y = num1;
                gameObject2.transform.localPosition = localPosition2;
                num1 -= height2 / 2f;
                gameObject2.GetComponent<ScoutBar>().SeedData(current2);
              }
            }
          }
        }
      }
      else
      {
        using (Dictionary<string, string>.Enumerator enumerator = d.type.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, string> current = enumerator.Current;
            float num2 = num1 - height1 / 2f;
            GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.type, this.type.transform.parent);
            this.pool.Add(gameObject);
            Vector3 localPosition = this.type.transform.localPosition;
            localPosition.y = num2;
            gameObject.transform.localPosition = localPosition;
            gameObject.GetComponent<TypeInfo>().SeedData(new TypeInfo.Data()
            {
              name = current.Key,
              count = long.Parse(current.Value)
            });
            num1 = num2 - height1 / 2f;
          }
        }
      }
    }
    else if (d.tier != null)
    {
      float height = (float) this.tier.GetComponent<UIWidget>().height;
      float num = this.tier.transform.localPosition.y + height / 2f;
      using (Dictionary<string, List<ScoutBar.Data>>.Enumerator enumerator1 = d.tier.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          using (List<ScoutBar.Data>.Enumerator enumerator2 = enumerator1.Current.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              ScoutBar.Data current = enumerator2.Current;
              num -= height / 2f;
              GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.tier, this.tier.transform.parent);
              this.pool.Add(gameObject);
              Vector3 localPosition = this.tier.transform.localPosition;
              localPosition.y = num;
              gameObject.transform.localPosition = localPosition;
              num -= height / 2f;
              gameObject.GetComponent<ScoutBar>().SeedData(current);
            }
          }
        }
      }
    }
    else
      this.togglemark.gameObject.SetActive(false);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.type)
      this.type.SetActive(false);
    this.tier.SetActive(false);
    this.content.SetActive(false);
  }

  public void ToggleInfo()
  {
    if (!this.togglemark.gameObject.activeSelf)
      return;
    this.content.SetActive(!this.content.activeSelf);
    this.togglemark.transform.localEulerAngles = this.content.activeSelf ? new Vector3(0.0f, 0.0f, 180f) : Vector3.zero;
    if (this.toggleDefendInfo == null)
      return;
    this.toggleDefendInfo();
  }

  public void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.pool.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        PrefabManagerEx.Instance.Destroy(current);
      }
    }
    this.pool.Clear();
    this.content.SetActive(true);
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.type)
      this.type.SetActive(true);
    this.tier.SetActive(true);
    this.togglemark.gameObject.SetActive(true);
    this.togglemark.transform.localEulerAngles = Vector3.zero;
  }

  public class Data
  {
    public long amount;
    public Dictionary<string, string> type;
    public Dictionary<string, List<ScoutBar.Data>> tier;
  }
}
