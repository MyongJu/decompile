﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerBenefitViewRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class MerlinTowerBenefitViewRender : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelBenefitName;
  [SerializeField]
  private UILabel _labelBenefitValue;
  private int _benefitId;
  private float _benefitValue;
  private BenefitViewData data;

  public void SetData(BenefitViewData data)
  {
    if (data == null)
      return;
    this.data = data;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.data.type == 2)
    {
      this._labelBenefitName.text = ScriptLocalization.GetWithPara(this.data.benefitName, new Dictionary<string, string>()
      {
        {
          "0",
          this.data.benefitValue.ToString()
        }
      }, true);
    }
    else
    {
      this._labelBenefitName.text = this.data.benefitName;
      this._labelBenefitValue.text = Utils.GetBenefitValueString(this.data.format, (double) this.data.benefitValue);
    }
  }
}
