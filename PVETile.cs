﻿// Decompiled with JetBrains decompiler
// Type: PVETile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PVETile : MonoBehaviour, IRecycle
{
  [SerializeField]
  private int m_SizeX = 1;
  [SerializeField]
  private int m_SizeY = 1;
  private List<SpriteRenderer> m_SpriteRenderers = new List<SpriteRenderer>();
  private List<IConstructionController> m_Constructions = new List<IConstructionController>();
  [SerializeField]
  private bool m_Centered;
  private ISortingLayerCalculator m_SortingLayerCalculator;
  private bool m_SortingLayerChanged;
  private TileData m_TileData;
  private bool m_TileDataChanged;

  private void Start()
  {
    this.GetComponentsInChildren<SpriteRenderer>(true, (List<M0>) this.m_SpriteRenderers);
    this.GetComponentsInChildren<IConstructionController>(true, (List<M0>) this.m_Constructions);
  }

  public ISortingLayerCalculator calculator
  {
    get
    {
      return this.m_SortingLayerCalculator;
    }
  }

  public virtual void SetSortingLayerID(ISortingLayerCalculator calculator)
  {
    this.m_SortingLayerCalculator = calculator;
    this.m_SortingLayerChanged = true;
  }

  public void UpdateUI(TileData tileData)
  {
    this.m_TileData = tileData;
    this.m_TileDataChanged = true;
  }

  protected virtual void UpdateSortingLayer()
  {
    if (!this.m_SortingLayerChanged)
      return;
    this.m_SortingLayerChanged = false;
    int sortingLayerId = this.calculator.GetSortingLayerID();
    int count1 = this.m_SpriteRenderers.Count;
    for (int index = 0; index < count1; ++index)
      this.m_SpriteRenderers[index].sortingLayerID = sortingLayerId;
    int count2 = this.m_Constructions.Count;
    for (int index = 0; index < count2; ++index)
      this.m_Constructions[index].SetSortingLayerID(sortingLayerId);
  }

  private void InternalUpdateUI()
  {
    if (!this.m_TileDataChanged)
      return;
    this.m_TileDataChanged = false;
    if (this.m_TileData == null)
      return;
    int count = this.m_Constructions.Count;
    for (int index = 0; index < count; ++index)
      this.m_Constructions[index].UpdateUI(this.m_TileData);
  }

  private void Update()
  {
    this.InternalUpdateUI();
    this.UpdateSortingLayer();
  }

  public int xSize
  {
    get
    {
      return this.m_SizeX;
    }
  }

  public int ySize
  {
    get
    {
      return this.m_SizeY;
    }
  }

  public bool IsCenter
  {
    get
    {
      return this.m_Centered;
    }
  }

  public virtual void OnInitialize()
  {
  }

  public virtual void OnFinalize()
  {
    this.m_SortingLayerCalculator = (ISortingLayerCalculator) null;
    this.m_SortingLayerChanged = false;
    this.m_TileData = (TileData) null;
  }
}
