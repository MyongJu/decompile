﻿// Decompiled with JetBrains decompiler
// Type: ConfigDKArenaParticipateReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDKArenaParticipateReward
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, DKArenaParticipateReward> datas;
  private Dictionary<int, DKArenaParticipateReward> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<DKArenaParticipateReward, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public int GetTotalTimes()
  {
    return this.datas.Count;
  }

  public DKArenaParticipateReward GetReward(int time)
  {
    Dictionary<string, DKArenaParticipateReward>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Times == time)
        return enumerator.Current;
    }
    return (DKArenaParticipateReward) null;
  }

  public Dictionary<int, int> GetRewardDetail(int time)
  {
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    DKArenaParticipateReward reward = this.GetReward(time);
    if (reward != null)
    {
      List<Reward.RewardsValuePair> rewards = reward.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
        dictionary.Add(rewards[index].internalID, rewards[index].value);
    }
    return dictionary;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, DKArenaParticipateReward>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, DKArenaParticipateReward>) null;
  }

  public bool Contains(int internalId)
  {
    return this.dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this.datas.ContainsKey(id);
  }

  public DKArenaParticipateReward GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (DKArenaParticipateReward) null;
  }

  public DKArenaParticipateReward GetItem(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (DKArenaParticipateReward) null;
  }
}
