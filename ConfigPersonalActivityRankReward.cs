﻿// Decompiled with JetBrains decompiler
// Type: ConfigPersonalActivityRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigPersonalActivityRankReward
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, PersonalActivityRankReward> datas;
  private Dictionary<int, PersonalActivityRankReward> dicByUniqueId;
  private Dictionary<int, PersonalActivityRankReward> dicByMainInternalID;
  private Dictionary<string, PersonalActivityRankReward> rewardInfoMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<PersonalActivityRankReward, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public List<PersonalActivityRankReward> GetTotalRewardInfos()
  {
    List<PersonalActivityRankReward> activityRankRewardList = new List<PersonalActivityRankReward>();
    Dictionary<int, PersonalActivityRankReward>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
      activityRankRewardList.Add(enumerator.Current);
    return activityRankRewardList;
  }

  public PersonalActivityRankReward GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (PersonalActivityRankReward) null;
  }

  public PersonalActivityRankReward GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (PersonalActivityRankReward) null;
  }

  public PersonalActivityRankReward GetDataByMainInternalID(int internalID)
  {
    return (PersonalActivityRankReward) null;
  }

  public PersonalActivityRankReward GetCurrentRewardByRank(int rank)
  {
    Dictionary<int, PersonalActivityRankReward>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Rank == rank)
        return enumerator.Current;
    }
    return (PersonalActivityRankReward) null;
  }

  public PersonalActivityRankReward GetDataByID(string id)
  {
    if (this.rewardInfoMap == null)
    {
      this.rewardInfoMap = new Dictionary<string, PersonalActivityRankReward>();
      using (Dictionary<int, PersonalActivityRankReward>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, PersonalActivityRankReward> current = enumerator.Current;
          this.rewardInfoMap.Add(current.Value.ID, current.Value);
        }
      }
    }
    return this.rewardInfoMap[id];
  }
}
