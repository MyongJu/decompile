﻿// Decompiled with JetBrains decompiler
// Type: ContactInviteBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class ContactInviteBar : MonoBehaviour
{
  private bool _invitedEnable = true;
  private long _uid;
  [SerializeField]
  private ContactInviteBar.Panel panel;

  public bool invitedEnable
  {
    get
    {
      return this._invitedEnable;
    }
    set
    {
      if (this._invitedEnable == value)
        return;
      this._invitedEnable = value;
      this.panel.BT_invited.isEnabled = value;
    }
  }

  public void OnInvitedClick()
  {
    ContactManager.AddContact(this._uid, new System.Action<bool, object>(this.InviteClick));
  }

  public void OnPlayerInfoButtonClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void InviteClick(bool result, object orgData)
  {
    if (!result)
      return;
    this.invitedEnable = false;
  }

  public void Setup(long uid, string userName, string portrait, string icon, int lordTitleId)
  {
    this._uid = uid;
    CustomIconLoader.Instance.requestCustomIcon(this.panel.userIcon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + portrait, icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.panel.userIcon, lordTitleId, 1);
    this.panel.userName.text = userName;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture userIcon;
    public UILabel userName;
    public UIButton BT_invited;
  }
}
