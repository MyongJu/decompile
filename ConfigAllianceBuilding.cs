﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceBuilding
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceBuilding
{
  private ConfigParse pare = new ConfigParse();
  private Dictionary<int, AllianceBuildingStaticInfo> internalId2Data;
  private Dictionary<string, AllianceBuildingStaticInfo> id2Data;

  public void BuildDB(object org)
  {
    this.pare.Parse<AllianceBuildingStaticInfo, string>(org as Hashtable, "ID", out this.id2Data, out this.internalId2Data);
  }

  public AllianceBuildingStaticInfo GetOneFortress()
  {
    if (this.internalId2Data != null)
    {
      Dictionary<int, AllianceBuildingStaticInfo>.ValueCollection.Enumerator enumerator = this.internalId2Data.Values.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.type == 0)
          return enumerator.Current;
      }
    }
    return (AllianceBuildingStaticInfo) null;
  }

  public AllianceBuildingStaticInfo Get(int interalId)
  {
    if (this.internalId2Data.ContainsKey(interalId))
      return this.internalId2Data[interalId];
    return (AllianceBuildingStaticInfo) null;
  }

  public AllianceBuildingStaticInfo Get(string id)
  {
    if (this.id2Data.ContainsKey(id))
      return this.id2Data[id];
    return (AllianceBuildingStaticInfo) null;
  }

  public List<AllianceBuildingStaticInfo> GetTotalBuildings(int type)
  {
    List<AllianceBuildingStaticInfo> buildingStaticInfoList = new List<AllianceBuildingStaticInfo>();
    Dictionary<int, AllianceBuildingStaticInfo>.ValueCollection.Enumerator enumerator = this.internalId2Data.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.type == type)
        buildingStaticInfoList.Add(enumerator.Current);
    }
    return buildingStaticInfoList;
  }

  private int Compare(AllianceBuildingStaticInfo a, AllianceBuildingStaticInfo b)
  {
    return a.ReqAllianceLev.CompareTo(b.ReqAllianceLev);
  }

  public int AllianceWarehouseConfigId
  {
    get
    {
      int num = 0;
      using (Dictionary<int, AllianceBuildingStaticInfo>.Enumerator enumerator = this.internalId2Data.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceBuildingStaticInfo> current = enumerator.Current;
          if (current.Value.type == 2)
            num = current.Key;
        }
      }
      return num;
    }
  }

  public int AllianceHospitalConfigId
  {
    get
    {
      int num = 0;
      using (Dictionary<int, AllianceBuildingStaticInfo>.Enumerator enumerator = this.internalId2Data.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceBuildingStaticInfo> current = enumerator.Current;
          if (current.Value.type == 8)
            num = current.Key;
        }
      }
      return num;
    }
  }

  public AllianceBuildingStaticInfo AllianceHospitalInfo
  {
    get
    {
      using (Dictionary<int, AllianceBuildingStaticInfo>.Enumerator enumerator = this.internalId2Data.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceBuildingStaticInfo> current = enumerator.Current;
          if (current.Value.type == 8)
            return current.Value;
        }
      }
      return (AllianceBuildingStaticInfo) null;
    }
  }

  public int AlliancePortalConfigId
  {
    get
    {
      int num = 0;
      using (Dictionary<int, AllianceBuildingStaticInfo>.Enumerator enumerator = this.internalId2Data.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, AllianceBuildingStaticInfo> current = enumerator.Current;
          if (current.Value.type == 7)
            num = current.Key;
        }
      }
      return num;
    }
  }
}
