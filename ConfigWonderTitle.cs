﻿// Decompiled with JetBrains decompiler
// Type: ConfigWonderTitle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigWonderTitle
{
  private List<WonderTitleInfo> wonderTitleInfoList = new List<WonderTitleInfo>();
  public const string KING_ID = "1";
  public System.Action onWonderTitleDataUpdated;
  private Dictionary<string, WonderTitleInfo> datas;
  private Dictionary<int, WonderTitleInfo> dicByUniqueId;
  private List<WonderTitleInfo> backupWonderTitleInfoList;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<WonderTitleInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, WonderTitleInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.wonderTitleInfoList.Add(enumerator.Current);
    }
    this.wonderTitleInfoList.Sort((Comparison<WonderTitleInfo>) ((a, b) => a.sort.CompareTo(b.sort)));
    this.backupWonderTitleInfoList = new List<WonderTitleInfo>((IEnumerable<WonderTitleInfo>) this.wonderTitleInfoList.ToArray());
  }

  public List<WonderTitleInfo> GetWonderTitleInfoList()
  {
    return this.wonderTitleInfoList;
  }

  public List<WonderTitleInfo> GetWonderTitleInfoListByType(int titleType)
  {
    if (this.wonderTitleInfoList != null)
    {
      List<WonderTitleInfo> all = this.wonderTitleInfoList.FindAll((Predicate<WonderTitleInfo>) (x => x.titleType == titleType));
      if (all != null)
      {
        all.Sort((Comparison<WonderTitleInfo>) ((a, b) => a.sort.CompareTo(b.sort)));
        return all;
      }
    }
    return (List<WonderTitleInfo>) null;
  }

  public WonderTitleInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (WonderTitleInfo) null;
  }

  public WonderTitleInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (WonderTitleInfo) null;
  }

  public void UpdateWonderTitleData(Hashtable data, bool forceUpdate = false)
  {
    if (forceUpdate)
    {
      this.wonderTitleInfoList = new List<WonderTitleInfo>((IEnumerable<WonderTitleInfo>) this.backupWonderTitleInfoList.ToArray());
      this.wonderTitleInfoList.ForEach((System.Action<WonderTitleInfo>) (x =>
      {
        x.IsNominated = false;
        x.NominatedUser = new WonderTitleInfo.NominatedUserInfo();
      }));
    }
    if (data == null)
      return;
    foreach (object key1 in (IEnumerable) data.Keys)
    {
      string key = key1.ToString();
      int outData1 = 0;
      long outData2 = 0;
      string empty1 = string.Empty;
      string outData3 = "0";
      string empty2 = string.Empty;
      string empty3 = string.Empty;
      int outData4 = 0;
      Hashtable inData = data[(object) key] as Hashtable;
      if (inData != null)
      {
        if (inData.ContainsKey((object) "cd_time"))
          DatabaseTools.UpdateData(inData, "cd_time", ref outData1);
        DatabaseTools.UpdateData(inData, "uid", ref outData2);
        DatabaseTools.UpdateData(inData, "name", ref empty1);
        DatabaseTools.UpdateData(inData, "portrait", ref outData3);
        DatabaseTools.UpdateData(inData, "icon", ref empty2);
        DatabaseTools.UpdateData(inData, "lord_title", ref outData4);
        if (inData.ContainsKey((object) "alliance"))
          DatabaseTools.UpdateData(inData, "alliance", ref empty3);
        WonderTitleInfo wonderTitleInfo = this.wonderTitleInfoList.Find((Predicate<WonderTitleInfo>) (x => x.id == key));
        if (wonderTitleInfo != null)
        {
          this.wonderTitleInfoList.Remove(wonderTitleInfo);
          WonderTitleInfo.NominatedUserInfo nominatedUserInfo = new WonderTitleInfo.NominatedUserInfo();
          nominatedUserInfo.TitleCdTime = outData1;
          nominatedUserInfo.Portrait = outData3;
          nominatedUserInfo.UserId = outData2;
          nominatedUserInfo.UserName = empty1;
          nominatedUserInfo.UserImage = empty2;
          nominatedUserInfo.LordTitleId = outData4;
          nominatedUserInfo.AllianceName = empty3;
          wonderTitleInfo.IsNominated = nominatedUserInfo.UserId > 0L;
          wonderTitleInfo.NominatedUser = nominatedUserInfo;
          this.wonderTitleInfoList.Add(wonderTitleInfo);
        }
      }
    }
    this.wonderTitleInfoList.Sort((Comparison<WonderTitleInfo>) ((a, b) => a.sort.CompareTo(b.sort)));
    if (this.onWonderTitleDataUpdated == null)
      return;
    this.onWonderTitleDataUpdated();
  }
}
