﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightScoreReqInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FallenKnightScoreReqInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "need_self_score")]
  public long IndividualReqScore;
  [Config(Name = "need_alliance_score")]
  public long AllianceReqScore;
  [Config(Name = "fund")]
  public long RewardFund;
  [Config(Name = "honor")]
  public long RewardHonor;
  [Config(Name = "reward_id_1")]
  public string RankReward_1;
  [Config(Name = "reward_value_1")]
  public string RankRewardValue_1;
  [Config(Name = "reward_id_2")]
  public string RankReward_2;
  [Config(Name = "reward_value_2")]
  public string RankRewardValue_2;
  [Config(Name = "reward_id_3")]
  public string RankReward_3;
  [Config(Name = "reward_value_3")]
  public string RankRewardValue_3;
  [Config(Name = "reward_id_4")]
  public string RankReward_4;
  [Config(Name = "reward_value_4")]
  public string RankRewardValue_4;
  [Config(Name = "reward_id_5")]
  public string RankReward_5;
  [Config(Name = "reward_value_5")]
  public string RankRewardValue_5;
  [Config(Name = "reward_id_6")]
  public string RankReward_6;
  [Config(Name = "reward_value_6")]
  public string RankRewardValue_6;
}
