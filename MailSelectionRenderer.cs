﻿// Decompiled with JetBrains decompiler
// Type: MailSelectionRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MailSelectionRenderer : MonoBehaviour
{
  public UITexture m_PlayerIcon;
  public UILabel m_PlayerName;
  public UIToggle m_Toggle;
  private string m_Target;
  private List<string> m_Selection;

  public void SetData(string playerIcon, string customIcon, int lordTitleId, string playerName, string target, List<string> selection)
  {
    this.m_Target = target;
    this.m_Selection = selection;
    CustomIconLoader.Instance.requestCustomIcon(this.m_PlayerIcon, playerIcon, customIcon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_PlayerIcon, lordTitleId, 2);
    this.m_PlayerName.text = playerName;
    this.m_Toggle.Set(selection.Contains(target));
  }

  public void OnToggleChanged()
  {
    if (this.m_Toggle.value)
    {
      if (this.m_Selection.Contains(this.m_Target))
        return;
      this.m_Selection.Add(this.m_Target);
    }
    else
      this.m_Selection.Remove(this.m_Target);
  }
}
