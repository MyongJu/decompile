﻿// Decompiled with JetBrains decompiler
// Type: LegendExpUseSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LegendExpUseSlot : MonoBehaviour
{
  [SerializeField]
  private UITexture m_Icon;
  [SerializeField]
  private UILabel m_Count;
  private ItemStaticInfo m_ItemInfo;
  private int m_UseCount;
  private System.Action<ItemStaticInfo> m_Callback;

  public void OnSlotPressed()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_ItemInfo);
  }

  public void SetData(ItemStaticInfo itemInfo, int useCount, System.Action<ItemStaticInfo> callback)
  {
    this.m_ItemInfo = itemInfo;
    this.m_UseCount = useCount;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public ItemStaticInfo GetItemStaticInfo()
  {
    return this.m_ItemInfo;
  }

  public void SetUseCount(int useCount)
  {
    this.m_UseCount = useCount;
    this.UpdateUI();
  }

  public int GetUseCount()
  {
    return this.m_UseCount;
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, this.m_ItemInfo == null ? "Texture/Legend/hero_exp_potion_slot_empty" : this.m_ItemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.SetCount(this.m_UseCount);
  }

  private void SetCount(int count)
  {
    if (!((UnityEngine.Object) this.m_Count != (UnityEngine.Object) null))
      return;
    this.m_Count.text = string.Format("x {0}", (object) count);
  }
}
