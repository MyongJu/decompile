﻿// Decompiled with JetBrains decompiler
// Type: VfxBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class VfxBase : MonoBehaviour, IVfx, IRecycle
{
  private IVfxTarget _target;
  private Transform _rawTarget;
  private bool _isPlaying;
  private float _startTime;

  public long Guid { get; set; }

  public bool IsAlive
  {
    get
    {
      if ((bool) ((Object) this) && (bool) ((Object) this.gameObject))
        return this.IsPlaying;
      return false;
    }
  }

  public bool IsPlaying
  {
    get
    {
      return this._isPlaying;
    }
  }

  protected float StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  protected IVfxTarget Target
  {
    get
    {
      return this._target;
    }
  }

  protected Transform RawTarget
  {
    get
    {
      return this._rawTarget;
    }
  }

  public virtual void Play(Transform target)
  {
    this.Attach(target, Vector3.zero, Vector3.zero, Vector3.one);
    this.PlayImpl(0.0f);
  }

  public virtual void Play(IVfxTarget target)
  {
    if (target == null)
      return;
    this._target = target;
    this.Play(target.transform);
  }

  public virtual void Pause()
  {
  }

  public virtual void Resume()
  {
  }

  public virtual void Stop()
  {
    this._target = (IVfxTarget) null;
    this._rawTarget = (Transform) null;
    this.gameObject.SetActive(false);
    this._isPlaying = false;
  }

  public virtual void ResetSelf()
  {
    this.ResetParticles(this.gameObject);
  }

  protected void Delete()
  {
    this.Stop();
    Object.Destroy((Object) this.gameObject);
  }

  protected void PlayImpl(float time)
  {
    this._startTime = Time.time;
    this._isPlaying = true;
    this.gameObject.SetActive(true);
    this.PlayParticles(this.gameObject, time);
  }

  public void Attach(Transform target, Vector3 offset, Vector3 rotation, Vector3 scale)
  {
    this._rawTarget = target;
    if (!(bool) ((Object) this.gameObject) || !(bool) ((Object) this.transform))
      return;
    this.transform.parent = this._rawTarget;
    this.transform.localPosition = offset;
    this.transform.localRotation = Quaternion.Euler(rotation);
    this.transform.localScale = scale;
  }

  private void PlayParticles(GameObject obj, float time)
  {
    if ((Object) obj == (Object) null)
      return;
    ParticleSystem[] componentsInChildren = obj.GetComponentsInChildren<ParticleSystem>();
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      componentsInChildren[index].Simulate(time);
      componentsInChildren[index].Play();
    }
  }

  private void ResetParticles(GameObject obj)
  {
    if ((Object) obj == (Object) null)
      return;
    foreach (ParticleSystem componentsInChild in obj.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Clear();
  }

  public void OnInitialize()
  {
  }

  public void OnFinalize()
  {
    this.ResetSelf();
  }

  GameObject IVfx.get_gameObject()
  {
    return this.gameObject;
  }
}
