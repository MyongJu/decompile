﻿// Decompiled with JetBrains decompiler
// Type: HallOfKingdomUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public static class HallOfKingdomUtils
{
  public static void CastKingSkill(KingSkillInfo skillInfo)
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
    {
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPMap.PendingGotoRequest = PlayerData.inst.CityData.Location;
        if (!skillInfo.IsSingleCast)
          return;
        PVPSystem.Instance.Map.EnterSingleSpellMode(skillInfo);
      }));
    }
    else
    {
      if (!skillInfo.IsSingleCast)
        return;
      PVPSystem.Instance.Map.EnterSingleSpellMode(skillInfo);
    }
  }
}
