﻿// Decompiled with JetBrains decompiler
// Type: StandardToast
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class StandardToast : ToastDialog
{
  public float movetime = 0.5f;
  public float showtime = 3f;
  public float fastshowtime = 1f;
  private Vector3 oldPos = Vector3.zero;
  public UITexture icon;
  public UILabel title;
  public UILabel content;
  public UISprite bg;
  private float height;
  private Vector3 initPos;
  private Vector3 tarPos;
  private float begintime;

  public override void Init()
  {
    base.Init();
    this.height = (float) this.bg.height;
    float num = (float) (UIManager.inst.uiRoot.manualHeight / 2);
    this.initPos = new Vector3(0.0f, num + this.height, 0.0f);
    this.tarPos = new Vector3(0.0f, num + 35f, 0.0f);
    this.transform.localPosition = this.initPos;
    if (!(this.oldPos != Vector3.zero))
      return;
    this.content.transform.localPosition = this.oldPos;
  }

  public override void SeedData(object args)
  {
    ToastArgs toastArgs = args as ToastArgs;
    this.title.text = ScriptLocalization.Get(toastArgs.data.title, true);
    string withPara = ScriptLocalization.Get(toastArgs.data.content, false);
    if (toastArgs.para != null)
      withPara = ScriptLocalization.GetWithPara(toastArgs.data.content, toastArgs.para, true);
    this.content.text = withPara;
    if (this.content.height > this.content.fontSize)
      return;
    this.oldPos = this.content.transform.localPosition;
    Vector3 oldPos = this.oldPos;
    oldPos.y -= (float) (this.content.fontSize / 2);
    this.content.transform.localPosition = oldPos;
  }

  public override void Play()
  {
    this.isplaying = true;
    this.StartCoroutine(this.Animation(this.showtime));
  }

  public override void FastPlay()
  {
    this.isplaying = true;
    this.StartCoroutine(this.Animation(this.fastshowtime));
  }

  public override void Interrupted()
  {
    if ((double) (Time.time - this.begintime) >= (double) this.showtime + (double) this.movetime - (double) this.fastshowtime)
      return;
    this.StopAllCoroutines();
    this.StartCoroutine(this.InterruptedAnimation(this.fastshowtime));
  }

  [DebuggerHidden]
  private IEnumerator Animation(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StandardToast.\u003CAnimation\u003Ec__IteratorA0()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator InterruptedAnimation(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new StandardToast.\u003CInterruptedAnimation\u003Ec__IteratorA1()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }
}
