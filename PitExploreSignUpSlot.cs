﻿// Decompiled with JetBrains decompiler
// Type: PitExploreSignUpSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PitExploreSignUpSlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelApplicant;
  private string _applicant;

  public void SetData(string applicant)
  {
    this._applicant = applicant;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this._labelApplicant.text = this._applicant;
  }
}
