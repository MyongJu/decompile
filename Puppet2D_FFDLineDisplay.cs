﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_FFDLineDisplay
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_FFDLineDisplay : MonoBehaviour
{
  public List<Transform> bones = new List<Transform>();
  public List<float> weight = new List<float>();
  public List<Vector3> delta = new List<Vector3>();
  private List<float> internalWeights = new List<float>();
  [HideInInspector]
  public Transform target;
  [HideInInspector]
  public Transform target2;
  [HideInInspector]
  public SkinnedMeshRenderer skinnedMesh;
  public SkinnedMeshRenderer outputSkinnedMesh;
  [HideInInspector]
  public int vertNumber;

  public void Init()
  {
    this.bones.Clear();
    this.weight.Clear();
    this.delta.Clear();
    this.internalWeights.Clear();
    Mesh sharedMesh = this.skinnedMesh.sharedMesh;
    Vector3 vertex = sharedMesh.vertices[this.vertNumber];
    BoneWeight boneWeight = sharedMesh.boneWeights[this.vertNumber];
    int[] numArray1 = new int[4]
    {
      boneWeight.boneIndex0,
      boneWeight.boneIndex1,
      boneWeight.boneIndex2,
      boneWeight.boneIndex3
    };
    float[] numArray2 = new float[4]
    {
      boneWeight.weight0,
      boneWeight.weight1,
      boneWeight.weight2,
      boneWeight.weight3
    };
    numArray2[1] = 1f - numArray2[0];
    for (int index = 0; index < 4; ++index)
    {
      if ((double) numArray2[index] > 0.0)
      {
        this.bones.Add(this.skinnedMesh.bones[numArray1[index]]);
        this.weight.Add(numArray2[index]);
        this.internalWeights.Add(numArray2[index]);
        this.delta.Add(this.bones[this.bones.Count - 1].InverseTransformPoint(vertex));
      }
    }
  }

  private void OnValidate()
  {
    float num1 = 0.0f;
    float num2 = 1f;
    for (int index = 0; index < this.weight.Count; ++index)
    {
      if (this.internalWeights.Count > index)
      {
        if ((double) this.internalWeights[index] == (double) this.weight[index])
          num1 += this.weight[index];
        else
          num2 -= this.weight[index];
      }
    }
    for (int index = 0; index < this.weight.Count; ++index)
    {
      if (this.internalWeights.Count > index)
      {
        if ((double) this.internalWeights[index] == (double) this.weight[index])
          this.weight[index] = (double) num1 > 0.0 ? this.weight[index] / num1 * num2 : 0.0f;
        this.internalWeights[index] = this.weight[index];
      }
    }
  }

  public void Run()
  {
    if (this.bones.Count <= 0)
      return;
    Vector3 zero = Vector3.zero;
    for (int index = 0; index < this.bones.Count; ++index)
      zero += this.bones[index].TransformPoint(this.delta[index]) * this.weight[index];
    this.transform.parent.position = zero;
  }

  private void OnDrawGizmos()
  {
    if (!this.GetComponent<Renderer>().enabled)
      return;
    this.transform.GetComponent<SpriteRenderer>().color = Color.white;
    if ((Object) this.target != (Object) null)
    {
      Gizmos.color = Color.white;
      Gizmos.DrawLine(this.transform.position, this.target.position);
    }
    if (!((Object) this.target2 != (Object) null))
      return;
    Gizmos.color = Color.white;
    Gizmos.DrawLine(this.transform.position, this.target2.position);
  }
}
