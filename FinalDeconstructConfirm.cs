﻿// Decompiled with JetBrains decompiler
// Type: FinalDeconstructConfirm
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FinalDeconstructConfirm : MonoBehaviour
{
  public UILabel mTitleLabel;
  public UILabel mTimeValue;
  public UITexture mIcon;
  public UILabel mLevelValue;
  public UILabel mLosePowerMsg;
  public GameObject mDestructTimeLabel;
  public GameObject mDestructTimeValue;
  public GameObject mDestructTimeIcon;
  private System.Action _onYesBtn;

  public void SetDetails(string title, string time, Texture icon, int iconW, int iconH, string level, string powerMsg, bool bInstant, System.Action onYesBtn)
  {
    this.mTitleLabel.text = title;
    this.mTimeValue.text = time;
    this.mLevelValue.text = level;
    this.mLosePowerMsg.text = powerMsg;
    this.mIcon.mainTexture = icon;
    this.mIcon.width = iconW;
    this.mIcon.height = iconH;
    this.mDestructTimeIcon.SetActive(!bInstant);
    this.mDestructTimeValue.SetActive(!bInstant);
    this.mDestructTimeLabel.SetActive(!bInstant);
    this._onYesBtn = onYesBtn;
  }

  public void OnNoBtnPressed()
  {
    this.gameObject.SetActive(false);
  }

  public void OnYesBtnPressed()
  {
    this.gameObject.SetActive(false);
    if (this._onYesBtn == null)
      return;
    this._onYesBtn();
  }
}
