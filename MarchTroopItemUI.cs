﻿// Decompiled with JetBrains decompiler
// Type: MarchTroopItemUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class MarchTroopItemUI : MonoBehaviour
{
  private const string TEXTURE_PATH_PRE = "Texture/Unit/portrait_unit_";
  public UITexture troopIcon;
  public UILabel troopName;
  public UILabel troopCount;
  public GameObject content;

  public void SetTroopInfo(Unit_StatisticsInfo unit, int count)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.troopIcon, string.Format("{0}{1}", (object) "Texture/Unit/portrait_unit_", (object) unit.Image), (System.Action<bool>) null, true, false, string.Empty);
    this.troopName.text = ScriptLocalization.Get(string.Format("{0}{1}", (object) unit.ID, (object) "_name"), true);
    this.troopCount.text = count.ToString();
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.troopIcon);
    NGUITools.SetActive(this.content, false);
  }
}
