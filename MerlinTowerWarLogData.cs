﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarLogData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MerlinTowerWarLogData : AllianceWarManager.LogData
{
  public override bool isStarter
  {
    get
    {
      return this.owner.uid == PlayerData.inst.uid;
    }
  }

  public override bool isAllyWin
  {
    get
    {
      return this.win_id == PlayerData.inst.uid;
    }
  }

  public override AllianceWarManager.LogData.AllianceWarPlayerData enemy
  {
    get
    {
      if (this.owner.uid == PlayerData.inst.uid)
        return this.target;
      return this.owner;
    }
  }

  public override AllianceWarManager.LogData.AllianceWarPlayerData ally
  {
    get
    {
      if (this.owner.uid == PlayerData.inst.uid)
        return this.owner;
      return this.target;
    }
  }
}
