﻿// Decompiled with JetBrains decompiler
// Type: TechLevelData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class TechLevelData : IGameData
{
  private static readonly string NameKey = "_name";
  private static readonly string DescKey = nameof (_description);
  public List<int> RequiredBuildingLevels = new List<int>();
  public List<int> RequiredTechLevels = new List<int>();
  public List<KeyValuePair<CityResourceInfo.ResourceType, int>> Costs = new List<KeyValuePair<CityResourceInfo.ResourceType, int>>();
  public List<Effect> Effects = new List<Effect>();
  private int _internalID;
  private string _id;
  private int _power;
  private int _duration;
  private int _techInternalID;
  private int _techLevel;
  private string _description;
  private string _treeName;
  private string _treeDescription;
  private string _loc_research_name_id;
  private string _loc_research_description_id;

  public TechLevelData(int internalID, string ID, int power, int duration, int techInternalID, int techLevel, string loc_research_name_id, string loc_research_description_id)
  {
    this._internalID = internalID;
    this._id = ID;
    this._power = power;
    this._duration = duration;
    this._techInternalID = techInternalID;
    this._techLevel = techLevel;
    this._loc_research_name_id = loc_research_name_id;
    this._loc_research_description_id = loc_research_description_id;
  }

  public int InternalID
  {
    get
    {
      return this._internalID;
    }
  }

  public string ID
  {
    get
    {
      return this._id;
    }
  }

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this._loc_research_name_id, true);
    }
  }

  public string NameLocKey
  {
    get
    {
      return this._id + TechLevelData.NameKey;
    }
  }

  public string Description
  {
    get
    {
      return ScriptLocalization.Get(this._loc_research_description_id, true);
    }
  }

  public int Power
  {
    get
    {
      return this._power;
    }
  }

  public int TechInternalID
  {
    get
    {
      return this._techInternalID;
    }
  }

  public int TechLevel
  {
    get
    {
      return this._techLevel;
    }
  }

  public int Duration
  {
    get
    {
      return this._duration;
    }
  }
}
