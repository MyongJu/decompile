﻿// Decompiled with JetBrains decompiler
// Type: SpellRangeHighlight
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SpellRangeHighlight : MonoBehaviour
{
  public float WALK_CROSS_FACTOR = 0.5f;
  public float VIEWPORT_TOP = 1f;
  public float VIEWPORT_BOTTOM = 1f;
  public float VIEWPORT_LEFT = 1f;
  public float VIEWPORT_RIGHT = 1f;
  protected bool m_selectTilesDirty = true;
  protected string m_errorString = string.Empty;
  protected List<TileData> m_allSelectedTiles = new List<TileData>();
  private const float POSITION_TOLERANCE = 0.001f;
  private const float MOVEMENT_TOLERANCE = 0.001f;
  private const float RESPONSE_CELL_SIZE = 214f;
  private const float HALF_TILE_WIDTH = 150f;
  private const float HALF_TILE_HEIGHT = 75f;
  public GameObject GridPrefab;
  public Transform GridRoot;
  public BoxCollider2D Response;
  public UIButton CastSpell;
  public SpellRangeHighlight.OnConfirmCallback OnConfirm;
  public SpellRangeHighlight.OnCancelCallback OnCancel;
  private WorldCoordinate m_WorldLocation;
  private bool m_Dragging;
  private Vector3 m_LastCursorPosition;
  private Vector2 m_LastScreenPosition;
  private TileCamera m_TileCamera;
  private TempleSkillInfo m_TempleSkillInfo;
  protected WorldCoordinate m_PreWorldPosition;

  protected void OnEnable()
  {
    PVPMapData.MapData.OnEnterKingdomBlock += new System.Action(this.onEnterKindomBlock);
  }

  protected void OnDisable()
  {
    PVPMapData.MapData.OnEnterKingdomBlock -= new System.Action(this.onEnterKindomBlock);
  }

  protected void onEnterKindomBlock()
  {
    this.m_selectTilesDirty = true;
  }

  protected bool HaveValidTiles
  {
    get
    {
      using (List<TileData>.Enumerator enumerator = this.m_allSelectedTiles.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TileData current = enumerator.Current;
          if (current != null && current.CurrentSpellAbleState == TileData.SpellAbleState.SpellAbleTrue)
            return true;
        }
      }
      return false;
    }
  }

  public void ClearSelectedTiles()
  {
    using (List<TileData>.Enumerator enumerator = this.m_allSelectedTiles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.CurrentSpellAbleState = TileData.SpellAbleState.SpellAbleNone;
    }
    this.m_allSelectedTiles.Clear();
  }

  protected void UpdateSelectedTiles()
  {
    this.ClearSelectedTiles();
    Coordinate kxy = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
    int targetArea = this.m_TempleSkillInfo.TargetArea;
    int num1 = targetArea / 2 + 1;
    int x = kxy.X;
    int num2 = kxy.Y - num1;
    for (int index1 = x; index1 > x - targetArea; --index1)
    {
      for (int index2 = 0; index2 < targetArea; ++index2)
      {
        TileData referenceAt = PVPMapData.MapData.GetReferenceAt(kxy.K, index1 + index2, num2 + index2);
        if (referenceAt != null && !this.m_allSelectedTiles.Contains(referenceAt))
        {
          bool targetTile = Utils.CanSpellToTargetTile(this.m_TempleSkillInfo, referenceAt, ref this.m_errorString);
          referenceAt.CurrentSpellAbleState = !targetTile ? TileData.SpellAbleState.SpellAbleFalse : TileData.SpellAbleState.SpellAbleTrue;
          this.m_allSelectedTiles.Add(referenceAt);
        }
      }
      ++num2;
    }
  }

  public void InitCamera(TileCamera tileCamera)
  {
    this.m_TileCamera = tileCamera;
  }

  public void Setup(WorldCoordinate target, TempleSkillInfo skillInfo)
  {
    this.m_TempleSkillInfo = skillInfo;
    this.SetLocation(target);
    this.GenerateGrids();
  }

  private void GenerateGrids()
  {
    int targetArea = this.m_TempleSkillInfo.TargetArea;
    int num1 = targetArea / 2;
    float num2 = (float) targetArea * 214f;
    this.Response.size = new Vector2(num2, num2);
    Vector2 zero = Vector2.zero;
    zero.y -= (float) (num1 * 2) * 75f;
    for (int index1 = 0; index1 < targetArea; ++index1)
    {
      for (int index2 = 0; index2 < targetArea; ++index2)
      {
        SpellGrid.Position position = SpellGrid.Position.Inner;
        if (index1 == 0 && index2 == 0)
          position = SpellGrid.Position.BottomCorner;
        else if (index1 == targetArea - 1 && index2 == targetArea - 1)
          position = SpellGrid.Position.TopCorner;
        else if (index1 == 0 && index2 == targetArea - 1)
          position = SpellGrid.Position.LeftCorner;
        else if (index1 == targetArea - 1 && index2 == 0)
          position = SpellGrid.Position.RightCorner;
        else if (index1 == 0)
          position = SpellGrid.Position.BottomLeft;
        else if (index2 == 0)
          position = SpellGrid.Position.BottomRight;
        else if (index1 == targetArea - 1)
          position = SpellGrid.Position.TopRight;
        else if (index2 == targetArea - 1)
          position = SpellGrid.Position.TopLeft;
        Vector2 vector2 = zero;
        vector2.x -= (float) index2 * 150f;
        vector2.y += (float) index2 * 75f;
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.GridPrefab);
        gameObject.SetActive(true);
        gameObject.transform.parent = this.GridRoot;
        gameObject.transform.localPosition = new Vector3(vector2.x, vector2.y, 1f);
        gameObject.transform.localScale = Vector3.one;
        gameObject.GetComponent<SpellGrid>().setPosition(position);
      }
      zero.x += 150f;
      zero.y += 75f;
    }
  }

  public void SetLocation(WorldCoordinate target)
  {
    this.transform.localPosition = PVPMapData.MapData.ConvertWorldCoordinateToPixelPosition(target);
    this.transform.localRotation = Quaternion.identity;
    this.transform.localScale = Vector3.one;
    this.UpdateGrids();
    this.AdjustCamera();
  }

  public void OnConfirmPressed()
  {
    this.UpdateSelectedTiles();
    Coordinate kxy1 = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
    if (kxy1.K != PlayerData.inst.playerCityData.cityLocation.K)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_spell_other_kingdom_forbidden", true), (System.Action) null, 4f, true);
    else if (!this.HaveValidTiles)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_spell_area_no_valid_target", true), (System.Action) null, 4f, true);
    }
    else
    {
      Hashtable hashtable = new Hashtable();
      hashtable[(object) "x"] = (object) kxy1.X;
      hashtable[(object) "y"] = (object) kxy1.Y;
      Hashtable postData = new Hashtable();
      postData[(object) "config_id"] = (object) this.m_TempleSkillInfo.internalId;
      postData[(object) "target"] = (object) hashtable;
      ArrayList arrayList = new ArrayList();
      using (List<TileData>.Enumerator enumerator = this.m_allSelectedTiles.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TileData current = enumerator.Current;
          if (current.CurrentSpellAbleState == TileData.SpellAbleState.SpellAbleTrue)
            arrayList.Add((object) new Hashtable()
            {
              {
                (object) "x",
                (object) current.Location.X
              },
              {
                (object) "y",
                (object) current.Location.Y
              }
            });
        }
      }
      postData[(object) "all_target"] = (object) arrayList;
      MessageHub.inst.GetPortByAction("AllianceTemple:castSpell").SendRequest(postData, (System.Action<bool, object>) ((result, param) =>
      {
        if (!result || this.m_TempleSkillInfo.NeedDonation != 0)
          return;
        AudioManager.Instance.PlaySound("sfx_alliance_altar_cast_spell", false);
      }), true);
      Coordinate kxy2 = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
      if (this.OnConfirm == null)
        return;
      this.OnConfirm(kxy2);
    }
  }

  public void OnCancelButtonPressed()
  {
    if (this.OnCancel == null)
      return;
    this.OnCancel();
  }

  public bool IsCollide(WorldCoordinate location)
  {
    Vector3 worldPosition = PVPMapData.MapData.ConvertWorldCoordinateToWorldPosition(location);
    return TeleportHighlight.Box2DOverlapPoint(this.Response, new Vector2(worldPosition.x, worldPosition.y));
  }

  public void StartDragging(DragGesture gesture)
  {
    this.m_LastScreenPosition = gesture.Position;
    this.m_LastCursorPosition = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
    this.m_Dragging = this.IsCollide(PVPMapData.MapData.ConvertWorldPositionToWorldCoordinate(this.m_LastCursorPosition));
  }

  public void UpdateDragging(DragGesture gesture)
  {
    if (!this.m_Dragging)
      return;
    Vector3 worldHitPoint = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
    if ((double) Vector3.Distance((Vector3) this.m_LastScreenPosition, (Vector3) gesture.Position) <= 1.0 / 1000.0)
      return;
    this.m_LastScreenPosition = gesture.Position;
    Vector3 vector3 = worldHitPoint - this.m_LastCursorPosition;
    this.m_LastCursorPosition = worldHitPoint;
    this.transform.position = this.transform.position + (!this.IsWalkCross ? 1f : this.WALK_CROSS_FACTOR) * vector3;
    this.ClampPosition();
    this.UpdateGrids();
    this.AdjustCamera();
  }

  public void StopDragging()
  {
    if (this.m_Dragging)
      this.SetLocation(PVPMapData.MapData.ConvertWorldPositionToWorldCoordinate(this.transform.position));
    this.m_Dragging = false;
  }

  private void AdjustCamera()
  {
    Rect viewport = this.GetViewport();
    Rect range = this.CalculateRange();
    Vector3 zero = Vector3.zero;
    float x1 = viewport.min.x;
    float x2 = viewport.max.x;
    float y1 = viewport.min.y;
    float y2 = viewport.max.y;
    if ((double) range.min.x < (double) x1)
      zero.x = range.min.x - x1;
    if ((double) range.max.x > (double) x2)
      zero.x = range.max.x - x2;
    if ((double) range.min.y < (double) y1)
      zero.y = range.min.y - y1;
    if ((double) range.max.y > (double) y2)
      zero.y = range.max.y - y2;
    this.m_TileCamera.Translate(zero);
  }

  private bool IsWalkCross
  {
    get
    {
      Rect viewport = this.GetViewport();
      Rect range = this.CalculateRange();
      float x1 = viewport.min.x;
      float x2 = viewport.max.x;
      float y1 = viewport.min.y;
      float y2 = viewport.max.y;
      if ((double) range.min.x >= (double) x1 + 1.0 / 1000.0 && (double) range.max.x <= (double) x2 - 1.0 / 1000.0 && (double) range.min.y >= (double) y1 + 1.0 / 1000.0)
        return (double) range.max.y > (double) y2 - 1.0 / 1000.0;
      return true;
    }
  }

  private Rect GetViewport()
  {
    Rect rect = new Rect();
    if ((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null)
    {
      Rect viewport = this.m_TileCamera.GetViewport();
      float x1 = viewport.min.x + this.VIEWPORT_LEFT;
      float x2 = viewport.max.x - this.VIEWPORT_RIGHT;
      float y1 = viewport.min.y + this.VIEWPORT_BOTTOM;
      float y2 = viewport.max.y - this.VIEWPORT_TOP;
      rect.min = new Vector2(x1, y1);
      rect.max = new Vector2(x2, y2);
    }
    return rect;
  }

  private void Update()
  {
    this.UpdateGrids();
  }

  private void UpdateGrids()
  {
    this.m_WorldLocation = PVPMapData.MapData.ConvertPixelPositionToWorldCoordinate((Vector2) this.transform.localPosition);
    if (!this.m_selectTilesDirty && this.m_PreWorldPosition.Equals(this.m_WorldLocation))
      return;
    this.m_selectTilesDirty = false;
    this.m_PreWorldPosition = this.m_WorldLocation;
    this.UpdateSelectedTiles();
  }

  private Rect CalculateRange()
  {
    return new Rect()
    {
      min = (Vector2) this.Response.bounds.min,
      max = (Vector2) this.Response.bounds.max
    };
  }

  private Rect GetConstraint()
  {
    float num = 0.01f;
    if ((bool) ((UnityEngine.Object) GameEngine.Instance))
      num = GameEngine.Instance.tileMapScale;
    Coordinate location = PlayerData.inst.CityData.Location;
    location.X = 1;
    location.Y = 1;
    Vector3 worldPosition1 = PVPMapData.MapData.ConvertTileKXYToWorldPosition(location);
    location.X = 1279;
    location.Y = 2559;
    Vector3 worldPosition2 = PVPMapData.MapData.ConvertTileKXYToWorldPosition(location);
    return new Rect()
    {
      xMin = worldPosition1.x,
      xMax = worldPosition2.x,
      yMin = worldPosition2.y,
      yMax = worldPosition1.y
    };
  }

  private void ClampPosition()
  {
    Rect constraint = this.GetConstraint();
    Vector3 position = this.transform.position;
    position.x = Mathf.Clamp(position.x, constraint.min.x, constraint.max.x);
    position.y = Mathf.Clamp(position.y, constraint.min.y, constraint.max.y);
    this.transform.position = position;
  }

  public delegate void OnConfirmCallback(Coordinate target);

  public delegate void OnCancelCallback();
}
