﻿// Decompiled with JetBrains decompiler
// Type: MazeMinimapItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MazeMinimapItem : MonoBehaviour
{
  private const float ANIM_DURATION = 0.3f;
  [SerializeField]
  protected MinimapRoomItem _templateRoomItem;
  [SerializeField]
  protected UIGrid _roomItemContainer;
  [SerializeField]
  protected GameObject _tagCurrentPosition;
  [SerializeField]
  protected GameObject _enterEffectTemplate;
  [SerializeField]
  protected GameObject _exitEffectTemplate;
  protected MinimapRoomItem[,] _allRoomItem;
  protected Room _currentRoom;
  protected System.Action<Room> _onMoveIntoRoomCallback;
  protected GameObject _enterEffect;
  protected GameObject _exitEffect;

  public void Awake()
  {
    this._enterEffectTemplate.SetActive(false);
    this._exitEffectTemplate.SetActive(false);
  }

  protected void DestoryFastMoveEffect()
  {
    if ((bool) ((UnityEngine.Object) this._enterEffect))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._enterEffect);
      this._enterEffect = (GameObject) null;
    }
    if (!(bool) ((UnityEngine.Object) this._exitEffect))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._exitEffect);
    this._exitEffect = (GameObject) null;
  }

  public void SetEnabled(bool enabled)
  {
    this.gameObject.SetActive(enabled);
    if (enabled)
      return;
    this.DestoryFastMoveEffect();
  }

  public void FastMoveTo(Room to, System.Action<Room> callback)
  {
    this.StartCoroutine(this.PlayFastMoveToCoroutine(this._currentRoom, to, callback));
  }

  [DebuggerHidden]
  private IEnumerator PlayFastMoveToCoroutine(Room from, Room to, System.Action<Room> callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MazeMinimapItem.\u003CPlayFastMoveToCoroutine\u003Ec__Iterator57()
    {
      callback = callback,
      from = from,
      to = to,
      \u003C\u0024\u003Ecallback = callback,
      \u003C\u0024\u003Efrom = from,
      \u003C\u0024\u003Eto = to,
      \u003C\u003Ef__this = this
    };
  }

  public void MoveIntoRoom(Room room, System.Action<Room> callback)
  {
    this._onMoveIntoRoomCallback = callback;
    if (room == null)
      return;
    this._currentRoom = room;
    iTween.MoveTo(this._tagCurrentPosition, new Hashtable()
    {
      {
        (object) "position",
        (object) this._allRoomItem[this._currentRoom.RoomX, this._currentRoom.RoomY].transform.position
      },
      {
        (object) "time",
        (object) 0.3f
      },
      {
        (object) "easetype",
        (object) "linear"
      },
      {
        (object) "oncompletetarget",
        (object) this.gameObject
      },
      {
        (object) "oncomplete",
        (object) "OnMoveIntoRoomCompleted"
      }
    });
  }

  public void UpdateRoom(Room room)
  {
    MinimapRoomItem minimapRoomItem = this._allRoomItem[room.RoomX, room.RoomY];
    if (this._currentRoom == null || this._currentRoom.Floor != room.Floor)
    {
      this._tagCurrentPosition.transform.position = minimapRoomItem.transform.position;
      this._currentRoom = room;
    }
    minimapRoomItem.SetRoom(room);
  }

  public void SetTagPosition(Room room)
  {
    this._tagCurrentPosition.transform.position = this._allRoomItem[room.RoomX, room.RoomY].transform.position;
  }

  protected void OnMoveIntoRoomCompleted()
  {
    this.NotifyMoveIntoRoom(this._currentRoom);
  }

  protected void NotifyMoveIntoRoom(Room room)
  {
    if (this._onMoveIntoRoomCallback == null)
      return;
    this._onMoveIntoRoomCallback(room);
  }

  public void UpdateCurrentFloor()
  {
    this._templateRoomItem.gameObject.SetActive(false);
    if (this._allRoomItem == null)
      this.CreateAllRoomItem();
    int currentFloor = DragonKnightSystem.Instance.RoomManager.CurrentFloor;
    for (int roomX = 0; roomX < 4; ++roomX)
    {
      for (int roomY = 0; roomY < 4; ++roomY)
      {
        Room roomByPosition = DragonKnightSystem.Instance.RoomManager.GetRoomByPosition(currentFloor, roomX, roomY);
        this._allRoomItem[roomX, roomY].SetRoom(roomByPosition);
      }
    }
  }

  protected void CreateAllRoomItem()
  {
    this._allRoomItem = new MinimapRoomItem[4, 4];
    for (int y = 3; y >= 0; --y)
    {
      for (int x = 0; x < 4; ++x)
        this._allRoomItem[x, y] = this.CreateRoomItem(x, y);
    }
    this._roomItemContainer.repositionNow = true;
    this._roomItemContainer.Reposition();
  }

  protected MinimapRoomItem CreateRoomItem(int x, int y)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._templateRoomItem.gameObject);
    gameObject.transform.SetParent(this._roomItemContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    gameObject.name = string.Format("Room{0}_{1}", (object) x, (object) y);
    return gameObject.GetComponent<MinimapRoomItem>();
  }
}
