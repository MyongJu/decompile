﻿// Decompiled with JetBrains decompiler
// Type: ConfigLordTitleMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLordTitleMain
{
  private List<LordTitleMainInfo> _lordTitleMainInfoList = new List<LordTitleMainInfo>();
  private Dictionary<string, LordTitleMainInfo> _datas;
  private Dictionary<int, LordTitleMainInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LordTitleMainInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LordTitleMainInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._lordTitleMainInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._lordTitleMainInfoList == null)
      return;
    this._lordTitleMainInfoList.Clear();
  }

  public List<LordTitleMainInfo> GetInfoList()
  {
    return this._lordTitleMainInfoList;
  }

  public List<LordTitleMainInfo> GetInfoListBySortId(int sortId)
  {
    return this._lordTitleMainInfoList.FindAll((Predicate<LordTitleMainInfo>) (x => x.sortId == sortId));
  }

  public LordTitleMainInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LordTitleMainInfo) null;
  }

  public LordTitleMainInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LordTitleMainInfo) null;
  }
}
