﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightUserPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonKnightUserPanel : MonoBehaviour
{
  public UITexture m_UserPortrait;
  public UILabel m_UserName;

  public void SetData(UserData userData)
  {
    if (userData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.m_UserPortrait, userData.PortraitIconPath, userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_UserPortrait, userData.LordTitle, 2);
    this.m_UserName.text = userData.userName_Alliance_Name;
  }
}
