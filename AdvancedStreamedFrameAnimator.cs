﻿// Decompiled with JetBrains decompiler
// Type: AdvancedStreamedFrameAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AdvancedStreamedFrameAnimator : MonoBehaviour
{
  public string mFramePrefix = string.Empty;
  public int mZeroPadding = 3;
  public int mLoadStartFrame = 1;
  public int mLoadEndFrame = 3;
  public string mAnimationList = string.Empty;
  public float mClipDuration = 1f;
  public bool mLoop = true;
  private int[] _expandedFrames = new int[(int) byte.MaxValue];
  private float _clipFPS = 0.03333334f;
  private List<Texture2D> _resourceImages = new List<Texture2D>();
  private List<string> _spriteNames = new List<string>();
  public bool isUITexture;
  public UITexture mTargetTexture;
  public UISprite mTargetSprite;
  private float _currentFrameIndex;
  private float _waitDelay;
  private int _currentFrame;
  private int _expandedFrameCount;

  public int ExpandedFrameCount
  {
    get
    {
      return this._expandedFrameCount;
    }
    set
    {
      this._expandedFrameCount = value;
    }
  }

  private void Start()
  {
    this.SubmitFrame(ref this._expandedFrames);
    int num = 0;
    while (++num <= 32)
    {
      this._currentFrameIndex = (float) Utils.RndRange(0, this._expandedFrameCount);
      if (this._expandedFrames[(int) this._currentFrameIndex] >= 0 && ((double) this._currentFrameIndex <= 0.0 || this._expandedFrames[(int) this._currentFrameIndex - 1] >= 0) && ((double) this._currentFrameIndex <= 1.0 || this._expandedFrames[(int) this._currentFrameIndex - 2] >= 0))
        goto label_4;
    }
    D.error((object) "Invalid animation data in gameObject {0}", (object) this.gameObject.name);
label_4:
    for (int mLoadStartFrame = this.mLoadStartFrame; mLoadStartFrame <= this.mLoadEndFrame; ++mLoadStartFrame)
    {
      if (this.isUITexture)
        this._resourceImages.Add(AssetManager.Instance.HandyLoad(this.mFramePrefix + Utils.ZeroPad(mLoadStartFrame, this.mZeroPadding), (System.Type) null) as Texture2D);
      else
        this._spriteNames.Add(this.mFramePrefix + Utils.ZeroPad(mLoadStartFrame, this.mZeroPadding));
    }
    this._clipFPS = this.mClipDuration / (float) this._expandedFrameCount;
  }

  public void ResetAnimation()
  {
    this._currentFrameIndex = 0.0f;
  }

  private void Update()
  {
    if ((UnityEngine.Object) this.mTargetSprite == (UnityEngine.Object) null || (double) (this._waitDelay -= Time.deltaTime) > 0.0)
      return;
    this._currentFrameIndex += (float) ((double) Time.deltaTime * (double) this._clipFPS * 90.0);
    if (this.mLoop)
    {
      if ((double) this._currentFrameIndex >= (double) this._expandedFrameCount)
        this._currentFrameIndex = 0.0f;
    }
    else if ((double) this._currentFrameIndex >= (double) this._expandedFrameCount)
      this._currentFrameIndex = (float) (this._expandedFrameCount - 1);
    this._currentFrame = (int) this._currentFrameIndex;
    if (this._currentFrame < 0)
      this._currentFrame = 0;
    if (this._currentFrame >= this._expandedFrames.Length)
      this._currentFrame = this._expandedFrames.Length - 1;
    int expandedFrame = this._expandedFrames[this._currentFrame];
    switch (expandedFrame)
    {
      case -2:
        this._waitDelay = Utils.RndRange((float) this._expandedFrames[++this._currentFrame], (float) this._expandedFrames[++this._currentFrame]);
        this._waitDelay /= 30f;
        this._currentFrameIndex += 3f;
        break;
      case -1:
        if (this.isUITexture)
        {
          this.mTargetTexture.gameObject.SetActive(false);
          break;
        }
        this.mTargetSprite.gameObject.SetActive(false);
        break;
      default:
        if (this.isUITexture)
        {
          this.mTargetTexture.gameObject.SetActive(true);
          if (expandedFrame >= this._resourceImages.Count)
            break;
          this.mTargetSprite.mainTexture = (Texture) this._resourceImages[expandedFrame];
          break;
        }
        this.mTargetSprite.gameObject.SetActive(true);
        if (expandedFrame >= this._spriteNames.Count)
          break;
        this.mTargetSprite.spriteName = this._spriteNames[expandedFrame];
        break;
    }
  }

  public void SubmitFrame(ref int[] frameArray)
  {
    string[] strArray1 = this.mAnimationList.Replace(" ", string.Empty).Split(',');
    for (int index = 0; index < strArray1.Length; ++index)
    {
      if (strArray1[index].IndexOf("-") > -1)
      {
        string[] strArray2 = strArray1[index].Split('-');
        int num1 = int.Parse(strArray2[0]);
        int num2 = int.Parse(strArray2[1]);
        if (num1 < num2)
        {
          while (num1 <= num2)
            frameArray[this._expandedFrameCount++] = num1++;
        }
        else
        {
          while (num1 >= num2)
            frameArray[this._expandedFrameCount++] = num1--;
        }
      }
      else if (strArray1[index].IndexOf(":") > -1)
      {
        string[] strArray2 = strArray1[index].Split(':');
        int num1 = int.Parse(strArray2[0]);
        int num2 = int.Parse(strArray2[1]);
        frameArray[this._expandedFrameCount++] = -2;
        frameArray[this._expandedFrameCount++] = num1;
        frameArray[this._expandedFrameCount++] = num2;
      }
      else
        frameArray[this._expandedFrameCount++] = !(strArray1[index] == "*") ? int.Parse(strArray1[index]) : -1;
    }
  }
}
