﻿// Decompiled with JetBrains decompiler
// Type: BarracksResourcesComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class BarracksResourcesComponent : MonoBehaviour
{
  public UILabel wood;
  public UILabel silver;
  public UILabel food;
  public UILabel ore;
  private CityData cityData;

  public void UpdateOwnedResources()
  {
    if (this.cityData == null)
      this.cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    long currentResource1 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource2 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    long currentResource3 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource4 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    string str1 = Utils.FormatShortThousandsLong(currentResource1);
    string str2 = Utils.FormatShortThousandsLong(currentResource2);
    string str3 = Utils.FormatShortThousandsLong(currentResource3);
    string str4 = Utils.FormatShortThousandsLong(currentResource4);
    this.wood.text = str1;
    this.silver.text = str2;
    this.food.text = str3;
    this.ore.text = str4;
  }

  public void UpdateCostResources(float wood, float silver, float food, float ore, bool updateColor = true)
  {
    if (this.cityData == null)
      this.cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    long currentResource1 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource2 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    long currentResource3 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource4 = (long) this.cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    this.wood.text = this.BuildLabelString((long) wood, currentResource1);
    this.silver.text = this.BuildLabelString((long) silver, currentResource2);
    this.food.text = this.BuildLabelString((long) food, currentResource3);
    this.ore.text = this.BuildLabelString((long) ore, currentResource4);
  }

  private string BuildLabelString(long cost, long own)
  {
    if (cost > own)
      return "[FF0000]" + Utils.FormatShortThousandsLong(own) + "[-]/" + Utils.FormatShortThousands((int) cost);
    return Utils.FormatShortThousandsLong(own) + "/" + Utils.FormatShortThousands((int) cost);
  }
}
