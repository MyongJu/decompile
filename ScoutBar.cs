﻿// Decompiled with JetBrains decompiler
// Type: ScoutBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class ScoutBar : MonoBehaviour
{
  public UILabel name;
  public UITexture icon;
  public UILabel count;
  public UISprite decoration;

  public void SeedData(ScoutBar.Data d)
  {
    this.name.text = ScriptLocalization.Get(d.name, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, false, string.Empty);
    this.count.text = d.count;
    if ("0" == d.count)
    {
      this.count.gameObject.SetActive(false);
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this.decoration))
        return;
      this.decoration.gameObject.SetActive(false);
    }
    else
    {
      this.count.gameObject.SetActive(true);
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this.decoration))
        return;
      this.decoration.gameObject.SetActive(true);
    }
  }

  public class Data
  {
    public string name;
    public string icon;
    public string count;
  }
}
