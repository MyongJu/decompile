﻿// Decompiled with JetBrains decompiler
// Type: CoordinateComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

[RequireComponent(typeof (UILabel))]
public class CoordinateComponent : MonoBehaviour
{
  private void OnClick()
  {
    string[] strArray = this.GetComponent<UILabel>().text.Split(':');
    if (strArray.Length <= 1)
      return;
    int result1 = 0;
    int.TryParse(strArray[0], out result1);
    int result2 = 0;
    int.TryParse(strArray[1], out result2);
    this.GoToTargetPlace(PlayerData.inst.CityData.Location.K, result1, result2);
  }

  protected void GoToTargetPlace(int k, int x, int y)
  {
    if (!MapUtils.IsPitWorld(k) && (x < 0 || x >= (int) PVPMapData.MapData.TilesPerKingdom.x || (y < 0 || y >= (int) PVPMapData.MapData.TilesPerKingdom.y)))
      UIManager.inst.toast.Show(Utils.XLAT("toast_chat_share_coordinates_invalid"), (System.Action) null, 4f, false);
    else if (MapUtils.IsPitWorld(k) && (x < 0 || x >= (int) PitMapData.MapData.TilesPerKingdom.x || (y < 0 || y >= (int) PitMapData.MapData.TilesPerKingdom.y)))
      UIManager.inst.toast.Show(Utils.XLAT("toast_chat_share_coordinates_invalid"), (System.Action) null, 4f, false);
    else if (!MapUtils.CanGotoTarget(k))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_kingdom_target_different_kingdom"), (System.Action) null, 4f, true);
    }
    else
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
        UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
        {
          GameEngine.Instance.CurrentGameMode = !MapUtils.IsPitWorld(k) ? GameEngine.GameMode.PVPMode : GameEngine.GameMode.PitMode;
          PVPSystem.Instance.Map.GotoLocation(new Coordinate(k, x, y), false);
        }));
      else
        PVPSystem.Instance.Map.GotoLocation(new Coordinate(k, x, y), false);
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }
}
