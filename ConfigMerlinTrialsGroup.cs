﻿// Decompiled with JetBrains decompiler
// Type: ConfigMerlinTrialsGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMerlinTrialsGroup
{
  private List<MerlinTrialsGroupInfo> _merlinTrialsGroupInfoList = new List<MerlinTrialsGroupInfo>();
  private Dictionary<string, MerlinTrialsGroupInfo> _datas;
  private Dictionary<int, MerlinTrialsGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<MerlinTrialsGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, MerlinTrialsGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._merlinTrialsGroupInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._merlinTrialsGroupInfoList == null)
      return;
    this._merlinTrialsGroupInfoList.Clear();
  }

  public List<MerlinTrialsGroupInfo> GetInfoList()
  {
    return this._merlinTrialsGroupInfoList;
  }

  public MerlinTrialsGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (MerlinTrialsGroupInfo) null;
  }

  public MerlinTrialsGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (MerlinTrialsGroupInfo) null;
  }

  public MerlinTrialsGroupInfo GetByDay(int day)
  {
    int index1 = 0;
    for (int count1 = this._merlinTrialsGroupInfoList.Count; index1 < count1; ++index1)
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = this._merlinTrialsGroupInfoList[index1];
      List<int> openDays = merlinTrialsGroupInfo.OpenDays;
      int index2 = 0;
      for (int count2 = openDays.Count; index2 < count2; ++index2)
      {
        if (day == openDays[index2])
          return merlinTrialsGroupInfo;
      }
    }
    return (MerlinTrialsGroupInfo) null;
  }

  public int Day2Group(int day)
  {
    int index1 = 0;
    for (int count1 = this._merlinTrialsGroupInfoList.Count; index1 < count1; ++index1)
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = this._merlinTrialsGroupInfoList[index1];
      List<int> openDays = merlinTrialsGroupInfo.OpenDays;
      int index2 = 0;
      for (int count2 = openDays.Count; index2 < count2; ++index2)
      {
        if (day == openDays[index2])
          return merlinTrialsGroupInfo.internalId;
      }
    }
    D.error((object) "MerlinTrials, day {0} cannot convert to group id", (object) day);
    return 0;
  }

  public int Group2Day(int groupId)
  {
    MerlinTrialsGroupInfo merlinTrialsGroupInfo = this.Get(groupId);
    if (merlinTrialsGroupInfo == null)
    {
      D.error((object) "MerlinTrials, groupId {0} cannot convert to day", (object) groupId);
      return -1;
    }
    if (merlinTrialsGroupInfo.OpenDays != null && merlinTrialsGroupInfo.OpenDays.Count != 0)
      return merlinTrialsGroupInfo.OpenDays[0];
    D.error((object) "MerlinTrials, groupId {0} has no days info", (object) groupId);
    return -1;
  }
}
