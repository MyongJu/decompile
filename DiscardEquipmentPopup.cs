﻿// Decompiled with JetBrains decompiler
// Type: DiscardEquipmentPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class DiscardEquipmentPopup : PopupBase
{
  public UITexture m_ItemIcon;
  private InventoryItemData m_ItemData;
  private System.Action<InventoryItemData> m_OnYes;

  public void Initialize(InventoryItemData itemData, System.Action<InventoryItemData> callback)
  {
    this.m_ItemData = itemData;
    string empty = string.Empty;
    this.m_OnYes = callback;
  }

  public void OnCloseClick()
  {
    this.Close();
  }

  public void OnYes()
  {
    this.Close();
    if (this.m_OnYes == null)
      return;
    this.m_OnYes(this.m_ItemData);
  }

  public void OnNo()
  {
    this.Close();
  }
}
