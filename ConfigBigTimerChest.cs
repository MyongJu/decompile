﻿// Decompiled with JetBrains decompiler
// Type: ConfigBigTimerChest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigBigTimerChest
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, BigTimerChestInfo> datas;
  private Dictionary<int, BigTimerChestInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<BigTimerChestInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public BigTimerChestInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (BigTimerChestInfo) null;
  }

  public BigTimerChestInfo GetData(string ID)
  {
    if (this.datas.ContainsKey(ID))
      return this.datas[ID];
    return (BigTimerChestInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, BigTimerChestInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, BigTimerChestInfo>) null;
  }
}
