﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestRewardRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GroupQuestRewardRender : MonoBehaviour
{
  public Icon icon;

  public void UpdateUI(QuestReward reward)
  {
    ConsumableReward consumableReward = reward as ConsumableReward;
    this.icon.FeedData((IComponentData) new IconData(consumableReward.GetRewardIconName(), new string[2]
    {
      consumableReward.GetRewardTypeName(),
      consumableReward.GetValueText()
    }));
  }
}
