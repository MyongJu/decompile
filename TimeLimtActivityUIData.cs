﻿// Decompiled with JetBrains decompiler
// Type: TimeLimtActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class TimeLimtActivityUIData : RoundActivityUIData
{
  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.TimeLimit;
    }
  }

  public override int ActivityMainConfigID
  {
    get
    {
      return ConfigManager.inst.DB_ActivityMainSub.GetData(this.CurrentRound.ActivitySubConfigID).ActivityMainID;
    }
  }

  public override string GetNormalDesc()
  {
    if (!this.IsStart())
      return base.GetNormalDesc();
    return ConfigManager.inst.DB_ActivityMainSub.GetData(this.CurrentRound.ActivitySubConfigID).LocName;
  }

  public override void DisplayActivityDetail()
  {
    RoundActivityData data = this.Data as RoundActivityData;
    if (!this.IsStart())
      UIManager.inst.OpenPopup("Activity/ActivityNotStartedPopup", (Popup.PopupParameter) new ActivityNotStartPopup.Parameter()
      {
        baseData = (ActivityBaseUIData) this
      });
    else if (data.IsOpenRanking)
      UIManager.inst.OpenDlg("Activity/ActivityDetailDlg", (UI.Dialog.DialogParameter) new ActivityDetailDlg.Parameter()
      {
        data = data
      }, 1 != 0, 1 != 0, 1 != 0);
    else
      UIManager.inst.OpenDlg("Activity/ActivityDetailDlgWithoutRank", (UI.Dialog.DialogParameter) new ActivityDetailDlgWithoutRank.Parameter()
      {
        data = data
      }, 1 != 0, 1 != 0, 1 != 0);
  }
}
