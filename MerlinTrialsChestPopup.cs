﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsChestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class MerlinTrialsChestPopup : MerlinTrialsRewardBasePopup
{
  [SerializeField]
  private UIButton _buttonCollect;
  [SerializeField]
  private GameObject _includeButtonFrame;
  [SerializeField]
  private GameObject _excludeButtonFrame;
  private MerlinTrialsMainInfo _merlinMainInfo;
  private MerlinChestStatus _chestStatus;
  private System.Action<bool> _onChestCollected;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    MerlinTrialsChestPopup.Parameter parameter = orgParam as MerlinTrialsChestPopup.Parameter;
    if (parameter == null)
      return;
    this._merlinMainInfo = parameter.merlinMainInfo;
    this._chestStatus = parameter.chestStatus;
    this._onChestCollected = parameter.onChestCollected;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCollectPressed()
  {
    if (this._merlinMainInfo == null)
      return;
    if (this._chestStatus == MerlinChestStatus.claimed)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_rewards_collect_success"), (System.Action) null, 4f, true);
    }
    else
    {
      if (this._chestStatus != MerlinChestStatus.can_claim)
        return;
      MessageHub.inst.GetPortByAction("Merlin:claimBattleReward").SendRequest(new Hashtable()
      {
        {
          (object) "battleId",
          (object) this._merlinMainInfo.internalId
        }
      }, (System.Action<bool, object>) ((ret, data) =>
      {
        if (ret)
        {
          this.ShowCollectEffect();
          this.OnClosePressed();
        }
        MerlinTrialsHelper.inst.UpdateChestData(this._merlinMainInfo.trialId, data as Hashtable);
        if (this._onChestCollected == null)
          return;
        this._onChestCollected(ret);
      }), true);
    }
  }

  protected override int GetRewardId()
  {
    if (this._merlinMainInfo != null)
      return this._merlinMainInfo.firstRewardId;
    return 0;
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this._buttonCollect.gameObject, this._chestStatus != MerlinChestStatus.not_ready);
    NGUITools.SetActive(this._includeButtonFrame, this._chestStatus != MerlinChestStatus.not_ready);
    NGUITools.SetActive(this._excludeButtonFrame, this._chestStatus == MerlinChestStatus.not_ready);
  }

  public class Parameter : Popup.PopupParameter
  {
    public MerlinTrialsMainInfo merlinMainInfo;
    public MerlinChestStatus chestStatus;
    public System.Action<bool> onChestCollected;
  }
}
