﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillIcon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonSkillIcon : MonoBehaviour
{
  [SerializeField]
  private GameObject _rootUnlocked;
  [SerializeField]
  private GameObject _rootStarLevel;
  [SerializeField]
  private UITexture _icon;
  [SerializeField]
  private UILabel _level;
  [SerializeField]
  private UILabel _starLevel;

  public void SetSkillId(int skillId)
  {
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skillId);
    if (skillMainInfo == null)
    {
      D.error((object) ("can not find skill info for skill id: " + (object) skillId));
    }
    else
    {
      ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
      if (dragonSkillGroupInfo == null)
      {
        D.error((object) ("can not find skill group info for skill id: " + (object) skillId));
      }
      else
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) this._icon, dragonSkillGroupInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
        if (ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(dragonSkillGroupInfo.internalId))
        {
          this._rootUnlocked.SetActive(true);
          this._level.text = skillMainInfo.level.ToString();
          DragonData dragonData = PlayerData.inst.dragonData;
          if (dragonData == null)
          {
            D.error((object) "no dragon data");
          }
          else
          {
            int skillGroupStarLevel = dragonData.GetSkillGroupStarLevel(dragonSkillGroupInfo.internalId);
            this._starLevel.text = skillGroupStarLevel.ToString();
            this._rootStarLevel.SetActive(skillGroupStarLevel > 0);
            GreyUtility.Normal(this._icon.gameObject);
          }
        }
        else
        {
          this._rootUnlocked.SetActive(false);
          GreyUtility.Grey(this._icon.gameObject);
        }
      }
    }
  }
}
