﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsDetailPopup : Popup
{
  private readonly GameObjectPool _pool = new GameObjectPool();
  private List<GameObject> _items = new List<GameObject>();
  private readonly Dictionary<int, Dictionary<string, double>> _benefitsMap = new Dictionary<int, Dictionary<string, double>>();
  public UILabel _trialName;
  public UILabel _day;
  public UILabel _tip;
  public UITexture _texture;
  public UISprite _backgroundColor;
  public GameObject _scrollRoot;
  public UITable _table;
  public GameObject _template;
  public GameObject _tableTitle;
  public GameObject _noBenefits;
  public GameObject _larrow;
  public GameObject _rarrow;
  private SimpleVfx _vfx;
  private int mCurrentIndex;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this._template.SetActive(false);
    this._tableTitle.SetActive(false);
    this._pool.Initialize(this._template, this._table.gameObject);
    this.mCurrentIndex = (orgParam as MerlinTrialsDetailPopup.Parameter).index;
    MerlinTrialsHelper.inst.RequestBenefits(this.mCurrentIndex, new System.Action<object>(this.OnRequestCallback));
  }

  private void OnRequestCallback(object data)
  {
    this.ParseData(data as Hashtable);
    this.UpdatePage(this.mCurrentIndex);
  }

  private void ParseData(Hashtable ht)
  {
    if (ht.ContainsKey((object) "dragon"))
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_dragon");
      this.ParseDataSingle(ht[(object) "dragon"] as Hashtable, merlinTrialsGroupInfo.internalId);
    }
    if (ht.ContainsKey((object) "equipment"))
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_equipment");
      this.ParseDataSingle(ht[(object) "equipment"] as Hashtable, merlinTrialsGroupInfo.internalId);
    }
    if (ht.ContainsKey((object) "gemstone"))
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_gemstone");
      this.ParseDataSingle(ht[(object) "gemstone"] as Hashtable, merlinTrialsGroupInfo.internalId);
    }
    if (ht.ContainsKey((object) "technology"))
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_technology");
      this.ParseDataSingle(ht[(object) "technology"] as Hashtable, merlinTrialsGroupInfo.internalId);
    }
    if (ht.ContainsKey((object) "construction"))
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_construction");
      this.ParseDataSingle(ht[(object) "construction"] as Hashtable, merlinTrialsGroupInfo.internalId);
    }
    if (!ht.ContainsKey((object) "equip_gem"))
      return;
    MerlinTrialsGroupInfo merlinTrialsGroupInfo1 = ConfigManager.inst.DB_MerlinTrialsGroup.Get("merlin_trial_equip_gem");
    this.ParseDataSingle(ht[(object) "equip_gem"] as Hashtable, merlinTrialsGroupInfo1.internalId);
  }

  private void ParseDataSingle(Hashtable ht, int groupId)
  {
    if (this._benefitsMap.ContainsKey(groupId))
      this._benefitsMap[groupId].Clear();
    else
      this._benefitsMap.Add(groupId, new Dictionary<string, double>());
    if (ht == null)
      return;
    foreach (object key1 in (IEnumerable) ht.Keys)
    {
      string key2 = key1 as string;
      double result;
      if (!key2.Equals(string.Empty) && double.TryParse(ht[(object) key2] as string, out result) && Math.Abs(result) > double.Epsilon)
      {
        string idByShortKey = ConfigManager.inst.DB_Properties.GetIDByShortKey(key2);
        if (!idByShortKey.Equals(string.Empty))
          this._benefitsMap[groupId].Add(idByShortKey, result);
      }
    }
  }

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this._pool.Clear();
    if (!((UnityEngine.Object) this._vfx != (UnityEngine.Object) null))
      return;
    VfxManager.Instance.Delete((IVfx) this._vfx);
    this._vfx = (SimpleVfx) null;
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnLeftClick()
  {
    this.MoveToPrevious();
  }

  public void OnRightClick()
  {
    this.MoveToNext();
  }

  private void TryMoveToPage(int index)
  {
    if (!this._benefitsMap.ContainsKey(ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(index).internalId))
      MerlinTrialsHelper.inst.RequestBenefits(index, new System.Action<object>(this.OnRequestCallback));
    else
      this.UpdatePage(index);
  }

  private void UpdatePage(int index)
  {
    this._rarrow.SetActive(index != 6);
    this._larrow.SetActive(index != 0);
    this.RecycleItems();
    this.UpdateUI();
  }

  private void MoveToNext()
  {
    if (this.mCurrentIndex >= 6)
      return;
    this.TryMoveToPage(++this.mCurrentIndex);
  }

  private void MoveToPrevious()
  {
    if (this.mCurrentIndex <= 0)
      return;
    this.TryMoveToPage(--this.mCurrentIndex);
  }

  private void UpdateUI()
  {
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this.mCurrentIndex);
    if (byDay == null)
      return;
    this._trialName.text = byDay.LocName;
    this._day.text = MerlinTrialsHelper.inst.GetDayString(this.mCurrentIndex);
    this._tip.text = this.mCurrentIndex != MerlinTrialsHelper.inst.Day ? Utils.XLAT("trial_inactive_status") : Utils.XLAT("trial_active_status");
    BuilderFactory.Instance.HandyBuild((UIWidget) this._texture, byDay.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Color color;
    ColorUtility.TryParseHtmlString(byDay.backgroundColor, out color);
    this._backgroundColor.color = color;
    if ((UnityEngine.Object) this._vfx != (UnityEngine.Object) null)
    {
      this._vfx.Stop();
      VfxManager.Instance.Delete((IVfx) this._vfx);
    }
    if (this.mCurrentIndex == MerlinTrialsHelper.inst.Day)
    {
      this._vfx = VfxManager.Instance.Create(byDay.DetailEffectPath) as SimpleVfx;
      this._vfx.offset.x = 0.0f;
      NGUITools.SetLayer(this._vfx.gameObject, LayerMask.NameToLayer("Overlay"));
      this._vfx.Play(this._texture.transform);
    }
    this.UpdateBenefits();
  }

  private void UpdateBenefits()
  {
    this.RecycleItems();
    MerlinTrialsGroupInfo byDay = ConfigManager.inst.DB_MerlinTrialsGroup.GetByDay(this.mCurrentIndex);
    if (!this._benefitsMap.ContainsKey(byDay.internalId))
    {
      this._scrollRoot.SetActive(false);
      this._noBenefits.SetActive(true);
    }
    else
    {
      Dictionary<string, double> benefits = this._benefitsMap[byDay.internalId];
      if (benefits.Count == 0)
      {
        this._scrollRoot.SetActive(false);
        this._noBenefits.SetActive(true);
      }
      else
      {
        this._scrollRoot.SetActive(true);
        this._noBenefits.SetActive(false);
        List<string> stringList = new List<string>();
        Dictionary<string, double>.Enumerator enumerator = benefits.GetEnumerator();
        while (enumerator.MoveNext())
          stringList.Add(enumerator.Current.Key);
        stringList.Sort((Comparison<string>) ((x, y) => ConfigManager.inst.DB_Properties[x].Priority.CompareTo(ConfigManager.inst.DB_Properties[y].Priority)));
        GameObject gameObject1 = NGUITools.AddChild(this._table.gameObject, this._tableTitle.gameObject);
        gameObject1.SetActive(true);
        gameObject1.GetComponentInChildren<UILabel>().text = Utils.XLAT("lord_details_military_subtitle");
        int index1 = 0;
        for (int count = stringList.Count; index1 < count; ++index1)
        {
          GameObject gameObject2 = this._pool.AddChild(this._table.gameObject);
          gameObject2.SetActive(true);
          string index2 = stringList[index1];
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[index2];
          double num = benefits[index2];
          gameObject2.transform.FindChild("valueLabel").GetComponent<UILabel>().text = Utils.GetBenefitValueString(dbProperty.Format, num);
          gameObject2.transform.FindChild("nameLabel").GetComponent<UILabel>().text = dbProperty.Name;
        }
        this._table.Reposition();
      }
    }
  }

  private void RecycleItems()
  {
    int index = 0;
    for (int count = this._items.Count; index < count; ++index)
      this._pool.Release(this._items[index]);
    this._items.Clear();
    UIUtils.ClearTable(this._table);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int index;
  }
}
