﻿// Decompiled with JetBrains decompiler
// Type: WonderSettingFlagSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WonderSettingFlagSlot : MonoBehaviour
{
  public System.Action<int> onButtonClick;
  public int serverStoredId;
  public UISprite icon;
  public Transform checker;
  public UIButton BT_checker;

  public void SetEnable(bool flag)
  {
    this.BT_checker.isEnabled = flag;
  }

  public void Selected(bool flag = true)
  {
    this.checker.gameObject.SetActive(flag);
  }

  public void Setup(int inIndex, string iconName)
  {
    this.serverStoredId = inIndex;
    this.icon.spriteName = iconName;
  }

  public void OnButtonClick()
  {
    if (this.onButtonClick == null)
      return;
    this.onButtonClick(this.serverStoredId);
  }

  public void Reset()
  {
    this.icon = this.transform.FindChild("Flag").GetComponent<UISprite>();
    this.checker = this.transform.FindChild("Tick");
    this.BT_checker = this.GetComponent<UIButton>();
  }
}
