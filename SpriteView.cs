﻿// Decompiled with JetBrains decompiler
// Type: SpriteView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SpriteView : IVfxTarget
{
  private bool _isVisible = true;
  private bool _isAsyncLoad = true;
  private Vector3 _modelOffset = Vector3.zero;
  private Dictionary<SpriteView.State, System.Action> _forwardAction = new Dictionary<SpriteView.State, System.Action>();
  private Dictionary<SpriteView.State, System.Action> _backAction = new Dictionary<SpriteView.State, System.Action>();
  private Queue<System.Action> _actionQueue = new Queue<System.Action>();
  private float _colliderSize = 1f;
  private HashSet<long> _vfxIds = new HashSet<long>();
  private SpriteView.State _state;
  private GameObject _gameObject;
  private GameObject _model;
  private SpriteViewData _viewData;
  private System.Action<bool, UnityEngine.Object> _callback;
  private UnityEngine.Object _mainAsset;
  private bool _inStateTransition;
  private Transform _transform;
  private Animator _animator;
  private AnimationEventHandler _animationEventHandler;
  private string _currentAnimation;
  private float _animtionStartTime;
  private string _animtionStateTrigger;
  private Collider _collider;
  private bool _colliderEnabled;
  private Renderer _mainRenderer;
  private ParticleScaler _particelScaler;

  public SpriteView()
  {
    this.SetupActionMap();
    this.InitailzeStateMachine();
    this.SetState(SpriteView.State.Created);
  }

  public event System.Action<SpriteView.State, SpriteView.State> OnStateChange;

  public event System.Action<AnimationEvent> OnAnimationEvent;

  public GameObject GameObject
  {
    get
    {
      return this._gameObject;
    }
  }

  public GameObject Model
  {
    get
    {
      return this._model;
    }
  }

  public SpriteViewData ViewData
  {
    get
    {
      return this._viewData;
    }
  }

  public bool IsLoaded
  {
    get
    {
      return this._state >= SpriteView.State.Loaded;
    }
  }

  public SpriteView.State CurrentState
  {
    get
    {
      return this._state;
    }
  }

  public void Load(SpriteViewData data, bool autoShow = true)
  {
    this.Unload();
    if (data == null)
      return;
    this._viewData = data;
    this._isVisible = autoShow;
    this._isAsyncLoad = false;
    this._callback = (System.Action<bool, UnityEngine.Object>) null;
    this.SetState(SpriteView.State.Initialized);
  }

  public void LoadAsync(SpriteViewData data, bool autoShow = true, System.Action<bool, UnityEngine.Object> callback = null)
  {
    this.Unload();
    if (data == null)
      return;
    this._viewData = data;
    this._isVisible = autoShow;
    this._isAsyncLoad = true;
    this._callback = callback;
    this.SetState(SpriteView.State.Initialized);
  }

  public void Show()
  {
    this._isVisible = true;
    if (!this.IsLoaded)
      return;
    this.SetState(SpriteView.State.Displayed);
  }

  public void Hide()
  {
    this._isVisible = false;
    if (!this.IsLoaded)
      return;
    this.SetState(SpriteView.State.Loaded);
  }

  public void Unload()
  {
    this.SetState(SpriteView.State.Created);
  }

  public void Dispose()
  {
    this.SetState(SpriteView.State.Invalid);
    this.FinalizeStateMachine();
    this._callback = (System.Action<bool, UnityEngine.Object>) null;
  }

  private void LoadCallback(UnityEngine.Object asset, bool result)
  {
    if (result)
    {
      this._mainAsset = asset;
      this.SetState(SpriteView.State.Loaded);
      this.SyncModelState();
    }
    if (this._callback == null)
      return;
    this._callback(result, asset);
  }

  protected virtual void OnCreate()
  {
    this._gameObject = new GameObject("DragonView");
    this._transform = this._gameObject.transform;
  }

  protected virtual void OnInitialize()
  {
    this._modelOffset = Vector3.zero;
    this._currentAnimation = (string) null;
    this._animtionStateTrigger = (string) null;
    this._animtionStartTime = -1f;
    if (this._viewData == null || string.IsNullOrEmpty(this._viewData.mainAsset))
      return;
    if (!this._isAsyncLoad)
    {
      UnityEngine.Object asset = AssetManager.Instance.HandyLoad(this._viewData.mainAsset, (System.Type) null);
      this.SafeCall((System.Action) (() => this.LoadCallback(asset, asset != (UnityEngine.Object) null)));
    }
    else
      AssetManager.Instance.LoadAsync(this._viewData.mainAsset, new System.Action<UnityEngine.Object, bool>(this.LoadCallback), (System.Type) null);
  }

  protected virtual void OnLoad()
  {
    GameObject mainAsset = this._mainAsset as GameObject;
    if ((UnityEngine.Object) mainAsset != (UnityEngine.Object) null)
    {
      this._model = UnityEngine.Object.Instantiate<GameObject>(mainAsset);
      if ((UnityEngine.Object) this._model != (UnityEngine.Object) null && (UnityEngine.Object) this._gameObject != (UnityEngine.Object) null)
      {
        this._animator = this._model.GetComponentInChildren<Animator>();
        if ((UnityEngine.Object) this._animator != (UnityEngine.Object) null)
        {
          this._animationEventHandler = Utils.EnsureComponent<AnimationEventHandler>(this._animator.gameObject);
          this._animationEventHandler.OnAnimationEvent += new System.Action<AnimationEvent>(this.AnimationEventCallback);
        }
        this._mainRenderer = (Renderer) this._model.GetComponentInChildren<SkinnedMeshRenderer>();
        this._model.transform.parent = this._gameObject.transform;
        this._model.transform.localPosition = this._modelOffset;
        this._model.transform.localRotation = Quaternion.identity;
        this._model.transform.localScale = Vector3.one;
        this._particelScaler = Utils.EnsureComponent<ParticleScaler>(this._model);
        this._particelScaler.alsoScaleGameobject = false;
        this._particelScaler.clearOldParticles = false;
        this._model.SetActive(false);
      }
    }
    this._mainAsset = (UnityEngine.Object) null;
    if (this._viewData == null)
      return;
    AssetManager.Instance.UnLoadAsset(this._viewData.mainAsset, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
  }

  protected virtual void OnShow()
  {
    if (!((UnityEngine.Object) this._model != (UnityEngine.Object) null))
      return;
    this._model.SetActive(true);
  }

  protected virtual void OnHide()
  {
    if (!(bool) ((UnityEngine.Object) this._model))
      return;
    this._model.SetActive(false);
  }

  protected virtual void OnUnload()
  {
    this.ClearAllVfxs();
    if ((UnityEngine.Object) this._animationEventHandler != (UnityEngine.Object) null)
    {
      this._animationEventHandler.OnAnimationEvent -= new System.Action<AnimationEvent>(this.AnimationEventCallback);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._animationEventHandler);
      this._animationEventHandler = (AnimationEventHandler) null;
    }
    if ((UnityEngine.Object) this._particelScaler != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._particelScaler);
      this._particelScaler = (ParticleScaler) null;
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) this._model);
    this._animtionStateTrigger = (string) null;
    this._currentAnimation = (string) null;
    this._animtionStartTime = -1f;
    this._modelOffset = Vector3.zero;
    this._mainRenderer = (Renderer) null;
    this._animator = (Animator) null;
    this._model = (GameObject) null;
  }

  protected virtual void OnFinalize()
  {
    this._viewData = (SpriteViewData) null;
  }

  protected virtual void OnDestory()
  {
    this.DestoryCollider();
    this.SetRendererLayer(0);
    this._transform = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._gameObject);
    this._gameObject = (GameObject) null;
  }

  protected virtual void SyncModelState()
  {
    if (!((UnityEngine.Object) this._model != (UnityEngine.Object) null) || !((UnityEngine.Object) this._gameObject != (UnityEngine.Object) null))
      return;
    Utils.SetLayer(this._model, this._gameObject.layer);
    if (this._isVisible)
      this.SetState(SpriteView.State.Displayed);
    this.SyncAnimationState();
    if (!((UnityEngine.Object) this._particelScaler != (UnityEngine.Object) null))
      return;
    this._particelScaler.particleScale = this._transform.lossyScale.x;
  }

  protected void SyncAnimationState()
  {
    if (!((UnityEngine.Object) this._animator != (UnityEngine.Object) null))
      return;
    if (!string.IsNullOrEmpty(this._currentAnimation))
    {
      this._animator.CrossFade(this._currentAnimation, 0.5f);
    }
    else
    {
      if (string.IsNullOrEmpty(this._animtionStateTrigger))
        return;
      this._animator.SetTrigger(this._animtionStateTrigger);
    }
  }

  public void SetModelOffset(float x, float y, float z)
  {
    this._modelOffset.x = x;
    this._modelOffset.y = y;
    this._modelOffset.z = z;
    if (!((UnityEngine.Object) this._model != (UnityEngine.Object) null))
      return;
    this._model.transform.localPosition = this._modelOffset;
  }

  public void SetModelOffset(Vector3 offset)
  {
    this.SetModelOffset(offset.x, offset.y, offset.z);
  }

  private void SetupActionMap()
  {
    this._forwardAction.Add(SpriteView.State.Invalid, new System.Action(this.OnCreate));
    this._forwardAction.Add(SpriteView.State.Created, new System.Action(this.OnInitialize));
    this._forwardAction.Add(SpriteView.State.Initialized, new System.Action(this.OnLoad));
    this._forwardAction.Add(SpriteView.State.Loaded, new System.Action(this.OnShow));
    this._forwardAction.Add(SpriteView.State.Displayed, (System.Action) null);
    this._backAction.Add(SpriteView.State.Displayed, new System.Action(this.OnHide));
    this._backAction.Add(SpriteView.State.Loaded, new System.Action(this.OnUnload));
    this._backAction.Add(SpriteView.State.Initialized, new System.Action(this.OnFinalize));
    this._backAction.Add(SpriteView.State.Created, new System.Action(this.OnDestory));
    this._backAction.Add(SpriteView.State.Invalid, (System.Action) null);
  }

  private void InitailzeStateMachine()
  {
    this._inStateTransition = false;
  }

  private void FinalizeStateMachine()
  {
    this._actionQueue.Clear();
    this._inStateTransition = false;
  }

  private void SetState(SpriteView.State state)
  {
    if (this._state == state)
      return;
    if (this._inStateTransition)
    {
      this.SafeCall((System.Action) (() => this.SetState(state)));
    }
    else
    {
      this._inStateTransition = true;
      Dictionary<SpriteView.State, System.Action> dictionary = (Dictionary<SpriteView.State, System.Action>) null;
      int num = 0;
      if (state > this._state)
      {
        num = 1;
        dictionary = this._forwardAction;
      }
      else if (state < this._state)
      {
        num = -1;
        dictionary = this._backAction;
      }
      SpriteView.State state1 = this._state;
      while (state1 != state)
      {
        System.Action action = dictionary[state1];
        if (action != null)
          action();
        state1 += (SpriteView.State) num;
      }
      SpriteView.State state2 = this._state;
      this._state = state;
      if (this.OnStateChange != null)
        this.OnStateChange(state2, this._state);
      this._inStateTransition = false;
      this.TriggerDelayedAction();
    }
  }

  private void SafeCall(System.Action action)
  {
    this._actionQueue.Enqueue(action);
  }

  private void TriggerDelayedAction()
  {
    while (this._actionQueue.Count != 0)
    {
      System.Action action = this._actionQueue.Dequeue();
      if (action != null)
        action();
    }
    this._actionQueue.Clear();
  }

  public void Mount(Transform node)
  {
    this._gameObject.transform.parent = node;
  }

  public void Mount(Transform node, Vector3 position, Quaternion rotation, Vector3 scale)
  {
    this.Mount(node);
    this.SetPosition(position);
    this.SetRotation(rotation);
    this.SetScale(scale);
  }

  public void Unmount()
  {
    this.Mount((Transform) null);
  }

  public void SetPosition(Vector3 position)
  {
    if ((UnityEngine.Object) this._transform == (UnityEngine.Object) null)
      return;
    this._transform.localPosition = position;
  }

  public void SetRotation(Vector3 rotation)
  {
    if ((UnityEngine.Object) this._transform == (UnityEngine.Object) null)
      return;
    this._transform.localRotation = Quaternion.Euler(rotation);
  }

  public void SetRotation(Quaternion rotation)
  {
    if ((UnityEngine.Object) this._transform == (UnityEngine.Object) null)
      return;
    this._transform.localRotation = rotation;
  }

  public void SetScale(Vector3 scale)
  {
    if ((UnityEngine.Object) this._transform == (UnityEngine.Object) null)
      return;
    this._transform.localScale = scale;
    if (!((UnityEngine.Object) this._particelScaler != (UnityEngine.Object) null))
      return;
    this._particelScaler.particleScale = this._transform.lossyScale.x;
  }

  public virtual void FaceTo(Transform target)
  {
    this._transform.LookAt(target);
  }

  public Animator animator
  {
    get
    {
      return this._animator;
    }
  }

  public void PlayAnimation(string name, float duration = 0.5f)
  {
    this._currentAnimation = name;
    this._animtionStateTrigger = (string) null;
    this._animtionStartTime = Time.time;
    if ((UnityEngine.Object) this._animator == (UnityEngine.Object) null)
      return;
    this._animator.CrossFade(name, duration, -1, 0.0f);
  }

  public void SetAnimationStateTrigger(string triggerName)
  {
    this._animtionStateTrigger = triggerName;
    this._currentAnimation = (string) null;
    if ((UnityEngine.Object) this._animator == (UnityEngine.Object) null)
      return;
    this._animator.SetTrigger(triggerName);
  }

  private void AnimationEventCallback(AnimationEvent e)
  {
    if (this.OnAnimationEvent == null)
      return;
    this.OnAnimationEvent(e);
  }

  public bool ColliderEnabled
  {
    set
    {
      if (this._colliderEnabled == value)
        return;
      if (value && (UnityEngine.Object) this._collider == (UnityEngine.Object) null)
        this.CreateCollider();
      if ((UnityEngine.Object) this._collider != (UnityEngine.Object) null)
        this._collider.enabled = value;
      this._colliderEnabled = value;
    }
    get
    {
      return this._colliderEnabled;
    }
  }

  public float ColliderSize
  {
    set
    {
      this._colliderSize = value;
      SphereCollider collider = this._collider as SphereCollider;
      if (!((UnityEngine.Object) collider != (UnityEngine.Object) null))
        return;
      collider.radius = value;
    }
    get
    {
      return this._colliderSize;
    }
  }

  protected virtual void CreateCollider()
  {
    SphereCollider sphereCollider = Utils.EnsureComponent<SphereCollider>(this._gameObject);
    sphereCollider.radius = this._colliderSize;
    this._collider = (Collider) sphereCollider;
  }

  protected virtual void DestoryCollider()
  {
    if ((UnityEngine.Object) this._collider != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._collider);
      this._collider = (Collider) null;
    }
    this._colliderEnabled = false;
  }

  public Renderer MainRenderer
  {
    get
    {
      return this._mainRenderer;
    }
  }

  public void SetRendererLayer(string layerName)
  {
    this.SetRendererLayer(LayerMask.NameToLayer(layerName));
  }

  public void SetRendererLayer(int layerId)
  {
    if (layerId < 0)
      return;
    this._gameObject.layer = layerId;
    if (!((UnityEngine.Object) this._model != (UnityEngine.Object) null))
      return;
    Utils.SetLayer(this._model, layerId);
  }

  public Transform transform
  {
    get
    {
      return this._transform;
    }
  }

  public Transform GetDummyPoint(string key)
  {
    GameObject child = Utils.FindChild(this._gameObject, key);
    if ((UnityEngine.Object) child != (UnityEngine.Object) null)
      return child.transform;
    return this._transform;
  }

  public long PlayVfx(string asset)
  {
    if (string.IsNullOrEmpty(asset))
      return -1;
    long andPlay = VfxManager.Instance.CreateAndPlay(asset, (IVfxTarget) this);
    this.AddVfxRef(andPlay);
    return andPlay;
  }

  public bool StopVfx(long vfxId)
  {
    if (!this.RemoveVfxRef(vfxId))
      return false;
    VfxManager.Instance.Delete(vfxId);
    return true;
  }

  public bool AddVfxRef(long vfxId)
  {
    return this._vfxIds.Add(vfxId);
  }

  public bool RemoveVfxRef(long vfxId)
  {
    return this._vfxIds.Remove(vfxId);
  }

  public void ClearAllVfxs()
  {
    HashSet<long>.Enumerator enumerator = this._vfxIds.GetEnumerator();
    while (enumerator.MoveNext())
      VfxManager.Instance.Delete(enumerator.Current);
    this._vfxIds.Clear();
  }

  public enum State
  {
    Invalid,
    Created,
    Initialized,
    Loaded,
    Displayed,
  }
}
