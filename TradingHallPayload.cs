﻿// Decompiled with JetBrains decompiler
// Type: TradingHallPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TradingHallPayload
{
  private TradingHallData _tradingData = new TradingHallData();
  private Dictionary<string, string> _errorMessage = new Dictionary<string, string>()
  {
    {
      "3000011",
      "toast_exchange_hall_item_sold_out"
    },
    {
      "3000021",
      "toast_exchange_hall_purchase_fail_insufficient_gold"
    },
    {
      "3000031",
      "toast_exchange_hall_purchase_fail_insufficient_items"
    },
    {
      "3000041",
      "toast_exchange_hall_list_fail_insufficient_items"
    },
    {
      "3000061",
      "exception_3000061"
    }
  };
  private static TradingHallPayload _instance;
  public System.Action OnSellDataUpdated;
  public System.Action OnBuyDataUpdated;

  public static TradingHallPayload Instance
  {
    get
    {
      if (TradingHallPayload._instance == null)
        TradingHallPayload._instance = new TradingHallPayload();
      return TradingHallPayload._instance;
    }
  }

  public TradingHallData TradingData
  {
    get
    {
      if (this._tradingData == null)
        this._tradingData = new TradingHallData();
      return this._tradingData;
    }
  }

  public Dictionary<string, string> ErrorMessage
  {
    get
    {
      return this._errorMessage;
    }
  }

  public void Initialize()
  {
    this.RequestServerData();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    MessageHub.inst.GetPortByAction("exchange_sell").AddEvent(new System.Action<object>(this.ExchangeSellDataPush));
  }

  public void Dispose()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    MessageHub.inst.GetPortByAction("exchange_sell").RemoveEvent(new System.Action<object>(this.ExchangeSellDataPush));
    this._tradingData = (TradingHallData) null;
  }

  public int HowManyDays(int seconds)
  {
    if (seconds > 0)
      return Mathf.CeilToInt((float) seconds / 86400f);
    return 0;
  }

  public void GetCatalogId(TradingHallCatalog catalog, ref int catalog1Id, ref int catalog2Id, ref int catalog3Id)
  {
    if ((UnityEngine.Object) catalog == (UnityEngine.Object) null)
      return;
    switch (catalog.CatalogLevel)
    {
      case 1:
        catalog1Id = catalog.CatalogId;
        break;
      case 2:
        catalog2Id = catalog.CatalogId;
        if (!((UnityEngine.Object) catalog.ParentCatalog != (UnityEngine.Object) null))
          break;
        catalog1Id = catalog.ParentCatalog.CatalogId;
        break;
      case 3:
        catalog3Id = catalog.CatalogId;
        if (!((UnityEngine.Object) catalog.ParentCatalog != (UnityEngine.Object) null))
          break;
        catalog2Id = catalog.ParentCatalog.CatalogId;
        if (!((UnityEngine.Object) catalog.ParentCatalog.ParentCatalog != (UnityEngine.Object) null))
          break;
        catalog1Id = catalog.ParentCatalog.ParentCatalog.CatalogId;
        break;
    }
  }

  public TradingHallData.BuyGoodData GetBuyGoodDataById(int exchangeId)
  {
    return this.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == exchangeId));
  }

  public int GoodOverdueCount
  {
    get
    {
      List<TradingHallData.SellGoodData> all = this.TradingData.SellGoodDataList.FindAll((Predicate<TradingHallData.SellGoodData>) (x => !x.IsValid));
      if (all != null)
        return all.Count;
      return 0;
    }
  }

  public BagType GetBagType(int exchangeId)
  {
    BagType bagType = BagType.None;
    ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(exchangeId);
    if (exchangeHallInfo != null)
    {
      if (exchangeHallInfo.itemType == "equipment")
        bagType = BagType.Hero;
      else if (exchangeHallInfo.itemType == "dk_equipment")
        bagType = BagType.DragonKnight;
    }
    return bagType;
  }

  public int GetEquipmentId(int exchangeId, int itemId)
  {
    ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(exchangeId);
    if (exchangeHallInfo != null && (exchangeHallInfo.itemType == "equipment" || exchangeHallInfo.itemType == "dk_equipment"))
    {
      BagType bagType = this.GetBagType(exchangeId);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
      if (itemStaticInfo != null)
      {
        ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(itemStaticInfo.ID);
        if (data != null)
          return data.internalId;
      }
    }
    return 0;
  }

  public int GetEquipmentEnhanced(int exchangeId)
  {
    int result = 0;
    ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(exchangeId);
    if (exchangeHallInfo != null && (exchangeHallInfo.itemType == "equipment" || exchangeHallInfo.itemType == "dk_equipment"))
      int.TryParse(exchangeHallInfo.param1, out result);
    return result;
  }

  public int MaxShelfCount
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("exchange_hall_field_num");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public int CurrentShelfCount
  {
    get
    {
      List<TradingHallData.SellGoodData> all = this.TradingData.SellGoodDataList.FindAll((Predicate<TradingHallData.SellGoodData>) (x => x.IsValid));
      if (all != null)
        return all.Count;
      return 0;
    }
  }

  public bool ShouldShowTradingHall
  {
    get
    {
      bool flag = true;
      GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("exchange_hall_stronghold_level_min");
      if (data1 != null)
        flag = data1.ValueInt <= PlayerData.inst.CityData.mStronghold.mLevel;
      GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("exchange_hall_kingdom_open_time_min");
      if (data2 != null)
        flag &= PlayerData.inst.ServerOpenTime + data2.ValueInt * 24 * 3600 < NetServerTime.inst.ServerTimestamp;
      return flag;
    }
  }

  public float TaxRate
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("exchange_hall_transaction_tax_rate");
      if (data != null)
        return (float) data.Value;
      return 0.0f;
    }
  }

  public int AutoRefreshCD
  {
    get
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("exchange_hall_auto_refresh_cd");
      if (data != null)
        return data.ValueInt;
      return 0;
    }
  }

  public void RequestServerData()
  {
    MessageHub.inst.GetPortByAction("exchange:getSellGoods").SendLoader(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.Decode(data as ArrayList, TradingHallData.ExchangeType.SELL);
    }), true, false);
  }

  public void GetSellGoods(bool blockScreen = true, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("exchange:getSellGoods").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as ArrayList, TradingHallData.ExchangeType.SELL);
      if (callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void RetrieveGood(int exchangeId, int equipmentId, int price, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "exchange_id", (object) exchangeId);
    postData.Add((object) nameof (price), (object) price);
    if (equipmentId > 0)
      postData.Add((object) "equipment_id", (object) equipmentId);
    MessageHub.inst.GetPortByAction("exchange:retrieveGoods").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as ArrayList, TradingHallData.ExchangeType.SELL);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void SellGood(int exchangeId, int price, int amount, int equipmentId, bool isResell, int originPrice, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "exchange_id", (object) exchangeId);
    postData.Add((object) nameof (price), (object) price);
    postData.Add((object) nameof (amount), (object) amount);
    if (isResell)
    {
      postData.Add((object) "re_sell", (object) 1);
      postData.Add((object) "origin_price", (object) originPrice);
    }
    if (equipmentId > 0)
      postData.Add((object) "equipment_id", (object) equipmentId);
    MessageHub.inst.GetPortByAction("exchange:sell").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as ArrayList, TradingHallData.ExchangeType.SELL);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void BuyGood(int exchangeId, int price, long amount, TradingHallCatalog catalog, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    postData.Add((object) "exchange_id", (object) exchangeId);
    postData.Add((object) nameof (price), (object) price);
    postData.Add((object) nameof (amount), (object) amount);
    int catalog1Id = -1;
    int catalog2Id = -1;
    int catalog3Id = -1;
    TradingHallPayload.Instance.GetCatalogId(catalog, ref catalog1Id, ref catalog2Id, ref catalog3Id);
    postData.Add((object) "sort_1", (object) catalog1Id);
    postData.Add((object) "sort_2", (object) catalog2Id);
    postData.Add((object) "sort_3", (object) catalog3Id);
    MessageHub.inst.GetPortByAction("exchange:buy").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        this.Decode(data as ArrayList, TradingHallData.ExchangeType.BUY);
        this.ShowBasicToast("toast_exchange_hall_buy_success", exchangeId, (int) amount);
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void GetBuyGoods(int catalog1Id, int catalog2Id, int catalog3Id, bool blockScreen = true, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("exchange:getList").SendRequest(new Hashtable()
    {
      {
        (object) "sort_1",
        (object) catalog1Id
      },
      {
        (object) "sort_2",
        (object) catalog2Id
      },
      {
        (object) "sort_3",
        (object) catalog3Id
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data as ArrayList, TradingHallData.ExchangeType.BUY);
      if (callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  public void ShowFirstScene(int openTabIndex = 0)
  {
    UIManager.inst.OpenDlg("TradingHall/TradingHallDlg", (UI.Dialog.DialogParameter) new TradingHallDlg.Parameter()
    {
      openTabIndex = openTabIndex
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void ShowBasicToast(string toastKey, int exchangeId, int amount)
  {
    ExchangeHallInfo exchangeHallInfo = ConfigManager.inst.DB_ExchangeHall.Get(exchangeId);
    if (exchangeHallInfo == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(exchangeHallInfo.itemId);
    if (itemStaticInfo == null)
      return;
    ToastManager.Instance.AddBasicToast(toastKey, "0", itemStaticInfo.LocName, "1", amount.ToString()).SetPriority(-1);
  }

  public void CheckErrorMessage(Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "errno"))
      return;
    string key = data[(object) "errno"].ToString();
    if (!this.ErrorMessage.ContainsKey(key))
      return;
    UIManager.inst.toast.Show(Utils.XLAT(this.ErrorMessage[key]), (System.Action) null, 4f, true);
  }

  private void OnSecondEvent(int time)
  {
    this.CheckSellGoodsOverdue();
  }

  private void ExchangeSellDataPush(object data)
  {
    if (data == null)
      return;
    Hashtable inData = data as Hashtable;
    if (inData == null)
      return;
    int outData1 = 0;
    int outData2 = 0;
    DatabaseTools.UpdateData(inData, "exchange_id", ref outData1);
    DatabaseTools.UpdateData(inData, "amount", ref outData2);
    this.ShowBasicToast("toast_exchange_hall_sell_success", outData1, outData2);
    if (!inData.ContainsKey((object) "sell_goods_list"))
      return;
    this.Decode(inData[(object) "sell_goods_list"] as ArrayList, TradingHallData.ExchangeType.SELL);
  }

  private void CheckSellGoodsOverdue()
  {
    TradingHallData.SellGoodData sellGoodData = this.TradingData.SellGoodDataList.Find((Predicate<TradingHallData.SellGoodData>) (x => x.ValidTime + 1 == NetServerTime.inst.ServerTimestamp));
    if (sellGoodData == null || sellGoodData.ExchangeId <= 0)
      return;
    this.ShowBasicToast("toast_exchange_hall_listed_item_expired", sellGoodData.ExchangeId, sellGoodData.Amount);
  }

  private void Decode(ArrayList data, TradingHallData.ExchangeType type)
  {
    this.TradingData.Decode(data, type);
    switch (type)
    {
      case TradingHallData.ExchangeType.BUY:
        if (this.OnBuyDataUpdated == null)
          break;
        this.OnBuyDataUpdated();
        break;
      case TradingHallData.ExchangeType.SELL:
        if (this.OnSellDataUpdated == null)
          break;
        this.OnSellDataUpdated();
        break;
    }
  }
}
