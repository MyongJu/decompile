﻿// Decompiled with JetBrains decompiler
// Type: SettingManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class SettingManager
{
  private bool _isDaltonOff = true;
  private bool _isPrioritySelectionOff = true;
  private static SettingManager _instance;
  private bool _isMusicOff;
  private bool _isSFXOff;
  private bool _isEnvironmentOff;

  public event System.Action OnSettingChanged;

  public static SettingManager Instance
  {
    get
    {
      if (SettingManager._instance == null)
        SettingManager._instance = new SettingManager();
      return SettingManager._instance;
    }
  }

  private void Save()
  {
    PlayerPrefsEx.SetInt("setting_init", 1);
    PlayerPrefsEx.SetInt("music_play", !this._isMusicOff ? 0 : 1);
    PlayerPrefsEx.SetInt("sfx_play", !this._isSFXOff ? 0 : 1);
    PlayerPrefsEx.SetInt("environment", !this._isEnvironmentOff ? 0 : 1);
    PlayerPrefsEx.SetInt("dalton_mode", !this._isDaltonOff ? 0 : 1);
    PlayerPrefsEx.SetInt("priority_selection", !this._isPrioritySelectionOff ? 0 : 1);
  }

  private void Notify()
  {
    if (this.OnSettingChanged == null)
      return;
    this.OnSettingChanged();
  }

  public bool IsSFXOff
  {
    get
    {
      return this._isSFXOff;
    }
    set
    {
      if (this._isSFXOff == value)
        return;
      this._isSFXOff = value;
      this.Save();
      this.Notify();
    }
  }

  public bool IsMusicOff
  {
    get
    {
      return this._isMusicOff;
    }
    set
    {
      if (this._isMusicOff == value)
        return;
      this._isMusicOff = value;
      this.Save();
      this.Notify();
    }
  }

  public bool IsEnvironmentOff
  {
    get
    {
      return this._isEnvironmentOff;
    }
    set
    {
      if (this._isEnvironmentOff == value)
        return;
      this._isEnvironmentOff = value;
      this.Save();
      this.Notify();
    }
  }

  public bool IsPrioritySelectionOff
  {
    get
    {
      return this._isPrioritySelectionOff;
    }
    set
    {
      if (this._isPrioritySelectionOff == value)
        return;
      this._isPrioritySelectionOff = value;
      this.Save();
      this.Notify();
    }
  }

  public bool IsDaltonOff
  {
    get
    {
      return this._isDaltonOff;
    }
    set
    {
      if (this._isDaltonOff == value)
        return;
      this._isDaltonOff = value;
      this.Save();
      this.Notify();
    }
  }

  public void Init()
  {
    if (PlayerPrefsEx.GetInt("setting_init") <= 0)
      return;
    this._isMusicOff = PlayerPrefsEx.GetInt("music_play") > 0;
    this._isSFXOff = PlayerPrefsEx.GetInt("sfx_play") > 0;
    this._isEnvironmentOff = PlayerPrefsEx.GetInt("environment") > 0;
    this._isDaltonOff = PlayerPrefsEx.GetInt("dalton_mode") > 0;
    if (!PlayerPrefsEx.HasKey("dalton_mode"))
      this._isDaltonOff = true;
    if (PlayerPrefsEx.HasKey("priority_selection"))
      this._isPrioritySelectionOff = PlayerPrefsEx.GetInt("priority_selection") > 0;
    else
      this._isPrioritySelectionOff = true;
  }
}
