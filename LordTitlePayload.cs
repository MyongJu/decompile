﻿// Decompiled with JetBrains decompiler
// Type: LordTitlePayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LordTitlePayload
{
  private Dictionary<int, long> _activatedAvatorDict = new Dictionary<int, long>();
  private const string LORD_TITLE_HAS_UNLOCKED_PREFIX = "lord_title_has_unlocked_";
  private const string AVATOR_OBJECT_NAME = "custom_user_avator";
  private const float AVATOR_SIZE_FACTOR = 1.65f;
  private const int AVATOR_DEPTH_OFFSET = 1;
  public System.Action OnLordTitleDataUpdate;
  private static LordTitlePayload _instance;

  public static LordTitlePayload Instance
  {
    get
    {
      if (LordTitlePayload._instance == null)
        LordTitlePayload._instance = new LordTitlePayload();
      return LordTitlePayload._instance;
    }
  }

  public string CityImagePath
  {
    get
    {
      return string.Format("Prefab/Tiles/tiles_stronghold_lv{0}", (object) Mathf.Min(Mathf.Max(1, PlayerData.inst.playerCityData.level), MapUtils.CityPrefabNames.Length));
    }
  }

  public Dictionary<int, long> ActivatedAvatorDict
  {
    get
    {
      return this._activatedAvatorDict;
    }
  }

  public void UnlockAvatorTitle(int lordTitleId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Item:useConsumableItem").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "item_id",
        (object) this.GetItemIdByAvator(lordTitleId)
      },
      {
        (object) "city_id",
        (object) PlayerData.inst.cityId
      },
      {
        (object) "amount",
        (object) 1
      }
    }, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      this.Decode(orgData as Hashtable);
      if (callback == null)
        return;
      callback(ret, orgData);
    }), true);
  }

  public string GetLordTitleName(int lordTitleId)
  {
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(lordTitleId);
    if (lordTitleMainInfo != null)
      return lordTitleMainInfo.LocName;
    return string.Empty;
  }

  public int GetItemIdByAvator(int lordTitleId)
  {
    List<ItemStaticInfo> itemListByType = ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.lord_title);
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(lordTitleId);
    if (itemListByType != null && lordTitleMainInfo != null)
    {
      using (List<ItemStaticInfo>.Enumerator enumerator = itemListByType.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ItemStaticInfo current = enumerator.Current;
          if (current.Param1 == lordTitleMainInfo.id)
            return current.internalId;
        }
      }
    }
    return 0;
  }

  public bool IsAvatorLocked(int lordTitleId)
  {
    if (this._activatedAvatorDict.ContainsKey(lordTitleId))
    {
      if (this._activatedAvatorDict[lordTitleId] == -1L)
        return false;
      JobHandle job = JobManager.Instance.GetJob(this._activatedAvatorDict[lordTitleId]);
      if (job != null && job.LeftTime() > 0)
        return false;
    }
    return true;
  }

  public bool IsAvatorActivated(int lordTitleId)
  {
    return lordTitleId == PlayerData.inst.userData.LordTitle;
  }

  public int GetItemCountByAvator(int lordTitleId)
  {
    ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.GetItemIdByAvator(lordTitleId));
    if (consumableItemData != null)
      return consumableItemData.quantity;
    return 0;
  }

  public string GetAvatorNameFixedTimeSuffix(int lordTitleId)
  {
    string str = string.Empty;
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(lordTitleId);
    if (lordTitleMainInfo != null)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(lordTitleMainInfo.itemId);
      if (itemStaticInfo != null && !string.IsNullOrEmpty(itemStaticInfo.Param2))
      {
        long result = 0;
        long.TryParse(itemStaticInfo.Param2, out result);
        if (result > 0L)
        {
          int num1 = (int) (result / 3600L / 24L);
          int num2 = (int) (result / 3600L % 24L);
          int num3 = (int) (result % 3600L / 60L);
          Dictionary<string, string> para = new Dictionary<string, string>();
          if (num1 > 0)
          {
            if (num2 == 0)
              para.Add("0", num1.ToString());
            else
              para.Add("0", string.Format("{0:N1}", (object) (float) ((double) num1 + (double) num2 / 24.0)));
            str = string.Format("({0})", (object) ScriptLocalization.GetWithPara("id_lord_title_prescription_day", para, true));
          }
          else
          {
            if (num3 == 0)
              para.Add("0", num2.ToString());
            else
              para.Add("0", string.Format("{0:N1}", (object) (float) ((double) num2 + (double) num3 / 60.0)));
            str = string.Format("({0})", (object) ScriptLocalization.GetWithPara("id_lord_title_prescription_hour", para, true));
          }
        }
      }
    }
    return str;
  }

  public string GetAvatorNameTimeSuffix(int lordTitleId, bool usePrefix = false)
  {
    string str = string.Empty;
    if (this._activatedAvatorDict.ContainsKey(lordTitleId) && this._activatedAvatorDict[lordTitleId] != -1L)
    {
      JobHandle job = JobManager.Instance.GetJob(this._activatedAvatorDict[lordTitleId]);
      if (job != null && job.LeftTime() > 0)
      {
        int time = job.LeftTime();
        str = string.Format(!usePrefix ? "({0})" : "{0}", (object) Utils.FormatTime(time, true, false, false));
        if (usePrefix)
          str = string.Format("({0}{1})", (object) Utils.XLAT("march_send_march_time_left"), (object) str);
      }
    }
    return str;
  }

  public void GetLordTitles(System.Action<bool, object> callback = null, bool blockScreen = true)
  {
    MessageHub.inst.GetPortByAction("player:getLordTitles").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      this.Decode(orgData as Hashtable);
      if (callback == null)
        return;
      callback(ret, orgData);
    }), blockScreen);
  }

  public void ActivateLordTitle(int lordTitleId, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "lord_title", (object) lordTitleId);
    bool isActivated = this.IsAvatorActivated(lordTitleId);
    MessageHub.inst.GetPortByAction("player:activeLordTitle").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      if (!ret)
        return;
      if (callback != null)
        callback(ret, orgData);
      if (isActivated || lordTitleId <= 0)
        return;
      string withPara = ScriptLocalization.GetWithPara("toast_accolade_frame_activate_success", new Dictionary<string, string>()
      {
        {
          "0",
          this.GetLordTitleName(lordTitleId)
        }
      }, true);
      ToastManager.Instance.AddBasicToast(string.Empty, "toast_accolade_frame_activate_success_title", withPara);
    }), true);
  }

  public void SetUnlockedStatus(int lordTitleId, int status)
  {
    if (!PlayerPrefsEx.HasUidKey("lord_title_has_unlocked_" + (object) lordTitleId) && status == 1)
    {
      PlayerPrefsEx.SetIntByUid("lord_title_has_unlocked_" + (object) lordTitleId, 1);
    }
    else
    {
      if (!PlayerPrefsEx.HasUidKey("lord_title_has_unlocked_" + (object) lordTitleId) || status != 0)
        return;
      PlayerPrefsEx.SetIntByUid("lord_title_has_unlocked_" + (object) lordTitleId, 0);
    }
  }

  public int GetUnlockedStatusCount()
  {
    int num1 = 0;
    int num2 = 0;
    List<LordTitleMainInfo> infoList = ConfigManager.inst.DB_LordTitleMain.GetInfoList();
    if (infoList != null)
    {
      for (int index = 0; index < infoList.Count; ++index)
      {
        if (PlayerPrefsEx.GetIntByUid("lord_title_has_unlocked_" + (object) infoList[index].internalId, 0) == 1)
          ++num1;
        if (this.IsAvatorLocked(infoList[index].internalId) && this.GetItemCountByAvator(infoList[index].internalId) > 0)
          ++num2;
      }
    }
    return num1 + num2;
  }

  public bool IsAvatorNew(int lordTitleId)
  {
    return PlayerPrefsEx.GetIntByUid("lord_title_has_unlocked_" + (object) lordTitleId, 0) == 1;
  }

  public void ApplyUserAvator(UITexture texture, int lordTitleId, int depthOffset = 1)
  {
    UITexture uiTexture = (UITexture) null;
    string path = string.Empty;
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(lordTitleId);
    if (lordTitleMainInfo != null)
      path = lordTitleMainInfo.AvatorIconPath;
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
      return;
    Transform child = texture.transform.FindChild("custom_user_avator");
    if ((bool) ((UnityEngine.Object) child))
      uiTexture = child.GetComponent<UITexture>();
    int num = (int) ((double) texture.height * 0.649999976158142);
    if ((UnityEngine.Object) uiTexture == (UnityEngine.Object) null)
    {
      uiTexture = NGUITools.AddWidget<UITexture>(texture.gameObject);
      uiTexture.gameObject.name = "custom_user_avator";
      uiTexture.height = texture.height + num;
      uiTexture.width = texture.width + num;
    }
    if (!(bool) ((UnityEngine.Object) uiTexture))
      return;
    uiTexture.depth = texture.depth + depthOffset;
    uiTexture.pivot = texture.pivot;
    uiTexture.enabled = true;
    BuilderFactory.Instance.HandyBuild((UIWidget) uiTexture, path, (System.Action<bool>) null, false, true, string.Empty);
    uiTexture.transform.localPosition = new Vector3((float) num * (uiTexture.pivotOffset.x - 0.5f), (float) num * (uiTexture.pivotOffset.y - 0.5f), 0.0f);
    if (!string.IsNullOrEmpty(path))
      return;
    uiTexture.enabled = false;
    uiTexture.mainTexture = (Texture) null;
  }

  public string GetKingdomNameBackground(int lordTitleId)
  {
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(lordTitleId);
    if (lordTitleMainInfo != null)
      return lordTitleMainInfo.BoardIconPath;
    return "rectangle";
  }

  private void Decode(Hashtable data)
  {
    if (data == null)
      return;
    if (data.ContainsKey((object) "lord_titles"))
    {
      Hashtable hashtable = data[(object) "lord_titles"] as Hashtable;
      if (hashtable != null)
      {
        this._activatedAvatorDict.Clear();
        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result1;
          int.TryParse(enumerator.Current.ToString(), out result1);
          long result2;
          long.TryParse(hashtable[(object) enumerator.Current.ToString()].ToString(), out result2);
          if (result1 > 0)
            this._activatedAvatorDict.Add(result1, result2);
        }
      }
    }
    if (this.OnLordTitleDataUpdate == null)
      return;
    this.OnLordTitleDataUpdate();
  }
}
