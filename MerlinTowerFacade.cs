﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerFacade
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public static class MerlinTowerFacade
{
  public static MerlinTowerSystem _merlinTowerSystem;
  public static int RequestEnterMineIndex;

  public static void RequestEnter(bool needOpenBattlLog = false, int mineIndex = 0)
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    MerlinTowerFacade.RequestEnterMineIndex = mineIndex;
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.MerlinTowerMode)
    {
      LoadingScreen.Instance.Show(2f, (System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.MerlinTowerMode;
        if (!needOpenBattlLog)
          return;
        UIManager.inst.OpenPopup("MerlinTower/MerlinTowerWarLogPop", (Popup.PopupParameter) null);
      }));
      LoadingScreen.Instance.SetTip(Utils.XLAT("load_scene_tip"));
      LoadingScreen.Instance.MarkHide();
    }
    else
    {
      if (!needOpenBattlLog)
        return;
      UIManager.inst.OpenPopup("MerlinTower/MerlinTowerWarLogPop", (Popup.PopupParameter) null);
    }
  }

  public static void RequestExit()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.MerlinTowerMode)
      return;
    UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
  }

  public static void OnEnter()
  {
    MerlinTowerFacade._merlinTowerSystem = (UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/MerlinTower/MerlinTowerSystem", (System.Type) null)) as GameObject).GetComponent<MerlinTowerSystem>();
    MerlinTowerFacade._merlinTowerSystem.Setup();
  }

  public static void OnExit()
  {
    if ((bool) ((UnityEngine.Object) MerlinTowerFacade._merlinTowerSystem))
    {
      MerlinTowerFacade._merlinTowerSystem.Shutdown();
      UnityEngine.Object.Destroy((UnityEngine.Object) MerlinTowerFacade._merlinTowerSystem.gameObject);
      MerlinTowerFacade._merlinTowerSystem = (MerlinTowerSystem) null;
    }
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public static void OnReset()
  {
    if (!(bool) ((UnityEngine.Object) MerlinTowerFacade._merlinTowerSystem))
      return;
    MerlinTowerFacade._merlinTowerSystem.EnterTower();
  }
}
