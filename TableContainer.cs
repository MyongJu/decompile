﻿// Decompiled with JetBrains decompiler
// Type: TableContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TableContainer : MonoBehaviour
{
  public int gap = 5;
  public UITable table;
  public UIWidget backGroundWidget;

  public void ResetPosition()
  {
    this.table.Reposition();
    this.backGroundWidget.height = (int) NGUIMath.CalculateRelativeWidgetBounds(this.table.transform).size.y - this.gap;
  }
}
