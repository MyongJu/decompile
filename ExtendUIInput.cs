﻿// Decompiled with JetBrains decompiler
// Type: ExtendUIInput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ExtendUIInput : UIInput
{
  private Rect currentKeyboardArea = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
  private bool beginCheckingKeyboardArea;

  public event ExtendUIInput.KeyboardAreaChange OnKeayboardAreaChange;

  public event ExtendUIInput.OnExtendUIInputDeselect OnExtendUIInputDeselectEvent;

  protected override void OnSelect(bool isSelected)
  {
    base.OnSelect(isSelected);
    this.beginCheckingKeyboardArea = true;
    if (isSelected || this.OnExtendUIInputDeselectEvent == null)
      return;
    this.OnExtendUIInputDeselectEvent();
  }

  protected override void Update()
  {
    base.Update();
    Rect area = TouchScreenKeyboard.area;
    if (!this.beginCheckingKeyboardArea || this.OnKeayboardAreaChange == null || !(area != this.currentKeyboardArea))
      return;
    this.beginCheckingKeyboardArea = false;
    this.currentKeyboardArea = area;
    this.OnKeayboardAreaChange(this.currentKeyboardArea);
  }

  public int GetKeyboardSize()
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    {
      using (AndroidJavaObject androidJavaObject = new AndroidJavaObject("android.graphics.Rect", new object[0]))
      {
        androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView").Call("getWindowVisibleDisplayFrame", new object[1]
        {
          (object) androidJavaObject
        });
        return Screen.height - androidJavaObject.Call<int>("height");
      }
    }
  }

  public delegate void KeyboardAreaChange(Rect area);

  public delegate void OnExtendUIInputDeselect();
}
