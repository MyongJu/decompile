﻿// Decompiled with JetBrains decompiler
// Type: SimpleJSON.JSONNode
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace SimpleJSON
{
  public class JSONNode
  {
    public virtual void Add(string aKey, JSONNode aItem)
    {
    }

    public virtual JSONNode this[int aIndex]
    {
      get
      {
        return (JSONNode) null;
      }
      set
      {
      }
    }

    public virtual JSONNode this[string aKey]
    {
      get
      {
        return (JSONNode) null;
      }
      set
      {
      }
    }

    public virtual string Value
    {
      get
      {
        return string.Empty;
      }
      set
      {
      }
    }

    public virtual int Count
    {
      get
      {
        return 0;
      }
    }

    public virtual void Add(JSONNode aItem)
    {
      this.Add(string.Empty, aItem);
    }

    public virtual JSONNode Remove(string aKey)
    {
      return (JSONNode) null;
    }

    public virtual JSONNode Remove(int aIndex)
    {
      return (JSONNode) null;
    }

    public virtual JSONNode Remove(JSONNode aNode)
    {
      return aNode;
    }

    public virtual IEnumerable<JSONNode> Childs
    {
      get
      {
        JSONNode.\u003C\u003Ec__Iterator16 cIterator16_1 = new JSONNode.\u003C\u003Ec__Iterator16();
        JSONNode.\u003C\u003Ec__Iterator16 cIterator16_2 = cIterator16_1;
        cIterator16_2.\u0024PC = -2;
        return (IEnumerable<JSONNode>) cIterator16_2;
      }
    }

    public IEnumerable<JSONNode> DeepChilds
    {
      get
      {
        JSONNode.\u003C\u003Ec__Iterator17 cIterator17 = new JSONNode.\u003C\u003Ec__Iterator17()
        {
          \u003C\u003Ef__this = this
        };
        cIterator17.\u0024PC = -2;
        return (IEnumerable<JSONNode>) cIterator17;
      }
    }

    public override string ToString()
    {
      return nameof (JSONNode);
    }

    public virtual string ToString(string aPrefix)
    {
      return nameof (JSONNode);
    }

    public virtual int AsInt
    {
      get
      {
        int result = 0;
        if (int.TryParse(this.Value, out result))
          return result;
        return 0;
      }
      set
      {
        this.Value = value.ToString();
      }
    }

    public virtual float AsFloat
    {
      get
      {
        float result = 0.0f;
        if (float.TryParse(this.Value, out result))
          return result;
        return 0.0f;
      }
      set
      {
        this.Value = value.ToString();
      }
    }

    public virtual double AsDouble
    {
      get
      {
        double result = 0.0;
        if (double.TryParse(this.Value, out result))
          return result;
        return 0.0;
      }
      set
      {
        this.Value = value.ToString();
      }
    }

    public virtual bool AsBool
    {
      get
      {
        bool result = false;
        if (bool.TryParse(this.Value, out result))
          return result;
        return !string.IsNullOrEmpty(this.Value);
      }
      set
      {
        this.Value = !value ? "false" : "true";
      }
    }

    public virtual JSONArray AsArray
    {
      get
      {
        return this as JSONArray;
      }
    }

    public virtual JSONClass AsObject
    {
      get
      {
        return this as JSONClass;
      }
    }

    public override bool Equals(object obj)
    {
      return object.ReferenceEquals((object) this, obj);
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    internal static string Escape(string aText)
    {
      string str = string.Empty;
      foreach (char ch1 in aText)
      {
        char ch2 = ch1;
        switch (ch2)
        {
          case '\b':
            str += "\\b";
            break;
          case '\t':
            str += "\\t";
            break;
          case '\n':
            str += "\\n";
            break;
          case '\f':
            str += "\\f";
            break;
          case '\r':
            str += "\\r";
            break;
          default:
            str = (int) ch2 == 34 ? str + "\\\"" : ((int) ch2 == 92 ? str + "\\\\" : str + (object) ch1);
            break;
        }
      }
      return str;
    }

    public static JSONNode Parse(string aJSON)
    {
      Stack<JSONNode> jsonNodeStack = new Stack<JSONNode>();
      JSONNode jsonNode = (JSONNode) null;
      int index = 0;
      string str = string.Empty;
      string aKey1 = string.Empty;
      bool flag = false;
      for (; index < aJSON.Length; ++index)
      {
        char ch1 = aJSON[index];
        switch (ch1)
        {
          case '\t':
label_44:
            if (flag)
            {
              str += (string) (object) aJSON[index];
              continue;
            }
            continue;
          case '\n':
          case '\r':
            continue;
          default:
            switch (ch1)
            {
              case ' ':
                goto label_44;
              case '"':
                flag = ((flag ? 1 : 0) ^ 1) != 0;
                continue;
              default:
                switch (ch1)
                {
                  case '[':
                    if (flag)
                    {
                      str += (string) (object) aJSON[index];
                      continue;
                    }
                    jsonNodeStack.Push((JSONNode) new JSONArray());
                    if (jsonNode != (object) null)
                    {
                      string aKey2 = aKey1.Trim();
                      if (jsonNode is JSONArray)
                        jsonNode.Add(jsonNodeStack.Peek());
                      else if (aKey2 != string.Empty)
                        jsonNode.Add(aKey2, jsonNodeStack.Peek());
                    }
                    aKey1 = string.Empty;
                    str = string.Empty;
                    jsonNode = jsonNodeStack.Peek();
                    continue;
                  case '\\':
                    ++index;
                    if (flag)
                    {
                      char ch2 = aJSON[index];
                      char ch3 = ch2;
                      switch (ch3)
                      {
                        case 'n':
                          str += (string) (object) '\n';
                          continue;
                        case 'r':
                          str += (string) (object) '\r';
                          continue;
                        case 't':
                          str += (string) (object) '\t';
                          continue;
                        case 'u':
                          string s = aJSON.Substring(index + 1, 4);
                          str += (string) (object) (char) int.Parse(s, NumberStyles.AllowHexSpecifier);
                          index += 4;
                          continue;
                        default:
                          str = (int) ch3 == 98 ? str + (object) '\b' : ((int) ch3 == 102 ? str + (object) '\f' : str + (object) ch2);
                          continue;
                      }
                    }
                    else
                      continue;
                  case ']':
                  case '}':
                    if (flag)
                    {
                      str += (string) (object) aJSON[index];
                      continue;
                    }
                    if (jsonNodeStack.Count == 0)
                      throw new Exception("JSON Parse: Too many closing brackets");
                    jsonNodeStack.Pop();
                    if (str != string.Empty)
                    {
                      string aKey2 = aKey1.Trim();
                      if (jsonNode is JSONArray)
                        jsonNode.Add((JSONNode) str);
                      else if (aKey2 != string.Empty)
                        jsonNode.Add(aKey2, (JSONNode) str);
                    }
                    aKey1 = string.Empty;
                    str = string.Empty;
                    if (jsonNodeStack.Count > 0)
                    {
                      jsonNode = jsonNodeStack.Peek();
                      continue;
                    }
                    continue;
                  case '{':
                    if (flag)
                    {
                      str += (string) (object) aJSON[index];
                      continue;
                    }
                    jsonNodeStack.Push((JSONNode) new JSONClass());
                    if (jsonNode != (object) null)
                    {
                      string aKey2 = aKey1.Trim();
                      if (jsonNode is JSONArray)
                        jsonNode.Add(jsonNodeStack.Peek());
                      else if (aKey2 != string.Empty)
                        jsonNode.Add(aKey2, jsonNodeStack.Peek());
                    }
                    aKey1 = string.Empty;
                    str = string.Empty;
                    jsonNode = jsonNodeStack.Peek();
                    continue;
                  default:
                    switch (ch1)
                    {
                      case ',':
                        if (flag)
                        {
                          str += (string) (object) aJSON[index];
                          continue;
                        }
                        if (str != string.Empty)
                        {
                          if (jsonNode is JSONArray)
                            jsonNode.Add((JSONNode) str);
                          else if (aKey1 != string.Empty)
                            jsonNode.Add(aKey1, (JSONNode) str);
                        }
                        aKey1 = string.Empty;
                        str = string.Empty;
                        continue;
                      case ':':
                        if (flag)
                        {
                          str += (string) (object) aJSON[index];
                          continue;
                        }
                        aKey1 = str;
                        str = string.Empty;
                        continue;
                      default:
                        str += (string) (object) aJSON[index];
                        continue;
                    }
                }
            }
        }
      }
      if (flag)
        throw new Exception("JSON Parse: Quotation marks seems to be messed up.");
      return jsonNode;
    }

    public virtual void Serialize(BinaryWriter aWriter)
    {
    }

    public void SaveToStream(Stream aData)
    {
      this.Serialize(new BinaryWriter(aData));
    }

    public void SaveToCompressedStream(Stream aData)
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public void SaveToCompressedFile(string aFileName)
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public string SaveToCompressedBase64()
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public void SaveToFile(string aFileName)
    {
      Directory.CreateDirectory(new FileInfo(aFileName).Directory.FullName);
      using (FileStream fileStream = File.OpenWrite(aFileName))
        this.SaveToStream((Stream) fileStream);
    }

    public string SaveToBase64()
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        this.SaveToStream((Stream) memoryStream);
        memoryStream.Position = 0L;
        return Convert.ToBase64String(memoryStream.ToArray());
      }
    }

    public static JSONNode Deserialize(BinaryReader aReader)
    {
      JSONBinaryTag jsonBinaryTag = (JSONBinaryTag) aReader.ReadByte();
      switch (jsonBinaryTag)
      {
        case JSONBinaryTag.Array:
          int num1 = aReader.ReadInt32();
          JSONArray jsonArray = new JSONArray();
          for (int index = 0; index < num1; ++index)
            jsonArray.Add(JSONNode.Deserialize(aReader));
          return (JSONNode) jsonArray;
        case JSONBinaryTag.Class:
          int num2 = aReader.ReadInt32();
          JSONClass jsonClass = new JSONClass();
          for (int index = 0; index < num2; ++index)
          {
            string aKey = aReader.ReadString();
            JSONNode aItem = JSONNode.Deserialize(aReader);
            jsonClass.Add(aKey, aItem);
          }
          return (JSONNode) jsonClass;
        case JSONBinaryTag.Value:
          return (JSONNode) new JSONData(aReader.ReadString());
        case JSONBinaryTag.IntValue:
          return (JSONNode) new JSONData(aReader.ReadInt32());
        case JSONBinaryTag.DoubleValue:
          return (JSONNode) new JSONData(aReader.ReadDouble());
        case JSONBinaryTag.BoolValue:
          return (JSONNode) new JSONData(aReader.ReadBoolean());
        case JSONBinaryTag.FloatValue:
          return (JSONNode) new JSONData(aReader.ReadSingle());
        default:
          throw new Exception("Error deserializing JSON. Unknown tag: " + (object) jsonBinaryTag);
      }
    }

    public static JSONNode LoadFromCompressedFile(string aFileName)
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public static JSONNode LoadFromCompressedStream(Stream aData)
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public static JSONNode LoadFromCompressedBase64(string aBase64)
    {
      throw new Exception("Can't use compressed functions. You need include the SharpZipLib and uncomment the define at the top of SimpleJSON");
    }

    public static JSONNode LoadFromStream(Stream aData)
    {
      using (BinaryReader aReader = new BinaryReader(aData))
        return JSONNode.Deserialize(aReader);
    }

    public static JSONNode LoadFromFile(string aFileName)
    {
      using (FileStream fileStream = File.OpenRead(aFileName))
        return JSONNode.LoadFromStream((Stream) fileStream);
    }

    public static JSONNode LoadFromBase64(string aBase64)
    {
      return JSONNode.LoadFromStream((Stream) new MemoryStream(Convert.FromBase64String(aBase64))
      {
        Position = 0L
      });
    }

    public static implicit operator JSONNode(string s)
    {
      return (JSONNode) new JSONData(s);
    }

    public static implicit operator string(JSONNode d)
    {
      if (d == (object) null)
        return (string) null;
      return d.Value;
    }

    public static bool operator ==(JSONNode a, object b)
    {
      if (b == null && (object) (a as JSONLazyCreator) != null)
        return true;
      return object.ReferenceEquals((object) a, b);
    }

    public static bool operator !=(JSONNode a, object b)
    {
      return !(a == b);
    }
  }
}
