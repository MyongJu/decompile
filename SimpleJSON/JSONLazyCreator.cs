﻿// Decompiled with JetBrains decompiler
// Type: SimpleJSON.JSONLazyCreator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace SimpleJSON
{
  internal class JSONLazyCreator : JSONNode
  {
    private JSONNode m_Node;
    private string m_Key;

    public JSONLazyCreator(JSONNode aNode)
    {
      this.m_Node = aNode;
      this.m_Key = (string) null;
    }

    public JSONLazyCreator(JSONNode aNode, string aKey)
    {
      this.m_Node = aNode;
      this.m_Key = aKey;
    }

    private void Set(JSONNode aVal)
    {
      if (this.m_Key == null)
        this.m_Node.Add(aVal);
      else
        this.m_Node.Add(this.m_Key, aVal);
      this.m_Node = (JSONNode) null;
    }

    public override JSONNode this[int aIndex]
    {
      get
      {
        return (JSONNode) new JSONLazyCreator((JSONNode) this);
      }
      set
      {
        JSONArray jsonArray = new JSONArray();
        jsonArray.Add(value);
        this.Set((JSONNode) jsonArray);
      }
    }

    public override JSONNode this[string aKey]
    {
      get
      {
        return (JSONNode) new JSONLazyCreator((JSONNode) this, aKey);
      }
      set
      {
        this.Set((JSONNode) new JSONClass()
        {
          {
            aKey,
            value
          }
        });
      }
    }

    public override void Add(JSONNode aItem)
    {
      JSONArray jsonArray = new JSONArray();
      jsonArray.Add(aItem);
      this.Set((JSONNode) jsonArray);
    }

    public override void Add(string aKey, JSONNode aItem)
    {
      this.Set((JSONNode) new JSONClass()
      {
        {
          aKey,
          aItem
        }
      });
    }

    public override bool Equals(object obj)
    {
      if (obj == null)
        return true;
      return object.ReferenceEquals((object) this, obj);
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public override string ToString()
    {
      return string.Empty;
    }

    public override string ToString(string aPrefix)
    {
      return string.Empty;
    }

    public override int AsInt
    {
      get
      {
        this.Set((JSONNode) new JSONData(0));
        return 0;
      }
      set
      {
        this.Set((JSONNode) new JSONData(value));
      }
    }

    public override float AsFloat
    {
      get
      {
        this.Set((JSONNode) new JSONData(0.0f));
        return 0.0f;
      }
      set
      {
        this.Set((JSONNode) new JSONData(value));
      }
    }

    public override double AsDouble
    {
      get
      {
        this.Set((JSONNode) new JSONData(0.0));
        return 0.0;
      }
      set
      {
        this.Set((JSONNode) new JSONData(value));
      }
    }

    public override bool AsBool
    {
      get
      {
        this.Set((JSONNode) new JSONData(false));
        return false;
      }
      set
      {
        this.Set((JSONNode) new JSONData(value));
      }
    }

    public override JSONArray AsArray
    {
      get
      {
        JSONArray jsonArray = new JSONArray();
        this.Set((JSONNode) jsonArray);
        return jsonArray;
      }
    }

    public override JSONClass AsObject
    {
      get
      {
        JSONClass jsonClass = new JSONClass();
        this.Set((JSONNode) jsonClass);
        return jsonClass;
      }
    }

    public static bool operator ==(JSONLazyCreator a, object b)
    {
      if (b == null)
        return true;
      return object.ReferenceEquals((object) a, b);
    }

    public static bool operator !=(JSONLazyCreator a, object b)
    {
      return !(a == b);
    }
  }
}
