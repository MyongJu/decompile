﻿// Decompiled with JetBrains decompiler
// Type: EquipmentBenefits
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class EquipmentBenefits : MonoBehaviour
{
  public UIGrid grid;
  public UIScrollView scrollView;
  public EquipmentBenefitComponent template;
  private List<EquipmentBenefitComponent> benefitsComponentList;

  public void Init(Benefits benefits)
  {
    this.ClearGrid();
    this.ClearEquipmentBenefitComponents();
    if (benefits != null)
    {
      List<Benefits.BenefitValuePair> benefitsPairs = benefits.GetBenefitsPairs();
      this.benefitsComponentList = new List<EquipmentBenefitComponent>();
      for (int index = 0; index < benefitsPairs.Count; ++index)
      {
        GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
        gameObject.SetActive(true);
        EquipmentBenefitComponent component = gameObject.GetComponent<EquipmentBenefitComponent>();
        component.FeedData((IComponentData) new EquipmentBenefitComponentData(benefitsPairs[index]));
        this.benefitsComponentList.Add(component);
      }
    }
    this.grid.repositionNow = true;
    this.grid.Reposition();
    if (!((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null))
      return;
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
  }

  public void Dispose()
  {
    if (this.benefitsComponentList == null)
      return;
    this.benefitsComponentList.ForEach((System.Action<EquipmentBenefitComponent>) (obj => obj.Dispose()));
  }

  private void ClearEquipmentBenefitComponents()
  {
    if (this.benefitsComponentList == null)
      return;
    for (int index = 0; index < this.benefitsComponentList.Count; ++index)
    {
      NGUITools.SetActive(this.benefitsComponentList[index].gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.benefitsComponentList[index]);
    }
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
