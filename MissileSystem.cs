﻿// Decompiled with JetBrains decompiler
// Type: MissileSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MissileSystem : MonoBehaviour
{
  public Vector3 angleOffset = Vector3.zero;
  public string shootStateName = "shoot";
  public float shootInterval = 1f;
  public Vector2 heightFactorRange = new Vector2(1f, 1.5f);
  public Vector2 animationSpeedRange = new Vector2(1f, 1f);
  public float assetScaleFactor = 1f;
  public string assetSortingLayer = "Default";
  private Matrix4x4 _offsetMatrix = new Matrix4x4();
  private List<MissileSet> _missileSets = new List<MissileSet>();
  public Animator normlizedAnimation;
  public GameObject missileAsset;
  public int maxAnimation;
  public int maxShootPerAnimation;
  public int assetSortingOrder;
  public bool faceToCamera;
  private bool _initialized;
  private GameObject _missileTemplate;
  private GameObjectPool _missilePool;
  private GameObjectPool _missileAssetPool;
  private int _randomSeed;

  public void Initialize()
  {
    if (this._initialized)
      return;
    this._missileTemplate = new GameObject();
    this._missileTemplate.SetActive(false);
    this._missileTemplate.name = "missile";
    this._missileTemplate.transform.parent = this.transform;
    this._missileTemplate.AddComponent<Missile>();
    this._missilePool = new GameObjectPool();
    this._missilePool.Initialize(this._missileTemplate, this.gameObject);
    this._missileAssetPool = new GameObjectPool();
    this._missileAssetPool.Initialize(this.missileAsset.gameObject, this.gameObject);
    this._offsetMatrix.SetTRS(Vector3.zero, Quaternion.Euler(this.angleOffset), Vector3.one);
    this.InitializeSet();
    this._initialized = true;
  }

  public void Dispose()
  {
    this._initialized = false;
    this.ClearSet();
    this.ClearPools();
    this._missilePool = (GameObjectPool) null;
    this._missileAssetPool = (GameObjectPool) null;
    Object.Destroy((Object) this._missileTemplate);
    this._missileTemplate = (GameObject) null;
    Object.Destroy((Object) this.gameObject);
  }

  private void Awake()
  {
    this.Initialize();
  }

  private void Update()
  {
    this.UpdateSet();
  }

  private void InitializeSet()
  {
    for (int index = 0; index < this.maxAnimation; ++index)
      this._missileSets.Add(new MissileSet(this)
      {
        ShootStateName = this.shootStateName,
        ShootInterval = this.shootInterval,
        MaxShoot = this.maxShootPerAnimation
      });
  }

  private void UpdateSet()
  {
    for (int index = 0; index < this._missileSets.Count; ++index)
      this._missileSets[index].Update();
  }

  private void ClearSet()
  {
    for (int index = 0; index < this._missileSets.Count; ++index)
      this._missileSets[index].Dispose();
    this._missileSets.Clear();
  }

  private MissileSet GetRandomSet()
  {
    if (this._missileSets.Count <= 0)
      return (MissileSet) null;
    return this._missileSets[++this._randomSeed % this._missileSets.Count];
  }

  public void Shoot(GameObject source, GameObject target, int tag = 0)
  {
    if ((Object) source == (Object) null || (Object) target == (Object) null)
      return;
    this.Shoot(source.transform.position, target.transform.position, tag);
  }

  public void Shoot(Vector3 source, Vector3 target, int tag = 0)
  {
    MissileSet randomSet = this.GetRandomSet();
    if (randomSet == null)
      return;
    randomSet.Shoot(source, target, tag);
  }

  public void Stop(int tag = 0)
  {
    for (int index = 0; index < this._missileSets.Count; ++index)
      this._missileSets[index].Stop(tag);
  }

  public void ClearMissiles()
  {
    for (int index = 0; index < this._missileSets.Count; ++index)
      this._missileSets[index].Clear();
  }

  public void ClearPools()
  {
    if (this._missileAssetPool != null)
      this._missileAssetPool.Clear();
    if (this._missilePool == null)
      return;
    this._missilePool.Clear();
  }

  public Animator CreateAnimation()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.normlizedAnimation.gameObject);
    gameObject.transform.parent = this.transform;
    Animator component = gameObject.GetComponent<Animator>();
    component.enabled = false;
    return component;
  }

  public float GenerateRandomAnimationSpeed()
  {
    return Random.Range(this.animationSpeedRange.x, this.animationSpeedRange.y);
  }

  public void DestoryAnimation(Animator animtion)
  {
    if ((Object) null == (Object) animtion)
      return;
    Object.Destroy((Object) animtion.gameObject);
  }

  public Missile CreateMissile()
  {
    if (this._missilePool == null)
      return (Missile) null;
    GameObject gameObject = this._missilePool.Allocate();
    if ((Object) gameObject == (Object) null)
      return (Missile) null;
    gameObject.transform.parent = this.transform;
    Missile missile = gameObject.GetComponent<Missile>();
    if ((Object) missile == (Object) null)
      missile = gameObject.AddComponent<Missile>();
    missile.heightScaleFactor = Random.Range(this.heightFactorRange.x, this.heightFactorRange.y);
    missile.system = this;
    return missile;
  }

  public void DestoryMissile(Missile missile)
  {
    if ((Object) null == (Object) missile)
      return;
    missile.Clear();
    this._missilePool.Release(missile.gameObject);
  }

  public GameObject CreateMissileAsset()
  {
    GameObject gameObject = this._missileAssetPool.Allocate();
    gameObject.layer = MissileSystemManager.RendererLayer;
    return gameObject;
  }

  public void DestoryMissileAsset(GameObject missileAsset)
  {
    if ((Object) null == (Object) missileAsset)
      return;
    this._missileAssetPool.Release(missileAsset);
  }

  public Vector3 OffsetVector3(Vector3 vector)
  {
    return this._offsetMatrix.MultiplyVector(vector);
  }
}
