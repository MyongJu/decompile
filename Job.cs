﻿// Decompiled with JetBrains decompiler
// Type: Job
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class Job
{
  public System.Action OnFinished;
  public object Data;
  public bool ByClient;
  public long uid;
  private JobEvent m_Event;
  private int m_StartTime;
  private int m_EndTime;
  private int m_CurrentTime;
  private long m_Id;
  private long m_ServerJobId;

  public long ServerJobID
  {
    get
    {
      return this.m_ServerJobId;
    }
    set
    {
      this.m_ServerJobId = value;
    }
  }

  public bool IsTrueJob
  {
    get
    {
      return !this.ByClient;
    }
  }

  public bool IsFakeJob
  {
    get
    {
      if (this.ByClient)
        return this.m_ServerJobId == 0L;
      return false;
    }
  }

  public bool IsRealJob
  {
    get
    {
      if (this.ByClient)
        return this.m_ServerJobId != 0L;
      return false;
    }
  }

  public override string ToString()
  {
    return string.Format("[Job ID={0}] Event={1}, TimeLeft={2}", (object) this.m_Id, (object) this.m_Event.ToString(), (object) this.LeftTime());
  }

  public void Reset(JobEvent evt, int startTime, int endTime, System.Action callback, long id)
  {
    this.m_Event = evt;
    this.m_StartTime = startTime;
    this.m_EndTime = endTime;
    this.OnFinished = callback;
    this.m_Id = id;
  }

  public void Reset(int startTime, int endTime)
  {
    this.m_StartTime = startTime <= 0 ? this.m_StartTime : startTime;
    this.m_EndTime = endTime <= 0 ? this.m_EndTime : endTime;
  }

  public void Reset(JobEvent evt, int startTime, int endTime)
  {
    this.m_Event = evt;
    this.m_StartTime = startTime;
    this.m_EndTime = endTime;
  }

  public void Update(int timestamp)
  {
    this.m_CurrentTime = timestamp;
  }

  public long GetId()
  {
    return this.m_Id;
  }

  public bool IsFinished()
  {
    return this.LeftTime() <= 0;
  }

  public void Dispose()
  {
    this.m_StartTime = 0;
    this.m_EndTime = 0;
    this.m_ServerJobId = 0L;
    this.Data = (object) null;
    this.OnFinished = (System.Action) null;
  }

  public JobEvent GetJobEvent()
  {
    return this.m_Event;
  }

  public bool Speedup(int dt)
  {
    if (this.IsFinished())
      return false;
    this.m_EndTime -= dt;
    return true;
  }

  public int StartTime()
  {
    return this.m_StartTime;
  }

  public int EndTime()
  {
    return this.m_EndTime;
  }

  public int Duration()
  {
    return this.m_EndTime - this.m_StartTime;
  }

  public int LeftTime()
  {
    return this.m_EndTime - NetServerTime.inst.ServerTimestamp;
  }

  public double LeftTimeInMillisecond()
  {
    return (double) this.m_EndTime - NetServerTime.inst.UpdateTime;
  }

  public bool IsHelped()
  {
    bool result = false;
    Hashtable data = this.Data as Hashtable;
    if (data != null && data.Contains((object) "helped"))
      bool.TryParse(data[(object) "helped"].ToString(), out result);
    return result;
  }

  public long MarchID
  {
    get
    {
      long result = 0;
      Hashtable data = this.Data as Hashtable;
      if (data != null && data.Contains((object) "march_id"))
        long.TryParse(data[(object) "march_id"].ToString(), out result);
      return result;
    }
  }

  public long RallyID
  {
    get
    {
      long result = -1;
      Hashtable data = this.Data as Hashtable;
      if (data != null && data.Contains((object) "rally_id"))
        long.TryParse(data[(object) "rally_id"].ToString(), out result);
      return result;
    }
  }
}
