﻿// Decompiled with JetBrains decompiler
// Type: WonderSettingCitySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WonderSettingCitySlot : MonoBehaviour
{
  public System.Action<int> onButtonClick;
  public int index;
  public UITexture icon;
  public Transform checker;
  public UIButton BT_checker;

  public void SetEnable(bool flag)
  {
    this.BT_checker.isEnabled = flag;
  }

  public void Selected(bool flag = true)
  {
    this.checker.gameObject.SetActive(flag);
  }

  public void Setup(int inIndex, string iconPath)
  {
    this.index = inIndex;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, iconPath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnButtonClick()
  {
    if (this.onButtonClick == null)
      return;
    this.onButtonClick(this.index);
  }

  public void Reset()
  {
    this.icon = this.transform.FindChild("Building").GetComponent<UITexture>();
    this.checker = this.transform.FindChild("Tick");
    this.BT_checker = this.GetComponent<UIButton>();
  }
}
