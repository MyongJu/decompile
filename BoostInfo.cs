﻿// Decompiled with JetBrains decompiler
// Type: BoostInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoostInfo : MonoBehaviour
{
  public UILabel name;
  public UITexture icon;
  public UILabel enhance;

  public void SeedData(BoostInfo.Data d)
  {
    this.name.text = d.name;
    string str1 = (double) d.enhance <= 0.0 ? " " : "+";
    if (d.ispercent)
    {
      string str2 = d.enhance.ToString("P").Replace(".00", string.Empty);
      this.enhance.text = str1 + str2;
    }
    else
      this.enhance.text = str1 + (object) d.enhance;
    this.SetHigherArrow(d.isHigher, d.showArrow);
  }

  public void SetHigherArrow(bool isHigher, bool isShow)
  {
    Transform transform1 = this.transform.Find("arrow");
    if ((bool) ((Object) transform1))
      NGUITools.SetActive(transform1.gameObject, isHigher && isShow);
    Transform transform2 = this.transform.Find("arrow_down");
    if (!(bool) ((Object) transform2))
      return;
    NGUITools.SetActive(transform2.gameObject, !isHigher && isShow);
  }

  public class Data
  {
    public bool ispercent = true;
    public string name;
    public string icon;
    public float enhance;
    public int pri;
    public bool isHigher;
    public bool showArrow;

    public Data()
    {
    }

    public Data(BoostInfo.Data data)
    {
      this.name = data.name;
      this.icon = data.icon;
      this.enhance = data.enhance;
      this.pri = data.pri;
      this.ispercent = data.ispercent;
    }
  }
}
