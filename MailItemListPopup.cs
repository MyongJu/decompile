﻿// Decompiled with JetBrains decompiler
// Type: MailItemListPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MailItemListPopup : Popup
{
  private List<GameObject> m_ItemList = new List<GameObject>();
  public GameObject m_ItemPrefab;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  private MailItemListPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as MailItemListPopup.Parameter;
    this.UpdateUI();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnItemClicked(ConsumableItemData itemData)
  {
    this.OnClosePressed();
    if (this.m_Parameter.callback == null)
      return;
    this.m_Parameter.callback(itemData);
  }

  private void UpdateUI()
  {
    Dictionary<long, ConsumableItemData>.Enumerator enumerator = DBManager.inst.DB_Item.Datas.GetEnumerator();
    while (enumerator.MoveNext())
    {
      ItemStaticInfo itemInfo = ConfigManager.inst.DB_Items.GetItem((int) enumerator.Current.Key);
      if (itemInfo != null && itemInfo.isPost == 1)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
        gameObject.SetActive(true);
        gameObject.transform.parent = this.m_Grid.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localScale = Vector3.one;
        gameObject.GetComponent<MailItemListRenderer>().SetData(enumerator.Current.Value, itemInfo, new System.Action<ConsumableItemData>(this.OnItemClicked));
        this.m_ItemList.Add(gameObject);
      }
    }
    this.m_Grid.sorting = UIGrid.Sorting.Custom;
    this.m_Grid.onCustomSort = new Comparison<Transform>(this.OnCustomSort);
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private int OnCustomSort(Transform x, Transform y)
  {
    return x.GetComponent<MailItemListRenderer>().ItemInfo.Priority - y.GetComponent<MailItemListRenderer>().ItemInfo.Priority;
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action<ConsumableItemData> callback;
  }
}
