﻿// Decompiled with JetBrains decompiler
// Type: CasinoConfirmationPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class CasinoConfirmationPopup : Popup
{
  private int type = -1;
  public UILabel panelTitle;
  public UILabel panelDetails;
  public UILabel okayBtnText;
  public UILabel shareAllianceBtnText;
  public UILabel shareKingdomBtnText;
  public GameObject okayBtn;
  public GameObject shareAllianceBtn;
  public GameObject shareKingdomBtn;
  private System.Action onConfirmBtnPressedHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow((UIControler.UIParameter) null);
    CasinoConfirmationPopup.Parameter parameter = orgParam as CasinoConfirmationPopup.Parameter;
    if (parameter != null)
    {
      this.type = parameter.type;
      this.onConfirmBtnPressedHandler = parameter.onConfirmBtnPressedEvent;
    }
    this.ShowPanelDetails();
  }

  public void ShowPanelDetails()
  {
    if (!this.isPlayerInAlliance())
    {
      this.shareAllianceBtn.GetComponent<UIButton>().isEnabled = false;
      this.shareAllianceBtn.GetComponent<UIButton>().UpdateColor(true);
    }
    switch (this.type)
    {
      case 0:
        this.panelTitle.text = Utils.XLAT("tavern_uppercase_share");
        this.panelDetails.text = Utils.XLAT("tavern_rewards_share_description");
        this.shareAllianceBtnText.text = Utils.XLAT("chat_alliance_chat_title");
        this.shareKingdomBtnText.text = Utils.XLAT("chat_kingdom_chat_title");
        this.okayBtn.SetActive(false);
        this.shareAllianceBtn.SetActive(true);
        this.shareKingdomBtn.SetActive(true);
        break;
      case 1:
        this.panelTitle.text = Utils.XLAT("tavern_cards_rewards_give_up_title");
        this.panelDetails.text = Utils.XLAT("tavern_cards_rewards_give_up_description");
        this.okayBtnText.text = Utils.XLAT("id_uppercase_okay");
        break;
    }
  }

  public void OnOkayBtnPressed()
  {
    if (this.type == 1)
      MessageHub.inst.GetPortByAction("casino:closeChest").SendRequest((Hashtable) null, new System.Action<bool, object>(this.OnGiveupChestCallback), true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnShareAllianceBtnPressed()
  {
    CasinoFunHelper.Instance.ShareCasinoCardDetails(false);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnShareKingdomBtnPressed()
  {
    CasinoFunHelper.Instance.ShareCasinoCardDetails(true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private bool isPlayerInAlliance()
  {
    return PlayerData.inst.allianceData != null && PlayerData.inst.allianceData.members != null && PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid) != null;
  }

  private void OnGiveupChestCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.OpenDlg("Casino/CasinoDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    if (this.onConfirmBtnPressedHandler == null)
      return;
    this.onConfirmBtnPressedHandler();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int type;
    public System.Action onConfirmBtnPressedEvent;
  }
}
