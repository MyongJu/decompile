﻿// Decompiled with JetBrains decompiler
// Type: TroopDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;

public class TroopDetail
{
  public string uid;
  public string name;
  public string acronym;
  public string portrait;
  public string icon;
  public int lord_title;
  public int lord_level;
  public List<Hashtable> legends;
  public Dictionary<string, string> kill_troops;
  public Dictionary<string, string> start_troops;
  public Dictionary<string, string> end_troops;
  public Dictionary<string, string> wounded_troops;
  public Hashtable dragon;
  public string legend_id;
  public List<string> skills;
  private List<long> skillsInt64;

  public long LegendInt64
  {
    get
    {
      if (this.legend_id == null)
        return 0;
      return long.Parse(this.legend_id);
    }
  }

  public List<long> SkillsInt64
  {
    get
    {
      if (this.skills == null)
        return (List<long>) null;
      if (this.skillsInt64 != null)
        return this.skillsInt64;
      this.skillsInt64 = new List<long>();
      using (List<string>.Enumerator enumerator = this.skills.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.skillsInt64.Add(long.Parse(enumerator.Current));
      }
      return this.skillsInt64;
    }
  }

  public long CalculatePowerLost()
  {
    if (this.start_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.start_troops.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        string s = (string) null;
        if (this.end_troops != null)
          this.end_troops.TryGetValue(current, out s);
        if (s == null)
          s = "0";
        Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current);
        if (data != null)
          num += (long.Parse(this.start_troops[current]) - long.Parse(s)) * (long) data.Power;
      }
    }
    return num;
  }

  public long CalculateTroopLost()
  {
    if (this.start_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.start_troops.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        string s1 = (string) null;
        string s2 = (string) null;
        if (this.end_troops != null)
          this.end_troops.TryGetValue(current, out s1);
        if (this.wounded_troops != null)
          this.wounded_troops.TryGetValue(current, out s2);
        num += long.Parse(this.start_troops[current]);
        if (s1 != null)
          num -= long.Parse(s1);
        if (s2 != null)
          num -= long.Parse(s2);
      }
    }
    return num;
  }

  public long CalculateTroop()
  {
    if (this.start_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.ValueCollection.Enumerator enumerator = this.start_troops.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        num += long.Parse(current);
      }
    }
    return num;
  }

  public long CalculateTroopWounded()
  {
    if (this.wounded_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.ValueCollection.Enumerator enumerator = this.wounded_troops.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        num += long.Parse(current);
      }
    }
    return num;
  }

  public long CalculateTroopSurvived()
  {
    if (this.end_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.ValueCollection.Enumerator enumerator = this.end_troops.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        num += long.Parse(current);
      }
    }
    return num;
  }

  public long CalculateTroopKill()
  {
    if (this.kill_troops == null)
      return 0;
    long num = 0;
    using (Dictionary<string, string>.ValueCollection.Enumerator enumerator = this.kill_troops.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        num += long.Parse(current);
      }
    }
    return num;
  }

  public void SummarizeTroopDetail(ref Dictionary<string, List<SoldierInfo.Data>> troopdetail)
  {
    troopdetail.Clear();
    if (this.start_troops == null)
      return;
    using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.start_troops.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        Unit_StatisticsInfo data1 = ConfigManager.inst.DB_Unit_Statistics.GetData(current);
        if (data1 != null)
        {
          string type = data1.Type;
          SoldierInfo.Data data2 = new SoldierInfo.Data();
          data2.icon = data1.Troop_ICON;
          data2.name = ScriptLocalization.Get(data1.Troop_Name_LOC_ID, true);
          data2.level = Utils.GetRomaNumeralsBelowTen(data1.Troop_Tier);
          data2.priority = data1.Priority;
          string s1 = (string) null;
          string s2 = (string) null;
          string s3 = (string) null;
          if (this.end_troops != null)
            this.end_troops.TryGetValue(current, out s1);
          if (s1 == null)
            s1 = "0";
          if (this.wounded_troops != null)
            this.wounded_troops.TryGetValue(current, out s2);
          if (s2 == null)
            s2 = "0";
          if (this.kill_troops != null)
            this.kill_troops.TryGetValue(current, out s3);
          if (s3 == null)
            s3 = "0";
          data2.dead = long.Parse(this.start_troops[current]) - long.Parse(s1) - long.Parse(s2);
          data2.survived = long.Parse(s1);
          data2.wounded = long.Parse(s2);
          data2.kill = long.Parse(s3);
          if (troopdetail.ContainsKey(type))
            troopdetail[type].Add(data2);
          else
            troopdetail.Add(type, new List<SoldierInfo.Data>()
            {
              data2
            });
        }
      }
    }
    using (Dictionary<string, List<SoldierInfo.Data>>.Enumerator enumerator = troopdetail.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Sort((Comparison<SoldierInfo.Data>) ((s1, s2) => s2.priority - s1.priority));
    }
  }
}
