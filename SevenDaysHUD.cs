﻿// Decompiled with JetBrains decompiler
// Type: SevenDaysHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class SevenDaysHUD : MonoBehaviour
{
  public GameObject rootNode;
  public GameObject redPoint;
  public UILabel remainTime;

  private void Start()
  {
    this.rootNode.SetActive(false);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int day1 = 7;
    int totalDays = DBManager.inst.DB_SevenDays.TotalDays;
    if (totalDays < day1)
    {
      NGUITools.SetActive(this.rootNode, false);
    }
    else
    {
      int day2 = 1;
      bool state1 = false;
      bool state2 = false;
      for (int index = 0; index < totalDays; ++index)
      {
        bool flag = DBManager.inst.DB_SevenDays.IsFinih(day2);
        state2 = DBManager.inst.DB_SevenQuests.IsFinish(day2);
        if (flag || state2)
        {
          state1 = true;
          break;
        }
        ++day2;
      }
      NGUITools.SetActive(this.redPoint.gameObject, state1);
      SevenDaysData sevenDaysData = DBManager.inst.DB_SevenDays.GetSevenDaysData(day1);
      if (sevenDaysData != null && sevenDaysData.IsExpired)
      {
        NGUITools.SetActive(this.rootNode, false);
        NGUITools.SetActive(this.rootNode, state2);
      }
      else
        NGUITools.SetActive(this.rootNode, true);
      this.RefreshTime();
    }
  }

  private void RefreshTime()
  {
    SevenDaysData sevenDaysData1 = DBManager.inst.DB_SevenDays.GetSevenDaysData(7);
    if (sevenDaysData1 == null)
      return;
    if (sevenDaysData1.IsExpired)
    {
      this.remainTime.text = ScriptLocalization.Get("id_finished", true);
    }
    else
    {
      int currentDay = DBManager.inst.DB_SevenDays.GetCurrentDay();
      int num1 = 7;
      int num2 = 86400;
      SevenDaysData sevenDaysData2 = DBManager.inst.DB_SevenDays.GetSevenDaysData(currentDay);
      if (sevenDaysData2 != null)
        num2 = sevenDaysData2.RemainTime;
      int time = num2 + (num1 - currentDay) * 86400;
      if (time <= 0)
      {
        this.remainTime.text = ScriptLocalization.Get("id_finished", true);
        if (!Oscillator.IsAvailable)
          return;
        Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
      }
      else
        this.remainTime.text = Utils.FormatTime(time, true, false, true);
    }
  }

  public void OnClickHandler()
  {
    UIManager.inst.OpenDlg("Activity/SevenDaysEventDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnEnable()
  {
    this.AddEventHandler();
  }

  private void OnDataChange(long configId)
  {
    this.UpdateUI();
  }

  private void OnSecondEvent(int time)
  {
    this.RefreshTime();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged += new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_SevenQuests.onDataChanged += new System.Action<long>(this.OnDataChange);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_SevenDays.onDataChanged -= new System.Action<long>(this.OnDataChange);
    DBManager.inst.DB_SevenQuests.onDataChanged -= new System.Action<long>(this.OnDataChange);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    this.RemoveEventHandler();
  }
}
