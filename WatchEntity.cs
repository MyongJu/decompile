﻿// Decompiled with JetBrains decompiler
// Type: WatchEntity
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public struct WatchEntity : IComparable<WatchEntity>
{
  public WatchType m_Type;
  public long m_Id;

  public WatchEntity(WatchType type, long id)
  {
    this.m_Type = type;
    this.m_Id = id;
  }

  public bool IsDefaultIgnore()
  {
    Color color = this.GetColor();
    if (!(color == WatchtowerUtilities.REINFORCE_COLOR))
      return color == WatchtowerUtilities.TRADE_COLOR;
    return true;
  }

  public bool IsEmpty()
  {
    return this.m_Type != WatchType.March ? DBManager.inst.DB_Rally.Get(this.m_Id) == null : DBManager.inst.DB_March.Get(this.m_Id) == null;
  }

  public Color GetColor()
  {
    Color color = WatchtowerUtilities.UNKNOWN_COLOR;
    UserWatchTowerData dataById = DBManager.inst.DB_WatchTower.GetDataById(this.m_Id);
    if (dataById != null)
    {
      switch (dataById.Color)
      {
        case 1:
          color = WatchtowerUtilities.ATTACK_COLOR;
          break;
        case 2:
          color = WatchtowerUtilities.REINFORCE_COLOR;
          break;
        case 3:
          color = WatchtowerUtilities.TRADE_COLOR;
          break;
      }
    }
    return color;
  }

  public int CompareTo(WatchEntity compareEntity)
  {
    UserWatchTowerData dataById1 = DBManager.inst.DB_WatchTower.GetDataById(this.m_Id);
    UserWatchTowerData dataById2 = DBManager.inst.DB_WatchTower.GetDataById(compareEntity.m_Id);
    if (dataById1.UrgentLevel == dataById2.UrgentLevel)
      return dataById1.EventId.CompareTo(dataById2.EventId);
    return dataById1.UrgentLevel.CompareTo(dataById2.UrgentLevel);
  }
}
