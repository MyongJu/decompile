﻿// Decompiled with JetBrains decompiler
// Type: ConstructionBuildingStats
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UnityEngine;

public class ConstructionBuildingStats : MonoBehaviour
{
  private ArrayList _requirements = new ArrayList();
  private string _buildingClass = string.Empty;
  public UITexture mBuildingIcon;
  public UILabel mBuildingLevel;
  public UILabel mConstructionTimeNormal;
  public UILabel mConstructionTimeBoost;
  public UILabel mInstantBuildValue;
  public UILabel mDialogTitle;
  public GameObject mQueueFullReqBtn;
  public GameObject mBuildingReqBtn;
  public GameObject mWoodReqBtn;
  public GameObject mFoodReqBtn;
  public GameObject mSilverReqBtn;
  public GameObject mOreReqBtn;
  public GameObject mItemReqBtn;
  public GameObject mBuildLimitReqBtn;
  public GameObject mRewardHeroXP;
  public GameObject mRewardPower;
  public GameObject mRewardDefault;
  public GameObject mRequirementsPage;
  public GameObject mRewardsPage;
  public UIButton mBuildBtn;
  public UIButton mInstBuidBtn;
  public Hashtable mInstBuildDetails;
  public GameObject mQueueSpeedupPopup;
  public PageTabsMonitor PTM;
  private GameObject _targetPlot;
  private System.Action _onFinished;
  private int _instBuildAmount;
  private bool bQueueIsFull;
  private float _requirementsY;
  private float _rewardsY;
  private CityManager.BuildingItem _buildingItem;

  public void SetDetails(CityManager.BuildingItem buildingItem, string iconStr, string buildingLevel, double normalSeconds, double boostSeconds, int instBuildAmount, string buildingName, GameObject targetPlot, int iLevel, string buildingClass, System.Action onFinished)
  {
    this._buildingItem = buildingItem;
    this._targetPlot = targetPlot;
    this._buildingClass = buildingClass;
    this._onFinished = onFinished;
    this._instBuildAmount = instBuildAmount;
    this.bQueueIsFull = false;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.mBuildingIcon, "Prefab/UI/BuildingPrefab/ui_" + iconStr, (System.Action<bool>) null, true, false, string.Empty);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      int width = 0;
      int height = 0;
      CityBuildingDims.Get(iconStr, out width, out height);
      float num = 600f / (float) width;
      this.mBuildingIcon.width = (int) ((double) width * (double) num);
      this.mBuildingIcon.height = (int) ((double) height * (double) num);
    }));
    this.mBuildingLevel.text = buildingLevel;
    this.mConstructionTimeNormal.text = Utils.ConvertSecsToString(normalSeconds);
    this.mConstructionTimeBoost.text = Utils.ConvertSecsToString(boostSeconds);
    this.mInstantBuildValue.text = Utils.FormatThousands(instBuildAmount.ToString());
    this.mDialogTitle.text = ScriptLocalization.Get(buildingName, true);
    this.PTM.onTabSelected += new System.Action<int>(this.OnTabSelected);
    this.PTM.SetCurrentTab(0, true);
    this.ClearRequirements();
  }

  public void SetQueueFull()
  {
    this.bQueueIsFull = true;
  }

  public void SetQueueTimer(BuildDlg.TrainTimerBar timer)
  {
  }

  public GameObject AddRequirement(ConstructionBuildingStats.Requirement requirement, string icon, string name, double amount, double need)
  {
    GameObject gob = (GameObject) null;
    switch (requirement)
    {
      case ConstructionBuildingStats.Requirement.FOOD:
        gob = Utils.DuplicateGOB(this.mFoodReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.WOOD:
        gob = Utils.DuplicateGOB(this.mWoodReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.SILVER:
        gob = Utils.DuplicateGOB(this.mSilverReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.ORE:
        gob = Utils.DuplicateGOB(this.mOreReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.BUILDING:
        gob = Utils.DuplicateGOB(this.mBuildingReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.ITEM:
        gob = Utils.DuplicateGOB(this.mItemReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.QUEUE:
        gob = Utils.DuplicateGOB(this.mQueueFullReqBtn);
        break;
      case ConstructionBuildingStats.Requirement.LIMIT:
        gob = Utils.DuplicateGOB(this.mBuildLimitReqBtn);
        break;
    }
    gob.SetActive(true);
    Utils.SetLPY(gob, this._requirementsY);
    this._requirementsY -= (float) gob.GetComponent<UIWidget>().height;
    ConstructionRequirementBtn component = gob.GetComponent<ConstructionRequirementBtn>();
    switch (requirement - 1)
    {
      case ConstructionBuildingStats.Requirement.NONE:
      case ConstructionBuildingStats.Requirement.FOOD:
      case ConstructionBuildingStats.Requirement.WOOD:
      case ConstructionBuildingStats.Requirement.SILVER:
        component.SetDetails((string) null, Utils.ConvertNumberToNormalString((long) need) + "/" + Utils.ConvertNumberToNormalString((long) amount), (string) null, amount >= need, true);
        break;
      case ConstructionBuildingStats.Requirement.ORE:
        component.SetDetails(icon, "LV." + need.ToString(), name, amount >= need, false);
        break;
      case ConstructionBuildingStats.Requirement.BUILDING:
        ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(int.Parse(name));
        component.SetDetails(icon, amount.ToString() + "/" + need.ToString(), dataByInternalId.LOC_ID, amount >= need, true);
        component.SetShopInfo(dataByInternalId);
        component.SetRequirement(amount, need);
        break;
      case ConstructionBuildingStats.Requirement.ITEM:
        component.SetDetails((string) null, Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need), amount >= need ? Utils.XLAT("forge_uppercase_queue_full") : "QUEUE", amount < need, false);
        break;
      case ConstructionBuildingStats.Requirement.QUEUE:
        component.SetDetails((string) null, Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need), amount >= need ? "LIMIT FULL" : "LIMIT", amount < need, false);
        break;
    }
    this._requirements.Add((object) gob);
    return gob;
  }

  public GameObject AddRewards(ConstructionBuildingStats.Rewards reward, string icon, string name, double amount)
  {
    GameObject gob = (GameObject) null;
    switch (reward)
    {
      case ConstructionBuildingStats.Rewards.HERO_XP:
        gob = Utils.DuplicateGOB(this.mRewardHeroXP);
        break;
      case ConstructionBuildingStats.Rewards.POWER:
        gob = Utils.DuplicateGOB(this.mRewardPower);
        break;
      case ConstructionBuildingStats.Rewards.DEFAULT:
        gob = Utils.DuplicateGOB(this.mRewardDefault);
        break;
    }
    gob.SetActive(true);
    Utils.SetLPY(gob, this._rewardsY);
    this._rewardsY -= (float) gob.GetComponent<UIWidget>().height;
    ConstructionRewardBtn component = gob.GetComponent<ConstructionRewardBtn>();
    switch (reward - 1)
    {
      case ConstructionBuildingStats.Rewards.NONE:
        component.SetDetails((string) null, "+" + Utils.ConvertNumberToNormalString((long) amount), (string) null);
        break;
      case ConstructionBuildingStats.Rewards.HERO_XP:
        component.SetDetails((string) null, "+" + Utils.ConvertNumberToNormalString((long) amount), (string) null);
        break;
      case ConstructionBuildingStats.Rewards.POWER:
        PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[int.Parse(name)];
        component.SetDetails((string) null, "+" + Utils.ConvertNumberToNormalString((long) amount), dbProperty.Name);
        break;
    }
    this._requirements.Add((object) gob);
    return gob;
  }

  public void ClearRequirements()
  {
    foreach (UnityEngine.Object requirement in this._requirements)
      UnityEngine.Object.Destroy(requirement);
    this._requirements.Clear();
    this._requirementsY = this.mQueueFullReqBtn.transform.localPosition.y;
    this._rewardsY = this.mRewardHeroXP.transform.localPosition.y;
  }

  public void OnCloseBtnPressed()
  {
    this.ClearRequirements();
    if (this._buildingItem != null)
      this._onFinished();
    else
      this.gameObject.SetActive(false);
  }

  private void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
      case 0:
        this.mRequirementsPage.SetActive(true);
        this.mRewardsPage.SetActive(false);
        break;
      case 1:
        this.mRequirementsPage.SetActive(false);
        this.mRewardsPage.SetActive(true);
        break;
    }
  }

  public void OnQueueRequirementPressed()
  {
  }

  public void OnBuildingRequirementPressed()
  {
  }

  public void OnItemRequirementPressed()
  {
  }

  public void OnResourceRequirementPressed()
  {
  }

  public void OnGetMoreFoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD);
  }

  public void OnGetMoreWoodBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD);
  }

  public void OnGetMoreSilverBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
  }

  public void OnGetMoreOreBtnPressed()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE);
  }

  public void GetMoreOfItemPressed()
  {
    ConstructionRequirementBtn component = UICamera.selectedObject.transform.parent.GetComponent<ConstructionRequirementBtn>();
    GameEngine.Instance.events.Publish_onItemClicked(component.GetShopInfo(), component.GetAmount(), component.GetNeeded());
  }

  public void OnQueueSpeedUpConfirmed()
  {
    this.OnQueueSpeedUpClosed();
    CityManager.BuildingItem fluxBuilding = CityManager.inst.FindFluxBuilding();
    if (fluxBuilding != null)
      CitadelSystem.inst.GetBuildControllerFromBuildingItem(fluxBuilding).transform.GetComponentInChildren<CityTimerbar>().OnSpeedUpBtnPressed();
    else
      D.warn((object) "No construction happening.");
  }

  public void OnQueueSpeedUpClosed()
  {
    this.mQueueSpeedupPopup.SetActive(false);
  }

  public void OnBuildPressed()
  {
    if (this.bQueueIsFull)
      this.mQueueSpeedupPopup.SetActive(true);
    else if (this._buildingItem != null)
    {
      CityManager.inst.UpgradeBuilding(this._buildingItem.mID, false, 0.0, 0);
      Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      this._onFinished();
    }
    else
    {
      CityManager.inst.CreateBuilding(this._buildingClass, this._targetPlot.GetComponent<CityPlotController>().SlotId, 0, false, 0.0, 0);
      Utils.FindClass<CitadelSystem>().OnCityMapClicked();
      this._onFinished();
    }
  }

  public void OnInstBuildPressed()
  {
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= this._instBuildAmount)
    {
      if (this._buildingItem != null)
      {
        CityManager.inst.UpgradeBuilding(this._buildingItem.mID, true, 0.0, 0);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
        this._onFinished();
      }
      else
      {
        CityManager.inst.CreateBuilding(this._buildingClass, this._targetPlot.GetComponent<CityPlotController>().SlotId, 0, true, 0.0, 0);
        Utils.FindClass<CitadelSystem>().OnCityMapClicked();
        this._onFinished();
      }
    }
    else
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.GOLD);
  }

  public enum Requirement
  {
    NONE,
    FOOD,
    WOOD,
    SILVER,
    ORE,
    BUILDING,
    ITEM,
    QUEUE,
    LIMIT,
  }

  public enum Rewards
  {
    NONE,
    HERO_XP,
    POWER,
    DEFAULT,
  }
}
