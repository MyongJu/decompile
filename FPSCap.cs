﻿// Decompiled with JetBrains decompiler
// Type: FPSCap
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FPSCap : MonoBehaviour
{
  public int frameRate = 30;
  private float oldTime;
  private float theDeltaTime;
  private float curTime;
  private float timeTaken;

  private void Start()
  {
    this.theDeltaTime = 1f / (float) this.frameRate;
    this.oldTime = Time.realtimeSinceStartup;
    Object.DontDestroyOnLoad((Object) this);
  }

  private void LateUpdate()
  {
    this.curTime = Time.realtimeSinceStartup;
    this.timeTaken = this.curTime - this.oldTime;
    if ((double) this.timeTaken >= (double) this.theDeltaTime)
      ;
    this.oldTime = Time.realtimeSinceStartup;
  }
}
