﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_Bakedmesh
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_Bakedmesh : MonoBehaviour
{
  public SkinnedMeshRenderer skin;

  private void Start()
  {
    this.skin = this.transform.GetComponent<SkinnedMeshRenderer>();
  }

  private void Update()
  {
    if (!(bool) ((UnityEngine.Object) this.skin))
      return;
    Mesh mesh = new Mesh();
    this.skin.BakeMesh(mesh);
    int index = 0;
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        Transform current = (Transform) enumerator.Current;
        if (!float.IsNaN(mesh.vertices[index].x))
          current.localPosition = mesh.vertices[index];
        else
          Debug.LogWarning((object) ("vertex " + (object) index + " is corrupted"));
        ++index;
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) mesh);
  }
}
