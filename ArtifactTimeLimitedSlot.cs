﻿// Decompiled with JetBrains decompiler
// Type: ArtifactTimeLimitedSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactTimeLimitedSlot : MonoBehaviour
{
  public UILabel artifactFinishedTime;
  public UITexture artifactFrame;
  public UITexture artifactTexture;
  public GameObject artifactEquippedNode;
  public GameObject artifactUnEquippedNode;
  public GameObject artifactCanEquipHintNode;
  private long _userId;
  private bool _showRedNode;

  public void RefreshForPlayer(long userId, bool showRedNode = true)
  {
    this._userId = userId;
    this._showRedNode = showRedNode;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
  }

  public void OnArtifactBtnPressed()
  {
    if (this._userId == PlayerData.inst.uid)
    {
      ArtifactSalePayload.Instance.ShowArtifactBuyInventory((System.Action<bool, object>) null);
    }
    else
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get(this._userId);
      CityData byUid = DBManager.inst.DB_City.GetByUid(this._userId);
      if (heroData == null || !heroData.IsArtifactLimitedEquiped(-1))
        return;
      Dictionary<int, int>.ValueCollection.Enumerator enumerator = heroData.ArtifactsLimited.Values.GetEnumerator();
      if (!enumerator.MoveNext() || byUid == null || !byUid.ArtifactsLimited.ContainsKey(enumerator.Current))
        return;
      ArtifactLimitedData artifactLimitedData = byUid.ArtifactsLimited[enumerator.Current];
      if (artifactLimitedData == null)
        return;
      ArtifactSalePayload.Instance.ShowArtifactDetail(artifactLimitedData.ArtifactId);
    }
  }

  private void UpdateUI()
  {
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(this._userId);
    CityData byUid = DBManager.inst.DB_City.GetByUid(this._userId);
    if (heroData == null || !heroData.IsArtifactLimitedEquiped(-1))
    {
      this.artifactFrame.mainTexture = (Texture) null;
      this.artifactTexture.mainTexture = (Texture) null;
      NGUITools.SetActive(this.artifactEquippedNode, false);
      NGUITools.SetActive(this.artifactUnEquippedNode, true);
      NGUITools.SetActive(this.artifactCanEquipHintNode, this._showRedNode && byUid != null && byUid.ArtifactsLimited.Count > 0);
    }
    else
    {
      Dictionary<int, int>.ValueCollection.Enumerator enumerator = heroData.ArtifactsLimited.Values.GetEnumerator();
      if (!enumerator.MoveNext() || byUid == null || !byUid.ArtifactsLimited.ContainsKey(enumerator.Current))
        return;
      ArtifactLimitedData artifactLimitedData = byUid.ArtifactsLimited[enumerator.Current];
      if (artifactLimitedData == null)
        return;
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactLimitedData.ArtifactId);
      if (artifactInfo != null)
      {
        if ((UnityEngine.Object) this.artifactFrame.mainTexture == (UnityEngine.Object) null || this.artifactFrame.mainTexture.name != "frame_equipment_3")
          BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactFrame, artifactInfo.TimeLimitedArtifactFramePath, (System.Action<bool>) null, true, false, string.Empty);
        if ((UnityEngine.Object) this.artifactTexture.mainTexture == (UnityEngine.Object) null || this.artifactTexture.mainTexture.name != artifactInfo.icon)
          BuilderFactory.Instance.HandyBuild((UIWidget) this.artifactTexture, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      }
      this.artifactFinishedTime.text = Utils.FormatTime(artifactLimitedData.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
      NGUITools.SetActive(this.artifactEquippedNode, true);
      NGUITools.SetActive(this.artifactUnEquippedNode, false);
      NGUITools.SetActive(this.artifactCanEquipHintNode, false);
    }
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }
}
