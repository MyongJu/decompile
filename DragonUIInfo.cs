﻿// Decompiled with JetBrains decompiler
// Type: DragonUIInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonUIInfo : MonoBehaviour
{
  public UILabel dragonOwnerName;
  public UILabel dragonOwnerLevel;
  public UILabel dragonOwnerTroopsCount;
  public Icon dragonIcon;
  public IconGroup skillGroup;

  public void SetDragon(long dragonId, string skillType, int tendency, int level)
  {
    if (DBManager.inst.DB_Dragon.Get(dragonId) == null)
    {
      NGUITools.SetActive(this.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.gameObject, true);
      string dragonIconPath = DragonUtils.GetDragonIconPath(tendency, level);
      this.dragonIcon.SetSingleData(level.ToString(), dragonIconPath);
      this.skillGroup.CreateIcon(DragonUtils.GetSkillIconData(skillType, dragonId, true));
    }
  }

  public void JustShow(long dragonId)
  {
    DragonData dragonData = DBManager.inst.DB_Dragon.Get(dragonId);
    if (dragonData == null)
    {
      NGUITools.SetActive(this.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.gameObject, true);
      string dragonIconPath = DragonUtils.GetDragonIconPath((int) dragonData.tendency, dragonData.Level);
      this.dragonIcon.SetSingleData(dragonData.Level.ToString(), dragonIconPath);
    }
  }

  public void SetDragonOwnerData(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    if ((Object) this.dragonOwnerName != (Object) null)
    {
      UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
      this.dragonOwnerName.text = userData == null ? string.Empty : userData.userName;
    }
    if ((Object) this.dragonOwnerLevel != (Object) null)
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get(marchData.ownerUid);
      this.dragonOwnerLevel.text = heroData == null ? string.Empty : Utils.XLAT("id_lv") + heroData.level.ToString();
    }
    if (!((Object) this.dragonOwnerTroopsCount != (Object) null))
      return;
    this.dragonOwnerTroopsCount.text = Utils.XLAT("alliance_fort_troops") + Utils.FormatThousands(marchData.troopsInfo.totalCount.ToString());
  }

  public void Clear()
  {
    if ((Object) this.dragonIcon != (Object) null)
      this.dragonIcon.Dispose();
    if (!((Object) this.skillGroup != (Object) null))
      return;
    this.skillGroup.Clear();
  }
}
