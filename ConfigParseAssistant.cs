﻿// Decompiled with JetBrains decompiler
// Type: ConfigParseAssistant
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

public class ConfigParseAssistant
{
  private Dictionary<string, string> contents = new Dictionary<string, string>();
  private Dictionary<string, ArrayList> headers = new Dictionary<string, ArrayList>();
  private Dictionary<string, Dictionary<string, int>> indexs = new Dictionary<string, Dictionary<string, int>>();
  private Dictionary<string, FieldInfo[]> fieldInfos = new Dictionary<string, FieldInfo[]>();
  private ConfigParse parse = new ConfigParse();
  private static ConfigParseAssistant _instance;

  public static ConfigParseAssistant Instance
  {
    get
    {
      if (ConfigParseAssistant._instance == null)
        ConfigParseAssistant._instance = new ConfigParseAssistant();
      return ConfigParseAssistant._instance;
    }
  }

  public void AddConfigData(string key, string content)
  {
    if (!this.contents.ContainsKey(key))
      this.contents.Add(key, content);
    else
      this.contents[key] = content;
  }

  public void Dispose()
  {
    this.contents.Clear();
    this.headers.Clear();
    this.indexs.Clear();
    this.fieldInfos.Clear();
  }

  private Dictionary<string, int> GetIndexs<T>(string configKey)
  {
    if (this.indexs.ContainsKey(configKey))
      return this.indexs[configKey];
    System.Type type = typeof (T);
    Dictionary<string, int> dictionary = new Dictionary<string, int>();
    ArrayList header = this.headers[configKey];
    FieldInfo[] fields = type.GetFields();
    if (!this.fieldInfos.ContainsKey(configKey))
      this.fieldInfos.Add(configKey, fields);
    for (int index1 = 0; index1 < fields.Length; ++index1)
    {
      object[] customAttributes = fields[index1].GetCustomAttributes(typeof (ConfigAttribute), false);
      if (customAttributes != null && customAttributes.Length != 0)
      {
        ConfigAttribute configAttribute = (ConfigAttribute) customAttributes[0];
        string empty = string.Empty;
        if (configAttribute.IsArray)
        {
          for (int index2 = 0; index2 < configAttribute.ArraySize; ++index2)
          {
            string str = string.Format("{0}{1}", (object) configAttribute.Name, (object) (index2 + 1));
            int index3 = ConfigManager.GetIndex(header, str);
            dictionary.Add(str, index3);
          }
        }
        else
        {
          string name = configAttribute.Name;
          int index2 = ConfigManager.GetIndex(header, configAttribute.Name);
          dictionary.Add(name, index2);
        }
      }
    }
    this.indexs.Add(configKey, dictionary);
    return dictionary;
  }

  public T Find<T>(string key, int internalId)
  {
    string json = this.Find(key, internalId);
    if (string.IsNullOrEmpty(json))
      return default (T);
    this.SetHeaders(key);
    Dictionary<string, int> indexs = this.GetIndexs<T>(key);
    FieldInfo[] fieldInfo = this.fieldInfos[key];
    T instance = Activator.CreateInstance<T>();
    ArrayList datas = (ArrayList) Utils.Json2Object(json, true);
    this.parse.SetFiledIndexs(indexs);
    this.parse.SetFiledInfos(fieldInfo);
    this.parse.Set<T>(instance, datas, internalId);
    return instance;
  }

  public List<T> FindAll<T>(string configName, string key)
  {
    if (this.IsEmpty(configName))
      return (List<T>) null;
    List<string> all = this.FindAll(this.contents[configName], key);
    List<T> objList = new List<T>();
    for (int index = 0; index < all.Count; ++index)
    {
      string content = all[index];
      T one = this.ParseOne<T>(configName, content);
      if ((object) one != null)
        objList.Add(one);
    }
    return objList;
  }

  private T ParseOne<T>(string configName, string content)
  {
    if (content == null)
      return default (T);
    string[] strArray = content.Split(':');
    if (strArray.Length < 2)
      return default (T);
    string s = strArray[0];
    if (s.IndexOf('"') > -1)
      s = s.Substring(1, s.Length - 2);
    int internalId = int.Parse(s);
    content = strArray[1];
    this.SetHeaders(configName);
    Dictionary<string, int> indexs = this.GetIndexs<T>(configName);
    FieldInfo[] fieldInfo = this.fieldInfos[configName];
    T instance = Activator.CreateInstance<T>();
    ArrayList datas = (ArrayList) Utils.Json2Object(content, true);
    this.parse.SetFiledIndexs(indexs);
    this.parse.SetFiledInfos(fieldInfo);
    this.parse.Set<T>(instance, datas, internalId);
    return instance;
  }

  public T Find<T>(string configName, string key)
  {
    if (this.IsEmpty(configName))
      return default (T);
    string one = this.FindOne(this.contents[configName], key);
    return this.ParseOne<T>(configName, one);
  }

  private void SetHeaders(string key)
  {
    if (this.headers.ContainsKey(key))
    {
      this.parse.SetHeader(this.headers[key]);
    }
    else
    {
      if (!this.contents.ContainsKey(key))
        return;
      ArrayList header = (ArrayList) Utils.Json2Object(this.Find(this.contents[key], "headers"), true);
      this.headers.Add(key, header);
      this.parse.SetHeader(header);
    }
  }

  private string Find(string content, string key)
  {
    string str1 = key;
    int startIndex1 = 0;
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    int num1 = content.IndexOf(str1, startIndex1);
    if (num1 == -1)
      return (string) null;
    while ((int) content[num1 + str1.Length + 1] != 58)
      startIndex1 += num1;
    int startIndex2 = num1 + str1.Length + 2;
    int num2 = content.IndexOf(']', startIndex2) - startIndex2;
    string str2 = content.Substring(startIndex2, num2 + 1);
    stopwatch.Stop();
    return str2;
  }

  public bool IsEmpty(string key)
  {
    return !this.contents.ContainsKey(key);
  }

  private string Find(string key, int internalId)
  {
    if (!this.contents.ContainsKey(key))
      return (string) null;
    return this.Find(this.contents[key], internalId.ToString());
  }

  private List<string> FindAll(string content, string key)
  {
    List<string> stringList = new List<string>();
    string str1 = "\"" + key + "\"";
    int startIndex1 = 0;
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    int length = content.Length;
    int startIndex2 = content.IndexOf(str1, startIndex1);
    if (startIndex2 < 0)
      return (List<string>) null;
    while (startIndex1 < length)
    {
      int charIndex1 = this.FindCharIndex(content, '[', startIndex2, -1, false);
      int charIndex2 = this.FindCharIndex(content, ']', startIndex2, length - startIndex2, true);
      int charIndex3 = this.FindCharIndex(content, '"', charIndex1 - 2, -1, false);
      if (charIndex3 > -1)
      {
        int num = charIndex2 - charIndex3;
        int startIndex3 = charIndex3;
        string str2 = content.Substring(startIndex3, num + 1);
        stringList.Add(str2);
        startIndex1 = charIndex2;
        startIndex2 = content.IndexOf(str1, startIndex1);
      }
      else
        break;
    }
    stopwatch.Stop();
    return stringList;
  }

  private string FindOne(string content, string key)
  {
    string str1 = string.Empty;
    string str2 = "\"" + key + "\"";
    int startIndex1 = 0;
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    int length = content.Length;
    int startIndex2 = content.IndexOf(str2, startIndex1);
    if (startIndex2 < 0)
      return (string) null;
    if (startIndex1 < length)
    {
      int charIndex1 = this.FindCharIndex(content, '[', startIndex2, -1, false);
      int charIndex2 = this.FindCharIndex(content, ']', startIndex2, length - startIndex2, true);
      int charIndex3 = this.FindCharIndex(content, '"', charIndex1 - 2, -1, false);
      if (charIndex3 > -1)
      {
        int num = charIndex2 - charIndex3;
        int startIndex3 = charIndex3;
        str1 = content.Substring(startIndex3, num + 1);
      }
    }
    stopwatch.Stop();
    return str1;
  }

  private int FindCharIndex(string content, char c, int startIndex, int endIndex, bool add = false)
  {
    int index = startIndex;
    int length = content.Length;
    while (index != endIndex)
    {
      if (add)
      {
        ++index;
        if (length >= index)
          break;
      }
      else
      {
        --index;
        if (index < 0)
          break;
      }
      if (index != endIndex)
      {
        if ((int) content[index] == (int) c)
          return index;
      }
      else
        break;
    }
    return -1;
  }

  private void ParseAll()
  {
  }

  public struct ConfigFileNames
  {
    public const string QUESTS_GROUP = "quest_group_main.json";
    public const string QUESTS = "quests.json";
    public const string ITEMS = "itemlist.json";
    public const string SHOP = "shop.json";
    public const string RESEARCH = "researches.json";
    public const string TECH = "tech.json";
    public const string DROP_MAIN = "drop_main.json";
    public const string SIGN = "sign_in.json";
  }
}
