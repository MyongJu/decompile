﻿// Decompiled with JetBrains decompiler
// Type: ItemTipChipInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class ItemTipChipInfo : MonoBehaviour
{
  private int _itemId;
  public EquipmentScrollChipComponent component;
  public UILabel desc;
  private BagType type;
  public UITable table;
  public EquipmentScrollComponent scroll;
  public GameObject normal;
  private ConfigEquipmentScrollInfo scrollInfo;

  public void SetData(int itemId)
  {
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo1 = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo1 != null)
      this.desc.text = itemStaticInfo1.LocDescription;
    ConfigEquipmentScrollChipInfo chipInfo = this.GetChipInfo();
    ConfigEquipmentScrollCombine scrollChipCombine = ConfigManager.inst.GetEquipmentScrollChipCombine(this.type);
    bool state = false;
    if (scrollChipCombine.GetCombineTarget(chipInfo.internalId) != null)
      state = scrollChipCombine.GetCombineTarget(chipInfo.internalId).Count == 1;
    NGUITools.SetActive(this.normal, !state);
    NGUITools.SetActive(this.scroll.gameObject, state);
    if (state)
    {
      this.scrollInfo = ConfigManager.inst.GetEquipmentScroll(this.type).GetData(scrollChipCombine.GetCombineTarget(chipInfo.internalId)[0]);
      ItemStaticInfo itemStaticInfo2 = ConfigManager.inst.DB_Items.GetItem(this.scrollInfo.itemID);
      this.desc.text = ScriptLocalization.GetWithMultyContents("forge_armory_scroll_fragments_synthesize_description", true, "[u][" + EquipmentConst.QULITY_HEX_COLOR_MAP[this.scrollInfo.quality] + "]" + itemStaticInfo2.LocName + "[/u]");
      this.desc.text = itemStaticInfo1.LocDescription;
      this.scroll.gameObject.SetActive(true);
      this.scroll.FeedData((IComponentData) new EquipmentScrollComponetData(this.scrollInfo, this.type));
    }
    if (chipInfo != null)
      this.component.FeedData((IComponentData) new EquipmentScrollChipComponetData(chipInfo, this.type));
    this.table.Reposition();
  }

  public Benefits GetBenefit()
  {
    if (this.scrollInfo != null)
      return ConfigManager.inst.GetEquipmentProperty(this.type).GetDataByEquipIDAndEnhanceLevel(this.scrollInfo.equipmenID, 0).benefits;
    return (Benefits) null;
  }

  public int GetEquipSuitID()
  {
    if (this.scrollInfo == null)
      return 0;
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.type).GetData(this.scrollInfo.equipmenID);
    if (data != null && data.suitGroup != 0)
      return data.suitGroup;
    return 0;
  }

  public ConfigEquipmentScrollInfo GetScrollInfo()
  {
    return this.scrollInfo;
  }

  public bool HadGemSlots()
  {
    if (this.scrollInfo == null)
      return false;
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(this.type).GetData(this.scrollInfo.equipmenID);
    int num = 0;
    if (data != null)
    {
      List<GemSlotConditions.Condition> conditionList = data.GemSlotConditions.ConditionList;
      for (int index = 0; index < conditionList.Count && conditionList[index].requiredLv <= 0; ++index)
        ++num;
    }
    return num > 0;
  }

  private ConfigEquipmentScrollChipInfo GetChipInfo()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._itemId);
    this.type = BagType.DragonKnight;
    if (itemStaticInfo != null && itemStaticInfo.Type == ItemBag.ItemType.equipment_scroll_chip)
      this.type = BagType.Hero;
    return ConfigManager.inst.GetEquipmentScrollChip(this.type).GetDataByItemID(this._itemId);
  }
}
