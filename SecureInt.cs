﻿// Decompiled with JetBrains decompiler
// Type: SecureInt
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SecureInt
{
  private int mValue;
  private int mSeed;

  public int val
  {
    get
    {
      return this.mValue - this.mSeed;
    }
    set
    {
      this.mSeed = Random.Range(0, 1000);
      this.mValue = value + this.mSeed;
    }
  }
}
