﻿// Decompiled with JetBrains decompiler
// Type: ConfigExchangeHallSort2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigExchangeHallSort2
{
  private List<ExchangeHallSort2Info> _exchangeHallSort2InfoList = new List<ExchangeHallSort2Info>();
  private Dictionary<string, ExchangeHallSort2Info> _datas;
  private Dictionary<int, ExchangeHallSort2Info> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ExchangeHallSort2Info, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ExchangeHallSort2Info>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._exchangeHallSort2InfoList.Add(enumerator.Current);
    }
    this._exchangeHallSort2InfoList.Sort((Comparison<ExchangeHallSort2Info>) ((a, b) => a.priority.CompareTo(b.priority)));
  }

  public ExchangeHallSort2Info Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ExchangeHallSort2Info) null;
  }

  public ExchangeHallSort2Info Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ExchangeHallSort2Info) null;
  }

  public List<ExchangeHallSort2Info> GetExchangeHallSort2InfoList()
  {
    return this._exchangeHallSort2InfoList;
  }
}
