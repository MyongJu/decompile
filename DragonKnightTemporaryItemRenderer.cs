﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTemporaryItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class DragonKnightTemporaryItemRenderer : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemOwned;
  public UILabel m_ItemName;
  public UILabel m_ItemDesc;
  private int m_ItemId;
  private System.Action<int> m_Callback;

  public void SetData(DragonKnightDungeonData.BagInfo item, System.Action<int> callback)
  {
    this.m_ItemId = item.itemId;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public void OnUseItem()
  {
    DragonKnightSystem.Instance.RoomManager.SyncSearchPathToServer((System.Action) (() =>
    {
      Hashtable postData = new Hashtable();
      postData[(object) "item"] = (object) this.m_ItemId;
      postData[(object) "count"] = (object) 1;
      MessageHub.inst.GetPortByAction("dragonKnight:useDkItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.UpdateUI();
        if (this.m_Callback == null)
          return;
        this.m_Callback(this.m_ItemId);
      }), true);
    }), (Room) null);
  }

  public DragonKnightDungeonData.BagInfo BagItem
  {
    get
    {
      return DBManager.inst.DB_DragonKnightDungeon.GetDragonKnightDungeonData(0L).TemporaryItems[this.m_ItemId];
    }
  }

  private void UpdateUI()
  {
    DragonKnightItemInfo dragonKnightItemInfo = ConfigManager.inst.DB_DragonKnightItem.Get(this.m_ItemId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, dragonKnightItemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_ItemOwned.text = "x " + this.BagItem.count.ToString();
    this.m_ItemName.text = dragonKnightItemInfo.LocName;
    this.m_ItemDesc.text = dragonKnightItemInfo.LocDesc;
  }
}
