﻿// Decompiled with JetBrains decompiler
// Type: ResearchTreeDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ResearchTreeDlg : UI.Dialog
{
  public int RowGap = 200;
  public int ColGap = 200;
  private Dictionary<string, TechSlot> slots = new Dictionary<string, TechSlot>();
  private Dictionary<string, SlotLine> lines = new Dictionary<string, SlotLine>();
  private Dictionary<string, SlotCircle> circles = new Dictionary<string, SlotCircle>();
  private const string Prefix_Line = "line_";
  private const string Prefix_Slot = "slot_";
  private const string Prefix_Circle = "circle_";
  private const int Depth_Line = 14;
  private const int Depth_Circle = 15;
  public UIScrollView scrollView;
  public Transform GridParent;
  public UIWidget DragArea;
  public GameObject LinePrefab;
  public GameObject CirclePrefab;
  public GameObject ResearchTreeSlot;
  public UILabel Title;
  private int tree;
  private Dictionary<int, Tech> Techs;
  private Vector3 scrollViewPosition;
  private bool keepLastPositon;
  private GameObject researchingSlot;

  public override void OnOpen(UIControler.UIParameter param = null)
  {
    base.OnOpen(param);
    ResearchTreeDlg.Parameter parameter = param as ResearchTreeDlg.Parameter;
    this.gameObject.SetActive(true);
    this.tree = parameter.tree;
    this.keepLastPositon = parameter.keepLastPositon;
    this.Techs = ResearchManager.inst.TechsOfTree(this.tree);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DBManager.inst.DB_Research.onDataChanged += new System.Action<long>(this.OnDataChange);
    this.UpdateTechs();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    DBManager.inst.DB_Research.onDataChanged -= new System.Action<long>(this.OnDataChange);
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnBackBrnPressed()
  {
    this.keepLastPositon = false;
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  [ContextMenu("Update Tree")]
  public void UpdateTechs()
  {
    this.ClearGrid();
    this.Title.text = ScriptLocalization.Get(ResearchConst.RESEARCH_TREE_KEY_PREFIXS[this.tree - 1] + "name", true);
    this.DragArea.width = 0;
    using (Dictionary<int, Tech>.Enumerator enumerator = this.Techs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddSlot(enumerator.Current.Value);
    }
    using (Dictionary<string, TechSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.ConfigSlotRequire(enumerator.Current.Value.CurrentTechLevel);
    }
    if (!((UnityEngine.Object) this.researchingSlot != (UnityEngine.Object) null))
      return;
    this.scrollView.MoveRelative(Vector3.left * (this.researchingSlot.transform.localPosition.x - 1200f));
    this.scrollView.RestrictWithinBounds(false);
  }

  public void OnTechButtonPressed(TechLevel techLevel)
  {
    if (techLevel.IsCompleted)
      UIManager.inst.OpenPopup("ResearchDetailInfoPopup", (Popup.PopupParameter) new ResearchDetailInfoPopup.UIParam()
      {
        techLevel = techLevel
      });
    else
      UIManager.inst.OpenDlg("Research/ResearchDetailDlg", (UI.Dialog.DialogParameter) new ResearchDetailDlg.Parameter()
      {
        techLevelInternalID = techLevel.InternalID
      }, true, true, true);
  }

  private void OnDataChange(long itemID)
  {
    this.researchingSlot = (GameObject) null;
    this.Techs = ResearchManager.inst.TechsOfTree(this.tree);
    this.UpdateTechs();
  }

  private void AddSlot(Tech tech)
  {
    int row = tech.Row;
    int tier = tech.Tier;
    string key = "slot_" + (object) row + "_" + (object) tier;
    if (this.slots.ContainsKey(key))
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.ResearchTreeSlot);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.GridParent;
    gameObject.transform.localPosition = new Vector3((float) (tier * this.ColGap), (float) (row * this.RowGap), 0.0f);
    gameObject.transform.localScale = Vector3.one;
    gameObject.name = key;
    TechSlot component = gameObject.GetComponent<TechSlot>();
    component.techID = tech.ID;
    component.tech = tech;
    component.UpdateUI();
    gameObject.GetComponent<UIDragScrollView>().scrollView = this.scrollView;
    this.slots.Add(gameObject.name, component);
    if ((double) gameObject.transform.localPosition.x > (double) this.DragArea.width)
      this.DragArea.width = (int) ((double) gameObject.transform.localPosition.x + (double) this.ColGap);
    TechLevel currentResearch = ResearchManager.inst.CurrentResearch;
    if (currentResearch == null || !(currentResearch.Tech.ID == tech.ID))
      return;
    this.researchingSlot = gameObject;
  }

  private void ConfigSlotRequire(TechLevel techLevel)
  {
    this.FindRequireTechLevels(ResearchManager.inst.FirstLevelOfSameTechLevel(techLevel)).ForEach((System.Action<TechLevel>) (require => this.AddLinesBetweenSlots(this.slots["slot_" + (object) require.Tech.Row + "_" + (object) require.Tech.Tier], this.slots["slot_" + (object) techLevel.Tech.Row + "_" + (object) techLevel.Tech.Tier], this.isRequireTechLevelConfirm(require))));
  }

  private void AddLinesBetweenSlots(TechSlot begin, TechSlot end, bool hightLight = false)
  {
    if (begin.Row == end.Row)
    {
      for (int col = begin.Col; col < end.Col; ++col)
        this.LineTo(begin.Row, col, end.Row, col + 1, hightLight);
    }
    else if (begin.Col == end.Col)
    {
      int num1 = Mathf.Min(begin.Row, end.Row);
      int num2 = Mathf.Abs(begin.Row - end.Row);
      for (int beginRow = num1; beginRow < num1 + num2; ++beginRow)
        this.LineTo(beginRow, begin.Col, beginRow + 1, end.Col, hightLight);
    }
    else
    {
      this.LineTo(begin.Row, begin.Col, begin.Row, begin.Col + 1, hightLight);
      this.LineTo(end.Row, end.Col - 1, end.Row, end.Col, hightLight);
      int num1 = Mathf.Min(begin.Row, end.Row);
      int num2 = Mathf.Abs(begin.Row - end.Row);
      for (int beginRow = num1; beginRow < num1 + num2; ++beginRow)
        this.LineTo(beginRow, begin.Col + 1, beginRow + 1, begin.Col + 1, hightLight);
    }
  }

  private void LineTo(int beginRow, int beginCol, int endRow, int endCol, bool hightLight = false)
  {
    Vector3 vector3 = new Vector3((float) (beginCol + endCol) / 2f * (float) this.ColGap, (float) (beginRow + endRow) / 2f * (float) this.RowGap, 0.0f);
    string key = "line_" + (object) vector3.x + "_" + (object) vector3.y;
    if (this.lines.ContainsKey(key) && (!this.lines[key].HighLighted || hightLight))
      return;
    if (!this.lines.ContainsKey(key))
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.LinePrefab);
      gameObject.name = key;
      gameObject.transform.parent = this.GridParent;
      gameObject.transform.localRotation = Quaternion.AngleAxis(beginRow != endRow ? 90f : 0.0f, Vector3.forward);
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localPosition = vector3;
      SlotLine component1 = gameObject.GetComponent<SlotLine>();
      component1.position = new Vector2(vector3.x, vector3.y);
      component1.HighLighted = hightLight;
      UISprite component2 = gameObject.GetComponent<UISprite>();
      component2.width = beginRow != endRow ? this.RowGap + 10 : this.ColGap + 10;
      component2.depth = 14;
      this.lines.Add(key, component1);
    }
    else
      this.lines[key].HighLighted = hightLight;
    this.CheckAroundLineToAddCircle(beginRow, beginCol, endRow, endCol);
  }

  private void CheckAroundLineToAddCircle(int beginRow, int beginCol, int endRow, int endCol)
  {
    this.CheckAroundLineOneEnd(beginRow, beginCol);
    this.CheckAroundLineOneEnd(endRow, endCol);
  }

  private void CheckAroundLineOneEnd(int Row, int Col)
  {
    int num1 = 0;
    int num2 = 0;
    List<Vector2> vector2List = new List<Vector2>();
    for (int index = 0; index < ResearchConst.Direction.Count; ++index)
    {
      string key = "line_" + (object) (float) (((double) Col + (double) ResearchConst.Direction[index].x) * (double) this.ColGap) + "_" + (object) (float) (((double) Row + (double) ResearchConst.Direction[index].y) * (double) this.RowGap);
      if (this.lines.ContainsKey(key))
      {
        ++num1;
        if (this.lines[key].HighLighted)
          ++num2;
        vector2List.Add(ResearchConst.Direction[index]);
      }
    }
    if (num1 > 2)
      this.AddCircle(Row, Col, num2 == num1);
    if (num1 != 2 || Mathf.Abs(ResearchConst.Direction.IndexOf(vector2List[0]) - ResearchConst.Direction.IndexOf(vector2List[1])) == 2)
      return;
    this.AddCircle(Row, Col, num2 == num1);
  }

  private void AddCircle(int Row, int Col, bool hightlight)
  {
    Vector3 vector3 = new Vector3((float) (Col * this.ColGap), (float) (Row * this.RowGap), 0.0f);
    string key = "circle_" + (object) Row + "_" + (object) Col;
    if (this.circles.ContainsKey(key))
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.CirclePrefab);
    gameObject.name = key;
    SlotCircle component = gameObject.GetComponent<SlotCircle>();
    component.Enable = hightlight;
    gameObject.transform.parent = this.GridParent;
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = vector3;
    this.circles.Add(key, component);
  }

  private List<TechLevel> FindRequireTechLevels(TechLevel techLevel)
  {
    List<TechLevel> techLevelList = new List<TechLevel>();
    using (List<int>.Enumerator enumerator = techLevel.RequiredTechs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        techLevelList.Add(ResearchManager.inst.GetTechLevel(current));
      }
    }
    return techLevelList;
  }

  private bool isRequireTechLevelConfirm(TechLevel techLevel)
  {
    return techLevel.IsCompleted;
  }

  private void ClearGrid()
  {
    this.slots.Clear();
    this.circles.Clear();
    this.lines.Clear();
    for (int index = 0; index < this.GridParent.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.GridParent.GetChild(index).gameObject);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int tree;
    public bool keepLastPositon;
  }
}
