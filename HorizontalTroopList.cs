﻿// Decompiled with JetBrains decompiler
// Type: HorizontalTroopList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HorizontalTroopList : MonoBehaviour
{
  private List<TroopInfoBar> _infoBars = new List<TroopInfoBar>();
  private char _currentChar = 'a';
  public TroopInfoBar troopInfoTemplate;
  public UITable table;
  public System.Action<HorizontalTroopList> OnTablePositioned;
  public bool positioned;
  private string _infoTemplateName;

  public void Start()
  {
    this._infoTemplateName = this.troopInfoTemplate.gameObject.name;
  }

  public void SetData(List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>> troopData)
  {
    this.Clear();
    List<KeyValuePair<TroopInfo, KeyValuePair<int, int>>>.Enumerator enumerator = troopData.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = NGUITools.AddChild(this.table.gameObject, this.troopInfoTemplate.gameObject);
      TroopInfoBar component = gameObject.GetComponent<TroopInfoBar>();
      component.SetData(enumerator.Current.Key, enumerator.Current.Value.Key, enumerator.Current.Value.Value);
      gameObject.name = this._currentChar.ToString();
      ++this._currentChar;
      gameObject.gameObject.SetActive(true);
      this._infoBars.Add(component);
    }
    this.table.onReposition += new UITable.OnReposition(this.OnReposition);
    this.table.repositionNow = true;
  }

  public void Clear()
  {
    this.positioned = false;
    this._currentChar = 'a';
    List<TroopInfoBar>.Enumerator enumerator = this._infoBars.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.gameObject.name = this._infoTemplateName;
      enumerator.Current.gameObject.SetActive(false);
      PrefabManagerEx.Instance.Destroy(enumerator.Current.gameObject);
    }
    this._infoBars.Clear();
  }

  private void OnReposition()
  {
    this.table.onReposition -= new UITable.OnReposition(this.OnReposition);
    this.positioned = true;
    if (this.OnTablePositioned == null)
      return;
    this.OnTablePositioned(this);
  }
}
