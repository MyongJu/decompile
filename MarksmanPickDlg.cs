﻿// Decompiled with JetBrains decompiler
// Type: MarksmanPickDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarksmanPickDlg : UI.Dialog
{
  private Dictionary<string, string> _propsValidTimeParam = new Dictionary<string, string>();
  private Dictionary<int, MarksmanRewardItemRenderer> _itemDict = new Dictionary<int, MarksmanRewardItemRenderer>();
  private Dictionary<int, MarksmanRewardItemRenderer> _itemPickedDict = new Dictionary<int, MarksmanRewardItemRenderer>();
  private Dictionary<int, MarksmanGroupRenderer> _itemGroupDict = new Dictionary<int, MarksmanGroupRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public UITexture npcTexture;
  public UITexture propsTexture;
  public UILabel propsText;
  public UILabel propsValidTimeText;
  public UILabel refreshNeedGoldText;
  public UIButton paidRefreshButton;
  public UIButton freeRefreshButton;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public UIScrollView pickedScrollView;
  public UITable pickedTable;
  public GameObject pickedItemPrefab;
  public UIScrollView groupScrollView;
  public UITable groupTable;
  public MarksmanGroupRenderer topGroupItem;
  public MarksmanGroupRenderer groupItemTemplate;
  public UISprite selectableGroupFrame;
  public UISprite groupItemTemplateArrow;
  private LuckyArcherGroupInfo _lastPickedGroupInfo;
  private bool _selectableGroupClicked;
  private string _propsImagePath;
  private Color _refreshButtonTextOriginColor;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._refreshButtonTextOriginColor = this.refreshNeedGoldText.color;
    this.CheckPaidUser();
    this.AddEventHandler();
    this.OnSecondEvent(0);
    this.UpdateNpcUI();
    this.UpdatePropsUI();
    this.UpdateRefreshUI();
    this.UpdateUI();
    this.UpdateLeftUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
    this.ClearGroupData();
    this.ClearGroupRewardData();
    this._lastPickedGroupInfo = (LuckyArcherGroupInfo) null;
    this._selectableGroupClicked = false;
  }

  public void OnPropsButtonClicked()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnHelpButtonClicked()
  {
    MarksmanPayload.Instance.ShowMarksmanHelp();
  }

  public void OnStartButtonClicked()
  {
    if (!this.CheckPaidUser())
      return;
    MarksmanPayload.Instance.StartMarksman((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      MarksmanPayload.Instance.ShowFirstMarksmanScene();
    }));
  }

  public void OnRefreshButtonClicked()
  {
    if (!this.CheckPaidUser())
      return;
    int num1 = this._lastPickedGroupInfo == null ? 0 : this._lastPickedGroupInfo.internalId;
    MarksmanPayload instance = MarksmanPayload.Instance;
    System.Action<bool, object> action = (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateRefreshUI();
      this.UpdateUI();
    });
    int groupId = num1;
    int num2 = 0;
    System.Action<bool, object> callback = action;
    instance.RefreshMarksmanRewards(groupId, num2 != 0, callback);
  }

  public void OnSelectableGroupClicked()
  {
    this._selectableGroupClicked = !this._selectableGroupClicked;
    if (this._selectableGroupClicked)
      this.OnSelectableGroupExpanded();
    else
      this.OnSelectableGroupCollapsed();
  }

  private bool CheckPaidUser()
  {
    if (!MarksmanPayload.Instance.TempMarksmanData.CanMarksmanOpen)
      MarksmanPayload.Instance.ShowUnlockMarksmanScene();
    return MarksmanPayload.Instance.TempMarksmanData.CanMarksmanOpen;
  }

  private void UpdateUI()
  {
    this.ClearData();
    List<MarksmanData.RewardData> rewardDataList = MarksmanPayload.Instance.TempMarksmanData.RewardDataList;
    if (rewardDataList != null)
    {
      for (int key = 0; key < rewardDataList.Count; ++key)
      {
        MarksmanRewardItemRenderer itemRenderer = this.CreateItemRenderer(rewardDataList[key], MarksmanData.RewardType.PICK, this.itemPrefab, this.itemRoot);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void UpdateRefreshUI()
  {
    this.refreshNeedGoldText.text = Utils.FormatThousands(MarksmanPayload.Instance.RefreshNeedGold.ToString());
    if (PlayerData.inst.userData.currency.gold < (long) MarksmanPayload.Instance.RefreshNeedGold)
      this.refreshNeedGoldText.color = Color.red;
    else
      this.refreshNeedGoldText.color = this._refreshButtonTextOriginColor;
    NGUITools.SetActive(this.paidRefreshButton.gameObject, !MarksmanPayload.Instance.IsRefreshFree);
    NGUITools.SetActive(this.freeRefreshButton.gameObject, MarksmanPayload.Instance.IsRefreshFree);
  }

  private void UpdateNpcUI()
  {
    Utils.SetPortrait(this.npcTexture, "Texture/STATIC_TEXTURE/tutorial_npc_05");
  }

  private void UpdatePropsUI()
  {
    this.propsText.text = Utils.FormatThousands(MarksmanPayload.Instance.TempMarksmanData.PropsCount.ToString());
    this._propsValidTimeParam.Clear();
    this._propsValidTimeParam.Add("0", Utils.FormatTime(MarksmanPayload.Instance.TempMarksmanData.PropsValidTime - NetServerTime.inst.ServerTimestamp, true, false, true));
    this.propsValidTimeText.text = ScriptLocalization.GetWithPara("tavern_lucky_archer_expires_num", this._propsValidTimeParam, true);
    NGUITools.SetActive(this.propsValidTimeText.transform.parent.gameObject, MarksmanPayload.Instance.TempMarksmanData.PropsValidTime > 0);
    if (!(this._propsImagePath != MarksmanPayload.Instance.TempMarksmanData.PropsImagePath))
      return;
    this._propsImagePath = MarksmanPayload.Instance.TempMarksmanData.PropsImagePath;
    MarksmanPayload.Instance.FeedMarksmanTexture(this.propsTexture, this._propsImagePath);
  }

  private void ClearData()
  {
    using (Dictionary<int, MarksmanRewardItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
  }

  private MarksmanRewardItemRenderer CreateItemRenderer(MarksmanData.RewardData rewardData, MarksmanData.RewardType rewardType, GameObject prototype, GameObject parentNode)
  {
    this._itemPool.Initialize(prototype, parentNode);
    GameObject gameObject = this._itemPool.AddChild(parentNode);
    gameObject.SetActive(true);
    MarksmanRewardItemRenderer component = gameObject.GetComponent<MarksmanRewardItemRenderer>();
    component.SetData(rewardData, rewardType);
    return component;
  }

  private void OnItemDataUpdated(int itemId)
  {
    if (itemId != MarksmanPayload.Instance.TempMarksmanData.PropsId)
      return;
    this.UpdatePropsUI();
  }

  private void OnUserDataUpdated(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.UpdateRefreshUI();
  }

  private void OnSecondEvent(int time)
  {
    this.UpdatePropsUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdated);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdated);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void ClearGroupData()
  {
    using (Dictionary<int, MarksmanGroupRenderer>.Enumerator enumerator = this._itemGroupDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, MarksmanGroupRenderer> current = enumerator.Current;
        current.Value.OnGroupNormalClick -= new System.Action<LuckyArcherGroupInfo>(this.OnGroupNormalClick);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemGroupDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionGroup()
  {
    this.groupTable.repositionNow = true;
    this.groupTable.Reposition();
    this.groupScrollView.ResetPosition();
  }

  private MarksmanGroupRenderer GenerateSlot(LuckyArcherGroupInfo groupInfo)
  {
    this._itemPool.Initialize(this.groupItemTemplate.gameObject, this.groupTable.gameObject);
    GameObject gameObject = this._itemPool.AddChild(this.groupTable.gameObject);
    gameObject.SetActive(true);
    MarksmanGroupRenderer component = gameObject.GetComponent<MarksmanGroupRenderer>();
    component.SetData(groupInfo);
    component.ShowArrow(false);
    component.OnGroupNormalClick += new System.Action<LuckyArcherGroupInfo>(this.OnGroupNormalClick);
    return component;
  }

  private void OnGroupNormalClick(LuckyArcherGroupInfo groupInfo)
  {
    this._selectableGroupClicked = false;
    if (groupInfo != null && this._lastPickedGroupInfo != null && groupInfo.internalId != this._lastPickedGroupInfo.internalId)
    {
      this._lastPickedGroupInfo = groupInfo;
      this.topGroupItem.SetData(groupInfo);
      this.UpdateGroupTop(true);
      this.UpdateGroupReward(groupInfo);
    }
    this.ClearGroupData();
    this.UpdateGroupTopArrow(false);
  }

  private void UpdateSelectableGroup()
  {
    this.ClearGroupData();
    List<LuckyArcherGroupInfo> selectableGroupInfo = MarksmanPayload.Instance.GetSelectableGroupInfo();
    if (selectableGroupInfo != null)
    {
      for (int key = 0; key < selectableGroupInfo.Count; ++key)
      {
        MarksmanGroupRenderer slot = this.GenerateSlot(selectableGroupInfo[key]);
        this._itemGroupDict.Add(key, slot);
      }
    }
    if (this._lastPickedGroupInfo != null)
    {
      using (Dictionary<int, MarksmanGroupRenderer>.Enumerator enumerator = this._itemGroupDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, MarksmanGroupRenderer> current = enumerator.Current;
          if (current.Value.GroupInfo.internalId == this._lastPickedGroupInfo.internalId)
          {
            this.topGroupItem.SetData(current.Value.GroupInfo);
            this.UpdateGroupReward(current.Value.GroupInfo);
          }
        }
      }
    }
    else if (this._itemGroupDict.ContainsKey(0))
    {
      this.topGroupItem.SetData(this._itemGroupDict[0].GroupInfo);
      this.UpdateGroupReward(this._itemGroupDict[0].GroupInfo);
      this._lastPickedGroupInfo = this._itemGroupDict[0].GroupInfo;
    }
    this.UpdateGroupTop(this._itemGroupDict.Count > 0);
    this.UpdateGroupTopArrow(false);
    this.RepositionGroup();
  }

  private void ClearGroupRewardData()
  {
    using (Dictionary<int, MarksmanRewardItemRenderer>.Enumerator enumerator = this._itemPickedDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemPickedDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionGroupReward()
  {
    this.pickedTable.repositionNow = true;
    this.pickedTable.Reposition();
    this.pickedScrollView.ResetPosition();
  }

  private void UpdateGroupReward(LuckyArcherGroupInfo groupInfo)
  {
    this.ClearGroupRewardData();
    List<LuckyArcherRewardsInfo> archerRewardsInfoList = ConfigManager.inst.DB_LuckyArcherRewards.GetLuckyArcherRewardsInfoList();
    if (archerRewardsInfoList != null)
    {
      List<LuckyArcherRewardsInfo> all = archerRewardsInfoList.FindAll((Predicate<LuckyArcherRewardsInfo>) (x =>
      {
        if (x.groupId == groupInfo.internalId)
          return x.IsRare;
        return false;
      }));
      if (all != null)
      {
        for (int key = 0; key < all.Count; ++key)
        {
          int result = 0;
          int.TryParse(all[key].id, out result);
          if (result > 0)
          {
            MarksmanRewardItemRenderer itemRenderer = this.CreateItemRenderer(new MarksmanData.RewardData(result, 0, 0), MarksmanData.RewardType.PICK, this.pickedItemPrefab, this.pickedTable.gameObject);
            this._itemPickedDict.Add(key, itemRenderer);
          }
        }
      }
    }
    this.RepositionGroupReward();
  }

  private void UpdateLeftUI()
  {
    this.UpdateSelectableGroup();
    this.ClearGroupData();
  }

  private void UpdateGroupTop(bool show)
  {
    NGUITools.SetActive(this.topGroupItem.gameObject, show);
  }

  private void UpdateGroupTopArrow(bool show)
  {
    NGUITools.SetActive(this.selectableGroupFrame.gameObject, show);
    if (show)
    {
      this.topGroupItem.ShowArrow(true);
      this.groupItemTemplateArrow.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, -90f);
      this.selectableGroupFrame.height = (int) ((double) (this._itemGroupDict.Count + 1) * (double) NGUIMath.CalculateRelativeWidgetBounds(this.topGroupItem.transform).size.y) + 30;
      BoxCollider component = this.selectableGroupFrame.transform.GetComponent<BoxCollider>();
      float y = component.size.y;
      component.size = new Vector3(this.selectableGroupFrame.localSize.x, this.selectableGroupFrame.localSize.y, 0.0f);
      component.center = new Vector3(0.0f, (float) (-(double) this.selectableGroupFrame.transform.localPosition.y + 120.0), 0.0f);
    }
    else
      this.groupItemTemplateArrow.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
  }

  private void UpdatePickedGroup()
  {
    if (!((UnityEngine.Object) this.topGroupItem != (UnityEngine.Object) null) || this.topGroupItem.GroupInfo == null)
      return;
    using (Dictionary<int, MarksmanGroupRenderer>.Enumerator enumerator = this._itemGroupDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, MarksmanGroupRenderer> current = enumerator.Current;
        if (current.Value.GroupInfo.internalId == this.topGroupItem.GroupInfo.internalId)
          current.Value.ShowPickedStatus(true);
      }
    }
  }

  private void OnSelectableGroupExpanded()
  {
    this.UpdateSelectableGroup();
    this.UpdateGroupTopArrow(true);
    this.UpdatePickedGroup();
  }

  private void OnSelectableGroupCollapsed()
  {
    this.ClearGroupData();
    this.UpdateGroupTopArrow(false);
  }
}
