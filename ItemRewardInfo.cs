﻿// Decompiled with JetBrains decompiler
// Type: ItemRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemRewardInfo : MonoBehaviour
{
  public UILabel name;
  public UITexture icon;
  public UILabel enhance;
  public UITexture background;
  public Transform border;
  private int _itemId;

  public void SeedData(ItemRewardInfo.Data d)
  {
    this._itemId = d.itemId;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.name && d.name != null)
      this.name.text = d.name;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.enhance)
      this.enhance.text = "x" + (object) d.count;
    if ((double) d.count == 0.0)
      this.enhance.gameObject.SetActive(false);
    if (d.icon != null)
      BuilderFactory.Instance.Build((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemNormalBackground(this.background, d.quality);
    Utils.SetItemNormalName(this.name, d.quality);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this._itemId, this.border, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this._itemId, this.border, 0L, 0L, 0);
  }

  public void OnDestroy()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }

  public class Data
  {
    public string name;
    public string icon;
    public float count;
    public int quality;
    public int itemId;
  }
}
