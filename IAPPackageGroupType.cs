﻿// Decompiled with JetBrains decompiler
// Type: IAPPackageGroupType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public struct IAPPackageGroupType
{
  public const int CONSOLE_EVENT = 0;
  public const int EVENT = 1;
  public const int PERIOD = 2;
  public const int GROWTH = 3;
  public const int LOST = 4;
  public const int FIRSTPAY = 5;
}
