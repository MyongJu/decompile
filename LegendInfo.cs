﻿// Decompiled with JetBrains decompiler
// Type: LegendInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;

public class LegendInfo
{
  private const string IMAGE_FOLDER_PATH = "Texture/Legend/Image";
  private const string ICON_FOLDER_PATH = "Texture/Legend/Icon";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "power")]
  public int power;
  [Config(CustomParse = true, CustomType = typeof (Requirements), Name = "Requirements")]
  public Requirements require;
  [Config(CustomParse = true, CustomType = typeof (Costs), Name = "Costs")]
  public Costs cost;
  [Config(Name = "gold")]
  public int gold;
  [Config(IsArrayList = true, Name = "Skills", NeedValueSort = true)]
  public ArrayList Skills;
  [Config(Name = "benefits_infantry")]
  public int Infantry;
  [Config(Name = "benefits_ranged")]
  public int Ranged;
  [Config(Name = "benefits_cavalry")]
  public int Cavalry;
  [Config(Name = "benefits_siege")]
  public int Siege;

  public string Loc_Name
  {
    get
    {
      return ScriptLocalization.Get(string.Format("{0}_name", (object) this.ID), true);
    }
  }

  public string Loc_Desc
  {
    get
    {
      return ScriptLocalization.Get(string.Format("{0}_description", (object) this.ID), true);
    }
  }

  public string Image
  {
    get
    {
      return string.Format("{0}/{1}{2}", (object) "Texture/Legend/Image", (object) "card_", (object) this.ID);
    }
  }

  public string Icon
  {
    get
    {
      return string.Format("{0}/{1}{2}", (object) "Texture/Legend/Icon", (object) "portrait_", (object) this.ID);
    }
  }
}
