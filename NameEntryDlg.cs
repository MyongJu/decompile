﻿// Decompiled with JetBrains decompiler
// Type: NameEntryDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class NameEntryDlg : MonoBehaviour
{
  public UILabel mUserName;
  private System.Action _onFinished;

  public void Show(System.Action onFinished)
  {
    this._onFinished = onFinished;
  }

  public void OnOkPressed()
  {
    NetApi.inst.Enqueue("Player:setMyUsername", new Hashtable()
    {
      {
        (object) "username",
        (object) this.mUserName.text
      }
    }, new System.Action<Hashtable, Hashtable>(this.SetUserNameCallback));
  }

  public void SetUserNameCallback(Hashtable json, Hashtable sendParams)
  {
    this._onFinished();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    GameDataManager.inst.AddCommand("UserNameEntered", (object) Utils.Hash((object) "AAA", (object) "BBB"));
  }
}
