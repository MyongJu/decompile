﻿// Decompiled with JetBrains decompiler
// Type: DefenseSlotBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DefenseSlotBar : MonoBehaviour
{
  private List<RallySlotBarDetail> _details = new List<RallySlotBarDetail>();
  private MarchData _marchData;
  [SerializeField]
  private DefenseSlotBar.Panel panel;
  private bool _detailExpended;
  private bool isSecondUpdate;

  private long _marchId
  {
    get
    {
      if (this._marchData != null)
        return this._marchData.marchId;
      return 0;
    }
    set
    {
      this._marchData = DBManager.inst.DB_March.Get(value);
    }
  }

  public bool detailExpended
  {
    get
    {
      return this._detailExpended;
    }
    set
    {
      if (value == this._detailExpended)
        return;
      this._detailExpended = value;
      if (this._detailExpended)
      {
        this.panel.TweenScale_ExpendDetals.PlayForward();
        this.panel.TweenRotate_ExpendButton.PlayForward();
      }
      else
      {
        this.panel.TweenScale_ExpendDetals.PlayReverse();
        this.panel.TweenRotate_ExpendButton.PlayReverse();
      }
    }
  }

  public void Setup(int index, long marchId)
  {
    this._marchId = marchId;
    this.SetupInfo(index);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  private void SetupInfo(int index)
  {
    this.panel.indexCount.text = (index + 1).ToString();
    if (this._marchData == null || this._marchData == null || this._marchData.troopsInfo.troops == null)
      return;
    if (this._details.Count < this._marchData.troopsInfo.troops.Count)
    {
      int count1 = this._details.Count;
      for (int count2 = this._marchData.troopsInfo.troops.Count; count1 < count2; ++count1)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.detailContainer.gameObject, this.panel.detailBarOrg.gameObject);
        gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(count1 + 1) * this.panel.detailBarOrgWidget.height), 0.0f);
        gameObject.name = string.Format("{0}", (object) (100 + count1));
        this._details.Add(gameObject.GetComponent<RallySlotBarDetail>());
      }
    }
    int index1 = 0;
    Dictionary<string, Unit>.ValueCollection.Enumerator enumerator = this._marchData.troopsInfo.troops.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      this._details[index1].Setup(enumerator.Current);
      this._details[index1].gameObject.SetActive(true);
      ++index1;
    }
    for (int index2 = index1; index2 < this._details.Count; ++index2)
      this._details[index2].gameObject.SetActive(false);
    this.panel.detailContainer.transform.localPosition = new Vector3(0.0f, (float) -this.panel.basicContainerHalfHeight, 0.0f);
    this.panel.detailContainer.height = this.panel.detailsTitle.height + index1 * this.panel.detailSlotPadding + this.panel.detailOffset;
    this.detailExpended = false;
    this.panel.troopsCount.text = Utils.ConvertNumberToNormalString(this._marchData.troopsInfo.totalCount);
  }

  public void OnSecond(int serverTime)
  {
    if (this._marchData == null)
      return;
    this.panel.playerName.text = this._marchData.ownerUserName;
    if (this._marchData.state == MarchData.MarchState.reinforcing)
    {
      this.panel.marchTimeContainer.gameObject.SetActive(false);
      this.panel.LB_Reinforced.gameObject.SetActive(true);
      this.RemoveSecondUpdate();
    }
    else
    {
      if (this._marchData.state != MarchData.MarchState.marching && this._marchData.state != MarchData.MarchState.returning)
        return;
      if (NetServerTime.inst.ServerTimestamp < this._marchData.timeDuration.endTime)
      {
        this.panel.marchTimeContainer.gameObject.SetActive(true);
        this.panel.marchTimeSlider.value = this._marchData.timeDuration.oneOverTimeDuration * (float) (NetServerTime.inst.ServerTimestamp - this._marchData.timeDuration.startTime);
        this.panel.marchTimeLeftText.text = Utils.FormatTime(this._marchData.timeDuration.endTime - NetServerTime.inst.ServerTimestamp, false, false, true);
        this.AddSecondUpdate();
      }
      else
      {
        this.panel.marchTimeContainer.gameObject.SetActive(false);
        this.panel.LB_Reinforced.gameObject.SetActive(true);
        this.RemoveSecondUpdate();
      }
    }
  }

  private void AddSecondUpdate()
  {
    if (this.isSecondUpdate)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.isSecondUpdate = true;
  }

  private void RemoveSecondUpdate()
  {
    if (!this.isSecondUpdate)
      return;
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    this.isSecondUpdate = false;
  }

  public void OnEnable()
  {
    if (!this.isSecondUpdate)
      return;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!this.isSecondUpdate || !Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void OnExpandButtonClick()
  {
    this.detailExpended = !this.detailExpended;
  }

  [Serializable]
  protected class Panel
  {
    public int basicContainerHalfHeight = 185;
    public int detailSlotPadding = 200;
    public int detailOffset = 60;
    public UILabel indexCount;
    public UISprite playerIcon;
    public UILabel playerName;
    public UILabel troopsCount;
    public Transform marchTimeContainer;
    public UISlider marchTimeSlider;
    public UILabel marchTimeLeftText;
    public UILabel LB_Reinforced;
    public UIButton BT_SlotExpend;
    public TweenRotation TweenRotate_ExpendButton;
    public TweenScale TweenScale_ExpendDetals;
    public UIWidget detailContainer;
    public UIWidget detailsTitle;
    public RallySlotBarDetail detailBarOrg;
    public UIWidget detailBarOrgWidget;
  }
}
