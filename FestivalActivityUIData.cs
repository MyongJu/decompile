﻿// Decompiled with JetBrains decompiler
// Type: FestivalActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class FestivalActivityUIData : ActivityBaseUIData
{
  private ActivityManager.FestivalActivityData _festival;

  protected ActivityManager.FestivalActivityData Festival
  {
    get
    {
      if (this._festival == null)
        this._festival = this.Data as ActivityManager.FestivalActivityData;
      return this._festival;
    }
  }

  public override string EventKey
  {
    get
    {
      return "festival_activity";
    }
  }

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.Festival;
    }
  }

  public override bool ShowADTime
  {
    get
    {
      return false;
    }
  }

  public override int StartTime
  {
    get
    {
      return this.Festival.startTime;
    }
  }

  public override int EndTime
  {
    get
    {
      return this.Festival.endTime;
    }
  }

  public override int ActivityID
  {
    get
    {
      return this.Festival.activityId;
    }
  }

  public override IconData GetADIconData()
  {
    IconData iconData = (IconData) null;
    FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.Festival.activityId);
    if (data != null)
    {
      iconData = new IconData();
      iconData.image = data.outImagePath;
      iconData.contents = new string[2];
      iconData.contents[0] = data.Loc_outTitle;
      iconData.contents[1] = data.Loc_outDescription;
    }
    return iconData;
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = (IconData) null;
    FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.Festival.activityId);
    if (data != null)
    {
      iconData = new IconData();
      iconData.image = data.outIconPath;
      iconData.contents = new string[2];
      iconData.contents[0] = data.Loc_outTitle;
      iconData.contents[1] = data.Loc_outDescription;
      iconData.Data = (object) this;
    }
    return iconData;
  }

  public override void DisplayActivityDetail()
  {
    FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.Festival.activityId);
    if (data != null)
    {
      string type = data.type;
      if (type != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (FestivalActivityUIData.\u003C\u003Ef__switch\u0024mapD == null)
        {
          // ISSUE: reference to a compiler-generated field
          FestivalActivityUIData.\u003C\u003Ef__switch\u0024mapD = new Dictionary<string, int>(1)
          {
            {
              "exchange",
              0
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (FestivalActivityUIData.\u003C\u003Ef__switch\u0024mapD.TryGetValue(type, out num) && num == 0)
        {
          ActivityMainDlg dlg = UIManager.inst.GetDlg<ActivityMainDlg>("Activity/ActivityMainDlg");
          if (!((Object) dlg != (Object) null))
            return;
          dlg.ShowFestivalEntrance(this.ActivityID);
          return;
        }
      }
      UIManager.inst.OpenDlg("Activity/FestivalEventDlg", (UI.Dialog.DialogParameter) new ActivityFestivalDlg.Params()
      {
        festivalActivetyInternalId = this.ActivityID
      }, 1 != 0, 1 != 0, 1 != 0);
    }
    else
      UIManager.inst.OpenDlg("Activity/FestivalEventDlg", (UI.Dialog.DialogParameter) new ActivityFestivalDlg.Params()
      {
        festivalActivetyInternalId = this.ActivityID
      }, 1 != 0, 1 != 0, 1 != 0);
  }
}
