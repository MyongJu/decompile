﻿// Decompiled with JetBrains decompiler
// Type: ConfigDailyActivy
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDailyActivy
{
  private ConfigParse parse = new ConfigParse();
  private ConfigParse parse1 = new ConfigParse();
  private ConfigParse parse2 = new ConfigParse();
  private Dictionary<int, DailyActivyInfo> dailyActivies;
  private List<DailyActiveRewardInfo> dailyActivyRewardList;
  private Dictionary<int, DailyRewardFactor> dailyRewardFactors;
  private Dictionary<int, DailyRewardFactor> levle2dailyRewardFactors;
  private int _maxPoint;

  public void BuildDB_DailyActivyInfo(object res)
  {
    this.parse.Parse<DailyActivyInfo>(res as Hashtable, out this.dailyActivies);
  }

  public void BuildDB_DailyActiveRewardInfo(object res)
  {
    this.parse1.Parse<DailyActiveRewardInfo>(res as Hashtable, out this.dailyActivyRewardList);
    this.dailyActivyRewardList.Sort(new Comparison<DailyActiveRewardInfo>(this.CompareFunc));
    if (this.dailyActivyRewardList.Count <= 0)
      return;
    int requrieScore = this.dailyActivyRewardList[this.dailyActivyRewardList.Count - 1].RequrieScore;
    this._maxPoint = requrieScore;
    for (int index = 0; index < this.dailyActivyRewardList.Count; ++index)
    {
      DailyActiveRewardInfo dailyActivyReward = this.dailyActivyRewardList[index];
      dailyActivyReward.Ratio = (float) dailyActivyReward.RequrieScore / (float) requrieScore;
    }
  }

  public int MaxPoint
  {
    get
    {
      return this._maxPoint;
    }
  }

  private int CompareFunc(DailyActiveRewardInfo a, DailyActiveRewardInfo b)
  {
    return a.RequrieScore.CompareTo(b.RequrieScore);
  }

  public void BuildDB_DailyRewardFactor(object res)
  {
    this.parse2.Parse<DailyRewardFactor, int>(res as Hashtable, "StrongholdLevel", out this.levle2dailyRewardFactors, out this.dailyRewardFactors);
  }

  public DailyActiveRewardInfo GetDailyActiveRewardInfo(int id)
  {
    for (int index = 0; index < this.dailyActivyRewardList.Count; ++index)
    {
      if (this.dailyActivyRewardList[index].internalId == id)
        return this.dailyActivyRewardList[index];
    }
    return (DailyActiveRewardInfo) null;
  }

  public DailyRewardFactor GetCurrentRewardFactor()
  {
    int mLevel = CityManager.inst.mStronghold.mLevel;
    DailyRewardFactor dailyRewardFactor = (DailyRewardFactor) null;
    if (this.levle2dailyRewardFactors.ContainsKey(mLevel))
      dailyRewardFactor = this.levle2dailyRewardFactors[mLevel];
    return dailyRewardFactor;
  }

  public List<DailyActiveRewardInfo> GetTotalRewardList()
  {
    return this.dailyActivyRewardList;
  }

  public List<DailyActivyInfo> GetTotalActivyInfos()
  {
    List<DailyActivyInfo> dailyActivyInfoList = new List<DailyActivyInfo>();
    Dictionary<int, DailyActivyInfo>.Enumerator enumerator = this.dailyActivies.GetEnumerator();
    while (enumerator.MoveNext())
      dailyActivyInfoList.Add(enumerator.Current.Value);
    return dailyActivyInfoList;
  }

  public DailyActivyInfo GetDailyActivyInfo(int id)
  {
    if (this.dailyActivies.ContainsKey(id))
      return this.dailyActivies[id];
    return (DailyActivyInfo) null;
  }

  public void Clear()
  {
    if (this.dailyActivies != null)
      this.dailyActivies.Clear();
    if (this.dailyActivyRewardList != null)
      this.dailyActivyRewardList.Clear();
    if (this.dailyRewardFactors == null)
      return;
    this.dailyRewardFactors.Clear();
  }
}
