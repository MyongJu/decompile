﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightEquipmentSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightEquipmentSlot : MonoBehaviour
{
  [SerializeField]
  private GameObject m_Equipped;
  [SerializeField]
  private GameObject m_Unequipped;
  [SerializeField]
  private UITexture m_ItemIcon;
  [SerializeField]
  private UITexture m_ItemQuality;
  [SerializeField]
  private UITexture m_ItemCategory;
  [SerializeField]
  private HeroItemType m_EquipmentType;
  [SerializeField]
  private Transform m_Border;
  private DragonKnightData m_DKData;
  private int _itemId;
  private long _equipmentId;
  private long _userId;
  private InventoryItemData _inventoryItem;

  public void SetData(DragonKnightData dkData, List<InventoryItemData> equippedItemList)
  {
    this.m_DKData = dkData;
    this._inventoryItem = DragonKnightEquipmentSlot.GetEquippedItemByType(equippedItemList, (int) this.m_EquipmentType);
    if (this._inventoryItem == null)
      return;
    bool isEquipped = this._inventoryItem.isEquipped;
    this._itemId = this._inventoryItem.equipment.itemID;
    this._equipmentId = this._inventoryItem.itemId;
    this._userId = this.m_DKData.UserId;
    this.m_Equipped.SetActive(isEquipped);
    this.m_Unequipped.SetActive(!isEquipped);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this._inventoryItem.equipment.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemQuality, this._inventoryItem.equipment.QualityImagePath, (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemCategory, ConfigEquipmentMainInfo.GetCategoryPath(this.m_EquipmentType, BagType.DragonKnight), (System.Action<bool>) null, true, true, string.Empty);
  }

  private static InventoryItemData GetEquippedItemByType(List<InventoryItemData> inventoryItemDataList, int type)
  {
    for (int index = 0; index < inventoryItemDataList.Count; ++index)
    {
      InventoryItemData inventoryItemData = inventoryItemDataList[index];
      if (inventoryItemData.equipment.type == type)
        return inventoryItemData;
    }
    return (InventoryItemData) null;
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this._inventoryItem, this.m_Border);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this._itemId, this.m_Border, this._equipmentId, this._userId, 0);
  }

  public void OnItemRelese()
  {
    Utils.StopShowItemTip();
  }
}
