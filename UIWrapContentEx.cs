﻿// Decompiled with JetBrains decompiler
// Type: UIWrapContentEx
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIWrapContentEx : UIWrapContent
{
  public int rowNum = 1;
  public int itemWidth = 100;
  public int itemHeight = 100;
  private int _lastUpdateRealIndex;
  private Vector3 _lastScrollViewPos;

  protected override void ResetChildPositions()
  {
    int index = 0;
    for (int count = this.mChildren.Count; index < count; ++index)
    {
      Transform mChild = this.mChildren[index];
      mChild.localPosition = !this.mHorizontal ? new Vector3((float) (index % this.rowNum * this.itemWidth), (float) (-(index / this.rowNum) * this.itemHeight), 0.0f) : new Vector3((float) (index / this.rowNum * this.itemWidth), (float) (-(index % this.rowNum) * this.itemHeight), 0.0f);
      this.UpdateItem(mChild, index);
    }
  }

  protected override void UpdateItem(Transform item, int index)
  {
    if (this.onInitializeItem == null)
      return;
    int num1 = Mathf.Abs(Mathf.RoundToInt(item.localPosition.y / (float) this.itemHeight));
    int num2 = Mathf.Abs(Mathf.RoundToInt(item.localPosition.x / (float) this.itemWidth));
    int realIndex = !this.mHorizontal ? this.rowNum * num1 + num2 : this.rowNum * num2 + num1;
    this._lastUpdateRealIndex = realIndex;
    this._lastScrollViewPos = this.mScroll.transform.localPosition;
    this.onInitializeItem(item.gameObject, index, realIndex);
  }

  public override void SortBasedOnScrollMovement()
  {
    if (!this.CacheScrollView())
      return;
    this.mChildren.Clear();
    for (int index = 0; index < this.mTrans.childCount; ++index)
    {
      Transform child = this.mTrans.GetChild(index);
      if (child.gameObject.activeInHierarchy)
        this.mChildren.Add(child);
    }
    this.ResetChildPositions();
  }

  public override void WrapContent()
  {
    if ((Object) this.mScroll == (Object) null)
      return;
    if (this.mHorizontal && (double) this.mScroll.transform.localPosition.x > (double) this._lastScrollViewPos.x || !this.mHorizontal && (double) this.mScroll.transform.localPosition.y < (double) this._lastScrollViewPos.y)
      this._lastUpdateRealIndex = 0;
    if (this._lastUpdateRealIndex >= this.maxIndex - this.minIndex)
    {
      this.mScroll.restrictWithinPanel = true;
      this.mScroll.InvalidateBounds();
    }
    else
    {
      float num1 = (float) ((!this.mHorizontal ? this.itemHeight : this.itemWidth) * (this.mChildren.Count / this.rowNum)) * 0.5f;
      Vector3[] worldCorners = this.mPanel.worldCorners;
      for (int index = 0; index < 4; ++index)
      {
        Vector3 vector3 = this.mTrans.InverseTransformPoint(worldCorners[index]);
        worldCorners[index] = vector3;
      }
      Vector3 vector3_1 = Vector3.Lerp(worldCorners[0], worldCorners[2], 0.5f);
      bool flag = true;
      float num2 = num1 * 2f;
      if (this.mHorizontal)
      {
        float num3 = worldCorners[0].x - (float) this.itemWidth;
        float num4 = worldCorners[2].x + (float) this.itemWidth;
        int index = 0;
        for (int count = this.mChildren.Count; index < count; ++index)
        {
          Transform mChild = this.mChildren[index];
          float num5 = mChild.localPosition.x - vector3_1.x;
          if ((double) num5 < -(double) num1)
          {
            Vector3 localPosition = mChild.localPosition;
            localPosition.x += num2;
            num5 = localPosition.x - vector3_1.x;
            int num6 = Mathf.RoundToInt(localPosition.x / (float) this.itemWidth);
            if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
            {
              mChild.localPosition = localPosition;
              this.UpdateItem(mChild, index);
            }
            else
              flag = false;
          }
          else if ((double) num5 > (double) num1)
          {
            Vector3 localPosition = mChild.localPosition;
            localPosition.x -= num2;
            num5 = localPosition.x - vector3_1.x;
            int num6 = Mathf.RoundToInt(localPosition.x / (float) this.itemWidth);
            if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
            {
              mChild.localPosition = localPosition;
              this.UpdateItem(mChild, index);
            }
            else
              flag = false;
          }
          else if (this.mFirstTime)
            this.UpdateItem(mChild, index);
          if (this.cullContent)
          {
            float num6 = num5 + (this.mPanel.clipOffset.x - this.mTrans.localPosition.x);
            if (!UICamera.IsPressed(mChild.gameObject))
              NGUITools.SetActive(mChild.gameObject, (double) num6 > (double) num3 && (double) num6 < (double) num4, false);
          }
        }
      }
      else
      {
        float num3 = worldCorners[0].y - (float) this.itemHeight;
        float num4 = worldCorners[2].y + (float) this.itemHeight;
        int index = 0;
        for (int count = this.mChildren.Count; index < count; ++index)
        {
          Transform mChild = this.mChildren[index];
          if (!((Object) mChild == (Object) null))
          {
            float num5 = mChild.localPosition.y - vector3_1.y;
            if ((double) num5 < -(double) num1)
            {
              Vector3 localPosition = mChild.localPosition;
              localPosition.y += num2;
              num5 = localPosition.y - vector3_1.y;
              int num6 = Mathf.RoundToInt(localPosition.y / (float) this.itemHeight);
              if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
              {
                mChild.localPosition = localPosition;
                this.UpdateItem(mChild, index);
              }
              else
                flag = false;
            }
            else if ((double) num5 > (double) num1)
            {
              Vector3 localPosition = mChild.localPosition;
              localPosition.y -= num2;
              num5 = localPosition.y - vector3_1.y;
              int num6 = Mathf.RoundToInt(localPosition.y / (float) this.itemHeight);
              if (this.minIndex == this.maxIndex || this.minIndex <= num6 && num6 <= this.maxIndex)
              {
                mChild.localPosition = localPosition;
                this.UpdateItem(mChild, index);
              }
              else
                flag = false;
            }
            else if (this.mFirstTime)
              this.UpdateItem(mChild, index);
            if (this.cullContent)
            {
              float num6 = num5 + (this.mPanel.clipOffset.y - this.mTrans.localPosition.y);
              if (!UICamera.IsPressed(mChild.gameObject))
                NGUITools.SetActive(mChild.gameObject, (double) num6 > (double) num3 && (double) num6 < (double) num4, false);
            }
          }
        }
      }
      this.mScroll.restrictWithinPanel = !flag;
      this.mScroll.InvalidateBounds();
    }
  }

  public void RemoveScrollView()
  {
    this.mScroll = (UIScrollView) null;
  }
}
