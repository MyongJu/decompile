﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroInfo
{
  private static Dictionary<int, Color> _QualityColor = new Dictionary<int, Color>()
  {
    {
      1,
      (Color) new Color32((byte) 97, byte.MaxValue, (byte) 21, byte.MaxValue)
    },
    {
      2,
      (Color) new Color32((byte) 11, (byte) 144, byte.MaxValue, byte.MaxValue)
    },
    {
      3,
      (Color) new Color32(byte.MaxValue, (byte) 0, (byte) 206, byte.MaxValue)
    }
  };
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "art_resource")]
  public string artResourceName;
  [Config(Name = "parliament_position")]
  public int parliamentPosition;
  [Config(Name = "quality")]
  public int quality;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "disband_chips")]
  public int disbandChips;
  [Config(Name = "level_benefit")]
  public int levelBenefit;
  [Config(Name = "level_benefit_value")]
  public float levelBenefitValue;
  [Config(Name = "star_1_req")]
  public int star1Req;
  [Config(Name = "star_1_benefit")]
  public int star1Benefit;
  [Config(Name = "star_1_value")]
  public float star1Value;
  [Config(Name = "star_2_req")]
  public int star2Req;
  [Config(Name = "star_2_benefit")]
  public int star2Benefit;
  [Config(Name = "star_2_value")]
  public float star2Value;
  [Config(Name = "star_3_req")]
  public int star3Req;
  [Config(Name = "star_3_benefit")]
  public int star3Benefit;
  [Config(Name = "star_3_value")]
  public float star3Value;
  [Config(Name = "star_4_req")]
  public int star4Req;
  [Config(Name = "star_4_benefit")]
  public int star4Benefit;
  [Config(Name = "star_4_value")]
  public float star4Value;
  [Config(Name = "star_5_req")]
  public int star5Req;
  [Config(Name = "star_5_benefit")]
  public int star5Benefit;
  [Config(Name = "star_5_value")]
  public float star5Value;
  [Config(Name = "hero_suit_group")]
  public int heroSuitGroup;
  [Config(Name = "chip_item")]
  public int chipItem;
  [Config(Name = "is_new")]
  public int isNew;
  [Config(Name = "grade_rate")]
  public float gradeRate;
  private int[] _AllStarRequirement;
  private int[] _AllStarBenefit;
  private float[] _AllStarBenefitValue;

  public bool IsNew
  {
    get
    {
      return PlayerPrefsEx.GetInt("hero_card_is_new_" + this.internalId.ToString(), this.isNew) == 1;
    }
  }

  public long InitScore
  {
    get
    {
      ParliamentHeroQualityInfo parliamentHeroQualityInfo = ConfigManager.inst.DB_ParliamentHeroQuality.Get(this.quality.ToString());
      if (parliamentHeroQualityInfo != null)
        return (long) ((double) this.gradeRate * (double) (1 + parliamentHeroQualityInfo.initialStar));
      return 0;
    }
  }

  public int[] AllStarRequirement
  {
    get
    {
      if (this._AllStarRequirement == null)
        this._AllStarRequirement = new int[5]
        {
          this.star1Req,
          this.star2Req,
          this.star3Req,
          this.star4Req,
          this.star5Req
        };
      return this._AllStarRequirement;
    }
  }

  public int[] AllStarBenefit
  {
    get
    {
      if (this._AllStarBenefit == null)
        this._AllStarBenefit = new int[5]
        {
          this.star1Benefit,
          this.star2Benefit,
          this.star3Benefit,
          this.star4Benefit,
          this.star5Benefit
        };
      return this._AllStarBenefit;
    }
  }

  public float[] AllStarBenefitValue
  {
    get
    {
      if (this._AllStarBenefitValue == null)
        this._AllStarBenefitValue = new float[5]
        {
          this.star1Value,
          this.star2Value,
          this.star3Value,
          this.star4Value,
          this.star5Value
        };
      return this._AllStarBenefitValue;
    }
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public Color QualityColor
  {
    get
    {
      Color white = Color.white;
      ParliamentHeroInfo._QualityColor.TryGetValue(this.quality, out white);
      return white;
    }
  }

  public string Description
  {
    get
    {
      return Utils.XLAT(this.description);
    }
  }

  public string SmallIcon
  {
    get
    {
      return "Texture/ItemIcons/s_" + this.artResourceName;
    }
  }

  public string BigIcon
  {
    get
    {
      return "Texture/HeroCard/l_" + this.artResourceName;
    }
  }

  public int ChipCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this.chipItem);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  public int ParliamentPosition
  {
    get
    {
      int result = 1;
      ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(this.parliamentPosition);
      if (parliamentInfo != null)
        int.TryParse(parliamentInfo.id, out result);
      return result;
    }
  }

  public float GetLevelBenefitByLevel(int level)
  {
    return this.levelBenefitValue * (float) level;
  }
}
