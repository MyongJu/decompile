﻿// Decompiled with JetBrains decompiler
// Type: SQHttpPostProcessor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SQHttpPostProcessor : MonoBehaviour
{
  private string _url;
  private SQChatChecker _postData;
  private SQHttpPostProcessor.SQPostCallback _callback;

  public void DoPost(string url, SQChatChecker postData, SQHttpPostProcessor.SQPostCallback callback)
  {
    this._url = url;
    this._postData = postData;
    this._callback = callback;
    this.StartCoroutine(this.PostCoroutine());
  }

  [DebuggerHidden]
  public IEnumerator PostCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SQHttpPostProcessor.\u003CPostCoroutine\u003Ec__Iterator13()
    {
      \u003C\u003Ef__this = this
    };
  }

  public delegate void SQPostCallback(bool result, SQResponseStatus status);
}
