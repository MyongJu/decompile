﻿// Decompiled with JetBrains decompiler
// Type: LegendTowerSkillSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UnityEngine;

public class LegendTowerSkillSlot : MonoBehaviour
{
  private int _skillId;
  [SerializeField]
  private LegendTowerSkillSlot.Panel panel;

  public int skillId
  {
    get
    {
      return this._skillId;
    }
    set
    {
      if (value > 0)
      {
        this._skillId = value;
        LegendSkillInfo skillInfo = ConfigManager.inst.DB_LegendSkill.GetSkillInfo(this._skillId);
        this.panel.skillName.text = skillInfo.Loc_Name;
        this.panel.skillLevel.text = ScriptLocalization.Get("id_lv", true) + skillInfo.Level.ToString();
        this.panel.skillBenefit.text = Utils.GetSkillValueDesc(skillInfo);
        this.panel.InfoContainer.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.skillTexture, skillInfo.Image, (System.Action<bool>) null, true, false, string.Empty);
      }
      else
      {
        this.panel.skillName.text = string.Empty;
        this.panel.skillLevel.text = string.Empty;
        this.panel.skillBenefit.text = string.Empty;
        this.panel.InfoContainer.gameObject.SetActive(false);
        this.panel.skillTexture.mainTexture = (Texture) null;
      }
    }
  }

  [Serializable]
  protected class Panel
  {
    public Texture TT_empty;
    public Transform InfoContainer;
    public UITexture skillTexture;
    public UILabel skillName;
    public UILabel skillLevel;
    public UILabel skillBenefit;
  }
}
