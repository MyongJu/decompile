﻿// Decompiled with JetBrains decompiler
// Type: TradingHallBuyGoodSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TradingHallBuyGoodSlot : MonoBehaviour
{
  private Dictionary<string, string> _buyParam = new Dictionary<string, string>();
  [SerializeField]
  private UILabel _labelBuyIndex;
  [SerializeField]
  private UILabel _labelItemCount;
  [SerializeField]
  private UILabel _labelItemUnitPrice;
  private TradingHallData.BuyGoodData _buyGoodData;
  private int _buyIndex;
  private int _itemUnitPrice;
  private int _itemCount;
  private TradingHallCatalog _catalog;
  public System.Action OnBuyGoodSuccess;

  public void SetData(TradingHallData.BuyGoodData buyGoodData, int itemUnitPrice, int itemCount, int buyIndex, TradingHallCatalog catalog = null)
  {
    this._buyGoodData = buyGoodData;
    this._buyIndex = buyIndex;
    this._itemUnitPrice = itemUnitPrice;
    this._itemCount = itemCount;
    this._catalog = catalog;
    this.UpdateUI();
  }

  public void OnBuyBtnPressed()
  {
    UIManager.inst.OpenPopup("TradingHall/TradingHallBuyPopup", (Popup.PopupParameter) new TradingHallBuyPopup.Parameter()
    {
      buyGoodData = this._buyGoodData,
      itemUnitPrice = this._itemUnitPrice,
      itemCount = (long) this._itemCount,
      catalog = this._catalog,
      onBuyGoodSuccess = (System.Action) (() =>
      {
        if (this.OnBuyGoodSuccess == null)
          return;
        this.OnBuyGoodSuccess();
      })
    });
  }

  private void UpdateUI()
  {
    if (this._buyGoodData == null)
      return;
    this._labelBuyIndex.text = this._buyIndex.ToString();
    this._buyParam.Clear();
    this._buyParam.Add("0", this._itemCount.ToString());
    this._labelItemCount.text = ScriptLocalization.GetWithPara("id_in_stock", this._buyParam, true);
    this._labelItemUnitPrice.text = Utils.FormatThousands(this._itemUnitPrice.ToString());
  }
}
