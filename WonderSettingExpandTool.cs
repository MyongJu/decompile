﻿// Decompiled with JetBrains decompiler
// Type: WonderSettingExpandTool
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class WonderSettingExpandTool : MonoBehaviour
{
  private bool _isExpandEnable = true;
  private bool _isExpanded;
  [SerializeField]
  private WonderSettingExpandTool.Panel panel;

  public bool isExpanded
  {
    get
    {
      return this._isExpanded;
    }
    set
    {
      if (this._isExpanded == value)
        return;
      this._isExpanded = value;
      if ((UnityEngine.Object) this.panel.expandAreaScale != (UnityEngine.Object) null)
        this.panel.expandAreaScale.Play(this._isExpanded);
      if (!((UnityEngine.Object) this.panel.expandButtonRotate != (UnityEngine.Object) null))
        return;
      this.panel.expandButtonRotate.Play(this._isExpanded);
    }
  }

  public bool isExpandEnable
  {
    get
    {
      return this._isExpandEnable;
    }
    set
    {
      this._isExpandEnable = value;
      this.panel.expandButton.enabled = value;
    }
  }

  public void SetDetailHeight(int height)
  {
    if (!((UnityEngine.Object) this.panel.expandArea != (UnityEngine.Object) null))
      return;
    this.panel.expandArea.height = height;
  }

  public void OnExpandButtonClick()
  {
    this.isExpanded = !this.isExpanded;
  }

  public void ResetExpandArea(int height)
  {
    this.panel.expandArea.height = height;
    this.panel.expandArea.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
    this.isExpanded = false;
  }

  [Serializable]
  protected class Panel
  {
    public Transform staticArea;
    public UIWidget expandArea;
    public TweenScale expandAreaScale;
    public UIButton expandButton;
    public TweenRotation expandButtonRotate;
  }
}
