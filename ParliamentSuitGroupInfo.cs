﻿// Decompiled with JetBrains decompiler
// Type: ParliamentSuitGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class ParliamentSuitGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "suit_benefit")]
  public int suitBenefit;
  [Config(Name = "suit_req_1")]
  public int suit1Req;
  [Config(Name = "benefit_1_value")]
  public float suit1BenefitValue;
  [Config(Name = "suit_req_2")]
  public int suit2Req;
  [Config(Name = "benefit_2_value")]
  public float suit2BenefitValue;
  [Config(Name = "suit_req_3")]
  public int suit3Req;
  [Config(Name = "benefit_3_value")]
  public float suit3BenefitValue;
  [Config(Name = "suit_req_4")]
  public int suit4Req;
  [Config(Name = "benefit_4_value")]
  public float suit4BenefitValue;
  private int[] _AllSuitRequirement;
  private int[] _AllSuitBenefit;
  private float[] _AllSuitBenefitValue;

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.name, true);
    }
  }

  public int[] AllSuitRequirement
  {
    get
    {
      if (this._AllSuitRequirement == null)
        this._AllSuitRequirement = new int[4]
        {
          this.suit1Req,
          this.suit2Req,
          this.suit3Req,
          this.suit4Req
        };
      return this._AllSuitRequirement;
    }
  }

  public int[] AllSuitBenefit
  {
    get
    {
      if (this._AllSuitBenefit == null)
        this._AllSuitBenefit = new int[4]
        {
          this.suit1Req == 0 ? 0 : this.suitBenefit,
          this.suit2Req == 0 ? 0 : this.suitBenefit,
          this.suit3Req == 0 ? 0 : this.suitBenefit,
          this.suit4Req == 0 ? 0 : this.suitBenefit
        };
      return this._AllSuitBenefit;
    }
  }

  public float[] AllSuitBenefitValue
  {
    get
    {
      if (this._AllSuitBenefitValue == null)
        this._AllSuitBenefitValue = new float[4]
        {
          this.suit1BenefitValue,
          this.suit1BenefitValue + this.suit2BenefitValue,
          this.suit1BenefitValue + this.suit2BenefitValue + this.suit3BenefitValue,
          this.suit1BenefitValue + this.suit2BenefitValue + this.suit3BenefitValue + this.suit4BenefitValue
        };
      return this._AllSuitBenefitValue;
    }
  }
}
