﻿// Decompiled with JetBrains decompiler
// Type: IAPDailyPackageRewardsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class IAPDailyPackageRewardsInfo
{
  [Config(Name = "Rewards")]
  public Dictionary<string, int> Rewards = new Dictionary<string, int>();
  public int Internal_ID;
  [Config(Name = "id")]
  public string id;

  public List<Reward.RewardsValuePair> GetRewards()
  {
    List<Reward.RewardsValuePair> rewardsValuePairList = new List<Reward.RewardsValuePair>();
    using (Dictionary<string, int>.Enumerator enumerator = this.Rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        rewardsValuePairList.Add(new Reward.RewardsValuePair(int.Parse(current.Key), current.Value));
      }
    }
    return rewardsValuePairList;
  }
}
