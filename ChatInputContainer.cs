﻿// Decompiled with JetBrains decompiler
// Type: ChatInputContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Globalization;
using UI;
using UnityEngine;

public class ChatInputContainer : MonoBehaviour
{
  private string itemId = "item_megaphone";
  public GameObject normalContent;
  public GameObject allianContent;
  public ExtendUIInput normalInput;
  public ExtendUIInput allianceInput;
  private int _selectedIndex;
  public GameObject normalMegaphone;
  public GameObject megaphoneActive;
  public UILabel megaphoneCount;
  private int itemInternalId;
  public UIButton normalSendBt;
  public UIButton allianceSendBt;
  public UILabel defaultText;
  public UILabel allianDefaultText;
  public Transform keyboardRelated;
  public Transform keyboardBaseLine;
  public ChatScrollView chatScrollView;
  [Header("ModInput")]
  public GameObject inputArea;
  private Vector3 originalKeyboardRelatedPosition;
  private int orignalAbs;
  private bool isActiveMegaphone;

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      this._selectedIndex = value;
      this.ChangeState();
    }
  }

  public string InputText
  {
    get
    {
      if (this.SelectedIndex == 0)
        return this.normalInput.value;
      return this.allianceInput.value;
    }
    set
    {
      if (this.SelectedIndex == 0)
        this.normalInput.value = value;
      else
        this.allianceInput.value = value;
    }
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    this.normalInput.OnKeayboardAreaChange += new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
    this.normalInput.onValidate = new UIInput.OnValidate(this.InputValidator);
    this.allianceInput.OnKeayboardAreaChange += new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
    this.allianceInput.onValidate = new UIInput.OnValidate(this.InputValidator);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    this.normalInput.OnKeayboardAreaChange -= new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
    this.allianceInput.OnKeayboardAreaChange -= new ExtendUIInput.KeyboardAreaChange(this.HandleOnKeayboardAreaChange);
  }

  private void HandleOnKeayboardAreaChange(Rect area)
  {
    if ((double) area.height > 0.0)
    {
      Vector3 screenPoint1 = UIManager.inst.ui2DCamera.WorldToScreenPoint(this.keyboardRelated.position);
      Vector3 screenPoint2 = UIManager.inst.ui2DCamera.WorldToScreenPoint(this.keyboardBaseLine.position);
      this.keyboardRelated.position = UIManager.inst.ui2DCamera.ScreenToWorldPoint(screenPoint1 + Vector3.up * (area.height - screenPoint2.y));
      this.chatScrollView.SetMessageBottom(-1);
    }
    else
    {
      this.keyboardRelated.localPosition = this.originalKeyboardRelatedPosition;
      Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.chatScrollView.SetMessageBottom(-1)));
    }
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (this.isValidateCharacter(addedChar))
      return addedChar;
    return char.MinValue;
  }

  private bool isValidateCharacter(char codePoint)
  {
    return char.GetUnicodeCategory(codePoint) != UnicodeCategory.Surrogate;
  }

  private void OnItemDataChanged(int itemId)
  {
    if (itemId != this.itemInternalId)
      return;
    int itemCount = ItemBag.Instance.GetItemCount(itemId);
    if (itemCount < 100)
      this.megaphoneCount.text = itemCount.ToString();
    else
      this.megaphoneCount.text = "99+";
  }

  private void Start()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.itemId);
    if (itemStaticInfo != null)
    {
      this.itemInternalId = itemStaticInfo.internalId;
      int itemCount = ItemBag.Instance.GetItemCount(this.itemId);
      this.megaphoneCount.text = itemCount >= 100 ? "99+" : itemCount.ToString();
    }
    this.RemoveEventHandler();
    this.normalSendBt.isEnabled = false;
    this.allianceSendBt.isEnabled = false;
    this.AddEventHandler();
    this.originalKeyboardRelatedPosition = this.keyboardRelated.localPosition;
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
  }

  public void CheckBt()
  {
    if (this.SelectedIndex == 0)
    {
      this.normalSendBt.isEnabled = !string.IsNullOrEmpty(this.normalInput.value);
    }
    else
    {
      if (this.SelectedIndex != 1)
        return;
      this.allianceSendBt.isEnabled = !string.IsNullOrEmpty(this.allianceInput.value);
    }
  }

  private void ChangeState()
  {
    NGUITools.SetActive(this.normalContent, this.SelectedIndex == 0);
    NGUITools.SetActive(this.allianContent, this.SelectedIndex == 1);
    this.Clear();
  }

  public bool IsUseMegahone()
  {
    return this.SelectedIndex == 0 && this.isActiveMegaphone;
  }

  public void OpenMegaphone()
  {
    this.SelectedIndex = 0;
    this.isActiveMegaphone = true;
    NGUITools.SetActive(this.megaphoneActive, this.isActiveMegaphone);
  }

  public void OnMegaphoneClick()
  {
    this.isActiveMegaphone = !this.isActiveMegaphone;
    NGUITools.SetActive(this.megaphoneActive, this.isActiveMegaphone);
  }

  public void OnKingdomEmojiInputClick()
  {
    EmojiManager.Instance.ShowEmojiInput((System.Action<string>) (obj => this.normalInput.value += obj));
  }

  public void OnAllianceEmojiInputClick()
  {
    EmojiManager.Instance.ShowEmojiInput((System.Action<string>) (obj => this.allianceInput.value += obj));
  }

  public void Clear()
  {
    if (this.SelectedIndex == 0)
      this.normalInput.value = string.Empty;
    else if (this.SelectedIndex == 1)
      this.allianceInput.value = string.Empty;
    this.defaultText.text = Utils.XLAT("chat_message_character_limit");
    this.allianDefaultText.text = Utils.XLAT("chat_message_character_limit");
  }

  public void UseLongInput(bool use)
  {
    if (this.orignalAbs == 0)
      this.orignalAbs = this.inputArea.GetComponent<UIWidget>().rightAnchor.absolute;
    if (use)
      this.inputArea.GetComponent<UIWidget>().rightAnchor.absolute = this.orignalAbs + 150;
    else
      this.inputArea.GetComponent<UIWidget>().rightAnchor.absolute = this.orignalAbs;
  }
}
