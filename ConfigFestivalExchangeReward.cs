﻿// Decompiled with JetBrains decompiler
// Type: ConfigFestivalExchangeReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigFestivalExchangeReward
{
  public Dictionary<string, FestivalExchangeRewardInfo> datas;
  private Dictionary<int, FestivalExchangeRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FestivalExchangeRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public FestivalExchangeRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (FestivalExchangeRewardInfo) null;
  }

  public FestivalExchangeRewardInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (FestivalExchangeRewardInfo) null;
  }

  public void Clear()
  {
    this.datas = (Dictionary<string, FestivalExchangeRewardInfo>) null;
    this.dicByUniqueId = (Dictionary<int, FestivalExchangeRewardInfo>) null;
  }
}
