﻿// Decompiled with JetBrains decompiler
// Type: ConfigTavernWheelGrade
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigTavernWheelGrade
{
  private Dictionary<string, TavernWheelGradeInfo> m_DataByID;
  private Dictionary<int, TavernWheelGradeInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<TavernWheelGradeInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public TavernWheelGradeInfo Get(string id)
  {
    TavernWheelGradeInfo tavernWheelGradeInfo;
    this.m_DataByID.TryGetValue(id, out tavernWheelGradeInfo);
    return tavernWheelGradeInfo;
  }

  public TavernWheelGradeInfo Get(int internalId)
  {
    TavernWheelGradeInfo tavernWheelGradeInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out tavernWheelGradeInfo);
    return tavernWheelGradeInfo;
  }

  public List<TavernWheelGradeInfo> GetGradesWithGroupId(int groupId)
  {
    List<TavernWheelGradeInfo> tavernWheelGradeInfoList = new List<TavernWheelGradeInfo>();
    Dictionary<int, TavernWheelGradeInfo>.Enumerator enumerator = this.m_DataByInternalID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      TavernWheelGradeInfo tavernWheelGradeInfo = enumerator.Current.Value;
      if (tavernWheelGradeInfo.groupId == groupId)
        tavernWheelGradeInfoList.Add(tavernWheelGradeInfo);
    }
    tavernWheelGradeInfoList.Sort(new Comparison<TavernWheelGradeInfo>(this.Compare));
    return tavernWheelGradeInfoList;
  }

  private int Compare(TavernWheelGradeInfo left, TavernWheelGradeInfo right)
  {
    return left.grade - right.grade;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, TavernWheelGradeInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, TavernWheelGradeInfo>) null;
  }
}
