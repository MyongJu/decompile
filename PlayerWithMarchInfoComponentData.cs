﻿// Decompiled with JetBrains decompiler
// Type: PlayerWithMarchInfoComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PlayerWithMarchInfoComponentData : IComponentData
{
  public string user_name;
  public string portrait;
  public string icon;
  public int lord_title;
  public string acronym;
  public string k;
  public string x;
  public string y;
  public string end_x;
  public string end_y;
  public string end_time;
  public int npc_uid;
}
