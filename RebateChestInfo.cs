﻿// Decompiled with JetBrains decompiler
// Type: RebateChestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RebateChestInfo : MonoBehaviour
{
  public GameObject openedBox;
  public GameObject closedBox;
  public GameObject canCollectEffect;
  public UISprite borderPrefab;
  public UILabel boxTargetCount;

  public void SetData(RebateByRechargeRewards rebateRewards)
  {
    if (rebateRewards == null)
      return;
    if (rebateRewards.Rebated)
    {
      NGUITools.SetActive(this.openedBox, true);
      NGUITools.SetActive(this.closedBox, false);
    }
    else
    {
      NGUITools.SetActive(this.openedBox, false);
      NGUITools.SetActive(this.closedBox, true);
    }
    if (!rebateRewards.Rebated && RebateByRechargePayload.Instance.RechargedAmount >= (long) rebateRewards.TargetRechargeAmount)
    {
      Utils.PlayParticles(this.canCollectEffect);
      GreyUtility.Normal(this.closedBox);
    }
    else
    {
      Utils.StopParticles(this.canCollectEffect);
      GreyUtility.Grey(this.closedBox);
    }
    this.boxTargetCount.text = Utils.FormatThousands(rebateRewards.TargetRechargeAmount.ToString());
  }
}
