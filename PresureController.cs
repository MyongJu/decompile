﻿// Decompiled with JetBrains decompiler
// Type: PresureController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class PresureController : MonoBehaviour
{
  private const float radius = 1000f;
  private const string sourcename = "source1";
  private const string troopname = "force1";
  public Camera cinematicCamera;

  public void PlayAction(int action)
  {
    if (this.cinematicCamera.gameObject.activeSelf)
      return;
    this.cinematicCamera.gameObject.SetActive(true);
  }

  private void BeginAttack()
  {
    foreach (OpeningTroopController componentsInChild in this.GetComponentsInChildren<OpeningTroopController>())
      componentsInChild.StartCoroutine(componentsInChild.BeginMove());
  }

  public void GenerateOneTroop(bool needdragon = true, bool needtroop = true)
  {
    GameObject child1 = Utils.FindChild(this.gameObject, "source1");
    GameObject gameObject = Utils.DuplicateGOB(child1, child1.transform.parent);
    double num = (double) UnityEngine.Random.Range(0.0f, 360f) * Math.PI / 180.0;
    float y = (float) Math.Sin(num) * 1000f;
    float x = (float) Math.Cos(num) * 1000f;
    gameObject.transform.localPosition = new Vector3(x, y, 0.0f);
    GameObject child2 = Utils.FindChild(this.gameObject, "force1");
    OpeningTroopController component = Utils.DuplicateGOB(child2, child2.transform.parent).GetComponent<OpeningTroopController>();
    component.source = gameObject.transform;
    component.hasDragon = needdragon;
    component.hasCavalry = needtroop;
    component.hasInfantry = needtroop;
    component.hasRanged = needtroop;
    component.hasSiege = needtroop;
    component.StartCoroutine(component.BeginMove());
  }
}
