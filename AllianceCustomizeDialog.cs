﻿// Decompiled with JetBrains decompiler
// Type: AllianceCustomizeDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class AllianceCustomizeDialog : UI.Dialog
{
  private const int CHANGE_NAME = 0;
  private const int CHANGE_TAG = 1;
  private const int CHANGE_SLOGAN = 2;
  private const int RECRUITMENT = 3;
  private const int REQUIREMENT = 4;
  private const int LANGUAGE = 5;
  private const int RANKING = 6;
  private const int FORTRESS_NAME = 7;
  public AllianceSymbol m_AllianceSymbol;
  public UITable m_Table;
  public UIScrollView m_ScrollView;
  public AllianceCustomizeHeader[] m_Headers;
  public UIButton m_ChangeSymbol;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceData allianceData = PlayerData.inst.allianceData;
    this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode);
    for (int index = 0; index < this.m_Headers.Length; ++index)
      this.m_Headers[index].SetData(allianceData, false, new System.Action(this.OnItemChanged));
    this.UpdatePrivileges();
    this.m_ScrollView.ResetPosition();
  }

  private void UpdatePrivileges()
  {
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    this.m_ChangeSymbol.gameObject.SetActive(allianceMemberData.title == 6);
    this.m_Headers[0].gameObject.SetActive(allianceMemberData.title == 6);
    this.m_Headers[1].gameObject.SetActive(allianceMemberData.title == 6);
    this.m_Headers[2].gameObject.SetActive(allianceMemberData.title == 6 || allianceMemberData.title == 4);
    this.m_Headers[3].gameObject.SetActive(allianceMemberData.title == 6 || allianceMemberData.title == 4);
    this.m_Headers[4].gameObject.SetActive(allianceMemberData.title == 6 || allianceMemberData.title == 4);
    this.m_Headers[5].gameObject.SetActive(allianceMemberData.title == 6 || allianceMemberData.title == 4);
    this.m_Headers[6].gameObject.SetActive(allianceMemberData.title == 6);
    this.m_Headers[7].gameObject.SetActive(allianceMemberData.title == 6);
    this.m_Table.Reposition();
  }

  private void OnItemChanged()
  {
    this.m_Table.Reposition();
  }

  public void OnCustomizePressed()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    PopupManager.Instance.Open<AllianceSymbolPopup>("AllianceSymbolPopup").Initialize(allianceData.allianceSymbolCode, (System.Action<int>) (symbol =>
    {
      this.m_AllianceSymbol.SetSymbols(symbol);
      AllianceManager.Instance.Customize(allianceData.allianceName, allianceData.allianceAcronym, symbol, allianceData.language, allianceData.publicMessage, (long) allianceData.powerLimit, allianceData.isPirvate, (System.Action<bool, object>) ((ret, data) => this.m_AllianceSymbol.SetSymbols(allianceData.allianceSymbolCode)));
    }));
  }
}
