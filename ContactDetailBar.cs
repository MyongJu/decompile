﻿// Decompiled with JetBrains decompiler
// Type: ContactDetailBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class ContactDetailBar : MonoBehaviour
{
  private bool _isOnline = true;
  public System.Action<long> onBarClick;
  [SerializeField]
  private ContactDetailBar.Panel panel;

  public long uid { get; private set; }

  public bool isOnline
  {
    get
    {
      return this._isOnline;
    }
    set
    {
      if (this._isOnline == value)
        return;
      this._isOnline = value;
      this.panel.state.color = !this._isOnline ? this.panel.offlineColor : this.panel.onlineColor;
    }
  }

  public void OnBarClick()
  {
    if (this.onBarClick == null)
      return;
    this.onBarClick(this.uid);
  }

  public void OnManagerButtonClick()
  {
    this.onBarClick(this.uid);
  }

  public void OnPlayerInfoButtonClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this.uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void Setup(long inUid, bool onlineState)
  {
    this.isOnline = onlineState;
    this.uid = inUid;
    UserData userData = DBManager.inst.DB_User.Get(inUid);
    if (userData == null)
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.panel.playerIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.panel.playerIcon, userData.LordTitle, 1);
    this.panel.playerName.text = userData.userName_Kingdom_Alliance_Name;
  }

  [Serializable]
  protected class Panel
  {
    [Tooltip("The Contact bar online color")]
    public Color onlineColor;
    [Tooltip("The Contact bar offline color")]
    public Color offlineColor;
    public UISprite state;
    public UITexture playerIcon;
    public UILabel playerName;
    public UIButton BT_Bar;
    public UIButton BT_Manage;
  }
}
