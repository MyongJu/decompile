﻿// Decompiled with JetBrains decompiler
// Type: GemEnhanceConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GemEnhanceConfirmPopup : Popup
{
  public GemEnhanceConfirmItem template;
  public UITable table;
  public System.Action OnRequestFinished;
  private long mainGemId;
  private List<KeyValuePair<long, long>> ateGems;

  private int SortRule(KeyValuePair<long, long> x, KeyValuePair<long, long> y)
  {
    GemData gemData1 = DBManager.inst.DB_Gems.Get(x.Key);
    GemData gemData2 = DBManager.inst.DB_Gems.Get(y.Key);
    if (gemData1 == null || gemData2 == null)
    {
      D.error((object) "GemEnhanceConfirmPopup Init error. gem data not found");
      return 0;
    }
    if (GemManager.Instance.IsExpGem(gemData1) && !GemManager.Instance.IsExpGem(gemData2))
      return 1;
    if (!GemManager.Instance.IsExpGem(gemData1) && GemManager.Instance.IsExpGem(gemData2))
      return -1;
    if (gemData1.Level == gemData2.Level)
      return gemData1.ConfigId.CompareTo(gemData2.ConfigId);
    return -gemData1.Level.CompareTo(gemData2.Level);
  }

  private void Init()
  {
    this.ateGems.Sort(new Comparison<KeyValuePair<long, long>>(this.SortRule));
    List<int> intList = new List<int>();
    Dictionary<int, long> dictionary1 = new Dictionary<int, long>();
    List<KeyValuePair<long, long>>.Enumerator enumerator1 = this.ateGems.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      long key = enumerator1.Current.Key;
      long num = enumerator1.Current.Value;
      GemData gemData = DBManager.inst.DB_Gems.Get(key);
      if (!dictionary1.ContainsKey(gemData.ConfigId))
      {
        dictionary1.Add(gemData.ConfigId, num);
        intList.Add(gemData.ConfigId);
      }
      else
      {
        Dictionary<int, long> dictionary2;
        int configId;
        (dictionary2 = dictionary1)[configId = gemData.ConfigId] = dictionary2[configId] + num;
      }
    }
    List<int>.Enumerator enumerator2 = intList.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      GameObject go = NGUITools.AddChild(this.table.gameObject, this.template.gameObject);
      NGUITools.SetActive(go, true);
      int current = enumerator2.Current;
      ConfigEquipmentGemInfo data = ConfigManager.inst.DB_EquipmentGem.GetData(current);
      if (data != null)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data.itemID);
        if (itemStaticInfo != null)
        {
          GemEnhanceConfirmItem component = go.GetComponent<GemEnhanceConfirmItem>();
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
            component.setGemDesc(string.Format("{0} × {1}", (object) itemStaticInfo.LocName, (object) dictionary1[current]));
        }
      }
    }
    this.table.Reposition();
  }

  public void OnConfirmBtnClick()
  {
    Dictionary<long, long> ateGemIds = new Dictionary<long, long>();
    List<KeyValuePair<long, long>>.Enumerator enumerator = this.ateGems.GetEnumerator();
    while (enumerator.MoveNext())
      ateGemIds.Add(enumerator.Current.Key, enumerator.Current.Value);
    GemManager.Instance.Upgrade(this.mainGemId, ateGemIds, (System.Action<bool, object>) ((ret, obj) =>
    {
      if (!ret)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      if (this.OnRequestFinished != null)
        this.OnRequestFinished();
      AudioManager.Instance.StopAndPlaySound("sfx_city_gemstone_refine");
    }));
  }

  private void Dispose()
  {
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    if (orgParam == null)
      return;
    this.mainGemId = ((GemEnhanceConfirmPopup.Parameter) orgParam).mainGemId;
    this.ateGems = ((GemEnhanceConfirmPopup.Parameter) orgParam).ateGems;
    this.OnRequestFinished = ((GemEnhanceConfirmPopup.Parameter) orgParam).onRequestFinished;
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long mainGemId;
    public List<KeyValuePair<long, long>> ateGems;
    public System.Action onRequestFinished;
  }
}
