﻿// Decompiled with JetBrains decompiler
// Type: ArmyView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ArmyView : MarchView
{
  public static float BLANK_LINE_STEP = 3f;
  private static MarchViewControler.TroopViewType[] troopTypeOrder = new MarchViewControler.TroopViewType[8]
  {
    MarchViewControler.TroopViewType.class_infantry,
    MarchViewControler.TroopViewType.class_cavalry,
    MarchViewControler.TroopViewType.class_dragon,
    MarchViewControler.TroopViewType.class_ranged,
    MarchViewControler.TroopViewType.class_artyfar,
    MarchViewControler.TroopViewType.class_darkKnight_1,
    MarchViewControler.TroopViewType.class_darkKnight_2,
    MarchViewControler.TroopViewType.class_darkKnight_3
  };
  private int[] _troopsCount = new int[8];
  private const string troopIconPath = "Prefab/TroopModels/TroopIcon";
  private List<ArmyFormation> _singleTypeFormations;
  private float armyLength;
  private int _checkTime;
  private ArmyView.LODLEVEL curLod;
  private GameObject lodIcon;

  public override void CreateModel(System.Action callback)
  {
    this._trans = this.transform;
    this._singleTypeFormations = new List<ArmyFormation>(ArmyView.troopTypeOrder.Length);
    for (int index = 0; index < ArmyView.troopTypeOrder.Length; ++index)
    {
      GameObject gameObject = new GameObject();
      gameObject.name = ArmyView.troopTypeOrder[index].ToString();
      gameObject.transform.parent = this._trans;
      gameObject.transform.localScale = Vector3.one;
      gameObject.transform.localPosition = Vector3.zero;
      this._singleTypeFormations.Add(gameObject.AddComponent<ArmyFormation>());
    }
    if (callback != null)
      callback();
    this._checkTime = NetServerTime.inst.ServerTimestamp;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.CheckFramerate);
  }

  public override float Setup(MarchViewControler.Data troopData, bool setImd, System.Action callback = null)
  {
    this.data = troopData;
    for (int index = 0; index < ArmyView.troopTypeOrder.Length; ++index)
      this._troopsCount[index] = 0;
    if (this.data.troops.troops != null)
    {
      NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid(this.data.ownerUid);
      if (dataByUid != null)
      {
        string empty = string.Empty;
        string para2 = dataByUid.para_2;
        if (para2 != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (ArmyView.\u003C\u003Ef__switch\u0024map84 == null)
          {
            // ISSUE: reference to a compiler-generated field
            ArmyView.\u003C\u003Ef__switch\u0024map84 = new Dictionary<string, int>(3)
            {
              {
                "event_fallen_knight_troop",
                0
              },
              {
                "event_fallen_knight_lord",
                1
              },
              {
                "event_fallen_knight_army",
                2
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (ArmyView.\u003C\u003Ef__switch\u0024map84.TryGetValue(para2, out num))
          {
            switch (num)
            {
              case 0:
                this._troopsCount[5] = 1;
                goto label_17;
              case 1:
                this._troopsCount[6] = 1;
                goto label_17;
              case 2:
                this._troopsCount[7] = 1;
                goto label_17;
            }
          }
        }
        this._troopsCount[5] = 1;
      }
      else
      {
        Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = troopData.troops.troops.Keys.GetEnumerator();
        while (enumerator.MoveNext())
          this._troopsCount[(int) MarchViewControler.ChangeToTroopViewType(ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator.Current).GetUnitType().ToString())] += troopData.troops.troops[enumerator.Current].Count;
      }
    }
label_17:
    this._troopsCount[2] = troopData.marchData == null || troopData.marchData.dragon == null || !troopData.marchData.dragon.isDragonIn ? 0 : 1;
    this.armyLength = 0.0f;
    int armyTypes = ArmyView.troopTypeOrder.Length;
    for (int index = 0; index < ArmyView.troopTypeOrder.Length; ++index)
    {
      if (this._troopsCount[(int) ArmyView.troopTypeOrder[index]] == 0)
        --armyTypes;
    }
    int armyFormationsCouter = 0;
    ArmyFormation.Param obj = new ArmyFormation.Param();
    for (int index = 0; index < ArmyView.troopTypeOrder.Length; ++index)
    {
      if (this._troopsCount[(int) ArmyView.troopTypeOrder[index]] == 0)
      {
        this._singleTypeFormations[index].gameObject.SetActive(false);
      }
      else
      {
        obj.type = ArmyView.troopTypeOrder[index];
        obj.troopsCount = this._troopsCount[(int) ArmyView.troopTypeOrder[index]];
        obj.deltaPos = this.armyLength;
        obj.setImd = true;
        if (ArmyView.troopTypeOrder[index] == MarchViewControler.TroopViewType.class_dragon)
        {
          obj.dragonLevel = troopData.marchData.dragon.level;
          obj.dragonTendency = troopData.marchData.dragon.tendency;
        }
        this._singleTypeFormations[index].gameObject.SetActive(true);
        this.armyLength += this._singleTypeFormations[index].SetUnits(obj, (System.Action) (() =>
        {
          ++armyFormationsCouter;
          if (armyFormationsCouter != armyTypes)
            return;
          callback();
        }));
      }
    }
    return this.armyLength;
  }

  public override void Show()
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Show();
    }
  }

  public override void Hide()
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Hide();
    }
  }

  public override void FadeIn(Vector3 showPosition, float delayTime, bool isDisplayByLine = true)
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].FadeIn(delayTime, showPosition, isDisplayByLine);
    }
  }

  public override void FadeOut(float delayTime, bool isDisplayByLine = true)
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].FadeOut(delayTime, isDisplayByLine);
    }
  }

  public override void Rotate(Vector3 dir)
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Rotate(dir, true);
    }
  }

  public override void Move()
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Move();
    }
  }

  public override void Action(int actionIndex, Vector3 deltaPos, float radius = 1f)
  {
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Action(actionIndex, deltaPos, radius);
    }
  }

  public override void Dispose()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.CheckFramerate);
    base.Dispose();
    for (int index = 0; index < this._singleTypeFormations.Count; ++index)
    {
      if (this._singleTypeFormations[index].gameObject.activeSelf)
        this._singleTypeFormations[index].Dispose();
    }
  }

  public void SetLOD(ArmyView.LODLEVEL lod)
  {
    if (this.curLod >= lod)
      return;
    this.curLod = lod;
    if (this._singleTypeFormations != null)
    {
      using (List<ArmyFormation>.Enumerator enumerator = this._singleTypeFormations.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ArmyFormation current = enumerator.Current;
          if ((UnityEngine.Object) null != (UnityEngine.Object) current)
            current.SetLOD(lod);
        }
      }
    }
    if (lod != ArmyView.LODLEVEL.LOD2)
      return;
    UnityEngine.Object @object = AssetManager.Instance.HandyLoad("Prefab/TroopModels/TroopIcon", (System.Type) null);
    if (!((UnityEngine.Object) null != @object))
      return;
    this.lodIcon = Utils.DuplicateGOB(@object as GameObject, this.transform);
  }

  private ArmyView.LODLEVEL lodLevel
  {
    get
    {
      if ((double) FramerateSampler.Instance.CurrentFPS > 20.0)
        return ArmyView.LODLEVEL.NORMAL;
      return (double) FramerateSampler.Instance.CurrentFPS <= 20.0 && (double) FramerateSampler.Instance.CurrentFPS > 10.0 ? ArmyView.LODLEVEL.LOD1 : ArmyView.LODLEVEL.LOD2;
    }
  }

  private void CheckFramerate(int time)
  {
    try
    {
      if (time <= this._checkTime + 2)
        return;
      this.SetLOD(this.lodLevel);
    }
    catch
    {
    }
  }

  public enum LODLEVEL
  {
    NORMAL,
    LOD1,
    LOD2,
  }
}
