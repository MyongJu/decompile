﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Log.SimpleLog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace TriangleNet.Log
{
  public sealed class SimpleLog : ILog<SimpleLogItem>
  {
    private static readonly SimpleLog instance = new SimpleLog();
    private List<SimpleLogItem> log = new List<SimpleLogItem>();
    private LogLevel level;

    private SimpleLog()
    {
    }

    public static ILog<SimpleLogItem> Instance
    {
      get
      {
        return (ILog<SimpleLogItem>) SimpleLog.instance;
      }
    }

    public void Add(SimpleLogItem item)
    {
      this.log.Add(item);
    }

    public void Clear()
    {
      this.log.Clear();
    }

    public void Info(string message)
    {
      this.log.Add(new SimpleLogItem(LogLevel.Info, message));
    }

    public void Warning(string message, string location)
    {
      this.log.Add(new SimpleLogItem(LogLevel.Warning, message, location));
    }

    public void Error(string message, string location)
    {
      this.log.Add(new SimpleLogItem(LogLevel.Error, message, location));
    }

    public IList<SimpleLogItem> Data
    {
      get
      {
        return (IList<SimpleLogItem>) this.log;
      }
    }

    public LogLevel Level
    {
      get
      {
        return this.level;
      }
    }
  }
}
