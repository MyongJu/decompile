﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Log.ILog`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace TriangleNet.Log
{
  public interface ILog<T> where T : ILogItem
  {
    void Add(T item);

    void Clear();

    void Info(string message);

    void Error(string message, string info);

    void Warning(string message, string info);

    IList<T> Data { get; }

    LogLevel Level { get; }
  }
}
