﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Log.SimpleLogItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace TriangleNet.Log
{
  public class SimpleLogItem : ILogItem
  {
    private DateTime time;
    private LogLevel level;
    private string message;
    private string info;

    public SimpleLogItem(LogLevel level, string message)
      : this(level, message, string.Empty)
    {
    }

    public SimpleLogItem(LogLevel level, string message, string info)
    {
      this.time = DateTime.Now;
      this.level = level;
      this.message = message;
      this.info = info;
    }

    public DateTime Time
    {
      get
      {
        return this.time;
      }
    }

    public LogLevel Level
    {
      get
      {
        return this.level;
      }
    }

    public string Message
    {
      get
      {
        return this.message;
      }
    }

    public string Info
    {
      get
      {
        return this.info;
      }
    }
  }
}
