﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Smoothing.SimpleSmoother
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TriangleNet.Data;
using TriangleNet.Geometry;
using TriangleNet.Tools;

namespace TriangleNet.Smoothing
{
  public class SimpleSmoother : ISmoother
  {
    private Mesh mesh;

    public SimpleSmoother(Mesh mesh)
    {
      this.mesh = mesh;
    }

    public void Smooth()
    {
      this.mesh.behavior.Quality = false;
      for (int index = 0; index < 5; ++index)
      {
        this.Step();
        this.mesh.Triangulate(this.Rebuild());
      }
    }

    private void Step()
    {
      using (List<VoronoiRegion>.Enumerator enumerator1 = new BoundedVoronoi(this.mesh, false).Regions.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          VoronoiRegion current1 = enumerator1.Current;
          int num1 = 0;
          double num2;
          double num3 = num2 = 0.0;
          using (List<Point>.Enumerator enumerator2 = current1.Vertices.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Point current2 = enumerator2.Current;
              ++num1;
              num3 += current2.x;
              num2 += current2.y;
            }
          }
          current1.Generator.x = num3 / (double) num1;
          current1.Generator.y = num2 / (double) num1;
        }
      }
    }

    private InputGeometry Rebuild()
    {
      InputGeometry inputGeometry = new InputGeometry(this.mesh.vertices.Count);
      using (Dictionary<int, Vertex>.ValueCollection.Enumerator enumerator = this.mesh.vertices.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Vertex current = enumerator.Current;
          inputGeometry.AddPoint(current.x, current.y, current.mark);
        }
      }
      using (Dictionary<int, Segment>.ValueCollection.Enumerator enumerator = this.mesh.subsegs.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Segment current = enumerator.Current;
          inputGeometry.AddSegment(current.P0, current.P1, current.Boundary);
        }
      }
      using (List<Point>.Enumerator enumerator = this.mesh.holes.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Point current = enumerator.Current;
          inputGeometry.AddHole(current.x, current.y);
        }
      }
      using (List<RegionPointer>.Enumerator enumerator = this.mesh.regions.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RegionPointer current = enumerator.Current;
          inputGeometry.AddRegion(current.point.x, current.point.y, current.id);
        }
      }
      return inputGeometry;
    }
  }
}
