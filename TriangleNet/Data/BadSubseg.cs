﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Data.BadSubseg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TriangleNet.Data
{
  internal class BadSubseg
  {
    private static int hashSeed;
    internal int Hash;
    public Osub encsubseg;
    public Vertex subsegorg;
    public Vertex subsegdest;

    public BadSubseg()
    {
      this.Hash = BadSubseg.hashSeed++;
    }

    public override int GetHashCode()
    {
      return this.Hash;
    }

    public override string ToString()
    {
      return string.Format("B-SID {0}", (object) this.encsubseg.seg.hash);
    }
  }
}
