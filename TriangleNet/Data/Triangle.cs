﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Data.Triangle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using TriangleNet.Geometry;

namespace TriangleNet.Data
{
  public class Triangle : ITriangle
  {
    internal int hash;
    internal int id;
    internal Otri[] neighbors;
    internal Vertex[] vertices;
    internal Osub[] subsegs;
    internal int region;
    internal double area;
    internal bool infected;

    public Triangle()
    {
      this.neighbors = new Otri[3];
      this.neighbors[0].triangle = Mesh.dummytri;
      this.neighbors[1].triangle = Mesh.dummytri;
      this.neighbors[2].triangle = Mesh.dummytri;
      this.vertices = new Vertex[3];
      this.subsegs = new Osub[3];
      this.subsegs[0].seg = Mesh.dummysub;
      this.subsegs[1].seg = Mesh.dummysub;
      this.subsegs[2].seg = Mesh.dummysub;
    }

    public int ID
    {
      get
      {
        return this.id;
      }
    }

    public int P0
    {
      get
      {
        if ((Point) this.vertices[0] == (Point) null)
          return -1;
        return this.vertices[0].id;
      }
    }

    public int P1
    {
      get
      {
        if ((Point) this.vertices[1] == (Point) null)
          return -1;
        return this.vertices[1].id;
      }
    }

    public Vertex GetVertex(int index)
    {
      return this.vertices[index];
    }

    public int P2
    {
      get
      {
        if ((Point) this.vertices[2] == (Point) null)
          return -1;
        return this.vertices[2].id;
      }
    }

    public bool SupportsNeighbors
    {
      get
      {
        return true;
      }
    }

    public int N0
    {
      get
      {
        return this.neighbors[0].triangle.id;
      }
    }

    public int N1
    {
      get
      {
        return this.neighbors[1].triangle.id;
      }
    }

    public int N2
    {
      get
      {
        return this.neighbors[2].triangle.id;
      }
    }

    public ITriangle GetNeighbor(int index)
    {
      if (this.neighbors[index].triangle == Mesh.dummytri)
        return (ITriangle) null;
      return (ITriangle) this.neighbors[index].triangle;
    }

    public ISegment GetSegment(int index)
    {
      if (this.subsegs[index].seg == Mesh.dummysub)
        return (ISegment) null;
      return (ISegment) this.subsegs[index].seg;
    }

    public double Area
    {
      get
      {
        return this.area;
      }
      set
      {
        this.area = value;
      }
    }

    public int Region
    {
      get
      {
        return this.region;
      }
    }

    public override int GetHashCode()
    {
      return this.hash;
    }

    public override string ToString()
    {
      return string.Format("TID {0}", (object) this.hash);
    }
  }
}
