﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Data.BadTriangle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TriangleNet.Data
{
  internal class BadTriangle
  {
    public static int OTID;
    public int ID;
    public Otri poortri;
    public double key;
    public Vertex triangorg;
    public Vertex triangdest;
    public Vertex triangapex;
    public BadTriangle nexttriang;

    public BadTriangle()
    {
      this.ID = BadTriangle.OTID++;
    }

    public override string ToString()
    {
      return string.Format("B-TID {0}", (object) this.poortri.triangle.hash);
    }
  }
}
