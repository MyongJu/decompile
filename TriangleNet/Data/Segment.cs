﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Data.Segment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using TriangleNet.Geometry;

namespace TriangleNet.Data
{
  public class Segment : ISegment
  {
    internal int hash;
    internal Osub[] subsegs;
    internal Vertex[] vertices;
    internal Otri[] triangles;
    internal int boundary;

    public Segment()
    {
      this.subsegs = new Osub[2];
      this.subsegs[0].seg = Mesh.dummysub;
      this.subsegs[1].seg = Mesh.dummysub;
      this.vertices = new Vertex[4];
      this.triangles = new Otri[2];
      this.triangles[0].triangle = Mesh.dummytri;
      this.triangles[1].triangle = Mesh.dummytri;
      this.boundary = 0;
    }

    public int P0
    {
      get
      {
        return this.vertices[0].id;
      }
    }

    public int P1
    {
      get
      {
        return this.vertices[1].id;
      }
    }

    public int Boundary
    {
      get
      {
        return this.boundary;
      }
    }

    public Vertex GetVertex(int index)
    {
      return this.vertices[index];
    }

    public ITriangle GetTriangle(int index)
    {
      if (this.triangles[index].triangle == Mesh.dummytri)
        return (ITriangle) null;
      return (ITriangle) this.triangles[index].triangle;
    }

    public override int GetHashCode()
    {
      return this.hash;
    }

    public override string ToString()
    {
      return string.Format("SID {0}", (object) this.hash);
    }
  }
}
