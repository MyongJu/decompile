﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.IO.FileWriter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TriangleNet.Data;
using TriangleNet.Geometry;

namespace TriangleNet.IO
{
  public static class FileWriter
  {
    private static NumberFormatInfo nfi = CultureInfo.InvariantCulture.NumberFormat;

    public static void Write(Mesh mesh, string filename)
    {
      FileWriter.WritePoly(mesh, Path.ChangeExtension(filename, ".poly"));
      FileWriter.WriteElements(mesh, Path.ChangeExtension(filename, ".ele"));
    }

    public static void WriteNodes(Mesh mesh, string filename)
    {
      using (StreamWriter writer = new StreamWriter(filename))
        FileWriter.WriteNodes(writer, mesh);
    }

    private static void WriteNodes(StreamWriter writer, Mesh mesh)
    {
      int num = mesh.vertices.Count;
      Behavior behavior = mesh.behavior;
      if (behavior.Jettison)
        num = mesh.vertices.Count - mesh.undeads;
      if (writer == null)
        return;
      writer.WriteLine("{0} {1} {2} {3}", (object) num, (object) mesh.mesh_dim, (object) mesh.nextras, !behavior.UseBoundaryMarkers ? (object) "0" : (object) "1");
      if (mesh.numbering == NodeNumbering.None)
        mesh.Renumber();
      if (mesh.numbering == NodeNumbering.Linear)
      {
        FileWriter.WriteNodes(writer, (IEnumerable<Vertex>) mesh.vertices.Values, behavior.UseBoundaryMarkers, mesh.nextras, behavior.Jettison);
      }
      else
      {
        Vertex[] vertexArray = new Vertex[mesh.vertices.Count];
        using (Dictionary<int, Vertex>.ValueCollection.Enumerator enumerator = mesh.vertices.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Vertex current = enumerator.Current;
            vertexArray[current.id] = current;
          }
        }
        FileWriter.WriteNodes(writer, (IEnumerable<Vertex>) vertexArray, behavior.UseBoundaryMarkers, mesh.nextras, behavior.Jettison);
      }
    }

    private static void WriteNodes(StreamWriter writer, IEnumerable<Vertex> nodes, bool markers, int attribs, bool jettison)
    {
      int num = 0;
      foreach (Vertex node in nodes)
      {
        if (!jettison || node.type != VertexType.UndeadVertex)
        {
          writer.Write("{0} {1} {2}", (object) num, (object) node.x.ToString((IFormatProvider) FileWriter.nfi), (object) node.y.ToString((IFormatProvider) FileWriter.nfi));
          for (int index = 0; index < attribs; ++index)
            writer.Write(" {0}", (object) node.attributes[index].ToString((IFormatProvider) FileWriter.nfi));
          if (markers)
            writer.Write(" {0}", (object) node.mark);
          writer.WriteLine();
          ++num;
        }
      }
    }

    public static void WriteElements(Mesh mesh, string filename)
    {
      Otri otri = new Otri();
      bool useRegions = mesh.behavior.useRegions;
      int num = 0;
      otri.orient = 0;
      using (StreamWriter streamWriter = new StreamWriter(filename))
      {
        streamWriter.WriteLine("{0} 3 {1}", (object) mesh.triangles.Count, (object) (!useRegions ? 0 : 1));
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            Vertex vertex1 = otri.Org();
            Vertex vertex2 = otri.Dest();
            Vertex vertex3 = otri.Apex();
            streamWriter.Write("{0} {1} {2} {3}", (object) num, (object) vertex1.id, (object) vertex2.id, (object) vertex3.id);
            if (useRegions)
              streamWriter.Write(" {0}", (object) otri.triangle.region);
            streamWriter.WriteLine();
            current.id = num++;
          }
        }
      }
    }

    public static void WritePoly(Mesh mesh, string filename)
    {
      FileWriter.WritePoly(mesh, filename, true);
    }

    public static void WritePoly(Mesh mesh, string filename, bool writeNodes)
    {
      Osub osub = new Osub();
      bool useBoundaryMarkers = mesh.behavior.UseBoundaryMarkers;
      using (StreamWriter writer = new StreamWriter(filename))
      {
        if (writeNodes)
          FileWriter.WriteNodes(writer, mesh);
        else
          writer.WriteLine("0 {0} {1} {2}", (object) mesh.mesh_dim, (object) mesh.nextras, !useBoundaryMarkers ? (object) "0" : (object) "1");
        writer.WriteLine("{0} {1}", (object) mesh.subsegs.Count, !useBoundaryMarkers ? (object) "0" : (object) "1");
        osub.orient = 0;
        int num1 = 0;
        using (Dictionary<int, Segment>.ValueCollection.Enumerator enumerator = mesh.subsegs.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Segment current = enumerator.Current;
            osub.seg = current;
            Vertex vertex1 = osub.Org();
            Vertex vertex2 = osub.Dest();
            if (useBoundaryMarkers)
              writer.WriteLine("{0} {1} {2} {3}", (object) num1, (object) vertex1.id, (object) vertex2.id, (object) osub.seg.boundary);
            else
              writer.WriteLine("{0} {1} {2}", (object) num1, (object) vertex1.id, (object) vertex2.id);
            ++num1;
          }
        }
        int num2 = 0;
        writer.WriteLine("{0}", (object) mesh.holes.Count);
        using (List<Point>.Enumerator enumerator = mesh.holes.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Point current = enumerator.Current;
            writer.WriteLine("{0} {1} {2}", (object) num2++, (object) current.X.ToString((IFormatProvider) FileWriter.nfi), (object) current.Y.ToString((IFormatProvider) FileWriter.nfi));
          }
        }
        if (mesh.regions.Count <= 0)
          return;
        int num3 = 0;
        writer.WriteLine("{0}", (object) mesh.regions.Count);
        using (List<RegionPointer>.Enumerator enumerator = mesh.regions.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            RegionPointer current = enumerator.Current;
            writer.WriteLine("{0} {1} {2} {3}", (object) num3, (object) current.point.X.ToString((IFormatProvider) FileWriter.nfi), (object) current.point.Y.ToString((IFormatProvider) FileWriter.nfi), (object) current.id);
            ++num3;
          }
        }
      }
    }

    public static void WriteEdges(Mesh mesh, string filename)
    {
      Otri otri = new Otri();
      Otri o2 = new Otri();
      Osub os = new Osub();
      Behavior behavior = mesh.behavior;
      using (StreamWriter streamWriter = new StreamWriter(filename))
      {
        streamWriter.WriteLine("{0} {1}", (object) mesh.edges, !behavior.UseBoundaryMarkers ? (object) "0" : (object) "1");
        long num = 0;
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            for (otri.orient = 0; otri.orient < 3; ++otri.orient)
            {
              otri.Sym(ref o2);
              if (otri.triangle.id < o2.triangle.id || o2.triangle == Mesh.dummytri)
              {
                Vertex vertex1 = otri.Org();
                Vertex vertex2 = otri.Dest();
                if (behavior.UseBoundaryMarkers)
                {
                  if (behavior.useSegments)
                  {
                    otri.SegPivot(ref os);
                    if (os.seg == Mesh.dummysub)
                      streamWriter.WriteLine("{0} {1} {2} {3}", (object) num, (object) vertex1.id, (object) vertex2.id, (object) 0);
                    else
                      streamWriter.WriteLine("{0} {1} {2} {3}", (object) num, (object) vertex1.id, (object) vertex2.id, (object) os.seg.boundary);
                  }
                  else
                    streamWriter.WriteLine("{0} {1} {2} {3}", (object) num, (object) vertex1.id, (object) vertex2.id, o2.triangle != Mesh.dummytri ? (object) "0" : (object) "1");
                }
                else
                  streamWriter.WriteLine("{0} {1} {2}", (object) num, (object) vertex1.id, (object) vertex2.id);
                ++num;
              }
            }
          }
        }
      }
    }

    public static void WriteNeighbors(Mesh mesh, string filename)
    {
      Otri otri = new Otri();
      Otri o2 = new Otri();
      int num = 0;
      using (StreamWriter streamWriter = new StreamWriter(filename))
      {
        streamWriter.WriteLine("{0} 3", (object) mesh.triangles.Count);
        Mesh.dummytri.id = -1;
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            otri.orient = 1;
            otri.Sym(ref o2);
            int id1 = o2.triangle.id;
            otri.orient = 2;
            otri.Sym(ref o2);
            int id2 = o2.triangle.id;
            otri.orient = 0;
            otri.Sym(ref o2);
            int id3 = o2.triangle.id;
            streamWriter.WriteLine("{0} {1} {2} {3}", (object) num++, (object) id1, (object) id2, (object) id3);
          }
        }
      }
    }

    public static void WriteVoronoi(Mesh mesh, string filename)
    {
      Otri otri = new Otri();
      Otri o2 = new Otri();
      double xi = 0.0;
      double eta = 0.0;
      int num1 = 0;
      otri.orient = 0;
      using (StreamWriter streamWriter = new StreamWriter(filename))
      {
        streamWriter.WriteLine("{0} 2 {1} 0", (object) mesh.triangles.Count, (object) mesh.nextras);
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            Point circumcenter = Primitives.FindCircumcenter((Point) otri.Org(), (Point) otri.Dest(), (Point) otri.Apex(), ref xi, ref eta);
            streamWriter.Write("{0} {1} {2}", (object) num1, (object) circumcenter.X.ToString((IFormatProvider) FileWriter.nfi), (object) circumcenter.Y.ToString((IFormatProvider) FileWriter.nfi));
            for (int index = 0; index < mesh.nextras; ++index)
              streamWriter.Write(" 0");
            streamWriter.WriteLine();
            otri.triangle.id = num1++;
          }
        }
        streamWriter.WriteLine("{0} 0", (object) mesh.edges);
        int num2 = 0;
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            for (otri.orient = 0; otri.orient < 3; ++otri.orient)
            {
              otri.Sym(ref o2);
              if (otri.triangle.id < o2.triangle.id || o2.triangle == Mesh.dummytri)
              {
                int id1 = otri.triangle.id;
                if (o2.triangle == Mesh.dummytri)
                {
                  Vertex vertex1 = otri.Org();
                  Vertex vertex2 = otri.Dest();
                  streamWriter.WriteLine("{0} {1} -1 {2} {3}", (object) num2, (object) id1, (object) (vertex2[1] - vertex1[1]).ToString((IFormatProvider) FileWriter.nfi), (object) (vertex1[0] - vertex2[0]).ToString((IFormatProvider) FileWriter.nfi));
                }
                else
                {
                  int id2 = o2.triangle.id;
                  streamWriter.WriteLine("{0} {1} {2}", (object) num2, (object) id1, (object) id2);
                }
                ++num2;
              }
            }
          }
        }
      }
    }

    public static void WriteOffFile(Mesh mesh, string filename)
    {
      long num1 = (long) mesh.vertices.Count;
      if (mesh.behavior.Jettison)
        num1 = (long) (mesh.vertices.Count - mesh.undeads);
      int num2 = 0;
      using (StreamWriter streamWriter = new StreamWriter(filename))
      {
        streamWriter.WriteLine("OFF");
        streamWriter.WriteLine("{0}  {1}  {2}", (object) num1, (object) mesh.triangles.Count, (object) mesh.edges);
        using (Dictionary<int, Vertex>.ValueCollection.Enumerator enumerator = mesh.vertices.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Vertex current = enumerator.Current;
            if (!mesh.behavior.Jettison || current.type != VertexType.UndeadVertex)
            {
              streamWriter.WriteLine(" {0}  {1}  0.0", (object) current[0].ToString((IFormatProvider) FileWriter.nfi), (object) current[1].ToString((IFormatProvider) FileWriter.nfi));
              current.id = num2++;
            }
          }
        }
        Otri otri;
        otri.orient = 0;
        using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Triangle current = enumerator.Current;
            otri.triangle = current;
            Vertex vertex1 = otri.Org();
            Vertex vertex2 = otri.Dest();
            Vertex vertex3 = otri.Apex();
            streamWriter.WriteLine(" 3   {0}  {1}  {2}", (object) vertex1.id, (object) vertex2.id, (object) vertex3.id);
          }
        }
      }
    }
  }
}
