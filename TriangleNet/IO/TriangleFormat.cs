﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.IO.TriangleFormat
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using TriangleNet.Geometry;

namespace TriangleNet.IO
{
  public class TriangleFormat : IGeometryFormat, IMeshFormat
  {
    public Mesh Import(string filename)
    {
      string extension = Path.GetExtension(filename);
      if (extension == ".node" || extension == ".poly" || extension == ".ele")
      {
        InputGeometry geometry;
        List<ITriangle> triangles;
        FileReader.Read(filename, out geometry, out triangles);
        if (geometry != null && triangles != null)
        {
          Mesh mesh = new Mesh();
          mesh.Load(geometry, triangles);
          return mesh;
        }
      }
      throw new NotSupportedException("Could not load '" + filename + "' file.");
    }

    public void Write(Mesh mesh, string filename)
    {
      FileWriter.WritePoly(mesh, Path.ChangeExtension(filename, ".poly"));
      FileWriter.WriteElements(mesh, Path.ChangeExtension(filename, ".ele"));
    }

    public InputGeometry Read(string filename)
    {
      string extension = Path.GetExtension(filename);
      if (extension == ".node")
        return FileReader.ReadNodeFile(filename);
      if (extension == ".poly")
        return FileReader.ReadPolyFile(filename);
      throw new NotSupportedException("File format '" + extension + "' not supported.");
    }
  }
}
