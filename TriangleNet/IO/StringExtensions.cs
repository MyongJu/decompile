﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.IO.StringExtensions
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TriangleNet.IO
{
  public static class StringExtensions
  {
    public static bool IsNullOrWhiteSpace(this string value)
    {
      if (value != null)
      {
        for (int index = 0; index < value.Length; ++index)
        {
          if (!char.IsWhiteSpace(value[index]))
            return false;
        }
      }
      return true;
    }
  }
}
