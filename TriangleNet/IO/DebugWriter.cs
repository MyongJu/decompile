﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.IO.DebugWriter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TriangleNet.Data;
using TriangleNet.Geometry;

namespace TriangleNet.IO
{
  internal class DebugWriter
  {
    private static NumberFormatInfo nfi = CultureInfo.InvariantCulture.NumberFormat;
    private static readonly DebugWriter instance = new DebugWriter();
    private int iteration;
    private string session;
    private StreamWriter stream;
    private string tmpFile;
    private int[] vertices;
    private int triangles;

    private DebugWriter()
    {
    }

    public static DebugWriter Session
    {
      get
      {
        return DebugWriter.instance;
      }
    }

    public void Start(string session)
    {
      this.iteration = 0;
      this.session = session;
      if (this.stream != null)
        throw new Exception("A session is active. Finish before starting a new.");
      this.tmpFile = Path.GetTempFileName();
      this.stream = new StreamWriter(this.tmpFile);
    }

    public void Write(Mesh mesh, bool skip = false)
    {
      this.WriteMesh(mesh, skip);
      this.triangles = mesh.Triangles.Count;
    }

    public void Finish()
    {
      this.Finish(this.session + ".mshx");
    }

    private void Finish(string path)
    {
      if (this.stream == null)
        return;
      this.stream.Flush();
      this.stream.Dispose();
      this.stream = (StreamWriter) null;
      File.Delete(this.tmpFile);
    }

    private void WriteGeometry(InputGeometry geometry)
    {
      this.stream.WriteLine("#!G{0}", (object) this.iteration++);
    }

    private void WriteMesh(Mesh mesh, bool skip)
    {
      if (this.triangles == mesh.triangles.Count && skip)
        return;
      this.stream.WriteLine("#!M{0}", (object) this.iteration++);
      if (this.VerticesChanged(mesh))
      {
        this.HashVertices(mesh);
        this.stream.WriteLine("{0}", (object) mesh.vertices.Count);
        using (Dictionary<int, Vertex>.ValueCollection.Enumerator enumerator = mesh.vertices.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Vertex current = enumerator.Current;
            this.stream.WriteLine("{0} {1} {2} {3}", (object) current.hash, (object) current.x.ToString((IFormatProvider) DebugWriter.nfi), (object) current.y.ToString((IFormatProvider) DebugWriter.nfi), (object) current.mark);
          }
        }
      }
      else
        this.stream.WriteLine("0");
      this.stream.WriteLine("{0}", (object) mesh.subsegs.Count);
      Osub osub = new Osub();
      osub.orient = 0;
      using (Dictionary<int, Segment>.ValueCollection.Enumerator enumerator = mesh.subsegs.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Segment current = enumerator.Current;
          if (current.hash > 0)
          {
            osub.seg = current;
            Vertex vertex1 = osub.Org();
            Vertex vertex2 = osub.Dest();
            this.stream.WriteLine("{0} {1} {2} {3}", (object) osub.seg.hash, (object) vertex1.hash, (object) vertex2.hash, (object) osub.seg.boundary);
          }
        }
      }
      Otri otri = new Otri();
      Otri o2 = new Otri();
      otri.orient = 0;
      this.stream.WriteLine("{0}", (object) mesh.triangles.Count);
      using (Dictionary<int, Triangle>.ValueCollection.Enumerator enumerator = mesh.triangles.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Triangle current = enumerator.Current;
          otri.triangle = current;
          Vertex vertex1 = otri.Org();
          Vertex vertex2 = otri.Dest();
          Vertex vertex3 = otri.Apex();
          int num1 = !((Point) vertex1 == (Point) null) ? vertex1.hash : -1;
          int num2 = !((Point) vertex2 == (Point) null) ? vertex2.hash : -1;
          int num3 = !((Point) vertex3 == (Point) null) ? vertex3.hash : -1;
          this.stream.Write("{0} {1} {2} {3}", (object) otri.triangle.hash, (object) num1, (object) num2, (object) num3);
          otri.orient = 1;
          otri.Sym(ref o2);
          int hash1 = o2.triangle.hash;
          otri.orient = 2;
          otri.Sym(ref o2);
          int hash2 = o2.triangle.hash;
          otri.orient = 0;
          otri.Sym(ref o2);
          int hash3 = o2.triangle.hash;
          this.stream.WriteLine(" {0} {1} {2}", (object) hash1, (object) hash2, (object) hash3);
        }
      }
    }

    private bool VerticesChanged(Mesh mesh)
    {
      if (this.vertices == null || mesh.Vertices.Count != this.vertices.Length)
        return true;
      int num = 0;
      foreach (Point vertex in (IEnumerable<Vertex>) mesh.Vertices)
      {
        if (vertex.id != this.vertices[num++])
          return true;
      }
      return false;
    }

    private void HashVertices(Mesh mesh)
    {
      if (this.vertices == null || mesh.Vertices.Count != this.vertices.Length)
        this.vertices = new int[mesh.Vertices.Count];
      int num = 0;
      foreach (Vertex vertex in (IEnumerable<Vertex>) mesh.Vertices)
        this.vertices[num++] = vertex.id;
    }
  }
}
