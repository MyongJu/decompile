﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Tools.VoronoiRegion
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TriangleNet.Data;
using TriangleNet.Geometry;

namespace TriangleNet.Tools
{
  public class VoronoiRegion
  {
    private int id;
    private Point generator;
    private List<Point> vertices;
    private bool bounded;

    public VoronoiRegion(Vertex generator)
    {
      this.id = generator.id;
      this.generator = (Point) generator;
      this.vertices = new List<Point>();
      this.bounded = true;
    }

    public int ID
    {
      get
      {
        return this.id;
      }
    }

    public Point Generator
    {
      get
      {
        return this.generator;
      }
    }

    public List<Point> Vertices
    {
      get
      {
        return this.vertices;
      }
    }

    public bool Bounded
    {
      get
      {
        return this.bounded;
      }
      set
      {
        this.bounded = value;
      }
    }

    public void Add(Point point)
    {
      this.vertices.Add(point);
    }

    public void Add(List<Point> points)
    {
      this.vertices.AddRange((IEnumerable<Point>) points);
    }

    public override string ToString()
    {
      return string.Format("R-ID {0}", (object) this.id);
    }
  }
}
