﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Tools.IVoronoi
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TriangleNet.Geometry;

namespace TriangleNet.Tools
{
  public interface IVoronoi
  {
    Point[] Points { get; }

    List<VoronoiRegion> Regions { get; }
  }
}
