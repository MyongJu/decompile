﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Tools.RegionIterator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TriangleNet.Data;

namespace TriangleNet.Tools
{
  public class RegionIterator
  {
    private List<Triangle> viri;

    public RegionIterator(Mesh mesh)
    {
      this.viri = new List<Triangle>();
    }

    private void ProcessRegion(Action<Triangle> func)
    {
      Otri otri = new Otri();
      Otri o2 = new Otri();
      Osub os = new Osub();
      for (int index = 0; index < this.viri.Count; ++index)
      {
        otri.triangle = this.viri[index];
        otri.Uninfect();
        func(otri.triangle);
        for (otri.orient = 0; otri.orient < 3; ++otri.orient)
        {
          otri.Sym(ref o2);
          otri.SegPivot(ref os);
          if (o2.triangle != Mesh.dummytri && !o2.IsInfected() && os.seg == Mesh.dummysub)
          {
            o2.Infect();
            this.viri.Add(o2.triangle);
          }
        }
        otri.Infect();
      }
      using (List<Triangle>.Enumerator enumerator = this.viri.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.infected = false;
      }
      this.viri.Clear();
    }

    public void Process(Triangle triangle)
    {
      this.Process(triangle, (Action<Triangle>) (tri => tri.region = triangle.region));
    }

    public void Process(Triangle triangle, Action<Triangle> func)
    {
      if (triangle != Mesh.dummytri && !Otri.IsDead(triangle))
      {
        triangle.infected = true;
        this.viri.Add(triangle);
        this.ProcessRegion(func);
      }
      this.viri.Clear();
    }
  }
}
