﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Geometry.InputGeometry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TriangleNet.Data;

namespace TriangleNet.Geometry
{
  public class InputGeometry
  {
    private int pointAttributes = -1;
    internal List<Vertex> points;
    internal List<Edge> segments;
    internal List<Point> holes;
    internal List<RegionPointer> regions;
    private BoundingBox bounds;

    public InputGeometry()
      : this(3)
    {
    }

    public InputGeometry(int capacity)
    {
      this.points = new List<Vertex>(capacity);
      this.segments = new List<Edge>();
      this.holes = new List<Point>();
      this.regions = new List<RegionPointer>();
      this.bounds = new BoundingBox();
      this.pointAttributes = -1;
    }

    public BoundingBox Bounds
    {
      get
      {
        return this.bounds;
      }
    }

    public bool HasSegments
    {
      get
      {
        return this.segments.Count > 0;
      }
    }

    public int Count
    {
      get
      {
        return this.points.Count;
      }
    }

    public List<Vertex> Points
    {
      get
      {
        return this.points;
      }
    }

    public ICollection<Edge> Segments
    {
      get
      {
        return (ICollection<Edge>) this.segments;
      }
    }

    public ICollection<Point> Holes
    {
      get
      {
        return (ICollection<Point>) this.holes;
      }
    }

    public ICollection<RegionPointer> Regions
    {
      get
      {
        return (ICollection<RegionPointer>) this.regions;
      }
    }

    public void Clear()
    {
      this.points.Clear();
      this.segments.Clear();
      this.holes.Clear();
      this.regions.Clear();
      this.pointAttributes = -1;
    }

    public void AddPoint(double x, double y)
    {
      this.AddPoint(x, y, 0);
    }

    public void AddPoint(double x, double y, int boundary)
    {
      this.points.Add(new Vertex(x, y, boundary));
      this.bounds.Update(x, y);
    }

    public void AddPoint(double x, double y, int boundary, double attribute)
    {
      this.AddPoint(x, y, 0, new double[1]{ attribute });
    }

    public void AddPoint(double x, double y, int boundary, double[] attribs)
    {
      if (this.pointAttributes < 0)
      {
        this.pointAttributes = attribs != null ? attribs.Length : 0;
      }
      else
      {
        if (attribs == null && this.pointAttributes > 0)
          throw new ArgumentException("Inconsitent use of point attributes.");
        if (attribs != null && this.pointAttributes != attribs.Length)
          throw new ArgumentException("Inconsitent use of point attributes.");
      }
      List<Vertex> points = this.points;
      Vertex vertex1 = new Vertex(x, y, boundary);
      vertex1.attributes = attribs;
      Vertex vertex2 = vertex1;
      points.Add(vertex2);
      this.bounds.Update(x, y);
    }

    public void AddHole(double x, double y)
    {
      this.holes.Add(new Point(x, y));
    }

    public void AddRegion(double x, double y, int id)
    {
      this.regions.Add(new RegionPointer(x, y, id));
    }

    public void AddSegment(int p0, int p1)
    {
      this.AddSegment(p0, p1, 0);
    }

    public void AddSegment(int p0, int p1, int boundary)
    {
      if (p0 == p1 || p0 < 0 || p1 < 0)
        throw new NotSupportedException("Invalid endpoints.");
      this.segments.Add(new Edge(p0, p1, boundary));
    }
  }
}
