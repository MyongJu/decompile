﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Geometry.ITriangle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using TriangleNet.Data;

namespace TriangleNet.Geometry
{
  public interface ITriangle
  {
    int ID { get; }

    int P0 { get; }

    int P1 { get; }

    int P2 { get; }

    Vertex GetVertex(int index);

    bool SupportsNeighbors { get; }

    int N0 { get; }

    int N1 { get; }

    int N2 { get; }

    ITriangle GetNeighbor(int index);

    ISegment GetSegment(int index);

    double Area { get; set; }

    int Region { get; }
  }
}
