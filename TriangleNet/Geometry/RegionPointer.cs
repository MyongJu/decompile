﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Geometry.RegionPointer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TriangleNet.Geometry
{
  public class RegionPointer
  {
    internal Point point;
    internal int id;

    public RegionPointer(double x, double y, int id)
    {
      this.point = new Point(x, y);
      this.id = id;
    }
  }
}
