﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Geometry.Edge
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace TriangleNet.Geometry
{
  public class Edge
  {
    public Edge(int p0, int p1)
      : this(p0, p1, 0)
    {
    }

    public Edge(int p0, int p1, int boundary)
    {
      this.P0 = p0;
      this.P1 = p1;
      this.Boundary = boundary;
    }

    public int P0 { get; private set; }

    public int P1 { get; private set; }

    public int Boundary { get; private set; }
  }
}
