﻿// Decompiled with JetBrains decompiler
// Type: TriangleNet.Geometry.ISegment
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using TriangleNet.Data;

namespace TriangleNet.Geometry
{
  public interface ISegment
  {
    int P0 { get; }

    int P1 { get; }

    int Boundary { get; }

    Vertex GetVertex(int index);

    ITriangle GetTriangle(int index);
  }
}
