﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEnhanceDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentEnhanceDlg : UI.Dialog
{
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Hashtable requireHT = new Hashtable();
  public GameObject template;
  public UIGrid grid;
  public UIScrollView scrollView;
  public EquipmentEnhanceInfo info;
  public GameObject empty;
  public UIButton enhanceBtn;
  public GameObject actionArea;
  public UILabel scrollNotEnoughLabel;
  public UILabel maxEnhanceLabel;
  public EquipmentSuitEnhanceItem equipmentSuitEnhanceItem;
  private long currentSelectInfoItemID;
  private EquipmentRender current;
  private List<EquipmentRender> contentList;
  private List<long> infos;
  private int normalGold;
  private BagType bagType;

  private void Init(long currentSelectInfoItemID = -1)
  {
    this.current = (EquipmentRender) null;
    this.ClearGrid();
    if (currentSelectInfoItemID != -1L)
      this.currentSelectInfoItemID = currentSelectInfoItemID;
    this.infos = new List<long>();
    List<InventoryItemData> myItemList = DBManager.inst.GetInventory(this.bagType).GetMyItemList();
    for (int index = 0; index < myItemList.Count; ++index)
    {
      if (!myItemList[index].InSale)
        this.infos.Add(myItemList[index].itemId);
    }
    this.Sort(this.infos);
    this.contentList = new List<EquipmentRender>();
    for (int index = 0; index < this.infos.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template);
      gameObject.SetActive(true);
      EquipmentRender component = gameObject.GetComponent<EquipmentRender>();
      component.OnSelectedHandler = new System.Action<EquipmentRender>(this.OnSelectedHandler);
      this.contentList.Add(component);
      InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.infos[index]);
      component.FeedData((IComponentData) new EquipmentComponentData(myItemByItemId.internalId, myItemByItemId.enhanced, this.bagType, myItemByItemId.itemId));
      if (component.componentData.itemId == currentSelectInfoItemID)
      {
        this.current = component;
        this.current.Select = true;
      }
    }
    if (this.contentList.Count > 0)
    {
      if ((UnityEngine.Object) this.current == (UnityEngine.Object) null)
      {
        this.current = this.contentList[0];
        this.current.Select = true;
        currentSelectInfoItemID = this.current.componentData.itemId;
      }
      this.info.Init(this.bagType, this.current.componentData.itemId);
      this.info.gameObject.SetActive(true);
      this.actionArea.SetActive(true);
      this.ConfigActionArea();
    }
    else
    {
      this.enhanceBtn.enabled = false;
      this.info.gameObject.SetActive(false);
      this.actionArea.SetActive(false);
    }
    this.grid.repositionNow = true;
    if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
      Utils.ExecuteInSecs(0.05f, (System.Action) (() =>
      {
        if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null))
          return;
        this.CenterOn(this.current.transform);
      }));
    this.empty.SetActive(this.contentList.Count == 0);
    this.UpdateEquipmentSuitEnhanceItem();
    DBManager.inst.GetInventory(this.bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this.bagType).onDataRemoved += new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this.bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this.bagType).onDataChanged += new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemChanged);
  }

  protected void UpdateEquipmentSuitEnhanceItem()
  {
    if (!(bool) ((UnityEngine.Object) this.equipmentSuitEnhanceItem))
      return;
    List<InventoryItemData> allInventoryItemData = new List<InventoryItemData>();
    if (this.bagType == BagType.Hero)
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
      if (heroData != null)
      {
        using (List<int>.Enumerator enumerator = heroData.equipments.GetAllEquipments().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID((long) enumerator.Current);
            if (myItemByItemId != null)
              allInventoryItemData.Add(myItemByItemId);
          }
        }
      }
    }
    else
    {
      DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
      if (dragonKnightData != null)
      {
        using (List<int>.Enumerator enumerator = dragonKnightData.equipments.GetAllEquipments().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID((long) enumerator.Current);
            if (myItemByItemId != null)
              allInventoryItemData.Add(myItemByItemId);
          }
        }
      }
    }
    this.equipmentSuitEnhanceItem.UpdateUI(allInventoryItemData, this.bagType);
  }

  public void CenterOn(Transform target)
  {
    if (!((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null))
      return;
    Vector3[] worldCorners = this.scrollView.panel.worldCorners;
    Vector3 panelBottom = (worldCorners[1] + worldCorners[2]) * 0.5f;
    this.CenterOn(target, panelBottom);
  }

  private void CenterOn(Transform target, Vector3 panelBottom)
  {
    if (!((UnityEngine.Object) target != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null) || !((UnityEngine.Object) this.scrollView.panel != (UnityEngine.Object) null))
      return;
    Transform cachedTransform = this.scrollView.panel.cachedTransform;
    Vector3 vector3 = cachedTransform.InverseTransformPoint(target.position) - cachedTransform.InverseTransformPoint(panelBottom);
    if (!this.scrollView.canMoveHorizontally)
      vector3.x = 0.0f;
    if (!this.scrollView.canMoveVertically)
      vector3.y = 0.0f;
    vector3.z = 0.0f;
    vector3.y += this.grid.cellHeight / 2f;
    cachedTransform.localPosition -= vector3;
    Vector4 clipOffset = (Vector4) this.scrollView.panel.clipOffset;
    clipOffset.x += vector3.x;
    clipOffset.y += vector3.y;
    this.scrollView.panel.clipOffset = (Vector2) clipOffset;
    this.scrollView.RestrictWithinBounds(true);
  }

  private void OnInventoryChanged(long uid, long itemId)
  {
    this.Init(DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.currentSelectInfoItemID) == null ? -1L : this.currentSelectInfoItemID);
  }

  private void OnItemChanged(int itemId)
  {
    this.Init(DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.currentSelectInfoItemID) == null ? -1L : this.currentSelectInfoItemID);
  }

  public void Dispose()
  {
    DBManager.inst.GetInventory(this.bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this.bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemChanged);
    this.info.Dispose();
  }

  private void OnSelectedHandler(EquipmentRender selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.componentData.itemId == selected.componentData.itemId)
      return;
    this.currentSelectInfoItemID = selected.componentData.itemId;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.info.Init(this.bagType, this.current.componentData.itemId);
    this.ConfigActionArea();
  }

  private void ConfigActionArea()
  {
    this.requireHT.Clear();
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
    if (!EquipmentManager.Instance.IsEnhancedToMax(this.current.componentData.bagType, myItemByItemId.itemId))
    {
      ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.current.componentData.equipmentInfo.internalId, myItemByItemId.enhanced + 1);
      Hashtable hashtable1 = new Hashtable();
      int num = Mathf.CeilToInt(ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) idAndEnhanceLevel.steelValue, "calc_crafting_steel_cost"));
      hashtable1[(object) ItemBag.ItemType.steel] = (object) num;
      Hashtable hashtable2 = new Hashtable();
      ShopStaticInfo byItemInternalId1 = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(idAndEnhanceLevel.scrollID);
      if (byItemInternalId1 != null)
        hashtable2[(object) byItemInternalId1.internalId.ToString()] = (object) idAndEnhanceLevel.scrollValue;
      if (idAndEnhanceLevel.Materials != null)
      {
        List<Materials.MaterialElement> materials = idAndEnhanceLevel.Materials.GetMaterials();
        for (int index = 0; index < materials.Count; ++index)
        {
          ShopStaticInfo byItemInternalId2 = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(materials[index].internalID);
          if (byItemInternalId2 != null)
            hashtable2[(object) byItemInternalId2.internalId.ToString()] = (object) materials[index].amount;
        }
      }
      this.requireHT[(object) "resources"] = (object) hashtable1;
      this.requireHT[(object) "items"] = (object) hashtable2;
      this.requireHT.Remove((object) "time");
      this.normalGold = ItemBag.CalculateCost(this.requireHT);
      this.m_lackRes = ItemBag.CalculateLackResources(this.requireHT);
      this.m_lackitem = ItemBag.CalculateLackItems(this.requireHT);
      ConfigEquipmentScrollInfo data = ConfigManager.inst.GetEquipmentScroll(this.bagType).GetData(idAndEnhanceLevel.scrollID);
      bool flag = data == null || idAndEnhanceLevel.scrollValue <= (long) ItemBag.Instance.GetItemCount(data.itemID);
      this.enhanceBtn.gameObject.SetActive(flag);
      this.enhanceBtn.enabled = flag;
      this.enhanceBtn.onClick.Clear();
      this.enhanceBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnEnhanceBtnClick)));
      this.scrollNotEnoughLabel.gameObject.SetActive(!flag);
      this.maxEnhanceLabel.gameObject.SetActive(false);
    }
    else
    {
      this.maxEnhanceLabel.gameObject.SetActive(true);
      this.enhanceBtn.gameObject.SetActive(false);
      this.scrollNotEnoughLabel.gameObject.SetActive(false);
    }
  }

  private void OnEnhanceBtnClick()
  {
    if (this.m_lackRes.Count > 0 || this.m_lackitem.Count > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        requireRes = this.requireHT,
        action = Utils.XLAT("id_uppercase_enhance"),
        gold = this.normalGold,
        buyResourCallBack = new System.Action<int>(this.SendEnhanceRequest),
        title = ScriptLocalization.Get("id_uppercase_not_enough_resources", true),
        content = ScriptLocalization.Get("forge_enhance_not_enough_resources_description", true)
      });
    else
      this.SendEnhanceRequest(0);
  }

  public void SendEnhanceRequest(int result = 0)
  {
    if (result > GameEngine.Instance.PlayerData.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      InventoryItemData inventory = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
      EquipmentManager.Instance.Enhance(this.bagType, inventory.itemId, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        Hashtable hashtable = arg2 as Hashtable;
        UIManager.inst.OpenPopup("Equipment/EquipmentForgeSuccessfullyPopup", (Popup.PopupParameter) new EquipmentForgeSuccessfullyPopup.Parameter()
        {
          bagType = this.bagType,
          equipmenItemID = inventory.itemId
        });
        this.Init(-1L);
        if (!inventory.isEquipped)
          return;
        ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance = (ConfigEquipmentSuitEnhanceInfo) null;
        ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
        Utils.GetEquipmentBenefitChangeInfoAfterEnhancement(this.bagType, inventory, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
        if (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null)
          return;
        UIManager.inst.ShowEquipmentSuitBenefitChangeTip((List<Benefits.BenefitValuePair>) null, (List<Benefits.BenefitValuePair>) null, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
      }));
    }
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  private void Sort(List<long> list)
  {
    list.Sort((Comparison<long>) ((x, y) =>
    {
      ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(x).internalId);
      ConfigEquipmentMainInfo data2 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(y).internalId);
      if (data1.type != data2.type)
        return data1.type.CompareTo(data2.type);
      if (data1.quality != data2.quality)
        return data2.quality.CompareTo(data1.quality);
      if (data1.heroLevelMin == data2.heroLevelMin)
        return data1.tendency.CompareTo(data2.tendency);
      return data2.heroLevelMin.CompareTo(data1.heroLevelMin);
    }));
  }

  public void OnGotoClick()
  {
    UIManager.inst.OpenDlg(this.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentTreasuryDialog" : "Equipment/EquipmentTreasuryDlg", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
    {
      bagType = this.bagType
    }, true, true, true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (orgParam != null)
    {
      this.currentSelectInfoItemID = (orgParam as EquipmentEnhanceDlg.Parameter).selectedEquipmentItemID;
      this.bagType = (orgParam as EquipmentEnhanceDlg.Parameter).bagType;
    }
    this.Init(-1L);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnHide(orgParam);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public long selectedEquipmentItemID = -1;
    public BagType bagType;
  }
}
