﻿// Decompiled with JetBrains decompiler
// Type: RouletteMultiWinItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class RouletteMultiWinItemRender : MonoBehaviour
{
  private const string MULTI_PREFIX = "x";
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private UILabel _labelRewardMultiplier;
  [SerializeField]
  private GameObject rareFlag;
  private MultiWinItemData _rewardData;
  private bool _winMultiAnimPlayed;
  private bool _winMultiAnimForceStopped;

  public MultiWinItemData Data
  {
    get
    {
      return this._rewardData;
    }
  }

  public bool WinMultiAnimPlayed
  {
    get
    {
      return this._winMultiAnimPlayed;
    }
  }

  public void SetData(MultiWinItemData rewardData)
  {
    this._rewardData = rewardData;
    this.UpdateUI();
  }

  public void StartMultiWinAnim()
  {
    this._winMultiAnimPlayed = true;
    this.StartCoroutine("PlayMultiWinAnim");
    bool flag = false;
    if (RoulettePayload.Instance.MiddleRewards.Contains(this._rewardData.itemId))
    {
      flag = true;
      this.AttachEffect("Prefab/VFX/fx_choujiang_02");
    }
    if (flag || !RoulettePayload.Instance.HighRewards.Contains(this._rewardData.itemId))
      return;
    this.AttachEffect("Prefab/VFX/fx_choujiang_01");
  }

  public void StopMultiWinAnim()
  {
    this._winMultiAnimForceStopped = true;
    this.StopCoroutine("PlayMultiWinAnim");
    if (this._rewardData == null)
      return;
    this._labelRewardMultiplier.text = "x" + this._rewardData.totalCount.ToString();
  }

  [DebuggerHidden]
  private IEnumerator PlayMultiWinAnim()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RouletteMultiWinItemRender.\u003CPlayMultiWinAnim\u003Ec__Iterator95()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void AttachEffect(string path)
  {
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.HandyLoad(path, (System.Type) null) as GameObject);
    go.transform.parent = this.transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    Utils.SetLayer(go, this.gameObject.layer);
  }

  private void UpdateUI()
  {
    if (this._rewardData == null)
      return;
    if (this._rewardData.itemId > 0)
      this._itemIconRenderer.SetData(this._rewardData.itemId, string.Empty, true);
    if (this._rewardData.grade == 3)
    {
      this.rareFlag.SetActive(true);
      this.rareFlag.GetComponentInChildren<UILabel>().text = ScriptLocalization.Get("id_uppercase_rare", true);
    }
    else
      this.rareFlag.SetActive(false);
    this._labelRewardMultiplier.text = "x" + this._rewardData.totalCount.ToString();
  }
}
