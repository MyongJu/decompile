﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_GlobalControl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_GlobalControl : MonoBehaviour
{
  public List<Puppet2D_SplineControl> _SplineControls = new List<Puppet2D_SplineControl>();
  public List<Puppet2D_IKHandle> _Ikhandles = new List<Puppet2D_IKHandle>();
  public List<Puppet2D_ParentControl> _ParentControls = new List<Puppet2D_ParentControl>();
  public List<Puppet2D_FFDLineDisplay> _ffdControls = new List<Puppet2D_FFDLineDisplay>();
  [HideInInspector]
  public List<SpriteRenderer> _Controls = new List<SpriteRenderer>();
  [HideInInspector]
  public List<SpriteRenderer> _Bones = new List<SpriteRenderer>();
  [HideInInspector]
  public List<SpriteRenderer> _FFDControls = new List<SpriteRenderer>();
  public bool ControlsVisiblity = true;
  public bool BonesVisiblity = true;
  public bool FFD_Visiblity = true;
  public bool AutoRefresh = true;
  public bool ControlsEnabled = true;
  public bool lateUpdate = true;
  public float startRotationY;
  public bool CombineMeshes;
  public bool flip;
  private bool internalFlip;

  private void OnEnable()
  {
    if (!this.AutoRefresh)
      return;
    this._Ikhandles.Clear();
    this._ParentControls.Clear();
    this._Controls.Clear();
    this._Bones.Clear();
    this._FFDControls.Clear();
    this._ffdControls.Clear();
    this.TraverseHierarchy(this.transform);
  }

  public void Refresh()
  {
    this._Ikhandles.Clear();
    this._ParentControls.Clear();
    this._Controls.Clear();
    this._Bones.Clear();
    this._FFDControls.Clear();
    this._ffdControls.Clear();
    this.TraverseHierarchy(this.transform);
  }

  private void Awake()
  {
    this.internalFlip = this.flip;
    if (!Application.isPlaying || !this.CombineMeshes)
      return;
    this.CombineAllMeshes();
  }

  public void Init()
  {
    this._Ikhandles.Clear();
    this._ParentControls.Clear();
    this._Controls.Clear();
    this._Bones.Clear();
    this._FFDControls.Clear();
    this._ffdControls.Clear();
    this.TraverseHierarchy(this.transform);
  }

  private void OnValidate()
  {
    if (this.AutoRefresh)
    {
      this._Ikhandles.Clear();
      this._ParentControls.Clear();
      this._Controls.Clear();
      this._Bones.Clear();
      this._FFDControls.Clear();
      this._ffdControls.Clear();
      this.TraverseHierarchy(this.transform);
    }
    using (List<SpriteRenderer>.Enumerator enumerator = this._Controls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpriteRenderer current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current) && current.enabled != this.ControlsVisiblity)
          current.enabled = this.ControlsVisiblity;
      }
    }
    using (List<SpriteRenderer>.Enumerator enumerator = this._Bones.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpriteRenderer current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current) && current.enabled != this.BonesVisiblity)
          current.enabled = this.BonesVisiblity;
      }
    }
    using (List<SpriteRenderer>.Enumerator enumerator = this._FFDControls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpriteRenderer current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current) && (bool) ((UnityEngine.Object) current.transform.parent) && ((bool) ((UnityEngine.Object) current.transform.parent.gameObject) && current.transform.parent.gameObject.activeSelf != this.FFD_Visiblity))
          current.transform.parent.gameObject.SetActive(this.FFD_Visiblity);
      }
    }
  }

  private void Update()
  {
    if (this.lateUpdate)
      return;
    if (this.ControlsEnabled)
      this.Run();
    if (this.internalFlip == this.flip)
      return;
    if (this.flip)
    {
      this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, -this.transform.localScale.z);
      this.transform.localEulerAngles = new Vector3(this.transform.rotation.eulerAngles.x, this.startRotationY + 180f, this.transform.rotation.eulerAngles.z);
    }
    else
    {
      this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), Mathf.Abs(this.transform.localScale.y), Mathf.Abs(this.transform.localScale.z));
      this.transform.localEulerAngles = new Vector3(this.transform.rotation.eulerAngles.x, this.startRotationY, this.transform.rotation.eulerAngles.z);
    }
    this.internalFlip = this.flip;
    this.Run();
  }

  private void LateUpdate()
  {
    if (!this.lateUpdate)
      return;
    if (this.ControlsEnabled)
      this.Run();
    if (this.internalFlip == this.flip)
      return;
    if (this.flip)
    {
      this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, -this.transform.localScale.z);
      this.transform.localEulerAngles = new Vector3(this.transform.rotation.eulerAngles.x, this.startRotationY + 180f, this.transform.rotation.eulerAngles.z);
    }
    else
    {
      this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), Mathf.Abs(this.transform.localScale.y), Mathf.Abs(this.transform.localScale.z));
      this.transform.localEulerAngles = new Vector3(this.transform.rotation.eulerAngles.x, this.startRotationY, this.transform.rotation.eulerAngles.z);
    }
    this.internalFlip = this.flip;
    this.Run();
  }

  public void Run()
  {
    using (List<Puppet2D_SplineControl>.Enumerator enumerator = this._SplineControls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Puppet2D_SplineControl current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          current.Run();
      }
    }
    using (List<Puppet2D_ParentControl>.Enumerator enumerator = this._ParentControls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Puppet2D_ParentControl current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          current.ParentControlRun();
      }
    }
    this.FaceCamera();
    using (List<Puppet2D_IKHandle>.Enumerator enumerator = this._Ikhandles.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Puppet2D_IKHandle current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          current.CalculateIK();
      }
    }
    using (List<Puppet2D_FFDLineDisplay>.Enumerator enumerator = this._ffdControls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Puppet2D_FFDLineDisplay current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
          current.Run();
      }
    }
  }

  public void TraverseHierarchy(Transform root)
  {
    IEnumerator enumerator = root.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        Transform current = (Transform) enumerator.Current;
        GameObject gameObject = current.gameObject;
        SpriteRenderer component1 = gameObject.transform.GetComponent<SpriteRenderer>();
        if ((bool) ((UnityEngine.Object) component1) && (bool) ((UnityEngine.Object) component1.sprite))
        {
          if (component1.sprite.name.Contains("Control"))
            this._Controls.Add(component1);
          else if (component1.sprite.name.Contains("ffd"))
            this._FFDControls.Add(component1);
          else if (component1.sprite.name.Contains("Bone"))
            this._Bones.Add(component1);
        }
        Puppet2D_ParentControl component2 = gameObject.transform.GetComponent<Puppet2D_ParentControl>();
        if ((bool) ((UnityEngine.Object) component2))
          this._ParentControls.Add(component2);
        Puppet2D_IKHandle component3 = gameObject.transform.GetComponent<Puppet2D_IKHandle>();
        if ((bool) ((UnityEngine.Object) component3))
          this._Ikhandles.Add(component3);
        Puppet2D_FFDLineDisplay component4 = gameObject.transform.GetComponent<Puppet2D_FFDLineDisplay>();
        if ((bool) ((UnityEngine.Object) component4))
          this._ffdControls.Add(component4);
        this.TraverseHierarchy(current);
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }

  private void CombineAllMeshes()
  {
    Vector3 localScale = this.transform.localScale;
    Quaternion rotation1 = this.transform.rotation;
    Vector3 position1 = this.transform.position;
    this.transform.localScale = Vector3.one;
    this.transform.rotation = Quaternion.identity;
    this.transform.position = Vector3.zero;
    SkinnedMeshRenderer[] componentsInChildren = this.GetComponentsInChildren<SkinnedMeshRenderer>();
    List<Transform> transformList = new List<Transform>();
    List<BoneWeight> boneWeightList = new List<BoneWeight>();
    List<CombineInstance> combineInstanceList = new List<CombineInstance>();
    List<Texture2D> texture2DList = new List<Texture2D>();
    Material material1 = (Material) null;
    int length = 0;
    Dictionary<SkinnedMeshRenderer, float> source = new Dictionary<SkinnedMeshRenderer, float>(componentsInChildren.Length);
    bool flag1 = false;
    foreach (SkinnedMeshRenderer key in componentsInChildren)
    {
      source.Add(key, key.transform.position.z);
      flag1 = key.updateWhenOffscreen;
    }
    IOrderedEnumerable<KeyValuePair<SkinnedMeshRenderer, float>> orderedEnumerable = source.OrderBy<KeyValuePair<SkinnedMeshRenderer, float>, int>((Func<KeyValuePair<SkinnedMeshRenderer, float>, int>) (pair => pair.Key.sortingOrder)).OrderByDescending<KeyValuePair<SkinnedMeshRenderer, float>, float>((Func<KeyValuePair<SkinnedMeshRenderer, float>, float>) (pair => pair.Value));
    foreach (KeyValuePair<SkinnedMeshRenderer, float> keyValuePair in (IEnumerable<KeyValuePair<SkinnedMeshRenderer, float>>) orderedEnumerable)
      length += keyValuePair.Key.sharedMesh.subMeshCount;
    int[] numArray = new int[length];
    int num = 0;
    int index1 = 0;
    foreach (KeyValuePair<SkinnedMeshRenderer, float> keyValuePair in (IEnumerable<KeyValuePair<SkinnedMeshRenderer, float>>) orderedEnumerable)
    {
      SkinnedMeshRenderer key = keyValuePair.Key;
      if ((UnityEngine.Object) material1 == (UnityEngine.Object) null)
        material1 = key.sharedMaterial;
      else if ((bool) ((UnityEngine.Object) material1.mainTexture) && (bool) ((UnityEngine.Object) key.sharedMaterial.mainTexture) && (UnityEngine.Object) material1.mainTexture != (UnityEngine.Object) key.sharedMaterial.mainTexture)
        continue;
      bool flag2 = false;
      foreach (Component bone in key.bones)
      {
        Puppet2D_FFDLineDisplay component = bone.GetComponent<Puppet2D_FFDLineDisplay>();
        if ((bool) ((UnityEngine.Object) component) && (UnityEngine.Object) component.outputSkinnedMesh != (UnityEngine.Object) key)
          flag2 = true;
      }
      if (!flag2)
      {
        foreach (BoneWeight boneWeight in key.sharedMesh.boneWeights)
        {
          boneWeight.boneIndex0 += num;
          boneWeight.boneIndex1 += num;
          boneWeight.boneIndex2 += num;
          boneWeight.boneIndex3 += num;
          boneWeightList.Add(boneWeight);
        }
        num += key.bones.Length;
        foreach (Transform bone in key.bones)
          transformList.Add(bone);
        if ((UnityEngine.Object) key.material.mainTexture != (UnityEngine.Object) null)
          texture2DList.Add(key.GetComponent<Renderer>().material.mainTexture as Texture2D);
        CombineInstance combineInstance = new CombineInstance();
        combineInstance.mesh = key.sharedMesh;
        numArray[index1] = combineInstance.mesh.vertexCount;
        combineInstance.transform = key.transform.localToWorldMatrix;
        combineInstanceList.Add(combineInstance);
        UnityEngine.Object.Destroy((UnityEngine.Object) key.gameObject);
        ++index1;
      }
    }
    List<Matrix4x4> matrix4x4List = new List<Matrix4x4>();
    for (int index2 = 0; index2 < transformList.Count; ++index2)
    {
      if ((bool) ((UnityEngine.Object) transformList[index2].GetComponent<Puppet2D_FFDLineDisplay>()))
      {
        Vector3 position2 = transformList[index2].transform.parent.parent.position;
        Quaternion rotation2 = transformList[index2].transform.parent.parent.rotation;
        transformList[index2].transform.parent.parent.position = Vector3.zero;
        transformList[index2].transform.parent.parent.rotation = Quaternion.identity;
        matrix4x4List.Add(transformList[index2].worldToLocalMatrix * this.transform.worldToLocalMatrix);
        transformList[index2].transform.parent.parent.position = position2;
        transformList[index2].transform.parent.parent.rotation = rotation2;
      }
      else
        matrix4x4List.Add(transformList[index2].worldToLocalMatrix * this.transform.worldToLocalMatrix);
    }
    SkinnedMeshRenderer skinnedMeshRenderer = this.gameObject.AddComponent<SkinnedMeshRenderer>();
    skinnedMeshRenderer.updateWhenOffscreen = flag1;
    skinnedMeshRenderer.sharedMesh = new Mesh();
    skinnedMeshRenderer.sharedMesh.CombineMeshes(combineInstanceList.ToArray(), true, true);
    Material material2 = !((UnityEngine.Object) material1 != (UnityEngine.Object) null) ? new Material(Shader.Find("Unlit/Transparent")) : material1;
    material2.mainTexture = (Texture) texture2DList[0];
    skinnedMeshRenderer.sharedMesh.uv = skinnedMeshRenderer.sharedMesh.uv;
    skinnedMeshRenderer.sharedMaterial = material2;
    skinnedMeshRenderer.bones = transformList.ToArray();
    skinnedMeshRenderer.sharedMesh.boneWeights = boneWeightList.ToArray();
    skinnedMeshRenderer.sharedMesh.bindposes = matrix4x4List.ToArray();
    skinnedMeshRenderer.sharedMesh.RecalculateBounds();
    this.transform.localScale = localScale;
    this.transform.rotation = rotation1;
    this.transform.position = position1;
  }

  private void FaceCamera()
  {
    using (List<Puppet2D_IKHandle>.Enumerator enumerator = this._Ikhandles.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.AimDirection = this.transform.forward.normalized;
    }
  }
}
