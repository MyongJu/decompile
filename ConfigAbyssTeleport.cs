﻿// Decompiled with JetBrains decompiler
// Type: ConfigAbyssTeleport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAbyssTeleport
{
  public Dictionary<string, AbyssTeleportInfo> datas;
  private Dictionary<int, AbyssTeleportInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AbyssTeleportInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AbyssTeleportInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AbyssTeleportInfo) null;
  }

  public AbyssTeleportInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AbyssTeleportInfo) null;
  }

  public int GetCost(int times)
  {
    using (Dictionary<string, AbyssTeleportInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AbyssTeleportInfo abyssTeleportInfo = enumerator.Current.Value;
        if (times >= abyssTeleportInfo.timesMin && times <= abyssTeleportInfo.timesMax)
          return abyssTeleportInfo.goldCost;
      }
    }
    return 0;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AbyssTeleportInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AbyssTeleportInfo>) null;
  }
}
