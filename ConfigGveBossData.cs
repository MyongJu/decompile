﻿// Decompiled with JetBrains decompiler
// Type: ConfigGveBossData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ConfigGveBossData
{
  public const string IMAGE_ROOT_PATH = "Texture/GvEBossIcon/";
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "boss_level")]
  public int boss_level;
  [Config(Name = "refresh_time")]
  public int refresh_time;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup Units;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "BaseRewards")]
  public Reward BaseRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "KillRewards")]
  public Reward KillRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "AdvanceRewards")]
  public Reward AdvanceRewards;
  [Config(Name = "advanced_req_id")]
  public int advanced_req_id;

  public string ImagePath
  {
    get
    {
      return "Texture/GvEBossIcon/" + this.image;
    }
  }

  public int TroopCount
  {
    get
    {
      List<UnitGroup.UnitRecord> unitList = this.Units.GetUnitList();
      int num = 0;
      for (int index = 0; index < unitList.Count; ++index)
        num += unitList[index].value;
      return num;
    }
  }
}
