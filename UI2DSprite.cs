﻿// Decompiled with JetBrains decompiler
// Type: UI2DSprite
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Unity2D Sprite")]
public class UI2DSprite : UIBasicSprite
{
  [SerializeField]
  [HideInInspector]
  private Vector4 mBorder = Vector4.zero;
  [SerializeField]
  [HideInInspector]
  private float mPixelSize = 1f;
  [NonSerialized]
  private int mPMA = -1;
  [HideInInspector]
  [SerializeField]
  private UnityEngine.Sprite mSprite;
  [SerializeField]
  [HideInInspector]
  private Material mMat;
  [SerializeField]
  [HideInInspector]
  private Shader mShader;
  [SerializeField]
  [HideInInspector]
  private bool mFixedAspect;
  public UnityEngine.Sprite nextSprite;

  public UnityEngine.Sprite sprite2D
  {
    get
    {
      return this.mSprite;
    }
    set
    {
      if (!((UnityEngine.Object) this.mSprite != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mSprite = value;
      this.nextSprite = (UnityEngine.Sprite) null;
      this.CreatePanel();
    }
  }

  public override Material material
  {
    get
    {
      return this.mMat;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mMat = value;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Shader shader
  {
    get
    {
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.shader;
      if ((UnityEngine.Object) this.mShader == (UnityEngine.Object) null)
        this.mShader = Shader.Find("Unlit/Transparent Colored");
      return this.mShader;
    }
    set
    {
      if (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mShader = value;
      if (!((UnityEngine.Object) this.mMat == (UnityEngine.Object) null))
        return;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Texture mainTexture
  {
    get
    {
      if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null)
        return (Texture) this.mSprite.texture;
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.mainTexture;
      return (Texture) null;
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if (this.mPMA == -1)
      {
        Shader shader = this.shader;
        this.mPMA = !((UnityEngine.Object) shader != (UnityEngine.Object) null) || !shader.name.Contains("Premultiplied") ? 0 : 1;
      }
      return this.mPMA == 1;
    }
  }

  public override float pixelSize
  {
    get
    {
      return this.mPixelSize;
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float a1 = -pivotOffset.x * (float) this.mWidth;
      float a2 = -pivotOffset.y * (float) this.mHeight;
      float b1 = a1 + (float) this.mWidth;
      float b2 = a2 + (float) this.mHeight;
      if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int num1 = Mathf.RoundToInt(this.mSprite.rect.width);
        int num2 = Mathf.RoundToInt(this.mSprite.rect.height);
        int num3 = Mathf.RoundToInt(this.mSprite.textureRectOffset.x);
        int num4 = Mathf.RoundToInt(this.mSprite.textureRectOffset.y);
        int num5 = Mathf.RoundToInt(this.mSprite.rect.width - this.mSprite.textureRect.width - this.mSprite.textureRectOffset.x);
        int num6 = Mathf.RoundToInt(this.mSprite.rect.height - this.mSprite.textureRect.height - this.mSprite.textureRectOffset.y);
        float num7 = 1f;
        float num8 = 1f;
        if (num1 > 0 && num2 > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((num1 & 1) != 0)
            ++num5;
          if ((num2 & 1) != 0)
            ++num6;
          num7 = 1f / (float) num1 * (float) this.mWidth;
          num8 = 1f / (float) num2 * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
        {
          a1 += (float) num5 * num7;
          b1 -= (float) num3 * num7;
        }
        else
        {
          a1 += (float) num3 * num7;
          b1 -= (float) num5 * num7;
        }
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
        {
          a2 += (float) num6 * num8;
          b2 -= (float) num4 * num8;
        }
        else
        {
          a2 += (float) num4 * num8;
          b2 -= (float) num6 * num8;
        }
      }
      float num9;
      float num10;
      if (this.mFixedAspect)
      {
        num9 = 0.0f;
        num10 = 0.0f;
      }
      else
      {
        Vector4 vector4 = this.border * this.pixelSize;
        num9 = vector4.x + vector4.z;
        num10 = vector4.y + vector4.w;
      }
      return new Vector4(Mathf.Lerp(a1, b1 - num9, this.mDrawRegion.x), Mathf.Lerp(a2, b2 - num10, this.mDrawRegion.y), Mathf.Lerp(a1 + num9, b1, this.mDrawRegion.z), Mathf.Lerp(a2 + num10, b2, this.mDrawRegion.w));
    }
  }

  public override Vector4 border
  {
    get
    {
      return this.mBorder;
    }
    set
    {
      if (!(this.mBorder != value))
        return;
      this.mBorder = value;
      this.MarkAsChanged();
    }
  }

  protected override void OnUpdate()
  {
    if ((UnityEngine.Object) this.nextSprite != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.nextSprite != (UnityEngine.Object) this.mSprite)
        this.sprite2D = this.nextSprite;
      this.nextSprite = (UnityEngine.Sprite) null;
    }
    base.OnUpdate();
    if (!this.mFixedAspect || !((UnityEngine.Object) this.mainTexture != (UnityEngine.Object) null))
      return;
    int num1 = Mathf.RoundToInt(this.mSprite.rect.width);
    int num2 = Mathf.RoundToInt(this.mSprite.rect.height);
    int num3 = Mathf.RoundToInt(this.mSprite.textureRectOffset.x);
    int num4 = Mathf.RoundToInt(this.mSprite.textureRectOffset.y);
    int num5 = Mathf.RoundToInt(this.mSprite.rect.width - this.mSprite.textureRect.width - this.mSprite.textureRectOffset.x);
    int num6 = Mathf.RoundToInt(this.mSprite.rect.height - this.mSprite.textureRect.height - this.mSprite.textureRectOffset.y);
    int num7 = num1 + (num3 + num5);
    int num8 = num2 + (num6 + num4);
    float mWidth = (float) this.mWidth;
    float mHeight = (float) this.mHeight;
    float num9 = mWidth / mHeight;
    float num10 = (float) num7 / (float) num8;
    if ((double) num10 < (double) num9)
    {
      float x = (float) (((double) mWidth - (double) mHeight * (double) num10) / (double) mWidth * 0.5);
      this.drawRegion = new Vector4(x, 0.0f, 1f - x, 1f);
    }
    else
    {
      float y = (float) (((double) mHeight - (double) mWidth / (double) num10) / (double) mHeight * 0.5);
      this.drawRegion = new Vector4(0.0f, y, 1f, 1f - y);
    }
  }

  public override void MakePixelPerfect()
  {
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && this.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    Rect rect = this.mSprite.rect;
    int num1 = Mathf.RoundToInt(rect.width);
    int num2 = Mathf.RoundToInt(rect.height);
    if ((num1 & 1) == 1)
      ++num1;
    if ((num2 & 1) == 1)
      ++num2;
    this.width = num1;
    this.height = num2;
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Rect outer = !((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null) ? new Rect(0.0f, 0.0f, (float) mainTexture.width, (float) mainTexture.height) : this.mSprite.textureRect;
    Rect inner = outer;
    Vector4 border = this.border;
    inner.xMin += border.x;
    inner.yMin += border.y;
    inner.xMax -= border.z;
    inner.yMax -= border.w;
    float num1 = 1f / (float) mainTexture.width;
    float num2 = 1f / (float) mainTexture.height;
    outer.xMin *= num1;
    outer.xMax *= num1;
    outer.yMin *= num2;
    outer.yMax *= num2;
    inner.xMin *= num1;
    inner.xMax *= num1;
    inner.yMin *= num2;
    inner.yMax *= num2;
    int size = verts.size;
    this.Fill(verts, uvs, cols, outer, inner);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, size, verts, uvs, cols);
  }
}
