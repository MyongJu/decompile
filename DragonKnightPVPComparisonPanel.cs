﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPVPComparisonPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightPVPComparisonPanel : MonoBehaviour
{
  public GameObject m_ItemPrefab;
  public GameObject m_ItemPrefabLast;
  public GameObject m_NoBenefit;
  public GameObject m_Guard;
  public DragonKnightEquipmentSlot[] m_EquipmentSlots;
  public UILabel m_Talent1;
  public UILabel m_Talent2;
  public UILabel m_Talent3;
  public UITable m_Table;

  public void SetData(UserData you, DragonKnightData yourDK, DragonKnightData otherDK, Dictionary<string, string> benefits, List<InventoryItemData> equippedItemList, int talent1, int talent2, int talent3)
  {
    for (int index = 0; index < this.m_EquipmentSlots.Length; ++index)
      this.m_EquipmentSlots[index].SetData(yourDK, equippedItemList);
    this.m_Talent1.text = string.Format(Utils.XLAT("dragon_knight_pvp_warrior_num"), (object) talent1);
    this.m_Talent2.text = string.Format(Utils.XLAT("dragon_knight_pvp_knight_num"), (object) talent2);
    this.m_Talent3.text = string.Format(Utils.XLAT("dragon_knight_pvp_mage_num"), (object) talent3);
    this.CompareBenefits(yourDK, otherDK, benefits);
  }

  private void CompareBenefits(DragonKnightData yourDK, DragonKnightData otherDK, Dictionary<string, string> benefits)
  {
    if (benefits.Count == 0)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.m_NoBenefit);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Table.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
      this.Reposition();
    }
    else
    {
      Dictionary<string, float> benefits1 = yourDK.Benefits;
      Dictionary<string, float> benefits2 = otherDK.Benefits;
      int num1 = benefits.Count - 1;
      int num2 = 0;
      using (Dictionary<string, string>.Enumerator enumerator = benefits.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, string> current = enumerator.Current;
          GameObject gameObject = num2 != num1 ? Object.Instantiate<GameObject>(this.m_ItemPrefab) : Object.Instantiate<GameObject>(this.m_ItemPrefabLast);
          gameObject.SetActive(true);
          gameObject.transform.parent = this.m_Table.transform;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.transform.localRotation = Quaternion.identity;
          gameObject.transform.localScale = Vector3.one;
          string key = current.Key;
          float source = !benefits1.ContainsKey(key) ? 0.0f : benefits1[key];
          float target = !benefits2.ContainsKey(key) ? 0.0f : benefits2[key];
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[ConfigManager.inst.DB_Properties.GetIDByShortKey(key)];
          gameObject.GetComponent<DragonKnightPVPBenefitRenderer>().SetData(dbProperty, source, target);
          ++num2;
        }
      }
      this.Reposition();
    }
  }

  private void Reposition()
  {
    this.m_Guard.transform.parent = (Transform) null;
    this.m_Guard.transform.parent = this.m_Table.transform;
    this.m_Table.Reposition();
  }
}
