﻿// Decompiled with JetBrains decompiler
// Type: ForceTeleportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class ForceTeleportPopup : Popup
{
  public const string SHOPITEM_EXCALIBUR_WAR_TELEPORT = "shopitem_excalibur_war_new_tele";
  public UILabel m_Price;
  public Color m_Color;
  private ForceTeleportPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as ForceTeleportPopup.Parameter;
    int shopItemPrice = ItemBag.Instance.GetShopItemPrice("shopitem_excalibur_war_new_tele");
    this.m_Price.text = shopItemPrice.ToString();
    this.m_Price.color = (long) shopItemPrice > PlayerData.inst.userData.currency.gold ? Color.red : this.m_Color;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnConfirmPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if ((long) ItemBag.Instance.GetShopItemPrice("shopitem_excalibur_war_new_tele") <= PlayerData.inst.userData.currency.gold)
      ItemBag.Instance.BuyAndUseShopItem("shopitem_excalibur_war_new_tele", this.m_Parameter.request, this.m_Parameter.onConfirm, 1);
    else
      Utils.ShowNotEnoughGoldTip();
  }

  public class Parameter : Popup.PopupParameter
  {
    public Hashtable request;
    public System.Action<bool, object> onConfirm;
  }
}
