﻿// Decompiled with JetBrains decompiler
// Type: TroopInfoManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class TroopInfoManager
{
  private Dictionary<string, TroopInfo> m_TroopInfoDict = new Dictionary<string, TroopInfo>();
  private static TroopInfoManager m_Instance;

  private TroopInfoManager()
  {
  }

  public static TroopInfoManager Instance
  {
    get
    {
      if (TroopInfoManager.m_Instance == null)
        TroopInfoManager.m_Instance = new TroopInfoManager();
      return TroopInfoManager.m_Instance;
    }
  }

  public void Initialize(string text)
  {
    this.m_TroopInfoDict.Clear();
    if (string.IsNullOrEmpty(text))
      return;
    IDictionaryEnumerator enumerator = (Utils.Json2Object(text, true) as Hashtable).GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!(enumerator.Key as string).Equals("headers", StringComparison.OrdinalIgnoreCase))
      {
        ArrayList arrayList = enumerator.Value as ArrayList;
        TroopInfo troopInfo = new TroopInfo();
        troopInfo.m_Key = arrayList[0] as string;
        troopInfo.m_Name = arrayList[41] as string;
        troopInfo.m_Icon = arrayList[17] as string;
        troopInfo.m_Level = 0;
        int.TryParse(arrayList[4] as string, out troopInfo.m_Level);
        if (!this.m_TroopInfoDict.ContainsKey(troopInfo.m_Key))
          this.m_TroopInfoDict.Add(troopInfo.m_Key, troopInfo);
      }
    }
  }

  public TroopInfo GetTroopInfo(string key)
  {
    TroopInfo troopInfo = (TroopInfo) null;
    this.m_TroopInfoDict.TryGetValue(key, out troopInfo);
    return troopInfo;
  }
}
