﻿// Decompiled with JetBrains decompiler
// Type: RoundReplayService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class RoundReplayService
{
  private int _roundIndex = -1;
  private List<IRound> _rounds;
  private bool _isRunning;

  public void Reset(List<IRound> remainRounds)
  {
    this.Clear();
    this._rounds = remainRounds;
  }

  public void AddRound(IRound round)
  {
    if (this._rounds == null)
      this._rounds = new List<IRound>();
    if (this._rounds.Contains(round))
      return;
    this._rounds.Add(round);
  }

  public Hashtable GetNeedValidateData()
  {
    Hashtable hashtable = new Hashtable();
    for (int index = 0; index < this._rounds.Count; ++index)
    {
      if (this._rounds[index].GetValidateData() != null)
        hashtable.Add((object) this._rounds[index].RoundID, (object) this._rounds[index].GetValidateData());
    }
    return hashtable;
  }

  public Hashtable GetRoundLogData()
  {
    Hashtable hashtable1 = new Hashtable();
    for (int index1 = 0; index1 < this._rounds.Count; ++index1)
    {
      List<RoundResult> results = this._rounds[index1].Results;
      Hashtable hashtable2;
      if (hashtable1.ContainsKey((object) this._rounds[index1].RoundID))
      {
        hashtable2 = hashtable1[(object) this._rounds[index1].RoundID] as Hashtable;
      }
      else
      {
        hashtable2 = new Hashtable();
        hashtable1.Add((object) this._rounds[index1].RoundID, (object) hashtable2);
      }
      Hashtable hashtable3 = new Hashtable();
      if (this._rounds[index1].SkillId > 0L)
        hashtable3.Add((object) "skillId", (object) this._rounds[index1].SkillId);
      for (int index2 = 0; index2 < results.Count; ++index2)
      {
        hashtable3.Add((object) results[index2].Target.PlayerId, (object) results[index2].Value);
        if (results[index2].Buffs.Count > 0)
          hashtable3.Add((object) "buffs", (object) results[index2].GetBuffIds());
      }
      hashtable2.Add((object) this._rounds[index1].Trigger.PlayerId, (object) hashtable3);
    }
    return hashtable1;
  }

  public int TotalRound
  {
    get
    {
      return this._rounds.Count;
    }
  }

  private bool HasNext()
  {
    return this._roundIndex + 1 < this._rounds.Count;
  }

  public IRound CurrentRound
  {
    get
    {
      if (this._rounds != null && this._rounds.Count > this._roundIndex)
        return this._rounds[this._roundIndex];
      return (IRound) null;
    }
  }

  public void AutoStep()
  {
    if (!this.HasNext())
      return;
    ++this._roundIndex;
  }

  public bool NextRound()
  {
    if (!this._isRunning)
      return false;
    if (this.HasNext())
    {
      ++this._roundIndex;
      this._rounds[this._roundIndex].Play();
      return true;
    }
    this.Stop();
    return false;
  }

  public void Start()
  {
    if (this._rounds == null || this._rounds.Count == 0)
      return;
    this._isRunning = true;
    this.NextRound();
  }

  public void Process()
  {
    if (!this._isRunning)
      return;
    this._rounds[this._roundIndex].Process();
  }

  public void Stop()
  {
    this._isRunning = false;
  }

  public void Destroy()
  {
    this.Clear();
  }

  public void Clear()
  {
    this.Stop();
    this._rounds = (List<IRound>) null;
    this._roundIndex = -1;
  }
}
