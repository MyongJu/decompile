﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarStateManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AllianceWarStateManager : MonoBehaviour
{
  private Dictionary<AllianceWarActivityData.AllianceWarState, AllianceWarBaseState> states = new Dictionary<AllianceWarActivityData.AllianceWarState, AllianceWarBaseState>();
  public AllianceWarBaseState notOpenState;
  public AllianceWarBaseState registeringState;
  public AllianceWarBaseState registerFailState;
  public AllianceWarBaseState matchingState;
  public AllianceWarBaseState PreparingState;
  public AllianceWarBaseState openedState;
  public AllianceWarBaseState completedState;
  private AllianceWarBaseState currentState;
  private AllianceWarBaseState nextState;
  private AllianceWarActivityData activityData;
  public System.Action<AllianceWarBaseState> OnStateChangeTo;

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int time = 0)
  {
    this.SetState(this.activityData.GetAllianceWarState(), true);
  }

  private void InitAllStates()
  {
    this.states.Add(AllianceWarActivityData.AllianceWarState.NotOpen, this.notOpenState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.Registering, this.registeringState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.RegisterFail, this.registerFailState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.Matching, this.matchingState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.Preparing, this.PreparingState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.Opened, this.openedState);
    this.states.Add(AllianceWarActivityData.AllianceWarState.Completed, this.completedState);
  }

  public void InitData(AllianceWarActivityData data)
  {
    this.AddEventHandler();
    this.activityData = data;
    this.InitAllStates();
  }

  public void UpdateData(AllianceWarActivityData data)
  {
    this.activityData = data;
  }

  public void SetState(AllianceWarActivityData.AllianceWarState state, bool requestData)
  {
    if (this.states.ContainsKey(state))
    {
      this.nextState = this.states[state];
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.currentState)
      {
        if (!((UnityEngine.Object) this.currentState != (UnityEngine.Object) this.nextState))
          return;
        this.currentState.Hide();
        this.currentState = this.nextState;
        this.currentState.SetAllianceWarData(this.activityData);
        this.currentState.SetStateManager(this);
        this.currentState.Show(requestData);
        if (this.OnStateChangeTo == null)
          return;
        this.OnStateChangeTo(this.nextState);
      }
      else
      {
        this.currentState = this.nextState;
        this.currentState.SetAllianceWarData(this.activityData);
        this.currentState.SetStateManager(this);
        this.currentState.Show(requestData);
        if (this.OnStateChangeTo == null)
          return;
        this.OnStateChangeTo(this.nextState);
      }
    }
    else
      Logger.Log("There is no state: " + (object) state);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Release()
  {
    this.states.Clear();
    this.states = (Dictionary<AllianceWarActivityData.AllianceWarState, AllianceWarBaseState>) null;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.currentState)
      this.currentState.Hide();
    this.RemoveEventHandler();
  }
}
