﻿// Decompiled with JetBrains decompiler
// Type: HeroRecruitUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;

public static class HeroRecruitUtils
{
  public const int THE_LAST_TIME = 9;
  public const int TOTAL_TIMES = 10;

  public static void Summon(int type, bool free, System.Action<bool, HeroRecruitPayload> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (type)] = (object) type;
    postData[(object) nameof (free)] = (object) (!free ? 0 : 1);
    MessageHub.inst.GetPortByAction("Legend:templeSummon").SendRequest(postData, (System.Action<bool, object>) ((ret, orgData) =>
    {
      HeroRecruitPayload heroRecruitPayload = new HeroRecruitPayload();
      heroRecruitPayload.Initialize(orgData);
      if (callback == null)
        return;
      callback(ret, heroRecruitPayload);
    }), true);
  }

  public static string GetRecruitText(int type)
  {
    if (type != 3)
      return string.Empty;
    ParliamentHeroSummonGroupInfo heroSummonGroupInfo = ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get("3");
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(heroSummonGroupInfo.activityHero);
    if ((long) NetServerTime.inst.ServerTimestamp <= Utils.DateTime2ServerTime(heroSummonGroupInfo.activityEndTime, "_"))
    {
      if (HeroRecruitUtils.GetSummonCount() == 9)
      {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["0"] = parliamentHeroInfo.Name;
        return ScriptLocalization.GetWithPara("hero_summon_guaranteed_purple_name_tip", para, true);
      }
      Dictionary<string, string> para1 = new Dictionary<string, string>();
      para1["0"] = parliamentHeroInfo.Name;
      para1["1"] = (10 - HeroRecruitUtils.GetSummonCount() - 1).ToString();
      return ScriptLocalization.GetWithPara("hero_summon_condition_purple_probability_description", para1, true);
    }
    if (HeroRecruitUtils.GetSummonCount() == 9)
      return Utils.XLAT("hero_summon_guaranteed_purple_tip");
    Dictionary<string, string> para2 = new Dictionary<string, string>();
    para2["0"] = (10 - HeroRecruitUtils.GetSummonCount() - 1).ToString();
    return ScriptLocalization.GetWithPara("hero_summon_master_circle_tip", para2, true);
  }

  public static ParliamentHeroSummonGroupInfo GetSummonGroupInfo(int type)
  {
    return ConfigManager.inst.DB_ParliamentHeroSummonGroup.Get(type.ToString());
  }

  public static int GetSummonCount()
  {
    LegendTempleData legendTempleData = DBManager.inst.DB_LegendTemple.Get(PlayerData.inst.uid);
    if (legendTempleData != null)
      return legendTempleData.SpecialSummonCount % 10;
    return 0;
  }
}
