﻿// Decompiled with JetBrains decompiler
// Type: PositionAutoSetter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PositionAutoSetter : MonoBehaviour
{
  [SerializeField]
  private List<GameObject> _allNodes = new List<GameObject>();
  private List<Transform> _allLocalPosition = new List<Transform>();
  private List<Transform> _allTransform = new List<Transform>();
  [SerializeField]
  private string _description;
  private bool _haveError;

  private void Start()
  {
    this.RecordPosition();
  }

  private void Update()
  {
    if (this._haveError)
      return;
    int num = 0;
    for (int index = 0; index < this._allNodes.Count; ++index)
    {
      if (this._allNodes[index].activeSelf)
      {
        Vector3 localPosition = this._allTransform[index].localPosition;
        localPosition.x = this._allLocalPosition[num++].localPosition.x;
        this._allTransform[index].localPosition = localPosition;
      }
    }
  }

  private void RecordPosition()
  {
    this._allLocalPosition.Clear();
    for (int index1 = 0; index1 < this._allNodes.Count; ++index1)
    {
      Transform transform = this._allNodes[index1].transform;
      while ((Object) transform != (Object) null && !((Object) transform.parent == (Object) this.transform))
        transform = transform.parent;
      if ((Object) transform == (Object) null)
      {
        this._haveError = true;
        Logger.Error((object) "PositionAutoSetter Use Error, all the nodes should be the child node of PositionAutoSetter");
      }
      else
      {
        bool activeSelf = transform.gameObject.activeSelf;
        transform.gameObject.SetActive(false);
        GameObject gameObject = Object.Instantiate<GameObject>(transform.gameObject);
        gameObject.transform.SetParent(transform.parent);
        gameObject.transform.localScale = transform.localScale;
        for (int index2 = gameObject.transform.childCount - 1; index2 >= 0; --index2)
          Object.Destroy((Object) gameObject.transform.GetChild(index2).gameObject);
        foreach (Component component in gameObject.GetComponents<Component>())
        {
          if (!(component is Transform))
          {
            if (component is UIWidget)
            {
              UIWidget uiWidget = component as UIWidget;
              if ((bool) ((Object) uiWidget))
                uiWidget.alpha = 0.0f;
            }
            else
              Object.Destroy((Object) component);
          }
        }
        gameObject.SetActive(true);
        this._allLocalPosition.Add(gameObject.transform);
        this._allTransform.Add(transform);
        transform.gameObject.SetActive(activeSelf);
      }
    }
  }
}
