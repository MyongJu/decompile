﻿// Decompiled with JetBrains decompiler
// Type: WonderTileRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class WonderTileRender : MonoBehaviour
{
  private const string prebuildingname = "Texture/WorldMap/worldmap_stronghold_";
  public UITexture wonderIcon;
  public UILabel wonderName;
  public UILabel kingName;
  public UISprite kingflag;
  public GameObject hotflag;
  public GameObject selfflag;
  public UILabel GroupName;
  public WonderData data;
  public System.Action<int> onClicked;

  public void seedData(WonderData data)
  {
    this.data = data;
    BuilderFactory.Instance.Build((UIWidget) this.wonderIcon, "Texture/WorldMap/worldmap_stronghold_" + data.wonderIcon, (System.Action<bool>) null, false, false, true, string.Empty);
    this.wonderName.text = !string.IsNullOrEmpty(data.wonderName) ? string.Format("{0} #{1}", (object) data.wonderName, (object) data.kid) : string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) data.kid);
    this.kingName.text = string.Empty;
    if (data.kingName != null)
      this.kingName.text = ScriptLocalization.Get("id_king", true) + data.kingName;
    if (!string.IsNullOrEmpty(data.GroupName))
      this.GroupName.text = data.GroupName;
    if (data.kingdomColonizedBy > 0)
    {
      this.kingName.text = ScriptLocalization.GetWithPara("kingdom_status_occupied_by", new Dictionary<string, string>()
      {
        {
          "0",
          data.kingdomColonizedBy.ToString()
        }
      }, true);
      this.kingName.color = Color.red;
    }
    this.kingflag.spriteName = data.FlagIconPath;
    this.hotflag.SetActive(data.ishot == 1);
    this.selfflag.SetActive(PlayerData.inst.playerCityData.cityLocation.K == data.kid);
  }

  public void OnClicked()
  {
    if (this.onClicked == null)
      return;
    this.onClicked(this.data.kid);
  }

  public void OnDestroy()
  {
    BuilderFactory.Instance.Release((UIWidget) this.wonderIcon);
  }
}
