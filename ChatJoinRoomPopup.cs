﻿// Decompiled with JetBrains decompiler
// Type: ChatJoinRoomPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ChatJoinRoomPopup : Popup
{
  public const string POPUP_TYPE = "Chat/ChatJoinRoomPopup";
  public JoinRoomDlg joinRoomDlg;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.joinRoomDlg.Show();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
  }
}
