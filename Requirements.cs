﻿// Decompiled with JetBrains decompiler
// Type: Requirements
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class Requirements : ICustomParse
{
  public Dictionary<string, int> requires = new Dictionary<string, int>();
  private List<Requirements.RequireValuePair> requireBuildings;

  public void Parse(object content)
  {
    Hashtable hashtable = content as Hashtable;
    if (hashtable == null)
      return;
    foreach (DictionaryEntry dictionaryEntry in hashtable)
      this.requires.Add(dictionaryEntry.Key.ToString(), int.Parse(dictionaryEntry.Value.ToString()));
  }

  public List<Requirements.RequireValuePair> RequireBuildings()
  {
    if (this.requireBuildings == null)
    {
      this.requireBuildings = new List<Requirements.RequireValuePair>();
      using (Dictionary<string, int>.Enumerator enumerator = this.requires.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          if (ConfigManager.inst.DB_ConfigAddress.IsBuilding(int.Parse(current.Key)))
            this.requireBuildings.Add(new Requirements.RequireValuePair(int.Parse(current.Key), current.Value));
        }
      }
    }
    return this.requireBuildings;
  }

  public class RequireValuePair
  {
    public int internalID;
    public int value;

    public RequireValuePair(int internalID, int value)
    {
      this.internalID = internalID;
      this.value = value;
    }
  }
}
