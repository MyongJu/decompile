﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressJoinedSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceFortressJoinedSlotItem : MonoBehaviour
{
  private bool firstTime = true;
  public bool isLock = true;
  public UITexture playerIcon;
  public UILabel playerName;
  public UILabel memberCount;
  public UILabel troopsCount;
  public UIProgressBar progressBar;
  public UILabel progressLabel;
  public UIButton speedUpBt;
  public UILabel btnsts;
  public UILabel tip;
  public UISprite dragonIcon;
  public GameObject noDragon;
  public GameObject OpenContent;
  public GameObject dragonContent;
  public UISprite openBg;
  public UISprite normalBg;
  public UISprite arrow;
  public DragonUIInfo dragonInfo;
  public MarchTroopsUIInfo marchInfo;
  public UITable childContainer;
  public UIButton disband;
  private long march_id;
  private string state;
  private bool isOpen;
  public System.Action OnOpenCallBackDelegate;

  public void SetData(long march_id, string rally_id)
  {
    this.march_id = march_id;
    this.state = rally_id;
    MarchData marchData = DBManager.inst.DB_March.Get(march_id);
    if (marchData == null)
      return;
    this.troopsCount.text = marchData.troopsInfo.totalCount.ToString();
    UserData userData = DBManager.inst.DB_User.Get(marchData.ownerUid);
    if (userData == null)
      return;
    this.playerName.text = userData.userName;
    CustomIconLoader.Instance.requestCustomIcon(this.playerIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.playerIcon, userData.LordTitle, 1);
    this.RefreshUI();
    this.RefreshTroops();
    this.RefreshDragon();
    NGUITools.SetActive(this.disband.gameObject, false);
    this.childContainer.repositionNow = true;
    this.childContainer.Reposition();
    this.UpdateCallBackBtnState();
  }

  private void UpdateCallBackBtnState()
  {
    int num1 = 0;
    int num2 = 0;
    AllianceData allianceData = PlayerData.inst.allianceData;
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    AllianceFortressData dataByCoordinate = DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(PVPSystem.Instance.Map.SelectedTile.Location);
    if (dataByCoordinate == null || marchData == null)
      return;
    if (allianceData != null && dataByCoordinate.OwnerAllianceID == allianceData.allianceId && marchData.ownerAllianceId != allianceData.allianceId)
      this.btnsts.text = ScriptLocalization.Get("watchtower_march_send_back_reinforcements_button", true);
    else if (allianceData == null || marchData.ownerAllianceId != allianceData.allianceId || dataByCoordinate.allianceId != allianceData.allianceId)
    {
      if (marchData.ownerUid == PlayerData.inst.uid)
        this.btnsts.text = ScriptLocalization.Get("march_callback", true);
      else
        NGUITools.SetActive(this.speedUpBt.gameObject, false);
    }
    else
    {
      if (allianceData != null)
      {
        AllianceMemberData allianceMemberData1 = allianceData.members.Get(PlayerData.inst.uid);
        if (allianceMemberData1 != null)
          num1 = allianceMemberData1.title;
        AllianceMemberData allianceMemberData2 = allianceData.members.Get(marchData.ownerUid);
        if (allianceMemberData2 != null)
          num2 = allianceMemberData2.title;
      }
      this.speedUpBt.isEnabled = 5 <= num1 || (num1 != 4 ? marchData.ownerUid == PlayerData.inst.uid : num2 < num1);
      if (marchData.ownerUid == PlayerData.inst.uid)
        this.speedUpBt.isEnabled = true;
      if (this.speedUpBt.isEnabled)
      {
        if (marchData.ownerUid == PlayerData.inst.uid)
          this.btnsts.text = ScriptLocalization.Get("march_callback", true);
        else
          this.btnsts.text = ScriptLocalization.Get("watchtower_march_send_back_reinforcements_button", true);
      }
      else
        NGUITools.SetActive(this.speedUpBt.gameObject, false);
    }
  }

  private void RefreshUI()
  {
    NGUITools.SetActive(this.progressBar.gameObject, false);
    NGUITools.SetActive(this.tip.gameObject, true);
    NGUITools.SetActive(this.speedUpBt.gameObject, true);
    this.tip.text = this.GetHintByState();
  }

  public bool IsOpen
  {
    get
    {
      return this.isOpen;
    }
    set
    {
      if (this.isOpen == value)
        return;
      this.isOpen = value;
      if (this.isOpen)
        this.arrow.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -90f);
      else
        this.arrow.transform.localEulerAngles = Vector3.zero;
    }
  }

  public void OnClickHandler()
  {
    if (this.march_id <= 0L)
      return;
    this.IsOpen = !this.IsOpen;
    NGUITools.SetActive(this.OpenContent, this.IsOpen);
    NGUITools.SetActive(this.normalBg.gameObject, false);
    DBManager.inst.DB_March.Get(this.march_id);
    NGUITools.SetActive(this.disband.transform.parent.gameObject, false);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      if (!this.IsOpen)
      {
        this.normalBg.height = (int) byte.MaxValue;
      }
      else
      {
        Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform, false);
        if (this.disband.transform.parent.gameObject.activeSelf)
        {
          if (this.firstTime)
          {
            this.firstTime = false;
            this.normalBg.height = (int) relativeWidgetBounds.size.y + 150;
          }
          else
            this.normalBg.height = (int) relativeWidgetBounds.size.y + 30;
        }
        else if (this.firstTime)
        {
          this.firstTime = false;
          this.normalBg.height = (int) relativeWidgetBounds.size.y;
        }
        else
          this.normalBg.height = (int) relativeWidgetBounds.size.y + 80;
      }
      NGUITools.SetActive(this.normalBg.gameObject, true);
      this.normalBg.ResizeCollider();
      NGUITools.AddWidgetCollider(this.transform.parent.gameObject);
      this.childContainer.Reposition();
      if (this.OnOpenCallBackDelegate == null)
        return;
      this.OnOpenCallBackDelegate();
    }));
  }

  private void RefreshTroops()
  {
    this.marchInfo.SetData(this.march_id);
  }

  private void RefreshDragon()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    string skillType = "attack";
    bool state = false;
    NGUITools.SetActive(this.dragonIcon.gameObject, marchData.withDragon);
    NGUITools.SetActive(this.dragonInfo.gameObject, state);
    NGUITools.SetActive(this.noDragon, !marchData.withDragon);
    if (!state)
      return;
    this.dragonInfo.SetDragon(marchData.dragonId, skillType, (int) marchData.dragon.tendency, marchData.dragon.level);
  }

  public void RecallMarch()
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.march_id);
    if (marchData.ownerUid == PlayerData.inst.uid)
      GameEngine.Instance.marchSystem.Recall(this.march_id);
    else
      GameEngine.Instance.marchSystem.SendFortressTroopHome(DBManager.inst.DB_AllianceFortress.GetDataByCoordinate(PVPSystem.Instance.Map.SelectedTile.Location).fortressId, marchData.ownerUid, (System.Action<bool, object>) null);
  }

  public void Refresh()
  {
    this.RefreshUI();
  }

  public void Speeduphandler()
  {
    UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) new MarchSpeedUpPopup.Parameter()
    {
      marchData = DBManager.inst.DB_March.Get(this.march_id)
    });
  }

  public void Clear()
  {
    this.OnOpenCallBackDelegate = (System.Action) null;
  }

  private string GetHintByState()
  {
    string empty = string.Empty;
    string Term = string.Empty;
    string state = this.state;
    if (state != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressJoinedSlotItem.\u003C\u003Ef__switch\u0024map1E == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceFortressJoinedSlotItem.\u003C\u003Ef__switch\u0024map1E = new Dictionary<string, int>(2)
        {
          {
            "building",
            0
          },
          {
            "defending",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AllianceFortressJoinedSlotItem.\u003C\u003Ef__switch\u0024map1E.TryGetValue(state, out num))
      {
        switch (num)
        {
          case 0:
            Term = "alliance_fort_player_status_constructing";
            break;
          case 1:
            Term = "alliance_fort_player_status_defending";
            break;
        }
      }
    }
    return ScriptLocalization.Get(Term, true);
  }
}
