﻿// Decompiled with JetBrains decompiler
// Type: NotificationFactory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class NotificationFactory
{
  private static NotificationFactory _instance;

  private NotificationFactory()
  {
  }

  public static NotificationFactory Instance
  {
    get
    {
      if (NotificationFactory._instance == null)
        NotificationFactory._instance = new NotificationFactory();
      return NotificationFactory._instance;
    }
  }

  public NotificationData CreateData(string configId, int finishTime, params string[] values)
  {
    NotificationData notificationData = new NotificationData();
    Dictionary<string, string> dict = new Dictionary<string, string>();
    for (int index = 0; index < values.Length; ++index)
      dict.Add((index + 1).ToString(), values[index]);
    NotificationStaticInfo data = ConfigManager.inst.DB_Notifications.GetData(configId);
    string empty = string.Empty;
    string str = dict.Count <= 0 ? data.LocalMessage : data.GetLocalMessage(dict);
    notificationData.Message = str;
    notificationData.SoundName = data.LocalSoundName;
    notificationData.RemainTime = finishTime;
    notificationData.ID = data.ID;
    return notificationData;
  }
}
