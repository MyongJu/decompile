﻿// Decompiled with JetBrains decompiler
// Type: AtlasManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AtlasManager : MonoBehaviour
{
  private static AtlasManager _inst;
  public UIAtlas atlasCommon;
  public UIAtlas atlasBasicGui;
  public UIAtlas atlasGemIcons;
  public UIAtlas atlasPVETiles;
  public UIAtlas atlasGUI_2;
  public UIAtlas atlasScene_UI;
  public UIAtlas atlasLordTitle_UI;

  public static AtlasManager inst
  {
    get
    {
      return AtlasManager._inst;
    }
  }

  private void Awake()
  {
    AtlasManager._inst = this;
  }

  private void Start()
  {
  }

  private void Update()
  {
  }
}
