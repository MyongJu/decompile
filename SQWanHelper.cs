﻿// Decompiled with JetBrains decompiler
// Type: SQWanHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UnityEngine;

public class SQWanHelper : MonoBehaviour
{
  public System.Action LoginSuccessHandler;
  public System.Action LoginFailureHandler;
  public System.Action PaymentSuccessHandler;
  public System.Action PaymentFailureHandler;
  private static int retryTimes;

  private void OnInitSuccess()
  {
    SQWanSdkManager.Instance.RegisterListeners();
    SQWanSdkManager.Instance.CallGetAppConfig();
    SQWanSdkManager.Instance.Inited = true;
    Preloader.inst.On37InitSuccess();
  }

  private void OnSwitchAccountClickAndSuccess(string result)
  {
    this.Log(string.Format("OnSwitchAccountClickAndSuccess {0}", (object) result));
    SQWanSdkManager.Instance.Token = JsonReader.Deserialize<Dictionary<string, string>>(result)["token"];
    SQWanSdkManager.Instance.IsLoggedInGame = true;
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  private void OnLogoutClicked(string result)
  {
    this.Log(string.Format("OnLogoutClicked {0}", (object) result));
    SQWanSdkManager.Instance.Logout();
  }

  private void OnLoginSuccess(string result)
  {
    this.Log(string.Format("OnLoginSuccess {0}", (object) result));
    string str = JsonReader.Deserialize<Dictionary<string, string>>(result)["token"];
    if (string.IsNullOrEmpty(str))
    {
      ++SQWanHelper.retryTimes;
      SQWanSdkManager.Instance.CallLoginRetry(SQWanHelper.retryTimes, SQWanSdkManager.LoginRetry.EmptyToken);
    }
    else
    {
      SQWanSdkManager.Instance.Token = str;
      if (this.LoginSuccessHandler == null)
        return;
      SQWanHelper.retryTimes = 0;
      this.LoginSuccessHandler();
    }
  }

  private void OnLoginFailure(string result)
  {
    this.Log(string.Format("OnLoginFailure {0}", (object) result));
    if (this.LoginFailureHandler == null)
      return;
    SQWanHelper.retryTimes = 0;
    this.LoginFailureHandler();
  }

  private void OnChangeAccountSuccess(string result)
  {
    this.Log(string.Format("OnChangeAccountSuccess {0}", (object) result));
    string str = JsonReader.Deserialize<Dictionary<string, string>>(result)["token"];
    if (string.IsNullOrEmpty(str))
    {
      ++SQWanHelper.retryTimes;
      SQWanSdkManager.Instance.CallLoginRetry(SQWanHelper.retryTimes, SQWanSdkManager.LoginRetry.EmptyToken);
    }
    else
    {
      SQWanSdkManager.Instance.Token = str;
      if (this.LoginSuccessHandler == null)
        return;
      SQWanHelper.retryTimes = 0;
      this.LoginSuccessHandler();
    }
  }

  private void OnChangeAccountFailure(string result)
  {
    this.Log(string.Format("OnChangeAccountFailure {0}", (object) result));
    if (this.LoginFailureHandler == null)
      return;
    SQWanHelper.retryTimes = 0;
    this.LoginFailureHandler();
  }

  private void OnGetAppConfig(string result)
  {
    this.Log(string.Format("OnGetAppConfig {0}", (object) result));
    Dictionary<string, string> dictionary = JsonReader.Deserialize<Dictionary<string, string>>(result);
    string str1 = dictionary["gid"];
    string str2 = dictionary["pid"];
    string str3 = dictionary["refer"];
    SQWanSdkManager.Instance.GID = str1;
    SQWanSdkManager.Instance.PID = str2;
    SQWanSdkManager.Instance.Refer = str3;
  }

  private void OnExitClicked(string result)
  {
    this.Log(string.Format("OnExitClicked {0}", (object) result));
    GameEngine.Instance.QuitGame();
  }

  private void OnPaymentSuccess(string result)
  {
    this.Log(string.Format("OnPaymentSuccess {0}", (object) result));
    if (this.PaymentSuccessHandler == null)
      return;
    this.PaymentSuccessHandler();
  }

  private void OnPaymentFailure(string result)
  {
    this.Log(string.Format("OnPaymentFailure {0}", (object) result));
    if (this.PaymentFailureHandler == null)
      return;
    this.PaymentFailureHandler();
  }

  private void Log(string msg)
  {
    SQWanSdkManager.Log(msg);
  }
}
