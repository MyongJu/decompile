﻿// Decompiled with JetBrains decompiler
// Type: AllianceTeleportUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;

public class AllianceTeleportUse : ItemBaseUse
{
  public AllianceTeleportUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public AllianceTeleportUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2300150", true), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
      {
        title = ScriptLocalization.Get("alliance_teleport_uppercase_title", true),
        content = ScriptLocalization.Get("alliance_teleport_confirm_use_description", true),
        yes = ScriptLocalization.Get("id_uppercase_yes", true),
        no = ScriptLocalization.Get("id_no", true),
        yesCallback = new System.Action(AllianceTeleportUse.OnUseAllianceTeleport),
        noCallback = (System.Action) null
      });
  }

  public static void OnUseAllianceTeleport()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      Hashtable postData = new Hashtable();
      postData[(object) "target_uid"] = (object) allianceData.creatorId;
      MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        if (DBManager.inst.DB_City.GetByUid(allianceData.creatorId).cityLocation.K == PlayerData.inst.playerCityData.cityLocation.K)
          AllianceTeleportUse.TakeAction();
        else
          UIManager.inst.toast.Show(Utils.XLAT("toast_excalibur_war_tele_leader_not_in_kingdom"), (System.Action) null, 4f, false);
      }), true);
    }
    else
      AllianceTeleportUse.TakeAction();
  }

  private static void TakeAction()
  {
    TeleportManager.Instance.OnInviteTeleport(TeleportMode.ALLIANCE_TELEPORT);
  }
}
