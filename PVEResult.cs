﻿// Decompiled with JetBrains decompiler
// Type: PVEResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PVEResult : MonoBehaviour
{
  public UILabel titleLabel;
  public UILabel xpEarned;
  public UILabel troopsSurvived;
  public UILabel enemyKilled;
  public UILabel lostInfantryLabel;
  public UISlider lostInfantrySlider;
  public UILabel lostCavalryLabel;
  public UISlider lostCavalrySlider;
  public UILabel lostRangedLabel;
  public UISlider lostRangedSlider;
  public UILabel lostArtilleryCloseLabel;
  public UISlider lostArtilleryCloseSlider;
  public UILabel lostArtilleryDistanceLabel;
  public UISlider lostArtilleryDistanceSlider;
  public UISprite[] rewardObject;
  public UILabel[] rewardLabel;

  public enum TroopType
  {
    infantry,
    ranged,
    cavalry,
    mage,
    siege,
    count,
  }
}
