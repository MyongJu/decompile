﻿// Decompiled with JetBrains decompiler
// Type: HeroItemFilter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class HeroItemFilter : MonoBehaviour
{
  public int accessorySlotIndex = -1;
  public long selectItemId;
  public long equipedId;
  private Dictionary<long, InventoryItemData> items;
  private Dictionary<long, ItemSlot_New> itemSlots;
  [SerializeField]
  private HeroItemFilter.Panel panel;
  public HeroDlg2 heroDlg;
  public GameObject itemSlotOrg;
  public UIGrid itemSlotsGrid;
  public GameObject filterArrows;
  public UILabel itemTypeName;
  private int _itemTypeFilterIndex;
  public HeroInfoPopup infoPopup;

  public void SlotClicked(long itemId)
  {
  }

  public void OnHelpClick()
  {
    this.infoPopup.gameObject.SetActive(true);
    this.infoPopup.Init(this.selectItemId);
  }

  public void OnEquipClick()
  {
  }

  public void OnEnhanceClick()
  {
  }

  public void EmbedClick()
  {
  }

  public void OnBackClick()
  {
    this.heroDlg.SelectRightArea(HeroDlg2.HeroRightType.none);
    this.heroDlg.SelectLeftArea(HeroDlg2.HeroLeftType.none);
    this.heroDlg.backButton.SetActive(false);
  }

  [Serializable]
  protected class Panel
  {
    public UIButton equip;
    public UIButton enhance;
    public UIButton embed;
    public UILabel equipText;

    public UIButton this[HeroItemFilter.Panel.ButtonType type]
    {
      get
      {
        switch (type)
        {
          case HeroItemFilter.Panel.ButtonType.Equip:
            return this.equip;
          case HeroItemFilter.Panel.ButtonType.Enhance:
            return this.enhance;
          case HeroItemFilter.Panel.ButtonType.Embed:
            return this.embed;
          default:
            return (UIButton) null;
        }
      }
    }

    public enum ButtonType
    {
      Equip,
      Enhance,
      Embed,
    }
  }
}
