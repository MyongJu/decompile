﻿// Decompiled with JetBrains decompiler
// Type: ConfigFallenKnightDifficult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigFallenKnightDifficult
{
  private Dictionary<string, FallenKnightDifficultInfo> datas;
  private Dictionary<int, FallenKnightDifficultInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FallenKnightDifficultInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public List<FallenKnightDifficultInfo> GetFallenKnightDifficultList()
  {
    List<FallenKnightDifficultInfo> knightDifficultInfoList = new List<FallenKnightDifficultInfo>();
    Dictionary<string, FallenKnightDifficultInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.ID))
        knightDifficultInfoList.Add(enumerator.Current);
    }
    knightDifficultInfoList.Sort(new Comparison<FallenKnightDifficultInfo>(this.SortByID));
    return knightDifficultInfoList;
  }

  public int SortByID(FallenKnightDifficultInfo a, FallenKnightDifficultInfo b)
  {
    return int.Parse(a.ID).CompareTo(int.Parse(b.ID));
  }
}
