﻿// Decompiled with JetBrains decompiler
// Type: CityBG
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class CityBG : MonoBehaviour
{
  public Vector3 initCameraPosition = new Vector3(-683280f, 1894185f, 1f);

  public void Init()
  {
    if (!((Object) UIManager.inst != (Object) null) || !((Object) UIManager.inst.cityCamera != (Object) null))
      return;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    if (!(bool) ((Object) cityCamera))
      return;
    cityCamera.transform.localPosition = this.initCameraPosition;
  }
}
