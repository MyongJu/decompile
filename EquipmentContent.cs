﻿// Decompiled with JetBrains decompiler
// Type: EquipmentContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentContent : MonoBehaviour
{
  public GameObject template;
  public UIGrid grid;
  public UIScrollView scrollView;
  public EquipmentInfo info;
  public GameObject empty;
  public GameObject btns;
  public UIButton equipBtn;
  public UIButton removeBtn;
  public UIButton replaceBtn;
  private long currentSelectItemID;
  private EquipmentRender current;
  private List<EquipmentRender> contentList;
  private List<long> infos;
  private BagType bagType;

  public void Init(BagType bagType, int currentSelectEquipmentID = -1)
  {
    this.current = (EquipmentRender) null;
    this.bagType = bagType;
    this.ClearGrid();
    if (this.currentSelectItemID != -1L)
      this.currentSelectItemID = this.currentSelectItemID;
    this.infos = new List<long>();
    List<InventoryItemData> myItemList = DBManager.inst.GetInventory(bagType).GetMyItemList();
    for (int index = 0; index < myItemList.Count; ++index)
    {
      if (!myItemList[index].InSale)
        this.infos.Add(myItemList[index].itemId);
    }
    this.Sort(this.infos);
    this.contentList = new List<EquipmentRender>();
    for (int index = 0; index < this.infos.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template);
      gameObject.SetActive(true);
      EquipmentRender component = gameObject.GetComponent<EquipmentRender>();
      component.OnSelectedHandler = new System.Action<EquipmentRender>(this.OnSelectedHandler);
      this.contentList.Add(component);
      InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.infos[index]);
      component.FeedData((IComponentData) new EquipmentComponentData(myItemByItemId.internalId, myItemByItemId.enhanced, this.bagType, myItemByItemId.itemId));
      if (myItemByItemId.itemId == this.currentSelectItemID)
      {
        this.current = component;
        this.current.Select = true;
      }
    }
    if (this.contentList.Count > 0)
    {
      if ((UnityEngine.Object) this.current == (UnityEngine.Object) null)
      {
        this.current = this.contentList[0];
        this.current.Select = true;
        this.currentSelectItemID = this.current.componentData.itemId;
      }
      this.info.gameObject.SetActive(true);
      this.info.Init(this.current.componentData.bagType, this.current.componentData.equipmentInfo.internalId, this.current.componentData.itemId);
      this.btns.SetActive(true);
    }
    else
    {
      this.info.gameObject.SetActive(false);
      this.btns.SetActive(false);
    }
    this.grid.repositionNow = true;
    this.empty.SetActive(this.contentList.Count == 0);
    if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
      this.ConfigBtns();
    this.removeBtn.onClick.Clear();
    this.removeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnRemoveBtnClick)));
    this.equipBtn.onClick.Clear();
    this.equipBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnEquipBtnClick)));
    this.replaceBtn.onClick.Clear();
    this.replaceBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnReplaceBtnClick)));
    DBManager.inst.GetInventory(bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(bagType).onDataRemoved += new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(bagType).onDataChanged += new System.Action<long, long>(this.OnInventoryChanged);
  }

  private void OnInventoryChanged(long uid, long itemId)
  {
    this.Init(this.bagType, -1);
  }

  public void Dispose()
  {
    DBManager.inst.GetInventory(this.bagType).onDataRemoved -= new System.Action<long, long>(this.OnInventoryChanged);
    DBManager.inst.GetInventory(this.bagType).onDataChanged -= new System.Action<long, long>(this.OnInventoryChanged);
    this.info.Dispose();
  }

  public void OnDecomposeBtnClick()
  {
    UIManager.inst.OpenPopup("Equipment/EquipmentDecomposePopup", (Popup.PopupParameter) new EquipmentDecomposePopup.Parameter()
    {
      bagType = this.bagType,
      equipmentMainInfo = this.current.componentData.equipmentInfo,
      itemID = this.current.componentData.itemId
    });
  }

  public void OnRemoveBtnClick()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, (InventoryItemData) null, myItemByItemId, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.UnEquip(this.bagType, myItemByItemId.itemId, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      this.Init(this.bagType, -1);
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public void OnEquipBtnClick()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, myItemByItemId, (InventoryItemData) null, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.Equip(this.bagType, myItemByItemId.itemId, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      this.Init(this.bagType, -1);
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public void OnReplaceBtnClick()
  {
    int equippedItemId = this.GetEquippedItemID(this.bagType, this.current.componentData.equipmentInfo.type);
    InventoryItemData myItemByItemId1 = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
    InventoryItemData myItemByItemId2 = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID((long) equippedItemId);
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, myItemByItemId1, myItemByItemId2, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.ReplaceEquipment(this.bagType, (long) equippedItemId, (long) (int) myItemByItemId1.itemId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.Init(this.bagType, -1);
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  private int GetEquippedItemID(BagType bagType, int equipType)
  {
    switch (bagType)
    {
      case BagType.Hero:
        return PlayerData.inst.heroData.equipments.GetSlotEquipID(equipType);
      case BagType.DragonKnight:
        if (PlayerData.inst.dragonKnightData != null)
          return PlayerData.inst.dragonKnightData.equipments.GetSlotEquipID(equipType);
        return 0;
      default:
        return 0;
    }
  }

  private void OnSelectedHandler(EquipmentRender selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.componentData.itemId == selected.componentData.itemId)
      return;
    this.currentSelectItemID = selected.componentData.itemId;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.info.Init(this.current.componentData.bagType, this.current.componentData.equipmentInfo.internalId, this.current.componentData.itemId);
    this.ConfigBtns();
  }

  private void ConfigBtns()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.current.componentData.itemId);
    this.removeBtn.gameObject.SetActive(myItemByItemId.isEquipped);
    this.equipBtn.gameObject.SetActive(this.GetEquippedItemID(this.bagType, this.current.componentData.equipmentInfo.type) == 0 && !myItemByItemId.isEquipped);
    this.replaceBtn.gameObject.SetActive(this.GetEquippedItemID(this.bagType, this.current.componentData.equipmentInfo.type) > 0 && !myItemByItemId.isEquipped);
    if (this.bagType == BagType.Hero)
    {
      this.equipBtn.isEnabled = this.current.componentData.equipmentInfo.heroLevelMin <= PlayerData.inst.heroData.level;
      this.replaceBtn.isEnabled = this.current.componentData.equipmentInfo.heroLevelMin <= PlayerData.inst.heroData.level;
    }
    else
    {
      this.equipBtn.isEnabled = this.current.componentData.equipmentInfo.heroLevelMin <= DragonKnightUtils.GetLevel();
      this.replaceBtn.isEnabled = this.current.componentData.equipmentInfo.heroLevelMin <= DragonKnightUtils.GetLevel();
    }
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  private void Sort(List<long> list)
  {
    list.Sort((Comparison<long>) ((x, y) =>
    {
      InventoryItemData myItemByItemId1 = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(x);
      InventoryItemData myItemByItemId2 = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(y);
      ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(myItemByItemId1.internalId);
      ConfigEquipmentMainInfo data2 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(myItemByItemId2.internalId);
      if (data1.type != data2.type)
        return data1.type.CompareTo(data2.type);
      if (data1.quality != data2.quality)
        return data2.quality.CompareTo(data1.quality);
      if (data1.heroLevelMin == data2.heroLevelMin)
        return data1.tendency.CompareTo(data2.tendency);
      return data2.heroLevelMin.CompareTo(data1.heroLevelMin);
    }));
  }
}
