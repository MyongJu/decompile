﻿// Decompiled with JetBrains decompiler
// Type: OpeningCinematicController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class OpeningCinematicController : MonoBehaviour
{
  public float battletime = 10f;
  public float showresulttime = 3f;
  private const string introduce = "Prefab/OpeningCinematic/Introduce";
  private const string talk1path = "Prefab/OpeningCinematic/OpeningTalk1";
  private const string firename = "fx_city_fire";
  private const string resultpath = "Prefab/OpeningCinematic/OpeningResult";
  public Camera cinematicCamera;
  public Animator ctrlAnimator;
  private GameObject go_introduce;
  [HideInInspector]
  public OpeningDragonController odc;

  private void Start()
  {
    this.BeginIntroduce();
  }

  public void PlayAction(int action)
  {
    if (!this.cinematicCamera.gameObject.activeSelf)
      this.cinematicCamera.gameObject.SetActive(true);
    this.ctrlAnimator.Play("Action" + (object) action);
  }

  private void OnCameraActionFinished(AnimationEvent obj)
  {
    switch (obj.intParameter)
    {
      case 1:
        this.BeginTalk();
        break;
      case 2:
        this.BeginAttack();
        break;
    }
  }

  private void StopIntroduce()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.go_introduce);
  }

  private void BeginIntroduce()
  {
    AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/Introduce", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      if (!ret)
        return;
      this.go_introduce = Utils.DuplicateGOB(obj as GameObject, UIManager.inst.ui2DCamera.transform);
      AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/Introduce", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
  }

  private void BeginTalk()
  {
    AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/OpeningTalk1", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      Utils.DuplicateGOB(obj as GameObject, UIManager.inst.ui2DCamera.transform);
      AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/OpeningTalk1", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }), (System.Type) null);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.go_introduce);
  }

  private void BeginFire()
  {
    GameObject child = Utils.FindChild(this.gameObject, "fx_city_fire");
    if (!((UnityEngine.Object) null != (UnityEngine.Object) child))
      return;
    child.SetActive(true);
  }

  private void BeginAttack()
  {
    foreach (OpeningTroopController componentsInChild in this.GetComponentsInChildren<OpeningTroopController>())
      componentsInChild.StartCoroutine(componentsInChild.BeginMove());
    this.odc.BeginMove();
    this.transform.FindChild("king").GetComponent<Animator>().Play("Attack");
    this.StartCoroutine(this.ShowResult());
  }

  [DebuggerHidden]
  private IEnumerator ShowResult()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new OpeningCinematicController.\u003CShowResult\u003Ec__Iterator8C()
    {
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator ShowFinish(GameObject go)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new OpeningCinematicController.\u003CShowFinish\u003Ec__Iterator8D()
    {
      go = go,
      \u003C\u0024\u003Ego = go,
      \u003C\u003Ef__this = this
    };
  }

  public void Dispose()
  {
    foreach (OpeningTroopController componentsInChild in this.GetComponentsInChildren<OpeningTroopController>())
    {
      componentsInChild.ReleaseTroops();
      componentsInChild.enabled = false;
    }
    this.odc.ReleaseTroops();
    this.odc.enabled = false;
    this.StopAllCoroutines();
  }
}
