﻿// Decompiled with JetBrains decompiler
// Type: AllianceManagePage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceManagePage : MonoBehaviour
{
  public GameObject m_BlockListDlg;
  public GameObject m_HighlordPage;
  public GameObject m_DukePage;
  public GameObject m_BaronPage;
  public GameObject m_KnightPage;
  public GameObject m_VassalPage;
  public GameObject m_HighlordLeave;
  public GameObject m_HighlordDisban;
  public UIGrid m_HighlordGrid;

  public void ShowPage()
  {
    this.gameObject.SetActive(true);
    this.UpdatePage();
  }

  public void HidePage()
  {
    this.gameObject.SetActive(false);
  }

  public void UpdatePage()
  {
    this.m_HighlordPage.SetActive(false);
    this.m_DukePage.SetActive(false);
    this.m_BaronPage.SetActive(false);
    this.m_KnightPage.SetActive(false);
    this.m_VassalPage.SetActive(false);
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null)
      return;
    AllianceMemberData allianceMemberData = allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData == null)
      return;
    switch (allianceMemberData.title)
    {
      case 1:
        this.m_VassalPage.SetActive(true);
        break;
      case 2:
        this.m_KnightPage.SetActive(true);
        break;
      case 3:
        this.m_BaronPage.SetActive(true);
        break;
      case 4:
        this.m_DukePage.SetActive(true);
        break;
      case 6:
        this.m_HighlordPage.SetActive(true);
        this.m_HighlordLeave.SetActive(allianceData.memberCount > 1);
        this.m_HighlordDisban.SetActive(allianceData.memberCount <= 1);
        this.m_HighlordGrid.Reposition();
        break;
    }
  }
}
