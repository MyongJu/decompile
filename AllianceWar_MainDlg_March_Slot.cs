﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_March_Slot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class AllianceWar_MainDlg_March_Slot : AllianceWar_MainDlg_Slot
{
  protected AllianceWar_MainDlg_March_Slot.Data data = new AllianceWar_MainDlg_March_Slot.Data();
  [SerializeField]
  protected AllianceWar_MainDlg_March_Slot.Panel panel;

  public override void Setup(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null)
      return;
    this.data.marchId = marchId;
    this.SetLeftItems(marchData, DBManager.inst.DB_User.Get(marchData.ownerUid), marchData.ownerLocation);
    this.SetRightItems(marchData, DBManager.inst.DB_User.Get(marchData.targetUid), marchData.targetLocation);
    this.OnSecond(NetServerTime.inst.ServerTimestamp);
  }

  protected virtual void SetLeftItems(MarchData marchData, UserData userData, Coordinate location)
  {
    if (userData != null)
    {
      CustomIconLoader.Instance.requestCustomIcon(this.panel.leftIcon, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.leftIcon, userData.LordTitle, 27);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      this.panel.leftName.text = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
    }
    this.panel.leftLocation.text = string.Format("X:{0}, Y:{1}", (object) location.X, (object) location.Y);
    this.data.leftLocation = location;
  }

  protected virtual void SetRightItems(MarchData marchData, UserData userData, Coordinate location)
  {
    if (userData != null)
    {
      CustomIconLoader.Instance.requestCustomIcon(this.panel.rightIcon, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.rightIcon, userData.LordTitle, 27);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
      this.panel.rightName.text = allianceData == null ? userData.userName : string.Format("[{0}]{1}", (object) allianceData.allianceAcronym, (object) userData.userName);
    }
    this.panel.rightLocation.text = string.Format("X:{0}, Y:{1}", (object) location.X, (object) location.Y);
    this.data.rightLocation = location;
  }

  protected GameObject RegenerateTargetGameObject(string type, Transform root)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad(string.Format("{0}{1}", (object) "Prefab/Tiles/", (object) type), (System.Type) null)) as GameObject;
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
    {
      gameObject.name = type;
      Transform[] componentsInChildren = gameObject.GetComponentsInChildren<Transform>();
      for (int index = 0; componentsInChildren != null && index < componentsInChildren.Length; ++index)
        componentsInChildren[index].gameObject.layer = LayerMask.NameToLayer("SmallViewport");
      gameObject.transform.parent = root;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
    }
    return gameObject;
  }

  public void OnSecond(int timeStamp)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(this.data.marchId);
    if (marchData != null && marchData.IsExpedition)
    {
      this.panel.marchProgress.gameObject.SetActive(true);
      this.panel.marchTime.text = string.Format("{0} {1}", (object) Utils.XLAT("war_rally_marching"), (object) Utils.FormatTime1(marchData.endTime - timeStamp));
      this.panel.marchProgress.value = (float) (timeStamp - marchData.startTime) * marchData.timeDuration.oneOverTimeDuration;
    }
    else
    {
      this.panel.marchProgress.gameObject.SetActive(false);
      PlayerData.inst.allianceWarManager.LoadDatas();
    }
  }

  public void OnLeftLocationClick()
  {
    if (DBManager.inst.DB_March.Get(this.data.marchId) == null)
      return;
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this.data.leftLocation.K,
      x = this.data.leftLocation.X,
      y = this.data.leftLocation.Y
    });
  }

  public void OnRightLocationClick()
  {
    if (DBManager.inst.DB_March.Get(this.data.marchId) == null)
      return;
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = this.data.rightLocation.K,
      x = this.data.rightLocation.X,
      y = this.data.rightLocation.Y
    });
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Reset()
  {
    this.panel.leftIcon = this.transform.Find("LeftContainer/Portrait").gameObject.GetComponent<UITexture>();
    this.panel.leftIcon.mainTexture = (Texture) null;
    this.panel.leftName = this.transform.Find("LeftContainer/Name").gameObject.GetComponent<UILabel>();
    this.panel.leftLocation = this.transform.Find("LeftContainer/LocationText").gameObject.GetComponent<UILabel>();
    this.panel.rightIcon = this.transform.Find("RightContainer/Portrait").gameObject.GetComponent<UITexture>();
    this.panel.rightIcon.mainTexture = (Texture) null;
    this.panel.rightName = this.transform.Find("RightContainer/Name").gameObject.GetComponent<UILabel>();
    this.panel.rightLocation = this.transform.Find("RightContainer/LocationText").gameObject.GetComponent<UILabel>();
    this.panel.marchProgress = this.transform.Find("Background/MarchTimer").gameObject.GetComponent<UISlider>();
    this.panel.marchTime = this.transform.Find("Background/MarchTimer/Exp").gameObject.GetComponent<UILabel>();
    UIEventTrigger component1 = this.transform.Find("LeftContainer").gameObject.GetComponent<UIEventTrigger>();
    component1.onClick.Clear();
    component1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnLeftLocationClick)));
    UIEventTrigger component2 = this.transform.Find("RightContainer").gameObject.GetComponent<UIEventTrigger>();
    component2.onClick.Clear();
    component2.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnRightLocationClick)));
  }

  protected class Data
  {
    public long marchId;
    public Coordinate leftLocation;
    public Coordinate rightLocation;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture leftIcon;
    public UILabel leftName;
    public UILabel leftLocation;
    public UITexture rightIcon;
    public UILabel rightName;
    public UILabel rightLocation;
    public UISlider marchProgress;
    public UILabel marchTime;
  }
}
