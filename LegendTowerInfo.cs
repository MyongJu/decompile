﻿// Decompiled with JetBrains decompiler
// Type: LegendTowerInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

public class LegendTowerInfo : IComparable<LegendTowerInfo>
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public string type;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "req_hero_power")]
  public int reqHeroPower;
  [Config(Name = "req_id_1")]
  public int reqBuildingId1;
  [Config(Name = "req_value_1")]
  public int reqBuildingLevel1;
  [Config(Name = "req_id_2")]
  public int reqBuildingId2;
  [Config(Name = "req_value_2")]
  public int reqBUidlingLevel2;
  [Config(Name = "benefit_id_1")]
  public int benefitId1;
  [Config(Name = "benefit_value_1")]
  public float benefitValue1;
  [Config(Name = "benefit_id_2")]
  public int benefitId2;
  [Config(Name = "benefit_value_2")]
  public float benefitValue2;
  [Config(Name = "benefit_id_3")]
  public int benefitId3;
  [Config(Name = "benefit_value_3")]
  public float benefitValue3;

  public int CompareTo(LegendTowerInfo other)
  {
    return this.reqHeroPower.CompareTo(other.reqHeroPower);
  }
}
