﻿// Decompiled with JetBrains decompiler
// Type: HeroCardRotator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroCardRotator : MonoBehaviour
{
  public const string LUCKY_DRAW_PATH = "Prefab/UI/Common/HeroLuckyDraw";
  public const string CARD_PATH = "Prefab/UI/Common/ParliamentHeroCard";
  public GameObject m_CardFront;
  public GameObject m_CardBack;
  public Animator m_Animator;
  public ParticleSystem m_ParticleSystem;
  private GameObject m_HeroCard;
  private TweenScale m_TweenBack;
  private TweenScale m_TweenCard;
  private HeroRecruitPayloadData m_HeroPayload;
  private System.Action m_Callback;

  public void Play(HeroRecruitPayloadData heroPayload, System.Action callback)
  {
    this.Reset();
    this.m_Callback = callback;
    this.m_HeroPayload = heroPayload;
    this.Rebind();
  }

  public ParliamentHeroInfo GetHeroInfo()
  {
    return ConfigManager.inst.DB_ParliamentHero.Get(this.m_HeroPayload.hero_id);
  }

  private void Rebind()
  {
    if ((bool) ((UnityEngine.Object) this.m_Animator))
    {
      this.m_Animator.enabled = true;
      this.m_Animator.Rebind();
    }
    else
      this.OnAnimationDone();
  }

  public void Reset()
  {
    if ((bool) ((UnityEngine.Object) this.m_TweenBack))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TweenBack);
      this.m_TweenBack = (TweenScale) null;
    }
    if ((bool) ((UnityEngine.Object) this.m_TweenCard))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TweenCard);
      this.m_TweenCard = (TweenScale) null;
    }
    if ((bool) ((UnityEngine.Object) this.m_HeroCard))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_HeroCard);
      this.m_HeroCard = (GameObject) null;
    }
    this.m_CardBack.transform.localScale = Vector3.one;
    this.m_Callback = (System.Action) null;
  }

  private void PlayVFX()
  {
    if (!(bool) ((UnityEngine.Object) this.m_ParticleSystem))
      return;
    this.m_ParticleSystem.Clear();
    this.m_ParticleSystem.Play(true);
  }

  private void OnFlipBackFinished()
  {
    if (this.m_HeroPayload.type == "hero_to_chip")
    {
      this.m_HeroCard = this.CreateParliamentHeroCard();
      ParliamentHeroCard component = this.m_HeroCard.GetComponent<ParliamentHeroCard>();
      ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.m_HeroPayload.hero_id);
      component.SetFragmentData(parliamentHeroInfo, this.m_HeroPayload.num);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(parliamentHeroInfo.chipItem);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["0"] = parliamentHeroInfo.Name;
      para["1"] = itemStaticInfo.LocName;
      para["2"] = this.m_HeroPayload.num.ToString();
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_hero_summon_duplicate_description", para, true), (System.Action) null, 4f, false);
      if (parliamentHeroInfo != null && parliamentHeroInfo.quality == 3)
        this.PlayVFX();
    }
    else if (this.m_HeroPayload.type == "chip")
    {
      this.m_HeroCard = this.CreateParliamentHeroCard();
      ParliamentHeroCard component = this.m_HeroCard.GetComponent<ParliamentHeroCard>();
      ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.m_HeroPayload.hero_id);
      component.SetFragmentData(parliamentHeroInfo, this.m_HeroPayload.num);
      if (parliamentHeroInfo != null && parliamentHeroInfo.quality == 3)
        this.PlayVFX();
    }
    else if (this.m_HeroPayload.type == "hero")
    {
      this.m_HeroCard = this.CreateParliamentHeroCard();
      ParliamentHeroCard component = this.m_HeroCard.GetComponent<ParliamentHeroCard>();
      ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.m_HeroPayload.hero_id);
      component.SetData(parliamentHeroInfo);
      if (parliamentHeroInfo != null && parliamentHeroInfo.quality == 3)
        this.PlayVFX();
    }
    else if (this.m_HeroPayload.type == "item")
    {
      this.m_HeroCard = this.CreateHeroLuckyDraw();
      this.m_HeroCard.GetComponent<ParliamentHeroLuckyDraw>().SetData(ConfigManager.inst.DB_Items.GetItem(this.m_HeroPayload.item_id), this.m_HeroPayload.num);
    }
    this.m_TweenCard = UITweener.Begin<TweenScale>(this.m_HeroCard, 0.2f);
    this.m_TweenCard.from = new Vector3(0.0f, 1f, 1f);
    this.m_TweenCard.to = Vector3.one;
    this.m_TweenCard.eventReceiver = this.gameObject;
    this.m_TweenCard.callWhenFinished = "OnFlipFrontFinished";
  }

  private void OnFlipFrontFinished()
  {
    if ((bool) ((UnityEngine.Object) this.m_Animator))
      this.m_Animator.enabled = false;
    if (this.m_Callback == null)
      return;
    this.m_Callback();
  }

  private GameObject CreateHeroLuckyDraw()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/HeroLuckyDraw", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/HeroLuckyDraw", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.m_CardFront.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    return gameObject;
  }

  private GameObject CreateParliamentHeroCard()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/ParliamentHeroCard", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/ParliamentHeroCard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.m_CardFront.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    return gameObject;
  }

  private void OnAnimationDone()
  {
    this.m_TweenBack = UITweener.Begin<TweenScale>(this.m_CardBack, 0.2f);
    this.m_TweenBack.from = Vector3.one;
    this.m_TweenBack.to = new Vector3(0.0f, 1f, 1f);
    this.m_TweenBack.eventReceiver = this.gameObject;
    this.m_TweenBack.callWhenFinished = "OnFlipBackFinished";
  }
}
