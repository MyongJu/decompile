﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerEmbassy
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerEmbassy : BuildingControllerNew
{
  private bool m_Start;

  private void OnEnable()
  {
    if (!this.m_Start)
      return;
    this.OnHelpChanged();
  }

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.m_Start = true;
    this.OnHelpChanged();
    AllianceManager.Instance.onHelpChanged += new System.Action(this.OnHelpChanged);
  }

  public override void Dispose()
  {
    base.Dispose();
    AllianceManager.Instance.onHelpChanged -= new System.Action(this.OnHelpChanged);
  }

  private void OnHelpChanged()
  {
    CollectionUI[] componentsInChildren = this.GetComponentsInChildren<CollectionUI>();
    if (PlayerData.inst.allianceId > 0L)
    {
      if (PlayerData.inst.allianceData.helps.GetValidHelpCount() > 0)
      {
        if (componentsInChildren.Length != 0)
          return;
        CollectionUIManager.Instance.OpenCollectionUI(this.transform, new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_help", "id_train", new EventDelegate((EventDelegate.Callback) (() => AllianceManager.Instance.HelpAll((System.Action<bool, object>) ((_param0, _param1) =>
        {
          AudioManager.Instance.StopAndPlaySound("sfx_alliance_help_request");
          UIManager.inst.toast.Show(ScriptLocalization.Get("toast_help_success", true), (System.Action) null, 4f, false);
        })))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false), CollectionUI.CollectionType.CollectionRes);
      }
      else
      {
        foreach (CollectionUI collectionUi in componentsInChildren)
        {
          if (collectionUi.CT == CollectionUI.CollectionType.CollectionRes)
            collectionUi.Close();
        }
      }
    }
    else
    {
      foreach (CollectionUI collectionUi in componentsInChildren)
      {
        if (collectionUi.CT == CollectionUI.CollectionType.CollectionRes)
          collectionUi.Close();
      }
    }
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["buildingreinforce"]);
    CityCircleBtnPara cityCircleBtnPara1 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_alliance_help", "id_help", new EventDelegate((EventDelegate.Callback) (() =>
    {
      if (PlayerData.inst.allianceId > 0L)
        UIManager.inst.OpenDlg("Alliance/AllianceHelpRequestsDlg", (UI.Dialog.DialogParameter) new AllianceHelpRequestDlg.Parameter()
        {
          showBack = true
        }, true, true, true);
      else
        UIManager.inst.publicHUD.OnReinforceBtnPressed();
    })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara1);
    DB.WorldMapData worldMapData = DBManager.inst.DB_WorldMap.Get(PlayerData.inst.playerCityData.cityLocation.K);
    if (worldMapData == null || !(worldMapData.State != "unOpen"))
      return;
    CityCircleBtnPara cityCircleBtnPara2 = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_hall", "id_kings_chamber", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenDlg("HallOfKing/HallOfKingDlg", (UI.Dialog.DialogParameter) null, true, true, true))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
    ftl.Add(cityCircleBtnPara2);
  }
}
