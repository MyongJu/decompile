﻿// Decompiled with JetBrains decompiler
// Type: AllianceTradeRequestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class AllianceTradeRequestItem : MonoBehaviour
{
  private Coordinate _loc = new Coordinate();
  public UITexture mPlayerIcon;
  public UILabel mPlayerName;
  public UILabel mPowerAmount;
  public UISprite mNeedIcon;
  public GameObject mSendBtn;
  public GameObject mCancelBtn;
  public UILabel mTimeLeft;
  private double _secondsLeft;
  private long _jobID;
  private long _uid;

  public void SetDetails(long jobID, long UID, string playerName, string portrait, string customIconUrl, int lordTitleId, int power, CityManager.ResourceTypes resType, double timeLeft, Coordinate loc)
  {
    this._jobID = jobID;
    this._uid = UID;
    this._loc = loc;
    this._secondsLeft = timeLeft;
    CustomIconLoader.Instance.requestCustomIcon(this.mPlayerIcon, portrait, customIconUrl, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.mPlayerIcon, lordTitleId, 1);
    this.mPlayerName.text = playerName;
    this.mPowerAmount.text = Utils.FormatThousands(power.ToString());
    switch (resType)
    {
      case CityManager.ResourceTypes.FOOD:
        this.mNeedIcon.spriteName = "food_icon";
        break;
      case CityManager.ResourceTypes.SILVER:
        this.mNeedIcon.spriteName = "silver_icon";
        break;
      case CityManager.ResourceTypes.WOOD:
        this.mNeedIcon.spriteName = "wood_icon";
        break;
      case CityManager.ResourceTypes.ORE:
        this.mNeedIcon.spriteName = "ore_icon";
        break;
    }
    if (UID == PlayerData.inst.uid)
    {
      this.mSendBtn.SetActive(false);
      this.mCancelBtn.SetActive(true);
      this.mTimeLeft.gameObject.SetActive(true);
    }
    else
    {
      this.mSendBtn.SetActive(true);
      this.mCancelBtn.SetActive(false);
      this.mTimeLeft.gameObject.SetActive(false);
    }
  }

  public void OnSendBtnPressed()
  {
    int level = CityManager.inst.GetHighestBuildingLevelFor("marketplace");
    if (level < 1)
      level = 1;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData("marketplace", level);
    AllianceSendResources.Parameter parameter = new AllianceSendResources.Parameter();
    parameter.Uid = this._uid;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(0.0f, "calc_tradetax");
    parameter.TaxPercent = Math.Round((double) finalData, 2);
    parameter.MaxResourceLoad = (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data.Benefit_Value_1, "calc_trade_capacity");
    parameter.TargetLocation = this._loc;
    UIManager.inst.OpenPopup("AllianceSendTradePopup", (Popup.PopupParameter) parameter);
  }

  public void OnCancelBtnPressed()
  {
    MessageHub.inst.GetPortByAction("Alliance:cancelTradeHelp").SendRequest(Utils.Hash((object) "uid", (object) this._uid, (object) "job_id", (object) this._jobID), new System.Action<bool, object>(this.CancelCallback), true);
  }

  private void CancelCallback(bool ret, object data)
  {
    Utils.FindClass<AllianceTradeDlg>().RefreshRequests();
  }

  private void Update()
  {
    if (this._uid != PlayerData.inst.uid)
      return;
    if ((this._secondsLeft -= (double) Time.deltaTime) > 0.0)
      this.mTimeLeft.text = Utils.ConvertSecsToString(this._secondsLeft);
    else
      Utils.FindClass<AllianceTradeDlg>().RefreshRequests();
  }
}
