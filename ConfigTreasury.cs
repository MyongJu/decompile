﻿// Decompiled with JetBrains decompiler
// Type: ConfigTreasury
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigTreasury
{
  private List<TreasuryInfo> treasuryInfoList = new List<TreasuryInfo>();
  private Dictionary<string, TreasuryInfo> datas;
  private Dictionary<int, TreasuryInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<TreasuryInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, TreasuryInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.treasuryInfoList.Add(enumerator.Current);
    }
    this.treasuryInfoList.Sort(new Comparison<TreasuryInfo>(this.SortByID));
  }

  public List<TreasuryInfo> GetTreasuryInfoList()
  {
    return this.treasuryInfoList;
  }

  public TreasuryInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (TreasuryInfo) null;
  }

  public int SortByID(TreasuryInfo a, TreasuryInfo b)
  {
    return a.internalId.CompareTo(b.internalId);
  }
}
