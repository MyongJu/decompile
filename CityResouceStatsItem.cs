﻿// Decompiled with JetBrains decompiler
// Type: CityResouceStatsItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class CityResouceStatsItem : MonoBehaviour
{
  public UILabel label;
  public UILabel resName;
  public CityResouceStatsItem.Type type;

  private string GetBuildType()
  {
    string str = string.Empty;
    switch (this.type)
    {
      case CityResouceStatsItem.Type.Food:
        str = "farm";
        this.resName.text = ScriptLocalization.Get("city_stats_total_food_production", true);
        break;
      case CityResouceStatsItem.Type.Wood:
        str = "lumber_mill";
        this.resName.text = ScriptLocalization.Get("city_stats_total_wood_production", true);
        break;
      case CityResouceStatsItem.Type.Ore:
        str = "mine";
        this.resName.text = ScriptLocalization.Get("city_stats_total_iron_production", true);
        break;
      case CityResouceStatsItem.Type.Silver:
        str = "house";
        this.resName.text = ScriptLocalization.Get("city_stats_total_silver_production", true);
        break;
    }
    return str;
  }

  private void Start()
  {
    List<CityManager.BuildingItem> buildingsByType = CityManager.inst.GetBuildingsByType(this.GetBuildType());
    double num = 0.0;
    for (int index = 0; index < buildingsByType.Count; ++index)
    {
      ResouceGenSpeedData resouceGenSpeedData = new ResouceGenSpeedData();
      resouceGenSpeedData.SetData(buildingsByType[index]);
      num += resouceGenSpeedData.GenerateSpeedInHour;
    }
    this.label.text = num.ToString() + "/h";
  }

  private void Update()
  {
  }

  public enum Type
  {
    Food,
    Wood,
    Ore,
    Silver,
  }
}
