﻿// Decompiled with JetBrains decompiler
// Type: UnitRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class UnitRender : MonoBehaviour
{
  public GameObject highLight_normal;
  public GameObject highLight_locked;
  public GameObject normal;
  public GameObject locked;
  public UISprite unitTrainningSprite;
  public UISprite trapTrainningSprite;
  public UITexture portraitTexture;
  public UILabel nameLabel;
  public UILabel levelLabel;
  public System.Action<UnitRender> OnUnitRenderClick;
  private Unit_StatisticsInfo unit;
  private UISprite trainningIcon;
  public int InternalID;

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void UpdateUI(Unit_StatisticsInfo unit, long buildingID, System.Action<UnitRender> OnUnitRenderClick, UIScrollView scrollView)
  {
    this.unit = unit;
    this.OnUnitRenderClick = OnUnitRenderClick;
    this.unitTrainningSprite.gameObject.SetActive(false);
    this.trapTrainningSprite.gameObject.SetActive(false);
    this.trainningIcon = !(unit.FromBuildingType == "fortress") ? this.unitTrainningSprite : this.trapTrainningSprite;
    this.HideAllState();
    if (BarracksManager.Instance.IsUnitUnlocked(unit.internalId))
    {
      this.normal.SetActive(true);
      GreyUtility.Normal(this.locked);
      GreyUtility.Normal(this.portraitTexture.gameObject);
      Unit_StatisticsInfo currentTrainningUnit = BarracksManager.Instance.GetCurrentTrainningUnit(buildingID);
      if (currentTrainningUnit != null && currentTrainningUnit.internalId == unit.internalId)
        this.trainningIcon.gameObject.SetActive(true);
      else
        this.trainningIcon.gameObject.SetActive(false);
    }
    else
    {
      this.locked.SetActive(true);
      GreyUtility.Grey(this.locked);
      GreyUtility.Grey(this.portraitTexture.gameObject);
    }
    foreach (UILabel componentsInChild in this.GetComponentsInChildren<UILabel>(true))
      componentsInChild.text = ScriptLocalization.Get(unit.Troop_Name_LOC_ID, true);
    if (unit.FromBuildingType == "range" || unit.FromBuildingType == "stables" || unit.FromBuildingType == "barracks")
      Utils.SetPortrait(this.portraitTexture, "Texture/Unit/portrait_unit_" + unit.ID);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.portraitTexture, "Texture/Unit/portrait_unit_" + unit.ID, (System.Action<bool>) null, true, false, string.Empty);
    this.nameLabel.text = ScriptLocalization.Get(unit.Troop_Name_LOC_ID, true);
    this.levelLabel.text = Utils.GetRomaNumeralsBelowTen(unit.Troop_Tier);
    this.InternalID = unit.internalId;
    this.GetComponent<UIDragScrollView>().scrollView = scrollView;
  }

  private void HideAllState()
  {
    this.highLight_normal.SetActive(false);
    this.highLight_locked.SetActive(false);
    this.normal.SetActive(false);
    this.locked.SetActive(false);
  }

  public void HighLight()
  {
    this.HideAllState();
    if (BarracksManager.Instance.IsUnitUnlocked(this.unit.internalId))
      this.highLight_normal.SetActive(true);
    else
      this.highLight_locked.SetActive(true);
  }

  public void UnHighLight()
  {
    this.HideAllState();
    if (BarracksManager.Instance.IsUnitUnlocked(this.unit.internalId))
      this.normal.SetActive(true);
    else
      this.locked.SetActive(true);
  }

  public Unit_StatisticsInfo Unit
  {
    get
    {
      return this.unit;
    }
  }

  public void OnClick()
  {
    if (this.OnUnitRenderClick == null)
      return;
    this.OnUnitRenderClick(this);
  }
}
