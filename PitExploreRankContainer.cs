﻿// Decompiled with JetBrains decompiler
// Type: PitExploreRankContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PitExploreRankContainer : MonoBehaviour
{
  private Dictionary<int, PitExploreRankSlot> _itemDict = new Dictionary<int, PitExploreRankSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelRankTime;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;
  private List<PitExplorePayload.PitRankDataWithTime> _rankInfoList;

  public void SetData(List<PitExplorePayload.PitRankDataWithTime> rankInfoList)
  {
    this._rankInfoList = rankInfoList;
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this.UpdateUI();
  }

  public void ClearData()
  {
    using (Dictionary<int, PitExploreRankSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void UpdateUI()
  {
    this.ClearData();
    if (this._rankInfoList != null && this._rankInfoList.Count > 0)
    {
      this._labelRankTime.text = string.Format("{0} {1} - {2}", (object) Utils.FormatTimeYYYYMMDD((long) this._rankInfoList[0].startTime, "/"), (object) Utils.GetTimeHHMM(Utils.FormatTimeForSystemSetting((long) this._rankInfoList[0].startTime)), (object) Utils.GetTimeHHMM(Utils.FormatTimeForSystemSetting((long) this._rankInfoList[0].endTime)));
      for (int key = 0; key < this._rankInfoList.Count; ++key)
      {
        PitExploreRankSlot itemRenderer = this.CreateItemRenderer(key + 1, this._rankInfoList[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
  }

  private PitExploreRankSlot CreateItemRenderer(int rank, PitExplorePayload.PitRankDataWithTime pitRankDataWithTime)
  {
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    PitExploreRankSlot component = gameObject.GetComponent<PitExploreRankSlot>();
    component.SetData(rank, pitRankDataWithTime);
    return component;
  }
}
