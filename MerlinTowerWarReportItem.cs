﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarReportItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTowerWarReportItem : MonoBehaviour
{
  [SerializeField]
  private UITexture _textureItem;
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelItemDescription;
  [SerializeField]
  private UILabel _labelFightArea;
  private Hashtable _mailData;

  public void SetData(Hashtable mailData)
  {
    if (mailData != null && mailData.ContainsKey((object) "data"))
      this._mailData = mailData[(object) "data"] as Hashtable;
    this.UpdateUI();
  }

  public void OnGotoButtonClick()
  {
    if (this._mailData == null)
      return;
    int mineIndex = 0;
    int outData = 0;
    long version = 0;
    DatabaseTools.UpdateData(this._mailData, "mine_level", ref outData);
    DatabaseTools.UpdateData(this._mailData, "mine_index", ref mineIndex);
    DatabaseTools.UpdateData(this._mailData, "version", ref version);
    MerlinTowerPayload.Instance.SendGetUserInfoRequest((System.Action<bool, object>) ((result, data) =>
    {
      if (result && MerlinTowerPayload.Instance.UserData.IsInKingdom && MerlinTowerPayload.Instance.UserData.version == version)
        MerlinTowerFacade.RequestEnter(true, mineIndex);
      else
        UIManager.inst.toast.Show(Utils.XLAT("toast_tower_already_refreshed"), (System.Action) null, 4f, true);
    }), true);
  }

  private void UpdateUI()
  {
    if (this._mailData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1");
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      BuilderFactory.Instance.HandyBuild((UIWidget) this._textureItem, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
      if (this._mailData.ContainsKey((object) "crystal_number") && this._mailData[(object) "crystal_number"] != null)
      {
        long result = 0;
        long.TryParse(this._mailData[(object) "crystal_number"].ToString(), out result);
        this._labelItemName.text = string.Format("{0} [4EE934]+{1}[-]", (object) itemStaticInfo.LocName, (object) result.ToString());
      }
    }
    if (this._mailData.ContainsKey((object) "loss") && this._mailData[(object) "loss"] != null)
    {
      long result = 0;
      long.TryParse(this._mailData[(object) "loss"].ToString(), out result);
      this._labelItemDescription.text = ScriptLocalization.GetWithPara("mail_subject_tower_mine_seized_succes", new Dictionary<string, string>()
      {
        {
          "0",
          result.ToString()
        }
      }, true);
    }
    int result1 = 0;
    int result2 = 0;
    if (this._mailData.ContainsKey((object) "mine_level") && this._mailData[(object) "mine_level"] != null)
      int.TryParse(this._mailData[(object) "mine_level"].ToString(), out result1);
    if (this._mailData.ContainsKey((object) "mine_index") && this._mailData[(object) "mine_index"] != null)
      int.TryParse(this._mailData[(object) "mine_index"].ToString(), out result2);
    this._labelFightArea.text = ScriptLocalization.GetWithPara("tower_mine_seized_info_subtitle", new Dictionary<string, string>()
    {
      {
        "0",
        result1.ToString()
      },
      {
        "1",
        result2.ToString()
      }
    }, true);
  }
}
