﻿// Decompiled with JetBrains decompiler
// Type: SQWanSdkManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UnityEngine;

public class SQWanSdkManager
{
  private static readonly SQWanSdkManager _instance = new SQWanSdkManager();
  private const string CallbackTargetGO = "SQWanHelper";
  private const int MaxRetry = 5;
  private AndroidJavaClass unityPlayerClass;
  private AndroidJavaObject unityActivity;
  private AndroidJavaClass sqwFacade;
  private SQWanHelper _monoHelper;

  private SQWanSdkManager()
  {
    this.unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    this.unityActivity = this.unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
    this.sqwFacade = new AndroidJavaClass("com.funplus.kingofavalon.SQWanFacade");
    GameObject gameObject = new GameObject("SQWanHelper");
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    this._monoHelper = gameObject.AddComponent<SQWanHelper>();
    this.IsLogout = false;
    this.IsLoggedInGame = false;
  }

  public string Token { set; get; }

  public string GID { set; get; }

  public string PID { set; get; }

  public string Refer { set; get; }

  public string Cn37Uid { set; get; }

  public bool IsLogout { set; get; }

  public bool IsLoggedInGame { set; get; }

  public int AdultStatus { set; get; }

  public bool Inited { set; get; }

  public bool IsNewUser { set; get; }

  public int RoleCreateTime { set; get; }

  public string Help37Url { set; get; }

  public string Service37Url { set; get; }

  public string UpdateUrl { set; get; }

  public void Reset()
  {
    this.Clear37AccountCache();
  }

  public void RegisterPush()
  {
    ThirdPartyPushManager.Instance.SetAlias(this.Cn37Uid);
  }

  public void UnregisterPush()
  {
    ThirdPartyPushManager.Instance.UnsetAlias(this.Cn37Uid);
  }

  private void Clear37AccountCache()
  {
    this.Token = string.Empty;
    this.Cn37Uid = string.Empty;
    this.GID = string.Empty;
    this.PID = string.Empty;
    this.Refer = string.Empty;
    this.IsNewUser = false;
  }

  public bool Cn37AccountReady()
  {
    if (!string.IsNullOrEmpty(this.Token) && !string.IsNullOrEmpty(this.GID) && !string.IsNullOrEmpty(this.PID))
      return !string.IsNullOrEmpty(this.Refer);
    return false;
  }

  public static SQWanSdkManager Instance
  {
    get
    {
      return SQWanSdkManager._instance;
    }
  }

  public void RegisterListeners()
  {
    this.CallSetSwitchAccountListener();
    this.CallSetBackToGameLoginListener();
  }

  public void Logout()
  {
    this.IsLogout = true;
    RequestManager.inst.SendRequest("player:unSubTopics", (Hashtable) null, (System.Action<bool, object>) null, true);
    this.UnregisterPush();
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  public void ShowFAQs()
  {
    Application.OpenURL(this.Service37Url);
  }

  public void SetLoginSuccessHandler(System.Action onSuccess)
  {
    this._monoHelper.LoginSuccessHandler = onSuccess;
  }

  public void SetLoginFailureHandler(System.Action onFailure)
  {
    this._monoHelper.LoginFailureHandler = onFailure;
  }

  public void CallRegisterContext()
  {
    SQWanSdkManager.Log(nameof (CallRegisterContext));
    this.sqwFacade.CallStatic("registerContext", (object) this.unityActivity, (object) "SQWanHelper");
  }

  public void CallInit()
  {
    SQWanSdkManager.Log(nameof (CallInit));
    this.sqwFacade.CallStatic("init37sdk", (object) this.unityActivity, (object) "SQWanHelper");
  }

  public void CallSetSwitchAccountListener()
  {
    SQWanSdkManager.Log("SetSwitchAccountListener");
    this.sqwFacade.CallStatic("setSwitchAccountListener");
  }

  public void CallSetBackToGameLoginListener()
  {
    SQWanSdkManager.Log("SetBackToGameLoginListener");
    this.sqwFacade.CallStatic("setBackToGameLoginListener");
  }

  public void CallLogin()
  {
    SQWanSdkManager.Log("Login");
    this.sqwFacade.CallStatic("login");
  }

  public void CallLoginRetry(int retryTime, SQWanSdkManager.LoginRetry reason)
  {
    if (retryTime >= 5)
    {
      retryTime = 0;
      string title = ScriptLocalization.Get("network_error_title", true);
      string tip = ScriptLocalization.Get("network_error_login_fail_description", true);
      if (!NetWorkDetector.Instance.IsReadyRestart())
        NetWorkDetector.Instance.Send37LoginError2RUM(string.Format("[SQWanSdkManager]CallLoginRetry Reason {0}, retry times {1}", (object) reason, (object) retryTime));
      NetWorkDetector.Instance.RestartOrQuitGame(tip, title, (string) null);
    }
    else
      this.sqwFacade.CallStatic("login");
  }

  public void CallChangeAccount()
  {
    SQWanSdkManager.Log("ChangeAccount");
    this.sqwFacade.CallStatic("changeAccount");
  }

  public void CallPay(string doid, string dpt, string dcn, string dsid, string dsname, string dext, string drid, string drname, int drlevel, int dmoney, int dradio)
  {
    SQWanSdkManager.Log(string.Format("Pay doid:{0}, dpt:{1}, dcn:{2}, dsid:{3}, dsname:{4}, dext:{5}, drid:{6}, drname:{7}, drlevel:{8}, dmoney:{9}, dradio:{10}", (object) doid, (object) dpt, (object) dcn, (object) dsid, (object) dsname, (object) dext, (object) drid, (object) drname, (object) drlevel, (object) dmoney, (object) dradio));
    this.sqwFacade.CallStatic("pay", (object) doid, (object) dpt, (object) dcn, (object) dsid, (object) dsname, (object) dext, (object) drid, (object) drname, (object) drlevel, (object) dmoney, (object) dradio);
  }

  public void CallCreateRoleInfo()
  {
    SQWanSdkManager.Log(nameof (CallCreateRoleInfo));
    string str1 = PlayerData.inst.userData.world_id.ToString();
    string str2 = string.Format("王国: {0}", (object) PlayerData.inst.userData.world_id);
    string str3 = PlayerData.inst.uid.ToString();
    string userName = PlayerData.inst.userData.userName;
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    string str4 = (heroData != null ? heroData.level : 0).ToString();
    string str5 = PlayerData.inst.userData.currency.gold.ToString();
    AllianceData allianceData = PlayerData.inst.allianceData;
    string str6 = allianceData == null ? string.Empty : allianceData.allianceName;
    string str7 = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) PlayerData.inst.userData.vipPoint).Level.ToString();
    string str8 = this.RoleCreateTime.ToString();
    string str9 = "-1";
    SQWanSdkManager.Log(string.Format("CreateRoleInfo detail, serverId:{0}, serverName:{1}, roleId:{2}, roleName:{3}, roleLevel:{4}, gold:{5}, allianceName:{6}, vipLevel:{7}, roleCTime:{8}, roleLevelMTime:{9}", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9));
    this.sqwFacade.CallStatic("createRoleInfo", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9);
  }

  public void CallSubmitRoleInfo()
  {
    SQWanSdkManager.Log(nameof (CallSubmitRoleInfo));
    string str1 = PlayerData.inst.userData.world_id.ToString();
    string str2 = string.Format("王国: {0}", (object) PlayerData.inst.userData.world_id);
    string str3 = PlayerData.inst.uid.ToString();
    string userName = PlayerData.inst.userData.userName;
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    string str4 = (heroData != null ? heroData.level : 0).ToString();
    string str5 = PlayerData.inst.userData.currency.gold.ToString();
    AllianceData allianceData = PlayerData.inst.allianceData;
    string str6 = allianceData == null ? string.Empty : allianceData.allianceName;
    string str7 = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) PlayerData.inst.userData.vipPoint).Level.ToString();
    string str8 = this.RoleCreateTime.ToString();
    string str9 = "-1";
    SQWanSdkManager.Log(string.Format("SubmitRoleInfo detail, serverId:{0}, serverName:{1}, roleId:{2}, roleName:{3}, roleLevel:{4}, gold:{5}, allianceName:{6}, vipLevel:{7}, roleCTime:{8}, roleLevelMTime:{9}", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9));
    this.sqwFacade.CallStatic("submitRoleInfo", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9);
  }

  public void CallUpgradeRoleInfo()
  {
    SQWanSdkManager.Log(nameof (CallUpgradeRoleInfo));
    string str1 = PlayerData.inst.userData.world_id.ToString();
    string str2 = string.Format("王国: {0}", (object) PlayerData.inst.userData.world_id);
    string str3 = PlayerData.inst.uid.ToString();
    string userName = PlayerData.inst.userData.userName;
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(PlayerData.inst.uid);
    string str4 = (heroData != null ? heroData.level : 0).ToString();
    string str5 = PlayerData.inst.userData.currency.gold.ToString();
    AllianceData allianceData = PlayerData.inst.allianceData;
    string str6 = allianceData == null ? string.Empty : allianceData.allianceName;
    string str7 = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) PlayerData.inst.userData.vipPoint).Level.ToString();
    string str8 = this.RoleCreateTime.ToString();
    string str9 = NetServerTime.inst.ServerTimestamp.ToString();
    SQWanSdkManager.Log(string.Format("UpgradeRoleInfo detail, serverId:{0}, serverName:{1}, roleId:{2}, roleName:{3}, roleLevel:{4}, gold:{5}, allianceName:{6}, vipLevel:{7}, roleCTime:{8}, roleLevelMTime:{9}", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9));
    this.sqwFacade.CallStatic("upgradeRoleInfo", (object) str1, (object) str2, (object) str3, (object) userName, (object) str4, (object) str5, (object) str6, (object) str7, (object) str8, (object) str9);
  }

  public void CallShowExitDialog()
  {
    SQWanSdkManager.Log("ShowExitDialog");
    this.sqwFacade.CallStatic("showExitDialog");
  }

  public void CallSubmitStatisticInfo(string key, string value)
  {
    SQWanSdkManager.Log("SubmitStatisticInfo");
    this.sqwFacade.CallStatic("submitStatisticInfo", (object) key, (object) value);
  }

  public void CallGetAppConfig()
  {
    SQWanSdkManager.Log("GetAppConfig");
    this.sqwFacade.CallStatic("getAppConfig");
  }

  public void CallShowSQPersonalDialog()
  {
    SQWanSdkManager.Log("ShowSQPersonalDialog");
    this.sqwFacade.CallStatic("showSQPersonalDialog");
  }

  public static void Log(string msg)
  {
    Debug.Log((object) string.Format("37WanSDK: {0}", (object) msg));
  }

  public enum LoginRetry
  {
    Unknow,
    EmptyToken,
    LoginFail,
  }
}
