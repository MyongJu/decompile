﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreHonorItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class AllianceStoreHonorItem : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemPrice;
  public UILabel m_ItemName;
  public UILabel m_ItemQuantity;
  public Transform m_border;
  public UITexture m_backgroud;
  public int _itemId;
  private AllianceStoreData m_StoreData;
  private AllianceShopInfo m_ShopInfo;

  public void SetData(AllianceStoreData allianceStoreData)
  {
    this.m_StoreData = allianceStoreData;
    this.m_ShopInfo = ConfigManager.inst.DB_AllianceShop.GetByInternalId(allianceStoreData.dataKey.itemId);
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ShopInfo.ImageIconPath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemName(this.m_ItemName, this.m_ShopInfo.ItemId);
    Utils.SetItemBackground(this.m_backgroud, this.m_ShopInfo.ItemId);
    this.m_ItemPrice.text = Utils.FormatThousands(this.m_ShopInfo.HonorPrice.ToString());
    this.m_ItemPrice.color = !this.CanBuy() ? Color.red : new Color(0.9568627f, 0.7921569f, 0.003921569f);
    this.m_ItemQuantity.text = Utils.FormatThousands(this.m_StoreData.quantity.ToString());
    this._itemId = this.m_ShopInfo.ItemId;
  }

  public void OnClickHandler()
  {
    Utils.ShowItemTip(this._itemId, this.m_border, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnPressHandler()
  {
    Utils.DelayShowTip(this._itemId, this.m_border, 0L, 0L, 0);
  }

  public void OnItemClick()
  {
    if (this.CanBuy())
      UIManager.inst.OpenPopup("AllianceHonorBuyDialog", (Popup.PopupParameter) new AllianceHonorBuyDialog.Parameter()
      {
        allianceShopInfo = this.m_ShopInfo,
        allianceStoreData = this.m_StoreData
      });
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_honor"), (System.Action) null, 4f, false);
  }

  private bool CanBuy()
  {
    return (long) this.m_ShopInfo.HonorPrice <= PlayerData.inst.userData.currency.honor;
  }
}
