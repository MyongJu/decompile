﻿// Decompiled with JetBrains decompiler
// Type: RendererSortingOrderModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class RendererSortingOrderModifier : MonoBehaviour
{
  private int m_LastSortingOrder = int.MinValue;
  [SerializeField]
  private int m_SortingOrder;
  private Renderer[] m_Renderers;

  private void Start()
  {
    this.m_Renderers = this.GetComponentsInChildren<Renderer>();
  }

  private void Update()
  {
    if (this.m_LastSortingOrder == this.m_SortingOrder)
      return;
    this.m_LastSortingOrder = this.m_SortingOrder;
    int length = this.m_Renderers.Length;
    for (int index = 0; index < length; ++index)
      this.m_Renderers[index].sortingOrder = this.m_SortingOrder;
  }

  public int SortingOrder
  {
    get
    {
      return this.m_SortingOrder;
    }
    set
    {
      this.m_SortingOrder = value;
    }
  }
}
