﻿// Decompiled with JetBrains decompiler
// Type: ItemSlot_New
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class ItemSlot_New : MonoBehaviour
{
  [SerializeField]
  public ItemSlot_New.ItemSlotInfo info;
  [SerializeField]
  public ItemSlot_New.ItemSlotFunc func;
  private InventoryItemData m_Item;
  private bool _isChecked;
  private bool _isNew;
  private bool _isHighlight;
  private Color _highLightColor;
  private bool _isEnable;
  private bool _isEquipped;
  private bool _isLocked;

  public event System.Action<long> onSlotClickEvent;

  public InventoryItemData item
  {
    get
    {
      return this.m_Item;
    }
  }

  public long itemId
  {
    get
    {
      if (this.m_Item != null)
        return this.m_Item.itemId;
      return 0;
    }
  }

  public bool isEmpty
  {
    get
    {
      return this.itemId == 0L;
    }
  }

  public bool isChecked
  {
    get
    {
      return this._isChecked;
    }
    set
    {
      if ((UnityEngine.Object) this.info.checkImage != (UnityEngine.Object) null)
        this.info.checkImage.enabled = value;
      this._isChecked = value;
    }
  }

  public bool isNew
  {
    get
    {
      return this._isNew;
    }
    set
    {
      if ((UnityEngine.Object) this.info.newImage != (UnityEngine.Object) null)
        this.info.newImage.enabled = value;
      this._isNew = value;
    }
  }

  public bool isHighlight
  {
    get
    {
      return this._isHighlight;
    }
    set
    {
      if ((UnityEngine.Object) this.info.highlightImage != (UnityEngine.Object) null)
        this.info.highlightImage.gameObject.SetActive(value);
      this._isHighlight = value;
    }
  }

  public Color highlightColor
  {
    get
    {
      return this._highLightColor;
    }
    set
    {
      if (!(value != this._highLightColor))
        return;
      this._highLightColor = value;
      this.info.highlightImage.color = this._highLightColor;
    }
  }

  public bool isEnable
  {
    get
    {
      return this._isEnable;
    }
    set
    {
      if ((UnityEngine.Object) this.func.frame != (UnityEngine.Object) null)
        this.func.frame.enabled = value;
      this._isEnable = value;
    }
  }

  public bool isEquipped
  {
    get
    {
      return this._isEquipped;
    }
    set
    {
      if ((UnityEngine.Object) this.info.equipImage != (UnityEngine.Object) null)
        this.info.equipImage.enabled = value;
      this._isEquipped = value;
    }
  }

  public bool isLocked
  {
    get
    {
      return this._isLocked;
    }
    set
    {
      if ((UnityEngine.Object) this.info.lockImage != (UnityEngine.Object) null)
        this.info.lockImage.gameObject.SetActive(value);
      this._isLocked = value;
    }
  }

  public void OnSlotClick()
  {
    if (this.onSlotClickEvent == null)
      return;
    this.onSlotClickEvent(this.m_Item.itemId);
  }

  public void Setup(long inItemId)
  {
    this.Setup(DBManager.inst.GetInventory(BagType.Hero).GetMyItemByItemID((long) (int) inItemId));
  }

  private void UpdateUI()
  {
  }

  public void Setup(InventoryItemData itemData)
  {
    this.isEnable = true;
    this.isChecked = false;
    this.isHighlight = false;
    this.isLocked = false;
    if (this.m_Item != null)
      this.m_Item.OnChanged -= new System.Action(this.OnItemDataChanged);
    this.m_Item = itemData;
    if (this.m_Item != null)
      this.m_Item.OnChanged += new System.Action(this.OnItemDataChanged);
    this.UpdateUI();
  }

  private void OnItemDataChanged()
  {
    this.UpdateUI();
  }

  private void OnDisable()
  {
    if (this.m_Item == null)
      return;
    this.m_Item.OnChanged -= new System.Action(this.OnItemDataChanged);
  }

  private void SetLevel(string level)
  {
    if (!((UnityEngine.Object) this.info.levelText != (UnityEngine.Object) null))
      return;
    this.info.levelText.text = level;
  }

  private void SetGemsContainer(bool active)
  {
    if (!((UnityEngine.Object) this.info.gemsContainer != (UnityEngine.Object) null))
      return;
    this.info.gemsContainer.SetActive(active);
  }

  private void SetIcon(UIAtlas atlas, string spriteName)
  {
    if (!((UnityEngine.Object) this.info.icon != (UnityEngine.Object) null))
      return;
    this.info.icon.atlas = atlas;
    this.info.icon.spriteName = spriteName;
  }

  [Serializable]
  public class ItemSlotInfo
  {
    public UISprite icon;
    public UISprite lockImage;
    public UISprite equipImage;
    public UISprite checkImage;
    public UISprite highlightImage;
    public UISprite qualityImage;
    public UILabel levelText;
    public UISprite newImage;
    public GameObject gemsContainer;
    public UISprite[] gemIcons;
  }

  [Serializable]
  public class ItemSlotFunc
  {
    public UIButton frame;
    public UIDragScrollView dragScrollView;
  }
}
