﻿// Decompiled with JetBrains decompiler
// Type: DebugMenu2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UnityEngine;

public class DebugMenu2 : MonoBehaviour
{
  private string[] labels = new string[8];
  private string[] strs = new string[8];
  private float lastClickTime;
  private bool _isDisplay;
  private string services;
  private string command;
  private int paramCount;

  private void Start()
  {
  }

  private void Update()
  {
    if ((double) Time.time - (double) this.lastClickTime <= 0.200000002980232 || !Input.GetKeyUp(KeyCode.F2))
      return;
    this._isDisplay = !this._isDisplay;
    this.GetComponent<BoxCollider>().enabled = this._isDisplay;
  }

  public void Callback(Hashtable data, Hashtable param)
  {
    DBManager.inst.UpdateDatas((object) data, 0L);
    DBManager.inst.DB_Rally.Update(data[(object) nameof (data)], 0L);
  }

  protected enum DisplayType
  {
    NONE,
    StartRally,
    JoinRally,
    CancleRally,
    UnlockRally,
  }
}
