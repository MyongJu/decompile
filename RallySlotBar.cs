﻿// Decompiled with JetBrains decompiler
// Type: RallySlotBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class RallySlotBar : MonoBehaviour
{
  private RallySlotBar.Data data = new RallySlotBar.Data();
  private List<RallySlotBarDetail> _details = new List<RallySlotBarDetail>();
  public System.Action onUnlockSlotClick;
  public System.Action onJoinSlotClick;
  public System.Action<long> onSpeedUpClick;
  [SerializeField]
  private RallySlotBar.Panel panel;
  public RallySlotInfo.RallyState _state;
  private bool _detailExpended;
  private bool _onSecondFlag;

  public RallySlotInfo.RallyState state
  {
    get
    {
      return this._state;
    }
    set
    {
      this.panel.MarchInfoContainer.gameObject.SetActive(false);
      this.panel.stateLockedIcon.gameObject.SetActive(false);
      this.panel.stateLabel.gameObject.SetActive(false);
      this.panel.slotIndex.gameObject.SetActive(false);
      this.panel.State_Rallied.gameObject.SetActive(false);
      this.panel.BT_TapToJoin.gameObject.SetActive(false);
      this.panel.BT_UnlockSlot.gameObject.SetActive(false);
      switch (value)
      {
        case RallySlotInfo.RallyState.locked:
          this.panel.BT_UnlockSlot.gameObject.SetActive(true);
          this.panel.stateLockedIcon.gameObject.SetActive(true);
          this.panel.stateLabel.gameObject.SetActive(true);
          this.panel.stateLabel.text = "LOCKED WAR SLOT";
          break;
        case RallySlotInfo.RallyState.unlockedForUser:
          this.panel.BT_TapToJoin.gameObject.SetActive(true);
          this.panel.stateLabel.gameObject.SetActive(true);
          this.panel.slotIndex.gameObject.SetActive(true);
          break;
        case RallySlotInfo.RallyState.empty:
          this.panel.BT_TapToJoin.gameObject.SetActive(true);
          this.panel.slotIndex.gameObject.SetActive(true);
          this.panel.stateLabel.gameObject.SetActive(true);
          this.panel.stateLabel.text = "SEND TROOPS";
          break;
        case RallySlotInfo.RallyState.joined:
          this.panel.MarchInfoContainer.gameObject.SetActive(true);
          this.panel.slotIndex.gameObject.SetActive(true);
          break;
      }
      this.detailExpended = false;
      this._state = value;
    }
  }

  public bool detailExpended
  {
    get
    {
      return this._detailExpended;
    }
    set
    {
      if (value == this._detailExpended)
        return;
      this._detailExpended = value;
      if (this._detailExpended)
      {
        this.panel.TweenScale_ExpendDetals.PlayForward();
        this.panel.TweenRotate_ExpendButton.PlayForward();
      }
      else
      {
        this.panel.TweenScale_ExpendDetals.PlayReverse();
        this.panel.TweenRotate_ExpendButton.PlayReverse();
      }
    }
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  public void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public void Setup(int slotIndex, RallySlotInfo slotInfo, long rallyId)
  {
    RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
    this.data.slotIndex = slotIndex;
    this.data.uid = slotInfo.uid;
    this.data._marchId = slotInfo.marchId;
    this.state = slotInfo.state;
    if (slotIndex != 2)
      ;
    this.panel.slotIndex.text = slotIndex.ToString();
    switch (this.state)
    {
      case RallySlotInfo.RallyState.locked:
        this.panel.BT_UnlockSlot.isEnabled = rallyData.state == RallyData.RallyState.waitRallying;
        break;
      case RallySlotInfo.RallyState.unlockedForUser:
        this.panel.BT_TapToJoin.isEnabled = slotInfo.uid == PlayerData.inst.uid;
        UserData userData1 = DBManager.inst.DB_User.Get(slotInfo.uid);
        this.panel.stateLabel.text = userData1 == null ? string.Format("Reserved by UID :{0}", (object) slotInfo.uid) : string.Format("Reserved by {0}", (object) userData1.userName);
        break;
      case RallySlotInfo.RallyState.empty:
        this.panel.BT_TapToJoin.isEnabled = !rallyData.isJoined && rallyData.state == RallyData.RallyState.waitRallying;
        break;
      case RallySlotInfo.RallyState.joined:
        UserData userData2 = DBManager.inst.DB_User.Get(slotInfo.uid);
        if (userData2 != null)
          this.panel.playerName.text = userData2.userName;
        this.data.marchData = DBManager.inst.DB_March.Get(this.data._marchId);
        if (this.data.marchData != null)
        {
          if (this.data.marchData.state == MarchData.MarchState.marching && !this._onSecondFlag)
          {
            this._onSecondFlag = true;
          }
          else
          {
            this.panel.State_Rallied.gameObject.SetActive(true);
            this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(false);
          }
          if (this.data.marchData.troopsInfo.troops != null)
          {
            if (this._details.Count < this.data.marchData.troopsInfo.troops.Count)
            {
              for (int index = this._details.Count + 1; index <= this.data.marchData.troopsInfo.troops.Count; ++index)
              {
                GameObject gameObject = NGUITools.AddChild(this.panel.DetailContainer.gameObject, this.panel.DetailBarOrg);
                gameObject.transform.localPosition = new Vector3(0.0f, (float) (-index * this.panel.DetailBarOrgWidget.height), 0.0f);
                gameObject.name = string.Format("{0}", (object) (char) (97 + index));
                this._details.Add(gameObject.GetComponent<RallySlotBarDetail>());
              }
            }
            int index1 = 0;
            Dictionary<string, Unit>.ValueCollection.Enumerator enumerator = this.data.marchData.troopsInfo.troops.Values.GetEnumerator();
            while (enumerator.MoveNext())
            {
              this._details[index1].Setup(enumerator.Current);
              this._details[index1].gameObject.SetActive(true);
              ++index1;
            }
            for (int index2 = index1; index2 < this._details.Count; ++index2)
              this._details[index1].gameObject.SetActive(false);
            this.panel.DetailContainer.height = this.panel.DetailsTitle.height + index1 * 200 + 60;
            this.panel.DetailContainer.transform.localScale = new Vector3(1f, 0.0f, 1f);
            this.panel.troopCount.text = Utils.ConvertNumberToNormalString(this.data.marchData.troopsInfo.totalCount);
            break;
          }
          break;
        }
        this.panel.State_Rallied.gameObject.SetActive(true);
        this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(false);
        break;
    }
    this.detailExpended = false;
  }

  public void OnSecond(int serverTime)
  {
    if (this.data.marchData == null)
      return;
    if (this.data.marchData.state == MarchData.MarchState.marching)
    {
      this.panel.State_Rallied.gameObject.SetActive(false);
      this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(false);
      if (this.data.marchData.endTime - serverTime > 0)
      {
        this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(true);
        this.panel.troopLeftTimeSlider.value = (float) (serverTime - this.data.marchData.startTime) / (float) this.data.marchData.timeDuration.duration;
        this.panel.troopLeftTimeLabel.text = Utils.FormatTime(this.data.marchData.endTime - serverTime <= 0 ? 0 : this.data.marchData.endTime - serverTime, false, false, true);
      }
      else
      {
        this.panel.State_Rallied.gameObject.SetActive(true);
        this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(false);
        if (!this._onSecondFlag)
          return;
        this._onSecondFlag = false;
      }
    }
    else
    {
      if (this.data.marchData.state != MarchData.MarchState.rallying)
        return;
      this.panel.State_Rallied.gameObject.SetActive(true);
      this.panel.troopLeftTimeSliderContainer.gameObject.SetActive(false);
      if (!this._onSecondFlag)
        return;
      this._onSecondFlag = false;
    }
  }

  public void OnExpendClick()
  {
    if (this.state != RallySlotInfo.RallyState.joined)
      return;
    this.detailExpended = !this.detailExpended;
  }

  public void OnTapToJoinClick()
  {
    if (this.onJoinSlotClick == null)
      return;
    this.onJoinSlotClick();
  }

  public void OnUnlockSlotClick()
  {
    if (this.onUnlockSlotClick == null)
      return;
    this.onUnlockSlotClick();
  }

  public void OnTroopSpeedUpClick()
  {
    if (this.onSpeedUpClick == null)
      return;
    this.onSpeedUpClick(this.data._marchId);
  }

  protected struct Data
  {
    public int slotIndex;
    public long uid;
    public long _marchId;
    public bool joinedRally;
    public MarchData marchData;
  }

  [Serializable]
  protected class Panel
  {
    public const string STATE_LOCKED_TEXT = "LOCKED WAR SLOT";
    public const string STATE_UNLOCKED_TEXT = "SEND TROOPS";
    public UISprite slotLocked;
    public Transform MarchInfoContainer;
    public UILabel slotIndex;
    public UISprite playerIcon;
    public UILabel playerName;
    public UILabel troopCount;
    public Transform troopLeftTimeSliderContainer;
    public UISlider troopLeftTimeSlider;
    public UILabel troopLeftTimeLabel;
    public UIButton BT_troopsSpeedUp;
    public UILabel stateLabel;
    public UISprite stateLockedIcon;
    public UILabel State_Rallied;
    public UIButton BT_TapToJoin;
    public UIButton BT_UnlockSlot;
    public UIButton BT_SlotExpend;
    public TweenRotation TweenRotate_ExpendButton;
    public TweenScale TweenScale_ExpendDetals;
    public UIWidget DetailContainer;
    public UIWidget DetailsTitle;
    public GameObject DetailBarOrg;
    public UIWidget DetailBarOrgWidget;
  }
}
