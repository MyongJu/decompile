﻿// Decompiled with JetBrains decompiler
// Type: ArmyFormationData_Ranged
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using March;
using UnityEngine;

public class ArmyFormationData_Ranged : ArmyFormationData
{
  public override MarchSystem.MissileType missileType
  {
    get
    {
      return MarchSystem.MissileType.arrow_system;
    }
  }

  protected override MarchConfig.MarchInfo configInfo
  {
    get
    {
      return (MarchConfig.MarchInfo) MarchConfig.rangeInfo;
    }
  }

  public override Vector3[] CalcAttackFormation(Vector3 deltaPos, float radius, Vector3 inDir)
  {
    this.centerDeltaPos = deltaPos;
    Vector3[] pos = new Vector3[this.curUnitCount];
    Vector3 vector3_1 = inDir;
    vector3_1.z = 0.0f;
    vector3_1 = vector3_1.normalized;
    Vector3 vector3_2 = new Vector3();
    vector3_2.x = vector3_1.y;
    vector3_2.y = -vector3_1.x;
    float num = this.configInfo.formation_action[0].x / (float) this.curUnitFormation_y;
    int index1 = 0;
    Vector3 zero = Vector3.zero;
    for (int index2 = 0; index2 < this.curUnitFormation_x; ++index2)
    {
      for (int index3 = -this.curUnitFormation_y + 1; index3 < this.curUnitFormation_y && index1 < pos.Length; ++index1)
      {
        pos[index1] = (float) -index2 * vector3_1 * 0.4f * this.configInfo.formation_action[0].y + (float) index3 * num * vector3_2 - this.configInfo.formation_action[0].z * vector3_1;
        index3 += 2;
      }
    }
    this.RecalcAttackFormation(ref pos, deltaPos, radius);
    return pos;
  }
}
