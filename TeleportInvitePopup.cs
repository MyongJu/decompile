﻿// Decompiled with JetBrains decompiler
// Type: TeleportInvitePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;

public class TeleportInvitePopup : BaseReportPopup
{
  private TeleportInvitePopup.TIData tid;
  public Icon ai;
  public UILabel content;

  private void Init()
  {
    this.ai.FeedData((IComponentData) new IconData(UserData.GetPortraitIconPath(this.tid.portrait), this.tid.icon, this.tid.lord_title, new string[1]
    {
      this.tid.name
    }));
    this.content.text = this.param.mail.GetBodyString();
  }

  public void OnMailTo()
  {
    UIManager.inst.OpenDlg("Mail/MailComposeDlg", (UI.Dialog.DialogParameter) new MailComposeDlg.Parameter()
    {
      recipents = new List<string>(1) { this.tid.name }
    }, 1 != 0, 1 != 0, 1 != 0);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public void OnTeleportClicked()
  {
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_2300150", true), (System.Action) null, 4f, false);
    }
    else
    {
      AllianceData allianceData = PlayerData.inst.allianceData;
      if (allianceData != null)
      {
        Hashtable postData = new Hashtable();
        postData[(object) "target_uid"] = (object) allianceData.creatorId;
        MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          if (DBManager.inst.DB_City.GetByUid(allianceData.creatorId).cityLocation.K == PlayerData.inst.playerCityData.cityLocation.K)
            TeleportInvitePopup.TakeAction();
          else
            UIManager.inst.toast.Show(Utils.XLAT("toast_excalibur_war_tele_leader_not_in_kingdom"), (System.Action) null, 4f, false);
        }), true);
      }
      else
        TeleportInvitePopup.TakeAction();
    }
  }

  private static void TakeAction()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    if (TeleportHighlight.HasAllianceTeleport())
      TeleportManager.Instance.OnInviteTeleport(TeleportMode.ALLIANCE_TELEPORT);
    else
      TeleportManager.Instance.OnInviteTeleport(TeleportMode.ADVANCE_TELEPORT);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.tid = JsonReader.Deserialize<TeleportInvitePopup.TIData>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class TIData
  {
    public int k;
    public int x;
    public int y;
    public int portrait;
    public string icon;
    public int lord_title;
    public string name;
  }
}
