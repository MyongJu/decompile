﻿// Decompiled with JetBrains decompiler
// Type: VIPLosePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class VIPLosePopup : Popup
{
  public UIGrid entriesGrid;
  public VIPEntryRender templateBright;
  public UIButton notNowBtn;
  public UIButton activeBtn;
  public UILabel levelLabe;
  private VIP_Level_Info levelInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.AddEvtListener();
    this.levelInfo = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) DBManager.inst.DB_User.Get(PlayerData.inst.uid).vipPoint);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.RemoveEvtListener();
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClose()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void AddEvtListener()
  {
    this.notNowBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnNotNowClicked)));
    this.activeBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnActiveBtnClicked)));
  }

  private void RemoveEvtListener()
  {
    this.notNowBtn.onClick.Clear();
    this.activeBtn.onClick.Clear();
  }

  public void UpdateUI()
  {
    this.levelLabe.text = this.levelInfo.Level.ToString();
    for (int index = 0; index < this.levelInfo.Effects.Count; ++index)
    {
      Effect effect1 = this.levelInfo.Effects[index];
      if ((double) Mathf.Abs(effect1.Modifier) > 0.0)
      {
        Effect effect2 = this.levelInfo.Effects[index];
        if ((double) Mathf.Abs(effect1.Modifier) > 0.0)
        {
          GameObject gameObject = NGUITools.AddChild(this.entriesGrid.gameObject, this.templateBright.gameObject);
          gameObject.SetActive(true);
          gameObject.GetComponent<VIPEntryRender>().Init(effect2, true);
        }
      }
    }
    this.entriesGrid.repositionNow = true;
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.entriesGrid);
  }

  private void OnNotNowClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnActiveBtnClicked()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }
}
