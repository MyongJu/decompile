﻿// Decompiled with JetBrains decompiler
// Type: ActivityFestivalShopSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ActivityFestivalShopSlot : MonoBehaviour
{
  private int activityId = -1;
  private int slotItemId = -1;
  private int newSlotItemId = -1;
  private int index;
  private bool initFlag;
  private int itemCostCount;
  public System.Action<int> onBuyClick;
  public int currentCount;
  private int _itemId;
  [SerializeField]
  private ActivityFestivalShopSlot.Panel panel;

  public void Setup(int inIndex, int inActivityId, int inSlotItemId, bool needRevert = false)
  {
    this.index = inIndex;
    if (this.slotItemId == -1 || this.activityId == -1)
    {
      this.activityId = inActivityId;
      this.initFlag = true;
      this.SetupImd(inSlotItemId);
    }
    else
    {
      if (this.slotItemId == inSlotItemId && this.activityId == inActivityId && !needRevert)
        return;
      this.activityId = inActivityId;
      this.newSlotItemId = inSlotItemId;
      EventDelegate.Add(this.panel.tweenScale.onFinished, new EventDelegate.Callback(this.OnTweenEnd), true);
      this.panel.tweenScale.PlayForward();
      this.panel.rarity_rare.gameObject.SetActive(false);
      this.panel.rarity_special.gameObject.SetActive(false);
    }
  }

  public void OnIconClick()
  {
    Utils.ShowItemTip(this._itemId, this.panel.itemBg.transform, 0L, 0L, 0);
  }

  public void OnIconPress()
  {
    Utils.DelayShowTip(this._itemId, this.panel.itemBg.transform, 0L, 0L, 0);
  }

  public void OnIconRelease()
  {
    Utils.StopShowItemTip();
  }

  public void SetupImd(int inSlotItemId)
  {
    this.slotItemId = inSlotItemId;
    FestivalExchangeRewardInfo data1 = ConfigManager.inst.DB_FestivalExchangeReward.GetData(this.slotItemId);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data1.itemRewardId);
    this._itemId = data1.itemRewardId;
    Utils.SetItemName(this.panel.itemName, itemStaticInfo.internalId);
    BuilderFactory.Instance.Build((UIWidget) this.panel.itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, false, false, true, string.Empty);
    this.panel.itemCount.text = data1.itemRewardValue.ToString();
    FestivalExchangeInfo data2 = ConfigManager.inst.DB_FestivalExchange.GetData(ConfigManager.inst.DB_FestivalActives.GetData(this.activityId).exchange_group_id);
    BuilderFactory.Instance.Build((UIWidget) this.panel.itemCostIcon, ConfigManager.inst.DB_Items.GetItem(data2.itemCostId).ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemBackground(this.panel.itemBg, this._itemId);
    this.itemCostCount = data1.cost * data2.itemCostRate;
    this.panel.itemCostCount.text = this.itemCostCount.ToString();
    this.Check(this.currentCount);
    this.panel.rarity_rare.gameObject.SetActive(false);
    this.panel.rarity_special.gameObject.SetActive(false);
    if (!this.initFlag)
      return;
    this.initFlag = false;
    if (data1.type == "rare")
    {
      this.panel.rarity_rare.gameObject.SetActive(true);
    }
    else
    {
      if (!(data1.type == "special"))
        return;
      this.panel.rarity_special.gameObject.SetActive(true);
    }
  }

  public void Check(int count)
  {
    this.panel.BT_Buy.isEnabled = count >= this.itemCostCount;
  }

  public void Release()
  {
    if ((UnityEngine.Object) this.panel.itemIcon.mainTexture != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.panel.itemIcon);
    if ((UnityEngine.Object) this.panel.itemCostIcon.mainTexture != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.panel.itemCostIcon);
    this.activityId = -1;
    this.slotItemId = -1;
  }

  public void OnTweenEnd()
  {
    this.SetupImd(this.newSlotItemId);
    EventDelegate.Add(this.panel.tweenScale.onFinished, (EventDelegate.Callback) (() =>
    {
      FestivalExchangeRewardInfo data = ConfigManager.inst.DB_FestivalExchangeReward.GetData(this.slotItemId);
      if (data.type == "rare")
      {
        this.panel.rarity_rare.gameObject.SetActive(true);
      }
      else
      {
        if (!(data.type == "special"))
          return;
        this.panel.rarity_special.gameObject.SetActive(true);
      }
    }), true);
    this.panel.tweenScale.PlayReverse();
  }

  public void OnBuyClick()
  {
    if (this.onBuyClick == null)
      return;
    this.onBuyClick(this.index);
  }

  public void Reset()
  {
    this.panel.tweenScale = this.GetComponent<TweenScale>();
    this.panel.tweenScale.to.y = 0.0f;
    this.panel.itemName = this.transform.Find("IconName").gameObject.GetComponent<UILabel>();
    this.panel.itemIcon = this.transform.Find("ItemIcon").gameObject.GetComponent<UITexture>();
    this.panel.itemCount = this.transform.Find("itemCount").gameObject.GetComponent<UILabel>();
    this.panel.rarity_rare = this.transform.Find("rarity_rare").gameObject.GetComponent<UILabel>();
    this.panel.rarity_special = this.transform.Find("rarity_special").gameObject.GetComponent<UILabel>();
    this.panel.BT_Buy = this.transform.Find("btn_buy").gameObject.GetComponent<UIButton>();
    this.panel.BT_Buy.onClick.Clear();
    EventDelegate.Add(this.panel.BT_Buy.onClick, new EventDelegate.Callback(this.OnBuyClick));
    this.panel.itemCostIcon = this.transform.Find("btn_buy/icon").gameObject.GetComponent<UITexture>();
    this.panel.itemCostCount = this.transform.Find("btn_buy/price").gameObject.GetComponent<UILabel>();
  }

  [Serializable]
  protected class Panel
  {
    public TweenScale tweenScale;
    public UILabel itemName;
    public UITexture itemIcon;
    public UILabel itemCount;
    public UILabel rarity_special;
    public UILabel rarity_rare;
    public UITexture itemCostIcon;
    public UILabel itemCostCount;
    public UIButton BT_Buy;
    public UITexture itemBg;
  }
}
