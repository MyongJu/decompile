﻿// Decompiled with JetBrains decompiler
// Type: RankData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class RankData
{
  private string _AllianceName = string.Empty;
  private string _name;
  private int _KingdomId;

  public int Rank { get; set; }

  public long Score { get; set; }

  public virtual string Image { get; set; }

  public string OrginName
  {
    get
    {
      return this._name;
    }
    set
    {
      this._name = value;
    }
  }

  public string Name
  {
    get
    {
      if (!string.IsNullOrEmpty(this.Tag))
        return "[" + this.Tag + "]" + this._name;
      return this._name;
    }
    set
    {
      this._name = value;
    }
  }

  public string FullName
  {
    get
    {
      string empty = string.Empty;
      string str = string.IsNullOrEmpty(this.Tag) ? this._name : "[" + this.Tag + "]" + this._name;
      if (this.KingdomId > 0)
        str = str + ".K" + (object) this.KingdomId;
      return str;
    }
    set
    {
      this._name = value;
    }
  }

  public int KingdomId
  {
    get
    {
      return this._KingdomId;
    }
    set
    {
      this._KingdomId = value;
    }
  }

  public string AllianceName
  {
    get
    {
      return this._AllianceName;
    }
    set
    {
      this._AllianceName = value;
    }
  }

  public string UID { get; set; }

  public string Tag { get; set; }

  public int Symbol { get; set; }

  public static RankData Parse(Hashtable source)
  {
    if (source == null)
      return (RankData) null;
    RankData rankData;
    if (RankData.IsAllianceData(source))
    {
      AllianceRankData allianceRankData = new AllianceRankData();
      allianceRankData.AllianceId = int.Parse(source[(object) "alliance_id"].ToString());
      allianceRankData.Lang = source[(object) "lang"].ToString();
      allianceRankData.Member = int.Parse(source[(object) "member"].ToString());
      allianceRankData.Rank = int.Parse(source[(object) "rank"].ToString());
      rankData = (RankData) allianceRankData;
    }
    else if (RankData.IsDragonData(source))
    {
      DragonLevelRankData dragonLevelRankData = new DragonLevelRankData();
      dragonLevelRankData.DragonLevel = int.Parse(source[(object) "dragon_level"].ToString());
      dragonLevelRankData.DragonExp = long.Parse(source[(object) "dragon_exp"].ToString());
      if (source.ContainsKey((object) "icon") && source[(object) "icon"] != null)
        dragonLevelRankData.CustomIconUrl = source[(object) "icon"].ToString();
      if (source.ContainsKey((object) "portrait") && source[(object) "portrait"] != null)
        dragonLevelRankData.Portrait = int.Parse(source[(object) "portrait"].ToString());
      if (source.ContainsKey((object) "lord_title") && source[(object) "lord_title"] != null)
        dragonLevelRankData.LordTitleId = int.Parse(source[(object) "lord_title"].ToString());
      if (source.ContainsKey((object) "power") && source[(object) "power"] != null)
        dragonLevelRankData.Power = long.Parse(source[(object) "power"].ToString());
      rankData = (RankData) dragonLevelRankData;
    }
    else if (RankData.IsLordData(source))
    {
      LordLevelRankData lordLevelRankData = new LordLevelRankData();
      lordLevelRankData.LordLevel = int.Parse(source[(object) "lord_level"].ToString());
      lordLevelRankData.LordExp = long.Parse(source[(object) "lord_exp"].ToString());
      if (source.ContainsKey((object) "icon") && source[(object) "icon"] != null)
        lordLevelRankData.CustomIconUrl = source[(object) "icon"].ToString();
      if (source.ContainsKey((object) "portrait") && source[(object) "portrait"] != null)
        lordLevelRankData.Portrait = int.Parse(source[(object) "portrait"].ToString());
      if (source.ContainsKey((object) "lord_title") && source[(object) "lord_title"] != null)
        lordLevelRankData.LordTitleId = int.Parse(source[(object) "lord_title"].ToString());
      if (source.ContainsKey((object) "power") && source[(object) "power"] != null)
        lordLevelRankData.Power = long.Parse(source[(object) "power"].ToString());
      rankData = (RankData) lordLevelRankData;
    }
    else
    {
      PlayerRankData playerRankData = new PlayerRankData();
      if (source.ContainsKey((object) "portrait") && source[(object) "portrait"] != null)
        playerRankData.Portrait = int.Parse(source[(object) "portrait"].ToString());
      if (source.ContainsKey((object) "lord_title") && source[(object) "lord_title"] != null)
        playerRankData.LordTitleId = int.Parse(source[(object) "lord_title"].ToString());
      if (source.ContainsKey((object) "power") && source[(object) "power"] != null)
        playerRankData.Power = long.Parse(source[(object) "power"].ToString());
      if (source.ContainsKey((object) "icon") && source[(object) "icon"] != null)
        playerRankData.CustomIconUrl = source[(object) "icon"].ToString();
      rankData = (RankData) playerRankData;
    }
    if (source.ContainsKey((object) "name") && source[(object) "name"] != null)
      rankData.Name = source[(object) "name"].ToString();
    rankData.Score = long.Parse(source[(object) "score"].ToString());
    rankData.Rank = int.Parse(source[(object) "rank"].ToString());
    if (source.ContainsKey((object) "uid"))
      rankData.UID = source[(object) "uid"].ToString();
    if (source.ContainsKey((object) "tag"))
    {
      rankData.Tag = source[(object) "tag"].ToString();
      rankData.Symbol = int.Parse(source[(object) "symbol_code"].ToString());
    }
    if (source.ContainsKey((object) "alliance_name"))
      rankData._AllianceName = source[(object) "alliance_name"].ToString();
    if (source.ContainsKey((object) "world_id"))
      int.TryParse(source[(object) "world_id"].ToString(), out rankData._KingdomId);
    return rankData;
  }

  private static bool IsAllianceData(Hashtable source)
  {
    return source.ContainsKey((object) "alliance_id");
  }

  private static bool IsDragonData(Hashtable source)
  {
    return source.ContainsKey((object) "dragon_level");
  }

  private static bool IsLordData(Hashtable source)
  {
    return source.ContainsKey((object) "lord_level");
  }
}
