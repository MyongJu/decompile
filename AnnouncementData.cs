﻿// Decompiled with JetBrains decompiler
// Type: AnnouncementData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AnnouncementData
{
  public string subject;
  public string content;
  public string buttonDes;
  public string linkUrl;

  public void ParseData(Hashtable data)
  {
    DatabaseTools.UpdateData(data, "subject", ref this.subject);
    DatabaseTools.UpdateData(data, "content", ref this.content);
    DatabaseTools.UpdateData(data, "linkButton", ref this.buttonDes);
    DatabaseTools.UpdateData(data, "linkUrl", ref this.linkUrl);
  }
}
