﻿// Decompiled with JetBrains decompiler
// Type: BladeTrailVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BladeTrailVFX : SimpleVfx
{
  [SerializeField]
  private float _length = 0.5f;
  [SerializeField]
  private MeshRenderer _renderer;
  private float _start;
  private bool _tween;

  public override void Play(Transform target)
  {
    base.Play(target);
    this._renderer.material.SetFloat("_Percentage", 0.0f);
    this._start = Time.realtimeSinceStartup;
    this._tween = true;
  }

  public override void Stop()
  {
    base.Stop();
    this._tween = false;
  }

  protected override void Update()
  {
    base.Update();
    if (!this._tween)
      return;
    float num = (double) this._length > 0.0 ? (Time.realtimeSinceStartup - this._start) / this._length : 1f;
    this._renderer.material.SetFloat("_Percentage", Mathf.Clamp01(num));
    if ((double) num < 1.0)
      return;
    this._tween = false;
  }
}
