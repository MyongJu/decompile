﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonChooseItemComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AltarSummonChooseItemComponent : ComponentRenderBase
{
  [SerializeField]
  private GameObject _selectedGo;
  [SerializeField]
  private UILabel _itemName;
  [SerializeField]
  private UILabel _itemCount;
  [SerializeField]
  private UITexture _itemIcon;
  public System.Action OnItemCheckChanged;
  public AltarSummonChooseItemComponent.CheckReachedMax CheckReachedMaxFunc;
  private int rewardItemId;
  private bool selected;

  public int RewardItemId
  {
    get
    {
      return this.rewardItemId;
    }
  }

  public bool Selected
  {
    set
    {
      if (this.selected == value || this.CheckReachedMaxFunc != null && this.CheckReachedMaxFunc() && value)
        return;
      this.selected = value;
      this.UpdateSelectedUI();
      if (this.OnItemCheckChanged == null)
        return;
      this.OnItemCheckChanged();
    }
    get
    {
      return this.selected;
    }
  }

  private void UpdateSelectedUI()
  {
    this._selectedGo.SetActive(this.Selected);
  }

  public void OnItemChecked()
  {
    this.Selected = !this.Selected;
  }

  public override void Init()
  {
    base.Init();
    AltarSummonChooseItemComponentData data1 = this.data as AltarSummonChooseItemComponentData;
    if (data1 == null)
      return;
    this.rewardItemId = data1.altarSummonRewardId;
    ConfigAltarSummonRewardInfo data2 = ConfigManager.inst.DB_ConfigAltarSummonRewardMain.GetData(this.rewardItemId);
    if (data2 == null)
      return;
    int itemId = data2.itemId;
    int itemCount = data2.itemCount;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    this._itemName.text = itemStaticInfo.LocName;
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this._itemName, itemStaticInfo.Quality);
    this._itemCount.text = itemCount.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this._itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.gameObject.GetComponentInChildren<ItemIconRenderer>().SetData(itemStaticInfo.internalId, string.Empty, true);
    if (itemStaticInfo.Type == ItemBag.ItemType.parliament_hero_card || itemStaticInfo.Type == ItemBag.ItemType.parliament_hero_card_chip)
      this._itemIcon.gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
    this.UpdateSelectedUI();
  }

  public override void Dispose()
  {
    base.Dispose();
  }

  public delegate bool CheckReachedMax();
}
