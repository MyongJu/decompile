﻿// Decompiled with JetBrains decompiler
// Type: AllianceStorageSlotItemContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceStorageSlotItemContainer : MonoBehaviour
{
  protected List<GameObject> m_allItems = new List<GameObject>();
  public GameObject m_itemTemplate;
  public UITable m_resourceTable;
  public UIScrollView m_resourceScrollView;

  public void SetWarehouseData(AllianceWareHouseData warehouseData)
  {
    int num = 1;
    using (Dictionary<long, AllianceWareHouseData.ResourceData>.Enumerator enumerator = warehouseData.Resource.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, AllianceWareHouseData.ResourceData> current = enumerator.Current;
        AllianceStorageSlotItem allianceStorageSlotItem = this.CreateItem();
        allianceStorageSlotItem.SetData(num++, current.Key, current.Value);
        allianceStorageSlotItem.ParentTable = this.m_resourceTable;
      }
    }
    this.m_resourceTable.Reposition();
    this.m_resourceScrollView.ResetPosition();
  }

  private AllianceStorageSlotItem CreateItem()
  {
    GameObject go = Utils.DuplicateGOB(this.m_itemTemplate);
    go.transform.SetParent(this.m_resourceTable.transform);
    this.m_allItems.Add(go);
    AllianceStorageSlotItem component = go.GetComponent<AllianceStorageSlotItem>();
    NGUITools.SetActive(go, true);
    return component;
  }

  public void Clear()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_allItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    this.m_allItems.Clear();
  }
}
