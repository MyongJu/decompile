﻿// Decompiled with JetBrains decompiler
// Type: QuickSearchPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class QuickSearchPopup : Popup
{
  private int m_SelectedIndex = -1;
  private Dictionary<Coordinate, QuickSearchItemRenderer> m_ItemDict = new Dictionary<Coordinate, QuickSearchItemRenderer>();
  private List<QuickSearchResultDB> m_SearchedResults = new List<QuickSearchResultDB>();
  private GameObjectPool m_ItemPool = new GameObjectPool();
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public UIToggle[] m_Categories;
  public GameObject[] m_Tips;
  public GameObject m_ItemPrefab;
  public GameObject m_ItemRoot;
  private int m_GveNum;
  private int m_MonsterNum;
  private bool isSearchedDataReady;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_ItemPool.Initialize(this.m_ItemPrefab, this.m_ItemRoot);
    this.m_SelectedIndex = -1;
    this.isSearchedDataReady = false;
    this.m_Categories[0].Set(true);
    this.ClearTabPageInfo();
    this.GetSearchedResults();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.m_SearchedResults.Clear();
    this.ClearData();
  }

  public void OnToggleValueChanged()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == this.m_SelectedIndex || selectedIndex < 0)
      return;
    this.m_SelectedIndex = selectedIndex;
    this.UpdateTabPageInfo(this.m_SelectedIndex);
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_Categories.Length; ++index)
    {
      if (this.m_Categories[index].value)
        return index;
    }
    return -1;
  }

  private void GetSearchedResults()
  {
    MessageHub.inst.GetPortByAction("map:findNearbyMonster").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ArrayList arrayList = data as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        QuickSearchResultDB quickSearchResultDb = new QuickSearchResultDB();
        quickSearchResultDb.Decode(arrayList[index]);
        this.m_SearchedResults.Add(quickSearchResultDb);
      }
      this.isSearchedDataReady = true;
      this.UpdateTabPageInfo(0);
    }), true);
  }

  private void UpdateTabPageInfo(int selectedIndex)
  {
    this.ClearTabPageInfo();
    this.m_SearchedResults.Sort((Comparison<QuickSearchResultDB>) ((a, b) => a.resource_level.CompareTo(b.resource_level)));
    for (int index = 0; index < this.m_SearchedResults.Count; ++index)
    {
      this.ShowSearchedResultItems(selectedIndex, this.m_SearchedResults[index]);
      if (this.m_SearchedResults[index].resource_type == 9)
        ++this.m_MonsterNum;
      else if (this.m_SearchedResults[index].resource_type == 15)
        ++this.m_GveNum;
    }
    if ((this.m_SearchedResults.Count == 0 || this.m_MonsterNum <= 0 && selectedIndex == 0 || this.m_GveNum <= 0 && selectedIndex == 1) && this.isSearchedDataReady)
      this.UpdateTipInfo(selectedIndex);
    this.Reposition();
  }

  private void ClearTabPageInfo()
  {
    for (int index = 0; index < this.m_Tips.Length; ++index)
      this.m_Tips[index].gameObject.SetActive(false);
    this.ClearData();
  }

  private void UpdateTipInfo(int selectedIndex)
  {
    if (selectedIndex >= this.m_Categories.Length || selectedIndex < 0)
      return;
    this.m_Tips[selectedIndex].gameObject.SetActive(true);
  }

  private void ShowSearchedResultItems(int selectedIndex, QuickSearchResultDB sr)
  {
    if (!this.CanItemShow(selectedIndex, sr))
      return;
    QuickSearchItemRenderer itemRenderer = this.CreateItemRenderer(selectedIndex, sr);
    this.m_ItemDict.Add(sr.location, itemRenderer);
  }

  private QuickSearchItemRenderer CreateItemRenderer(int index, QuickSearchResultDB sr)
  {
    GameObject gameObject = this.m_ItemPool.AddChild(this.m_Grid.gameObject);
    gameObject.SetActive(true);
    QuickSearchItemRenderer component = gameObject.GetComponent<QuickSearchItemRenderer>();
    component.SetData(index, sr);
    return component;
  }

  private bool CanItemShow(int selectedIndex, QuickSearchResultDB searchedResult)
  {
    return selectedIndex == 0 && searchedResult.resource_type == 9 || selectedIndex == 1 && searchedResult.resource_type == 15;
  }

  private void Reposition()
  {
    this.m_Grid.repositionNow = true;
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
    this.m_ScrollView.gameObject.SetActive(false);
    this.m_ScrollView.gameObject.SetActive(true);
  }

  private void ClearData()
  {
    using (Dictionary<Coordinate, QuickSearchItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.m_ItemPool.Release(gameObject);
      }
    }
    this.m_ItemDict.Clear();
    this.m_ItemPool.Clear();
    this.m_MonsterNum = 0;
    this.m_GveNum = 0;
  }
}
