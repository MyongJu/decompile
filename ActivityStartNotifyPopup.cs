﻿// Decompiled with JetBrains decompiler
// Type: ActivityStartNotifyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class ActivityStartNotifyPopup : Popup
{
  [SerializeField]
  private ActivityStartNotifyPopup.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    ActivityStartNotifyPopup.Parameter parameter = orgParam as ActivityStartNotifyPopup.Parameter;
    if (parameter == null)
      return;
    if (parameter.isKnight)
    {
      this.panel.title.text = Utils.XLAT("event_fallen_knight_name");
      this.panel.description.text = Utils.XLAT("event_fallen_knight_description");
    }
    else
    {
      this.panel.title.text = Utils.XLAT("event_gold_event_name");
      this.panel.description.text = Utils.XLAT("event_gold_event_description");
    }
  }

  public void OnButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    GameObject gameObject = GameObject.Find("activity");
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    LinkerHub.Instance.MountHint(gameObject.transform, 0, (object) null);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("LittleTitle/rewards").GetComponent<UILabel>();
    this.panel.description = this.transform.Find("DiscriptionLabel").GetComponent<UILabel>();
  }

  [Serializable]
  public class Panel
  {
    public UILabel title;
    public UILabel description;
  }

  public class Parameter : Popup.PopupParameter
  {
    public long activityId;
    public bool isKnight;
  }
}
