﻿// Decompiled with JetBrains decompiler
// Type: MinimapRoomItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MinimapRoomItem : MonoBehaviour
{
  private const string SPRITE_NAME_MONSTER = "icon_dungeons_mini_map_monster";
  private const string SPRITE_NAME_BOSS = "icon_dungeons_mini_map_boss";
  private const string SPRITE_NAME_CHEST = "icon_dungeons_mini_map_chest";
  private const string SPRITE_NAME_BUFF = "icon_dungeons_mini_map_buff";
  [SerializeField]
  protected GameObject _tagRoom;
  [SerializeField]
  protected GameObject _tagLeftWay;
  [SerializeField]
  protected GameObject _tagRightWay;
  [SerializeField]
  protected GameObject _tagForwardWay;
  [SerializeField]
  protected GameObject _tagBackwardWay;
  [SerializeField]
  protected UISprite _tagRoomContent;
  [SerializeField]
  protected GameObject _tagExit;
  protected Room _room;

  public void SetRoom(Room room)
  {
    this._room = room;
    if (this._room != null && this._room.IsSearched)
    {
      this.SetWayVisible(this._room.LeftRoom != null, this._room.RightRoom != null, this._room.ForwardRoom != null, this._room.BackwardRoom != null);
      this._tagRoom.SetActive(true);
      this._tagExit.SetActive(room.IsExit);
      this._tagRoomContent.gameObject.SetActive(true);
      if (this._room.ContainsBoss())
        this._tagRoomContent.spriteName = "icon_dungeons_mini_map_boss";
      else if (this._room.ContainsMonsters())
        this._tagRoomContent.spriteName = "icon_dungeons_mini_map_monster";
      else if (this._room.ContainsChest())
        this._tagRoomContent.spriteName = "icon_dungeons_mini_map_chest";
      else if (this._room.ContainsBuff())
        this._tagRoomContent.spriteName = "icon_dungeons_mini_map_buff";
      else
        this._tagRoomContent.gameObject.SetActive(false);
    }
    else
    {
      this.SetWayVisible(false, false, false, false);
      this._tagRoom.SetActive(false);
      this._tagRoomContent.gameObject.SetActive(false);
      this._tagExit.SetActive(false);
    }
  }

  protected void SetWayVisible(bool left, bool right, bool forward, bool backward)
  {
    this._tagLeftWay.SetActive(left);
    this._tagRightWay.SetActive(right);
    this._tagForwardWay.SetActive(forward);
    this._tagBackwardWay.SetActive(backward);
  }

  public void OnClicked()
  {
    if (!PlayerData.inst.hostPlayer.VIPActive || !PlayerData.inst.hostPlayer.VIPLevelInfo.ContainsPrivilege("dk_dungeon_teleport") || (!this._room.IsSearched || this._room == DragonKnightSystem.Instance.RoomManager.CurrentRoom))
      return;
    DragonKnightSystem.Instance.Controller.RoomHud.RequestMoveToCertainRoom(this._room);
  }
}
