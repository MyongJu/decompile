﻿// Decompiled with JetBrains decompiler
// Type: ToastDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class ToastDialog : MonoBehaviour
{
  [HideInInspector]
  public bool isplaying;
  [HideInInspector]
  public bool played;

  public virtual void Init()
  {
    this.isplaying = false;
    this.played = false;
  }

  public abstract void SeedData(object args);

  public abstract void Play();

  public virtual void Interrupted()
  {
  }

  public virtual void FastPlay()
  {
    this.Play();
  }
}
