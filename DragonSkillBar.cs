﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillBar : MonoBehaviour
{
  private const int DRAGON_SKILL_BAR_SLOT_COUNT = 8;
  public DragonSkillBarSlot[] m_Slots;
  private string m_Usability;

  private void OnEnable()
  {
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
  }

  private void OnDragonDataChanged(long generalID)
  {
    if (generalID != PlayerData.inst.uid)
      return;
    this.UpdateUI();
  }

  public void SetData(string usability)
  {
    this.m_Usability = usability;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    List<int> skillBar = PlayerData.inst.dragonData.Skills.GetSkillBar(this.m_Usability);
    for (int index = 0; index < 8; ++index)
    {
      this.m_Slots[index].Usability = this.m_Usability;
      this.m_Slots[index].SlotIndex = index;
      this.m_Slots[index].OnUnEquip = new OnDragonSkillUnEquipCallback(this.UnEquip);
      if (skillBar != null && index < skillBar.Count)
        this.m_Slots[index].SetData(PlayerData.inst.dragonData.Uid, skillBar[index]);
      else
        this.m_Slots[index].SetData(PlayerData.inst.uid, 0);
    }
  }

  public void Equip(int skill_id)
  {
    if (!((UnityEngine.Object) this.GetEquippedSlot(skill_id) == (UnityEngine.Object) null))
      return;
    DragonSkillBarSlot freeSlot = this.GetFreeSlot();
    if (!((UnityEngine.Object) freeSlot != (UnityEngine.Object) null))
      return;
    freeSlot.SetData(PlayerData.inst.uid, skill_id);
    this.UpdateServerDatabase((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ConfigDragonSkillGroupInfo skillGroupBySkillId = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupBySkillId(skill_id);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["skill"] = Utils.XLAT(skillGroupBySkillId.localization_name);
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_dragon_skill_assign_success", para, true), (System.Action) null, 4f, false);
      AudioManager.Instance.StopAndPlaySound("sfx_city_dragon_skill_assign");
    }));
  }

  public void UnEquip(int skill_id)
  {
    DragonSkillBarSlot equippedSlot = this.GetEquippedSlot(skill_id);
    if (!((UnityEngine.Object) equippedSlot != (UnityEngine.Object) null))
      return;
    int index = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level).skill_slot - 1;
    for (int slotIndex = equippedSlot.SlotIndex; slotIndex < index; ++slotIndex)
      this.m_Slots[slotIndex].SetData(PlayerData.inst.uid, this.m_Slots[slotIndex + 1].dragonSkillID);
    this.m_Slots[index].SetData(PlayerData.inst.uid, 0);
    this.UpdateServerDatabase((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ConfigDragonSkillGroupInfo skillGroupBySkillId = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupBySkillId(skill_id);
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["skill"] = Utils.XLAT(skillGroupBySkillId.localization_name);
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("toast_dragon_skill_unassign_success", para, true), (System.Action) null, 4f, false);
    }));
  }

  private void UpdateServerDatabase(System.Action<bool, object> callback)
  {
    Hashtable hashtable = new Hashtable();
    hashtable[(object) this.m_Usability] = (object) this.GetUpdateParameters();
    Hashtable postData = new Hashtable();
    postData[(object) "skill_column"] = (object) hashtable;
    MessageHub.inst.GetPortByAction("dragon:updateSkillColumn").SendRequest(postData, callback, true);
  }

  private ArrayList GetUpdateParameters()
  {
    ArrayList arrayList = new ArrayList();
    int skillSlot = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level).skill_slot;
    for (int index = 0; index < skillSlot; ++index)
    {
      if (this.m_Slots[index].dragonSkill == null)
        arrayList.Add((object) 0);
      else
        arrayList.Add((object) this.m_Slots[index].dragonSkill.internalId);
    }
    return arrayList;
  }

  private DragonSkillBarSlot GetFreeSlot()
  {
    int skillSlot = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(PlayerData.inst.dragonData.Level).skill_slot;
    for (int index = 0; index < skillSlot; ++index)
    {
      if (this.m_Slots[index].dragonSkill == null)
        return this.m_Slots[index];
    }
    return (DragonSkillBarSlot) null;
  }

  private DragonSkillBarSlot GetEquippedSlot(int skill_id)
  {
    for (int index = 0; index < 8; ++index)
    {
      if (this.m_Slots[index].dragonSkill != null && this.m_Slots[index].dragonSkill.internalId == skill_id)
        return this.m_Slots[index];
    }
    return (DragonSkillBarSlot) null;
  }
}
