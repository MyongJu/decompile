﻿// Decompiled with JetBrains decompiler
// Type: LinkedList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LinkedList
{
  private LinkedList.Node m_Null = new LinkedList.Node();

  public LinkedList()
  {
    this.m_Null.m_Next = this.m_Null;
    this.m_Null.m_Prev = this.m_Null;
  }

  public LinkedList.Node GetFirst()
  {
    return this.m_Null.m_Next;
  }

  public LinkedList.Node GetLast()
  {
    return this.m_Null.m_Prev;
  }

  public LinkedList.Node GetEnd()
  {
    return this.m_Null;
  }

  public void PushBack(LinkedList.Node node)
  {
    LinkedList.Node prev = this.m_Null.m_Prev;
    prev.m_Next = node;
    node.m_Prev = prev;
    node.m_Next = this.m_Null;
    this.m_Null.m_Prev = node;
  }

  public LinkedList.Node PopBack()
  {
    if (this.IsEmpty())
      return (LinkedList.Node) null;
    LinkedList.Node prev1 = this.m_Null.m_Prev;
    LinkedList.Node prev2 = prev1.m_Prev;
    prev2.m_Next = this.m_Null;
    this.m_Null.m_Prev = prev2;
    prev1.m_Prev = (LinkedList.Node) null;
    prev1.m_Next = (LinkedList.Node) null;
    return prev1;
  }

  public void PushFront(LinkedList.Node node)
  {
    LinkedList.Node next = this.m_Null.m_Next;
    next.m_Prev = node;
    node.m_Next = next;
    node.m_Prev = this.m_Null;
    this.m_Null.m_Next = node;
  }

  public LinkedList.Node PopFront()
  {
    if (this.IsEmpty())
      return (LinkedList.Node) null;
    LinkedList.Node next1 = this.m_Null.m_Next;
    LinkedList.Node next2 = next1.m_Next;
    next2.m_Prev = this.m_Null;
    this.m_Null.m_Next = next2;
    next1.m_Prev = (LinkedList.Node) null;
    next1.m_Next = (LinkedList.Node) null;
    return next1;
  }

  public void Remove(LinkedList.Node node)
  {
    LinkedList.Node prev = node.m_Prev;
    LinkedList.Node next = node.m_Next;
    prev.m_Next = next;
    next.m_Prev = prev;
    node.m_Prev = (LinkedList.Node) null;
    node.m_Next = (LinkedList.Node) null;
  }

  public bool IsEmpty()
  {
    if (this.m_Null.m_Prev == this.m_Null)
      return this.m_Null.m_Next == this.m_Null;
    return false;
  }

  public class Node
  {
    public LinkedList.Node m_Prev;
    public LinkedList.Node m_Next;
  }
}
