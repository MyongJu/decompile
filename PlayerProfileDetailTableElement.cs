﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileDetailTableElement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PlayerProfileDetailTableElement : MonoBehaviour
{
  private const string LABEL_PAIR_PATH = "Prefab/UI/Dialogs/PlayerProfile/LabelPair";
  public UILabel titleLabel;
  private List<NameValuePair> pairs;

  public void UpdateUI(string title, List<NameValuePair> pairs)
  {
    this.titleLabel.text = title;
    PlayerProfileLabelPair[] componentsInChildren = this.GetComponentsInChildren<PlayerProfileLabelPair>();
    GameObject prefab = AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/PlayerProfile/LabelPair", (System.Type) null) as GameObject;
    for (int index = 0; index < componentsInChildren.Length; ++index)
      componentsInChildren[index].gameObject.SetActive(false);
    for (int index = 0; index < pairs.Count; ++index)
    {
      if (index >= componentsInChildren.Length)
      {
        NGUITools.AddChild(this.gameObject, prefab).GetComponent<PlayerProfileLabelPair>().UpdateUI(pairs[index].nameKey, pairs[index].value);
      }
      else
      {
        componentsInChildren[index].gameObject.SetActive(true);
        componentsInChildren[index].UpdateUI(pairs[index].nameKey, pairs[index].value);
      }
    }
  }
}
