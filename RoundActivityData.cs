﻿// Decompiled with JetBrains decompiler
// Type: RoundActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class RoundActivityData : ActivityBaseData, IRankUIRewardData
{
  public Dictionary<int, RoundActivityData.RoundData> Rounds = new Dictionary<int, RoundActivityData.RoundData>();
  private int _currentRound = 1;
  public const long INVALID_ID = 0;
  private int _stronghold_level;
  private int _totalRounds;
  private long _currentRoundScore;
  private int _currentRoundRank;
  private int _activityRank;
  private bool _isOpenRanking;

  public override void Decode(Hashtable source)
  {
    if (source == null)
      return;
    base.Decode(source);
    bool flag = false | DatabaseTools.UpdateData(source, "count", ref this._totalRounds) | DatabaseTools.UpdateData(source, "current_step", ref this._currentRound) | DatabaseTools.UpdateData(source, "current_integral", ref this._currentRoundScore) | DatabaseTools.UpdateData(source, "step_rank", ref this._currentRoundRank) | DatabaseTools.UpdateData(source, "total_rank", ref this._activityRank) | DatabaseTools.UpdateData(source, "stronghold_level", ref this._stronghold_level) | DatabaseTools.UpdateData(source, "is_open_ranking", ref this._isOpenRanking);
    if (this._totalRounds <= 0)
      return;
    if (this._currentRound == 0)
      this._currentRound = 1;
    this.Rounds.Clear();
    int startTime = this.StartTime;
    for (int index = 0; index < this._totalRounds; ++index)
    {
      string str = "step" + (object) (index + 1);
      if (source.ContainsKey((object) str))
      {
        RoundActivityData.RoundData roundData = new RoundActivityData.RoundData();
        if (roundData.Decode(source[(object) str]))
        {
          startTime += roundData.duration;
          roundData.EndTime = startTime;
          roundData.index = index + 1;
          this.Rounds.Add(index + 1, roundData);
        }
      }
    }
  }

  public int StrongholdLevel
  {
    get
    {
      if (this._stronghold_level == 0)
        return CityManager.inst.mStronghold.mLevel;
      return this._stronghold_level;
    }
  }

  public bool NextRound()
  {
    ++this._currentRound;
    if (!this.Rounds.ContainsKey(this._currentRound))
    {
      --this._currentRound;
      return false;
    }
    this._currentRoundScore = 0L;
    this._currentRoundRank = 0;
    return true;
  }

  public RoundActivityData.RoundData CurrentRound
  {
    get
    {
      if (this.Rounds.Count > 0 && this.Rounds.ContainsKey(this._currentRound))
        return this.Rounds[this._currentRound];
      return (RoundActivityData.RoundData) null;
    }
  }

  public int TotalRounds
  {
    get
    {
      return this._totalRounds;
    }
  }

  public long CurrentRoundScore
  {
    get
    {
      return this._currentRoundScore;
    }
  }

  public int CurrentRoundRank
  {
    get
    {
      return this._currentRoundRank;
    }
  }

  public int ActivityRank
  {
    get
    {
      return this._activityRank;
    }
  }

  public bool IsOpenRanking
  {
    get
    {
      return this._isOpenRanking;
    }
  }

  public float ProgressValue
  {
    get
    {
      float num1 = 0.0f;
      float num2 = 1f / (float) this.CurrentRoundTargetScores.Count;
      int num3 = 0;
      if (this.CurrentRoundScore >= (long) this.CurrentRoundTargetScores[this.CurrentRoundTargetScores.Count - 1])
        return 1f;
      for (int index = 0; index < this.CurrentRoundTargetScores.Count; ++index)
      {
        if (this.CurrentRoundScore > (long) this.CurrentRoundTargetScores[index])
        {
          num3 = this.CurrentRoundTargetScores[index];
        }
        else
        {
          float num4 = (float) (this.CurrentRoundScore - (long) num3) / (float) (this.CurrentRoundTargetScores[index] - num3);
          num1 = ((float) index + num4) * num2;
          break;
        }
      }
      if ((double) num1 > 1.0)
        num1 = 1f;
      return num1;
    }
  }

  public virtual List<int> CurrentRoundTargetScores
  {
    get
    {
      return (List<int>) null;
    }
  }

  public virtual Dictionary<int, int> CurrentRoundRewardItems
  {
    get
    {
      return (Dictionary<int, int>) null;
    }
  }

  public virtual List<int> CurrentItems
  {
    get
    {
      return (List<int>) null;
    }
  }

  public virtual List<int> TotalRankTopOneItems
  {
    get
    {
      return (List<int>) null;
    }
  }

  public virtual Dictionary<int, int> TotalRankTopOneRewardItems
  {
    get
    {
      return (Dictionary<int, int>) null;
    }
  }

  public List<int> GetRankTopOneItems()
  {
    return this.RankTopOneItems;
  }

  public Dictionary<int, int> GetRankTopOneRewardItems()
  {
    return this.RankTopOneRewardItems;
  }

  public virtual List<int> RankTopOneItems
  {
    get
    {
      return this.GetRankItems(this.CurrentRound.ActivitySubConfigID, 1);
    }
  }

  public virtual Dictionary<int, int> RankTopOneRewardItems
  {
    get
    {
      return this.GetRankRewardItems(this.CurrentRound.ActivitySubConfigID, 1);
    }
  }

  public virtual List<int> GetRankItems(int subActivityId, int rank)
  {
    return (List<int>) null;
  }

  public virtual Dictionary<int, int> GetRankRewardItems(int subActivityId, int rank)
  {
    return (Dictionary<int, int>) null;
  }

  public virtual List<string> GetRequirementDesc()
  {
    return (List<string>) null;
  }

  public override int RemainTime
  {
    get
    {
      return !this.IsStart() ? this.StartTime - NetServerTime.inst.ServerTimestamp : this.CurrentRound.EndTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public class RoundData
  {
    public int index;
    public int activityId;
    public int duration;
    public int configId;

    public int ActivitySubConfigID
    {
      get
      {
        return this.configId;
      }
    }

    public int EndTime { set; get; }

    public bool Decode(object orgData)
    {
      bool flag = false;
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return false;
      return flag | DatabaseTools.UpdateData(inData, "activity_id", ref this.activityId) | DatabaseTools.UpdateData(inData, "duration", ref this.duration) | DatabaseTools.UpdateData(inData, "internal_id", ref this.configId);
    }
  }

  private struct Params
  {
    public const string KEY = "integral_config";
    public const string UID = "uid";
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string TOTAL_ROUND = "count";
    public const string CURRENT_ROUND = "current_step";
    public const string CURRENT_ROUND_RANK = "step_rank";
    public const string CURRENT_ROUND_SCORE = "current_integral";
    public const string ACTIVITY_RANK = "total_rank";
    public const string ROUDN_PRE = "step";
    public const string ACTIVITY_MAIN_ID = "activity_id";
    public const string DURATION = "duration";
    public const string INTERNAL_ID = "internal_id";
    public const string STRONGHOLD_LEVEL = "stronghold_level";
    public const string IS_OPEN_RANKING = "is_open_ranking";
  }
}
