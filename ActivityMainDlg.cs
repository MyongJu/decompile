﻿// Decompiled with JetBrains decompiler
// Type: ActivityMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityMainDlg : UI.Dialog
{
  private List<ActivityADItem> totalAdItems = new List<ActivityADItem>();
  private List<ActivityBaseUIData> activityData = new List<ActivityBaseUIData>();
  private List<ActivityBaseUIData> _adActivityData = new List<ActivityBaseUIData>();
  public IconGroup group;
  public UIScrollView scrollView;
  public LoopAD loop;
  public UILabel title;
  public ActivityFestivalShopPopup festivalShop;
  public ActivityFestivalShopEntrancePopup festivalEntrance;
  private bool init;

  private void CreateUIData()
  {
    this.activityData.Clear();
    this._adActivityData.Clear();
    if (!ActivityManager.Intance.kingdomWarData.IsEmpty())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new KingdomWarActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.kingdomWarData;
      this.activityData.Add(activityBaseUiData);
    }
    if (ActivityManager.Intance.pitData.IsActivityShouldOpen)
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new PitActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.pitData;
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (!ActivityManager.Intance.dkArenaActivity.IsEmpty())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new DKArenaActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.dkArenaActivity;
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (!ActivityManager.Intance.worldBoss.IsEmpty())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new WorldBossActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.worldBoss;
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (!ActivityManager.Intance.allianceWar.IsEmpty())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new AllianceWarActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.allianceWar;
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (!ActivityManager.Intance.personalData.IsEmpty())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new PersonalActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.personalData;
      if (!activityBaseUiData.IsFinish())
      {
        this.activityData.Add(activityBaseUiData);
        this._adActivityData.Add(activityBaseUiData);
      }
    }
    if (ActivityManager.Intance.isPortalExist)
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new AllianceBossActivityUIData();
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (ActivityManager.Intance.HadAllianceActivity())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new AllianceActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.GetRoundData(ActivityBaseData.ActivityType.AllianceActivity);
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (ActivityManager.Intance.HadTimeLimitActivity())
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new TimeLimtActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.GetRoundData(ActivityBaseData.ActivityType.TimeLimit);
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    for (int index = 0; index < ActivityManager.Intance.festivals.Count; ++index)
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new FestivalActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.festivals[index];
      this.activityData.Add(activityBaseUiData);
      this._adActivityData.Add(activityBaseUiData);
    }
    if (ActivityManager.Intance.knightData.isContainActivity)
    {
      ActivityBaseUIData activityBaseUiData = (ActivityBaseUIData) new KnightActivityUIData();
      activityBaseUiData.Data = (object) ActivityManager.Intance.knightData;
      if (!activityBaseUiData.IsFinish())
      {
        this.activityData.Add(activityBaseUiData);
        this._adActivityData.Add(activityBaseUiData);
      }
    }
    this.activityData.Sort(new Comparison<ActivityBaseUIData>(this.Compare));
    this._adActivityData.Sort(new Comparison<ActivityBaseUIData>(this.Compare));
  }

  private int Compare(ActivityBaseUIData a, ActivityBaseUIData b)
  {
    int priority1 = ConfigManager.inst.DB_ActivityPriority.GetPriority(a.EventKey);
    int priority2 = ConfigManager.inst.DB_ActivityPriority.GetPriority(b.EventKey);
    if (priority1 != priority2)
      return priority1.CompareTo(priority2);
    return a.EndTime.CompareTo(b.EndTime);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
    WorldBossActivityPayload.Instance.NeedToRefreshActivityMainDlg = false;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this._adActivityData.Count > 0)
    {
      NGUITools.SetActive(this.loop.gameObject, true);
      this.loop.StartPlay();
    }
    else
      NGUITools.SetActive(this.loop.gameObject, false);
    if (WorldBossActivityPayload.Instance.NeedToRefreshActivityMainDlg)
    {
      this.OnRefreshHandler();
      WorldBossActivityPayload.Instance.NeedToRefreshActivityMainDlg = false;
    }
    ActivityManager.Intance.HasNewActivty = false;
    PlayerPrefsEx.SetIntByUid("NEW_ACTIVITY_TIME", NetServerTime.inst.ServerTimestamp);
    Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() => this.scrollView.ResetPosition()));
  }

  private void UpdateUI()
  {
    if (!ActivityManager.Intance.isActivityExist)
    {
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
    {
      this.CreateUIData();
      this.title.text = ScriptLocalization.Get("event_center_name", true);
      this.loop.minIndex = 0;
      this.loop.maxIndex = this._adActivityData.Count - 1;
      this.group.CreateIcon(this.GetIconData());
      Utils.ExecuteInSecs(1f / 1000f, (System.Action) (() =>
      {
        this.scrollView.ResetPosition();
        this.scrollView.gameObject.SetActive(false);
        this.scrollView.gameObject.SetActive(true);
      }));
      this.AddEventHandler();
      this.group.gameObject.SetActive(true);
      this.festivalEntrance.Hide();
      this.festivalShop.Hide();
    }
  }

  public void ShowFestivalEntrance(int activityId)
  {
    int realIndex = -1;
    for (int index = 0; index < this._adActivityData.Count; ++index)
    {
      if (this._adActivityData[index].ActivityID == activityId)
      {
        realIndex = index;
        break;
      }
    }
    this.festivalEntrance.Show(activityId);
    this.loop.SetSelected(realIndex);
    this.festivalShop.Hide();
    this.group.gameObject.SetActive(false);
  }

  public void ShowFestvialShop(int activityId)
  {
    this.festivalShop.Show(activityId);
    this.festivalEntrance.Hide();
    this.group.gameObject.SetActive(false);
  }

  private void OnAdItemInit(GameObject go, int index)
  {
    ActivityADItem component = go.GetComponent<ActivityADItem>();
    if (!this.totalAdItems.Contains(component))
      this.totalAdItems.Add(component);
    if (index >= this._adActivityData.Count)
      return;
    component.SetData(this._adActivityData[index]);
  }

  private List<IconData> GetIconData()
  {
    List<IconData> iconDataList = new List<IconData>();
    for (int index = 0; index < this.activityData.Count; ++index)
    {
      if (this.activityData[index].GetNormalIconData() != null)
        iconDataList.Add(this.activityData[index].GetNormalIconData());
    }
    return iconDataList;
  }

  private void AddEventHandler()
  {
    if (this.init)
      return;
    this.loop.OnItemInHandler += new System.Action<GameObject, int>(this.OnAdItemInit);
    ActivityManager.Intance.OnTimeLimitActivityFinish += new System.Action(this.OnFinishHandler);
    ActivityManager.Intance.OnActivtyRefresh -= new System.Action(this.OnRefreshHandler);
    this.init = true;
  }

  private void OnFinishHandler()
  {
    ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.UpdateUI));
  }

  private void OnNewRoundHandler()
  {
  }

  private void OnRefreshHandler()
  {
    if (!ActivityManager.Intance.isActivityExist)
      UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    else
      ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.UpdateUI));
  }

  private void RemoveEventHandler()
  {
    this.init = false;
    this.loop.OnItemInHandler -= new System.Action<GameObject, int>(this.OnAdItemInit);
    ActivityManager.Intance.OnTimeLimitActivityFinish -= new System.Action(this.OnFinishHandler);
    ActivityManager.Intance.OnActivtyRefresh += new System.Action(this.OnRefreshHandler);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    this.group.Dispose();
    this.loop.Stop();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    this.Clear();
    for (int index = 0; index < this.totalAdItems.Count; ++index)
      this.totalAdItems[index].Clear();
    this.totalAdItems.Clear();
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.DragonKnight || !BattleManager.Instance.IsFinish)
      return;
    UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
  }
}
