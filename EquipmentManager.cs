﻿// Decompiled with JetBrains decompiler
// Type: EquipmentManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentManager
{
  private List<int> noticedScrollChipItemID = new List<int>();
  private static EquipmentManager m_Instance;

  public static EquipmentManager Instance
  {
    get
    {
      if (EquipmentManager.m_Instance == null)
        EquipmentManager.m_Instance = new EquipmentManager();
      return EquipmentManager.m_Instance;
    }
  }

  public void Init()
  {
    GameEngine.Instance.OnGameModeReady += new System.Action(this.OnGameModeReady);
  }

  private void OnGameModeReady()
  {
    DBManager.inst.DB_Item.onDataCreated += new System.Action<int>(this.OnItemCreate);
  }

  public void Dispose()
  {
    DBManager.inst.DB_Item.onDataCreated -= new System.Action<int>(this.OnItemCreate);
    GameEngine.Instance.OnGameModeReady -= new System.Action(this.OnGameModeReady);
  }

  private void OnItemCreate(int item_id)
  {
    ConsumableItemData data = DBManager.inst.DB_Item.Datas[(long) item_id];
    if (data == null)
      return;
    data.isNew = true;
  }

  public void ForgeEquipment(BagType bagType, int scrollID, bool instant, int gold, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "scroll_id", (object) scrollID);
    postData.Add((object) nameof (instant), (object) instant);
    postData.Add((object) nameof (gold), (object) gold);
    if (bagType == BagType.Hero)
      postData.Add((object) "building_id", (object) CitadelSystem.inst.GetForge().mBuildingID);
    MessageHub.inst.GetPortByAction(bagType != BagType.DragonKnight ? "Blacksmith:forge" : "dragonKnight:forge").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void CollectEquipment(BagType bagType, System.Action<bool, object> callBack = null)
  {
    Hashtable postData = new Hashtable();
    postData.Add((object) "uid", (object) PlayerData.inst.uid);
    if (bagType == BagType.Hero)
      postData.Add((object) "building_id", (object) CitadelSystem.inst.GetForge().mBuildingID);
    MessageHub.inst.GetPortByAction(bagType != BagType.DragonKnight ? "Blacksmith:collect" : "dragonKnight:collectEquip").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callBack == null)
        return;
      callBack(ret, data);
      int num = int.Parse((data as Hashtable)[(object) "equip_id"].ToString());
      ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(bagType).GetData(num);
      if (bagType == BagType.Hero)
        ChatMessageManager.SendForgeSuccessMessage(data1.LocNameId, data1.quality, num, 0);
      else
        ChatMessageManager.SendForgeDKSuccessMessage(data1.LocNameId, data1.quality, num, 0);
    }), true);
  }

  public void CombinScroll(BagType bagType, int chipID, int amount, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.DragonKnight ? "Blacksmith:combineScroll" : "dragonKnight:combineScroll").SendRequest(new Hashtable()
    {
      {
        (object) "chip_id",
        (object) chipID
      },
      {
        (object) nameof (amount),
        (object) amount
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Enhance(BagType bagType, long equipID, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.DragonKnight ? "Blacksmith:enhance" : "dragonKnight:enhanceEquip").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) equipID
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        int num = int.Parse((data as Hashtable)[(object) "equip_id"].ToString());
        ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(bagType).GetData(num);
        InventoryItemData myItemByItemId = DBManager.inst.GetInventory(bagType).GetMyItemByItemID(equipID);
        Utils.GetColoredString(data1.LocName + (myItemByItemId.enhanced <= 0 ? string.Empty : "+" + (object) myItemByItemId.enhanced), (Color32) EquipmentConst.QULITY_COLOR_MAP[data1.quality]);
        if (myItemByItemId.enhanced > 0)
        {
          if (bagType == BagType.Hero)
            ChatMessageManager.SendEquiomentEnhance(data1.LocNameId, data1.quality, myItemByItemId.enhanced.ToString(), num, myItemByItemId.enhanced);
          else
            ChatMessageManager.SendEquiomentEnhanceDK(data1.LocNameId, data1.quality, myItemByItemId.enhanced.ToString(), num, myItemByItemId.enhanced);
        }
        UIManager.inst.ShowBasicToast("toast_equipment_enhance_success");
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Disassemble(BagType bagType, long itemId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.DragonKnight ? "Blacksmith:disassemble" : "dragonKnight:disassambleEquip").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) itemId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Equip(BagType bagType, long itemID, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.Hero ? "dragonKnight:equip" : "Hero:equip").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) itemID
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        UIManager.inst.ShowBasicToast("toast_equipment_equip_success");
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void UnEquip(BagType bagType, long itemID, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.Hero ? "dragonKnight:unequip" : "Hero:unequip").SendRequest(new Hashtable()
    {
      {
        (object) "item_id",
        (object) itemID
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        UIManager.inst.ShowBasicToast("toast_equipment_remove_success");
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ReplaceEquipment(BagType bagType, long oldItemID, long newItemID, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction(bagType != BagType.Hero ? "dragonKnight:replaceEquipment" : "Hero:replaceEquipment").SendRequest(new Hashtable()
    {
      {
        (object) "old_item_id",
        (object) oldItemID
      },
      {
        (object) "new_item_id",
        (object) newItemID
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        UIManager.inst.ShowBasicToast("toast_equipment_replace_success");
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public int GetEquippedItemID(BagType bagType, int equipType)
  {
    switch (bagType)
    {
      case BagType.Hero:
        return PlayerData.inst.heroData.equipments.GetSlotEquipID(equipType);
      case BagType.DragonKnight:
        if (PlayerData.inst.dragonKnightData != null)
          return PlayerData.inst.dragonKnightData.equipments.GetSlotEquipID(equipType);
        return 0;
      default:
        return 0;
    }
  }

  public void ConfigQualityLabelWithColor(UILabel label, int quality)
  {
    label.color = this.GetColorWithQuality(quality);
  }

  public int GetOwnForgeQueue(BagType bagType)
  {
    switch (bagType)
    {
      case BagType.Hero:
        return JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_FORGE) == null ? 1 : 0;
      case BagType.DragonKnight:
        return JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_DK_FORGE) == null ? 1 : 0;
      default:
        return 0;
    }
  }

  public bool HasUnCollectedEquipment(BagType bagType)
  {
    switch (bagType)
    {
      case BagType.Hero:
        BuildingController forge = CitadelSystem.inst.GetForge();
        if ((UnityEngine.Object) forge != (UnityEngine.Object) null)
          return forge.mBuildingItem.mInBuilding;
        return false;
      case BagType.DragonKnight:
        DragonKnightData dragonKnightData = PlayerData.inst.dragonKnightData;
        if (dragonKnightData != null)
          return dragonKnightData.UncollectedEquipments.Count > 0;
        break;
    }
    return false;
  }

  public bool IsForgeConstructing(BagType bagType)
  {
    if (bagType == BagType.Hero)
    {
      BuildingController forge = CitadelSystem.inst.GetForge();
      if ((UnityEngine.Object) forge != (UnityEngine.Object) null)
        return forge.mBuildingItem.mBuildingJobID > 0L;
    }
    return false;
  }

  public bool IsEnhancedToMax(BagType bagType, long itemID)
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(bagType).GetMyItemByItemID(itemID);
    return myItemByItemId.enhanced >= ConfigManager.inst.GetEquipmentProperty(bagType).GetMaxEnhanceLevel(myItemByItemId.internalId);
  }

  public Color GetColorWithQuality(int quality)
  {
    return EquipmentConst.QULITY_COLOR_MAP[quality];
  }

  public ConfigEquipmentScrollChipInfo GetSynAbleScrollChip(BagType bagType)
  {
    List<ConsumableItemData> consumableScrollChipItems = ItemBag.Instance.GetConsumableScrollChipItems(bagType);
    List<ConfigEquipmentScrollChipInfo> list = new List<ConfigEquipmentScrollChipInfo>();
    for (int index = 0; index < consumableScrollChipItems.Count; ++index)
    {
      ConfigEquipmentScrollChipInfo dataByItemId = ConfigManager.inst.GetEquipmentScrollChip(bagType).GetDataByItemID(consumableScrollChipItems[index].internalId);
      list.Add(dataByItemId);
    }
    this.Sort(list);
    for (int index = 0; index < list.Count; ++index)
    {
      int itemCount = ItemBag.Instance.GetItemCount(list[index].itemID);
      int combineReqValue = list[index].combineReqValue;
      if (!this.noticedScrollChipItemID.Contains(list[index].itemID) && combineReqValue <= itemCount)
        return list[index];
    }
    return (ConfigEquipmentScrollChipInfo) null;
  }

  public void AddNoticedScrollChipItemID(int itemID)
  {
    if (this.noticedScrollChipItemID.Contains(itemID))
      return;
    this.noticedScrollChipItemID.Add(itemID);
  }

  private void Sort(List<ConfigEquipmentScrollChipInfo> list)
  {
    list.Sort((Comparison<ConfigEquipmentScrollChipInfo>) ((x, y) =>
    {
      if (x.quality == y.quality)
        return y.level.CompareTo(x.level);
      return y.quality.CompareTo(x.quality);
    }));
  }
}
