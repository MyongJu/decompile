﻿// Decompiled with JetBrains decompiler
// Type: DefaultAdaptor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DefaultAdaptor : IPlatformAdaptor
{
  public virtual int GetFrameRate()
  {
    return 30;
  }

  public virtual void OnApplicationStart()
  {
    Application.targetFrameRate = this.GetFrameRate();
    Application.runInBackground = true;
  }

  public virtual void OnGameStart()
  {
  }

  public virtual void OnGameRestart()
  {
  }

  public virtual void OnApplicationQuit()
  {
  }
}
