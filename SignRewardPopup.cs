﻿// Decompiled with JetBrains decompiler
// Type: SignRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SignRewardPopup : Popup
{
  private List<SignRewardItemRenderer> m_ItemList = new List<SignRewardItemRenderer>();
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  private List<DropMainData> m_Items;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Items = (orgParam as SignRewardPopup.Parameter).items;
    this.m_Items.Sort(new Comparison<DropMainData>(this.OnCustomSort));
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void UpdateUI()
  {
    this.ClearData();
    int count = this.m_Items.Count;
    for (int index = 0; index < count; ++index)
    {
      DropMainData dropMainData = this.m_Items[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      SignRewardItemRenderer component = gameObject.GetComponent<SignRewardItemRenderer>();
      component.SetData(dropMainData);
      this.m_ItemList.Add(component);
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private int OnCustomSort(DropMainData x, DropMainData y)
  {
    return int.Parse(x.ID) - int.Parse(y.ID);
  }

  private void ClearData()
  {
    int count = this.m_ItemList.Count;
    for (int index = 0; index < count; ++index)
    {
      SignRewardItemRenderer rewardItemRenderer = this.m_ItemList[index];
      rewardItemRenderer.gameObject.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) rewardItemRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  public void OnClosePopupPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<DropMainData> items;
  }
}
