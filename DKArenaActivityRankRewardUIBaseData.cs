﻿// Decompiled with JetBrains decompiler
// Type: DKArenaActivityRankRewardUIBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DKArenaActivityRankRewardUIBaseData : RoundActivityRankRewardUIBaseData
{
  private DKArenaRankReward _rewardInfo;

  private DKArenaRankReward RewardInfo
  {
    get
    {
      if (this._rewardInfo == null)
        this._rewardInfo = this.Data as DKArenaRankReward;
      return this._rewardInfo;
    }
  }

  public override int MinRank
  {
    get
    {
      return this.RewardInfo.RankMin;
    }
  }

  public override int MaxRank
  {
    get
    {
      return this.RewardInfo.RankMax;
    }
  }

  public override Dictionary<int, int> CurrentRewardItems
  {
    get
    {
      Dictionary<int, int> souces = new Dictionary<int, int>();
      List<Reward.RewardsValuePair> rewards = this.RewardInfo.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
        this.AddDict(souces, rewards[index].internalID, rewards[index].value);
      return souces;
    }
  }

  public override List<int> CurrentItems
  {
    get
    {
      List<int> souces = new List<int>();
      List<Reward.RewardsValuePair> rewards = this.RewardInfo.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
        this.AddList(souces, rewards[index].internalID);
      return souces;
    }
  }
}
