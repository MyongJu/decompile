﻿// Decompiled with JetBrains decompiler
// Type: PVPKingdomBossBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPKingdomBossBanner : SpriteBanner
{
  [SerializeField]
  protected TextMesh m_bossName;
  [SerializeField]
  protected TextMesh m_bossLevel;
  [SerializeField]
  protected UISpriteMesh m_circle;
  [SerializeField]
  protected TextMesh m_progressDesc;
  [SerializeField]
  protected UISpriteMeshProgressBar m_troopProgressBar;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    KingdomBossData kingdomBossData = tile.KingdomBossData;
    if (kingdomBossData == null)
      return;
    KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem(kingdomBossData.configId);
    if (kingdomBossStaticInfo != null)
    {
      this.m_bossName.text = kingdomBossStaticInfo.LocalName;
      this.m_bossLevel.text = kingdomBossStaticInfo.BossLevel.ToString();
    }
    this.m_progressDesc.text = string.Format("{0:0.00%}", (object) (float) ((double) kingdomBossData.CurrentTroopCount / (double) kingdomBossData.StartTroopCount));
    this.m_troopProgressBar.SetProgress((float) kingdomBossData.CurrentTroopCount / (float) kingdomBossData.StartTroopCount);
  }
}
