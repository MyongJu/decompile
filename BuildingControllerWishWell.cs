﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerWishWell
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingControllerWishWell : BuildingControllerNew
{
  private const string WISH_WELL_PATH = "Prefab/City/CollectionWishUI";
  private GameObject m_Template;
  private GameObject m_WishButton;
  private int m_CurrentLevel;

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.m_Template = AssetManager.Instance.HandyLoad("Prefab/City/CollectionWishUI", (System.Type) null) as GameObject;
    WishWellPayload.Instance.onWishWellDataUpdated += new System.Action(this.OnWishWellDataUpdated);
    this.m_CurrentLevel = CityManager.inst.GetHighestBuildingLevelFor("wish_well");
    this.RequestWishWellData();
  }

  public override void Dispose()
  {
    base.Dispose();
    WishWellPayload.Instance.onWishWellDataUpdated -= new System.Action(this.OnWishWellDataUpdated);
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["wish"]);
  }

  private void OnWishWellDataUpdated()
  {
    if (WishWellPayload.Instance.gems > 0)
    {
      if ((bool) ((UnityEngine.Object) this.m_WishButton))
        return;
      this.m_WishButton = UnityEngine.Object.Instantiate<GameObject>(this.m_Template);
      this.m_WishButton.transform.parent = this.transform;
      this.m_WishButton.transform.localPosition = new Vector3(0.0f, 50f, -10f);
      this.m_WishButton.transform.localScale = new Vector3(1.6f, 1.6f, 1.6f);
    }
    else
    {
      if (!(bool) ((UnityEngine.Object) this.m_WishButton))
        return;
      this.m_WishButton.SetActive(false);
      this.m_WishButton.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.m_WishButton);
      this.m_WishButton = (GameObject) null;
    }
  }

  private void OnHandlePayload(bool ret, object data)
  {
    if (!ret)
      return;
    WishWellPayload.Instance.Decode(data);
  }

  private void RequestWishWellData()
  {
    if (CityManager.inst.GetHighestBuildingLevelFor("wish_well") <= 0)
      return;
    MessageHub.inst.GetPortByAction("Wishing:loadData").SendLoader((Hashtable) null, new System.Action<bool, object>(this.OnHandlePayload), true, false);
  }

  public override void OnChanged()
  {
    base.OnChanged();
    int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor("wish_well");
    if (this.m_CurrentLevel == buildingLevelFor)
      return;
    this.m_CurrentLevel = buildingLevelFor;
    this.RequestWishWellData();
  }
}
