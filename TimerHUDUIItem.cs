﻿// Decompiled with JetBrains decompiler
// Type: TimerHUDUIItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class TimerHUDUIItem : MonoBehaviour
{
  public string mIPAddrLink = string.Empty;
  public string mTargetName = string.Empty;
  public string mActionName = string.Empty;
  public TimerType mTimerType = TimerType.TIMER_CONSTRUCT;
  public long mJobID = -1;
  public bool mFreeTimeAllowed = true;
  public float totalProgressWidth = 766f;
  public UILabel mTimeLabel;
  public UILabel mSpeedUpDescription;
  public UILabel mHelpDescription;
  public UILabel mFreeDescription;
  public UISprite mIcon;
  public UISprite mTimerBar;
  public UIProgressBar mProgressBar;
  public GameObject mSpeedUpBar;
  public GameObject mHelpBar;
  public GameObject mFreeBar;
  public double mRealtimeRemaining;
  public double mSecondsRemaining;
  public double mTotalSeconds;
  public int mTargetLevel;
  private float _displayTimer;
  private bool _showAction;
  private bool _ready;
  private TimerHUDUIItem.UpdateTimerHUDUIItem _updater;
  public TimerHUDUIItem.UpdateTimerHUDUIItem frameUpdater;
  public TimerHUDUIItem.UpdateTimerHUDUIItem onFinished;
  public TimerHUDUIItem.UpdateTimerHUDUIItem onCancelled;
  public double mFreeTimeSecs;
  private bool isShowRemain;
  private ColorMultiplier _colorMultiplier;
  public UIButton speedUpBt;

  public void SetDetails(string targetName, string IPAddrLink, TimerType timerType, double secsRemaining, double totalSeconds, int targetLevel, TimerHUDUIItem.UpdateTimerHUDUIItem updater, TimerHUDUIItem.UpdateTimerHUDUIItem frameUpdater, bool isShowRemain = false)
  {
    this.isShowRemain = isShowRemain;
    this.mIPAddrLink = IPAddrLink;
    this.mTimerType = timerType;
    this.mRealtimeRemaining = secsRemaining;
    this.mSecondsRemaining = secsRemaining;
    this.mTotalSeconds = totalSeconds;
    this.mTargetLevel = targetLevel;
    this._updater = updater;
    this.frameUpdater = frameUpdater;
    if (targetName != string.Empty)
      this.mTargetName = ScriptLocalization.Get(targetName, true);
    switch (timerType)
    {
      case TimerType.TIMER_CONSTRUCT:
        this.mActionName = "BUILDING";
        break;
      case TimerType.TIMER_UPGRADE:
        this.mActionName = "UPGRADING";
        break;
      case TimerType.TIMER_DESTRUCT:
        this.mActionName = "DESTROYING";
        break;
      case TimerType.TIMER_TRAIN:
        this.mActionName = Utils.XLAT("time_bar_uppercase_training");
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_UPGRADE_TROOPS:
        this.mActionName = Utils.XLAT("id_upgrading");
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_TRAP_BUILD:
        this.mActionName = Utils.XLAT("time_bar_uppercase_trap_building");
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_MARCH:
        this.mActionName = "MARCHING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_ENCAMPING:
        this.mActionName = "ENCAMPING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_GATHER:
        this.mActionName = "GATHERING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_QUEST:
        this.mActionName = "QUEST";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_PVE:
        this.mActionName = "MARCHING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_PVPMarch:
        this.mActionName = "MARCHING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_RESEARCH:
        this.mActionName = Utils.XLAT("time_bar_researching");
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_PEACE_SHIELD:
        this.mActionName = "PEACE_SHIELD";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_ANTI_SCOUT:
        this.mActionName = "ANTI_SCOUT";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_HEAL:
        this.mActionName = "HEALING";
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_FORGE:
        this.mActionName = Utils.XLAT("time_bar_forging");
        this.mFreeTimeAllowed = false;
        break;
      case TimerType.TIMER_DK_FORGE:
        this.mActionName = Utils.XLAT("time_bar_forging");
        this.mFreeTimeAllowed = false;
        break;
    }
    if ((UnityEngine.Object) this.mProgressBar != (UnityEngine.Object) null)
      this.mProgressBar.value = 0.0f;
    else
      this.mTimerBar.width = 0;
    if ((UnityEngine.Object) this.mSpeedUpDescription != (UnityEngine.Object) null)
      this.mSpeedUpDescription.text = string.Empty;
    if ((UnityEngine.Object) this.mHelpDescription != (UnityEngine.Object) null)
      this.mHelpDescription.text = string.Empty;
    if ((UnityEngine.Object) this.mFreeDescription != (UnityEngine.Object) null)
      this.mFreeDescription.text = string.Empty;
    if ((UnityEngine.Object) this.mTimeLabel != (UnityEngine.Object) null)
      this.mTimeLabel.text = string.Empty;
    this._displayTimer = 2f;
    this.EstablishFreeTimeSecs();
    this._colorMultiplier = this.gameObject.GetComponent<ColorMultiplier>();
    this._ready = true;
    if (!((UnityEngine.Object) this.speedUpBt != (UnityEngine.Object) null))
      return;
    this.speedUpBt.isEnabled = !isShowRemain;
  }

  public void Destroy()
  {
    this.Stop();
    UnityEngine.Object.DestroyObject((UnityEngine.Object) this.gameObject);
  }

  public void Resume()
  {
    this._ready = true;
  }

  public void Stop()
  {
    this.StopCoroutine("SwitchMessage");
    this._ready = false;
  }

  public void KillCR()
  {
    this.StopCoroutine("SwitchMessage");
  }

  public void OnUpdate(int serverTime)
  {
    if (!this._ready || !this.gameObject.activeInHierarchy)
      return;
    this.EstablishFreeTimeSecs();
    JobHandle job = JobManager.Instance.GetJob(this.mJobID);
    this.mSecondsRemaining = job != null ? (double) job.LeftTime() : 0.0;
    if (this.mFreeTimeAllowed && this.mSecondsRemaining < this.mFreeTimeSecs)
    {
      this.mFreeBar.SetActive(true);
      this.mSpeedUpBar.SetActive(false);
      this.mHelpBar.SetActive(false);
      this.mTimerBar = this.mFreeBar.GetComponent<UISprite>();
    }
    else
    {
      JobEvent jobEvent = TimerHUDUIItem.GetJobEvent(this.mTimerType);
      if (job != null && TimerHUDUIItem.CanHelp(jobEvent))
      {
        if ((UnityEngine.Object) this.mFreeBar != (UnityEngine.Object) null)
          this.mFreeBar.SetActive(false);
        if ((UnityEngine.Object) this.mHelpBar != (UnityEngine.Object) null)
        {
          this.mHelpBar.SetActive(true);
          this.mTimerBar = this.mHelpBar.GetComponent<UISprite>();
        }
        if ((UnityEngine.Object) this.mSpeedUpBar != (UnityEngine.Object) null)
          this.mSpeedUpBar.SetActive(false);
      }
      else
      {
        if ((UnityEngine.Object) this.mFreeBar != (UnityEngine.Object) null)
          this.mFreeBar.SetActive(false);
        if ((UnityEngine.Object) this.mHelpBar != (UnityEngine.Object) null)
          this.mHelpBar.SetActive(false);
        this.mSpeedUpBar.SetActive(true);
        this.mTimerBar = this.mSpeedUpBar.GetComponent<UISprite>();
      }
    }
    if (this.mSecondsRemaining < 0.0)
      this.mSecondsRemaining = 0.0;
    float num = ((float) this.mTotalSeconds - (float) this.mSecondsRemaining) / (float) this.mTotalSeconds;
    if (this.isShowRemain)
      num = 1f - num;
    if ((UnityEngine.Object) this.mProgressBar != (UnityEngine.Object) null)
      this.mProgressBar.value = num;
    else
      this.mTimerBar.width = 20 + (int) ((double) this.totalProgressWidth * (double) num);
    this.mTimeLabel.text = Utils.ConvertSecsToString(this.mSecondsRemaining);
    ++this._displayTimer;
    if ((double) this._displayTimer > 2.0)
    {
      if ((UnityEngine.Object) this._colorMultiplier != (UnityEngine.Object) null && (double) this._colorMultiplier.currentValue == 1.0)
      {
        if (this._showAction)
        {
          this._showAction = false;
          this.StartCoroutine(this.SwitchMessage(this.mActionName));
        }
        else
        {
          this._showAction = true;
          if (this._updater != null)
            this._updater(this);
          this.StartCoroutine(this.SwitchMessage(this.mTargetName));
        }
      }
      this._displayTimer -= 2f;
    }
    --this.mRealtimeRemaining;
    this.mRealtimeRemaining = this.mRealtimeRemaining >= 0.0 ? this.mRealtimeRemaining : 0.0;
    if (this.frameUpdater == null)
      return;
    this.frameUpdater(this);
  }

  private static bool CanHelp(JobEvent jobEvent)
  {
    if (jobEvent == JobEvent.JOB_UNKNOWN || PlayerData.inst.allianceId <= 0L)
      return false;
    long allianceId = PlayerData.inst.allianceId;
    long uid = PlayerData.inst.uid;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(allianceId);
    Dictionary<long, JobHandle>.Enumerator enumerator = JobManager.Instance.GetJobDictByClass(jobEvent).GetEnumerator();
    bool flag = false;
    while (enumerator.MoveNext())
    {
      long key = enumerator.Current.Key;
      if (allianceData.helps.Get(allianceId, uid, key) != null)
      {
        flag = true;
        break;
      }
    }
    return !flag;
  }

  private static JobEvent GetJobEvent(TimerType type)
  {
    switch (type)
    {
      case TimerType.TIMER_CONSTRUCT:
      case TimerType.TIMER_UPGRADE:
        return JobEvent.JOB_BUILDING_CONSTRUCTION;
      case TimerType.TIMER_DESTRUCT:
        return JobEvent.JOB_BUILDING_DECONSTRUCTION;
      case TimerType.TIMER_RESEARCH:
        return JobEvent.JOB_RESEARCH;
      case TimerType.TIMER_HEAL:
        return JobEvent.JOB_HEALING_TROOPS;
      default:
        return JobEvent.JOB_UNKNOWN;
    }
  }

  [DebuggerHidden]
  public IEnumerator SwitchMessage(string toMsg)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TimerHUDUIItem.\u003CSwitchMessage\u003Ec__Iterator9D()
    {
      toMsg = toMsg,
      \u003C\u0024\u003EtoMsg = toMsg,
      \u003C\u003Ef__this = this
    };
  }

  public void OnSpeedUpBtnPressed()
  {
    if (this.mTimerType == TimerType.TIMER_MARCH || this.mTimerType == TimerType.TIMER_PVPMarch || this.mTimerType == TimerType.TIMER_PVE)
      this.SelectUseItemByType(ItemBag.ItemType.marchspeed, this);
    else
      this.SelectUseItemByType(ItemBag.ItemType.speedup, this);
  }

  private void SelectUseItemByType(ItemBag.ItemType tp, TimerHUDUIItem timerItem)
  {
    ItemBag.ItemType itemType = tp;
    switch (itemType)
    {
      case ItemBag.ItemType.speedup:
        UIManager.inst.OpenPopup("SpeedUpPopup", (Popup.PopupParameter) new SpeedUpPopUp.Parameter()
        {
          jobId = timerItem.mJobID,
          speedupType = ItemBag.ItemType.speedup,
          timerType = timerItem.mTimerType
        });
        break;
      case ItemBag.ItemType.marchspeed:
        Hashtable data = JobManager.Instance.GetJob(timerItem.mJobID).Data as Hashtable;
        MarchSpeedUpPopup.Parameter parameter = new MarchSpeedUpPopup.Parameter();
        long id = long.Parse(data[(object) "marchId"].ToString());
        parameter.marchData = DBManager.inst.DB_March.Get(id);
        UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) parameter);
        break;
      default:
        switch (itemType - 12)
        {
          case ItemBag.ItemType.Invalid:
          case ItemBag.ItemType.refresh:
          case ItemBag.ItemType.speedup:
          case ItemBag.ItemType.heroxp:
            UIManager.inst.OpenDlg("ItemStore/ItemStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
            return;
          default:
            return;
        }
    }
  }

  public void OnFreeBtnPressed()
  {
    this.OnFree();
  }

  public void OnFree()
  {
    ItemBag.Instance.UseFreeSpeedup(this.mJobID, (System.Action<bool, object>) null);
  }

  public void OnHelpBtnPressed()
  {
    JobEvent jobEvent = TimerHUDUIItem.GetJobEvent(this.mTimerType);
    switch (jobEvent)
    {
      case JobEvent.JOB_BUILDING_CONSTRUCTION:
      case JobEvent.JOB_BUILDING_DECONSTRUCTION:
      case JobEvent.JOB_RESEARCH:
        AllianceManager.Instance.AskHelp(this.mJobID, new System.Action<bool, object>(this.HelpRequestCallback));
        break;
      default:
        if (jobEvent != JobEvent.JOB_HEALING_TROOPS)
          break;
        goto case JobEvent.JOB_BUILDING_CONSTRUCTION;
    }
  }

  private void HelpRequestCallback(bool ret, object data)
  {
    if (ret)
    {
      this.mHelpBar.SetActive(false);
      this.mSpeedUpBar.SetActive(true);
      UIManager.inst.toast.Show("Help request sent", (System.Action) null, 4f, false);
    }
    else
      UIManager.inst.toast.Show("Help request failed", (System.Action) null, 4f, false);
  }

  public TimerHUDUIItem.SpeedupTypes GetSpeedupState()
  {
    if ((UnityEngine.Object) this.mFreeBar != (UnityEngine.Object) null && this.mFreeBar.activeSelf)
      return TimerHUDUIItem.SpeedupTypes.SPEEDUP_FREE;
    if ((UnityEngine.Object) this.mHelpBar != (UnityEngine.Object) null && this.mHelpBar.activeSelf)
      return TimerHUDUIItem.SpeedupTypes.SPEEDUP_HELP;
    return (UnityEngine.Object) this.mSpeedUpBar != (UnityEngine.Object) null && this.mSpeedUpBar.activeSelf ? TimerHUDUIItem.SpeedupTypes.SPEEDUP_SPEEDUP : TimerHUDUIItem.SpeedupTypes.SPEEDUP_NONE;
  }

  public void PushSpeedupBtn()
  {
    switch (this.GetSpeedupState())
    {
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_FREE:
        this.OnFreeBtnPressed();
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_HELP:
        this.OnHelpBtnPressed();
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_SPEEDUP:
        this.OnSpeedUpBtnPressed();
        break;
    }
  }

  public void CancelTimer()
  {
    JobManager.Instance.RemoveJob(this.mJobID);
    if (this.onCancelled == null)
      return;
    this.onCancelled(this);
  }

  public void GetSpriteData(out string fillerName, out string normal, out string hover, out string pressed, out string btnText)
  {
    fillerName = string.Empty;
    normal = string.Empty;
    hover = string.Empty;
    pressed = string.Empty;
    btnText = string.Empty;
    switch (this.GetSpeedupState())
    {
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_FREE:
        fillerName = this.mTimerBar.spriteName;
        normal = this.mFreeBar.GetComponentInChildren<UIButton>().normalSprite;
        hover = this.mFreeBar.GetComponentInChildren<UIButton>().hoverSprite;
        pressed = this.mFreeBar.GetComponentInChildren<UIButton>().pressedSprite;
        btnText = ScriptLocalization.Get("FREE", true);
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_HELP:
        fillerName = this.mTimerBar.spriteName;
        normal = this.mHelpBar.GetComponentInChildren<UIButton>().normalSprite;
        hover = this.mHelpBar.GetComponentInChildren<UIButton>().hoverSprite;
        pressed = this.mHelpBar.GetComponentInChildren<UIButton>().pressedSprite;
        btnText = ScriptLocalization.Get("HELP", true);
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_SPEEDUP:
        fillerName = this.mTimerBar.spriteName;
        normal = this.mSpeedUpBar.GetComponentInChildren<UIButton>().normalSprite;
        hover = this.mSpeedUpBar.GetComponentInChildren<UIButton>().hoverSprite;
        pressed = this.mSpeedUpBar.GetComponentInChildren<UIButton>().pressedSprite;
        break;
    }
  }

  private void EstablishFreeTimeSecs()
  {
    this.mFreeTimeSecs = (double) Property.EffectiveValue(0.0f, Property.SPEEDUP_TIME, 0, Effect.ConditionType.all);
  }

  public enum SpeedupTypes
  {
    SPEEDUP_NONE,
    SPEEDUP_FREE,
    SPEEDUP_HELP,
    SPEEDUP_SPEEDUP,
  }

  public delegate void UpdateTimerHUDUIItem(TimerHUDUIItem timerItem);
}
