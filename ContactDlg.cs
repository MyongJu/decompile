﻿// Decompiled with JetBrains decompiler
// Type: ContactDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ContactDlg : UI.Dialog
{
  private Dictionary<long, long> onlineUserToChannel = new Dictionary<long, long>();
  private HubPort searchHub;
  [SerializeField]
  private ContactDlg.Panel panel;
  [SerializeField]
  private ContactDlg.ContactPanel contactPanel;
  [SerializeField]
  private ContactDlg.InvitePanel invitePanel;
  [SerializeField]
  private ContactDlg.PendingInvitePanel pendingInvitedPanl;
  [SerializeField]
  private ContactDlg.BlockPanel blockPanel;
  private List<long> onlineChannels;

  public void OnShow(bool showPendingInvited = false)
  {
    DBManager.inst.DB_Contacts.onDataChanged += new System.Action<long>(this.OnDatachanged);
    DBManager.inst.DB_Contacts.onDataRemoved += new System.Action(this.OnDataRemoved);
    PlayerData.inst.onlineFriendsManager.onOnlineFriendStatuesChanged += new System.Action(this.OnOnlineStatuesChanged);
    this.OpenDialog(showPendingInvited);
  }

  public void OnHide()
  {
    DBManager.inst.DB_Contacts.onDataChanged -= new System.Action<long>(this.OnDatachanged);
    DBManager.inst.DB_Contacts.onDataRemoved -= new System.Action(this.OnDataRemoved);
    PlayerData.inst.onlineFriendsManager.onOnlineFriendStatuesChanged -= new System.Action(this.OnOnlineStatuesChanged);
  }

  public void OpenDialog(bool showPendingInvited = false)
  {
    this.RefreshPanel(showPendingInvited);
    RequestManager.inst.SendRequest("Player:loadContacts", Utils.Hash((object) "uid", (object) PlayerData.inst.uid), (System.Action<bool, object>) ((result, orgData) =>
    {
      if (!result)
        return;
      this.ShowDialog();
      this.RefreshPanel(showPendingInvited);
    }), true);
  }

  public void ShowDialog()
  {
  }

  public void CloseDialog()
  {
    this.gameObject.SetActive(false);
  }

  public void OnCloseButtonClick()
  {
    this.CloseDialog();
  }

  public void OnDatachanged(long uid)
  {
    this.RefreshPanel(false);
  }

  public void OnDataRemoved()
  {
    this.RefreshPanel(false);
  }

  public void RefreshPanel(bool showPendingInvited = false)
  {
    this.contactPanel.RefreshList();
    this.invitePanel.RefreshList();
    this.pendingInvitedPanl.RefreshList();
    this.blockPanel.RefreshList();
    this.panel.listTable.repositionNow = true;
    if (!this.invitePanel.inviteExpander.isExpanded)
      this.panel.scorllView.ResetPosition();
    if (showPendingInvited)
      this.pendingInvitedPanl.pendingInviteExpander.isExpanded = true;
    this.CheckOnlineState();
  }

  public void OnSearchValueChanged()
  {
    this.invitePanel.FreshSearchButtonState();
  }

  public void OnSearchUserClick()
  {
    if (this.searchHub == null)
      this.searchHub = MessageHub.inst.GetPortByAction("Player:searchUser");
    this.searchHub.SendRequest(Utils.Hash((object) "name", (object) this.invitePanel.input.value), new System.Action<bool, object>(this.invitePanel.SearchUserCallback), true);
  }

  public void OnOnlineStatuesChanged()
  {
    this.contactPanel.FreshOnlineStatues();
  }

  public void ShowContactManager(long uid)
  {
    this.panel.contactManager.Show(uid);
  }

  private void CheckOnlineState()
  {
    List<long> friends = DBManager.inst.DB_Contacts.friends;
    List<long> uid = new List<long>();
    this.onlineUserToChannel.Clear();
    for (int index = 0; index < friends.Count; ++index)
    {
      UserData userData = DBManager.inst.DB_User.Get(friends[index]);
      if (userData != null)
      {
        this.onlineUserToChannel.Add(userData.uid, userData.channelId);
        uid.Add(userData.channelId);
      }
    }
    if (uid.Count <= 0)
      return;
    PushManager.inst.GetOnlineUsers(uid, new System.Action<List<long>>(this.GetOnlineUsersCallback));
  }

  private void GetOnlineUsersCallback(List<long> result)
  {
    this.onlineChannels = result;
  }

  private void Update()
  {
    if (this.onlineChannels == null)
      return;
    this.contactPanel.RefreshOnlineState(this.onlineChannels, this.onlineUserToChannel);
    this.onlineChannels = (List<long>) null;
  }

  [Serializable]
  protected class Panel
  {
    public UIScrollView scorllView;
    public UITable listTable;
    public ContactManagerContactPopup contactManager;
  }

  [Serializable]
  protected class ContactPanel
  {
    private List<ContactDetailBar> contactBarList = new List<ContactDetailBar>();
    public ContactDlg contactDlg;
    public Transform contactList;
    public ContactExpendTool listExpander;
    public UITable listTable;
    public ContactDetailBar detailBarOrg;
    public UILabel contactNumber;

    public void RefreshList()
    {
      List<long> friends = DBManager.inst.DB_Contacts.friends;
      if (this.contactBarList.Count < friends.Count)
      {
        int height = this.detailBarOrg.GetComponent<UIWidget>().height;
        for (int count = this.contactBarList.Count; count < friends.Count; ++count)
        {
          GameObject gameObject = NGUITools.AddChild(this.listTable.gameObject, this.detailBarOrg.gameObject);
          gameObject.SetActive(true);
          gameObject.name = string.Format("contact_{0}", (object) (100 + count));
          gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(height >> 1) - height * count), 0.0f);
          ContactDetailBar component = gameObject.GetComponent<ContactDetailBar>();
          component.onBarClick = new System.Action<long>(this.OnContactDetailClick);
          this.contactBarList.Add(component);
        }
      }
      for (int index = 0; index < friends.Count; ++index)
      {
        this.contactBarList[index].gameObject.SetActive(true);
        this.contactBarList[index].Setup(friends[index], PlayerData.inst.onlineFriendsManager.IsUserOnline(friends[index]));
      }
      for (int count = friends.Count; count < this.contactBarList.Count; ++count)
        this.contactBarList[count].gameObject.SetActive(false);
      this.listExpander.isExpanded = false;
      this.listExpander.isExpandEnable = friends.Count > 0;
      this.contactNumber.text = string.Format("({0}/{1})", (object) friends.Count, (object) friends.Count);
      this.listExpander.ResetExpandArea(friends.Count * this.detailBarOrg.GetComponent<UIWidget>().height);
    }

    public void RefreshOnlineState(List<long> onlineUsers, Dictionary<long, long> uidToChannels)
    {
      if (onlineUsers == null)
        return;
      for (int index = 0; index < this.contactBarList.Count; ++index)
      {
        if (this.contactBarList[index].gameObject.activeSelf && uidToChannels.ContainsKey(this.contactBarList[index].uid))
          this.contactBarList[index].isOnline = onlineUsers.Contains(uidToChannels[this.contactBarList[index].uid]);
      }
    }

    private void OnContactDetailClick(long uid)
    {
      this.contactDlg.ShowContactManager(uid);
    }

    public void FreshOnlineStatues()
    {
      for (int index = 0; index < this.contactBarList.Count; ++index)
      {
        if (this.contactBarList[index].gameObject.activeSelf)
          this.contactBarList[index].isOnline = PlayerData.inst.onlineFriendsManager.IsUserOnline(this.contactBarList[index].uid);
      }
    }

    public void FreshOnlineStatue(long uid)
    {
      for (int index = 0; index < this.contactBarList.Count; ++index)
      {
        if (this.contactBarList[index].gameObject.activeSelf && this.contactBarList[index].uid == uid)
          this.contactBarList[index].isOnline = PlayerData.inst.onlineFriendsManager.IsUserOnline(this.contactBarList[index].uid);
      }
    }
  }

  [Serializable]
  protected class InvitePanel
  {
    private List<ContactInviteBar> inviteBarList = new List<ContactInviteBar>();
    public Transform inviteList;
    public ContactExpendTool inviteExpander;
    public UITable listTable;
    public ContactInviteBar detailBarOrg;
    public UIInput input;
    public UIButton BT_search;
    public UIWidget SearchContainer;
    public UIWidget noResultContainer;

    public bool searchEnable
    {
      set
      {
        this.BT_search.isEnabled = value;
      }
    }

    public void FreshSearchButtonState()
    {
      bool flag = true;
      if (Encoding.UTF8.GetBytes(this.input.value).Length < 3)
        flag = false;
      this.BT_search.isEnabled = flag;
    }

    public void SearchUserCallback(bool result, object orgData)
    {
      if (!result)
        return;
      ArrayList data;
      if (DatabaseTools.CheckAndParseOrgData(orgData, out data))
      {
        int num = (int) ConfigManager.inst.DB_GameConfig.GetData("contact_search_limit_value").Value;
        if (this.inviteBarList.Count < data.Count)
        {
          int height = this.detailBarOrg.GetComponent<UIWidget>().height;
          for (int count = this.inviteBarList.Count; count < data.Count && count < num; ++count)
          {
            GameObject gameObject = NGUITools.AddChild(this.listTable.gameObject, this.detailBarOrg.gameObject);
            gameObject.SetActive(true);
            gameObject.name = string.Format("invited_{0}", (object) (100 + count));
            gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(height >> 1) - height * count), 0.0f);
            this.inviteBarList.Add(gameObject.GetComponent<ContactInviteBar>());
          }
          this.listTable.repositionNow = true;
        }
        if (data.Count <= 0)
          return;
        for (int index = 0; index < data.Count && index < num; ++index)
        {
          Hashtable hashtable = data[index] as Hashtable;
          string str = string.Empty;
          if (hashtable.Contains((object) "alliance_tag") && hashtable[(object) "alliance_tag"] != null && !string.IsNullOrEmpty(hashtable[(object) "alliance_tag"].ToString()))
            str = "(" + hashtable[(object) "alliance_tag"].ToString() + ")";
          int result1 = 0;
          if (hashtable.ContainsKey((object) "lord_title"))
            int.TryParse(hashtable[(object) "lord_title"].ToString(), out result1);
          this.inviteBarList[index].gameObject.SetActive(true);
          this.inviteBarList[index].Setup(long.Parse(hashtable[(object) "uid"].ToString()), "K" + hashtable[(object) "world_id"].ToString() + " - " + str + hashtable[(object) "name"].ToString(), hashtable[(object) "icon"].ToString(), hashtable[(object) "portrait"].ToString(), result1);
          this.inviteBarList[index].invitedEnable = true;
        }
        for (int count = data.Count; count < this.inviteBarList.Count; ++count)
          this.inviteBarList[count].gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(false);
        this.inviteExpander.ResetExpandArea(this.SearchContainer.height + (data.Count >= num ? num : data.Count) * this.detailBarOrg.GetComponent<UIWidget>().height);
        this.inviteExpander.isExpanded = true;
      }
      else
      {
        for (int index = 0; index < this.inviteBarList.Count; ++index)
          this.inviteBarList[index].gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(false);
        this.noResultContainer.gameObject.SetActive(true);
        this.inviteExpander.ResetExpandArea(this.SearchContainer.height + this.noResultContainer.height);
        this.inviteExpander.isExpanded = true;
      }
    }

    public void RefreshList()
    {
      this.noResultContainer.gameObject.SetActive(false);
      this.input.value = string.Empty;
      this.FreshSearchButtonState();
      for (int index = 0; index < this.inviteBarList.Count; ++index)
        this.inviteBarList[index].gameObject.SetActive(false);
      this.inviteExpander.ResetExpandArea(this.SearchContainer.height + this.noResultContainer.height);
      this.inviteExpander.isExpanded = false;
    }
  }

  [Serializable]
  protected class PendingInvitePanel
  {
    private List<ContactPendingBar> pendingBarList = new List<ContactPendingBar>();
    public Transform pendingInviteList;
    public ContactExpendTool pendingInviteExpander;
    public UITable listTable;
    public ContactPendingBar detailBarOrg;
    public UILabel pendingNumber;

    public void RefreshList()
    {
      List<long> beInvited = DBManager.inst.DB_Contacts.beInvited;
      this.pendingNumber.text = string.Format("({0})", (object) beInvited.Count);
      if (beInvited.Count > 0)
      {
        this.pendingInviteExpander.isExpandEnable = true;
        if (beInvited.Count > this.pendingBarList.Count)
        {
          int height = this.detailBarOrg.GetComponent<UIWidget>().height;
          for (int count = this.pendingBarList.Count; count < beInvited.Count; ++count)
          {
            GameObject gameObject = NGUITools.AddChild(this.listTable.gameObject, this.detailBarOrg.gameObject);
            gameObject.SetActive(true);
            gameObject.name = string.Format("pending_{0}", (object) (100 + count));
            gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(height >> 1) - height * count), 0.0f);
            this.pendingBarList.Add(gameObject.GetComponent<ContactPendingBar>());
          }
        }
        for (int index = 0; index < beInvited.Count; ++index)
        {
          this.pendingBarList[index].gameObject.SetActive(true);
          this.pendingBarList[index].Setup(beInvited[index]);
        }
        for (int count = beInvited.Count; count < this.pendingBarList.Count; ++count)
          this.pendingBarList[count].gameObject.SetActive(false);
        this.pendingInviteExpander.ResetExpandArea(beInvited.Count * this.detailBarOrg.GetComponent<UIWidget>().height);
      }
      else
      {
        this.pendingInviteExpander.isExpandEnable = false;
        for (int index = 0; index < this.pendingBarList.Count; ++index)
          this.pendingBarList[index].gameObject.SetActive(false);
        this.pendingInviteExpander.ResetExpandArea(0);
      }
    }
  }

  [Serializable]
  protected class BlockPanel
  {
    private List<ContactBlockBar> blockBarList = new List<ContactBlockBar>();
    public Transform blockList;
    public ContactExpendTool blockExpander;
    public UITable listTable;
    public ContactBlockBar detailBarOrg;
    public UILabel blockNumber;

    public void RefreshList()
    {
      List<long> blocking = DBManager.inst.DB_Contacts.blocking;
      this.blockNumber.text = string.Format("({0})", (object) blocking.Count);
      if (blocking.Count > 0)
      {
        this.blockExpander.isExpandEnable = true;
        if (blocking.Count > this.blockBarList.Count)
        {
          int height = this.detailBarOrg.GetComponent<UIWidget>().height;
          for (int count = this.blockBarList.Count; count < blocking.Count; ++count)
          {
            GameObject gameObject = NGUITools.AddChild(this.listTable.gameObject, this.detailBarOrg.gameObject);
            gameObject.SetActive(true);
            gameObject.name = string.Format("pending_{0}", (object) (100 + count));
            gameObject.transform.localPosition = new Vector3(0.0f, (float) (-(height >> 1) - height * count), 0.0f);
            this.blockBarList.Add(gameObject.GetComponent<ContactBlockBar>());
          }
        }
        for (int index = 0; index < blocking.Count; ++index)
        {
          this.blockBarList[index].gameObject.SetActive(true);
          this.blockBarList[index].Setup(blocking[index]);
        }
        for (int count = blocking.Count; count < this.blockBarList.Count; ++count)
          this.blockBarList[count].gameObject.SetActive(false);
        this.blockExpander.ResetExpandArea(blocking.Count * this.detailBarOrg.GetComponent<UIWidget>().height);
      }
      else
      {
        this.blockExpander.isExpandEnable = false;
        for (int index = 0; index < this.blockBarList.Count; ++index)
          this.blockBarList[index].gameObject.SetActive(false);
        this.blockExpander.ResetExpandArea(0);
      }
    }
  }
}
