﻿// Decompiled with JetBrains decompiler
// Type: PublicHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PublicHUD : Hud
{
  private static string[] m_VFXPaths = new string[1]
  {
    "Prefab/VFX/fx_power2"
  };
  public PlayerResourceData mPlayerResourceData = new PlayerResourceData();
  public string Separate_Line_0 = "------------------------------------------------------------------------";
  [Header("City part")]
  public PublicHUD.CityButtons mCityButtons = new PublicHUD.CityButtons();
  public Color mColorWaitForStart = NGUIText.ParseColor32("FD8900FF", 0);
  public Color mColorWaitForEnd = NGUIText.ParseColor32("43E024FF", 0);
  public string Separate_Line_1 = "-----------------------------------------------------------------------";
  public string Separate_Line_2 = "-----------------------------------------------------------------------";
  public string Separate_Line_3 = "------------below for goto function--------------------------------";
  public string Separate_Line_4 = "------------ below for build queue --------------------------------";
  private bool HUDUIVisiable = true;
  private bool KingdomShow = true;
  private Vector3 pos = new Vector3(0.0f, -170f, 0.0f);
  private long[] m_VFXHandles = new long[1]{ -1L };
  private int _previousCenterTipCount = -1;
  protected bool _centerTipLayoutDirty = true;
  public const float HudHideOffset = 100000f;
  private const string POWER_UP = "Prefab/VFX/fx_power2";
  private const int TipMask_PersonalChest = 1;
  private const int TipMask_AllianceChest = 2;
  private bool isShow;
  [Header("Public part")]
  public UIPanel panelHUD;
  public UIWidget resourceContainer;
  public HUDResource mSilver;
  public HUDResource mFood;
  public HUDResource mWood;
  public HUDResource mOre;
  public UILabel mGoldValue;
  public UILabel mMailCount;
  public GameObject mAllianceBadge;
  public UILabel mAllianceBadgeCount;
  public GameObject mItemsBadge;
  public UILabel mItemsBadgeCount;
  public GameObject mMailBadge;
  public UILabel mMailBadgeCount;
  public UILabel mVIPLevel;
  public GameObject VIPGameobject;
  public GameObject partHeroInfo;
  public GameObject partPublic;
  public GameObject partChat;
  public GameObject partCity;
  public GameObject partKingdom;
  public GameObject partWorld;
  public GameObject partMiniMap;
  public GameObject partHideWhenSelectTarget;
  public GameObject partShowWhenSelectTarget;
  public GameObject newActivityFlag;
  public QuestCounter counter;
  public UILabel mCityPower;
  public HeroMiniDlg heroMiniDlg;
  public ChatMiniDlg chatMiniDlg;
  public TalentMiniDlg talentMiniDlg;
  public System.Action onBuildPressed;
  public System.Action onWorldMapBtnPressed;
  public System.Action onInfoBtnPressed;
  public System.Action onTrainBtnPressed;
  public System.Action onBarracksResearchBtnPressed;
  public System.Action onUpgradeBtnPressed;
  public System.Action onBlacksmithBtnPressed;
  public System.Action onGamblingBtnPressed;
  public System.Action onResearchBtnPressed;
  public System.Action onHospitalBtnPressed;
  public System.Action onRallyBtnPressed;
  public System.Action onReinforceBtnPressed;
  public System.Action onTradeBtnPressed;
  public System.Action onEnhanceBtnPressed;
  public UIButton mInfoBtn;
  public UIButton mTrainBtn;
  public UIButton mUpgradeBtn;
  public UIButton mCampainBtn;
  public UIButton mBlacksmithBtn;
  public UIButton mInventoryBtn;
  public UIButton mResearchBtn;
  public UITable mButtonContainer;
  public GameObject mTempleSkillButton;
  public GameObject mHallOfKingButton;
  public GameObject mQuickSearchButton;
  public GameObject mPitRankButton;
  public GameObject mPitGoBackButton;
  public GameObject mAllianceWarBriefButton;
  public UILabel mLabelAllianceWarTime;
  [Header("Kingdom part")]
  public UILabel PitLeftTime;
  public UILabel CurrentX;
  public UILabel CurrentY;
  public UIButton HideButton;
  public GameObject CoordinatesUI;
  public UIButton CityBtn;
  public WatchtowerAlarm WatchtowerBtn;
  public DistanceMarker CityLocationMarker;
  public GameObject KingdomSpin;
  public GameObject BackToMyCity;
  public System.Action onKingdomViewBtnPressed;
  public System.Action onMarchesBtnPressed;
  public System.Action onEventsBtnPressed;
  public System.Action onHelpBtnPressed;
  public System.Action onBookmarkBtnPressed;
  public System.Action onSearchBtnPressed;
  public System.Action onWorldSearchBtnPressed;
  public System.Action onQuickSearchBtnPressed;
  [Header("Mini map part")]
  public UIToggle AllianceToggle;
  public UIToggle ResourceToggle;
  public UIButton KingdomButton;
  public UIButton WorldButton;
  public GameObject MiniMapLegend;
  public UILabel KingdomName;
  public UILabel KingName;
  public UILabel WonderStatus;
  public UILabel ProtectionState;
  public UILabel KingdomColonizedBy;
  public UISprite KingdomFlag;
  public AllianceWarMiniMapTrackView AllianceTrackView;
  [Header("Goto Function part")]
  public GameObject mQuestBtn;
  public GameObject mAllianceBtn;
  public GameObject mKingdomBtn;
  public GroupQuestBtn mGroupQuestBtn;
  [Header("Build queue part")]
  public BuildQueueUI buildQueueUI;
  public UITable TableCenterTip;
  public RedDotTipItem PersonalChestTipItem;
  public RedDotTipItem ChestLibraryTipItem;
  private bool _isAllianceWarClicked;
  private List<long> _trackedAllianceIdList;
  public GameObject buildingmovepanel;
  private int _previousTipMask;

  public bool IsShow
  {
    get
    {
      return this.isShow;
    }
  }

  protected override void _Show(UIControler.UIParameter orgParam)
  {
    if (this.gameObject.activeSelf)
      this.transform.localPosition = Vector3.zero;
    else
      this.gameObject.SetActive(true);
    this.isShow = true;
    this.OnShow(orgParam);
  }

  protected override void _Hide(UIControler.UIParameter orgParam)
  {
    this.isShow = false;
    this.transform.localPosition = new Vector3(-100000f, 0.0f, 0.0f);
    this.OnHide(orgParam);
  }

  public void Awake()
  {
  }

  public void Dispose()
  {
    if (!((UnityEngine.Object) this.counter != (UnityEngine.Object) null))
      return;
    this.counter.Dispose();
    this.counter = (QuestCounter) null;
  }

  private void Start()
  {
    this.InitPublic();
    this.InitCity();
    this.InitKingdom();
    NameManager.Instance.Initialize(GameEngine.Instance.nameLookupTable.text);
    IconManager.Instance.Initialize(GameEngine.Instance.iconLookupTable.text);
    this.BackToMyCity.SetActive(false);
    this.UpdateAllianceBadgeCount();
    UIManager.inst.splashScreen.onSplashScreenHide += new System.Action(this.OnSplashScreenHide);
  }

  private void OnSplashScreenHide()
  {
    this.chatMiniDlg.Init();
  }

  public override void OnCreate(UIControler.UIParameter orgParam = null)
  {
    GameEngine.Instance.events.onResourceClicked += new System.Action<CityManager.ResourceTypes>(this.OnResourceClicked);
    GameEngine.Instance.events.onItemClicked += new System.Action<ShopStaticInfo, double, double>(this.OnItemClicked);
    WatchtowerUtilities.onAdded += new System.Action<WatchEntity>(this.OnMarchAdded);
    WatchtowerUtilities.onRemoved += new System.Action<WatchEntity>(this.OnMarchRemoved);
    WatchtowerUtilities.onUpdate += new System.Action(this.OnMarchUpdate);
    AllianceManager.Instance.onInviteChanged += new System.Action(this.UpdateAllianceBadgeCount);
    AllianceManager.Instance.onHelpChanged += new System.Action(this.UpdateAllianceBadgeCount);
    AllianceManager.Instance.onJoin += new System.Action(this.OnJoinOrLeaveAlliance);
    AllianceManager.Instance.onLeave += new System.Action(this.OnJoinOrLeaveAlliance);
    AllianceManager.Instance.onPushLeave += new System.Action<object>(this.OnPushLeaveAlliance);
    AllianceUtilities.OnAllianceDevoteRewardTipsUpdate += new System.Action(this.UpdateAllianceBadgeCount);
    DBManager.inst.DB_March.onDataCreate += new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_March.onDataRemoved += new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_March.onDataUpdate += new System.Action<long>(this.OnWarUpdate);
    DBManager.inst.DB_March.onOwnerDataRemove += new System.Action<long>(this.OnMarchOwerRemove);
    DBManager.inst.DB_Rally.onRallyDataCreated += new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_Rally.onRallyDataRemoved += new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_AllianceTech.onDataChanged += new System.Action<int>(this.OnAllianceTechDataChanged);
    DBManager.inst.DB_AllianceTech.onDataRemoved += new System.Action<int>(this.OnAllianceTechDataChanged);
    DBManager.inst.DB_AllianceMagic.onDataChanged += new System.Action<AllianceMagicData>(this.OnMagicChanged);
    DBManager.inst.DB_AllianceTemple.onDataChanged += new System.Action<AllianceTempleData>(this.OnAllianceTempleChanged);
    DBManager.inst.DB_Wonder.onDataChanged += new System.Action<DB.WonderData>(this.OnWonderDataChanged);
    DBManager.inst.DB_Alliance.onDataChanged += new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    IAPStorePackagePayload.Instance.OnPurchaseSucceed += new System.Action<object>(this.OnPaymentSuccess);
    AllianceManager.Instance.onPushHelpResponse += new System.Action<object>(this.OnPushHelpResponse);
    JobManager.Instance.OnJobError += new System.Action<long, JobError>(this.OnJobError);
    MessageHub.inst.GetPortByAction("shutup").AddEvent(new System.Action<object>(this.OnShutup));
    MessageHub.inst.GetPortByAction("king:castKingSkill").AddEvent(new System.Action<object>(this.OnKingUseSkill));
    AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged += new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryVaultCountChanged);
    DBManager.inst.DB_UserTreasureChest.onDataChanged += new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
    MessageHub.inst.GetPortByAction("abyss_over").AddEvent(new System.Action<object>(this.OnAbyssOver));
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
  }

  public override void OnFinalize(UIControler.UIParameter orgParam = null)
  {
    if (GameEngine.IsAvailable && !GameEngine.IsShuttingDown)
    {
      GameEngine.Instance.events.onResourceClicked -= new System.Action<CityManager.ResourceTypes>(this.OnResourceClicked);
      GameEngine.Instance.events.onItemClicked -= new System.Action<ShopStaticInfo, double, double>(this.OnItemClicked);
    }
    UIManager.inst.splashScreen.onSplashScreenHide -= new System.Action(this.OnSplashScreenHide);
    WatchtowerUtilities.onAdded -= new System.Action<WatchEntity>(this.OnMarchAdded);
    WatchtowerUtilities.onRemoved -= new System.Action<WatchEntity>(this.OnMarchRemoved);
    WatchtowerUtilities.onUpdate -= new System.Action(this.OnMarchUpdate);
    WatchtowerUtilities.Deinit();
    AllianceManager.Instance.onInviteChanged -= new System.Action(this.UpdateAllianceBadgeCount);
    AllianceManager.Instance.onHelpChanged -= new System.Action(this.UpdateAllianceBadgeCount);
    AllianceManager.Instance.onJoin -= new System.Action(this.OnJoinOrLeaveAlliance);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnJoinOrLeaveAlliance);
    AllianceManager.Instance.onPushLeave -= new System.Action<object>(this.OnPushLeaveAlliance);
    AllianceUtilities.OnAllianceDevoteRewardTipsUpdate -= new System.Action(this.UpdateAllianceBadgeCount);
    DBManager.inst.DB_March.onDataCreate -= new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_March.onDataRemoved -= new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_March.onDataUpdate -= new System.Action<long>(this.OnWarUpdate);
    DBManager.inst.DB_March.onOwnerDataRemove -= new System.Action<long>(this.OnMarchOwerRemove);
    DBManager.inst.DB_Rally.onRallyDataCreated -= new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_Rally.onRallyDataRemoved -= new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_AllianceTech.onDataChanged -= new System.Action<int>(this.OnAllianceTechDataChanged);
    DBManager.inst.DB_AllianceTech.onDataRemoved -= new System.Action<int>(this.OnAllianceTechDataChanged);
    DBManager.inst.DB_AllianceMagic.onDataChanged -= new System.Action<AllianceMagicData>(this.OnMagicChanged);
    DBManager.inst.DB_AllianceTemple.onDataChanged -= new System.Action<AllianceTempleData>(this.OnAllianceTempleChanged);
    DBManager.inst.DB_Wonder.onDataChanged -= new System.Action<DB.WonderData>(this.OnWonderDataChanged);
    DBManager.inst.DB_Alliance.onDataChanged -= new System.Action<long>(this.OnAllianceDataChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    IAPStorePackagePayload.Instance.OnPurchaseSucceed -= new System.Action<object>(this.OnPaymentSuccess);
    AllianceManager.Instance.onPushHelpResponse -= new System.Action<object>(this.OnPushHelpResponse);
    JobManager.Instance.OnJobError -= new System.Action<long, JobError>(this.OnJobError);
    MessageHub.inst.GetPortByAction("king:castKingSkill").RemoveEvent(new System.Action<object>(this.OnKingUseSkill));
    AllianceTreasuryCountManager.Instance.OnTreasuryCountChanged -= new AllianceTreasuryCountManager.TreasuryCountChangedHandler(this.OnTreasuryVaultCountChanged);
    DBManager.inst.DB_UserTreasureChest.onDataChanged -= new System.Action<UserTreasureChestData>(this.OnUserTreasureChestDataChanged);
    MessageHub.inst.GetPortByAction("abyss_over").RemoveEvent(new System.Action<object>(this.OnAbyssOver));
    this.mGroupQuestBtn.Dispose();
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  private void OnKingUseSkill(object data)
  {
    Hashtable data1;
    if (DatabaseTools.CheckAndParseOrgData(data, out data1) && data1 != null && data1.ContainsKey((object) "errno"))
      return;
    UIManager.inst.toast.Show(ScriptLocalization.Get("toast_throne_king_skill_success", true), (System.Action) null, 4f, false);
  }

  private void OnAbyssOver(object data)
  {
    GameEngine.Instance.MarkRestartGame(GameEngine.LoadMode.Deep);
  }

  private void OnJobError(long jobID, JobError errorCode)
  {
    D.error((object) "[Job Error][RestartGameError]: job_id={0}, error_code={1}", (object) jobID, (object) errorCode);
    if (!NetWorkDetector.Instance.IsReadyRestart())
      NetWorkDetector.Instance.SendRestartGameError2RUM(NetWorkDetector.RestartErrorType.Job, "Remove", jobID.ToString() + "|" + (object) errorCode);
    NetWorkDetector.Instance.RestartGame(true);
  }

  private void OnPushHelpResponse(object orgData)
  {
    Hashtable inData = orgData as Hashtable;
    long outData = 0;
    string empty = string.Empty;
    DatabaseTools.UpdateData(inData, "name", ref empty);
    DatabaseTools.UpdateData(inData, "job_id", ref outData);
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null)
      return;
    AllianceHelpData allianceHelpData = allianceData.helps.Get(PlayerData.inst.allianceId, PlayerData.inst.uid, outData);
    if (allianceHelpData == null)
      return;
    UIManager.inst.toast.Show(this.GetHelpToastContent(empty, allianceHelpData.trace), (System.Action) null, 4f, false);
  }

  private string GetHelpToastContent(string name, Hashtable ht)
  {
    if (ht == null || !ht.ContainsKey((object) "event_type"))
    {
      Debug.LogError((object) "[PublicHUD]GetHelpToastContent: missing event_type!");
      return string.Empty;
    }
    int result1 = 0;
    int.TryParse(ht[(object) "event_type"].ToString(), out result1);
    JobEvent jobEvent = (JobEvent) result1;
    switch (jobEvent)
    {
      case JobEvent.JOB_BUILDING_CONSTRUCTION:
        int result2 = 0;
        int.TryParse(ht[(object) "building_internal_id"].ToString(), out result2);
        BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData(result2);
        return string.Format(ScriptLocalization.Get("toast_help_received_upgrade", true), (object) name, (object) (data1.Building_Lvl + 1), (object) ScriptLocalization.Get(data1.Building_LOC_ID, true));
      case JobEvent.JOB_BUILDING_DECONSTRUCTION:
        int result3 = 0;
        int.TryParse(ht[(object) "building_internal_id"].ToString(), out result3);
        BuildingInfo data2 = ConfigManager.inst.DB_Building.GetData(result3);
        return string.Format(ScriptLocalization.Get("toast_help_received_upgrade", true), (object) name, (object) data2.Building_Lvl, (object) ScriptLocalization.Get(data2.Building_LOC_ID, true));
      case JobEvent.JOB_RESEARCH:
        int result4 = 0;
        int.TryParse(ht[(object) "research_id"].ToString(), out result4);
        TechLevel techLevel = ResearchManager.inst.GetTechLevel(result4);
        return string.Format(ScriptLocalization.Get("toast_help_received_research", true), (object) name, (object) ScriptLocalization.Get(techLevel.Name, true));
      default:
        if (jobEvent == JobEvent.JOB_HEALING_TROOPS)
          return string.Format(ScriptLocalization.Get("toast_help_received_heal", true), (object) name);
        if (jobEvent != JobEvent.JOB_FORGE)
        {
          if (jobEvent != JobEvent.JOB_DK_FORGE)
            return string.Empty;
          ConfigEquipmentMainInfo data3 = ConfigManager.inst.GetEquipmentMain(BagType.DragonKnight).GetData(int.Parse(ht[(object) "equip_id"].ToString()));
          return string.Format(ScriptLocalization.Get("toast_help_received_forge", true), (object) name, (object) data3.LocName);
        }
        ConfigEquipmentMainInfo data4 = ConfigManager.inst.GetEquipmentMain(BagType.Hero).GetData(int.Parse(ht[(object) "equip_id"].ToString()));
        return string.Format(ScriptLocalization.Get("toast_help_received_forge", true), (object) name, (object) data4.LocName);
    }
  }

  private void OnWarCreate(long id)
  {
    this.UpdateAllianceBadgeCount();
  }

  private void OnWarUpdate(long id)
  {
    this.UpdateAllianceBadgeCount();
  }

  private void OnWarRemoved()
  {
    this.UpdateAllianceBadgeCount();
  }

  private void OnAllianceTechDataChanged(int researchId)
  {
    this.UpdateAllianceBadgeCount();
  }

  private void OnMagicChanged(AllianceMagicData magic)
  {
    this.UpdateAllianceBadgeCount();
  }

  private void OnMarchOwerRemove(long marchId)
  {
    MarchData marchData = DBManager.inst.DB_March.Get(marchId);
    if (marchData == null || marchData.resourcesInfo.resources == null)
      return;
    using (Dictionary<string, int>.Enumerator enumerator = marchData.resourcesInfo.resources.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        string key = current.Key;
        if (key != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (PublicHUD.\u003C\u003Ef__switch\u0024mapA3 == null)
          {
            // ISSUE: reference to a compiler-generated field
            PublicHUD.\u003C\u003Ef__switch\u0024mapA3 = new Dictionary<string, int>(4)
            {
              {
                "food",
                0
              },
              {
                "wood",
                1
              },
              {
                "ore",
                2
              },
              {
                "silver",
                3
              }
            };
          }
          int num;
          // ISSUE: reference to a compiler-generated field
          if (PublicHUD.\u003C\u003Ef__switch\u0024mapA3.TryGetValue(key, out num))
          {
            switch (num)
            {
              case 0:
                this.mFood.PlayIncreaseAnimaition(this.mFood.transform, this.pos, (long) current.Value);
                continue;
              case 1:
                this.mWood.PlayIncreaseAnimaition(this.mWood.transform, this.pos, (long) current.Value);
                continue;
              case 2:
                this.mOre.PlayIncreaseAnimaition(this.mOre.transform, this.pos, (long) current.Value);
                continue;
              case 3:
                this.mSilver.PlayIncreaseAnimaition(this.mSilver.transform, this.pos, (long) current.Value);
                continue;
              default:
                continue;
            }
          }
        }
      }
    }
  }

  private void UpdateAllianceBadgeCount()
  {
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    int num5 = 0;
    if (PlayerData.inst.allianceId > 0L && PlayerData.inst.allianceData != null)
    {
      num1 = PlayerData.inst.allianceData.helps.GetValidHelpCount();
      num2 = GameEngine.Instance.marchSystem.allianceWarCount;
      if (PlayerData.inst.isAllianceR4Member && DBManager.inst.DB_AllianceTech.readyResearchId != 0 && DBManager.inst.DB_AllianceTech.researchingId == 0)
        num4 = 1;
    }
    using (List<AllianceInvitedApplyData>.Enumerator enumerator = DBManager.inst.DB_AllianceInviteApply.GetDataByUid(PlayerData.inst.uid).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.status == AllianceInvitedApplyData.Status.invited)
          ++num3;
      }
    }
    int num6 = 0;
    if (PlayerData.inst.allianceId != 0L)
      num6 = AllianceTreasuryCountManager.Instance.UnOpenedChestCount + DBManager.inst.DB_UserTreasureChest.GetAllCanSendUserChest(PlayerData.inst.uid);
    if (AllianceUtilities.IsDevoteRewardAvailable)
      num5 = 1;
    this.SetAllianceBadgeCount(num1 + num2 + num3 + num4 + num6 + num5);
  }

  public void OnQuestTipClick()
  {
  }

  public void OnActivityBtPressed()
  {
    ActivityManager.Intance.LoadTimeLimitActivityData(new System.Action(this.LoadDataCallBack));
    OperationTrace.TraceClick(ClickArea.ActivityCenter);
  }

  private void LoadDataCallBack()
  {
    AudioManager.Instance.PlaySound("sfx_city_click_event_center", false);
    if (ActivityManager.Intance.isActivityExist)
      UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("event_not_started_description", true), (System.Action) null, 4f, false);
  }

  public void OnWallDefensePressed()
  {
    CityTouchCircle.Instance.CloseSelf();
    MessageHub.inst.GetPortByAction("City:getCityDefenseInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenDlg("Wall/WallDefense", (UI.Dialog.DialogParameter) null, true, true, true);
    }), true);
  }

  private void OnJoinOrLeaveAlliance()
  {
    this.UpdateAllianceBadgeCount();
    this.UpdateButtonVisibles();
    this.UpdateCenterTipItem();
  }

  private void OnPushLeaveAlliance(object data)
  {
    this.UpdateAllianceBadgeCount();
    this.UpdateButtonVisibles();
  }

  private void OnMarchAdded(WatchEntity entity)
  {
    this.WatchtowerBtn.UpdateEntity(WatchtowerUtilities.GetPriorityEntity());
    AudioManager.Instance.PlayBGM("bgm_city_on_fire");
  }

  private void OnMarchRemoved(WatchEntity entity)
  {
    if (!WatchtowerUtilities.IsAlive())
    {
      AudioManager.Instance.PlayBGM("bgm_city2");
      this.WatchtowerBtn.gameObject.SetActive(false);
      this.WatchtowerBtn.StopBuildingVfx();
      this.WatchtowerBtn.StopVfx();
    }
    else
      this.WatchtowerBtn.UpdateEntity(WatchtowerUtilities.GetPriorityEntity());
  }

  private void OnMarchUpdate()
  {
    if (!WatchtowerUtilities.IsAlive())
    {
      this.WatchtowerBtn.gameObject.SetActive(false);
      this.WatchtowerBtn.StopVfx();
    }
    else
      this.WatchtowerBtn.UpdateEntity(WatchtowerUtilities.GetPriorityEntity());
  }

  private void InitCity()
  {
    this.onWorldMapBtnPressed += new System.Action(CitadelSystem.inst.OnLoadWorldMap);
  }

  public void SetCityPower(long newPower)
  {
    this.mCityPower.text = Utils.FormatThousands(newPower.ToString());
  }

  public void SetVIPLevel(int level)
  {
    this.mVIPLevel.text = "VIP : " + level.ToString();
    if (!((UnityEngine.Object) this.VIPGameobject != (UnityEngine.Object) null))
      return;
    if (PlayerData.inst.hostPlayer.VIPActive)
    {
      GreyUtility.Normal(this.VIPGameobject);
      this.mVIPLevel.color = Color.yellow;
    }
    else
    {
      GreyUtility.Grey(this.VIPGameobject);
      this.mVIPLevel.color = Color.grey;
    }
  }

  public void OnVIPButtonClicked()
  {
    UIManager.inst.OpenDlg("VIP/VipBaseDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Vip);
  }

  public void OnHeroBtnPressed()
  {
    LinkerHub.Instance.DisposeHint(this.transform);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.PlayerInformation);
  }

  public void OnKingdomMapBtnPressed()
  {
    LinkerHub.Instance.DisposeHint(this.transform);
    BuildingMoveManager.Instance.ExitBuildingMove();
    List<CityManager.BuildingItem> buildingsByType = CityManager.inst.GetBuildingsByType("farm");
    bool flag = false;
    if (buildingsByType.Count < 2)
      flag = true;
    if (buildingsByType.Count == 2 && (1 > buildingsByType[0].mLevel || 1 > buildingsByType[1].mLevel))
      flag = true;
    string str = TutorialManager.Instance.Resolution != TutorialManager.ABTestType.Basic ? NewTutorial.Instance.GetRecordPoint("testb_Tutorial_quest") : NewTutorial.Instance.GetRecordPoint("Tutorial_quest");
    if (flag && "finished" != str)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_kingdom_map_blocked", true), (System.Action) null, 4f, true);
      PVPMap.PendingGotoRequest = Coordinate.zero;
      this.TipHandler();
    }
    else
    {
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        if (this.onWorldMapBtnPressed == null)
          return;
        this.onWorldMapBtnPressed();
      }));
      OperationTrace.TraceClick(ClickArea.SwichToKingdomMap);
    }
  }

  private void TipHandler()
  {
    QuestData2 questData2 = new QuestData2();
    QuestLinkHUDInfo questLinkHudInfo1 = new QuestLinkHUDInfo();
    if (DBManager.inst.DB_Quest.RecommendQuest != null)
      questData2 = DBManager.inst.DB_Quest.RecommendQuest;
    QuestLinkHUDInfo questLinkHudInfo2 = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(questData2.Info.LinkHudId);
    if (questLinkHudInfo2 == null)
      return;
    LinkerHub.Instance.Distribute(questLinkHudInfo2.Param);
  }

  private void OnHideClicked()
  {
    if (this.HUDUIVisiable)
    {
      this.HideHUD();
      this.KingdomShow = false;
    }
    else
    {
      this.ShowHUD();
      this.KingdomShow = true;
    }
  }

  public void ShowHUD()
  {
    this.HUDUIVisiable = true;
    UIHideHelperManager.Instance.ShowHudUI();
    this.HideButton.GetComponent<StateButton>().ChangeState("open");
  }

  public void HideHUD()
  {
    this.HUDUIVisiable = false;
    UIHideHelperManager.Instance.HideHudUI();
    this.HideButton.GetComponent<StateButton>().ChangeState("close");
  }

  private void QuickBackHUD()
  {
    this.HUDUIVisiable = true;
    UIHideHelperManager.Instance.BackToOriginal();
  }

  private void QuickHideHUD()
  {
    this.HUDUIVisiable = false;
    UIHideHelperManager.Instance.QuickHide();
  }

  private void InitKingdom()
  {
    EventDelegate.Add(this.HideButton.onClick, new EventDelegate.Callback(this.OnHideClicked));
    this.HideButton.GetComponent<StateButton>().ChangeState("open");
  }

  public void UpdateKingdomCoordinate(Coordinate currentLocation, Coordinate lastLocation)
  {
    this.CurrentX.text = currentLocation.X.ToString();
    this.CurrentY.text = currentLocation.Y.ToString();
    this.BackToMyCity.SetActive(currentLocation.K != 0 && currentLocation.K != PlayerData.inst.playerCityData.cityLocation.K && !AllianceWarPayload.Instance.IsInAllianceWarTeleport);
    if (SwitchKingdomPopup.IsOneShowing || currentLocation.K == 0 || (lastLocation.K == 0 || currentLocation.K == lastLocation.K))
      return;
    UIManager.inst.tileCamera.EnableDragging = false;
    ActiveKingdom kingdomById1 = PVPMapData.MapData.FindKingdomByID(lastLocation.K);
    ActiveKingdom kingdomById2 = PVPMapData.MapData.FindKingdomByID(currentLocation.K);
    if (kingdomById1 != null && kingdomById2 != null)
    {
      Vector3 curWorldPos = PVPMapData.MapData.ConvertTileKXYToWorldPosition(currentLocation);
      Vector3 direction = new Vector3((float) (kingdomById2.KingdomX - kingdomById1.KingdomX), (float) (kingdomById2.KingdomY - kingdomById1.KingdomY), 0.0f);
      direction.Normalize();
      SwitchKingdomPopup.Parameter parameter = new SwitchKingdomPopup.Parameter();
      parameter.kingdomID = currentLocation.K;
      parameter.onOkay = (System.Action) (() =>
      {
        UIManager.inst.tileCamera.SetTargetLookPosition(curWorldPos + direction * 3f, true);
        UIManager.inst.tileCamera.EnableDragging = true;
      });
      parameter.onNo = (System.Action) (() =>
      {
        PVPSystem.Instance.Map.CurrentKXY = lastLocation;
        UIManager.inst.tileCamera.SetTargetLookLocation(lastLocation, false);
        UIManager.inst.tileCamera.SetTargetLookPosition(PVPMapData.MapData.ConvertTileKXYToWorldPosition(lastLocation) - direction * 3f, true);
        UIManager.inst.tileCamera.EnableDragging = true;
      });
      if (AllianceWarPayload.Instance.IsInAllianceWarTeleport)
        return;
      UIManager.inst.OpenPopup("SwitchKingdomPopup", (Popup.PopupParameter) parameter);
    }
    else
      UIManager.inst.tileCamera.EnableDragging = true;
  }

  public void OnMarchesBtnPressed()
  {
    if (this.onMarchesBtnPressed == null)
      return;
    this.onMarchesBtnPressed();
  }

  public void OnBookmarkBtnPressed()
  {
    if (this.onBookmarkBtnPressed != null)
      this.onBookmarkBtnPressed();
    UIManager.inst.OpenPopup("Bookmark/BookmarkPopup", (Popup.PopupParameter) null);
  }

  public void OnQuickSearchBtnPressed()
  {
    if (this.onQuickSearchBtnPressed != null)
      this.onQuickSearchBtnPressed();
    UIManager.inst.OpenPopup("QuickSearchPopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.QuickSearchMonster);
  }

  public void OnSearchBtnPressed()
  {
    if (this.onSearchBtnPressed != null)
      this.onSearchBtnPressed();
    UIManager.inst.OpenDlg("popupSearchCoordinates", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.SearchCoordinate);
  }

  public void OnWorldSearchBtnPressed()
  {
    if (this.onWorldSearchBtnPressed == null)
      return;
    this.onWorldSearchBtnPressed();
  }

  public void OnEventsBtnPressed()
  {
    if (this.onEventsBtnPressed == null)
      return;
    this.onEventsBtnPressed();
  }

  public void OnHelpBtnPressed()
  {
    if (this.onHelpBtnPressed != null)
      this.onHelpBtnPressed();
    AudioManager.Instance.StopAndPlaySound("sfx_alliance_help_request");
    UIManager.inst.OpenDlg("Alliance/AllianceHelpRequestsDlg", (UI.Dialog.DialogParameter) new AllianceHelpRequestDlg.Parameter()
    {
      showBack = false
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnCityViewBtnPressed()
  {
    UIManager.inst.Cloud.CoverCloud((System.Action) (() => GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.CityMode));
    OperationTrace.TraceClick(ClickArea.SwichToCity);
  }

  public void OnWorldViewBtnPressed()
  {
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode)
    {
      UIManager.inst.OpenPopup("Pit/PitMinimapPopup", (Popup.PopupParameter) null);
    }
    else
    {
      int k = PVPSystem.Instance.Map.CurrentKXY.K;
      if (!PVPMapData.MapData.IsKingdomActive(k))
        return;
      if (PlayerData.inst.allianceId > 0L)
      {
        Hashtable postData = new Hashtable();
        postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
        MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param0, _param1) =>
        {
          MiniMapSystem.Init();
          MiniMapSystem.Instance.Open();
          PVPSystem.Instance.Hide();
        }), true);
      }
      else
      {
        MiniMapSystem.Init();
        MiniMapSystem.Instance.Open();
        PVPSystem.Instance.Hide();
      }
      OperationTrace.TraceClick(ClickArea.MiniMap);
    }
  }

  public void OnKingdomViewBtnPressed()
  {
    if (this.onKingdomViewBtnPressed == null)
      return;
    this.onKingdomViewBtnPressed();
  }

  public void OnWatchtowerBtnPressed()
  {
    UIManager.inst.OpenDlg("Watchtower/WatchtowerMarchesDialog", (UI.Dialog.DialogParameter) null, true, true, true);
    this.WatchtowerBtn.StopVfx();
  }

  private void OnAllianceTempleChanged(AllianceTempleData allianceTempleData)
  {
    this.UpdateButtonVisibles();
  }

  private void OnWonderDataChanged(DB.WonderData wonderData)
  {
    this.UpdateButtonVisibles();
  }

  private void OnAllianceDataChanged(long id)
  {
    this.UpdateCenterTipItem();
    this.UpdateAllianceBadgeCount();
  }

  private void OnUserDataChanged(long id)
  {
    if (id != PlayerData.inst.uid)
      return;
    this.UpdateCenterTipItem();
    this.UpdateAllianceBadgeCount();
    this.UpdateAllianceWarButtons();
  }

  private void UpdateAllianceWarButtons()
  {
    this.mAllianceWarBriefButton.SetActive(PlayerData.inst.userData.AcAllianceId != 0L);
    this.UpdateAllianceWarLeftTime();
  }

  private void UpdateButtonVisibles()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
    {
      this.PitLeftTime.gameObject.SetActive(false);
      if ((UnityEngine.Object) this.mTempleSkillButton != (UnityEngine.Object) null)
      {
        AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.GetMyAllianceTempleData();
        this.mTempleSkillButton.SetActive(allianceTempleData != null && allianceTempleData.CanUseSkill);
      }
      if ((UnityEngine.Object) this.mHallOfKingButton != (UnityEngine.Object) null)
      {
        DB.WorldMapData worldMapData = DBManager.inst.DB_WorldMap.Get(PlayerData.inst.userData.world_id);
        this.mHallOfKingButton.SetActive(worldMapData != null && worldMapData.State != "unOpen");
      }
      this.mQuickSearchButton.SetActive(true);
      this.mPitRankButton.SetActive(false);
      this.mPitGoBackButton.SetActive(false);
    }
    else
    {
      this.PitLeftTime.gameObject.SetActive(true);
      this.mQuickSearchButton.SetActive(false);
      this.mPitRankButton.SetActive(true);
      this.mPitGoBackButton.SetActive(true);
      this.mTempleSkillButton.SetActive(false);
      this.mHallOfKingButton.SetActive(false);
    }
    if (!(bool) ((UnityEngine.Object) this.mButtonContainer))
      return;
    this.mButtonContainer.repositionNow = true;
    this.mButtonContainer.Reposition();
  }

  public void OnTempleSkillButtonClicked()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceTempleSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.TempleSkill);
  }

  public void OnHallOfKingButtonClicked()
  {
    UIManager.inst.OpenDlg("HallOfKing/HallOfKingDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.HallOfKing);
  }

  public void OnPitRankButtonClicked()
  {
    UIManager.inst.OpenPopup("Pit/PitExploreScoreRankPopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.PitRank);
  }

  public void OnPitGoBackButtonClicked()
  {
    UIManager.inst.OpenPopup("Pit/PitGoHomePopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.PitGoBack);
  }

  private void PlayEffect(PublicHUD.VisualEffect slot, Transform parent)
  {
    this.StopCurrentEffect(slot);
    if (!((UnityEngine.Object) parent != (UnityEngine.Object) null))
      return;
    this.m_VFXHandles[(int) slot] = VfxManager.Instance.CreateAndPlay(PublicHUD.m_VFXPaths[(int) slot], parent);
  }

  private void StopCurrentEffect(PublicHUD.VisualEffect effect)
  {
    if (this.m_VFXHandles[(int) effect] == -1L)
      return;
    VfxManager.Instance.Delete(this.m_VFXHandles[(int) effect]);
    this.m_VFXHandles[(int) effect] = -1L;
  }

  private void InitPublic()
  {
    this.OnPlayerStatChanged(PlayerData.inst.uid);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnPlayerStatChanged);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.OnCityStatChanged);
    GameEngine.Instance.events.onHeroStatChanged += new System.Action<Events.HeroStatChangedArgs>(this.OnHeroStatChanged);
    GameEngine.Instance.events.onResearchStateChanged += new System.Action<Events.ResearchStateChangedEventArgs>(this.OnResearchStateChanged);
    GameEngine.Instance.events.onPlayerStatChanged += (System.Action<Events.PlayerStatChangedArgs>) (obj =>
    {
      Events.PlayerStatChangedArgs playerStatChangedArgs = obj;
      if (playerStatChangedArgs.Stat != Events.PlayerStat.Power)
        return;
      if (playerStatChangedArgs.Delta != 0.0)
        RewardsCollectionAnimator.Instance.CollectPower((int) playerStatChangedArgs.Delta, false);
      if (playerStatChangedArgs.Delta >= 0.0)
        return;
      VfxManager.Instance.CreateAndPlay("Prefab/VFX/fx_Power_lower", this.heroMiniDlg.effectroot);
    });
    CityManager inst = CityManager.inst;
    if (inst != null)
      inst.onResourcesUpdated += new CityManager.OnResourcesUpdated(this.OnResourcesUpdated);
    CityData playerCityData = PlayerData.inst.playerCityData;
    this.OnResourcesUpdated((long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER), (long) playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE), PlayerData.inst.userData.currency.gold);
    this.SetResourceLevel();
    this.talentMiniDlg.Init();
    this.mGroupQuestBtn.Init();
    this.WatchtowerBtn.gameObject.SetActive(false);
  }

  public void SetGold(long gold)
  {
    this.mGoldValue.text = Utils.FormatThousands(gold.ToString());
    this.mPlayerResourceData.mGold = gold;
  }

  public void SetResourceLevel()
  {
    int num1 = 1;
    int num2 = 1;
    int num3 = 10;
    int num4 = 15;
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_food");
    if (data1 != null)
      num1 = data1.ValueInt;
    GameConfigInfo data2 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_wood");
    if (data2 != null)
      num2 = data2.ValueInt;
    GameConfigInfo data3 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_ore");
    if (data3 != null)
      num3 = data3.ValueInt;
    GameConfigInfo data4 = ConfigManager.inst.DB_GameConfig.GetData("stronghold_level_unlock_silver");
    if (data4 != null)
      num4 = data4.ValueInt;
    Vector3 localPosition = this.resourceContainer.transform.localPosition;
    if (PlayerData.inst.CityData.mStronghold.mLevel >= num4)
    {
      localPosition.x = -800f;
      this.resourceContainer.transform.localPosition = localPosition;
    }
    else if (PlayerData.inst.CityData.mStronghold.mLevel >= num3)
    {
      localPosition.x = -400f;
      this.resourceContainer.transform.localPosition = localPosition;
    }
    else if (PlayerData.inst.CityData.mStronghold.mLevel >= num2)
    {
      localPosition.x = 0.0f;
      this.resourceContainer.transform.localPosition = localPosition;
    }
    else
    {
      if (PlayerData.inst.CityData.mStronghold.mLevel < num1)
        return;
      localPosition.x = 400f;
      this.resourceContainer.transform.localPosition = localPosition;
    }
  }

  public void SetSilver(long silver, long maxSilver)
  {
    this.mSilver.SetLevels(silver, maxSilver);
    this.mPlayerResourceData.mSilver = silver;
    this.mPlayerResourceData.mSilverLimit = maxSilver;
  }

  public void SetFood(long food, long maxFood)
  {
    this.mFood.SetLevels(food, maxFood);
    this.mPlayerResourceData.mFood = food;
    this.mPlayerResourceData.mFoodLimit = maxFood;
  }

  public void SetWood(long wood, long maxWood)
  {
    this.mWood.SetLevels(wood, maxWood);
    this.mPlayerResourceData.mWood = wood;
    this.mPlayerResourceData.mWoodLimit = maxWood;
  }

  public void SetOre(long ore, long maxOre)
  {
    this.mOre.SetLevels(ore, maxOre);
    this.mPlayerResourceData.mOre = ore;
    this.mPlayerResourceData.mOreLimit = maxOre;
  }

  public void SetAllianceBadgeCount(int count)
  {
    if (count == 0)
    {
      this.mAllianceBadge.SetActive(false);
    }
    else
    {
      this.mAllianceBadge.SetActive(true);
      this.mAllianceBadgeCount.text = count.ToString();
    }
  }

  public void SetItemsBadgeCount(int count)
  {
    if (count == 0)
    {
      this.mItemsBadge.SetActive(false);
    }
    else
    {
      this.mItemsBadge.SetActive(true);
      this.mItemsBadgeCount.text = count.ToString();
    }
  }

  public void SetMailBadgeCount(int count)
  {
    if (count == 0)
    {
      this.mMailBadge.SetActive(false);
    }
    else
    {
      this.mMailBadge.SetActive(true);
      this.mMailBadgeCount.text = count >= 100 ? "99+" : count.ToString();
    }
  }

  public void LockButtons()
  {
    UIManager.inst.HidePublicHud();
    UIManager.inst.HideTimerHud();
  }

  public void UnlockButtons()
  {
    UIManager.inst.ShowPublicHud();
    UIManager.inst.ShowTimerHud();
  }

  public void EnterSelectTargetMode()
  {
    this.UpdateUIForLocation(GAME_LOCATION.KINGDOM, true);
  }

  public void ExitSelectTargetMode()
  {
    this.UpdateUIForLocation(GAME_LOCATION.KINGDOM, false);
  }

  public void SwitchToPart(GAME_LOCATION nextLocation)
  {
    if (!GameEngine.IsAvailable && GameEngine.IsShuttingDown)
      return;
    UIManager.inst.ShowPublicHud();
    this.UpdateUIForLocation(nextLocation, false);
  }

  protected void UpdateUIForLocation(GAME_LOCATION location, bool selectTarget = false)
  {
    this.CoordinatesUI.SetActive(GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode);
    this.CityBtn.gameObject.SetActive(GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode);
    switch (location)
    {
      case GAME_LOCATION.CITY:
        this.partKingdom.SetActive(false);
        this.partHeroInfo.SetActive(true);
        this.partPublic.SetActive(true);
        this.partChat.SetActive(true);
        this.partCity.SetActive(true);
        this.partMiniMap.SetActive(false);
        this.partWorld.SetActive(false);
        this.QuickBackHUD();
        this.UpdateCenterTipItem();
        this.UpdateAllianceWarButtons();
        UIManager.inst.timerHUD.Display = false;
        break;
      case GAME_LOCATION.KINGDOM:
        this.PitLeftTime.text = string.Empty;
        this.KingdomSpin.SetActive(false);
        this.partCity.SetActive(false);
        this.partKingdom.SetActive(true);
        this.partMiniMap.SetActive(false);
        this.partWorld.SetActive(false);
        UIManager.inst.timerHUD.Display = true;
        if (selectTarget)
        {
          this.partHeroInfo.SetActive(false);
          this.partPublic.SetActive(false);
          this.partChat.SetActive(false);
          this.partShowWhenSelectTarget.SetActive(true);
          this.partHideWhenSelectTarget.SetActive(false);
        }
        else
        {
          this.partHeroInfo.SetActive(true);
          this.partPublic.SetActive(true);
          this.partChat.SetActive(true);
          this.partShowWhenSelectTarget.SetActive(true);
          this.partHideWhenSelectTarget.SetActive(true);
        }
        this.UpdateButtonVisibles();
        this.UpdateAllianceWarButtons();
        if (this.KingdomShow || !this.HUDUIVisiable)
          break;
        this.QuickHideHUD();
        break;
      case GAME_LOCATION.WORLD:
        this.partCity.SetActive(false);
        this.partHeroInfo.SetActive(false);
        this.partPublic.SetActive(false);
        this.partChat.SetActive(false);
        this.partKingdom.SetActive(false);
        this.partMiniMap.SetActive(false);
        this.partWorld.SetActive(true);
        UIManager.inst.timerHUD.Display = false;
        break;
      case GAME_LOCATION.MINI_MAP:
        this.partCity.SetActive(false);
        this.partHeroInfo.SetActive(false);
        this.partPublic.SetActive(false);
        this.partChat.SetActive(false);
        this.partKingdom.SetActive(false);
        this.partMiniMap.SetActive(true);
        this.partWorld.SetActive(false);
        this.AllianceToggle.Set(true);
        this.ResourceToggle.Set(false);
        this.ResourceToggleChanged();
        this.AllianceToggleChanged();
        this.UpdateAllianceTrackStatus();
        break;
      case GAME_LOCATION.DRAGON_KNIGHT:
      case GAME_LOCATION.MERLIN_TOWER_FLOOR:
      case GAME_LOCATION.MERLIN_TOWER_KINGDOM:
        this.QuickBackHUD();
        this.partCity.SetActive(false);
        if (location == GAME_LOCATION.MERLIN_TOWER_KINGDOM)
          this.partHeroInfo.SetActive(true);
        else
          this.partHeroInfo.SetActive(false);
        this.partPublic.SetActive(false);
        this.partChat.SetActive(true);
        this.partKingdom.SetActive(false);
        this.partMiniMap.SetActive(false);
        this.partWorld.SetActive(false);
        break;
    }
  }

  public void SetKingdomInfo(ActiveKingdom kingdom)
  {
    DB.WonderData wonderData = DBManager.inst.DB_Wonder.Get((long) kingdom.WorldID);
    if (wonderData != null)
    {
      UserData userData = DBManager.inst.DB_User.Get(wonderData.KING_UID);
      string state = wonderData.State;
      int kingdomColonizedBy;
      if (state != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (PublicHUD.\u003C\u003Ef__switch\u0024mapA4 == null)
        {
          // ISSUE: reference to a compiler-generated field
          PublicHUD.\u003C\u003Ef__switch\u0024mapA4 = new Dictionary<string, int>(3)
          {
            {
              "fighting",
              0
            },
            {
              "protected",
              1
            },
            {
              "unOpen",
              1
            }
          };
        }
        // ISSUE: reference to a compiler-generated field
        if (PublicHUD.\u003C\u003Ef__switch\u0024mapA4.TryGetValue(state, out kingdomColonizedBy))
        {
          switch (kingdomColonizedBy)
          {
            case 0:
              this.WonderStatus.text = ScriptLocalization.Get("throne_challenge_status", true);
              goto label_9;
            case 1:
              this.WonderStatus.text = ScriptLocalization.Get("throne_protected_status", true);
              goto label_9;
          }
        }
      }
      this.WonderStatus.text = string.Empty;
label_9:
      this.KingdomName.text = wonderData.LabelName;
      this.KingName.text = userData == null ? Utils.XLAT("id_none") : userData.userName;
      this.KingdomFlag.spriteName = wonderData.FlagIconPath;
      if (wonderData.State == "protected" && wonderData.KingdomColonizedBy > 0)
      {
        Dictionary<string, string> para = new Dictionary<string, string>();
        Dictionary<string, string> dictionary = para;
        string key = "0";
        kingdomColonizedBy = wonderData.KingdomColonizedBy;
        string str = kingdomColonizedBy.ToString();
        dictionary.Add(key, str);
        this.KingdomColonizedBy.text = ScriptLocalization.GetWithPara("kingdom_status_occupied_by", para, true);
        NGUITools.SetActive(this.KingdomColonizedBy.transform.parent.gameObject, true);
      }
      else
        NGUITools.SetActive(this.KingdomColonizedBy.transform.parent.gameObject, false);
      int num = (int) (wonderData.StateChangeTime - (long) NetServerTime.inst.ServerTimestamp);
      this.ProtectionState.text = !(wonderData.State == "fighting") || wonderData.OWNER_ALLIANCE_ID != 0L ? Utils.FormatTime(num <= 0 ? 0 : num, true, false, true) : string.Empty;
      UIGrid componentInParent = this.KingdomColonizedBy.transform.parent.GetComponentInParent<UIGrid>();
      if ((bool) ((UnityEngine.Object) componentInParent))
      {
        componentInParent.repositionNow = true;
        componentInParent.Reposition();
      }
    }
    UIManager.inst.timerHUD.Display = false;
  }

  public void ResourceToggleChanged()
  {
    if (this.ResourceToggle.value)
      MiniMapSystem.Instance.ShowResources();
    else
      MiniMapSystem.Instance.HideResources();
  }

  public void AllianceToggleChanged()
  {
    if (this.AllianceToggle.value)
    {
      MiniMapSystem.Instance.ShowAlliance();
      this.MiniMapLegend.SetActive(true);
    }
    else
    {
      MiniMapSystem.Instance.HideAlliance();
      this.MiniMapLegend.SetActive(false);
    }
  }

  public void KingdomButtonClicked()
  {
    MiniMapSystem.Instance.Close();
    PVPSystem.Instance.Show(false);
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.KINGDOM);
    OperationTrace.TraceClick(ClickArea.SwichToKingdomMap);
  }

  public void WorldButtonClicked()
  {
    MiniMapSystem.Instance.Hide();
    WorldSystem worldSystem = WorldSystem.Create();
    if (!(bool) ((UnityEngine.Object) worldSystem))
      return;
    worldSystem.EnterWorldSystem();
  }

  public void OnWorldReturnButtonClicked()
  {
    WorldSystem.Instance.ExitWorldSystem();
    MiniMapSystem.Instance.Show();
  }

  private void OnPlayerStatChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    UserData userData = DBManager.inst.DB_User.Get(PlayerData.inst.uid);
    this.SetGold((long) (int) userData.currency.gold);
    this.SetVIPLevel(ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints((int) userData.vipPoint).Level);
    this.SetCityPower(userData.power);
    this.SetResourceLevel();
  }

  private void OnCityStatChanged(long cityId)
  {
    if (cityId != (long) PlayerData.inst.cityId)
      return;
    this.SetResourceLevel();
  }

  private void OnHeroStatChanged(Events.HeroStatChangedArgs ev)
  {
    if (ev.Stat == Events.HeroStat.Experience)
    {
      if (ev.Delta > 0.0)
        RewardsCollectionAnimator.Instance.CollectXP((int) ev.Delta, false);
      else if (ev.Delta < 0.0)
        ;
    }
    else if (ev.Stat != Events.HeroStat.Level || !(bool) ((UnityEngine.Object) this.heroMiniDlg))
      ;
  }

  private void OnResourcesUpdated(long food, long wood, long silver, long ore, long gold)
  {
    this.SetFood(food, 0L);
    this.SetWood(wood, 0L);
    this.SetSilver(silver, 0L);
    this.SetOre(ore, 0L);
  }

  public void OnQuestsBtnPressed()
  {
    LinkerHub.Instance.DisposeHint(this.transform);
    GameDataManager.inst.LockScrolling();
    UIManager.inst.OpenDlg("QuestDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Quest);
  }

  public void OnItemsBtnPressed()
  {
    LinkerHub.Instance.DisposeHint(this.transform);
    UIManager.inst.OpenDlg("ItemStore/ItemStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Item);
  }

  public void OnBlacksmithBtnPressed()
  {
    if (this.onBlacksmithBtnPressed == null)
      return;
    this.onBlacksmithBtnPressed();
  }

  public void OnTalentBtnPressed()
  {
    this.talentMiniDlg.Show();
    OperationTrace.TraceClick(ClickArea.Talent);
  }

  public void OnGamblingBtnPressed()
  {
    if (this.onGamblingBtnPressed == null)
      return;
    this.onGamblingBtnPressed();
  }

  public void OnEnhanceBtnPressed()
  {
    if (this.onEnhanceBtnPressed == null)
      return;
    this.onEnhanceBtnPressed();
  }

  public void OnDKDetailPressed()
  {
    if (TutorialManager.Instance.EarlyGoalType == TutorialManager.ABTestType.Second)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("dragon_knight_summon_limit");
      if (data == null)
        return;
      if (PlayerData.inst.dragonData.Level < data.ValueInt)
        UIManager.inst.OpenDlg("DragonKnight/DragonKnightGuideDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("DragonKnight/DragonKnightProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }
    else
      UIManager.inst.OpenDlg("DragonKnight/DragonKnightProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnDKBlacksmithPressed()
  {
    UIManager.inst.OpenDlg("Equipment/DragonKnightEquipmentForgeDialog", (UI.Dialog.DialogParameter) new EquipmentForgeDlg.Parameter()
    {
      bagType = BagType.DragonKnight
    }, true, true, true);
  }

  public void OnDKEnhancePressed()
  {
    UIManager.inst.OpenDlg("Equipment/DragonKnightEquipmentEnhanceDialog", (UI.Dialog.DialogParameter) new EquipmentEnhanceDlg.Parameter()
    {
      bagType = BagType.DragonKnight
    }, true, true, true);
  }

  public void OnDKArmoryPressed()
  {
    UIManager.inst.OpenDlg("Equipment/DragonKnightEquipmentTreasuryDialog", (UI.Dialog.DialogParameter) new EquipmentTreasuryDlg.Parameter()
    {
      bagType = BagType.DragonKnight
    }, true, true, true);
  }

  public void OnMailBtnPressed()
  {
    UIManager.inst.OpenDlg("Mail/MailMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Mail);
  }

  public void OnAllianceBtnPressed()
  {
    LinkerHub.Instance.DisposeHint(this.transform);
    if (PlayerData.inst.allianceId > 0L)
      UIManager.inst.OpenDlg("Alliance/AllianceDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    else
      UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Alliance);
  }

  public void OnInfoBtnPressed()
  {
    if (this.onInfoBtnPressed == null)
      return;
    this.onInfoBtnPressed();
  }

  public void OnTrainBtnPressed()
  {
    if (this.onTrainBtnPressed == null)
      return;
    this.onTrainBtnPressed();
  }

  public void OnBarracksResearchBtnPressed()
  {
    if (this.onBarracksResearchBtnPressed == null)
      return;
    this.onBarracksResearchBtnPressed();
  }

  public void OnUpgradeBtnPressed()
  {
    if (this.onUpgradeBtnPressed == null)
      return;
    this.onUpgradeBtnPressed();
  }

  public void OnHospitalBtnPressed()
  {
    if (this.onHospitalBtnPressed == null)
      return;
    this.onHospitalBtnPressed();
  }

  public void OnResearchBtnPressed()
  {
    if (this.onResearchBtnPressed == null)
      return;
    this.onResearchBtnPressed();
  }

  public void OnRallyBtnPressed()
  {
    if (this.onRallyBtnPressed == null)
      return;
    this.onRallyBtnPressed();
  }

  public void OnReinforceBtnPressed()
  {
    if (this.onReinforceBtnPressed == null)
      return;
    this.onReinforceBtnPressed();
  }

  public void OnTradeBtnPressed()
  {
    if (this.onTradeBtnPressed == null)
      return;
    this.onTradeBtnPressed();
  }

  public void ShowBuildingMovePanel(int step)
  {
    this.buildingmovepanel.SetActive(true);
    UILabel componentInChildren = this.buildingmovepanel.GetComponentInChildren<UILabel>();
    if (step == 1)
    {
      componentInChildren.text = ScriptLocalization.Get("move_building_select_building_description", true);
    }
    else
    {
      if (step != 2)
        return;
      componentInChildren.text = ScriptLocalization.Get("move_building_select_destination_description", true);
    }
  }

  public void HideBuildingMovePanel()
  {
    this.buildingmovepanel.SetActive(false);
  }

  public void OnBuildingMoveCancellBtnPressed()
  {
    BuildingMoveManager.Instance.ExitBuildingMove();
  }

  public void OnSilverResourceClicked()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
    OperationTrace.TraceClick(ClickArea.Resource);
  }

  public void OnFoodResourceClicked()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD);
    OperationTrace.TraceClick(ClickArea.Resource);
  }

  public void OnWoodResourceClicked()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD);
    OperationTrace.TraceClick(ClickArea.Resource);
  }

  public void OnOreResourceClicked()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE);
    OperationTrace.TraceClick(ClickArea.Resource);
  }

  public void OnRunesClicked()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    OperationTrace.TraceClick(ClickArea.IAPStore);
  }

  public void OnGroupQuestClicked()
  {
    UIManager.inst.OpenPopup("GroupQuest/GroupQuestPopup", (Popup.PopupParameter) null);
    OperationTrace.TraceClick(ClickArea.GroupQuest);
  }

  private void OnResourceClicked(CityManager.ResourceTypes types)
  {
    UserOrBuyResourceItemsDlg.Parameter parameter = new UserOrBuyResourceItemsDlg.Parameter();
    switch (types)
    {
      case CityManager.ResourceTypes.FOOD:
        parameter.selectedIndex = 0;
        break;
      case CityManager.ResourceTypes.SILVER:
        parameter.selectedIndex = 3;
        break;
      case CityManager.ResourceTypes.WOOD:
        parameter.selectedIndex = 1;
        break;
      case CityManager.ResourceTypes.ORE:
        parameter.selectedIndex = 2;
        break;
    }
    UIManager.inst.OpenDlg("ItemStore/GetMoreResDlg", (UI.Dialog.DialogParameter) parameter, true, true, true);
  }

  private void OnItemClicked(ShopStaticInfo shopInfo, double amount, double needed)
  {
    UIManager.inst.OpenDlg("ItemStore/MoreItemDialog", (UI.Dialog.DialogParameter) new GetMoreItemDialog.Parameter()
    {
      shopInfo = shopInfo,
      needed = needed
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void OnResearchStateChanged(Events.ResearchStateChangedEventArgs args)
  {
    if (args.State != TechLevel.ResearchState.Completed)
      return;
    UIManager.inst.toast.Show(string.Format("{0} researched", (object) ResearchManager.inst.GetTechLevel(args.InternalID).Tech.Name), (System.Action) null, 4f, false);
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || !int.TryParse(hashtable[(object) "event_type"].ToString(), out result1))
      return;
    if (result1 == 44 && !TutorialManager.Instance.IsRunning)
    {
      UIManager.inst.priorityPopupQueue.OpenPopup("VIP/VIPLosePopup", (Popup.PopupParameter) null, 4);
    }
    else
    {
      if (result1 != 39 && result1 != 66)
        return;
      UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
    }
  }

  private void OnShutup(object result)
  {
    UIManager.inst.toast.Show(ScriptLocalization.GetWithMultyContents("toast_chat_silenced_time", true, Utils.FormatTime(PlayerData.inst.userData.ShutUpLeftTime, false, false, true)), (System.Action) null, 4f, false);
  }

  private void OnPaymentSuccess(object result)
  {
    Hashtable hashtable = result as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "package_id") || !hashtable.ContainsKey((object) "group_id"))
      return;
    string id1 = hashtable[(object) "package_id"].ToString();
    string id2 = hashtable[(object) "group_id"].ToString();
    int num1 = 0;
    int num2 = 0;
    bool flag;
    if (hashtable.ContainsKey((object) "quantity") && hashtable.ContainsKey((object) "max_quantity"))
    {
      num1 = int.Parse(hashtable[(object) "quantity"].ToString());
      num2 = int.Parse(hashtable[(object) "max_quantity"].ToString());
      flag = false;
    }
    else
      flag = true;
    IAPPackageInfo iapPackageInfo = ConfigManager.inst.DB_IAPPackage.Get(id1);
    IAPPackageGroupInfo packageGroupInfo = ConfigManager.inst.DB_IAPPackageGroup.Get(id2);
    if (iapPackageInfo == null || packageGroupInfo == null)
      return;
    AccountManager.Instance.ShowBindPop();
    UIManager.inst.OpenPopup("IAPBuySuccessPopup", (Popup.PopupParameter) new IAPBuySuccessPopup.Parameter()
    {
      package = new IAPStorePackage()
      {
        package = iapPackageInfo,
        group = packageGroupInfo,
        quantity = num1,
        max_quantity = num2
      },
      isCrossLevel = flag
    });
  }

  private void OnSecondTick(int delta)
  {
    if (this.PitLeftTime.gameObject.activeInHierarchy)
      this.PitLeftTime.text = Utils.FormatTime(PitExplorePayload.Instance.PitEndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    if ((UnityEngine.Object) this.newActivityFlag != (UnityEngine.Object) null)
      this.newActivityFlag.SetActive(ActivityManager.Intance.HasNewActivty);
    int num = this.UpdateCenterTipItem();
    if (this._previousCenterTipCount != num)
    {
      this._previousCenterTipCount = num;
      this.UpdateAllianceBadgeCount();
    }
    if (!this.mAllianceWarBriefButton.activeInHierarchy)
      return;
    this.UpdateAllianceWarLeftTime();
  }

  protected void UpdateAllianceWarLeftTime()
  {
    if (!(bool) ((UnityEngine.Object) this.mLabelAllianceWarTime))
      return;
    AllianceWarActivityData allianceWar = ActivityManager.Intance.allianceWar;
    if (allianceWar == null)
      return;
    int serverTimestamp = NetServerTime.inst.ServerTimestamp;
    int fightStartTime = allianceWar.FightStartTime;
    int fightEndTime = allianceWar.FightEndTime;
    if (serverTimestamp < fightStartTime)
    {
      this.mLabelAllianceWarTime.text = Utils.FormatTime(fightStartTime - serverTimestamp, true, true, true);
      this.mLabelAllianceWarTime.color = this.mColorWaitForStart;
    }
    else
    {
      this.mLabelAllianceWarTime.text = Utils.FormatTime(fightEndTime - serverTimestamp, true, true, true);
      this.mLabelAllianceWarTime.color = this.mColorWaitForEnd;
    }
  }

  public void ClearOldDelegates()
  {
    this.onBuildPressed = (System.Action) null;
    this.onWorldMapBtnPressed = (System.Action) null;
    this.onInfoBtnPressed = (System.Action) null;
    this.onTrainBtnPressed = (System.Action) null;
    this.onBarracksResearchBtnPressed = (System.Action) null;
    this.onUpgradeBtnPressed = (System.Action) null;
    this.onBlacksmithBtnPressed = (System.Action) null;
    this.onGamblingBtnPressed = (System.Action) null;
    this.onEnhanceBtnPressed = (System.Action) null;
    this.onResearchBtnPressed = (System.Action) null;
    this.onHospitalBtnPressed = (System.Action) null;
    this.onRallyBtnPressed = (System.Action) null;
    this.onReinforceBtnPressed = (System.Action) null;
    this.onTradeBtnPressed = (System.Action) null;
  }

  private void Update()
  {
    if (!this._centerTipLayoutDirty || !this.TableCenterTip.gameObject.activeInHierarchy)
      return;
    this._centerTipLayoutDirty = false;
    this.TableCenterTip.Reposition();
  }

  protected void OnTreasuryVaultCountChanged(int chestLibraryCount)
  {
    this.UpdateCenterTipItem();
    this.UpdateAllianceBadgeCount();
  }

  protected void OnUserTreasureChestDataChanged(UserTreasureChestData data)
  {
    this.UpdateCenterTipItem();
    this.UpdateAllianceBadgeCount();
  }

  public int UpdateCenterTipItem()
  {
    bool flag = PlayerData.inst.allianceId != 0L;
    int tipCount1 = !flag ? 0 : AllianceTreasuryCountManager.Instance.UnOpenedChestCount;
    int tipCount2 = !flag ? 0 : DBManager.inst.DB_UserTreasureChest.GetAllCanSendUserChest(PlayerData.inst.uid);
    int num = 0;
    this.ChestLibraryTipItem.SetTipCount(tipCount1);
    if (tipCount1 > 0)
      num += 2;
    this.PersonalChestTipItem.SetTipCount(tipCount2);
    if (tipCount2 > 0)
      ++num;
    if (this._previousTipMask != num)
    {
      this._previousTipMask = num;
      this._centerTipLayoutDirty = true;
    }
    return tipCount1 + tipCount2;
  }

  public void OnButtonChestLibraryClicked()
  {
    UIManager.inst.OpenDlg("AllianceChest/AllianceChestDialog", (UI.Dialog.DialogParameter) new AllianceChestDialog.Parameter()
    {
      page = AllianceChestDialog.Page.ChestLibrary
    }, 1 != 0, 1 != 0, 1 != 0);
    OperationTrace.TraceClick(ClickArea.ChestLibrary);
  }

  public void OnButtonPersonalChestClicked()
  {
    UIManager.inst.OpenDlg("AllianceChest/AllianceChestDialog", (UI.Dialog.DialogParameter) new AllianceChestDialog.Parameter()
    {
      page = AllianceChestDialog.Page.PersonalChest
    }, 1 != 0, 1 != 0, 1 != 0);
    OperationTrace.TraceClick(ClickArea.PersonalChest);
  }

  public void OnButtonAllianceWarBriefClicked()
  {
    UIManager.inst.OpenDlg("AllianceWarBriefDlg", (UI.Dialog.DialogParameter) null, true, true, false);
    OperationTrace.TraceClick(ClickArea.PersonalChest);
  }

  public void OnAllianceWarClicked()
  {
    this._isAllianceWarClicked = !this.AllianceTrackView.IsAllianceWarMiniMapTrackViewExplanded;
    if (this._isAllianceWarClicked)
    {
      if (this._trackedAllianceIdList.Count <= 0)
        UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
        {
          Title = Utils.XLAT("id_uppercase_notice"),
          Okay = Utils.XLAT("id_uppercase_okay"),
          Content = Utils.XLAT("help_alliance_warfare_trace_help")
        });
      else
        this.AllianceTrackView.Show();
    }
    else
      this.AllianceTrackView.Hide();
    OperationTrace.TraceClick(ClickArea.AllianceWarBrife);
  }

  private void UpdateAllianceTrackStatus()
  {
    if (AllianceWarPayload.Instance.IsAllianceWarStarted && AllianceWarPayload.Instance.IsPlayerCurrentJoined(0L))
      AllianceWarPayload.Instance.LoadTrackedAlliance((System.Action<List<long>>) (trackedAllianceIdList =>
      {
        this._trackedAllianceIdList = trackedAllianceIdList;
        if (this._trackedAllianceIdList.Count < 0)
          return;
        this.AllianceTrackView.Init(this._trackedAllianceIdList);
      }));
    else
      NGUITools.SetActive(this.AllianceTrackView.gameObject, false);
  }

  [Serializable]
  public class CityButtons
  {
    public GameObject mActionButtons;
    public GameObject mDefaultRoot;
    public GameObject mForgeRoot;
    public GameObject mPVERoot;
    public GameObject mResearchRoot;
    public GameObject mTrainTroopsRoot;
    public GameObject mHospitalRoot;
    public GameObject mRallyRoot;
    public GameObject mReinforceRoot;
    public GameObject mTradeRoot;
    public GameObject mHeroTowerRoot;
    public GameObject mBottomBtns1;
    public GameObject mBottomBtns2;
    public GameObject mWatchtowerRoot;
    public GameObject mBurningButton;
  }

  private enum VisualEffect
  {
    POWER_UP,
    COUNT,
  }
}
