﻿// Decompiled with JetBrains decompiler
// Type: MuteAPI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class MuteAPI
{
  public static void MuteUser(long oppUid, long muteTime, System.Action callBack)
  {
    new HubPort("Player:mute").SendRequest(new Hashtable()
    {
      {
        (object) "opp_uid",
        (object) oppUid
      },
      {
        (object) "time",
        (object) muteTime
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack();
    }), true);
  }

  public static void UnMuteUser(long oppUid, System.Action callBack)
  {
    new HubPort("Player:unmute").SendRequest(new Hashtable()
    {
      {
        (object) "opp_uid",
        (object) oppUid
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack();
    }), true);
  }

  public static void ReportAdmin(long oppUid, string content, System.Action callBack)
  {
    new HubPort("Player:reportAdmin").SendRequest(new Hashtable()
    {
      {
        (object) "opp_uid",
        (object) oppUid
      },
      {
        (object) nameof (content),
        (object) content
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack();
    }), true);
  }
}
