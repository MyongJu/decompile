﻿// Decompiled with JetBrains decompiler
// Type: CollectionUIManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CollectionUIManager
{
  private const string path1 = "Prefab/City/CollectionUI";
  private const string path2 = "Prefab/City/CollectionResUI";
  private static CollectionUIManager _instance;
  private GameObject template1;
  private GameObject template2;
  private bool inited;

  public static CollectionUIManager Instance
  {
    get
    {
      if (CollectionUIManager._instance == null)
        CollectionUIManager._instance = new CollectionUIManager();
      return CollectionUIManager._instance;
    }
  }

  private void Init()
  {
    this.template1 = AssetManager.Instance.HandyLoad("Prefab/City/CollectionUI", (System.Type) null) as GameObject;
    this.template2 = AssetManager.Instance.HandyLoad("Prefab/City/CollectionResUI", (System.Type) null) as GameObject;
  }

  public CollectionUI OpenCollectionUI(Transform parent, CityCircleBtnPara para, CollectionUI.CollectionType ct = CollectionUI.CollectionType.TrainSoldier)
  {
    if (!this.inited)
    {
      this.inited = true;
      this.Init();
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) parent.GetComponentInChildren<CollectionUI>())
      return (CollectionUI) null;
    CollectionUI component = (ct == CollectionUI.CollectionType.CollectionRes ? Utils.DuplicateGOB(this.template2, parent) : Utils.DuplicateGOB(this.template1, parent)).GetComponent<CollectionUI>();
    component.InitCollectionUI(para, ct);
    component.Open(parent);
    return component;
  }

  internal void Dispose()
  {
    this.inited = false;
    this.template1 = (GameObject) null;
    this.template2 = (GameObject) null;
    CollectionUIManager._instance = (CollectionUIManager) null;
  }
}
