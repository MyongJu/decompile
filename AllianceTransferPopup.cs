﻿// Decompiled with JetBrains decompiler
// Type: AllianceTransferPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceTransferPopup : Popup
{
  private AllianceTransferPopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as AllianceTransferPopup.Parameter;
  }

  public void OnNo()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnYes()
  {
    this.OnNo();
    AllianceManager.Instance.TransferLeadership(this.m_Parameter.targetUid, this.m_Parameter.callback);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long targetUid;
    public System.Action<bool, object> callback;
  }
}
