﻿// Decompiled with JetBrains decompiler
// Type: ConsumableReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class ConsumableReward : QuestReward
{
  public readonly ItemStaticInfo consumableItem;
  public int value;

  public ConsumableReward(int consumablePHPID, int inValue)
  {
    this.consumableItem = ConfigManager.inst.DB_Items.GetItem(consumablePHPID);
    this.value = inValue;
  }

  public override void Claim()
  {
  }

  public override string GetRewardIconName()
  {
    return ConfigManager.inst.DB_Items.GetItem(this.consumableItem.internalId).ImagePath;
  }

  public override string GetRewardTypeName()
  {
    return ScriptLocalization.Get(this.consumableItem.Loc_Name_Id, true);
  }

  public override int GetValue()
  {
    return this.value;
  }

  public override string GetValueText()
  {
    return this.value.ToString();
  }
}
