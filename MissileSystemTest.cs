﻿// Decompiled with JetBrains decompiler
// Type: MissileSystemTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MissileSystemTest : MonoBehaviour
{
  public int shootNumber = 10;
  public string missileSystemPath;
  public MissileSystem missileSystem;
  public GameObject source;
  public GameObject target;

  private void Start()
  {
    if (!((Object) this.missileSystem == (Object) null) || string.IsNullOrEmpty(this.missileSystemPath))
      return;
    MissileSystemManager.Instance.RegisterMissileSystem("arrow_system", this.missileSystemPath);
    this.missileSystem = MissileSystemManager.Instance.GetMissileSystem("arrow_system");
  }
}
