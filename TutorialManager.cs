﻿// Decompiled with JetBrains decompiler
// Type: TutorialManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
  private TutorialManager.ABTestType resolution = TutorialManager.ABTestType.Second;
  private LayerMask cacheCityCamera = (LayerMask) 0;
  private LayerMask cacheTileCamera = (LayerMask) 0;
  private LayerMask cacheUI2DCamera = (LayerMask) 0;
  private LayerMask cacheUIOverlay2DCamera = (LayerMask) 0;
  private static TutorialManager _instance;
  private TutorialManager.ABTestType earlyGoalType;
  private int joinAllianceStrongholdLevel;
  private int enterWonderStrongholdLevel;
  private int lastJoinAllianceLevel;
  private int lastEnterWonderLevel;
  private List<TutorialData> dataQueue;
  private GameObject dialog;
  private bool isRunning;
  private bool isPause;
  private float _zoomFactorCache;
  private float _zoomSpeedCache;
  public System.Action onTutotialFinished;
  private UICamera ccamera;
  private UICamera tcamera;
  private UICamera u2camera;
  private UICamera u2Overlaycamera;

  public static TutorialManager Instance
  {
    get
    {
      if ((UnityEngine.Object) TutorialManager._instance == (UnityEngine.Object) null)
      {
        TutorialManager._instance = UnityEngine.Object.FindObjectOfType<TutorialManager>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) TutorialManager._instance)
          throw new ArgumentException("TutorialManager hasn't been created yet.");
        TutorialManager._instance.DataQueue = new List<TutorialData>();
      }
      return TutorialManager._instance;
    }
  }

  public TutorialManager.ABTestType Resolution
  {
    get
    {
      return this.resolution;
    }
    set
    {
      this.resolution = value;
    }
  }

  public TutorialManager.ABTestType EarlyGoalType
  {
    get
    {
      return this.earlyGoalType;
    }
    set
    {
      this.earlyGoalType = value;
    }
  }

  public int JoinAllianceStrongholdLevel
  {
    get
    {
      return this.joinAllianceStrongholdLevel;
    }
    set
    {
      this.joinAllianceStrongholdLevel = value;
    }
  }

  public int EnterWonderStrongholdLevel
  {
    get
    {
      return this.enterWonderStrongholdLevel;
    }
    set
    {
      this.enterWonderStrongholdLevel = value;
    }
  }

  public int LastJoinAllianceLevel
  {
    get
    {
      return this.lastJoinAllianceLevel;
    }
    set
    {
      this.lastJoinAllianceLevel = value;
    }
  }

  public int LastEnterWonderLevel
  {
    get
    {
      return this.lastEnterWonderLevel;
    }
    set
    {
      this.lastEnterWonderLevel = value;
    }
  }

  public List<TutorialData> DataQueue
  {
    get
    {
      return this.dataQueue;
    }
    set
    {
      this.dataQueue = value;
    }
  }

  public TutorialDialog TutorialDialogObj
  {
    get
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this.dialog)
        return this.dialog.GetComponent<TutorialDialog>();
      return (TutorialDialog) null;
    }
  }

  public bool IsRunning
  {
    get
    {
      return this.isRunning;
    }
  }

  public bool IsPause
  {
    get
    {
      return this.isPause;
    }
  }

  public void Startup()
  {
    if (this.DataQueue[0].name != "Tutorial_hero_summon.1" && (UIManager.inst.dialogStackCount != 0 || UIManager.inst.popupStackCount != 0 || UIManager.inst.HasSystemBlocker))
    {
      UIManager.inst.onPublicHudStateChange += new System.Action<bool>(this.OnPublicHudShow);
      UIManager.inst.onAllPopupClosed += new System.Action<bool>(this.OnPublicHudShow);
      UIManager.inst.onAllSystemBlockerClosed += new System.Action<bool>(this.OnPublicHudShow);
    }
    else
      this.StartTutorial();
  }

  private void OnPublicHudShow(bool show)
  {
    if (UIManager.inst.dialogStackCount != 0 || UIManager.inst.popupStackCount != 0 || UIManager.inst.HasSystemBlocker)
      return;
    this.StartTutorial();
  }

  private void StartTutorial()
  {
    if (this.DataQueue.Count == 0 || this.isRunning)
      return;
    this.CreateDialogInstance();
    this.dialog.GetComponent<TutorialDialog>().onFinished += new System.Action(this.NextStep);
    this.isRunning = true;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    this._zoomFactorCache = cityCamera.zoomSmoothFactor;
    this._zoomSpeedCache = cityCamera.zoomSpeed;
    this.CacheOldCameraLayerMask();
    this.ActiveTutorialLayer(true);
    this.BeginTutorial();
  }

  private void CreateDialogInstance()
  {
    if (!((UnityEngine.Object) null == (UnityEngine.Object) this.dialog))
      return;
    this.dialog = UnityEngine.Object.Instantiate(this.Resolution != TutorialManager.ABTestType.Basic ? AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/Tutorial/TutorialB", (System.Type) null) : AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/Tutorial/Tutorial", (System.Type) null)) as GameObject;
    this.dialog.transform.parent = UIManager.inst.ui2DOverlayCamera.transform;
    this.dialog.transform.localScale = Vector3.one;
  }

  public void Terminate()
  {
    if (!this.isRunning)
      return;
    this.isRunning = false;
    this.isPause = false;
    CityCamera cityCamera = UIManager.inst.cityCamera;
    cityCamera.zoomSmoothFactor = this._zoomFactorCache;
    cityCamera.zoomSpeed = this._zoomSpeedCache;
    this.dialog.GetComponent<TutorialDialog>().onFinished -= new System.Action(this.NextStep);
    UIManager.inst.onPublicHudStateChange -= new System.Action<bool>(this.OnPublicHudShow);
    UIManager.inst.onAllPopupClosed -= new System.Action<bool>(this.OnPublicHudShow);
    UIManager.inst.onAllSystemBlockerClosed -= new System.Action<bool>(this.OnPublicHudShow);
    this.DestoryDialogInstance();
    this.ResetCameraLayer();
    NewTutorial.Instance.Dispose();
    if (this.onTutotialFinished == null)
      return;
    this.onTutotialFinished();
  }

  public void Pause()
  {
    if (this.isPause || !this.isRunning)
      return;
    this.dialog.SetActive(false);
    this.ActiveTutorialLayer(false);
    this.isPause = true;
  }

  public void Resume()
  {
    if (!this.isPause)
      return;
    this.dialog.SetActive(true);
    this.ActiveTutorialLayer(true);
    this.isPause = false;
  }

  private void DestoryDialogInstance()
  {
    this.dialog.gameObject.SetActive(false);
    this.dialog.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.dialog);
    this.dialog = (GameObject) null;
  }

  private void BeginTutorial()
  {
    if (this.DataQueue.Count > 0)
    {
      if (!Application.isEditor)
      {
        FunplusBi.Instance.TraceEvent("tutorial", JsonWriter.Serialize((object) new TutorialManager.BIFormat()
        {
          d_c1 = new TutorialManager.BITutorialStep()
          {
            key = "action",
            value = "step"
          },
          d_c2 = new TutorialManager.BITutorialStep()
          {
            value = (TutorialManager.Instance.Resolution != TutorialManager.ABTestType.Basic ? (this.DataQueue[0].npcindex + 100).ToString() + " " + this.DataQueue[0].name : this.DataQueue[0].npcindex.ToString() + " " + this.DataQueue[0].name),
            value = (this.DataQueue[0].npcindex.ToString() + " " + this.DataQueue[0].name)
          },
          d_c3 = new TutorialManager.BITutorialStep()
          {
            key = "opening",
            value = OpeningCinematicManager.Instance.Resolution.ToString()
          }
        }));
        if ("Tutorial_military_tent.1" == this.DataQueue[0].name)
          TrackEvent.Instance.TraceEventToFB("Completed Tutorial");
      }
      LinkerHub.Instance.DisposeHintAnyway();
      this.dialog.GetComponent<TutorialDialog>().Open(new TutorialDialog.Parameter()
      {
        td = this.DataQueue[0]
      });
    }
    else
      this.Terminate();
  }

  public void NextStep()
  {
    this.DataQueue.RemoveAt(0);
    this.BeginTutorial();
  }

  private void CacheOldCameraLayerMask()
  {
    this.ccamera = UIManager.inst.cityCamera.GetComponent<UICamera>();
    this.cacheCityCamera = this.ccamera.eventReceiverMask;
    this.tcamera = UIManager.inst.tileCamera.GetComponent<UICamera>();
    this.cacheTileCamera = this.tcamera.eventReceiverMask;
    this.u2camera = UIManager.inst.ui2DCamera.GetComponent<UICamera>();
    this.cacheUI2DCamera = this.u2camera.eventReceiverMask;
    this.u2Overlaycamera = UIManager.inst.ui2DOverlayCamera.GetComponent<UICamera>();
    this.cacheUIOverlay2DCamera = this.u2Overlaycamera.eventReceiverMask;
  }

  public void ActiveTutorialLayer(bool b)
  {
    if (b)
    {
      this.u2camera.eventReceiverMask = (LayerMask) 0;
      this.ccamera.eventReceiverMask = (LayerMask) 0;
      this.tcamera.eventReceiverMask = (LayerMask) 0;
      UICamera u2Overlaycamera = this.u2Overlaycamera;
      u2Overlaycamera.eventReceiverMask = (LayerMask) ((int) u2Overlaycamera.eventReceiverMask | 1 << GameEngine.Instance.TutorialLayer);
    }
    else
    {
      this.u2camera.eventReceiverMask = this.cacheUI2DCamera;
      this.ccamera.eventReceiverMask = this.cacheCityCamera;
      this.tcamera.eventReceiverMask = (LayerMask) ((int) this.cacheTileCamera | 1 << GameEngine.Instance.TileLayer | 1 << GameEngine.Instance.PVPUILayer);
      this.u2Overlaycamera.eventReceiverMask = this.cacheUIOverlay2DCamera;
    }
  }

  public void ResetCameraLayer()
  {
    this.u2camera.eventReceiverMask = this.cacheUI2DCamera;
    this.ccamera.eventReceiverMask = this.cacheCityCamera;
    this.tcamera.eventReceiverMask = this.cacheTileCamera;
    this.u2Overlaycamera.eventReceiverMask = this.cacheUIOverlay2DCamera;
  }

  public bool CheckTutorialIsFinish()
  {
    if (NewTutorial.skipTutorial)
      return true;
    string empty = string.Empty;
    return "finished" == (TutorialManager.Instance.Resolution != TutorialManager.ABTestType.Basic ? NewTutorial.Instance.GetRecordPoint("testb_beginner") : NewTutorial.Instance.GetRecordPoint("beginner"));
  }

  public void SendRecordEarlyGoalRequest(string type, System.Action<bool, object> callback = null)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (type)] = (object) type;
    MessageHub.inst.GetPortByAction("Player:recordEarlyGoalTutorial").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || callback == null)
        return;
      callback(ret, data);
    }), false);
  }

  public enum ABTestType
  {
    Basic,
    Second,
  }

  private class BITutorialStep
  {
    public string key = "tutorial_step";
    public string value;
  }

  private class BIFormat
  {
    public TutorialManager.BITutorialStep d_c1;
    public TutorialManager.BITutorialStep d_c2;
    public TutorialManager.BITutorialStep d_c3;
  }
}
