﻿// Decompiled with JetBrains decompiler
// Type: BookmarkDeletePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class BookmarkDeletePopup : Popup
{
  private BookmarkDeletePopup.Parameter m_Parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as BookmarkDeletePopup.Parameter;
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnYesPressed()
  {
    this.OnClosePressed();
    if (this.m_Parameter.onYes == null)
      return;
    this.m_Parameter.onYes();
  }

  public void OnNoPressed()
  {
    this.OnClosePressed();
    if (this.m_Parameter.onNo == null)
      return;
    this.m_Parameter.onNo();
  }

  public class Parameter : Popup.PopupParameter
  {
    public BookmarkDeletePopup.OnYesCallback onYes;
    public BookmarkDeletePopup.OnNoCallback onNo;
  }

  public delegate void OnYesCallback();

  public delegate void OnNoCallback();
}
