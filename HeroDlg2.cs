﻿// Decompiled with JetBrains decompiler
// Type: HeroDlg2
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class HeroDlg2 : UI.Dialog
{
  private const string _leftStatText = "PRODUCTION BOOSTS";
  private const string _leftItemText = "SELECT AN ITEM";
  private const string _rightStatText = "COMBAT BOOSTS";
  public GameObject leftArea;
  public GameObject rightArea;
  public GameObject equipmentGrid;
  public GameObject backButton;
  public HeroInformationDlg infoDlg;
  public HeroStatDlg leftStatDlg;
  public HeroStatDlg rightStatDlg;
  public HeroModelDlg heroModelDlg;
  public UILabel leftFrameTitle;
  public UILabel rightFrameTitle;
  public GameObject titleBar;
  public HeroMainDlg mainDlg;
  public HeroItemFilter equipmentFilter;
  public PageTabsMonitor tabs;
  private HeroDlg2.HeroLeftType _currentLeftType;

  public void Init()
  {
    this.mainDlg.Init();
    this.heroModelDlg.Init();
  }

  public void EnableStatsTab()
  {
    this.mainDlg.gameObject.SetActive(false);
    this.SelectLeftArea(HeroDlg2.HeroLeftType.heroStats);
    this.SelectRightArea(HeroDlg2.HeroRightType.heroStats);
  }

  public void EnableMainTab()
  {
    this.mainDlg.gameObject.SetActive(true);
    this.SelectLeftArea(HeroDlg2.HeroLeftType.none);
    this.SelectRightArea(HeroDlg2.HeroRightType.none);
  }

  public void OnEnable()
  {
  }

  public void OnDisable()
  {
  }

  public void SelectLeftArea(HeroDlg2.HeroLeftType selectedType)
  {
    this.leftArea.SetActive(selectedType != HeroDlg2.HeroLeftType.none);
    this._SetLeftDlg(selectedType);
    this._currentLeftType = selectedType;
  }

  public void SelectRightArea(HeroDlg2.HeroRightType selectedType)
  {
    this.rightArea.SetActive(selectedType != HeroDlg2.HeroRightType.none);
    this._SetRightDlg(selectedType);
  }

  private void _SetLeftDlg(HeroDlg2.HeroLeftType dialog)
  {
    this.leftArea.SetActive(dialog != HeroDlg2.HeroLeftType.none);
    this.heroModelDlg.gameObject.SetActive(dialog == HeroDlg2.HeroLeftType.none);
    if (dialog == HeroDlg2.HeroLeftType.none)
      this.tabs.SetCurrentTab(0, true);
    UIManager.inst.ui3DCamera.gameObject.SetActive(dialog == HeroDlg2.HeroLeftType.none);
    this.leftStatDlg.gameObject.SetActive(dialog == HeroDlg2.HeroLeftType.heroStats);
    if (dialog == HeroDlg2.HeroLeftType.heroStats)
      this.leftFrameTitle.text = "PRODUCTION BOOSTS";
    else if ((Object) this.leftFrameTitle != (Object) null)
      this.leftFrameTitle.text = "SELECT AN ITEM";
    this.infoDlg.gameObject.SetActive(dialog == HeroDlg2.HeroLeftType.itemInfo);
    this.titleBar.SetActive(dialog == HeroDlg2.HeroLeftType.itemInfo);
  }

  private void _SetRightDlg(HeroDlg2.HeroRightType dialog)
  {
    this.rightArea.SetActive(dialog != HeroDlg2.HeroRightType.none);
    this.mainDlg.gameObject.SetActive(dialog == HeroDlg2.HeroRightType.none);
    if (dialog == HeroDlg2.HeroRightType.none)
    {
      this.mainDlg.Fresh();
      this.tabs.gameObject.SetActive(true);
      this.equipmentGrid.SetActive(false);
      this.backButton.SetActive(false);
    }
    this.equipmentFilter.gameObject.SetActive(dialog == HeroDlg2.HeroRightType.itemInventory);
    this.equipmentFilter.filterArrows.SetActive(dialog == HeroDlg2.HeroRightType.itemInventory);
    this.rightStatDlg.gameObject.SetActive(dialog == HeroDlg2.HeroRightType.heroStats);
    if (dialog != HeroDlg2.HeroRightType.heroStats)
      return;
    this.rightFrameTitle.text = "COMBAT BOOSTS";
  }

  public void SelectDefaultDlg()
  {
    this.SelectLeftArea(HeroDlg2.HeroLeftType.none);
    this.SelectRightArea(HeroDlg2.HeroRightType.none);
  }

  public void ClickStatistics()
  {
    if (this._currentLeftType != HeroDlg2.HeroLeftType.heroStats)
      this.SelectLeftArea(HeroDlg2.HeroLeftType.heroStats);
    else
      this.SelectLeftArea(HeroDlg2.HeroLeftType.none);
  }

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.Init();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen((UIControler.UIParameter) null);
    this.SelectDefaultDlg();
  }

  public void CloseDlg()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void BackDlg()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public enum HeroLeftType
  {
    none,
    itemInfo,
    heroStats,
  }

  public enum HeroRightType
  {
    none,
    itemInventory,
    heroStats,
  }
}
