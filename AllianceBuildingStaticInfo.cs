﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceBuildingStaticInfo
{
  [Config(Name = "name")]
  public string BuildName = string.Empty;
  [Config(Name = "description")]
  public string BuildDesc = string.Empty;
  [Config(Name = "icon")]
  public string Icon = string.Empty;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "req_alliance_lv")]
  public int ReqAllianceLev;
  [Config(Name = "init_durability")]
  public int InitDurability;
  [Config(Name = "max_durability")]
  public int MaxDurability;
  [Config(Name = "invalid_durability")]
  public int InvalidDurability;
  [Config(Name = "effective_area")]
  public int EffectiveArea;
  [Config(Name = "tile_number")]
  public int TileNumber;
  [Config(Name = "resource_type")]
  public string ResourceType;
  [Config(Name = "amount_stored")]
  public long AmountStored;
  [Config(Name = "gather_rate")]
  public float GatherRate;
  [Config(Name = "param_1")]
  public float Param1;

  public string LocalName
  {
    get
    {
      return ScriptLocalization.Get(this.BuildName, true);
    }
  }

  public string LocalDesc
  {
    get
    {
      return ScriptLocalization.Get(this.BuildDesc, true);
    }
  }

  public string IconPath
  {
    get
    {
      return this.Icon;
    }
  }
}
