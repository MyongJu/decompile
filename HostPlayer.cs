﻿// Decompiled with JetBrains decompiler
// Type: HostPlayer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

public class HostPlayer : PlayerInfo
{
  public void SetUid(long inUid)
  {
    this.uid = inUid;
  }

  public int Currency
  {
    get
    {
      return (int) PlayerData.inst.userData.currency.gold;
    }
  }

  public int Power
  {
    get
    {
      return (int) PlayerData.inst.userData.power;
    }
  }

  public int VIPPoints
  {
    get
    {
      return (int) PlayerData.inst.userData.vipPoint;
    }
  }

  public int VIPLevel
  {
    get
    {
      VIP_Level_Info vipLevelInfo = ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints(this.VIPPoints);
      if (vipLevelInfo == null)
        return 0;
      return vipLevelInfo.Level;
    }
  }

  public VIP_Level_Info VIPLevelInfo
  {
    get
    {
      return ConfigManager.inst.DB_VIP_Level.VIPInfoForPoints(this.VIPPoints);
    }
  }

  public bool VIPActive
  {
    get
    {
      return PlayerData.inst.userData.vipJobId != 0L;
    }
  }

  public long VipJobId
  {
    get
    {
      return PlayerData.inst.userData.vipJobId;
    }
  }

  public int ConsecutiveLoginDays { get; set; }

  public int NextLoginBonus
  {
    get
    {
      return Math.Min((int) ConfigManager.inst.DB_GameConfig.GetData("vip_point_reward_max").Value, (int) ConfigManager.inst.DB_GameConfig.GetData("vip_base_point_reward").Value + this.ConsecutiveLoginDays * (int) ConfigManager.inst.DB_GameConfig.GetData("vip_incremental_point_reward").Value);
    }
  }

  public int LoginBonus
  {
    get
    {
      return Math.Min((int) ConfigManager.inst.DB_GameConfig.GetData("vip_point_reward_max").Value, (int) ConfigManager.inst.DB_GameConfig.GetData("vip_base_point_reward").Value + (this.ConsecutiveLoginDays - 1) * (int) ConfigManager.inst.DB_GameConfig.GetData("vip_incremental_point_reward").Value);
    }
  }

  public void Init(object res)
  {
  }
}
