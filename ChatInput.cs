﻿// Decompiled with JetBrains decompiler
// Type: ChatInput
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Chat Input")]
[RequireComponent(typeof (UIInput))]
public class ChatInput : MonoBehaviour
{
  public UITextList textList;
  public bool fillWithDummyData;
  private UIInput mInput;

  private void Start()
  {
    this.mInput = this.GetComponent<UIInput>();
    this.mInput.label.maxLineCount = 1;
    if (!this.fillWithDummyData || !((Object) this.textList != (Object) null))
      return;
    for (int index = 0; index < 30; ++index)
      this.textList.Add((index % 2 != 0 ? (object) "[AAAAAA]" : (object) "[FFFFFF]").ToString() + "This is an example paragraph for the text list, testing line " + (object) index + "[-]");
  }

  public void OnSubmit()
  {
    if (!((Object) this.textList != (Object) null))
      return;
    string text = NGUIText.StripSymbols(this.mInput.value);
    if (string.IsNullOrEmpty(text))
      return;
    this.textList.Add(text);
    this.mInput.value = string.Empty;
    this.mInput.isSelected = false;
  }
}
