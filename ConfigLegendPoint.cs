﻿// Decompiled with JetBrains decompiler
// Type: ConfigLegendPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLegendPoint
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, LegendPointInfo> datas;
  private Dictionary<int, LegendPointInfo> dicByUniqueId;
  private List<LegendPointInfo> pointList;

  public void BuildDB(object res)
  {
    this.parse.Parse<LegendPointInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.pointList = new List<LegendPointInfo>();
    Dictionary<int, LegendPointInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
      this.pointList.Add(enumerator.Current.Value);
    this.pointList.Sort(new Comparison<LegendPointInfo>(ConfigLegendPoint.Compare));
  }

  private static int Compare(LegendPointInfo a, LegendPointInfo b)
  {
    return a.totalXP - b.totalXP;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, LegendPointInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, LegendPointInfo>) null;
  }

  public int GetLegendLevelByXP(long xp, out int nextXp)
  {
    int num = this.GetLegendMaxLevel();
    nextXp = this.GetLegenMaxPoint();
    int count = this.pointList.Count;
    for (int index = 0; index < count; ++index)
    {
      LegendPointInfo point = this.pointList[index];
      if (xp < (long) point.totalXP)
      {
        num = point.legendLevel - 1;
        nextXp = point.totalXP;
        break;
      }
    }
    return num;
  }

  public int GetLegendMaxLevel()
  {
    return this.pointList[this.pointList.Count - 1].legendLevel;
  }

  public int GetLegenMaxPoint()
  {
    return this.pointList[this.pointList.Count - 1].totalXP;
  }

  public LegendPointInfo GetLegendPointInfo(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (LegendPointInfo) null;
  }

  public LegendPointInfo GetLegendPointInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (LegendPointInfo) null;
  }
}
