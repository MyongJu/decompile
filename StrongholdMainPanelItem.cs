﻿// Decompiled with JetBrains decompiler
// Type: StrongholdMainPanelItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StrongholdMainPanelItem : MonoBehaviour
{
  public UILabel mItem1;
  public UILabel mItem2;
  public UILabel mItem3;

  public void SetDetails(string item1, string item2, string item3)
  {
    this.mItem1.text = item1;
    this.mItem2.text = item2;
    this.mItem3.text = item3;
  }
}
