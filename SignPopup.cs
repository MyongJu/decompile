﻿// Decompiled with JetBrains decompiler
// Type: SignPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;

public class SignPopup : Popup
{
  public SignSlot[] m_Slots;
  public UIButton m_Collect;
  public UILabel m_Tip;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TutorialManager.Instance.Pause();
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    TutorialManager.Instance.Resume();
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCollectButtonPressed()
  {
    MessageHub.inst.GetPortByAction("Player:signIn").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      AudioManager.Instance.StopAndPlaySound("sfx_ui_signin");
      this.UpdateUI();
      IDictionaryEnumerator enumerator = (data as Hashtable).GetEnumerator();
      RewardsCollectionAnimator.Instance.Clear();
      while (enumerator.MoveNext())
      {
        try
        {
          int internalId = int.Parse(enumerator.Key.ToString());
          int num = int.Parse(enumerator.Value.ToString());
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
          {
            count = (float) num,
            icon = itemStaticInfo.ImagePath
          });
        }
        catch (FormatException ex)
        {
          D.error((object) "{0}: item_id = {1}, count = {2}", (object) ex.Message, enumerator.Key, enumerator.Value);
        }
      }
      RewardsCollectionAnimator.Instance.CollectItems(true);
    }), true);
  }

  private void UpdateUI()
  {
    int signInDays = Utils.GetSignInDays();
    this.m_Collect.isEnabled = false;
    for (int slotIndex = 0; slotIndex < 7; ++slotIndex)
    {
      SignInData byTimes = ConfigManager.inst.DB_SignIn.GetByTimes(slotIndex + 1);
      this.m_Slots[slotIndex].SetData(byTimes, slotIndex);
      if (slotIndex < signInDays)
        this.m_Slots[slotIndex].SetPast();
      else if (slotIndex == signInDays)
      {
        if (Utils.IsSignedIn())
        {
          this.m_Slots[slotIndex].SetNormal();
        }
        else
        {
          this.m_Slots[slotIndex].SetUnsigned();
          this.m_Collect.isEnabled = true;
        }
      }
      else
        this.m_Slots[slotIndex].SetNormal();
    }
    string str = "00FF00";
    string format = "[{0}]{1}[-]";
    if (signInDays <= 1)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["num"] = string.Format(format, (object) str, (object) signInDays);
      this.m_Tip.text = ScriptLocalization.GetWithPara("continuous_sign_in_total_number_description", para, true);
    }
    else
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["num"] = string.Format(format, (object) str, (object) signInDays);
      this.m_Tip.text = ScriptLocalization.GetWithPara("continuous_sign_in_total_number_description", para, true);
    }
  }
}
