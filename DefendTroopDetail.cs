﻿// Decompiled with JetBrains decompiler
// Type: DefendTroopDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class DefendTroopDetail
{
  public string amount;
  public Dictionary<string, string> type;
  public Dictionary<string, string> tier;

  public void SummarizeDefendTrap(ref DefendInfo.Data did)
  {
    did.amount = long.Parse(this.amount);
    if (this.tier == null)
      return;
    did.tier = new Dictionary<string, List<ScoutBar.Data>>();
    using (Dictionary<string, string>.Enumerator enumerator = this.tier.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        Unit_StatisticsInfo data1 = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
        string type = data1.Type;
        ScoutBar.Data data2 = new ScoutBar.Data();
        data2.icon = data1.Troop_ICON;
        data2.name = ScriptLocalization.Get(data1.Troop_Name_LOC_ID, true);
        data2.count = current.Value.ToString();
        if (did.tier.ContainsKey(type))
          did.tier[type].Add(data2);
        else
          did.tier.Add(type, new List<ScoutBar.Data>()
          {
            data2
          });
      }
    }
  }

  public void SummarizeDefendTroop(ref DefendInfo.Data did)
  {
    did.amount = long.Parse(this.amount);
    if (this.type != null)
      did.type = this.type;
    if (this.tier == null)
      return;
    did.tier = new Dictionary<string, List<ScoutBar.Data>>();
    using (Dictionary<string, string>.Enumerator enumerator = this.tier.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        Unit_StatisticsInfo data1 = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
        string type = data1.Type;
        ScoutBar.Data data2 = new ScoutBar.Data();
        data2.icon = data1.Troop_ICON;
        data2.name = ScriptLocalization.Get(data1.Troop_Name_LOC_ID, true);
        data2.count = current.Value.ToString();
        if (did.tier.ContainsKey(type))
          did.tier[type].Add(data2);
        else
          did.tier.Add(type, new List<ScoutBar.Data>()
          {
            data2
          });
      }
    }
  }

  public void SummarizeDefendReinforcement(ref DefendInfo.Data did)
  {
    this.SummarizeDefendTroop(ref did);
  }
}
