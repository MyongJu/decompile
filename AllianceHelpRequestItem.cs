﻿// Decompiled with JetBrains decompiler
// Type: AllianceHelpRequestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UnityEngine;

public class AllianceHelpRequestItem : MonoBehaviour
{
  public UITexture m_RankIcon;
  public UILabel m_PlayerName;
  public UILabel m_TaskLabel;
  public UILabel m_HelpCountLabel;
  public UIProgressBar m_ProgressBar;
  private AllianceHelpData m_AllianceHelpData;

  public void SetDetails(AllianceHelpData help)
  {
    this.m_AllianceHelpData = help;
    if (this.m_AllianceHelpData == null)
      return;
    UserData userData = DBManager.inst.DB_User.Get(help.identifier.uid);
    if (userData != null)
    {
      this.m_PlayerName.text = userData.userName;
      CustomIconLoader.Instance.requestCustomIcon(this.m_RankIcon, userData.PortraitIconPath, userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.m_RankIcon, userData.LordTitle, 1);
    }
    else
    {
      D.error((object) "[AllianceHelpRequestItem]SetDetails: Bad user data!");
      this.m_PlayerName.text = string.Empty;
    }
    this.m_TaskLabel.text = this.GetTaskString(help.trace);
    int logCount = help.GetLogCount();
    int num = help.helpAvailable + logCount;
    this.m_HelpCountLabel.text = string.Format("{0}/{1}", (object) logCount, (object) num);
    this.m_ProgressBar.value = (float) logCount / (float) num;
  }

  private string GetTaskString(Hashtable ht)
  {
    if (ht == null || !ht.ContainsKey((object) "event_type"))
    {
      Debug.LogError((object) string.Format("[AllianceHelpRequestItem]GetTaskString: missing event_type! <{0}>", (object) this.m_AllianceHelpData.identifier.ToString()));
      return string.Empty;
    }
    int result1 = 0;
    int.TryParse(ht[(object) "event_type"].ToString(), out result1);
    JobEvent jobEvent = (JobEvent) result1;
    switch (jobEvent)
    {
      case JobEvent.JOB_BUILDING_CONSTRUCTION:
        int result2 = 0;
        int.TryParse(ht[(object) "building_internal_id"].ToString(), out result2);
        BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData(result2);
        return string.Format(ScriptLocalization.Get("alliance_help_uppercase_build_description", true), (object) (data1.Building_Lvl + 1), (object) ScriptLocalization.Get(data1.Building_LOC_ID, true));
      case JobEvent.JOB_BUILDING_DECONSTRUCTION:
        int result3 = 0;
        int.TryParse(ht[(object) "building_internal_id"].ToString(), out result3);
        BuildingInfo data2 = ConfigManager.inst.DB_Building.GetData(result3);
        return string.Format(ScriptLocalization.Get("alliance_help_uppercase_demolish_description", true), (object) data2.Building_Lvl, (object) ScriptLocalization.Get(data2.Building_LOC_ID, true));
      case JobEvent.JOB_RESEARCH:
        int result4 = 0;
        int.TryParse(ht[(object) "research_id"].ToString(), out result4);
        TechLevel techLevel = ResearchManager.inst.GetTechLevel(result4);
        return string.Format(ScriptLocalization.Get("alliance_help_uppercase_research_description", true), (object) techLevel.Level, (object) ScriptLocalization.Get(techLevel.Name, true));
      default:
        if (jobEvent == JobEvent.JOB_HEALING_TROOPS)
          return ScriptLocalization.Get("alliance_help_uppercase_heal_description", true);
        if (jobEvent == JobEvent.JOB_FORGE)
          return string.Format(ScriptLocalization.Get("alliance_help_uppercase_forge_description", true), (object) ConfigManager.inst.GetEquipmentMain(BagType.Hero).GetData(int.Parse(ht[(object) "equip_id"].ToString())).LocName);
        if (jobEvent == JobEvent.JOB_DK_FORGE)
          return string.Format(ScriptLocalization.Get("alliance_help_uppercase_forge_description", true), (object) ConfigManager.inst.GetEquipmentMain(BagType.DragonKnight).GetData(int.Parse(ht[(object) "equip_id"].ToString())).LocName);
        return string.Empty;
    }
  }
}
