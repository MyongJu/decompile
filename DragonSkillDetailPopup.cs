﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillDetailPopup : Popup
{
  private List<DragonSkillPropertyChangeItem> _allPropertyChangeItem = new List<DragonSkillPropertyChangeItem>();
  public UILabel m_SkillName;
  public UILabel m_SkillLevel;
  public UITexture m_SkillIcon;
  public UIButton m_Distribute;
  public UIButton m_Undistribute;
  public UILabel m_extraLevel;
  public GameObject m_rootStarLevel;
  public UILabel m_labelStarLevel;
  [SerializeField]
  private DragonSkillPropertyChangeItem _propertyChangeItemTemplate;
  [SerializeField]
  private UITable _tableBenefits;
  [SerializeField]
  private UIScrollView _scrollViewBenefits;
  private DragonSkillDetailPopup.Parameter m_Parameter;
  private ConfigDragonSkillMainInfo m_DragonSkillMainInfo;
  private ConfigDragonSkillGroupInfo m_DragonSkillGroupInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as DragonSkillDetailPopup.Parameter;
    this._propertyChangeItemTemplate.gameObject.SetActive(false);
    this.m_DragonSkillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(this.m_Parameter.skillId);
    this.m_DragonSkillGroupInfo = this.m_DragonSkillMainInfo == null ? (ConfigDragonSkillGroupInfo) null : ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(this.m_DragonSkillMainInfo.group_id);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  private void UpdateUI()
  {
    this.m_rootStarLevel.SetActive(false);
    this.m_Distribute.gameObject.SetActive(this.m_Parameter.isDistribute);
    this.m_Undistribute.gameObject.SetActive(!this.m_Parameter.isDistribute);
    if (this.m_DragonSkillMainInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_DragonSkillGroupInfo.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_SkillName.text = Utils.XLAT(this.m_DragonSkillGroupInfo.localization_name);
    this.m_SkillLevel.text = this.m_DragonSkillMainInfo.level.ToString();
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, this.m_DragonSkillGroupInfo.internalId);
    this.m_extraLevel.text = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
    List<Benefits.BenefitValuePair> benefitsPairs = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfoOfGrounpByLevel(this.m_DragonSkillMainInfo.group_id, this.m_DragonSkillMainInfo.level + extarLevel, true).benefit.GetBenefitsPairs();
    int internalId = benefitsPairs[0].internalID;
    double num = (double) benefitsPairs[0].value;
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[internalId];
    if (PlayerData.inst.dragonData != null)
    {
      int skillGroupStarLevel = PlayerData.inst.dragonData.GetSkillGroupStarLevel(this.m_DragonSkillMainInfo.group_id);
      if (skillGroupStarLevel > 0)
      {
        this.m_rootStarLevel.SetActive(true);
        this.m_labelStarLevel.text = skillGroupStarLevel.ToString();
      }
    }
    if (this.m_Parameter.isDistribute)
      this.m_Distribute.isEnabled = !PlayerData.inst.dragonData.Skills.IsSetupSkill(this.m_Parameter.usability, this.m_Parameter.skillId) && !PlayerData.inst.dragonController.IsFull(this.m_Parameter.usability);
    this.UpdateBenefits();
  }

  protected void UpdateBenefits()
  {
    using (List<DragonSkillPropertyChangeItem>.Enumerator enumerator = this._allPropertyChangeItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DragonSkillPropertyChangeItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.parent = (Transform) null;
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._allPropertyChangeItem.Clear();
    if (this.m_DragonSkillMainInfo == null)
      return;
    List<Benefits.BenefitValuePair> benefitsPairs = this.m_DragonSkillMainInfo.benefit.GetBenefitsPairs();
    for (int index = 0; index < benefitsPairs.Count; ++index)
    {
      GameObject go = Utils.DuplicateGOB(this._propertyChangeItemTemplate.gameObject);
      NGUITools.SetActive(go, true);
      DragonSkillPropertyChangeItem component = go.GetComponent<DragonSkillPropertyChangeItem>();
      component.SetData(benefitsPairs[index], (Benefits.BenefitValuePair) null, false);
      this._allPropertyChangeItem.Add(component);
    }
    this._tableBenefits.Reposition();
    this._scrollViewBenefits.ResetPosition();
  }

  public void OnEquip()
  {
    this.OnCloseClick();
    if (this.m_Parameter.OnEquip == null)
      return;
    this.m_Parameter.OnEquip(this.m_DragonSkillMainInfo.internalId);
  }

  public void OnUnEquip()
  {
    this.OnCloseClick();
    if (this.m_Parameter.OnUnEquip == null)
      return;
    this.m_Parameter.OnUnEquip(this.m_DragonSkillMainInfo.internalId);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string usability;
    public int skillId;
    public bool isDistribute;
    public OnDragonSkillEquipCallback OnEquip;
    public OnDragonSkillUnEquipCallback OnUnEquip;
  }
}
