﻿// Decompiled with JetBrains decompiler
// Type: SpriteAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UISprite))]
[ExecuteInEditMode]
public class SpriteAnimation : MonoBehaviour
{
  [SerializeField]
  private int frameRate = 30;
  [SerializeField]
  private string namePrefix = string.Empty;
  [SerializeField]
  private bool loop = true;
  private bool mActive = true;
  private List<string> mSpriteNames = new List<string>();
  private UISprite mSprite;
  private float mDelta;
  private int mIndex;

  public int FrameCount
  {
    get
    {
      return this.mSpriteNames.Count;
    }
  }

  public int FramesPerSecond
  {
    get
    {
      return this.frameRate;
    }
    set
    {
      this.frameRate = value;
    }
  }

  public string NamePrefix
  {
    get
    {
      return this.namePrefix;
    }
    set
    {
      if (!(this.namePrefix != value))
        return;
      this.namePrefix = value;
      this.RebuildSpriteList();
    }
  }

  public bool Loop
  {
    get
    {
      return this.loop;
    }
    set
    {
      this.loop = value;
    }
  }

  public bool isPlaying
  {
    get
    {
      return this.mActive;
    }
  }

  private void Start()
  {
    this.RebuildSpriteList();
  }

  private void Update()
  {
    if (!this.mActive || this.mSpriteNames.Count <= 1 || (!Application.isPlaying || (double) this.frameRate <= 0.0))
      return;
    this.mDelta += RealTime.deltaTime;
    float num = 1f / (float) this.frameRate;
    if ((double) num >= (double) this.mDelta)
      return;
    this.mDelta = (double) num <= 0.0 ? 0.0f : this.mDelta - num;
    if (++this.mIndex >= this.mSpriteNames.Count)
    {
      this.mIndex = 0;
      this.mActive = this.loop;
    }
    if (!this.mActive)
      return;
    this.mSprite.spriteName = this.mSpriteNames[this.mIndex];
  }

  public void RebuildSpriteList()
  {
    if ((Object) this.mSprite == (Object) null)
      this.mSprite = this.GetComponent<UISprite>();
    this.mSpriteNames.Clear();
    if (!((Object) this.mSprite != (Object) null) || !((Object) this.mSprite.atlas != (Object) null))
      return;
    List<UISpriteData> spriteList = this.mSprite.atlas.spriteList;
    int index = 0;
    for (int count = spriteList.Count; index < count; ++index)
    {
      UISpriteData uiSpriteData = spriteList[index];
      if (string.IsNullOrEmpty(this.namePrefix) || uiSpriteData.name.StartsWith(this.namePrefix))
        this.mSpriteNames.Add(uiSpriteData.name);
    }
    this.mSpriteNames.Sort();
  }

  public void Reset()
  {
    this.mActive = true;
    this.mIndex = 0;
    if (!((Object) this.mSprite != (Object) null) || this.mSpriteNames.Count <= 0)
      return;
    this.mSprite.spriteName = this.mSpriteNames[this.mIndex];
  }
}
