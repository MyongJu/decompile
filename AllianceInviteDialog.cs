﻿// Decompiled with JetBrains decompiler
// Type: AllianceInviteDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceInviteDialog : UI.Dialog
{
  private Dictionary<long, AllianceInviteItemRenderer> m_ItemDict = new Dictionary<long, AllianceInviteItemRenderer>();
  public UIViewport m_Viewport;
  public UIInput m_SearchInput;
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  public GameObject m_Loading;
  private bool m_Language;
  private bool m_ShouldResetPosition;
  private int m_Offset;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Loading.SetActive(false);
    this.m_Viewport.gameObject.SetActive(false);
    this.m_Viewport.sourceCamera = UIManager.inst.ui2DCamera;
    this.m_SearchInput.defaultText = ScriptLocalization.Get("id_search_bar_placeholder", true);
    this.m_SearchInput.value = string.Empty;
    this.m_ScrollView.onDragFinished += new UIScrollView.OnDragNotification(this.OnDragFinished);
    this.m_Language = true;
    this.SendRequest();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.m_ScrollView.onDragFinished -= new UIScrollView.OnDragNotification(this.OnDragFinished);
  }

  private void OnDragFinished()
  {
    Bounds bounds = this.m_ScrollView.bounds;
    if ((double) this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max).y > -200.0)
      return;
    this.m_ShouldResetPosition = false;
    this.SendRequest();
  }

  private void Update()
  {
    Bounds bounds = this.m_ScrollView.bounds;
    Vector3 constrainOffset = this.m_ScrollView.panel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!this.m_ScrollView.canMoveHorizontally)
      constrainOffset.x = 0.0f;
    if (!this.m_ScrollView.canMoveVertically)
      constrainOffset.y = 0.0f;
    this.m_Loading.SetActive((double) constrainOffset.y <= -200.0);
  }

  private void ClearData()
  {
    using (Dictionary<long, AllianceInviteItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceInviteItemRenderer current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemDict.Clear();
    this.m_Viewport.gameObject.SetActive(false);
  }

  public void OnSearchButtonClick()
  {
    this.ClearData();
    this.m_Language = true;
    this.m_Offset = 0;
    this.m_ShouldResetPosition = true;
    this.SendRequest();
  }

  private void SendRequest()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "lang"] = (object) this.m_Language;
    postData[(object) "offset"] = (object) this.m_Offset;
    postData[(object) "context"] = (object) this.m_SearchInput.value;
    MessageHub.inst.GetPortByAction("Alliance:searchUserByAlliance").SendRequest(postData, new System.Action<bool, object>(this.AllianceMemberListCallback), true);
  }

  private void AllianceMemberListCallback(bool ret, object data)
  {
    try
    {
      Hashtable hashtable = data as Hashtable;
      this.m_Language = (bool) hashtable[(object) "lang"];
      this.m_Offset = int.Parse(hashtable[(object) "offset"] as string);
      this.m_Viewport.gameObject.SetActive(true);
      ArrayList arrayList = hashtable[(object) "res"] as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        InviteSearchData inviteData = new InviteSearchData();
        inviteData.Decode(arrayList[index]);
        if (!this.m_ItemDict.ContainsKey(inviteData.uid))
        {
          GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab);
          gameObject.SetActive(true);
          AllianceInviteItemRenderer component = gameObject.GetComponent<AllianceInviteItemRenderer>();
          component.SetData(inviteData);
          this.m_ItemDict.Add(inviteData.uid, component);
        }
      }
      this.m_Grid.Reposition();
      if (!this.m_ShouldResetPosition)
        return;
      this.m_ScrollView.ResetPosition();
    }
    catch
    {
    }
  }
}
