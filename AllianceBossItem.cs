﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class AllianceBossItem : MonoBehaviour
{
  [SerializeField]
  protected GameObject m_rootFront;
  [SerializeField]
  protected GameObject m_rootBack;
  [SerializeField]
  protected GameObject m_rootUnlocked;
  [SerializeField]
  protected GameObject m_rootLocked;
  [SerializeField]
  protected UILabel m_labelBossName;
  [SerializeField]
  protected UILabel m_labelHitpoint;
  [SerializeField]
  protected UITexture m_textureBossQuality;
  [SerializeField]
  protected UITexture m_textureBossIcon;
  [SerializeField]
  protected GameObject m_rootSelect;
  [SerializeField]
  protected UITexture[] m_allKillRewardIcons;
  [SerializeField]
  protected UILabel[] m_allKillRewardCounts;
  [SerializeField]
  protected UITexture[] m_allDamageRewardIcons;
  [SerializeField]
  protected UILabel[] m_allDamageRewardCounts;
  [SerializeField]
  protected GameObject[] m_allNeedGreyWhenLocked;
  protected AllianceBossInfo m_bossInfo;
  protected bool m_unlocked;
  public AllianceBossItem.OnSelectCallBack OnSelect;
  private bool m_playing;

  public bool Select
  {
    get
    {
      return this.m_rootSelect.activeSelf;
    }
    set
    {
      if (value)
      {
        if (!this.m_unlocked)
          return;
        this.m_rootSelect.SetActive(value);
      }
      else
        this.m_rootSelect.SetActive(value);
    }
  }

  private AllianceBossInfo GetBossInfo()
  {
    return this.m_bossInfo;
  }

  public void SetBossInfo(AllianceBossInfo bossInfo)
  {
    this.m_bossInfo = bossInfo;
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData == null)
    {
      D.error((object) "there is no alliance data");
    }
    else
    {
      this.m_rootFront.SetActive(true);
      this.m_rootBack.SetActive(false);
      this.m_unlocked = this.m_bossInfo.bossLevel <= allianceData.bossLevel;
      this.m_rootUnlocked.SetActive(this.m_unlocked);
      this.m_rootLocked.SetActive(!this.m_unlocked);
      if (this.m_allNeedGreyWhenLocked != null)
      {
        foreach (GameObject gameObject in this.m_allNeedGreyWhenLocked)
        {
          if (!this.m_unlocked)
          {
            GreyUtility.Grey(gameObject);
            foreach (UIButton componentsInChild in gameObject.GetComponentsInChildren<UIButton>())
              componentsInChild.hover = componentsInChild.pressed = componentsInChild.disabledColor = new Color(0.0f, 0.0f, 1f, 1f);
          }
        }
      }
      this.m_labelBossName.text = ScriptLocalization.Get(this.m_bossInfo.name, true);
      this.m_labelHitpoint.text = Utils.FormatThousands(this.m_bossInfo.TroopCount.ToString());
      Utils.SetPortrait(this.m_textureBossIcon, this.m_bossInfo.ImagePath);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_textureBossQuality, this.m_bossInfo.StageImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.m_rootSelect.SetActive(false);
      List<Reward.RewardsValuePair> allRewards = this.m_bossInfo.AllRewards;
      for (int index = 0; index < this.m_allKillRewardIcons.Length; ++index)
        this.m_allKillRewardIcons[index].transform.parent.gameObject.SetActive(false);
      int index1 = 0;
      if (this.m_bossInfo.fund > 0L)
      {
        this.m_allKillRewardIcons[index1].transform.parent.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allKillRewardIcons[index1], "Texture/Alliance/alliance_fund", (System.Action<bool>) null, true, false, string.Empty);
        this.m_allKillRewardCounts[index1].text = this.m_bossInfo.fund.ToString();
        ++index1;
      }
      if (this.m_bossInfo.honor > 0L)
      {
        this.m_allKillRewardIcons[index1].transform.parent.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allKillRewardIcons[index1], "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
        this.m_allKillRewardCounts[index1].text = this.m_bossInfo.honor.ToString();
        ++index1;
      }
      for (int index2 = 0; index2 < allRewards.Count; ++index2)
      {
        int internalId = allRewards[index2].internalID;
        bool flag = internalId != 0;
        if (flag)
        {
          this.m_allKillRewardIcons[index1].transform.parent.gameObject.SetActive(flag);
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          if (itemStaticInfo != null)
          {
            BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allKillRewardIcons[index1], itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
            this.m_allKillRewardCounts[index1].text = allRewards[index2].value.ToString();
          }
          ++index1;
        }
      }
      AllianceBossRankRewardInfo bossRankRewardInfo = ConfigManager.inst.DB_AllianceBossRankReward.GetAllianceBossRankRewardInfoList().Find((Predicate<AllianceBossRankRewardInfo>) (p => p.rank == 1));
      float[] numArray = new float[6]
      {
        bossRankRewardInfo.rewardRatio1,
        bossRankRewardInfo.rewardRatio2,
        bossRankRewardInfo.rewardRatio3,
        bossRankRewardInfo.rewardRatio4,
        bossRankRewardInfo.rewardRatio5,
        bossRankRewardInfo.rewardRatio6
      };
      for (int index2 = 0; index2 < this.m_allDamageRewardIcons.Length; ++index2)
        this.m_allDamageRewardIcons[index2].transform.parent.gameObject.SetActive(false);
      int index3 = 0;
      if (this.m_bossInfo.fund > 0L && (double) bossRankRewardInfo.fundRatio > 0.0)
      {
        this.m_allDamageRewardIcons[index3].transform.parent.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allDamageRewardIcons[index3], "Texture/Alliance/alliance_fund", (System.Action<bool>) null, true, false, string.Empty);
        this.m_allDamageRewardCounts[index3].text = ((int) ((double) bossRankRewardInfo.fundRatio * (double) this.m_bossInfo.fund)).ToString();
        ++index3;
      }
      if (this.m_bossInfo.honor > 0L && (double) bossRankRewardInfo.honorRatio > 0.0)
      {
        this.m_allDamageRewardIcons[index3].transform.parent.gameObject.SetActive(true);
        BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allDamageRewardIcons[index3], "Texture/Alliance/alliance_honor", (System.Action<bool>) null, true, false, string.Empty);
        this.m_allDamageRewardCounts[index3].text = ((int) ((double) bossRankRewardInfo.honorRatio * (double) this.m_bossInfo.honor)).ToString();
        ++index3;
      }
      for (int index2 = 0; index2 < allRewards.Count; ++index2)
      {
        int internalId = allRewards[index2].internalID;
        bool flag = internalId != 0 && (double) numArray[index2] > 0.0;
        if (flag)
        {
          this.m_allDamageRewardIcons[index3].transform.parent.gameObject.SetActive(flag);
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          if (itemStaticInfo != null)
          {
            BuilderFactory.Instance.HandyBuild((UIWidget) this.m_allDamageRewardIcons[index3], itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
            this.m_allDamageRewardCounts[index3].text = ((int) ((double) numArray[index2] * (double) allRewards[index2].value)).ToString();
          }
          ++index3;
        }
      }
    }
  }

  public AllianceBossInfo getAllianceBossInfo()
  {
    return this.m_bossInfo;
  }

  public void OnButtonInformationClicked()
  {
    if (this.m_playing)
      return;
    this.StartCoroutine(this.playToggleAnimation(this.m_rootFront, this.m_rootBack));
  }

  [DebuggerHidden]
  public IEnumerator playToggleAnimation(GameObject previous, GameObject final)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceBossItem.\u003CplayToggleAnimation\u003Ec__Iterator23()
    {
      previous = previous,
      final = final,
      \u003C\u0024\u003Eprevious = previous,
      \u003C\u0024\u003Efinal = final,
      \u003C\u003Ef__this = this
    };
  }

  public void OnClicked()
  {
    if (this.m_unlocked)
      this.OnSelect(this);
    if (this.m_playing || !this.m_rootBack.activeSelf)
      return;
    this.StartCoroutine(this.playToggleAnimation(this.m_rootBack, this.m_rootFront));
  }

  public delegate void OnSelectCallBack(AllianceBossItem selectItem);
}
