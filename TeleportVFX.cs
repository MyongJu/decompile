﻿// Decompiled with JetBrains decompiler
// Type: TeleportVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TeleportVFX : MonoBehaviour
{
  private void Update()
  {
    Rect clipViewport = PVPSystem.Instance.Map.GetClipViewport();
    Vector3 point = 1f / GameEngine.Instance.tileMapScale * this.transform.position;
    if (!clipViewport.Contains(point, true))
      return;
    this.UpdateVFXSortingOrder(clipViewport, point.y, 2f);
  }

  private void UpdateVFXSortingOrder(Rect viewport, float py, float ySize)
  {
    int sortingLayerIndex = PVPTile.CalculateSortingLayerIndex(viewport, py, ySize, PVPMapData.MapData);
    RendererSortingLayerIdModifier componentInChildren = this.gameObject.GetComponentInChildren<RendererSortingLayerIdModifier>();
    if (!((Object) componentInChildren != (Object) null))
      return;
    componentInChildren.SortingLayerName = PVPTile.GetSortingLayerName(sortingLayerIndex);
  }
}
