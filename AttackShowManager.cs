﻿// Decompiled with JetBrains decompiler
// Type: AttackShowManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class AttackShowManager : MonoBehaviour
{
  private const string boardpath = "Prefab/AnniversaryAttackShow/AttackShowBaseboard";
  private const string terrainpath = "Prefab/AnniversaryAttackShow/AttackShowTerrain";
  private const string citypath = "Prefab/AnniversaryAttackShow/AttackShowCity";
  private const string skippath = "Prefab/AnniversaryAttackShow/AttackShowSkip";
  private static AttackShowManager _instance;
  private bool isRunning;
  private AttackShowPara _para;
  private Coroutine _attackShowCoroutine;
  public AttackShowController board;
  private GameObject skipBtn;
  private AttackShowTargetCity _attackShowTargetCity;
  public System.Action onAttackShowTerminate;
  public System.Action onAttackShowBegin;

  public static AttackShowManager Instance
  {
    get
    {
      if ((UnityEngine.Object) AttackShowManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        AttackShowManager._instance = gameObject.AddComponent<AttackShowManager>();
        gameObject.AddComponent<DontDestroy>();
        gameObject.name = nameof (AttackShowManager);
        if ((UnityEngine.Object) null == (UnityEngine.Object) AttackShowManager._instance)
          throw new ArgumentException("AttackShowManager hasn't been created yet.");
      }
      return AttackShowManager._instance;
    }
  }

  public bool IsRunning
  {
    get
    {
      return this.isRunning;
    }
  }

  public AttackShowPara Param
  {
    get
    {
      return this._para;
    }
  }

  public void Startup(AttackShowPara para)
  {
    if (para == null)
      return;
    this._para = para;
    this.isRunning = true;
    this.PlayAttackShow();
  }

  public void Terminate()
  {
    this.isRunning = false;
    AudioManager.Instance.PlayBGM("bgm_city2");
    if ((UnityEngine.Object) null != (UnityEngine.Object) this._attackShowTargetCity)
      this._attackShowTargetCity.Dispose();
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.board)
    {
      this.board.Dispose();
      UnityEngine.Object.Destroy((UnityEngine.Object) this.board.gameObject);
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.skipBtn)
    {
      this.skipBtn.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.skipBtn);
    }
    if (this.onAttackShowTerminate == null)
      return;
    this.onAttackShowTerminate();
  }

  public void PlayAttackShow()
  {
    AssetManager.Instance.LoadAsync("Prefab/AnniversaryAttackShow/AttackShowBaseboard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      GameObject go = UnityEngine.Object.Instantiate(obj) as GameObject;
      AssetManager.Instance.UnLoadAsset("Prefab/AnniversaryAttackShow/AttackShowBaseboard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      this.board = go.GetComponent<AttackShowController>();
      this._attackShowCoroutine = Utils.ExecuteInSecs(this.board.battletime, (System.Action) (() => UIManager.inst.CloseTopPopup((Popup.PopupParameter) null)));
      if (!SettingManager.Instance.IsEnvironmentOff)
        AssetManager.Instance.LoadAsync("Prefab/AnniversaryAttackShow/AttackShowTerrain", (System.Action<UnityEngine.Object, bool>) ((objterrain, retterrain) =>
        {
          Utils.DuplicateGOB(objterrain as GameObject, go.transform).transform.localScale = Vector3.one;
          AssetManager.Instance.UnLoadAsset("Prefab/AnniversaryAttackShow/AttackShowTerrain", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
      AssetManager.Instance.LoadAsync("Prefab/AnniversaryAttackShow/AttackShowCity", (System.Action<UnityEngine.Object, bool>) ((objcity, retcity) =>
      {
        GameObject gameObject = Utils.DuplicateGOB(objcity as GameObject, go.transform);
        gameObject.transform.localScale = Vector3.one;
        this._attackShowTargetCity = gameObject.GetComponentInChildren<AttackShowTargetCity>();
        this._attackShowTargetCity.FeedData(this._para);
        this.board.PlayAction(1);
        AssetManager.Instance.UnLoadAsset("Prefab/AnniversaryAttackShow/AttackShowCity", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }), (System.Type) null);
      if (this.onAttackShowBegin == null)
        return;
      this.onAttackShowBegin();
    }), (System.Type) null);
  }

  public void StopAttackShowCoroutine()
  {
    if (this._attackShowCoroutine == null)
      return;
    Utils.StopCoroutine(this._attackShowCoroutine);
    this._attackShowCoroutine = (Coroutine) null;
  }
}
