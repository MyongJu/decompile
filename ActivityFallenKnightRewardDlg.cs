﻿// Decompiled with JetBrains decompiler
// Type: ActivityFallenKnightRewardDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityFallenKnightRewardDlg : UI.Dialog
{
  private int currentSelectedIndex = -1;
  private Dictionary<string, FallenKnightScoreReqItemRenderer> itemDict = new Dictionary<string, FallenKnightScoreReqItemRenderer>();
  private Dictionary<int, ActivityFallenKnightRewardDlg.RankRewardPair> dicRankReward = new Dictionary<int, ActivityFallenKnightRewardDlg.RankRewardPair>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollViewTabPage1;
  public UITable table1;
  public UIToggle[] tabCategories;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public GameObject[] tabPageContents;
  public ActivityFallenKnightRewardDlg.IconInfo[] itemTabPage2;
  public ActivityFallenKnightRewardDlg.IconInfo[] itemTabPage3;
  public UILabel unstartOrOverTimeLabel;
  public UILabel startedOrGoingTimeLabel;
  public UILabel Tab1AlliancePoints;
  public UILabel Tab1IndividualPoints;
  public UILabel Tab2AlliancePoints;
  public UILabel Tab2AllianceRank;
  public UILabel Tab3IndividualPoints;
  public UILabel Tab3IndividualRank;
  public UILabel Tab2RankNumLabel;
  public UILabel Tab3RankNumLabel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.AddEventHandler();
    this.ExecuteInSecond(0);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void ViewAllRewardsRank()
  {
    UIManager.inst.OpenPopup("Activity/FallenKnightRewardsPopup", (Popup.PopupParameter) new FallenKnightRewardsPopup.Parameter()
    {
      selectedIndex = this.currentSelectedIndex
    });
  }

  public void ViewAllianceRanking()
  {
    UIManager.inst.OpenPopup("Activity/FallenKnightAllianceRankPopup", (Popup.PopupParameter) null);
  }

  public void ViewIndividualRanking()
  {
    UIManager.inst.OpenPopup("Activity/FallenKnightIndividualRankPopup", (Popup.PopupParameter) null);
  }

  public void OnTabToggleUpdate()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == this.currentSelectedIndex || selectedIndex < 0)
      return;
    this.currentSelectedIndex = selectedIndex;
    this.ShowTabPageContents(this.currentSelectedIndex);
  }

  public void OnHelpBtnPressed()
  {
    UIManager.inst.OpenPopup("Activity/FallenKnightRewardsHelpPopup", (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    using (Dictionary<string, FallenKnightScoreReqItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void ShowRewardsScoreReq()
  {
    this.ClearData();
    List<FallenKnightScoreReqInfo> scoreReqList = ConfigManager.inst.DB_FallenKnightScoreReq.GetScoreReqList();
    for (int achievedScoreInfoIndex = ConfigManager.inst.DB_FallenKnightScoreReq.GetAchievedScoreInfoIndex(this.GetScoreOrRank("user_score"), this.GetScoreOrRank("alliance_score")); achievedScoreInfoIndex < scoreReqList.Count; ++achievedScoreInfoIndex)
    {
      FallenKnightScoreReqItemRenderer itemRenderer = this.CreateItemRenderer(scoreReqList[achievedScoreInfoIndex]);
      this.itemDict.Add(scoreReqList[achievedScoreInfoIndex].ID, itemRenderer);
    }
    this.Tab1AlliancePoints.text = string.Format(Utils.XLAT("event_fallen_knight_alliance_points"), (object) this.GetScoreOrRank("alliance_score"));
    this.Tab1IndividualPoints.text = string.Format(Utils.XLAT("event_fallen_knight_my_points"), (object) this.GetScoreOrRank("user_score"));
    this.Reposition();
  }

  private void Reposition()
  {
    this.table1.repositionNow = true;
    this.table1.Reposition();
    this.scrollViewTabPage1.ResetPosition();
    this.scrollViewTabPage1.gameObject.SetActive(false);
    this.scrollViewTabPage1.gameObject.SetActive(true);
  }

  private void ShowTabPageContents(int currentSelectedIndex)
  {
    for (int index = 0; index < this.tabPageContents.Length; ++index)
    {
      if (currentSelectedIndex == index)
        this.tabPageContents[index].SetActive(true);
      else
        this.tabPageContents[index].SetActive(false);
    }
    switch (currentSelectedIndex)
    {
      case 0:
        this.ShowRewardsScoreReq();
        break;
      case 1:
        this.ShowPointsRankingReward(ConfigFallenKnightRankReward.RankType.AllianceRank);
        break;
      case 2:
        this.ShowPointsRankingReward(ConfigFallenKnightRankReward.RankType.IndividualRank);
        break;
    }
  }

  private FallenKnightScoreReqItemRenderer CreateItemRenderer(FallenKnightScoreReqInfo info)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table1.gameObject);
    gameObject.SetActive(true);
    FallenKnightScoreReqItemRenderer component = gameObject.GetComponent<FallenKnightScoreReqItemRenderer>();
    component.SetData(info, this.GetScoreOrRank("user_score"), this.GetScoreOrRank("alliance_score"));
    return component;
  }

  private void ShowPointsRankingReward(ConfigFallenKnightRankReward.RankType type)
  {
    FallenKnightRankRewardInfo firstRankRewardInfo = ConfigManager.inst.DB_FallenKnightRankReward.GetFirstRankRewardInfo(type);
    this.dicRankReward.Clear();
    this.dicRankReward.Add(0, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = "alliance_fund",
      rankRewardCount = firstRankRewardInfo.Fund
    });
    this.dicRankReward.Add(1, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = "alliance_honor",
      rankRewardCount = firstRankRewardInfo.Honor
    });
    this.dicRankReward.Add(2, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_1,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_1
    });
    this.dicRankReward.Add(3, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_2,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_2
    });
    this.dicRankReward.Add(4, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_3,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_3
    });
    this.dicRankReward.Add(5, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_4,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_4
    });
    this.dicRankReward.Add(6, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_5,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_5
    });
    this.dicRankReward.Add(7, new ActivityFallenKnightRewardDlg.RankRewardPair()
    {
      rankRewardName = firstRankRewardInfo.RankReward_6,
      rankRewardCount = firstRankRewardInfo.RankRewardValue_6
    });
    if (firstRankRewardInfo == null)
      return;
    if (type == ConfigFallenKnightRankReward.RankType.AllianceRank && this.itemTabPage2.Length >= this.dicRankReward.Count)
    {
      int index1 = 0;
      int index2 = 0;
      for (; index1 < this.dicRankReward.Count; ++index1)
      {
        if (!string.IsNullOrEmpty(this.dicRankReward[index1].rankRewardName) && this.dicRankReward[index1].rankRewardCount > 0L)
        {
          IconData iconData = this.GetIconData(this.dicRankReward[index1].rankRewardName, this.dicRankReward[index1].rankRewardCount);
          this.itemTabPage2[index2].itemIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
          this.itemTabPage2[index2].itemIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
          this.itemTabPage2[index2].itemIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
          this.itemTabPage2[index2].itemIcon.FeedData((IComponentData) iconData);
          this.itemTabPage2[index2].itemName.text = iconData.contents[0];
          this.itemTabPage2[index2].itemNumber.text = Utils.FormatThousands(this.dicRankReward[index1].rankRewardCount.ToString());
          this.itemTabPage2[index2].itemName.transform.parent.gameObject.SetActive(true);
          if (iconData.Data == null)
            this.itemTabPage2[index2].itemIcon.LoadDefaultBackgroud();
          ++index2;
          this.Tab2AlliancePoints.text = string.Format(Utils.XLAT("event_fallen_knight_alliance_points"), (object) this.GetScoreOrRank("alliance_score"));
          this.Tab2AllianceRank.text = this.GetScoreOrRank("alliance_rank") <= 0L ? string.Format(Utils.XLAT("event_fallen_knight_alliance_points_ranking"), (object) "~") : string.Format(Utils.XLAT("event_fallen_knight_alliance_points_ranking"), (object) this.GetScoreOrRank("alliance_rank"));
        }
      }
      this.Tab2RankNumLabel.text = string.Format(Utils.XLAT("event_rank_num"), (object) 1);
    }
    else
    {
      if (type != ConfigFallenKnightRankReward.RankType.IndividualRank || this.itemTabPage3.Length < this.dicRankReward.Count)
        return;
      int index1 = 0;
      int index2 = 0;
      for (; index1 < this.dicRankReward.Count; ++index1)
      {
        if (!string.IsNullOrEmpty(this.dicRankReward[index1].rankRewardName) && this.dicRankReward[index1].rankRewardCount > 0L)
        {
          IconData iconData = this.GetIconData(this.dicRankReward[index1].rankRewardName, this.dicRankReward[index1].rankRewardCount);
          this.itemTabPage3[index2].itemIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
          this.itemTabPage3[index2].itemIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
          this.itemTabPage3[index2].itemIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
          this.itemTabPage3[index2].itemIcon.FeedData((IComponentData) iconData);
          this.itemTabPage3[index2].itemName.text = iconData.contents[0];
          this.itemTabPage3[index2].itemNumber.text = Utils.FormatThousands(this.dicRankReward[index1].rankRewardCount.ToString());
          this.itemTabPage3[index2].itemName.transform.parent.gameObject.SetActive(true);
          if (iconData.Data == null)
            this.itemTabPage3[index2].itemIcon.LoadDefaultBackgroud();
          ++index2;
          this.Tab3IndividualPoints.text = string.Format(Utils.XLAT("event_fallen_knight_my_points"), (object) this.GetScoreOrRank("user_score"));
          this.Tab3IndividualRank.text = this.GetScoreOrRank("user_rank") <= 0L ? string.Format(Utils.XLAT("event_fallen_knight_my_ranking"), (object) "~") : string.Format(Utils.XLAT("event_fallen_knight_my_ranking"), (object) this.GetScoreOrRank("user_rank"));
        }
      }
      this.Tab3RankNumLabel.text = string.Format(Utils.XLAT("event_rank_num"), (object) 1);
    }
  }

  private IconData GetIconData(string item_id, long count)
  {
    IconData iconData = new IconData();
    iconData.contents = new string[2];
    if (item_id.Contains("alliance_fund") || item_id.Contains("alliance_honor"))
    {
      iconData.image = "Texture/Alliance/" + item_id;
      iconData.contents[0] = Utils.XLAT(!item_id.Contains("alliance_fund") ? "id_honor" : "id_fund");
    }
    else
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(int.Parse(item_id));
      iconData.image = itemStaticInfo.ImagePath;
      iconData.Data = (object) itemStaticInfo.internalId;
      iconData.contents[0] = itemStaticInfo.LocName;
    }
    iconData.contents[1] = count.ToString();
    return iconData;
  }

  private long GetScoreOrRank(string id)
  {
    Hashtable scoreAndRank = ActivityManager.Intance.knightData.scoreAndRank;
    if (scoreAndRank != null && scoreAndRank.ContainsKey((object) id))
      return long.Parse(scoreAndRank[(object) id].ToString());
    return 0;
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.tabCategories.Length; ++index)
    {
      if (this.tabCategories[index].value)
        return index;
    }
    return -1;
  }

  private void ExecuteInSecond(int time = 0)
  {
    int num1 = ActivityManager.Intance.knightData.endTime - NetServerTime.inst.ServerTimestamp;
    int num2 = 43200;
    int time1 = num1 - num2;
    if (ActivityManager.Intance.knightData.isActivity || ActivityManager.Intance.knightData.isActivityAbleToStart)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("1", Utils.FormatTime(num1 < 0 ? 0 : num1, true, false, true));
      if (num1 - num2 > 0)
      {
        para.Remove("1");
        para.Add("1", Utils.FormatTime(time1, true, false, true));
        this.startedOrGoingTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", para, true);
      }
      else
        this.startedOrGoingTimeLabel.text = ScriptLocalization.GetWithPara("event_rewards_timer", para, true);
      this.startedOrGoingTimeLabel.transform.parent.gameObject.SetActive(true);
      this.unstartOrOverTimeLabel.transform.parent.gameObject.SetActive(false);
    }
    else if (ActivityManager.Intance.knightData.isActivityOver)
    {
      if (num1 <= 0)
        return;
      this.unstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara("event_finishes_num", new Dictionary<string, string>()
      {
        {
          "1",
          Utils.FormatTime(time1 < 0 ? 0 : time1, true, false, true)
        }
      }, true);
      this.startedOrGoingTimeLabel.transform.parent.gameObject.SetActive(false);
      this.unstartOrOverTimeLabel.transform.parent.gameObject.SetActive(true);
    }
    else
    {
      int time2 = ActivityManager.Intance.knightData.startTime - NetServerTime.inst.ServerTimestamp;
      if (time2 < 0)
        time2 = ActivityManager.Intance.knightData.endTime - NetServerTime.inst.ServerTimestamp;
      this.unstartOrOverTimeLabel.text = ScriptLocalization.GetWithPara(time2 <= 0 ? "event_finishes_num" : "event_starts_num", new Dictionary<string, string>()
      {
        {
          "1",
          Utils.FormatTime(time2, true, false, true)
        }
      }, true);
      this.startedOrGoingTimeLabel.transform.parent.gameObject.SetActive(false);
      this.unstartOrOverTimeLabel.transform.parent.gameObject.SetActive(true);
    }
  }

  private void OnActivityDataUpdate()
  {
    this.ShowTabPageContents(this.GetSelectedIndex());
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.ExecuteInSecond);
    ActivityManager.Intance.onActivityDataUpdate += new System.Action(this.OnActivityDataUpdate);
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.ExecuteInSecond);
    ActivityManager.Intance.onActivityDataUpdate -= new System.Action(this.OnActivityDataUpdate);
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  [Serializable]
  public class IconInfo
  {
    public Icon itemIcon;
    public UILabel itemName;
    public UILabel itemNumber;
  }

  public class RankRewardPair
  {
    public string rankRewardName;
    public long rankRewardCount;
  }
}
