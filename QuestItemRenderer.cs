﻿// Decompiled with JetBrains decompiler
// Type: QuestItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class QuestItemRenderer : MonoBehaviour
{
  private long _questId = -1;
  public UILabel questName;
  public UIProgressBar progressBar;
  public UILabel progressLabel;
  public UIButton gotoButton;
  public UIButton claimButton;
  private bool isAdd;
  public QuestIconRenderer iconRenderer;
  private bool _selected;
  public UISprite normalBg;
  public UISprite selectedBg;
  public System.Action<long> OnQuestItemClickDelegate;

  public bool Selected
  {
    get
    {
      return this._selected;
    }
    set
    {
      if (this._selected == value)
        return;
      this._selected = value;
      NGUITools.SetActive(this.normalBg.gameObject, !this._selected);
      NGUITools.SetActive(this.selectedBg.gameObject, this._selected);
      if (!this._selected || this.OnQuestItemClickDelegate == null)
        return;
      this.OnQuestItemClickDelegate(this._questId);
    }
  }

  public long QuestID
  {
    get
    {
      return this._questId;
    }
  }

  public void SetData(long questID)
  {
    this._questId = questID;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.normalBg.gameObject, !this._selected);
    NGUITools.SetActive(this.selectedBg.gameObject, this._selected);
    this.AddEventHandler();
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(this._questId);
    this.iconRenderer.SetData(this._questId);
    this.questName.text = ScriptLocalization.Get(questData2.Info.Loc_Name, true);
    if ((UnityEngine.Object) this.gotoButton != (UnityEngine.Object) null)
      this.gotoButton.gameObject.SetActive(false);
    if (questData2.Status == 1)
    {
      this.claimButton.gameObject.SetActive(true);
    }
    else
    {
      if ((UnityEngine.Object) this.gotoButton != (UnityEngine.Object) null)
        this.gotoButton.gameObject.SetActive(this.ShowGOTO());
      this.claimButton.gameObject.SetActive(false);
    }
    this.RefreshProgressBar();
  }

  public void OnClickHandler()
  {
    this.Selected = true;
  }

  private void RefreshProgressBar()
  {
    if (this._questId < 0L)
      return;
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(this._questId);
    if (questData2 == null)
      return;
    BasePresent present = questData2.Present;
    if (present == null)
      return;
    present.Refresh();
    this.progressBar.value = present.GetProgressBarValue();
    this.progressLabel.text = present.GetProgressContent();
  }

  private bool ShowGOTO()
  {
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(this._questId);
    return questData2.Info.LinkHudId > 0 || questData2.Info.Category == "goto_facebook";
  }

  public void OnGoToButtonPress()
  {
    QuestData2 questData2 = DBManager.inst.DB_Quest.Get(this._questId);
    if (questData2.Info.LinkHudId > 0)
    {
      QuestLinkHUDInfo questLinkHudInfo = ConfigManager.inst.DB_QuestLinkHud.GetQuestLinkHudInfo(questData2.Info.LinkHudId);
      if (questLinkHudInfo == null)
        return;
      LinkerHub.Instance.Distribute(questLinkHudInfo.Param);
    }
    else
    {
      if (!(questData2.Info.Category == "goto_facebook"))
        return;
      RequestManager.inst.SendRequest("Quest:jumpToFacebook", Utils.Hash((object) "quest_id", (object) this._questId), (System.Action<bool, object>) null, true);
      Application.OpenURL(NetApi.inst.FacebookURL);
    }
  }

  public void OnClaimButtonPress()
  {
    PlayerData.inst.QuestManager.ClaimQuest(this._questId);
  }

  public void DisplayDetail()
  {
    if (this._questId < 0L)
      return;
    UIManager.inst.OpenDlg("QuestEmpireDetailsDlg", (UI.Dialog.DialogParameter) new QuestEmpireDetailsDlg.Parameter()
    {
      questId = this._questId
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void Refresh(long questId)
  {
    if (questId != this._questId)
      return;
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    if (this.isAdd)
      return;
    this.isAdd = true;
    DBManager.inst.DB_Quest.onDataChanged += new System.Action<long>(this.Refresh);
    DBManager.inst.DB_Quest.onDataUpdated += new System.Action<long>(this.Refresh);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateProgressBar);
  }

  private void UpdateProgressBar(int obj)
  {
    this.RefreshProgressBar();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Quest.onDataChanged -= new System.Action<long>(this.Refresh);
    DBManager.inst.DB_Quest.onDataUpdated -= new System.Action<long>(this.Refresh);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateProgressBar);
    this.isAdd = false;
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable)
      return;
    this.RemoveEventHandler();
    this.OnQuestItemClickDelegate = (System.Action<long>) null;
  }

  public void Clear()
  {
    this.RemoveEventHandler();
  }
}
