﻿// Decompiled with JetBrains decompiler
// Type: ConfigIAP_DailyPackage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigIAP_DailyPackage
{
  private Dictionary<string, IAPDailyPackageInfo> m_DataByID;
  private Dictionary<int, IAPDailyPackageInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<IAPDailyPackageInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public IAPDailyPackageInfo Get(string id)
  {
    IAPDailyPackageInfo dailyPackageInfo;
    this.m_DataByID.TryGetValue(id, out dailyPackageInfo);
    return dailyPackageInfo;
  }

  public IAPDailyPackageInfo Get(int internalId)
  {
    IAPDailyPackageInfo dailyPackageInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out dailyPackageInfo);
    return dailyPackageInfo;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, IAPDailyPackageInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, IAPDailyPackageInfo>) null;
  }
}
