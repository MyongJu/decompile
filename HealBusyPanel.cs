﻿// Decompiled with JetBrains decompiler
// Type: HealBusyPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HealBusyPanel : MonoBehaviour
{
  public System.Action OnHealCanceled;
  public TimerHUDUIItem timerHUDUIItem;
  public UILabel goldSpeedUpCostLabel;
  private CityManager.BuildingItem BI;

  public void Init()
  {
    this.BI = CityManager.inst.GetBuildingWithActionTimer("hospital");
    this.SetWaitState();
  }

  public void Dispose()
  {
  }

  public void SetWaitState()
  {
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_HEALING_TROOPS);
    if (singleJobByClass == null)
      return;
    double secsRemaining = singleJobByClass != null ? (double) singleJobByClass.LeftTime() : 0.0;
    double totalSeconds = singleJobByClass != null ? (double) singleJobByClass.Duration() : 0.0;
    double num = 0.0;
    this.timerHUDUIItem.gameObject.SetActive(true);
    this.timerHUDUIItem.mJobID = singleJobByClass.GetJobID();
    this.timerHUDUIItem.mFreeTimeSecs = num;
    this.timerHUDUIItem.SetDetails("time_bar_healing", string.Empty, TimerType.TIMER_HEAL, secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
    this.timerHUDUIItem.gameObject.SetActive(true);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
    this.OnSecondHandler(0);
  }

  private void OnSecondHandler(int timeStamp)
  {
    if ((UnityEngine.Object) this.timerHUDUIItem != (UnityEngine.Object) null)
      this.timerHUDUIItem.OnUpdate(timeStamp);
    if (this.BI == null)
      return;
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(this.BI.mTrainingJobID);
    if (unfinishedJob == null)
      return;
    Utils.SetPriceToLabel(this.goldSpeedUpCostLabel, ItemBag.CalculateCost(new Hashtable()
    {
      {
        (object) "time",
        (object) unfinishedJob.LeftTime()
      }
    }));
  }

  public void OnCancelHealCancel()
  {
  }

  public void OnCanelBtnClick()
  {
    (UIManager.inst.OpenPopup("CancelHealConfirmPopup", (Popup.PopupParameter) null) as CancelHealConfirmPopup).Init(new System.Action(this.OnCancelHealConfirmed));
  }

  public void OnCancelHealConfirmed()
  {
    this.BI = CityManager.inst.GetBuildingWithActionTimer("hospital");
    if (this.BI == null)
      return;
    MessageHub.inst.GetPortByAction("City:cancelHealing").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) this.BI.mID.ToString(), (object) "job_id", (object) this.BI.mTrainingJobID), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (bSuccess)
        ;
    }), true);
  }

  public void OnTimerSpeedUpBtnPressed()
  {
    long mTrainingJobId = this.BI.mTrainingJobID;
    GoldConsumePopup.Parameter parameter = new GoldConsumePopup.Parameter();
    Hashtable ht = new Hashtable();
    int num1 = JobManager.Instance.GetJob(mTrainingJobId).LeftTime();
    int num2 = num1;
    ht.Add((object) "time", (object) num2);
    int cost = ItemBag.CalculateCost(ht);
    parameter.confirmButtonClickEvent = new System.Action(this.ConFirmCallBack);
    parameter.lefttime = num1;
    parameter.goldNum = cost;
    parameter.freetime = 0;
    parameter.description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        cost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) parameter);
  }

  private void ConFirmCallBack()
  {
    long mTrainingJobId = this.BI.mTrainingJobID;
    JobHandle job = JobManager.Instance.GetJob(mTrainingJobId);
    int num = job != null ? job.LeftTime() : 0;
    if (num <= 0)
      return;
    int cost = ItemBag.CalculateCost(new Hashtable()
    {
      {
        (object) "time",
        (object) num
      }
    });
    if (cost > PlayerData.inst.hostPlayer.Currency)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_insufficient_gold_tip", true), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) mTrainingJobId
        },
        {
          (object) "gold",
          (object) cost
        }
      }, (System.Action<bool, object>) null, true);
  }
}
