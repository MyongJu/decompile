﻿// Decompiled with JetBrains decompiler
// Type: MailAPI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class MailAPI
{
  public MailController parent;
  private HubPort _getListPort;
  private HubPort _deleteMail;
  private HubPort _sendMail;
  private HubPort _sendAllianceMail;
  private HubPort _getUserInfo;
  private HubPort _getContacts;
  private HubPort _validateRecipient;
  private HubPort _gainAttachment;
  private HubPort _tagRead;
  private HubPort _tagFavorite;
  private HubPort _batchUnTagFavorite;
  private HubPort _getUnReadCount;
  private HubPort _sendModeMail;
  private HubPort _getSharedMail;
  private HubPort _batchGainAttachment;

  public void Initialize()
  {
    this._getListPort = new HubPort("Mail:getList");
    this._deleteMail = new HubPort("Mail:deleteMail");
    this._sendMail = new HubPort("Mail:sendMail");
    this._getUserInfo = new HubPort("Player:getUserInfoByUsername");
    this._getContacts = new HubPort("Player:fetchContacts");
    this._validateRecipient = new HubPort("Mail:validateRecipient");
    this._gainAttachment = new HubPort("Mail:gainAttachment");
    this._tagRead = new HubPort("Mail:tagRead");
    this._tagFavorite = new HubPort("Mail:tagFavorite");
    this._getUnReadCount = new HubPort("Mail:getUnReadCount");
    this._getSharedMail = new HubPort("Mail:getSharedMail");
    this._sendAllianceMail = new HubPort("Mail:sendAllianceMail");
    this._sendModeMail = new HubPort("Mail:sendModMail");
    this._batchGainAttachment = new HubPort("Mail:batchGainAttachment");
    this._batchUnTagFavorite = new HubPort("Mail:batchUnTagFavorite");
    MessageHub.inst.GetPortByAction("mail").AddEvent(new System.Action<object>(this.OnMailReceived));
  }

  public void SendAllianceMail(long alliance_id, string content, System.Action callBack)
  {
    this._sendAllianceMail.SendRequest(new Hashtable()
    {
      {
        (object) nameof (alliance_id),
        (object) alliance_id
      },
      {
        (object) "text",
        (object) content
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack();
    }), true);
  }

  public void SendModMail(long toUid, string content, System.Action callBack)
  {
    this.SendModMails(new ArrayList((ICollection) new List<long>()
    {
      toUid
    }), content, callBack);
  }

  public void SendModMails(ArrayList toUids, string content, System.Action callback)
  {
    this._sendModeMail.SendRequest(new Hashtable()
    {
      {
        (object) "to_uid",
        (object) toUids
      },
      {
        (object) "text",
        (object) content
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success)
        return;
      ArrayList arrayList = obj as ArrayList;
      if (arrayList != null)
      {
        for (int index = 0; index < arrayList.Count; ++index)
          GameEngine.Instance.events.Publish_OnUpdateMail(arrayList[index] as Hashtable);
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  public void GetUnreadCount(System.Action<Hashtable> calllBack)
  {
    Hashtable hashtable = new Hashtable();
    RequestManager.inst.SendLoader("Mail:getUnReadCount", (Hashtable) null, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || calllBack == null)
        return;
      calllBack(obj as Hashtable);
    }), true, false);
  }

  public void TagFavorite(Dictionary<long, bool> mails, int category, System.Action callBack = null)
  {
    Hashtable hashtable = new Hashtable();
    bool isFav = false;
    using (Dictionary<long, bool>.Enumerator enumerator = mails.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, bool> current = enumerator.Current;
        isFav = current.Value;
        hashtable.Add((object) current.Key, (object) (!current.Value ? 0 : 1));
      }
    }
    this._tagFavorite.SendLoader(new Hashtable()
    {
      {
        (object) nameof (mails),
        (object) hashtable
      },
      {
        (object) nameof (category),
        (object) category
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success)
        return;
      if (isFav)
        UIManager.inst.ShowBasicToast("toast_favorite_add");
      else
        UIManager.inst.ShowBasicToast("toast_favorite_delete");
      if (callBack == null)
        return;
      callBack();
    }), true, false);
  }

  public void BatchUnTagFavorite(List<AbstractMailEntry> mails, System.Action callBack = null)
  {
    Hashtable hashtable = new Hashtable();
    bool isFav = false;
    using (List<AbstractMailEntry>.Enumerator enumerator = mails.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AbstractMailEntry current = enumerator.Current;
        hashtable.Add((object) current.mailID, (object) current.category);
      }
    }
    this._batchUnTagFavorite.SendLoader(new Hashtable()
    {
      {
        (object) nameof (mails),
        (object) hashtable
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success)
        return;
      if (isFav)
        UIManager.inst.ShowBasicToast("toast_favorite_add");
      else
        UIManager.inst.ShowBasicToast("toast_favorite_delete");
      if (callBack == null)
        return;
      callBack();
    }), true, false);
  }

  public void GetList(long lastMailID, int category, System.Action initCallBack, bool blockSceen = true)
  {
    this._getListPort.SendRequest(new Hashtable()
    {
      {
        (object) "last_mail_id",
        (object) lastMailID
      },
      {
        (object) nameof (category),
        (object) category
      }
    }, (System.Action<bool, object>) ((success, result) =>
    {
      if (!success)
        return;
      Hashtable hashtable = result as Hashtable;
      if (hashtable != null)
      {
        ArrayList arrayList = hashtable[(object) "list"] as ArrayList;
        if (arrayList == null)
          return;
        for (int index = 0; index < arrayList.Count; ++index)
          GameEngine.Instance.events.Publish_OnReceiveMail(arrayList[index] as Hashtable, true);
        PlayerData.inst.mail.GetMailListOfType((MailCategory) category).LowerMailId = long.Parse(hashtable[(object) "last_mail_id"].ToString());
      }
      if (initCallBack == null)
        return;
      initCallBack();
    }), blockSceen);
  }

  public void GetFavList(long lastMailID, System.Action initCallBack, bool blockScene = true)
  {
    this._getListPort.SendRequest(new Hashtable()
    {
      {
        (object) "last_mail_id",
        (object) lastMailID
      },
      {
        (object) "category",
        (object) 5
      }
    }, (System.Action<bool, object>) ((success, result) =>
    {
      if (!success)
        return;
      Hashtable hashtable = result as Hashtable;
      if (hashtable != null)
      {
        ArrayList arrayList = hashtable[(object) "list"] as ArrayList;
        if (arrayList == null)
          return;
        for (int index = 0; index < arrayList.Count; ++index)
          GameEngine.Instance.events.Publish_OnReceiveFavMail(arrayList[index] as Hashtable, true);
        PlayerData.inst.mail.GetMailListOfType(MailCategory.Favorite).LowerMailId = long.Parse(hashtable[(object) "last_mail_id"].ToString());
      }
      if (initCallBack == null)
        return;
      initCallBack();
    }), blockScene);
  }

  public void DeleteMail(int category, List<long> mailIds, System.Action callBack = null)
  {
    if (mailIds.Count == 0)
    {
      callBack();
    }
    else
    {
      ArrayList arrayList = new ArrayList(mailIds.Count);
      for (int index = 0; index < mailIds.Count; ++index)
        arrayList.Add((object) mailIds[index]);
      this._deleteMail.SendRequest(new Hashtable()
      {
        {
          (object) nameof (category),
          (object) category
        },
        {
          (object) "mail_ids",
          (object) arrayList
        }
      }, (System.Action<bool, object>) ((success, obj) =>
      {
        if (!success || callBack == null)
          return;
        callBack();
      }), true);
    }
  }

  public void TagRead(int category, List<long> mailIds, System.Action callBack = null)
  {
    if (mailIds.Count == 0)
      return;
    ArrayList arrayList = new ArrayList(mailIds.Count);
    for (int index = 0; index < mailIds.Count; ++index)
      arrayList.Add((object) mailIds[index]);
    this._tagRead.SendRequest(new Hashtable()
    {
      {
        (object) nameof (category),
        (object) category
      },
      {
        (object) "mail_ids",
        (object) arrayList
      }
    }, (System.Action<bool, object>) ((success, obj) =>
    {
      if (!success || callBack == null)
        return;
      callBack();
    }), true);
  }

  public void SendMail(ArrayList recipents, string content, System.Action callBack)
  {
    this._sendMail.SendRequest(new Hashtable()
    {
      {
        (object) "text",
        (object) content
      },
      {
        (object) "to_uids",
        (object) recipents
      }
    }, (System.Action<bool, object>) ((success, response) =>
    {
      ArrayList arrayList = response as ArrayList;
      if (arrayList != null)
      {
        for (int index = 0; index < arrayList.Count; ++index)
          GameEngine.Instance.events.Publish_OnUpdateMail(arrayList[index] as Hashtable);
      }
      if (callBack == null)
        return;
      callBack();
    }), true);
  }

  public void SendMail(ArrayList recipents, string content, int itemId, int itemCount, System.Action callBack)
  {
    this._sendMail.SendRequest(new Hashtable()
    {
      {
        (object) "text",
        (object) content
      },
      {
        (object) "to_uids",
        (object) recipents
      },
      {
        (object) "goods_id",
        (object) itemId
      },
      {
        (object) "amount",
        (object) itemCount
      }
    }, (System.Action<bool, object>) ((success, response) =>
    {
      ArrayList arrayList = response as ArrayList;
      if (arrayList != null)
      {
        for (int index = 0; index < arrayList.Count; ++index)
          GameEngine.Instance.events.Publish_OnUpdateMail(arrayList[index] as Hashtable);
      }
      if (callBack == null)
        return;
      callBack();
    }), true);
  }

  public void GetUserInfoByUsername(string username, System.Action<long> success, System.Action fail)
  {
    this._getUserInfo.SendRequest(new Hashtable()
    {
      {
        (object) nameof (username),
        (object) username
      }
    }, (System.Action<bool, object>) ((succeeded, response) =>
    {
      Hashtable hashtable = response as Hashtable;
      if (succeeded && success != null && (hashtable != null && hashtable.ContainsKey((object) "uid")))
      {
        success(long.Parse(hashtable[(object) "uid"].ToString()));
      }
      else
      {
        if (fail == null)
          return;
        fail();
      }
    }), true);
  }

  public void ValidateRecipient(ArrayList recipients, System.Action<bool, object> callBack)
  {
    this._validateRecipient.SendRequest(new Hashtable()
    {
      {
        (object) "user_names",
        (object) recipients
      }
    }, callBack, true);
  }

  public void GainAttachment(int category, long mailID, System.Action<bool, object> callBack)
  {
    this._gainAttachment.SendRequest(new Hashtable()
    {
      {
        (object) nameof (category),
        (object) category
      },
      {
        (object) "mail_id",
        (object) mailID
      }
    }, callBack, true);
  }

  public void BatchGainAttachments(int category, ArrayList mailIDs, System.Action<bool, object> callBack = null)
  {
    this._batchGainAttachment.SendRequest(new Hashtable()
    {
      {
        (object) nameof (category),
        (object) category
      },
      {
        (object) "mail_ids",
        (object) mailIDs
      }
    }, callBack, true);
  }

  public void GetContacts(string uid, System.Action<Hashtable> success)
  {
    this._getContacts.SendRequest(new Hashtable()
    {
      {
        (object) nameof (uid),
        (object) int.Parse(uid)
      }
    }, (System.Action<bool, object>) ((succeeded, response) =>
    {
      Hashtable hashtable = response as Hashtable;
      if (!succeeded || hashtable == null || success == null)
        return;
      success(hashtable);
    }), true);
  }

  public void GetSharedMail(long user, long mailid, System.Action<Hashtable> success)
  {
    this._getSharedMail.SendRequest(new Hashtable()
    {
      {
        (object) "mail_uid",
        (object) user
      },
      {
        (object) "mail_id",
        (object) mailid
      }
    }, (System.Action<bool, object>) ((succeeded, response) =>
    {
      Hashtable hashtable = response as Hashtable;
      if (!succeeded || hashtable == null || success == null)
        return;
      success(hashtable);
    }), true);
  }

  private void OnMailReceived(object obj)
  {
    Hashtable hashtable = obj as Hashtable;
    if (hashtable == null)
      return;
    GameEngine.Instance.events.Publish_OnUpdateMail(hashtable[(object) "mail"] as Hashtable);
  }
}
