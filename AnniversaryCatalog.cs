﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryCatalog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnniversaryCatalog : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelSelected;
  [SerializeField]
  private UILabel _labelUnselected;
  [SerializeField]
  private GameObject _objSelected;
  [SerializeField]
  private GameObject _objUnselected;
  private string _catalogName;
  private AnniversaryCatalog.CatalogType _catalogType;
  private int _catalogIndex;
  private bool _defaultSelected;
  private bool _showHot;
  public System.Action<AnniversaryCatalog.CatalogType, int> OnCurrentCatalogClicked;

  public AnniversaryCatalog.CatalogType CurrentCatalogType
  {
    get
    {
      return this._catalogType;
    }
  }

  public void SetData(string name, AnniversaryCatalog.CatalogType type, int catalogIndex = -1, bool selected = false, bool showHot = false)
  {
    this._catalogName = name;
    this._catalogType = type;
    this._catalogIndex = catalogIndex;
    this._defaultSelected = selected;
    this._showHot = showHot;
    if (type == AnniversaryCatalog.CatalogType.TOURNAMENT)
    {
      AnniversaryManager.Instance.OnPointChange -= new System.Action<int>(this.OnTournamentPointChanged);
      AnniversaryManager.Instance.OnPointChange += new System.Action<int>(this.OnTournamentPointChanged);
    }
    this.UpdateUI();
  }

  public void ClearData()
  {
    AnniversaryManager.Instance.OnPointChange -= new System.Action<int>(this.OnTournamentPointChanged);
  }

  public void OnCatalogClicked()
  {
    AnniversaryCatalog[] componentsInChildren = this.transform.parent.GetComponentsInChildren<AnniversaryCatalog>();
    if (componentsInChildren != null)
    {
      for (int index = 0; index < componentsInChildren.Length; ++index)
        componentsInChildren[index].ResetCatalog();
    }
    this.SetCatalog();
  }

  public void ResetCatalog()
  {
    NGUITools.SetActive(this._objSelected, false);
    NGUITools.SetActive(this._objUnselected, true);
  }

  private void SetCatalog()
  {
    NGUITools.SetActive(this._objSelected, true);
    NGUITools.SetActive(this._objUnselected, false);
    if (this.OnCurrentCatalogClicked == null)
      return;
    this.OnCurrentCatalogClicked(this._catalogType, this._catalogIndex);
  }

  private void UpdateUI()
  {
    UILabel labelSelected = this._labelSelected;
    string catalogName = this._catalogName;
    this._labelUnselected.text = catalogName;
    string str = catalogName;
    labelSelected.text = str;
    if (this._defaultSelected)
      this.SetCatalog();
    else
      this.ResetCatalog();
    Transform child = this.transform.FindChild("Hot");
    if (!(bool) ((UnityEngine.Object) child))
      return;
    NGUITools.SetActive(child.gameObject, this._showHot);
  }

  private void OnTournamentPointChanged(int count)
  {
    if (!(bool) ((UnityEngine.Object) this.gameObject))
      return;
    Transform child = this.transform.FindChild("Badge");
    if (!(bool) ((UnityEngine.Object) child))
      return;
    UILabel component = child.GetComponent<UILabel>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    component.text = count.ToString();
    NGUITools.SetActive(component.gameObject, count > 0);
  }

  public enum CatalogType
  {
    IAP_PACKAGE,
    TOURNAMENT,
    NEW_SKIN,
    COMMUNITY_EVENT,
    OTHER,
  }
}
