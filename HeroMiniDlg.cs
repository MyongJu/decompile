﻿// Decompiled with JetBrains decompiler
// Type: HeroMiniDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class HeroMiniDlg : MonoBehaviour
{
  private int m_LastPortrait = -1;
  private string m_LastIcon = string.Empty;
  public UITexture heroPortrait;
  public UILabel level;
  public UISlider expProgress;
  public UISlider vitProgress;
  public UISpriteAnimation animation;
  public Transform effectroot;
  public SpiritDecreaseController spiritController;
  private bool m_Play;
  private bool m_Refresh;

  private void Start()
  {
    this.m_Refresh = true;
    NGUITools.SetActive(this.animation.gameObject, false);
  }

  public void ForceRefresh()
  {
    this.m_Refresh = true;
  }

  private void OnEnable()
  {
    this.m_Refresh = true;
    DBManager.inst.DB_hero.onDataChanged += new System.Action<long>(this.OnHeroInfoChanged);
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUserDataChanged);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown || !GameEngine.IsReady())
      return;
    DBManager.inst.DB_hero.onDataChanged -= new System.Action<long>(this.OnHeroInfoChanged);
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUserDataChanged);
    NGUITools.SetActive(this.animation.gameObject, false);
  }

  public void PlayAnimation()
  {
    NGUITools.SetActive(this.animation.gameObject, true);
    this.animation.ResetToBeginning();
    this.animation.Play();
    this.m_Play = true;
  }

  private void Refresh()
  {
    try
    {
      DB.HeroData heroData = PlayerData.inst.heroData;
      if (heroData != null)
      {
        this.level.text = heroData.level.ToString();
        if (heroData.level == 0 || ConfigManager.inst.DB_Hero_Point.MaxLevel == 0 || heroData.level == ConfigManager.inst.DB_Hero_Point.MaxLevel)
        {
          this.expProgress.value = 0.0f;
        }
        else
        {
          int experienceRequired1 = ConfigManager.inst.DB_Hero_Point[heroData.level].ExperienceRequired;
          int experienceRequired2 = ConfigManager.inst.DB_Hero_Point[heroData.level + 1].ExperienceRequired;
          this.expProgress.value = ((float) heroData.xp - (float) experienceRequired1) / (float) (experienceRequired2 - experienceRequired1);
        }
      }
      if (this.m_LastPortrait != PlayerData.inst.userData.portrait || this.m_LastIcon != PlayerData.inst.userData.Icon)
      {
        CustomIconLoader.Instance.requestCustomIcon(this.heroPortrait, PlayerData.inst.userData.PortraitIconPath, PlayerData.inst.userData.Icon, false);
        this.m_LastPortrait = PlayerData.inst.userData.portrait;
        this.m_LastIcon = PlayerData.inst.userData.Icon;
      }
      this.UpdateSpiritSlider();
    }
    catch
    {
    }
  }

  private void Update()
  {
    if (this.m_Play && !this.animation.isPlaying)
    {
      this.m_Play = false;
      NGUITools.SetActive(this.animation.gameObject, false);
    }
    this.UpdateSpiritSlider();
    if (!this.m_Refresh)
      return;
    this.m_Refresh = false;
    this.Refresh();
  }

  private void OnHeroInfoChanged(long id)
  {
    if (PlayerData.inst.uid != id)
      return;
    this.m_Refresh = true;
  }

  private void OnUserDataChanged(long id)
  {
    if (PlayerData.inst.uid != id)
      return;
    this.m_Refresh = true;
  }

  private void UpdateSpiritSlider()
  {
    try
    {
      DB.HeroData heroData = PlayerData.inst.heroData;
      int num = 100;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("spirit_max");
      if (data != null)
        num = data.ValueInt;
      this.vitProgress.value = (float) heroData.spirit / (float) num;
    }
    catch
    {
    }
  }
}
