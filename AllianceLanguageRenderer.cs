﻿// Decompiled with JetBrains decompiler
// Type: AllianceLanguageRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceLanguageRenderer : MonoBehaviour
{
  public UILabel m_Text;
  public string m_Locale;

  public void Set(string locale)
  {
    this.m_Locale = locale;
    this.m_Text.text = Language.Instance.GetLanguageName(this.m_Locale);
  }
}
