﻿// Decompiled with JetBrains decompiler
// Type: ConfigSimulatorForbidden
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigSimulatorForbidden
{
  private Dictionary<string, SimulatorForbiddenInfo> _stringIdData;
  private Dictionary<int, SimulatorForbiddenInfo> _intIdData;

  public void BuildDB(object res)
  {
    ConfigParse configParse = new ConfigParse();
    Hashtable sources = res as Hashtable;
    if (sources == null)
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    else
      configParse.Parse<SimulatorForbiddenInfo, string>(sources, "ID", out this._stringIdData, out this._intIdData);
  }

  public SimulatorForbiddenInfo Get(string key)
  {
    SimulatorForbiddenInfo simulatorForbiddenInfo = (SimulatorForbiddenInfo) null;
    if (this._stringIdData != null)
      this._stringIdData.TryGetValue(key, out simulatorForbiddenInfo);
    return simulatorForbiddenInfo;
  }

  public void Clear()
  {
    if (this._stringIdData != null)
      this._stringIdData.Clear();
    if (this._intIdData == null)
      return;
    this._intIdData.Clear();
  }
}
