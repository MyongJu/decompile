﻿// Decompiled with JetBrains decompiler
// Type: DungeonStorePurchasePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UI;
using UnityEngine;

public class DungeonStorePurchasePopup : Popup
{
  private bool _autoFix = true;
  private ProgressBarTweenProxy _proxy = new ProgressBarTweenProxy();
  private int _proxyMinSize = 1;
  [SerializeField]
  private UILabel _labelTitle;
  [SerializeField]
  private UILabel _labelMaxBuyCount;
  [SerializeField]
  private UILabel _labelItemDescription;
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UITexture _currencyIcon;
  [SerializeField]
  private UILabel _currencyCount;
  [SerializeField]
  private UIButton _buttonExchange;
  [SerializeField]
  private UIProgressBar _progressBar;
  [SerializeField]
  private UILabel _labelInputCount;
  [SerializeField]
  private UIInput _inputExchangeCount;
  private ShopCommonMain _shopInfo;
  private int _exchangeCount;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DungeonStorePurchasePopup.Parameter parameter = orgParam as DungeonStorePurchasePopup.Parameter;
    if (parameter != null)
      this._shopInfo = parameter.shopInfo;
    if (this._shopInfo != null && (double) this._shopInfo.req_value > 0.0)
    {
      this._proxy.MaxSize = (int) (this.CurrencyCount / (long) this._shopInfo.req_value) - 1;
      int shopItemLeftCount = ShopCommonPayload.Instance.GetShopItemLeftCount(this._shopInfo.internalId);
      if (shopItemLeftCount >= 0)
        this._proxy.MaxSize = Mathf.Min(this._proxy.MaxSize, shopItemLeftCount - 1);
      if (this._proxy.MaxSize < 0)
      {
        this._proxy.MinSize = this._proxy.MaxSize;
        this._progressBar.enabled = false;
        UILabel currencyCount = this._currencyCount;
        string str1 = "0";
        this._labelInputCount.text = str1;
        string str2 = str1;
        currencyCount.text = str2;
      }
      this._labelMaxBuyCount.text = string.Format("/{0}", (object) (this._proxy.MaxSize + 1).ToString());
      this._buttonExchange.isEnabled = shopItemLeftCount > 0 || shopItemLeftCount == -1;
    }
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  private long CurrencyCount
  {
    get
    {
      return (long) ItemBag.Instance.GetItemCount(this._shopInfo.req_item_id);
    }
  }

  public void OnSliderValueChanged()
  {
    this._exchangeCount = Mathf.RoundToInt((float) this._proxy.MaxSize * this._progressBar.value) + this._proxyMinSize;
    this._exchangeCount = Mathf.Clamp(this._exchangeCount, 1, this._proxy.MaxSize + 1);
    this._labelInputCount.text = this._exchangeCount.ToString();
    this._currencyCount.text = (this._exchangeCount * this._shopInfo.req_value).ToString();
  }

  public void OnSliderSubtractBtnDown()
  {
    this._autoFix = false;
    this._proxy.Slider = this._progressBar;
    this._proxy.Play(-1, -1f);
  }

  public void OnSliderSubtractBtnUp()
  {
    this._autoFix = true;
    this._proxy.Stop();
  }

  public void OnSliderPlusBtnDown()
  {
    this._autoFix = false;
    this._proxy.Slider = this._progressBar;
    this._proxy.Play(1, -1f);
  }

  public void OnSliderPlusBtnUp()
  {
    this._autoFix = true;
    this._proxy.Stop();
  }

  public void OnInputChange()
  {
    if (this._shopInfo == null)
      return;
    int num = 0;
    try
    {
      num = Convert.ToInt32(this._inputExchangeCount.value);
    }
    catch (FormatException ex)
    {
      num = 0;
    }
    catch (OverflowException ex)
    {
      num = int.MaxValue;
    }
    finally
    {
      this._exchangeCount = Mathf.Clamp(num, 1, this._proxy.MaxSize + 1);
      if (this._autoFix)
        this._progressBar.value = (float) (this._exchangeCount - 1) / (float) (this._proxy.MaxSize + 1);
      this._inputExchangeCount.value = this._exchangeCount.ToString();
      this._labelInputCount.text = this._exchangeCount.ToString();
      this._currencyCount.text = (this._exchangeCount * this._shopInfo.req_value).ToString();
    }
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnExchangePressed()
  {
    if (this._shopInfo == null)
      return;
    this._buttonExchange.isEnabled = false;
    Hashtable postData = new Hashtable();
    postData[(object) "shop_id"] = (object) this._shopInfo.internalId;
    postData[(object) "amount"] = (object) this._exchangeCount;
    MessageHub.inst.GetPortByAction("Item:buyCommonShopGoods").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        ShopCommonPayload.Instance.DecodeShopItemStatus(data as Hashtable);
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._shopInfo.item_id);
        ItemRewardInfo.Data data1 = new ItemRewardInfo.Data();
        data1.icon = itemStaticInfo.ImagePath;
        data1.count = (float) this._exchangeCount;
        RewardsCollectionAnimator.Instance.Clear();
        RewardsCollectionAnimator.Instance.items.Add(data1);
        RewardsCollectionAnimator.Instance.CollectItems(true);
      }
      this.OnClosePressed();
    }), true);
  }

  private void UpdateUI()
  {
    if (this._shopInfo == null)
      return;
    this._itemIcon.SetData(this._shopInfo.item_id, string.Empty, true);
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._shopInfo.item_id);
    if (itemStaticInfo != null)
    {
      this._labelTitle.text = itemStaticInfo.LocName;
      this._labelItemDescription.text = itemStaticInfo.LocDescription;
    }
    BuilderFactory.Instance.HandyBuild((UIWidget) this._currencyIcon, ConfigManager.inst.DB_Items.GetItem(this._shopInfo.req_item_id).ImagePath, (System.Action<bool>) null, true, true, string.Empty);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ShopCommonMain shopInfo;
  }
}
