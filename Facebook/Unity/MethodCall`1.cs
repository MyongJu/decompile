﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.MethodCall`1
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal abstract class MethodCall<T> where T : IResult
  {
    public MethodCall(FacebookBase facebookImpl, string methodName)
    {
      this.Parameters = new MethodArguments();
      this.FacebookImpl = facebookImpl;
      this.MethodName = methodName;
    }

    public string MethodName { get; private set; }

    public FacebookDelegate<T> Callback { protected get; set; }

    protected FacebookBase FacebookImpl { get; set; }

    protected MethodArguments Parameters { get; set; }

    public abstract void Call(MethodArguments args = null);
  }
}
