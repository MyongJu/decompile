﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.IAppLinkResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Facebook.Unity
{
  public interface IAppLinkResult : IResult
  {
    string Url { get; }

    string TargetUrl { get; }

    string Ref { get; }

    IDictionary<string, object> Extras { get; }
  }
}
