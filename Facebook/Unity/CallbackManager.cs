﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.CallbackManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace Facebook.Unity
{
  internal class CallbackManager
  {
    private IDictionary<string, object> facebookDelegates = (IDictionary<string, object>) new Dictionary<string, object>();
    private int nextAsyncId;

    public string AddFacebookDelegate<T>(FacebookDelegate<T> callback) where T : IResult
    {
      if (callback == null)
        return (string) null;
      ++this.nextAsyncId;
      this.facebookDelegates.Add(this.nextAsyncId.ToString(), (object) callback);
      return this.nextAsyncId.ToString();
    }

    public void OnFacebookResponse(IInternalResult result)
    {
      object callback;
      if (result == null || result.CallbackId == null || !this.facebookDelegates.TryGetValue(result.CallbackId, out callback))
        return;
      CallbackManager.CallCallback(callback, (IResult) result);
      this.facebookDelegates.Remove(result.CallbackId);
    }

    private static void CallCallback(object callback, IResult result)
    {
      if (callback != null && result != null && (!CallbackManager.TryCallCallback<IAppRequestResult>(callback, result) && !CallbackManager.TryCallCallback<IShareResult>(callback, result)) && (!CallbackManager.TryCallCallback<IGroupCreateResult>(callback, result) && !CallbackManager.TryCallCallback<IGroupJoinResult>(callback, result) && (!CallbackManager.TryCallCallback<IPayResult>(callback, result) && !CallbackManager.TryCallCallback<IAppInviteResult>(callback, result))) && (!CallbackManager.TryCallCallback<IAppLinkResult>(callback, result) && !CallbackManager.TryCallCallback<ILoginResult>(callback, result) && !CallbackManager.TryCallCallback<IAccessTokenRefreshResult>(callback, result)))
        throw new NotSupportedException("Unexpected result type: " + callback.GetType().FullName);
    }

    private static bool TryCallCallback<T>(object callback, IResult result) where T : IResult
    {
      FacebookDelegate<T> facebookDelegate = callback as FacebookDelegate<T>;
      if (facebookDelegate == null)
        return false;
      facebookDelegate((T) result);
      return true;
    }
  }
}
