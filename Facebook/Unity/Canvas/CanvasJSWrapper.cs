﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Canvas.CanvasJSWrapper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Facebook.Unity.Canvas
{
  internal class CanvasJSWrapper : ICanvasJSWrapper
  {
    private const string JSSDKBindingFileName = "JSSDKBindings";

    public string IntegrationMethodJs
    {
      get
      {
        TextAsset textAsset = Resources.Load("JSSDKBindings") as TextAsset;
        if ((bool) ((Object) textAsset))
          return textAsset.text;
        return (string) null;
      }
    }

    public string GetSDKVersion()
    {
      return "v2.5";
    }

    public void ExternalCall(string functionName, params object[] args)
    {
      Application.ExternalCall(functionName, args);
    }

    public void ExternalEval(string script)
    {
      Application.ExternalEval(script);
    }

    public void DisableFullScreen()
    {
      if (!Screen.fullScreen)
        return;
      Screen.fullScreen = false;
    }
  }
}
