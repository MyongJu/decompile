﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity.Canvas
{
  internal interface ICanvasFacebookCallbackHandler : IFacebookCallbackHandler
  {
    void OnPayComplete(string message);

    void OnFacebookAuthResponseChange(string message);

    void OnUrlResponse(string message);

    void OnHideUnity(bool hide);
  }
}
