﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Canvas.CanvasFacebookGameObject
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Facebook.Unity.Canvas
{
  internal class CanvasFacebookGameObject : FacebookGameObject, ICanvasFacebookCallbackHandler, IFacebookCallbackHandler
  {
    protected ICanvasFacebookImplementation CanvasFacebookImpl
    {
      get
      {
        return (ICanvasFacebookImplementation) this.Facebook;
      }
    }

    public void OnPayComplete(string result)
    {
      this.CanvasFacebookImpl.OnPayComplete(result);
    }

    public void OnFacebookAuthResponseChange(string message)
    {
      this.CanvasFacebookImpl.OnFacebookAuthResponseChange(message);
    }

    public void OnUrlResponse(string message)
    {
      this.CanvasFacebookImpl.OnUrlResponse(message);
    }

    public void OnHideUnity(bool hide)
    {
      this.CanvasFacebookImpl.OnHideUnity(hide);
    }

    protected override void OnAwake()
    {
      GameObject gameObject = new GameObject("FacebookJsBridge");
      gameObject.AddComponent<JsBridge>();
      gameObject.transform.parent = this.gameObject.transform;
    }
  }
}
