﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Canvas.CanvasFacebook
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Facebook.Unity.Canvas
{
  internal sealed class CanvasFacebook : FacebookBase, ICanvasFacebook, ICanvasFacebookCallbackHandler, ICanvasFacebookImplementation, IFacebook, IFacebookCallbackHandler
  {
    internal const string MethodAppRequests = "apprequests";
    internal const string MethodFeed = "feed";
    internal const string MethodPay = "pay";
    internal const string MethodGameGroupCreate = "game_group_create";
    internal const string MethodGameGroupJoin = "game_group_join";
    internal const string CancelledResponse = "{\"cancelled\":true}";
    internal const string FacebookConnectURL = "https://connect.facebook.net";
    private const string AuthResponseKey = "authResponse";
    private const string ResponseKey = "response";
    private string appId;
    private string appLinkUrl;
    private ICanvasJSWrapper canvasJSWrapper;

    public CanvasFacebook()
      : this((ICanvasJSWrapper) new CanvasJSWrapper(), new CallbackManager())
    {
    }

    public CanvasFacebook(ICanvasJSWrapper canvasJSWrapper, CallbackManager callbackManager)
      : base(callbackManager)
    {
      this.canvasJSWrapper = canvasJSWrapper;
    }

    public override bool LimitEventUsage { get; set; }

    public override string SDKName
    {
      get
      {
        return "FBJSSDK";
      }
    }

    public override string SDKVersion
    {
      get
      {
        return this.canvasJSWrapper.GetSDKVersion();
      }
    }

    public override string SDKUserAgent
    {
      get
      {
        string productName;
        switch (Constants.CurrentPlatform)
        {
          case FacebookUnityPlatform.WebGL:
          case FacebookUnityPlatform.WebPlayer:
            productName = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "FBUnity{0}", new object[1]
            {
              (object) Constants.CurrentPlatform.ToString()
            });
            break;
          default:
            FacebookLogger.Warn("Currently running on uknown web platform");
            productName = "FBUnityWebUnknown";
            break;
        }
        return string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0} {1}", new object[2]
        {
          (object) base.SDKUserAgent,
          (object) Utilities.GetUserAgent(productName, FacebookSdkVersion.Build)
        });
      }
    }

    public void Init(string appId, bool cookie, bool logging, bool status, bool xfbml, string channelUrl, string authResponse, bool frictionlessRequests, string jsSDKLocale, HideUnityDelegate hideUnityDelegate, InitDelegate onInitComplete)
    {
      if (this.canvasJSWrapper.IntegrationMethodJs == null)
        throw new Exception("Cannot initialize facebook javascript");
      this.Init(hideUnityDelegate, onInitComplete);
      this.canvasJSWrapper.ExternalEval(this.canvasJSWrapper.IntegrationMethodJs);
      this.appId = appId;
      bool flag = true;
      MethodArguments methodArguments = new MethodArguments();
      methodArguments.AddString(nameof (appId), appId);
      methodArguments.AddPrimative<bool>(nameof (cookie), cookie);
      methodArguments.AddPrimative<bool>(nameof (logging), logging);
      methodArguments.AddPrimative<bool>(nameof (status), status);
      methodArguments.AddPrimative<bool>(nameof (xfbml), xfbml);
      methodArguments.AddString(nameof (channelUrl), channelUrl);
      methodArguments.AddString(nameof (authResponse), authResponse);
      methodArguments.AddPrimative<bool>(nameof (frictionlessRequests), frictionlessRequests);
      methodArguments.AddString("version", FB.GraphApiVersion);
      this.canvasJSWrapper.ExternalCall("FBUnity.init", (object) (!flag ? 0 : 1), (object) "https://connect.facebook.net", (object) jsSDKLocale, (object) (!Constants.DebugMode ? 0 : 1), (object) methodArguments.ToJsonString(), (object) (!status ? 0 : 1));
    }

    public override void LogInWithPublishPermissions(IEnumerable<string> permissions, FacebookDelegate<ILoginResult> callback)
    {
      this.canvasJSWrapper.DisableFullScreen();
      this.canvasJSWrapper.ExternalCall("FBUnity.login", (object) permissions, (object) this.CallbackManager.AddFacebookDelegate<ILoginResult>(callback));
    }

    public override void LogInWithReadPermissions(IEnumerable<string> permissions, FacebookDelegate<ILoginResult> callback)
    {
      this.canvasJSWrapper.DisableFullScreen();
      this.canvasJSWrapper.ExternalCall("FBUnity.login", (object) permissions, (object) this.CallbackManager.AddFacebookDelegate<ILoginResult>(callback));
    }

    public override void LogOut()
    {
      base.LogOut();
      this.canvasJSWrapper.ExternalCall("FBUnity.logout");
    }

    public override void AppRequest(string message, OGActionType? actionType, string objectId, IEnumerable<string> to, IEnumerable<object> filters, IEnumerable<string> excludeIds, int? maxRecipients, string data, string title, FacebookDelegate<IAppRequestResult> callback)
    {
      this.ValidateAppRequestArgs(message, actionType, objectId, to, filters, excludeIds, maxRecipients, data, title, callback);
      MethodArguments args = new MethodArguments();
      args.AddString(nameof (message), message);
      args.AddCommaSeparatedList(nameof (to), to);
      args.AddString("action_type", !actionType.HasValue ? (string) null : actionType.ToString());
      args.AddString("object_id", objectId);
      args.AddList<object>(nameof (filters), filters);
      args.AddList<string>("exclude_ids", excludeIds);
      args.AddNullablePrimitive<int>("max_recipients", maxRecipients);
      args.AddString(nameof (data), data);
      args.AddString(nameof (title), title);
      CanvasFacebook.CanvasUIMethodCall<IAppRequestResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IAppRequestResult>(this, "apprequests", "OnAppRequestsComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public override void ActivateApp(string appId)
    {
      this.canvasJSWrapper.ExternalCall("FBUnity.activateApp");
    }

    public override void ShareLink(Uri contentURL, string contentTitle, string contentDescription, Uri photoURL, FacebookDelegate<IShareResult> callback)
    {
      MethodArguments args = new MethodArguments();
      args.AddUri("link", contentURL);
      args.AddString("name", contentTitle);
      args.AddString("description", contentDescription);
      args.AddUri("picture", photoURL);
      CanvasFacebook.CanvasUIMethodCall<IShareResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IShareResult>(this, "feed", "OnShareLinkComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public override void FeedShare(string toId, Uri link, string linkName, string linkCaption, string linkDescription, Uri picture, string mediaSource, FacebookDelegate<IShareResult> callback)
    {
      MethodArguments args = new MethodArguments();
      args.AddString("to", toId);
      args.AddUri(nameof (link), link);
      args.AddString("name", linkName);
      args.AddString("caption", linkCaption);
      args.AddString("description", linkDescription);
      args.AddUri(nameof (picture), picture);
      args.AddString("source", mediaSource);
      CanvasFacebook.CanvasUIMethodCall<IShareResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IShareResult>(this, "feed", "OnShareLinkComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public void Pay(string product, string action, int quantity, int? quantityMin, int? quantityMax, string requestId, string pricepointId, string testCurrency, FacebookDelegate<IPayResult> callback)
    {
      MethodArguments args = new MethodArguments();
      args.AddString(nameof (product), product);
      args.AddString(nameof (action), action);
      args.AddPrimative<int>(nameof (quantity), quantity);
      args.AddNullablePrimitive<int>("quantity_min", quantityMin);
      args.AddNullablePrimitive<int>("quantity_max", quantityMax);
      args.AddString("request_id", requestId);
      args.AddString("pricepoint_id", pricepointId);
      args.AddString("test_currency", testCurrency);
      CanvasFacebook.CanvasUIMethodCall<IPayResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IPayResult>(this, "pay", "OnPayComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public override void GameGroupCreate(string name, string description, string privacy, FacebookDelegate<IGroupCreateResult> callback)
    {
      MethodArguments args = new MethodArguments();
      args.AddString(nameof (name), name);
      args.AddString(nameof (description), description);
      args.AddString(nameof (privacy), privacy);
      args.AddString("display", "async");
      CanvasFacebook.CanvasUIMethodCall<IGroupCreateResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IGroupCreateResult>(this, "game_group_create", "OnGroupCreateComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public override void GameGroupJoin(string id, FacebookDelegate<IGroupJoinResult> callback)
    {
      MethodArguments args = new MethodArguments();
      args.AddString(nameof (id), id);
      args.AddString("display", "async");
      CanvasFacebook.CanvasUIMethodCall<IGroupJoinResult> canvasUiMethodCall = new CanvasFacebook.CanvasUIMethodCall<IGroupJoinResult>(this, "game_group_join", "OnJoinGroupComplete");
      canvasUiMethodCall.Callback = callback;
      canvasUiMethodCall.Call(args);
    }

    public override void GetAppLink(FacebookDelegate<IAppLinkResult> callback)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>()
      {
        {
          "url",
          (object) this.appLinkUrl
        }
      };
      callback((IAppLinkResult) new AppLinkResult(Json.Serialize((object) dictionary)));
      this.appLinkUrl = string.Empty;
    }

    public override void AppEventsLogEvent(string logEvent, float? valueToSum, Dictionary<string, object> parameters)
    {
      this.canvasJSWrapper.ExternalCall("FBUnity.logAppEvent", (object) logEvent, (object) valueToSum, (object) Json.Serialize((object) parameters));
    }

    public override void AppEventsLogPurchase(float logPurchase, string currency, Dictionary<string, object> parameters)
    {
      this.canvasJSWrapper.ExternalCall("FBUnity.logPurchase", (object) logPurchase, (object) currency, (object) Json.Serialize((object) parameters));
    }

    public override void OnLoginComplete(string responseJsonData)
    {
      this.OnAuthResponse(new LoginResult(CanvasFacebook.FormatAuthResponse(responseJsonData)));
    }

    public override void OnGetAppLinkComplete(string message)
    {
      throw new NotImplementedException();
    }

    public void OnFacebookAuthResponseChange(string responseJsonData)
    {
      AccessToken.CurrentAccessToken = new LoginResult(CanvasFacebook.FormatAuthResponse(responseJsonData)).AccessToken;
    }

    public void OnPayComplete(string responseJsonData)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new PayResult(CanvasFacebook.FormatResult(responseJsonData)));
    }

    public override void OnAppRequestsComplete(string responseJsonData)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AppRequestResult(CanvasFacebook.FormatResult(responseJsonData)));
    }

    public override void OnShareLinkComplete(string responseJsonData)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new ShareResult(CanvasFacebook.FormatResult(responseJsonData)));
    }

    public override void OnGroupCreateComplete(string responseJsonData)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new GroupCreateResult(CanvasFacebook.FormatResult(responseJsonData)));
    }

    public override void OnGroupJoinComplete(string responseJsonData)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new GroupJoinResult(CanvasFacebook.FormatResult(responseJsonData)));
    }

    public void OnUrlResponse(string url)
    {
      this.appLinkUrl = url;
    }

    private static string FormatAuthResponse(string result)
    {
      if (string.IsNullOrEmpty(result))
        return result;
      IDictionary<string, object> responseDictionary = CanvasFacebook.GetFormattedResponseDictionary(result);
      IDictionary<string, object> dictionary;
      if (responseDictionary.TryGetValue<IDictionary<string, object>>("authResponse", out dictionary))
      {
        responseDictionary.Remove("authResponse");
        foreach (KeyValuePair<string, object> keyValuePair in (IEnumerable<KeyValuePair<string, object>>) dictionary)
          responseDictionary[keyValuePair.Key] = keyValuePair.Value;
      }
      return Json.Serialize((object) responseDictionary);
    }

    private static string FormatResult(string result)
    {
      if (string.IsNullOrEmpty(result))
        return result;
      return Json.Serialize((object) CanvasFacebook.GetFormattedResponseDictionary(result));
    }

    private static IDictionary<string, object> GetFormattedResponseDictionary(string result)
    {
      IDictionary<string, object> dictionary1 = (IDictionary<string, object>) Json.Deserialize(result);
      IDictionary<string, object> dictionary2;
      if (!dictionary1.TryGetValue<IDictionary<string, object>>("response", out dictionary2))
        return dictionary1;
      object obj;
      if (dictionary1.TryGetValue("callback_id", out obj))
        dictionary2["callback_id"] = obj;
      return dictionary2;
    }

    private class CanvasUIMethodCall<T> : MethodCall<T> where T : IResult
    {
      private CanvasFacebook canvasImpl;
      private string callbackMethod;

      public CanvasUIMethodCall(CanvasFacebook canvasImpl, string methodName, string callbackMethod)
        : base((FacebookBase) canvasImpl, methodName)
      {
        this.canvasImpl = canvasImpl;
        this.callbackMethod = callbackMethod;
      }

      public override void Call(MethodArguments args)
      {
        this.UI(this.MethodName, args, this.Callback);
      }

      private void UI(string method, MethodArguments args, FacebookDelegate<T> callback = null)
      {
        this.canvasImpl.canvasJSWrapper.DisableFullScreen();
        MethodArguments methodArguments = new MethodArguments(args);
        methodArguments.AddString("app_id", this.canvasImpl.appId);
        methodArguments.AddString(nameof (method), method);
        string str = this.canvasImpl.CallbackManager.AddFacebookDelegate<T>(callback);
        this.canvasImpl.canvasJSWrapper.ExternalCall("FBUnity.ui", (object) methodArguments.ToJsonString(), (object) str, (object) this.callbackMethod);
      }
    }
  }
}
