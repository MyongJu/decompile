﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.GroupCreateResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal class GroupCreateResult : ResultBase, IGroupCreateResult, IResult
  {
    public const string IDKey = "id";

    public GroupCreateResult(string result)
      : base(result)
    {
      string str;
      if (this.ResultDictionary == null || !this.ResultDictionary.TryGetValue<string>("id", out str))
        return;
      this.GroupId = str;
    }

    public string GroupId { get; private set; }
  }
}
