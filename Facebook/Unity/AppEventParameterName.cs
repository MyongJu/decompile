﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.AppEventParameterName
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  public static class AppEventParameterName
  {
    public const string ContentID = "fb_content_id";
    public const string ContentType = "fb_content_type";
    public const string Currency = "fb_currency";
    public const string Description = "fb_description";
    public const string Level = "fb_level";
    public const string MaxRatingValue = "fb_max_rating_value";
    public const string NumItems = "fb_num_items";
    public const string PaymentInfoAvailable = "fb_payment_info_available";
    public const string RegistrationMethod = "fb_registration_method";
    public const string SearchString = "fb_search_string";
    public const string Success = "fb_success";
  }
}
