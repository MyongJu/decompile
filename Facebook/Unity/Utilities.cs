﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Utilities
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Facebook.Unity
{
  internal static class Utilities
  {
    private const string WarningMissingParameter = "Did not find expected value '{0}' in dictionary";

    public static bool TryGetValue<T>(this IDictionary<string, object> dictionary, string key, out T value)
    {
      object obj;
      if (dictionary.TryGetValue(key, out obj) && obj is T)
      {
        value = (T) obj;
        return true;
      }
      value = default (T);
      return false;
    }

    public static long TotalSeconds(this DateTime dateTime)
    {
      return (long) (dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
    }

    public static T GetValueOrDefault<T>(this IDictionary<string, object> dictionary, string key, bool logWarning = true)
    {
      T obj;
      if (!dictionary.TryGetValue<T>(key, out obj))
        FacebookLogger.Warn("Did not find expected value '{0}' in dictionary", key);
      return obj;
    }

    public static string ToCommaSeparateList(this IEnumerable<string> list)
    {
      if (list == null)
        return string.Empty;
      return string.Join(",", list.ToArray<string>());
    }

    public static string AbsoluteUrlOrEmptyString(this Uri uri)
    {
      if (uri == (Uri) null)
        return string.Empty;
      return uri.AbsoluteUri;
    }

    public static string GetUserAgent(string productName, string productVersion)
    {
      return string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0}/{1}", new object[2]
      {
        (object) productName,
        (object) productVersion
      });
    }

    public static string ToJson(this IDictionary<string, object> dictionary)
    {
      return Json.Serialize((object) dictionary);
    }

    public static void AddAllKVPFrom<T1, T2>(this IDictionary<T1, T2> dest, IDictionary<T1, T2> source)
    {
      foreach (T1 key in (IEnumerable<T1>) source.Keys)
        dest[key] = source[key];
    }

    public static AccessToken ParseAccessTokenFromResult(IDictionary<string, object> resultDictionary)
    {
      string valueOrDefault1 = resultDictionary.GetValueOrDefault<string>(LoginResult.UserIdKey, true);
      string valueOrDefault2 = resultDictionary.GetValueOrDefault<string>(LoginResult.AccessTokenKey, true);
      DateTime expirationDateFromResult = Utilities.ParseExpirationDateFromResult(resultDictionary);
      ICollection<string> permissionFromResult = Utilities.ParsePermissionFromResult(resultDictionary);
      DateTime? refreshFromResult = Utilities.ParseLastRefreshFromResult(resultDictionary);
      return new AccessToken(valueOrDefault2, valueOrDefault1, expirationDateFromResult, (IEnumerable<string>) permissionFromResult, refreshFromResult);
    }

    private static DateTime ParseExpirationDateFromResult(IDictionary<string, object> resultDictionary)
    {
      int result;
      return !Constants.IsWeb ? (!int.TryParse(resultDictionary.GetValueOrDefault<string>(LoginResult.ExpirationTimestampKey, true), out result) || result <= 0 ? DateTime.MaxValue : Utilities.FromTimestamp(result)) : DateTime.Now.AddSeconds((double) resultDictionary.GetValueOrDefault<long>(LoginResult.ExpirationTimestampKey, true));
    }

    private static DateTime? ParseLastRefreshFromResult(IDictionary<string, object> resultDictionary)
    {
      int result;
      if (int.TryParse(resultDictionary.GetValueOrDefault<string>(LoginResult.ExpirationTimestampKey, true), out result) && result > 0)
        return new DateTime?(Utilities.FromTimestamp(result));
      return new DateTime?();
    }

    private static ICollection<string> ParsePermissionFromResult(IDictionary<string, object> resultDictionary)
    {
      string str;
      IEnumerable<object> source;
      if (resultDictionary.TryGetValue<string>(LoginResult.PermissionsKey, out str))
        source = (IEnumerable<object>) str.Split(',');
      else if (!resultDictionary.TryGetValue<IEnumerable<object>>(LoginResult.PermissionsKey, out source))
      {
        source = (IEnumerable<object>) new string[0];
        FacebookLogger.Warn("Failed to find parameter '{0}' in login result", LoginResult.PermissionsKey);
      }
      return (ICollection<string>) source.Select<object, string>((Func<object, string>) (permission => permission.ToString())).ToList<string>();
    }

    private static DateTime FromTimestamp(int timestamp)
    {
      return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((double) timestamp);
    }
  }
}
