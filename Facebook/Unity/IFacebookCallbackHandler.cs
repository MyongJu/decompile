﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.IFacebookCallbackHandler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal interface IFacebookCallbackHandler
  {
    void OnInitComplete(string message);

    void OnLoginComplete(string message);

    void OnLogoutComplete(string message);

    void OnGetAppLinkComplete(string message);

    void OnGroupCreateComplete(string message);

    void OnGroupJoinComplete(string message);

    void OnAppRequestsComplete(string message);

    void OnShareLinkComplete(string message);
  }
}
