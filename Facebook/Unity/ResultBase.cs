﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.ResultBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections.Generic;

namespace Facebook.Unity
{
  internal abstract class ResultBase : IInternalResult, IResult
  {
    internal ResultBase(string result)
    {
      string error = (string) null;
      bool cancelled = false;
      string callbackId = (string) null;
      if (!string.IsNullOrEmpty(result))
      {
        Dictionary<string, object> dictionary = Json.Deserialize(result) as Dictionary<string, object>;
        if (dictionary != null)
        {
          this.ResultDictionary = (IDictionary<string, object>) dictionary;
          error = ResultBase.GetErrorValue((IDictionary<string, object>) dictionary);
          cancelled = ResultBase.GetCancelledValue((IDictionary<string, object>) dictionary);
          callbackId = ResultBase.GetCallbackId((IDictionary<string, object>) dictionary);
        }
      }
      this.Init(result, error, cancelled, callbackId);
    }

    internal ResultBase(string result, string error, bool cancelled)
    {
      this.Init(result, error, cancelled, (string) null);
    }

    public virtual string Error { get; protected set; }

    public virtual IDictionary<string, object> ResultDictionary { get; protected set; }

    public virtual string RawResult { get; protected set; }

    public virtual bool Cancelled { get; protected set; }

    public virtual string CallbackId { get; protected set; }

    public override string ToString()
    {
      return string.Format("[BaseResult: Error={0}, Result={1}, RawResult={2}, Cancelled={3}]", (object) this.Error, (object) this.ResultDictionary, (object) this.RawResult, (object) this.Cancelled);
    }

    protected void Init(string result, string error, bool cancelled, string callbackId)
    {
      this.RawResult = result;
      this.Cancelled = cancelled;
      this.Error = error;
      this.CallbackId = callbackId;
    }

    private static string GetErrorValue(IDictionary<string, object> result)
    {
      if (result == null)
        return (string) null;
      string str;
      if (result.TryGetValue<string>("error", out str))
        return str;
      return (string) null;
    }

    private static bool GetCancelledValue(IDictionary<string, object> result)
    {
      object obj;
      if (result == null || !result.TryGetValue("cancelled", out obj))
        return false;
      bool? nullable1 = obj as bool?;
      if (nullable1.HasValue)
      {
        if (nullable1.HasValue)
          return nullable1.Value;
        return false;
      }
      string str = obj as string;
      if (str != null)
        return Convert.ToBoolean(str);
      int? nullable2 = obj as int?;
      if (nullable2.HasValue && nullable2.HasValue)
        return nullable2.Value != 0;
      return false;
    }

    private static string GetCallbackId(IDictionary<string, object> result)
    {
      if (result == null)
        return (string) null;
      string str;
      if (result.TryGetValue<string>("callback_id", out str))
        return str;
      return (string) null;
    }
  }
}
