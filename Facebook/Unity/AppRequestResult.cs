﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.AppRequestResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Facebook.Unity
{
  internal class AppRequestResult : ResultBase, IAppRequestResult, IResult
  {
    public const string RequestIDKey = "request";
    public const string ToKey = "to";

    public AppRequestResult(string result)
      : base(result)
    {
      if (this.ResultDictionary == null)
        return;
      string str1;
      if (this.ResultDictionary.TryGetValue<string>("request", out str1))
        this.RequestID = str1;
      string str2;
      if (this.ResultDictionary.TryGetValue<string>("to", out str2))
      {
        this.To = (IEnumerable<string>) str2.Split(',');
      }
      else
      {
        IEnumerable<object> objects;
        if (!this.ResultDictionary.TryGetValue<IEnumerable<object>>("to", out objects))
          return;
        List<string> stringList = new List<string>();
        foreach (object obj in objects)
        {
          string str3 = obj as string;
          if (str3 != null)
            stringList.Add(str3);
        }
        this.To = (IEnumerable<string>) stringList;
      }
    }

    public string RequestID { get; private set; }

    public IEnumerable<string> To { get; private set; }
  }
}
