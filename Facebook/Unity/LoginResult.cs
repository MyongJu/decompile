﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.LoginResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal class LoginResult : ResultBase, ILoginResult, IResult
  {
    public static readonly string UserIdKey = !Constants.IsWeb ? "user_id" : "userID";
    public static readonly string ExpirationTimestampKey = !Constants.IsWeb ? "expiration_timestamp" : "expiresIn";
    public static readonly string PermissionsKey = !Constants.IsWeb ? "permissions" : "grantedScopes";
    public static readonly string AccessTokenKey = !Constants.IsWeb ? "access_token" : "accessToken";
    public const string LastRefreshKey = "last_refresh";

    internal LoginResult(string response)
      : base(response)
    {
      if (this.ResultDictionary == null || !this.ResultDictionary.ContainsKey(LoginResult.AccessTokenKey))
        return;
      this.AccessToken = Utilities.ParseAccessTokenFromResult(this.ResultDictionary);
    }

    public AccessToken AccessToken { get; private set; }
  }
}
