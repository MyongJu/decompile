﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Mobile.IMobileFacebook
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

namespace Facebook.Unity.Mobile
{
  internal interface IMobileFacebook : IFacebook
  {
    ShareDialogMode ShareDialogMode { get; set; }

    void AppInvite(Uri appLinkUrl, Uri previewImageUrl, FacebookDelegate<IAppInviteResult> callback);

    void FetchDeferredAppLink(FacebookDelegate<IAppLinkResult> callback);

    void RefreshCurrentAccessToken(FacebookDelegate<IAccessTokenRefreshResult> callback);
  }
}
