﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Mobile.MobileFacebookGameObject
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity.Mobile
{
  internal abstract class MobileFacebookGameObject : FacebookGameObject, IFacebookCallbackHandler, IMobileFacebookCallbackHandler
  {
    private IMobileFacebookImplementation MobileFacebook
    {
      get
      {
        return (IMobileFacebookImplementation) this.Facebook;
      }
    }

    public void OnAppInviteComplete(string message)
    {
      this.MobileFacebook.OnAppInviteComplete(message);
    }

    public void OnFetchDeferredAppLinkComplete(string message)
    {
      this.MobileFacebook.OnFetchDeferredAppLinkComplete(message);
    }

    public void OnRefreshCurrentAccessTokenComplete(string message)
    {
      this.MobileFacebook.OnRefreshCurrentAccessTokenComplete(message);
    }
  }
}
