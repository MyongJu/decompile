﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Mobile.Android.AndroidFacebookLoader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity.Mobile.Android
{
  internal class AndroidFacebookLoader : FB.CompiledFacebookLoader
  {
    protected override FacebookGameObject FBGameObject
    {
      get
      {
        AndroidFacebookGameObject component = ComponentFactory.GetComponent<AndroidFacebookGameObject>(ComponentFactory.IfNotExist.AddNew);
        if (component.Facebook == null)
          component.Facebook = (IFacebookImplementation) new AndroidFacebook();
        return (FacebookGameObject) component;
      }
    }
  }
}
