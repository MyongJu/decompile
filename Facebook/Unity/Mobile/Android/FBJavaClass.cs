﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Mobile.Android.FBJavaClass
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Facebook.Unity.Mobile.Android
{
  internal class FBJavaClass : IAndroidJavaClass
  {
    private AndroidJavaClass facebookJavaClass = new AndroidJavaClass("com.facebook.unity.FB");
    private const string FacebookJavaClassName = "com.facebook.unity.FB";

    public T CallStatic<T>(string methodName)
    {
      return this.facebookJavaClass.CallStatic<T>(methodName);
    }

    public void CallStatic(string methodName, params object[] args)
    {
      this.facebookJavaClass.CallStatic(methodName, args);
    }
  }
}
