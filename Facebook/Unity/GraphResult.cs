﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.GraphResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System.Collections.Generic;
using UnityEngine;

namespace Facebook.Unity
{
  internal class GraphResult : ResultBase, IGraphResult, IResult
  {
    internal GraphResult(WWW result)
      : base(result.text, result.error, false)
    {
      this.Init(this.RawResult);
      if (result.error != null)
        return;
      this.Texture = result.texture;
    }

    public IList<object> ResultList { get; private set; }

    public Texture2D Texture { get; private set; }

    private void Init(string rawResult)
    {
      if (string.IsNullOrEmpty(rawResult))
        return;
      object obj = Json.Deserialize(this.RawResult);
      IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
      if (dictionary != null)
      {
        this.ResultDictionary = dictionary;
      }
      else
      {
        IList<object> objectList = obj as IList<object>;
        if (objectList == null)
          return;
        this.ResultList = objectList;
      }
    }
  }
}
