﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.AccessTokenRefreshResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal class AccessTokenRefreshResult : ResultBase, IAccessTokenRefreshResult, IResult
  {
    public AccessTokenRefreshResult(string result)
      : base(result)
    {
      if (this.ResultDictionary == null || !this.ResultDictionary.ContainsKey(LoginResult.AccessTokenKey))
        return;
      this.AccessToken = Utilities.ParseAccessTokenFromResult(this.ResultDictionary);
    }

    public AccessToken AccessToken { get; private set; }
  }
}
