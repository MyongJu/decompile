﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.EditorFacebookLoader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity.Editor
{
  internal class EditorFacebookLoader : FB.CompiledFacebookLoader
  {
    protected override FacebookGameObject FBGameObject
    {
      get
      {
        EditorFacebookGameObject component = ComponentFactory.GetComponent<EditorFacebookGameObject>(ComponentFactory.IfNotExist.AddNew);
        component.Facebook = (IFacebookImplementation) new EditorFacebook();
        return (FacebookGameObject) component;
      }
    }
  }
}
