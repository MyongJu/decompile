﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.EditorFacebook
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using Facebook.Unity.Canvas;
using Facebook.Unity.Editor.Dialogs;
using Facebook.Unity.Mobile;
using System;
using System.Collections.Generic;

namespace Facebook.Unity.Editor
{
  internal class EditorFacebook : FacebookBase, ICanvasFacebook, ICanvasFacebookCallbackHandler, ICanvasFacebookImplementation, IFacebook, IFacebookCallbackHandler, IMobileFacebook, IMobileFacebookCallbackHandler, IMobileFacebookImplementation
  {
    private const string WarningMessage = "You are using the facebook SDK in the Unity Editor. Behavior may not be the same as when used on iOS, Android, or Web.";
    private const string AccessTokenKey = "com.facebook.unity.editor.accesstoken";

    public EditorFacebook()
      : base(new CallbackManager())
    {
    }

    public override bool LimitEventUsage { get; set; }

    public ShareDialogMode ShareDialogMode { get; set; }

    public override string SDKName
    {
      get
      {
        return "FBUnityEditorSDK";
      }
    }

    public override string SDKVersion
    {
      get
      {
        return FacebookSdkVersion.Build;
      }
    }

    private IFacebookCallbackHandler EditorGameObject
    {
      get
      {
        return (IFacebookCallbackHandler) ComponentFactory.GetComponent<EditorFacebookGameObject>(ComponentFactory.IfNotExist.AddNew);
      }
    }

    public override void Init(HideUnityDelegate hideUnityDelegate, InitDelegate onInitComplete)
    {
      FacebookLogger.Warn("You are using the facebook SDK in the Unity Editor. Behavior may not be the same as when used on iOS, Android, or Web.");
      base.Init(hideUnityDelegate, onInitComplete);
      this.EditorGameObject.OnInitComplete(string.Empty);
    }

    public override void LogInWithReadPermissions(IEnumerable<string> permissions, FacebookDelegate<ILoginResult> callback)
    {
      this.LogInWithPublishPermissions(permissions, callback);
    }

    public override void LogInWithPublishPermissions(IEnumerable<string> permissions, FacebookDelegate<ILoginResult> callback)
    {
      MockLoginDialog component = ComponentFactory.GetComponent<MockLoginDialog>(ComponentFactory.IfNotExist.AddNew);
      component.Callback = new EditorFacebookMockDialog.OnComplete(this.EditorGameObject.OnLoginComplete);
      component.CallbackID = this.CallbackManager.AddFacebookDelegate<ILoginResult>(callback);
    }

    public override void AppRequest(string message, OGActionType? actionType, string objectId, IEnumerable<string> to, IEnumerable<object> filters, IEnumerable<string> excludeIds, int? maxRecipients, string data, string title, FacebookDelegate<IAppRequestResult> callback)
    {
      this.ShowEmptyMockDialog<IAppRequestResult>(new EditorFacebookMockDialog.OnComplete(((FacebookBase) this).OnAppRequestsComplete), callback, "Mock App Request");
    }

    public override void ShareLink(Uri contentURL, string contentTitle, string contentDescription, Uri photoURL, FacebookDelegate<IShareResult> callback)
    {
      this.ShowMockShareDialog(nameof (ShareLink), callback);
    }

    public override void FeedShare(string toId, Uri link, string linkName, string linkCaption, string linkDescription, Uri picture, string mediaSource, FacebookDelegate<IShareResult> callback)
    {
      this.ShowMockShareDialog(nameof (FeedShare), callback);
    }

    public override void GameGroupCreate(string name, string description, string privacy, FacebookDelegate<IGroupCreateResult> callback)
    {
      this.ShowEmptyMockDialog<IGroupCreateResult>(new EditorFacebookMockDialog.OnComplete(((FacebookBase) this).OnGroupCreateComplete), callback, "Mock Group Create");
    }

    public override void GameGroupJoin(string id, FacebookDelegate<IGroupJoinResult> callback)
    {
      this.ShowEmptyMockDialog<IGroupJoinResult>(new EditorFacebookMockDialog.OnComplete(((FacebookBase) this).OnGroupJoinComplete), callback, "Mock Group Join");
    }

    public override void ActivateApp(string appId)
    {
      FacebookLogger.Info("This only needs to be called for iOS or Android.");
    }

    public override void GetAppLink(FacebookDelegate<IAppLinkResult> callback)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["url"] = (object) "mockurl://testing.url";
      dictionary["callback_id"] = (object) this.CallbackManager.AddFacebookDelegate<IAppLinkResult>(callback);
      this.OnGetAppLinkComplete(Json.Serialize((object) dictionary));
    }

    public override void AppEventsLogEvent(string logEvent, float? valueToSum, Dictionary<string, object> parameters)
    {
      FacebookLogger.Log("Pew! Pretending to send this off.  Doesn't actually work in the editor");
    }

    public override void AppEventsLogPurchase(float logPurchase, string currency, Dictionary<string, object> parameters)
    {
      FacebookLogger.Log("Pew! Pretending to send this off.  Doesn't actually work in the editor");
    }

    public void AppInvite(Uri appLinkUrl, Uri previewImageUrl, FacebookDelegate<IAppInviteResult> callback)
    {
      this.ShowEmptyMockDialog<IAppInviteResult>(new EditorFacebookMockDialog.OnComplete(this.OnAppInviteComplete), callback, "Mock App Invite");
    }

    public void FetchDeferredAppLink(FacebookDelegate<IAppLinkResult> callback)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["url"] = (object) "mockurl://testing.url";
      dictionary["ref"] = (object) "mock ref";
      dictionary["extras"] = (object) new Dictionary<string, object>()
      {
        {
          "mock extra key",
          (object) "mock extra value"
        }
      };
      dictionary["target_url"] = (object) "mocktargeturl://mocktarget.url";
      dictionary["callback_id"] = (object) this.CallbackManager.AddFacebookDelegate<IAppLinkResult>(callback);
      this.OnFetchDeferredAppLinkComplete(Json.Serialize((object) dictionary));
    }

    public void Pay(string product, string action, int quantity, int? quantityMin, int? quantityMax, string requestId, string pricepointId, string testCurrency, FacebookDelegate<IPayResult> callback)
    {
      this.ShowEmptyMockDialog<IPayResult>(new EditorFacebookMockDialog.OnComplete(this.OnPayComplete), callback, "Mock Pay Dialog");
    }

    public void RefreshCurrentAccessToken(FacebookDelegate<IAccessTokenRefreshResult> callback)
    {
      if (callback == null)
        return;
      Dictionary<string, object> dictionary = new Dictionary<string, object>()
      {
        {
          "callback_id",
          (object) this.CallbackManager.AddFacebookDelegate<IAccessTokenRefreshResult>(callback)
        }
      };
      if (AccessToken.CurrentAccessToken == null)
      {
        dictionary["error"] = (object) "No current access token";
      }
      else
      {
        IDictionary<string, object> source = (IDictionary<string, object>) Json.Deserialize(AccessToken.CurrentAccessToken.ToJson());
        dictionary.AddAllKVPFrom<string, object>(source);
      }
      this.OnRefreshCurrentAccessTokenComplete(dictionary.ToJson());
    }

    public override void OnAppRequestsComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AppRequestResult(message));
    }

    public override void OnGetAppLinkComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AppLinkResult(message));
    }

    public override void OnGroupCreateComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new GroupCreateResult(message));
    }

    public override void OnGroupJoinComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new GroupJoinResult(message));
    }

    public override void OnLoginComplete(string message)
    {
      this.OnAuthResponse(new LoginResult(message));
    }

    public override void OnShareLinkComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new ShareResult(message));
    }

    public void OnAppInviteComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AppInviteResult(message));
    }

    public void OnFetchDeferredAppLinkComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AppLinkResult(message));
    }

    public void OnPayComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new PayResult(message));
    }

    public void OnRefreshCurrentAccessTokenComplete(string message)
    {
      this.CallbackManager.OnFacebookResponse((IInternalResult) new AccessTokenRefreshResult(message));
    }

    public void OnFacebookAuthResponseChange(string message)
    {
      throw new NotSupportedException();
    }

    public void OnUrlResponse(string message)
    {
      throw new NotSupportedException();
    }

    private void ShowEmptyMockDialog<T>(EditorFacebookMockDialog.OnComplete callback, FacebookDelegate<T> userCallback, string title) where T : IResult
    {
      EmptyMockDialog component = ComponentFactory.GetComponent<EmptyMockDialog>(ComponentFactory.IfNotExist.AddNew);
      component.Callback = callback;
      component.CallbackID = this.CallbackManager.AddFacebookDelegate<T>(userCallback);
      component.EmptyDialogTitle = title;
    }

    private void ShowMockShareDialog(string subTitle, FacebookDelegate<IShareResult> userCallback)
    {
      MockShareDialog component = ComponentFactory.GetComponent<MockShareDialog>(ComponentFactory.IfNotExist.AddNew);
      component.SubTitle = subTitle;
      component.Callback = new EditorFacebookMockDialog.OnComplete(this.EditorGameObject.OnShareLinkComplete);
      component.CallbackID = this.CallbackManager.AddFacebookDelegate<IShareResult>(userCallback);
    }
  }
}
