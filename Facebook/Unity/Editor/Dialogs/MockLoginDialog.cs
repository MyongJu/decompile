﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.Dialogs.MockLoginDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Facebook.Unity.Editor.Dialogs
{
  internal class MockLoginDialog : EditorFacebookMockDialog
  {
    private string accessToken = string.Empty;

    protected override string DialogTitle
    {
      get
      {
        return "Mock Login Dialog";
      }
    }

    protected override void DoGui()
    {
      GUILayout.BeginHorizontal();
      GUILayout.Label("User Access Token:");
      this.accessToken = GUILayout.TextField(this.accessToken, GUI.skin.textArea, GUILayout.MinWidth(400f));
      GUILayout.EndHorizontal();
      GUILayout.Space(10f);
      if (GUILayout.Button("Find Access Token"))
        Application.OpenURL(string.Format("https://developers.facebook.com/tools/accesstoken/?app_id={0}", (object) FB.AppId));
      GUILayout.Space(20f);
    }

    protected override void SendSuccessResult()
    {
      if (string.IsNullOrEmpty(this.accessToken))
        this.SendErrorResult("Empty Access token string");
      else
        FB.API("/me?fields=id&access_token=" + this.accessToken, HttpMethod.GET, (FacebookDelegate<IGraphResult>) (graphResult =>
        {
          if (!string.IsNullOrEmpty(graphResult.Error))
          {
            this.SendErrorResult("Graph API error: " + graphResult.Error);
          }
          else
          {
            string facebookID = graphResult.ResultDictionary["id"] as string;
            FB.API("/me/permissions?access_token=" + this.accessToken, HttpMethod.GET, (FacebookDelegate<IGraphResult>) (permResult =>
            {
              if (!string.IsNullOrEmpty(permResult.Error))
              {
                this.SendErrorResult("Graph API error: " + permResult.Error);
              }
              else
              {
                List<string> stringList1 = new List<string>();
                List<string> stringList2 = new List<string>();
                using (List<object>.Enumerator enumerator = (permResult.ResultDictionary["data"] as List<object>).GetEnumerator())
                {
                  while (enumerator.MoveNext())
                  {
                    Dictionary<string, object> current = (Dictionary<string, object>) enumerator.Current;
                    if (current["status"] as string == "granted")
                      stringList1.Add(current["permission"] as string);
                    else
                      stringList2.Add(current["permission"] as string);
                  }
                }
                IDictionary<string, object> dictionary = (IDictionary<string, object>) Json.Deserialize(new AccessToken(this.accessToken, facebookID, DateTime.Now.AddDays(60.0), (IEnumerable<string>) stringList1, new DateTime?(DateTime.Now)).ToJson());
                dictionary.Add("granted_permissions", (object) stringList1);
                dictionary.Add("declined_permissions", (object) stringList2);
                if (!string.IsNullOrEmpty(this.CallbackID))
                  dictionary["callback_id"] = (object) this.CallbackID;
                if (this.Callback == null)
                  return;
                this.Callback(Json.Serialize((object) dictionary));
              }
            }), (IDictionary<string, string>) null);
          }
        }), (IDictionary<string, string>) null);
    }
  }
}
