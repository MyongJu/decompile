﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.Dialogs.MockShareDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Facebook.Unity.Editor.Dialogs
{
  internal class MockShareDialog : EditorFacebookMockDialog
  {
    public string SubTitle { private get; set; }

    protected override string DialogTitle
    {
      get
      {
        return "Mock " + this.SubTitle + " Dialog";
      }
    }

    protected override void DoGui()
    {
    }

    protected override void SendSuccessResult()
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      if (FB.IsLoggedIn)
        dictionary["postId"] = (object) this.GenerateFakePostID();
      else
        dictionary["did_complete"] = (object) true;
      if (!string.IsNullOrEmpty(this.CallbackID))
        dictionary["callback_id"] = (object) this.CallbackID;
      if (this.Callback == null)
        return;
      this.Callback(Json.Serialize((object) dictionary));
    }

    protected override void SendCancelResult()
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["cancelled"] = (object) "true";
      if (!string.IsNullOrEmpty(this.CallbackID))
        dictionary["callback_id"] = (object) this.CallbackID;
      this.Callback(Json.Serialize((object) dictionary));
    }

    private string GenerateFakePostID()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append(AccessToken.CurrentAccessToken.UserId);
      stringBuilder.Append('_');
      for (int index = 0; index < 17; ++index)
        stringBuilder.Append(Random.Range(0, 10));
      return stringBuilder.ToString();
    }
  }
}
