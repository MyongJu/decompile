﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.Dialogs.EmptyMockDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System.Collections.Generic;

namespace Facebook.Unity.Editor.Dialogs
{
  internal class EmptyMockDialog : EditorFacebookMockDialog
  {
    public string EmptyDialogTitle { get; set; }

    protected override string DialogTitle
    {
      get
      {
        return this.EmptyDialogTitle;
      }
    }

    protected override void DoGui()
    {
    }

    protected override void SendSuccessResult()
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["did_complete"] = (object) true;
      if (!string.IsNullOrEmpty(this.CallbackID))
        dictionary["callback_id"] = (object) this.CallbackID;
      if (this.Callback == null)
        return;
      this.Callback(Json.Serialize((object) dictionary));
    }
  }
}
