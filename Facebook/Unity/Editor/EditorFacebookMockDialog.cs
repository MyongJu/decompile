﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.Editor.EditorFacebookMockDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.MiniJSON;
using System.Collections.Generic;
using UnityEngine;

namespace Facebook.Unity.Editor
{
  internal abstract class EditorFacebookMockDialog : MonoBehaviour
  {
    private Rect modalRect;
    private GUIStyle modalStyle;

    public EditorFacebookMockDialog.OnComplete Callback { protected get; set; }

    public string CallbackID { protected get; set; }

    protected abstract string DialogTitle { get; }

    public void Start()
    {
      this.modalRect = new Rect(10f, 10f, (float) (Screen.width - 20), (float) (Screen.height - 20));
      Texture2D texture2D = new Texture2D(1, 1);
      texture2D.SetPixel(0, 0, new Color(0.2f, 0.2f, 0.2f, 1f));
      texture2D.Apply();
      this.modalStyle = new GUIStyle();
      this.modalStyle.normal.background = texture2D;
    }

    public void OnGUI()
    {
      GUI.ModalWindow(this.GetHashCode(), this.modalRect, new GUI.WindowFunction(this.OnGUIDialog), this.DialogTitle, this.modalStyle);
    }

    protected abstract void DoGui();

    protected abstract void SendSuccessResult();

    protected virtual void SendCancelResult()
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["cancelled"] = (object) true;
      if (!string.IsNullOrEmpty(this.CallbackID))
        dictionary["callback_id"] = (object) this.CallbackID;
      this.Callback(Json.Serialize((object) dictionary));
    }

    protected virtual void SendErrorResult(string errorMessage)
    {
      Dictionary<string, object> dictionary = new Dictionary<string, object>();
      dictionary["error"] = (object) errorMessage;
      if (!string.IsNullOrEmpty(this.CallbackID))
        dictionary["callback_id"] = (object) this.CallbackID;
      this.Callback(Json.Serialize((object) dictionary));
    }

    private void OnGUIDialog(int windowId)
    {
      GUILayout.Space(10f);
      GUILayout.BeginVertical();
      GUILayout.Label("Warning! Mock dialog responses will NOT match production dialogs");
      GUILayout.Label("Test your app on one of the supported platforms");
      this.DoGui();
      GUILayout.EndVertical();
      GUILayout.BeginHorizontal();
      GUILayout.FlexibleSpace();
      GUIContent content1 = new GUIContent("Send Success");
      if (GUI.Button(GUILayoutUtility.GetRect(content1, GUI.skin.button), content1))
      {
        this.SendSuccessResult();
        Object.Destroy((Object) this);
      }
      GUIContent content2 = new GUIContent("Send Cancel");
      if (GUI.Button(GUILayoutUtility.GetRect(content2, GUI.skin.button), content2, GUI.skin.button))
      {
        this.SendCancelResult();
        Object.Destroy((Object) this);
      }
      GUIContent content3 = new GUIContent("Send Error");
      if (GUI.Button(GUILayoutUtility.GetRect(content2, GUI.skin.button), content3, GUI.skin.button))
      {
        this.SendErrorResult("Error: Error button pressed");
        Object.Destroy((Object) this);
      }
      GUILayout.EndHorizontal();
    }

    public delegate void OnComplete(string result);
  }
}
