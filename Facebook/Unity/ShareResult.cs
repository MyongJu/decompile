﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.ShareResult
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace Facebook.Unity
{
  internal class ShareResult : ResultBase, IResult, IShareResult
  {
    internal ShareResult(string result)
      : base(result)
    {
      object obj;
      if (this.ResultDictionary == null || !this.ResultDictionary.TryGetValue("id", out obj))
        return;
      this.PostId = obj as string;
    }

    public string PostId { get; private set; }
  }
}
