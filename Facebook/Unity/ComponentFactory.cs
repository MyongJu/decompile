﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.ComponentFactory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Facebook.Unity
{
  internal class ComponentFactory
  {
    public const string GameObjectName = "UnityFacebookSDKPlugin";
    private static GameObject facebookGameObject;

    private static GameObject FacebookGameObject
    {
      get
      {
        if ((Object) ComponentFactory.facebookGameObject == (Object) null)
          ComponentFactory.facebookGameObject = new GameObject("UnityFacebookSDKPlugin");
        return ComponentFactory.facebookGameObject;
      }
    }

    public static T GetComponent<T>(ComponentFactory.IfNotExist ifNotExist = ComponentFactory.IfNotExist.AddNew) where T : MonoBehaviour
    {
      GameObject facebookGameObject = ComponentFactory.FacebookGameObject;
      T obj = facebookGameObject.GetComponent<T>();
      if ((Object) obj == (Object) null && ifNotExist == ComponentFactory.IfNotExist.AddNew)
        obj = facebookGameObject.AddComponent<T>();
      return obj;
    }

    public static T AddComponent<T>() where T : MonoBehaviour
    {
      return ComponentFactory.FacebookGameObject.AddComponent<T>();
    }

    internal enum IfNotExist
    {
      AddNew,
      ReturnNull,
    }
  }
}
