﻿// Decompiled with JetBrains decompiler
// Type: Facebook.Unity.FacebookGameObject
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Facebook.Unity
{
  internal abstract class FacebookGameObject : MonoBehaviour, IFacebookCallbackHandler
  {
    public IFacebookImplementation Facebook { get; set; }

    public void Awake()
    {
      Object.DontDestroyOnLoad((Object) this);
      AccessToken.CurrentAccessToken = (AccessToken) null;
      this.OnAwake();
    }

    public void OnInitComplete(string message)
    {
      this.Facebook.OnInitComplete(message);
    }

    public void OnLoginComplete(string message)
    {
      this.Facebook.OnLoginComplete(message);
    }

    public void OnLogoutComplete(string message)
    {
      this.Facebook.OnLogoutComplete(message);
    }

    public void OnGetAppLinkComplete(string message)
    {
      this.Facebook.OnGetAppLinkComplete(message);
    }

    public void OnGroupCreateComplete(string message)
    {
      this.Facebook.OnGroupCreateComplete(message);
    }

    public void OnGroupJoinComplete(string message)
    {
      this.Facebook.OnGroupJoinComplete(message);
    }

    public void OnAppRequestsComplete(string message)
    {
      this.Facebook.OnAppRequestsComplete(message);
    }

    public void OnShareLinkComplete(string message)
    {
      this.Facebook.OnShareLinkComplete(message);
    }

    protected virtual void OnAwake()
    {
    }
  }
}
