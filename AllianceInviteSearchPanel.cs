﻿// Decompiled with JetBrains decompiler
// Type: AllianceInviteSearchPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class AllianceInviteSearchPanel : MonoBehaviour
{
  private int m_MemberID = -1;
  public UITexture mIcon;
  public UILabel mName;
  public UILabel mPower;
  public UILabel mLevel;
  public System.Action onInviteSucceed;

  public void SetDetails(int memberID, string name, int power, int level, int portrait)
  {
    this.m_MemberID = memberID;
    this.mName.text = name;
    this.mPower.text = power.ToString();
    this.mLevel.text = level.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.mIcon, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (object) portrait, (System.Action<bool>) null, true, false, string.Empty);
  }

  public void OnInviteBtnPressed()
  {
    MessageHub.inst.GetPortByAction("Alliance:allianceSendInvite").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "invite_uid",
        (object) this.m_MemberID
      }
    }, new System.Action<bool, object>(this.AllianceInvitedMemberCallback), true);
  }

  public void OnInfoBtnPressed()
  {
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = (long) this.m_MemberID
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void AllianceInvitedMemberCallback(bool ret, object data)
  {
    if (!ret || this.onInviteSucceed == null)
      return;
    this.onInviteSucceed();
  }
}
