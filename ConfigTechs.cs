﻿// Decompiled with JetBrains decompiler
// Type: ConfigTechs
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTechs
{
  public Dictionary<int, Tech> Techs = new Dictionary<int, Tech>();
  private Dictionary<string, int> TechIDs = new Dictionary<string, int>();

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "{0} load error - invalid hashtable", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "{0} load error - invalid header hashtable", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "image");
        int index3 = ConfigManager.GetIndex(inHeader, "tree");
        int index4 = ConfigManager.GetIndex(inHeader, "row");
        int index5 = ConfigManager.GetIndex(inHeader, "tier");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
              this.Add(new Tech(result, ConfigManager.GetString(arr, index1), ConfigManager.GetString(arr, index2), ConfigManager.GetInt(arr, index3), ConfigManager.GetInt(arr, index4), ConfigManager.GetInt(arr, index5)));
          }
        }
      }
    }
  }

  private void Add(Tech tech)
  {
    if (this.Techs.ContainsKey(tech.InternalID))
      return;
    this.Techs.Add(tech.InternalID, tech);
    if (!this.TechIDs.ContainsKey(tech.ID))
      this.TechIDs.Add(tech.ID, tech.InternalID);
    ConfigManager.inst.AddData(tech.InternalID, (object) tech);
  }

  public void Clear()
  {
    if (this.Techs != null)
      this.Techs.Clear();
    if (this.TechIDs == null)
      return;
    this.TechIDs.Clear();
  }

  public Tech this[string ID]
  {
    get
    {
      return this.GetData(ID);
    }
  }

  public Tech this[int internalID]
  {
    get
    {
      return this.GetData(internalID);
    }
  }

  public bool Contains(string ID)
  {
    return this.TechIDs.ContainsKey(ID);
  }

  public bool Contains(int internalID)
  {
    return this.Techs.ContainsKey(internalID);
  }

  private Tech GetData(int internalID)
  {
    if (this.Techs.ContainsKey(internalID))
      return this.Techs[internalID];
    return (Tech) null;
  }

  private Tech GetData(string ID)
  {
    if (this.TechIDs.ContainsKey(ID))
      return this.Techs[this.TechIDs[ID]];
    return (Tech) null;
  }
}
