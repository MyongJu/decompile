﻿// Decompiled with JetBrains decompiler
// Type: SkillEffectTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SkillEffectTest : MonoBehaviour
{
  public DragonView source;
  public IVfxTarget target;
  private SkillEffectPlayer _player;

  private void Start()
  {
  }

  private void Play()
  {
    SkillEffectData data = new SkillEffectData();
    data.animation = "attack";
    data.sourceVfx = "fire";
    data.sourceEventId = 10;
    data.targetVfx = "fire";
    data.targetEventId = 10;
    this._player = new SkillEffectPlayer();
    this._player.SetData(data);
    this._player.Play((SpriteView) this.source, this.target);
  }

  private void Stop()
  {
    this._player.Stop(false);
  }
}
