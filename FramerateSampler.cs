﻿// Decompiled with JetBrains decompiler
// Type: FramerateSampler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class FramerateSampler : MonoBehaviour
{
  private float _updateInterval = 1f;
  private Queue<float> _recordQueue = new Queue<float>(11);
  private const int _recordCount = 10;
  private static FramerateSampler _instance;
  private float _lastInterval;
  private int _frames;
  private float _fps;
  public bool needFake;
  private float _currentFPS;

  public static FramerateSampler Instance
  {
    get
    {
      if ((UnityEngine.Object) FramerateSampler._instance == (UnityEngine.Object) null)
      {
        FramerateSampler._instance = UnityEngine.Object.FindObjectOfType<FramerateSampler>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) FramerateSampler._instance)
          throw new ArgumentException("FramerateSampler hasn't been created yet.");
      }
      return FramerateSampler._instance;
    }
  }

  public void Startup()
  {
  }

  private void Update()
  {
    ++this._frames;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if ((double) realtimeSinceStartup <= (double) this._lastInterval + (double) this._updateInterval)
      return;
    this._fps = (float) this._frames / (realtimeSinceStartup - this._lastInterval);
    this._frames = 0;
    this._lastInterval = realtimeSinceStartup;
    this._recordQueue.Enqueue(this._fps);
    if (this._recordQueue.Count > 10)
    {
      double num = (double) this._recordQueue.Dequeue();
    }
    this._currentFPS = this.GetAverageFPS();
  }

  private float GetAverageFPS()
  {
    float num = 0.0f;
    using (Queue<float>.Enumerator enumerator = this._recordQueue.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        float current = enumerator.Current;
        num += current;
      }
    }
    return num / (float) this._recordQueue.Count;
  }

  public float CurrentFPS
  {
    get
    {
      if (this.needFake)
        return 2f;
      return this._currentFPS;
    }
  }
}
