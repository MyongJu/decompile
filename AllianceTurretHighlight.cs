﻿// Decompiled with JetBrains decompiler
// Type: AllianceTurretHighlight
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTurretHighlight : MonoBehaviour
{
  public float WALK_CROSS_FACTOR = 0.5f;
  public float VIEWPORT_TOP = 1f;
  public float VIEWPORT_BOTTOM = 2.1f;
  public float VIEWPORT_LEFT = 1.5f;
  public float VIEWPORT_RIGHT = 1.5f;
  private const float POSITION_TOLERANCE = 0.001f;
  private const float MOVEMENT_TOLERANCE = 0.001f;
  public SpriteRenderer Background;
  public BoxCollider2D Response;
  public UIButton PlaceFortress;
  public AllianceTurretHighlight.OnConfirmCallback OnConfirm;
  public AllianceTurretHighlight.OnCancelCallback OnCancel;
  private WorldCoordinate m_WorldLocation;
  private bool m_Dragging;
  private Vector3 m_LastCursorPosition;
  private Vector2 m_LastScreenPosition;
  private TileCamera m_TileCamera;
  private int m_BuildingConfigId;

  public void InitCamera(TileCamera tileCamera)
  {
    this.m_TileCamera = tileCamera;
  }

  public void Setup(WorldCoordinate target, int internalID, bool keepTargetInCenter = false)
  {
    this.m_BuildingConfigId = internalID;
    this.SetLocation(target, keepTargetInCenter);
    this.GenerateBuilding();
  }

  private void SetLocation(WorldCoordinate target, bool keepTargetInCenter = false)
  {
    this.transform.localPosition = PVPMapData.MapData.ConvertWorldCoordinateToPixelPosition(target);
    this.transform.localRotation = Quaternion.identity;
    this.transform.localScale = Vector3.one;
    this.UpdateBackgroundColor();
    if (keepTargetInCenter)
      this.AdjustCameraToTargetCenter();
    else
      this.AdjustCamera();
  }

  public void OnConfirmPressed()
  {
    Coordinate kxy = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
    this.PlaceFortressAt(kxy);
    if (this.OnConfirm == null)
      return;
    this.OnConfirm(kxy);
  }

  public void OnCancelButtonPressed()
  {
    if (this.OnCancel == null)
      return;
    this.OnCancel();
  }

  public bool IsCollide(WorldCoordinate location)
  {
    Vector3 worldPosition = PVPMapData.MapData.ConvertWorldCoordinateToWorldPosition(location);
    return TeleportHighlight.Box2DOverlapPoint(this.Response, new Vector2(worldPosition.x, worldPosition.y));
  }

  public void StartDragging(DragGesture gesture)
  {
    this.m_LastScreenPosition = gesture.Position;
    this.m_LastCursorPosition = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
    this.m_Dragging = this.IsCollide(PVPMapData.MapData.ConvertWorldPositionToWorldCoordinate(this.m_LastCursorPosition));
  }

  public void UpdateDragging(DragGesture gesture)
  {
    if (!this.m_Dragging)
      return;
    Vector3 worldHitPoint = this.m_TileCamera.GetWorldHitPoint((Vector3) gesture.Position);
    if ((double) Vector3.Distance((Vector3) this.m_LastScreenPosition, (Vector3) gesture.Position) <= 1.0 / 1000.0)
      return;
    this.m_LastScreenPosition = gesture.Position;
    Vector3 vector3 = worldHitPoint - this.m_LastCursorPosition;
    this.m_LastCursorPosition = worldHitPoint;
    this.transform.position = this.transform.position + (!this.IsWalkCross ? 1f : this.WALK_CROSS_FACTOR) * vector3;
    this.ClampPosition();
    this.UpdateBackgroundColor();
    this.AdjustCamera();
  }

  private void AdjustCamera()
  {
    Rect viewport = this.GetViewport();
    Rect range = this.CalculateRange();
    Vector3 zero = Vector3.zero;
    float x1 = viewport.min.x;
    float x2 = viewport.max.x;
    float y1 = viewport.min.y;
    float y2 = viewport.max.y;
    if ((double) range.min.x < (double) x1)
      zero.x = range.min.x - x1;
    if ((double) range.max.x > (double) x2)
      zero.x = range.max.x - x2;
    if ((double) range.min.y < (double) y1)
      zero.y = range.min.y - y1;
    if ((double) range.max.y > (double) y2)
      zero.y = range.max.y - y2;
    this.m_TileCamera.Translate(zero);
  }

  private void AdjustCameraToTargetCenter()
  {
    Vector3 offset = this.Response.transform.position - this.m_TileCamera.transform.position;
    offset.z = 0.0f;
    this.m_TileCamera.Translate(offset);
  }

  private bool IsWalkCross
  {
    get
    {
      Rect viewport = this.GetViewport();
      Rect range = this.CalculateRange();
      float x1 = viewport.min.x;
      float x2 = viewport.max.x;
      float y1 = viewport.min.y;
      float y2 = viewport.max.y;
      if ((double) range.min.x >= (double) x1 + 1.0 / 1000.0 && (double) range.max.x <= (double) x2 - 1.0 / 1000.0 && (double) range.min.y >= (double) y1 + 1.0 / 1000.0)
        return (double) range.max.y > (double) y2 - 1.0 / 1000.0;
      return true;
    }
  }

  private Rect GetViewport()
  {
    Rect rect = new Rect();
    if ((UnityEngine.Object) this.m_TileCamera != (UnityEngine.Object) null)
    {
      Rect viewport = this.m_TileCamera.GetViewport();
      float x1 = viewport.min.x + this.VIEWPORT_LEFT;
      float x2 = viewport.max.x - this.VIEWPORT_RIGHT;
      float y1 = viewport.min.y + this.VIEWPORT_BOTTOM;
      float y2 = viewport.max.y - this.VIEWPORT_TOP;
      rect.min = new Vector2(x1, y1);
      rect.max = new Vector2(x2, y2);
    }
    return rect;
  }

  public void StopDragging()
  {
    if (this.m_Dragging)
      this.SetLocation(PVPMapData.MapData.ConvertWorldPositionToWorldCoordinate(this.transform.position), false);
    this.m_Dragging = false;
  }

  private void Update()
  {
    this.UpdateBackgroundColor();
  }

  private static bool CanPlaceAt(Coordinate location)
  {
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(location);
    if (referenceAt == null || location.X == 0 || location.Y == 0)
      return false;
    if (referenceAt.TileType != TileType.Terrain)
      return referenceAt.TileType == TileType.None;
    return true;
  }

  private void UpdateBackgroundColor()
  {
    this.m_WorldLocation = PVPMapData.MapData.ConvertPixelPositionToWorldCoordinate((Vector2) this.transform.localPosition);
    Coordinate kxy = PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation);
    bool flag = AllianceTurretHighlight.CanPlaceAt(kxy) && this.Contains(kxy);
    this.Background.material.SetColor("_Color", !flag ? Color.red : Color.green);
    this.PlaceFortress.isEnabled = flag;
  }

  private bool Contains(Coordinate p2)
  {
    List<ZoneBorderData> bordersByAllianceId = DBManager.inst.DB_ZoneBorder.GetZoneBordersByAllianceID(PlayerData.inst.allianceId);
    for (int index = 0; index < bordersByAllianceId.Count; ++index)
    {
      if (bordersByAllianceId[index].Contains(p2))
        return true;
    }
    return false;
  }

  private Rect CalculateRange()
  {
    return new Rect()
    {
      min = (Vector2) this.Response.bounds.min,
      max = (Vector2) this.Response.bounds.max
    };
  }

  private Rect GetConstraint()
  {
    float num = 0.01f;
    if ((bool) ((UnityEngine.Object) GameEngine.Instance))
      num = GameEngine.Instance.tileMapScale;
    Coordinate location = PlayerData.inst.CityData.Location;
    location.X = 1;
    location.Y = 1;
    Vector3 worldPosition1 = PVPMapData.MapData.ConvertTileKXYToWorldPosition(location);
    location.X = 1279;
    location.Y = 2559;
    Vector3 worldPosition2 = PVPMapData.MapData.ConvertTileKXYToWorldPosition(location);
    return new Rect()
    {
      xMin = worldPosition1.x,
      xMax = worldPosition2.x,
      yMin = worldPosition2.y,
      yMax = worldPosition1.y
    };
  }

  private void ClampPosition()
  {
    Rect constraint = this.GetConstraint();
    Vector3 position = this.transform.position;
    position.x = Mathf.Clamp(position.x, constraint.min.x, constraint.max.x);
    position.y = Mathf.Clamp(position.y, constraint.min.y, constraint.max.y);
    this.transform.position = position;
  }

  private void PlaceFortressAt(Coordinate targetLocation)
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.m_BuildingConfigId);
    if (buildingStaticInfo == null)
      return;
    string str = string.Format(ScriptLocalization.Get("alliance_buildings_confirm_placement_description", true), (object) ScriptLocalization.Get(buildingStaticInfo.BuildName, true));
    UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
    {
      Title = ScriptLocalization.Get("id_uppercase_confirm", true),
      Content = str,
      Okay = ScriptLocalization.Get("id_uppercase_confirm", true),
      OkayCallback = (System.Action) (() =>
      {
        Hashtable postData = new Hashtable();
        postData[(object) "k"] = (object) targetLocation.K;
        postData[(object) "x"] = (object) targetLocation.X;
        postData[(object) "y"] = (object) targetLocation.Y;
        postData[(object) "config_id"] = (object) this.m_BuildingConfigId;
        MessageHub.inst.GetPortByAction("Map:placeAllianceFortress").SendRequest(postData, new System.Action<bool, object>(this.OnTeleportCallback), true);
      })
    });
  }

  private void OnTeleportCallback(bool ret, object orgData)
  {
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "errno"))
      return;
    int result = 0;
    int.TryParse(hashtable[(object) "errno"].ToString(), out result);
    if (result != 1400040)
      return;
    Utils.RefreshBlock(PVPMapData.MapData.ConvertWorldCoordinateToKXY(this.m_WorldLocation), (System.Action<bool, object>) null);
  }

  private void GenerateBuilding()
  {
    GameObject go = PrefabManagerEx.Instance.Spawn(MapUtils.GetAllianceBuildingPrefab(1), PVPSystem.Instance.Map.Fortress);
    if (!(bool) ((UnityEngine.Object) go))
      return;
    go.transform.parent = this.transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localRotation = Quaternion.identity;
    go.transform.localScale = Vector3.one;
    go.GetComponent<AllianceFortressController>().Reset();
    List<SpriteRenderer> spriteRendererList = new List<SpriteRenderer>();
    go.GetComponentsInChildren<SpriteRenderer>(true, (List<M0>) spriteRendererList);
    int count = spriteRendererList.Count;
    for (int index = 0; index < count; ++index)
    {
      spriteRendererList[index].sortingOrder += 200;
      spriteRendererList[index].sortingLayerName = "HUD";
    }
    int layer = LayerMask.NameToLayer("TeleportUI");
    NGUITools.SetLayer(go, layer);
  }

  public delegate void OnConfirmCallback(Coordinate target);

  public delegate void OnCancelCallback();
}
