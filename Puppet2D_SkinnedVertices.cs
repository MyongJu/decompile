﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_SkinnedVertices
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_SkinnedVertices : MonoBehaviour
{
  private List<List<Puppet2D_SkinnedVertices.Bone>> allBones = new List<List<Puppet2D_SkinnedVertices.Bone>>();
  private Mesh mesh;

  private void Start()
  {
    SkinnedMeshRenderer component = this.GetComponent(typeof (SkinnedMeshRenderer)) as SkinnedMeshRenderer;
    this.mesh = component.sharedMesh;
    for (int index1 = 0; index1 < this.mesh.vertexCount; ++index1)
    {
      Vector3 position = this.transform.TransformPoint(this.mesh.vertices[index1]);
      BoneWeight boneWeight = this.mesh.boneWeights[index1];
      int[] numArray1 = new int[4]
      {
        boneWeight.boneIndex0,
        boneWeight.boneIndex1,
        boneWeight.boneIndex2,
        boneWeight.boneIndex3
      };
      float[] numArray2 = new float[4]
      {
        boneWeight.weight0,
        boneWeight.weight1,
        boneWeight.weight2,
        boneWeight.weight3
      };
      List<Puppet2D_SkinnedVertices.Bone> boneList = new List<Puppet2D_SkinnedVertices.Bone>();
      this.allBones.Add(boneList);
      for (int index2 = 0; index2 < 4; ++index2)
      {
        if ((double) numArray2[index2] > 0.0)
        {
          Puppet2D_SkinnedVertices.Bone bone = new Puppet2D_SkinnedVertices.Bone();
          boneList.Add(bone);
          bone.bone = component.bones[numArray1[index2]];
          bone.weight = numArray2[index2];
          bone.delta = bone.bone.InverseTransformPoint(position);
        }
      }
    }
  }

  private void OnDrawGizmos()
  {
    if (!Application.isPlaying || !this.enabled)
      return;
    for (int index = 0; index < this.mesh.vertexCount; ++index)
    {
      List<Puppet2D_SkinnedVertices.Bone> allBone = this.allBones[index];
      Vector3 zero1 = Vector3.zero;
      using (List<Puppet2D_SkinnedVertices.Bone>.Enumerator enumerator = allBone.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Puppet2D_SkinnedVertices.Bone current = enumerator.Current;
          zero1 += current.bone.TransformPoint(current.delta) * current.weight;
        }
      }
      int count = allBone.Count;
      Color color;
      switch (count)
      {
        case 2:
          color = Color.green;
          break;
        case 3:
          color = Color.blue;
          break;
        case 4:
          color = Color.red;
          break;
        default:
          color = Color.black;
          break;
      }
      Gizmos.color = color;
      Gizmos.DrawWireCube(zero1, (float) count * 0.05f * Vector3.one);
      Vector3 zero2 = Vector3.zero;
      using (List<Puppet2D_SkinnedVertices.Bone>.Enumerator enumerator = allBone.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Puppet2D_SkinnedVertices.Bone current = enumerator.Current;
          zero2 += current.bone.TransformDirection(this.mesh.normals[index]) * current.weight;
        }
      }
    }
  }

  private class Bone
  {
    internal Transform bone;
    internal float weight;
    internal Vector3 delta;
  }
}
