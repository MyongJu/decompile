﻿// Decompiled with JetBrains decompiler
// Type: AllianceRallyEmptySlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class AllianceRallyEmptySlotItem : MonoBehaviour
{
  private bool isLock = true;
  public GameObject lockContent;
  public GameObject unlockContent;
  public UIButton joinBt;
  public System.Action onClickDelegate;
  private int req_lev;
  private AllianceWarManager.WarData warData;
  private bool isDestroy;
  private bool isReadyJoin;

  public void SetData(RallyInfoData data)
  {
    this.Data = data;
  }

  private void JoinHandler()
  {
    if (this.Data.RemainCount <= 0L)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_rally_capacity_full", true), (System.Action) null, 4f, false);
    }
    else
    {
      RallyData rallyData = DBManager.inst.DB_Rally.Get(this.Data.DataID);
      if (rallyData.IsWonderRally() && GameEngine.Instance.marchSystem.HasMarchToWonder())
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("throne_invasion_existing_garrison_gather", true), (System.Action) null, 4f, false);
      }
      else
      {
        if (this.Data.Type == RallyInfoData.RallyDataType.GVE)
        {
          WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(rallyData.bossId);
          if (worldBossData != null)
          {
            if (PlayerData.inst.heroData.spirit < ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId).StaminaCost)
            {
              Utils.ShowException(1200050);
              return;
            }
          }
          else
          {
            GameConfigInfo data = ConfigManager.inst._DB_GameConfig.GetData("gve_boss_spirit_cost");
            if (data != null && PlayerData.inst.heroData.spirit < data.ValueInt)
            {
              Utils.ShowException(1200050);
              return;
            }
          }
        }
        UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
        {
          marchType = this.Data.ReinforceMarchType,
          location = this.Data.ReinforceLocation,
          rallyId = this.Data.DataID,
          desTroopCount = (int) this.Data.RemainCount
        }, 1 != 0, 1 != 0, 1 != 0);
      }
    }
  }

  private void OnDestroy()
  {
    this.isDestroy = true;
  }

  public void Join()
  {
    this.JoinHandler();
  }

  public void Lock()
  {
    NGUITools.SetActive(this.lockContent, true);
    NGUITools.SetActive(this.unlockContent, false);
  }

  public void UnLock()
  {
    this.isLock = false;
    NGUITools.SetActive(this.lockContent, false);
    NGUITools.SetActive(this.unlockContent, true);
    this.CheckCanJoin();
  }

  public void OnTabJoinHandler()
  {
  }

  public void Refresh()
  {
    if (this.isLock)
      return;
    this.CheckCanJoin();
  }

  public RallyInfoData Data { get; set; }

  private void CheckCanJoin()
  {
    NGUITools.SetActive(this.joinBt.gameObject, this.Data.CanJoin && !this.Data.IsJoined);
  }

  public void OnLockClickHandler()
  {
  }
}
