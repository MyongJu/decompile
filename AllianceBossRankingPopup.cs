﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRankingPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBossRankingPopup : Popup
{
  private Dictionary<string, AllianceBossRankingItemRenderer> itemDict = new Dictionary<string, AllianceBossRankingItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  public GameObject itemContainer;
  public UILabel noRankTipInfo;
  public UILabel individualRank;
  public UIButton viewRewardBtn;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowAllianceBossRankingInfo();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnViewRewardBtnPressed()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceBossRewardsPopup", (Popup.PopupParameter) null);
  }

  private void ShowRankInfoList()
  {
    this.individualRank.text = string.Empty;
    this.ClearData();
    this.UpdateViewRewardBtnState();
    AllianceBossData abd = DBManager.inst.DB_AllianceBoss.GetAllianceBossData();
    AlliancePortalData alliancePortalData = DBManager.inst.DB_AlliancePortal.GetAlliancePortalData();
    if (alliancePortalData == null)
      return;
    MessageHub.inst.GetPortByAction("alliance:loadBossDamageRank").SendRequest(new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) alliancePortalData.WorldId
      },
      {
        (object) "alliance_id",
        (object) alliancePortalData.AllianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      int num = abd == null ? 0 : (int) abd.CTime;
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null && hashtable.Count > 0)
      {
        this.itemContainer.SetActive(true);
        this.individualRank.gameObject.SetActive(true);
        this.noRankTipInfo.gameObject.SetActive(false);
        List<AllianceBossRankingPopup.RankingData> rankingDataList = new List<AllianceBossRankingPopup.RankingData>();
        IDictionaryEnumerator enumerator = hashtable.GetEnumerator();
        while (enumerator.MoveNext())
        {
          long result1 = 0;
          long result2 = 0;
          long.TryParse(enumerator.Key.ToString(), out result1);
          long.TryParse(enumerator.Value.ToString(), out result2);
          rankingDataList.Add(new AllianceBossRankingPopup.RankingData()
          {
            rankTime = num,
            uid = result1,
            score = result2
          });
        }
        this.ShowRankingDetails(rankingDataList);
      }
      else
      {
        this.itemContainer.SetActive(false);
        this.individualRank.gameObject.SetActive(false);
        this.noRankTipInfo.gameObject.SetActive(true);
      }
      this.Reposition();
    }), true);
  }

  private void ShowRankingDetails(List<AllianceBossRankingPopup.RankingData> rankingDataList)
  {
    rankingDataList.Sort(new Comparison<AllianceBossRankingPopup.RankingData>(this.SortByScore));
    for (int index = 0; index < rankingDataList.Count; ++index)
    {
      AllianceBossRankingPopup.RankingData rankingData = rankingDataList[index];
      if (rankingData != null)
      {
        int rank = index + 1;
        AllianceBossRankingItemRenderer itemRenderer = this.CreateItemRenderer(rank, rankingData.uid, rankingData.score, rankingData.rankTime);
        this.itemDict.Add(rank.ToString(), itemRenderer);
        if (rankingData.uid == PlayerData.inst.uid)
          this.individualRank.text = string.Format(Utils.XLAT("event_fallen_knight_my_ranking"), (object) rank);
      }
    }
  }

  private AllianceBossRankingItemRenderer CreateItemRenderer(int rank, long uid, long score, int rankTime)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    AllianceBossRankingItemRenderer component = gameObject.GetComponent<AllianceBossRankingItemRenderer>();
    component.SetData(rank, uid, score, rankTime);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<string, AllianceBossRankingItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private void UpdateViewRewardBtnState()
  {
    this.viewRewardBtn.isEnabled = AllianceBuildUtils.isAllianceBossSummoned();
  }

  private void ShowAllianceBossRankingInfo()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.ShowRankInfoList();
    }), false);
  }

  public int SortByScore(AllianceBossRankingPopup.RankingData a, AllianceBossRankingPopup.RankingData b)
  {
    return b.score.CompareTo(a.score);
  }

  public class RankingData
  {
    public long uid;
    public long score;
    public int rankTime;
  }
}
