﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentMiniDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightTalentMiniDlg : MonoBehaviour
{
  [SerializeField]
  private DragonKnightTalentMiniDlg.Panel panel;

  public void Init()
  {
    int slotCount = ConfigManager.inst.DB_DragonKnightTalentActive.slotCount;
    this.panel.slots.Clear();
    for (int index = 0; index < slotCount; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.scrollView.gameObject, this.panel.miniSlotOrg.gameObject);
      gameObject.name = string.Format("slot_{0}", (object) (100 + index));
      gameObject.transform.localPosition = new Vector3((float) ((this.panel.slotWidget.width + 10) * index), 0.0f, 0.0f);
      gameObject.SetActive(true);
      DragonKnightTalentMiniSlot component = gameObject.GetComponent<DragonKnightTalentMiniSlot>();
      component.onSkillClick = new System.Action<int>(this.OnSlotClick);
      this.panel.slots.Add(component);
    }
    this.panel.scrollView.ResetPosition();
    this.Hide();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.panel.scrollView.ResetPosition();
    this.panel.detailDlg.Close();
    if ((UnityEngine.Object) this.panel.viewPort.sourceCamera == (UnityEngine.Object) null)
      this.panel.viewPort.sourceCamera = UIManager.inst.ui2DCamera;
    int num = 0;
    for (int index = 0; index < ConfigManager.inst.DB_DragonKnightTalentActive.slotCount; ++index)
    {
      DragonKnightTalentActiveInfo skillBySlotIndex = ConfigManager.inst.DB_DragonKnightTalentActive.GetSkillBySlotIndex(index + 1);
      if (DBManager.inst.DB_DragonKnightTalent.Get(skillBySlotIndex.talentId) != null)
        this.panel.slots[num++].Setup(skillBySlotIndex.internalId);
    }
    for (int index = 0; index < ConfigManager.inst.DB_DragonKnightTalentActive.slotCount; ++index)
    {
      DragonKnightTalentActiveInfo skillBySlotIndex = ConfigManager.inst.DB_DragonKnightTalentActive.GetSkillBySlotIndex(index + 1);
      if (DBManager.inst.DB_DragonKnightTalent.Get(skillBySlotIndex.talentId) == null)
        this.panel.slots[num++].Setup(skillBySlotIndex.internalId);
    }
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OnSlotClick(int skillInternalId)
  {
    this.panel.detailDlg.Open(skillInternalId);
  }

  public void Reset()
  {
    GameObject gameObject = this.transform.Find("BottomList/SkillSlotOrg").gameObject;
    this.panel.miniSlotOrg = gameObject.GetComponent<DragonKnightTalentMiniSlot>();
    this.panel.slotWidget = gameObject.GetComponent<UIWidget>();
    this.panel.miniSlotOrg.Reset();
    this.panel.miniSlotOrg.gameObject.SetActive(false);
    this.panel.viewPort = this.transform.Find("BottomList/svCamera").gameObject.GetComponent<UIViewport>();
    this.panel.scrollView = this.transform.Find("BottomList/Slots").gameObject.GetComponent<UIScrollView>();
    this.panel.detailDlg = this.transform.Find("SkillDetail").gameObject.GetComponent<TalentMiniDetailDlg>();
    this.panel.detailDlg.Reset();
  }

  [Serializable]
  protected class Panel
  {
    public List<DragonKnightTalentMiniSlot> slots = new List<DragonKnightTalentMiniSlot>();
    public UIViewport viewPort;
    public DragonKnightTalentMiniSlot miniSlotOrg;
    public UIWidget slotWidget;
    public UIScrollView scrollView;
    public TalentMiniDetailDlg detailDlg;
  }
}
