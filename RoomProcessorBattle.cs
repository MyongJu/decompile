﻿// Decompiled with JetBrains decompiler
// Type: RoomProcessorBattle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class RoomProcessorBattle : IRoomEventProcessor
{
  public bool IsSkipAble()
  {
    return false;
  }

  public void ProcessRoomEvent(Room room)
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightBattleSplashPopup", (Popup.PopupParameter) new DragonKnightBattleSplashPopup.Parameter()
    {
      isBoss = room.ContainsBoss(),
      groupId = room.AllMonster[0].GroupId
    });
    DragonKnightSystem.Instance.Controller.BattleHud.HideBagAndItems();
    DragonKnightSystem.Instance.Controller.BattleHud.ShowSpeedAndQuickBattle();
  }
}
