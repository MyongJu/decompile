﻿// Decompiled with JetBrains decompiler
// Type: SwitchManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class SwitchManager
{
  private static SwitchManager _instance;
  private Dictionary<string, string> _switch;

  public static SwitchManager Instance
  {
    get
    {
      if (SwitchManager._instance == null)
        SwitchManager._instance = new SwitchManager();
      return SwitchManager._instance;
    }
  }

  public void Init(Hashtable source)
  {
    this._switch = new Dictionary<string, string>();
    if (source == null)
      return;
    foreach (string key in (IEnumerable) source.Keys)
      this._switch.Add(key, source[(object) key].ToString());
  }

  public bool GetSwitch(string feathureName)
  {
    bool result = false;
    if (this._switch.ContainsKey(feathureName))
      bool.TryParse(this._switch[feathureName], out result);
    return result;
  }
}
