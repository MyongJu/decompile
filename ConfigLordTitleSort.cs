﻿// Decompiled with JetBrains decompiler
// Type: ConfigLordTitleSort
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigLordTitleSort
{
  private List<LordTitleSortInfo> _lordTitleSortInfoList = new List<LordTitleSortInfo>();
  private Dictionary<string, LordTitleSortInfo> _datas;
  private Dictionary<int, LordTitleSortInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LordTitleSortInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, LordTitleSortInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._lordTitleSortInfoList.Add(enumerator.Current);
    }
  }

  public void Clear()
  {
    if (this._datas != null)
      this._datas.Clear();
    if (this._dicByUniqueId != null)
      this._dicByUniqueId.Clear();
    if (this._lordTitleSortInfoList == null)
      return;
    this._lordTitleSortInfoList.Clear();
  }

  public List<LordTitleSortInfo> GetInfoList()
  {
    this._lordTitleSortInfoList.Sort((Comparison<LordTitleSortInfo>) ((a, b) => a.sortPriority.CompareTo(b.sortPriority)));
    return this._lordTitleSortInfoList;
  }

  public LordTitleSortInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (LordTitleSortInfo) null;
  }

  public LordTitleSortInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (LordTitleSortInfo) null;
  }
}
