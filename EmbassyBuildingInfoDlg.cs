﻿// Decompiled with JetBrains decompiler
// Type: EmbassyBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;

public class EmbassyBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;
  public UITexture texture3;
  public UILabel nameLabel3;
  public UILabel valueLabel3;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    if (this.buildingInfo.Benefit_ID_1.Length > 0)
      this.DrawBenefit(this.buildingInfo.Benefit_Value_1, this.nameLabel1, this.valueLabel1, int.Parse(this.buildingInfo.Benefit_ID_1), "prop_city_reinforcement_capacity_base_value", "calc_city_reinforcement_capacity", (UITexture) null);
    if (this.buildingInfo.Benefit_ID_2.Length > 0)
      this.DrawBenefit2(this.buildingInfo.Benefit_Value_2, this.nameLabel2, this.valueLabel2, int.Parse(this.buildingInfo.Benefit_ID_2), ConfigManager.inst.DB_GameConfig.GetData("alliance_help_time_base").ValueInt, "calc_alliance_help", (UITexture) null);
    if (this.buildingInfo.Benefit_ID_3.Length <= 0)
      return;
    this.DrawBenefit2(this.buildingInfo.Benefit_Value_3, this.nameLabel3, this.valueLabel3, int.Parse(this.buildingInfo.Benefit_ID_3), ConfigManager.inst.DB_GameConfig.GetData("alliance_help_max_times_base").ValueInt, "calc_alliance_help_max", (UITexture) null);
  }

  private void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, int baseValue, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float finalData = (float) ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, caclID);
    string str = (double) finalData <= 0.0 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
  }

  private void DrawBenefit2(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, int baseValue, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float finalData = (float) ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, caclID);
    baseValue += (int) benifitValue;
    string str = (double) baseValue <= 0.0 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) baseValue, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - (float) baseValue), true, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    BuilderFactory.Instance.Release((UIWidget) this.texture2);
    BuilderFactory.Instance.Release((UIWidget) this.texture3);
    base.OnClose(orgParam);
  }
}
