﻿// Decompiled with JetBrains decompiler
// Type: CityDecorationUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;

public class CityDecorationUse : ItemBaseUse
{
  public CityDecorationUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public CityDecorationUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_confirm", true),
      content = ScriptLocalization.Get("item_use_warning", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnUseCityDecroation),
      noCallback = (System.Action) null
    });
  }

  private void OnUseCityDecroation()
  {
    ItemBag.Instance.UseItem(this.Info.internalId, 1, (Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.GotoMyCity();
    }));
  }

  private void GotoMyCity()
  {
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = PlayerData.inst.playerCityData.cityLocation.K,
      x = PlayerData.inst.playerCityData.cityLocation.X,
      y = PlayerData.inst.playerCityData.cityLocation.Y
    });
  }
}
