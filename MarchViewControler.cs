﻿// Decompiled with JetBrains decompiler
// Type: MarchViewControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MarchViewControler : MonoBehaviour
{
  [SerializeField]
  public static MarchViewControler.Data dataParam = new MarchViewControler.Data();
  public const float DEFAULT_FADE_OUT_TIME = 2f;
  public const float DEFAULT_ATTACKT_MIN_TIME = 10f;
  public const float DEFAULT_ATTACK_TIME = 10f;
  public const float DEFAULT_ENCAMP_TIME = 2f;
  public const float DEFAULT_RETURN_TIME = 3f;
  private const float MIN_DISTANCE = 1f;
  public const float CITY_RADIUS = 2f;
  public const float ENCAMP_RADIUS = 1f;
  public const float MONSTER_RADIUS = 1.6f;
  public const float MINI_RADIUS = 0.5f;
  public const float MARCH_DIR_SEPERATE = 0.38f;
  public Transform _modelsParent;
  public Transform _circleBtnParent;
  public BoxCollider _clickCollider;
  [NonSerialized]
  protected StateMachine _stateMachine;
  public bool needRelease;
  public March.March owner;
  public List<long> rallyJoinMarchId;
  private IMarchBaseState _moveSate;
  private IMarchBaseState _attackSate;
  private IMarchBaseState _returningState;
  private IMarchBaseState _doneSate;
  private bool initState;
  private MarchViewControler.StateIndex _stateIndex;
  private MarchViewControler.StateIndex _nextIndex;
  private Dictionary<MarchViewControler.StateIndex, IMarchBaseState> _stateDict;
  private bool rallyAttackDone;
  private bool fortressAttackDone;
  public MarchData marchData;
  [SerializeField]
  private MarchView _marchView;
  [SerializeField]
  private MarchViewControler.Data _data;
  private MarchPathScroll _path;

  public static MarchViewControler.TroopViewType ChangeToTroopViewType(string unitType)
  {
    string key = unitType;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (MarchViewControler.\u003C\u003Ef__switch\u0024map83 == null)
      {
        // ISSUE: reference to a compiler-generated field
        MarchViewControler.\u003C\u003Ef__switch\u0024map83 = new Dictionary<string, int>(4)
        {
          {
            "class_infantry",
            0
          },
          {
            "class_ranged",
            1
          },
          {
            "class_cavalry",
            2
          },
          {
            "class_artyfar",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (MarchViewControler.\u003C\u003Ef__switch\u0024map83.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            return MarchViewControler.TroopViewType.class_infantry;
          case 1:
            return MarchViewControler.TroopViewType.class_ranged;
          case 2:
            return MarchViewControler.TroopViewType.class_cavalry;
          case 3:
            return MarchViewControler.TroopViewType.class_artyfar;
        }
      }
    }
    return MarchViewControler.TroopViewType.class_infantry;
  }

  public void UpdateData(MarchData marchData)
  {
    if (marchData == null || marchData.endTime < NetServerTime.inst.ServerTimestamp)
      return;
    this.InitMarchSate();
    MarchViewControler.Data initData = this.CreateStateData(marchData);
    initData.moveTargetPosition = marchData.endPosition;
    this.CreateMarchView(marchData, (System.Action) (() =>
    {
      MarchViewControler.StateIndex index = this.GetIndex(marchData);
      switch (index)
      {
        case MarchViewControler.StateIndex.NONE:
          return;
        case MarchViewControler.StateIndex.Move:
          if (this.NeedToChangeEndPoint(marchData))
            initData.moveTargetPosition = this.CalcPoint(initData.endPosition, -1, marchData);
          this.CheckAndCreateAttackData(marchData);
          break;
        case MarchViewControler.StateIndex.Attack:
          this.CheckAndCreateAttackData(marchData);
          break;
        case MarchViewControler.StateIndex.Returning:
          if (this.NeedToChangeEndPoint(marchData))
          {
            initData.startPosition = this.CalcPoint(initData.startPosition, 1, marchData);
            break;
          }
          break;
      }
      IMarchBaseState marchBaseState = this._stateDict[index];
      if ((index == MarchViewControler.StateIndex.Returning || index == MarchViewControler.StateIndex.Done) && (marchData.rallyId > 0L && GameEngine.Instance.marchSystem != null) && !GameEngine.Instance.marchSystem.GetRallyAttackMarch(marchData.rallyId))
        this.rallyAttackDone = true;
      if (index != MarchViewControler.StateIndex.Attack)
      {
        marchBaseState.StateData = initData;
        marchBaseState.IsReady = true;
      }
      if (this._stateIndex == MarchViewControler.StateIndex.Attack)
        return;
      this._stateIndex = index;
      this.ChangeState(true);
    }));
  }

  private void CheckAndCreateAttackData(MarchData marchData)
  {
    IMarchBaseState marchBaseState = this._stateDict[MarchViewControler.StateIndex.Attack];
    if (!marchBaseState.IsReady)
    {
      marchBaseState.StateData = this.CreateStateData(marchData);
      marchBaseState.IsReady = true;
    }
    if (marchBaseState.Data.ContainsKey((object) "delta"))
      return;
    float radius = 0.0f;
    marchBaseState.Data.Add((object) "delta", (object) this.GetDeltaPosition(ref radius, marchData));
    marchBaseState.Data.Add((object) "radius", (object) radius);
  }

  private bool NeedToChangeEndPoint(MarchData data)
  {
    MarchData.MarchState state = data.state;
    bool flag = false;
    MarchData.MarchType type = data.type;
    switch (type)
    {
      case MarchData.MarchType.city_attack:
      case MarchData.MarchType.encamp_attack:
      case MarchData.MarchType.monster_attack:
      case MarchData.MarchType.fortress_attack:
      case MarchData.MarchType.rally_attack:
      case MarchData.MarchType.gve_rally_attack:
      case MarchData.MarchType.rab_rally_attack:
        flag = true;
        break;
      default:
        if (type == MarchData.MarchType.attack || type == MarchData.MarchType.world_boss_attack)
          goto case MarchData.MarchType.city_attack;
        else
          break;
    }
    return flag;
  }

  private Vector3 CalcPoint(Vector3 point, int dir, MarchData marchData)
  {
    float radius = 0.0f;
    Vector3 deltaPosition = this.GetDeltaPosition(ref radius, marchData);
    return point + (float) dir * deltaPosition;
  }

  public void SetRallyAttackDone()
  {
    this.rallyAttackDone = true;
  }

  public void SetFortressAttackDone()
  {
    this.fortressAttackDone = true;
  }

  public bool CanDone
  {
    get
    {
      return this._stateIndex != MarchViewControler.StateIndex.Attack;
    }
  }

  public void RecallHandler()
  {
    this._nextIndex = MarchViewControler.StateIndex.Returning;
    this.NextState();
  }

  public bool HasNextState()
  {
    if (this._stateIndex == MarchViewControler.StateIndex.Done)
      return false;
    return this._stateDict[this._stateIndex + 1].IsReady;
  }

  public void NextState()
  {
    if (this._stateIndex == MarchViewControler.StateIndex.Done)
      return;
    if (this._nextIndex != MarchViewControler.StateIndex.NONE)
    {
      this._stateIndex = this._nextIndex;
      this._nextIndex = MarchViewControler.StateIndex.NONE;
      this.ChangeState(false);
    }
    else
    {
      ++this._stateIndex;
      this.ChangeState(false);
    }
  }

  private void ChangeState(bool isInit = false)
  {
    if (this._stateIndex == MarchViewControler.StateIndex.NONE)
      return;
    IMarchBaseState marchBaseState = this._stateDict[this._stateIndex];
    marchBaseState.IsInitState = isInit;
    if (!marchBaseState.IsReady)
      return;
    this._stateMachine.SetState((IState) marchBaseState, (Hashtable) null);
  }

  public float SetUpMarchView(MarchViewControler.Data data, System.Action callback)
  {
    if ((UnityEngine.Object) this._marchView != (UnityEngine.Object) null)
      return this._marchView.Setup(data, true, callback);
    return 0.0f;
  }

  private MarchViewControler.Data CreateStateData(MarchData inMarchData)
  {
    MarchViewControler.Data data = new MarchViewControler.Data();
    data.targetLocation = inMarchData.targetLocation;
    data.marchId = inMarchData.marchId;
    data.marchState = inMarchData.state;
    data.troops = this.CloneTroopsInfo(inMarchData);
    data.marchType = inMarchData.type;
    data.marchData = inMarchData;
    data.ownerUid = inMarchData.ownerUid;
    data.ownerAllianceId = inMarchData.ownerAllianceId;
    data.rallyId = inMarchData.rallyId;
    data.startPosition = this.marchData.startPosition;
    data.endPosition = this.marchData.endPosition;
    data.isDragonIn = this.marchData.dragon.isDragonIn;
    data.isFortressAttack = this.marchData.IsAttackFortress;
    data.isCanAttackFortress = this.marchData.IsCanAttackFortress;
    data.isInFortress = this.marchData.IsInFortress;
    data.startTime = NetServerTime.inst.UpdateTime + (double) (this.marchData.startTime - NetServerTime.inst.ServerTimestamp);
    data.endTime = NetServerTime.inst.UpdateTime + (double) (this.marchData.endTime - NetServerTime.inst.ServerTimestamp);
    --data.endTime;
    return data;
  }

  private DB.TroopsInfo CloneTroopsInfo(MarchData inMarchData)
  {
    DB.TroopsInfo troopsInfo = new DB.TroopsInfo();
    troopsInfo.totalCount = inMarchData.troopsInfo.totalCount;
    troopsInfo.troops = new Dictionary<string, Unit>();
    Dictionary<string, Unit>.Enumerator enumerator = inMarchData.troopsInfo.troops.GetEnumerator();
    while (enumerator.MoveNext())
      troopsInfo.troops.Add(enumerator.Current.Key, enumerator.Current.Value);
    return troopsInfo;
  }

  private MarchViewControler.StateIndex GetIndex(MarchData data)
  {
    switch (data.state)
    {
      case MarchData.MarchState.marching:
      case MarchData.MarchState.rallying:
        return MarchViewControler.StateIndex.Move;
      case MarchData.MarchState.returning:
      case MarchData.MarchState.scoutReturning:
        return MarchViewControler.StateIndex.Returning;
      case MarchData.MarchState.done:
        return MarchViewControler.StateIndex.Done;
      default:
        return MarchViewControler.StateIndex.Attack;
    }
  }

  private float GetEndTime(MarchData.MarchType type, int serverEndTime, bool isReturn = false)
  {
    return (float) serverEndTime;
  }

  private void InitMarchSate()
  {
    if (this.initState)
      return;
    this.initState = true;
    this._stateMachine = new StateMachine();
    this._moveSate = (IMarchBaseState) new MarchMoveState();
    this._moveSate.Controler = this;
    this._stateMachine.AddState((IState) this._moveSate);
    this._attackSate = (IMarchBaseState) new MarchActionState();
    this._attackSate.Controler = this;
    this._stateMachine.AddState((IState) this._attackSate);
    this._returningState = (IMarchBaseState) new MarchReturningState();
    this._returningState.Controler = this;
    this._stateMachine.AddState((IState) this._returningState);
    this._doneSate = (IMarchBaseState) new MarchDoneState();
    this._doneSate.Controler = this;
    this._stateMachine.AddState((IState) this._doneSate);
    this._stateDict = new Dictionary<MarchViewControler.StateIndex, IMarchBaseState>();
    this._stateDict.Add(MarchViewControler.StateIndex.Move, this._moveSate);
    this._stateDict.Add(MarchViewControler.StateIndex.Attack, this._attackSate);
    this._stateDict.Add(MarchViewControler.StateIndex.Returning, this._returningState);
    this._stateDict.Add(MarchViewControler.StateIndex.Done, this._doneSate);
    this.AddEventHandler();
    this.rallyAttackDone = false;
    this.fortressAttackDone = false;
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
  }

  private void RemoveEventHanlder()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
  }

  private void Process(double time)
  {
    if (this._stateMachine == null || this._stateMachine.CurrState == null)
      return;
    this._stateMachine.Process();
  }

  private void CreateMarchView(MarchData data, System.Action callback)
  {
    if ((this.marchData == null || this.marchData.type != data.type) && (UnityEngine.Object) this._marchView != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._marchView.gameObject);
      this._marchView = (MarchView) null;
    }
    if ((UnityEngine.Object) this._marchView == (UnityEngine.Object) null)
    {
      GameObject gameObject = new GameObject();
      gameObject.transform.parent = this._modelsParent;
      gameObject.transform.localPosition = Vector3.zero;
      MarchData.MarchType type = data.type;
      switch (type)
      {
        case MarchData.MarchType.trade:
label_8:
          this._marchView = (MarchView) gameObject.AddComponent<TradeView>();
          gameObject.name = "trade";
          break;
        case MarchData.MarchType.scout:
label_6:
          this._marchView = (MarchView) gameObject.AddComponent<ScoutView>();
          gameObject.name = "scout";
          break;
        case MarchData.MarchType.gather:
label_7:
          this._marchView = (MarchView) gameObject.AddComponent<GatherView>();
          gameObject.name = "gather";
          break;
        default:
          switch (type - 25)
          {
            case MarchData.MarchType.none:
            case MarchData.MarchType.rally:
              goto label_8;
            case MarchData.MarchType.gve_rally:
              goto label_6;
            default:
              if (type != MarchData.MarchType.gather_attack)
              {
                this._marchView = (MarchView) gameObject.AddComponent<ArmyView>();
                gameObject.name = "army";
                break;
              }
              goto label_7;
          }
      }
    }
    this.marchData = data;
    if ((UnityEngine.Object) this._marchView != (UnityEngine.Object) null)
    {
      this._marchView.Init((System.Action) (() =>
      {
        if (callback == null)
          return;
        callback();
      }));
    }
    else
    {
      if (callback == null)
        return;
      callback();
    }
  }

  public void AttackAction()
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null) || !this._stateMachine.CurrState.Data.ContainsKey((object) "delta"))
      return;
    this._marchView.Action(0, (Vector3) this._stateMachine.CurrState.Data[(object) "delta"], (float) this._stateMachine.CurrState.Data[(object) "radius"]);
  }

  private Vector3 GetDeltaPosition(ref float radius, MarchData data = null)
  {
    if (data == null)
      data = this.marchData;
    Vector3 vector3 = data.endPosition - data.startPosition;
    vector3.Normalize();
    Vector3 zero = Vector3.zero;
    radius = 0.0f;
    MarchData.MarchType type = data.type;
    switch (type)
    {
      case MarchData.MarchType.city_attack:
      case MarchData.MarchType.fortress_attack:
      case MarchData.MarchType.rally_attack:
        radius = 2f;
        break;
      case MarchData.MarchType.encamp_attack:
      case MarchData.MarchType.gve_rally_attack:
      case MarchData.MarchType.rab_rally_attack:
        radius = 1f;
        break;
      case MarchData.MarchType.monster_attack:
        radius = 1.6f;
        break;
      default:
        if (type != MarchData.MarchType.attack)
        {
          if (type != MarchData.MarchType.world_boss_attack)
          {
            radius = 0.0f;
            break;
          }
          goto case MarchData.MarchType.monster_attack;
        }
        else
          goto case MarchData.MarchType.city_attack;
    }
    return vector3 * radius;
  }

  public void MoveAction()
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null))
      return;
    this._marchView.Move();
  }

  public bool FortressAttackDone()
  {
    return this.fortressAttackDone;
  }

  public bool RallyIsDone()
  {
    return this.rallyAttackDone;
  }

  public bool HasReturingStateData()
  {
    return this._returningState.IsReady;
  }

  public bool HasFinishStateData()
  {
    return this._doneSate.IsReady;
  }

  public void SetFinishState()
  {
    this._nextIndex = MarchViewControler.StateIndex.Done;
    this.NextState();
  }

  private float GetMarchSpeed()
  {
    float num = 0.0f;
    if (this._stateIndex == MarchViewControler.StateIndex.Move)
    {
      IMarchBaseState currState = this._stateMachine.CurrState as IMarchBaseState;
      num = (currState.StateData.moveTargetPosition - currState.StateData.startPosition).magnitude / (float) (currState.StateData.endTime - currState.StateData.startTime);
    }
    return num;
  }

  public void ReloadControler()
  {
    this.InitMarchSate();
    this.UpdateData(this.marchData);
  }

  public bool isShow { get; private set; }

  public bool isHide
  {
    get
    {
      return !this.isShow;
    }
  }

  public void Init()
  {
    this.isShow = false;
  }

  public void OnTroopsViewClick()
  {
    if (this._stateMachine == null)
      return;
    UIManager.inst.OpenKingdomArmyCircle((this._stateMachine.CurrState as IMarchBaseState).StateData.marchData.marchId, this._circleBtnParent);
  }

  public void ShowAt(Vector3 pos)
  {
    if ((UnityEngine.Object) this._marchView != (UnityEngine.Object) null)
    {
      if (this.isShow)
        this._marchView.Show();
      else
        this._marchView.FadeIn(pos, this.GetMarchSpeed(), true);
    }
    this.SetPosition(pos);
  }

  private void SetPosition(Vector3 pos)
  {
    this.transform.position = pos;
  }

  private void FadeIn(bool isDisplayByLine = true)
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null) || !this.isHide)
      return;
    this._marchView.FadeIn(Vector3.zero, 0.0f, isDisplayByLine);
    this.isShow = true;
  }

  public void FadeOut(bool isDisplayByline = true)
  {
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null) || !this.isShow)
      return;
    this._marchView.FadeOut(0.0f, isDisplayByline);
    this.isShow = false;
  }

  public void Rotate(Vector3 dirV3)
  {
    IMarchBaseState currState = this._stateMachine.CurrState as IMarchBaseState;
    this._circleBtnParent.localPosition = -currState.StateData.dirV3.normalized * (currState.StateData.troopLength / 2f);
    this._clickCollider.transform.position = currState.StateData.startPosition;
    this._clickCollider.transform.LookAt(currState.StateData.endPosition);
    this._clickCollider.transform.localPosition = this._circleBtnParent.localPosition;
    this._clickCollider.size = new Vector3(1f, 1.32f, (double) currState.StateData.troopLength >= 1.32000005245209 ? currState.StateData.troopLength : 1.32f);
    if (!((UnityEngine.Object) this._marchView != (UnityEngine.Object) null))
      return;
    this._marchView.Rotate(dirV3);
  }

  public void HideClickCollider()
  {
    this._clickCollider.gameObject.SetActive(false);
    if (!((UnityEngine.Object) this._circleBtnParent.GetComponentInChildren<KingdomTouchCircle>() != (UnityEngine.Object) null))
      return;
    UIManager.inst.CloseKingdomTouchCircle();
  }

  public void ShowClickCollider()
  {
    this._clickCollider.gameObject.SetActive(true);
  }

  public void ReleaseTroops()
  {
    this.DestoryPath();
    this.RemoveEventHanlder();
    if (PVPSystem.Instance.IsAvailable && PVPSystem.Instance.troopViewManager != null)
      PVPSystem.Instance.troopViewManager.ReleaseTroopView(this);
    this.marchData = (MarchData) null;
    this._stateDict.Clear();
    if (this._stateMachine != null)
    {
      this.initState = false;
      this._stateMachine.Dispose();
      this._stateMachine = (StateMachine) null;
    }
    this._stateIndex = MarchViewControler.StateIndex.NONE;
    this._nextIndex = MarchViewControler.StateIndex.NONE;
    if ((UnityEngine.Object) this._marchView != (UnityEngine.Object) null && (UnityEngine.Object) this._marchView.gameObject != (UnityEngine.Object) null)
    {
      if ((bool) ((UnityEngine.Object) this._circleBtnParent.Find("KingdomTouchCircle(Clone)")))
        UIManager.inst.CloseKingdomTouchCircle();
      this._marchView.Dispose();
      UnityEngine.Object.Destroy((UnityEngine.Object) this._marchView.gameObject);
      this._marchView = (MarchView) null;
    }
    this.owner = (March.March) null;
    this._data.Clear();
  }

  public void CreatePath()
  {
    this.DestoryPath();
    IMarchBaseState currState = this._stateMachine.CurrState as IMarchBaseState;
    MarchData marchData = currState.StateData.marchData;
    if (marchData == null)
      return;
    GameObject gameObject = marchData.ownerUid == PlayerData.inst.uid || marchData.ownerAllianceId != 0L && marchData.ownerAllianceId == PlayerData.inst.allianceId ? MapUtils.Instantiate("Prefab/Kingdom/MarchPathPlayer") : (marchData.targetUid == PlayerData.inst.uid || marchData.targetAllianceId != 0L && marchData.targetAllianceId == PlayerData.inst.allianceId ? (!SettingManager.Instance.IsDaltonOff ? MapUtils.Instantiate("Prefab/Kingdom/PathEnemyDalton") : MapUtils.Instantiate("Prefab/Kingdom/MarchPathEnemy")) : MapUtils.Instantiate("Prefab/Kingdom/MarchPathAlly"));
    Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
    componentInChildren.sortingOrder = 29998;
    componentInChildren.sortingLayerName = "March";
    MarchPathScroll component = gameObject.GetComponent<MarchPathScroll>();
    component.SetPath(currState.StateData.startPosition, currState.StateData.endPosition, PVPSystem.Instance.Map.transform);
    this._path = component;
    if (!((UnityEngine.Object) this._clickCollider != (UnityEngine.Object) null))
      return;
    this._clickCollider.enabled = true;
  }

  public void DestoryPath()
  {
    if ((UnityEngine.Object) this._path != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._path.gameObject);
      this._path = (MarchPathScroll) null;
    }
    if (!((UnityEngine.Object) this._clickCollider != (UnityEngine.Object) null))
      return;
    this._clickCollider.enabled = false;
  }

  private bool IsAttackType(MarchData.MarchType type)
  {
    if (!MarchData.IsNormalAttack(type) && type != MarchData.MarchType.rally && (type != MarchData.MarchType.rally_attack && type != MarchData.MarchType.gve_rally_attack) && (type != MarchData.MarchType.rab_rally_attack && type != MarchData.MarchType.wonder_attack && type != MarchData.MarchType.world_boss_attack))
      return type == MarchData.MarchType.monster_attack;
    return true;
  }

  public static Vector2 CalcVector3(Vector3 v3)
  {
    return new Vector2((double) v3.normalized.x >= -0.379999995231628 ? ((double) v3.normalized.x >= 0.379999995231628 ? 1f : 0.0f) : -1f, (double) v3.normalized.y >= -0.379999995231628 ? ((double) v3.normalized.y >= 0.379999995231628 ? 1f : 0.0f) : -1f);
  }

  public static void CalcVector3(Vector3 v3, ref Vector2 v2)
  {
    v2.x = (double) v3.normalized.x >= -0.379999995231628 ? ((double) v3.normalized.x >= 0.379999995231628 ? 1f : 0.0f) : -1f;
    v2.y = (double) v3.normalized.y >= -0.379999995231628 ? ((double) v3.normalized.y >= 0.379999995231628 ? 1f : 0.0f) : -1f;
  }

  public enum TroopViewType
  {
    class_infantry,
    class_ranged,
    class_dragon,
    class_cavalry,
    class_artyfar,
    class_darkKnight_1,
    class_darkKnight_2,
    class_darkKnight_3,
    count,
  }

  public enum StateIndex
  {
    NONE,
    Move,
    Attack,
    Returning,
    Done,
  }

  public enum TroopViewState
  {
    none,
    invalid,
    hide,
    moveTo,
    attackTo,
    attacking,
    attackEnd,
  }

  [Serializable]
  public struct Data
  {
    public long marchId;
    public long rallyId;
    public bool isInit;
    public MarchData marchData;
    public MarchData.MarchType marchType;
    public DB.TroopsInfo troops;
    public bool isDragonIn;
    public MarchData.MarchState marchState;
    public long ownerUid;
    public long ownerAllianceId;
    public Coordinate targetLocation;
    public Vector3 moveTargetPosition;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public bool isFortressAttack;
    public bool isCanAttackFortress;
    public bool isInFortress;
    public double startTime;
    public double endTime;
    public Vector3 nextStartPosition;
    public Vector3 nextEndPosition;
    public float nextStartTime;
    public float nextEndTime;
    public float nextTimeDuration;
    public float troopLength;

    public Vector3 dirV3
    {
      get
      {
        return this.endPosition - this.startPosition;
      }
    }

    public Vector2 dirV2
    {
      get
      {
        return MarchViewControler.CalcVector3(this.dirV3);
      }
    }

    public float timeDuration
    {
      get
      {
        return (float) (this.endTime - this.startTime);
      }
    }

    public float moveSpeed
    {
      get
      {
        if ((double) this.timeDuration > 0.0)
          return this.dirV3.magnitude / this.timeDuration;
        return 0.0f;
      }
    }

    public void Clear()
    {
      this.marchData = (MarchData) null;
      this.marchType = MarchData.MarchType.none;
    }

    public string Show()
    {
      return string.Empty + "March Type" + this.marchType.ToString() + string.Empty + "Troops :" + this.troops.ShowInfo() + "\n" + "\n" + "\tStart Time: " + this.startTime.ToString() + "\n" + "\tEnd   Time: " + this.endTime.ToString() + "\n" + "\tStart Pos :" + this.startPosition.ToString() + "\n" + "\tEnd   Pos :" + this.endPosition.ToString() + "\n" + "\n" + "\n Next " + "\tStart Time: " + this.nextStartTime.ToString() + "\n" + "\tEnd   Time: " + this.nextEndTime.ToString() + "\n" + "\tStart Pos :" + this.nextStartPosition.ToString() + "\n" + "\tEnd   Pos :" + this.nextEndPosition.ToString() + "\n";
    }
  }
}
