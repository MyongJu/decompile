﻿// Decompiled with JetBrains decompiler
// Type: PopupManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
  private Stack<PopupManager.PopupRecord> m_PopupStack = new Stack<PopupManager.PopupRecord>();
  public const string BASE_PATH = "Prefab/UI/Popups/";
  public const int BASE_DEPTH = 50;
  public Camera m_Camera;
  private static PopupManager m_Instance;

  public static PopupManager Instance
  {
    get
    {
      if ((UnityEngine.Object) PopupManager.m_Instance == (UnityEngine.Object) null)
        Debug.LogError((object) "PopupManager not yet created.");
      return PopupManager.m_Instance;
    }
  }

  public static bool IsAvailable
  {
    get
    {
      return (UnityEngine.Object) PopupManager.m_Instance != (UnityEngine.Object) null;
    }
  }

  public void Dispose()
  {
    PopupManager.m_Instance = (PopupManager) null;
  }

  public T Open<T>(string assetName) where T : PopupBase
  {
    PopupBase instance = PopupManager.CreateInstance("Prefab/UI/Popups/" + assetName);
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
    {
      T obj = instance as T;
      if ((UnityEngine.Object) obj != (UnityEngine.Object) null)
      {
        UIPanel[] componentsInChildren = obj.gameObject.GetComponentsInChildren<UIPanel>(true);
        int length = componentsInChildren.Length;
        int num1 = this.m_PopupStack.Count <= 0 ? 50 : this.m_PopupStack.Peek().m_MaxDepth + 1;
        int num2 = int.MinValue;
        for (int index = 0; index < length; ++index)
        {
          componentsInChildren[index].depth += num1;
          num2 = Math.Max(num2, componentsInChildren[index].depth);
        }
        obj.gameObject.transform.parent = this.m_Camera.transform;
        obj.gameObject.transform.localPosition = Vector3.zero;
        obj.gameObject.transform.localRotation = Quaternion.identity;
        obj.gameObject.transform.localScale = Vector3.one;
        obj.gameObject.SetActive(true);
        this.m_PopupStack.Push(new PopupManager.PopupRecord((PopupBase) obj, num2));
        obj.OnShowPopup();
        return obj;
      }
      UnityEngine.Object.Destroy((UnityEngine.Object) instance.gameObject);
    }
    return (T) null;
  }

  public void Close(PopupBase popup)
  {
    if (!((UnityEngine.Object) this.m_PopupStack.Peek().m_Popup == (UnityEngine.Object) popup))
      return;
    popup.OnClosePopup();
    this.m_PopupStack.Pop();
    UnityEngine.Object.Destroy((UnityEngine.Object) popup.gameObject);
  }

  private void Awake()
  {
    if ((UnityEngine.Object) PopupManager.m_Instance != (UnityEngine.Object) null && (UnityEngine.Object) PopupManager.m_Instance != (UnityEngine.Object) this)
    {
      Debug.LogError((object) "There is should be only one PopupManager instance.");
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }
    else
      PopupManager.m_Instance = this;
  }

  private static PopupBase CreateInstance(string assetName)
  {
    GameObject original = AssetManager.Instance.HandyLoad(assetName, (System.Type) null) as GameObject;
    if ((UnityEngine.Object) original != (UnityEngine.Object) null)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(original);
      if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
      {
        PopupBase component = gameObject.GetComponent<PopupBase>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          return component;
        UnityEngine.Object.Destroy((UnityEngine.Object) gameObject);
      }
    }
    return (PopupBase) null;
  }

  private class PopupRecord
  {
    public PopupBase m_Popup;
    public int m_MaxDepth;

    public PopupRecord(PopupBase popup, int maxDepth)
    {
      this.m_Popup = popup;
      this.m_MaxDepth = maxDepth;
    }
  }
}
