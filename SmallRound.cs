﻿// Decompiled with JetBrains decompiler
// Type: SmallRound
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRound : IRound
{
  private SmallRound.RoundType _type = SmallRound.RoundType.Attack;
  private float _time = 1f;
  private long _skillId = -1;
  private Dictionary<string, object> _datas = new Dictionary<string, object>();
  private RoundPlayer _trigger;
  private List<RoundPlayer> _targets;
  private List<RoundResult> _results;
  private bool _isFinish;
  private bool _isRunning;
  private float _timeStamp;
  private bool _flag;
  private bool _underAttackFlag;
  private SmallRound.State _state;
  private bool _isNeedToValidate;
  private long _mainTargetId;
  private int _roundId;
  private int _finishFlag;

  public SmallRound()
  {
    this._state = SmallRound.State.Init;
  }

  public void SetData(string key, object data)
  {
    if (!this._datas.ContainsKey(key))
      this._datas.Add(key, data);
    else
      this._datas[key] = data;
  }

  public T GetData<T>(string key)
  {
    if (this._datas.ContainsKey(key))
      return (T) this._datas[key];
    return default (T);
  }

  public long MainTargetId
  {
    get
    {
      return this._mainTargetId;
    }
    set
    {
      this._mainTargetId = value;
    }
  }

  public int RoundID
  {
    get
    {
      return this._roundId;
    }
    set
    {
      this._roundId = value;
    }
  }

  public bool IsNeedToValidate
  {
    get
    {
      return this._isNeedToValidate;
    }
    set
    {
      this._isNeedToValidate = value;
    }
  }

  public Hashtable GetValidateData()
  {
    if (!this._isNeedToValidate)
      return (Hashtable) null;
    string str1 = string.Empty;
    string underAttackId = this.GetUnderAttackId();
    if (this._skillId > 0L)
    {
      str1 = this._skillId.ToString();
      if (!string.IsNullOrEmpty(underAttackId))
        str1 = str1 + "_" + underAttackId;
    }
    string str2 = this.MainTargetId.ToString();
    Hashtable hashtable = new Hashtable();
    if (!string.IsNullOrEmpty(str1))
      hashtable.Add((object) "skill", (object) str1);
    hashtable.Add((object) "target", (object) str2);
    return hashtable;
  }

  private string GetUnderAttackId()
  {
    string empty = string.Empty;
    bool flag = true;
    if (this.SkillId > 0L)
      flag = !ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo((int) this.SkillId).IsSplash();
    if (flag)
    {
      for (int index = 0; index < this._results.Count; ++index)
      {
        if (this._trigger.PlayerId != this._results[index].Target.PlayerId)
        {
          empty += (string) (object) this._results[index].Target.PlayerId;
          if (index + 1 < this._results.Count)
            empty += ",";
        }
      }
    }
    else
      empty = this._mainTargetId.ToString();
    return empty;
  }

  public long SkillId
  {
    get
    {
      return this._skillId;
    }
    set
    {
      this._skillId = value;
    }
  }

  public RoundPlayer Trigger
  {
    get
    {
      return this._trigger;
    }
    set
    {
      this._trigger = value;
    }
  }

  public List<RoundPlayer> Targets
  {
    get
    {
      return this._targets;
    }
    set
    {
      this._targets = value;
    }
  }

  public List<RoundResult> Results
  {
    get
    {
      return this._results;
    }
    set
    {
      this._results = value;
    }
  }

  public void AddTarget(RoundPlayer unit)
  {
    if (this._targets == null)
      this._targets = new List<RoundPlayer>();
    if (this._targets.Contains(unit))
      return;
    this._targets.Add(unit);
  }

  public void AddResult(RoundResult result)
  {
    if (this._results == null)
      this._results = new List<RoundResult>();
    if (this._results.Contains(result))
      return;
    this._results.Add(result);
  }

  public SmallRound.RoundType Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public bool IsFinish
  {
    get
    {
      return this._isFinish;
    }
    set
    {
      this._isFinish = value;
    }
  }

  public bool IsRunning
  {
    get
    {
      return this._isRunning;
    }
    set
    {
      this._isRunning = value;
    }
  }

  public void Process()
  {
    if (!this._flag || (double) Time.time - (double) this._timeStamp < (double) this._time)
      return;
    this._flag = false;
    this.Finish();
  }

  public void MarkFinish()
  {
    if (this._targets.Count == 0)
    {
      this.Finish();
    }
    else
    {
      ++this._finishFlag;
      if (this._finishFlag < this._targets.Count)
        return;
      this.Finish();
    }
  }

  public void Finish()
  {
    if (this._state != SmallRound.State.UnderAttack)
      return;
    this.IsFinish = true;
    this._state = SmallRound.State.Finish;
    BattleManager.Instance.NextRound(this.GUID);
  }

  public void Play()
  {
    this.IsRunning = true;
    if (this._trigger == null)
      this.Finish();
    else if (this.SkillId > 0L)
      this.Spell();
    else
      this.NormalAttack();
  }

  public void Spell()
  {
    this._state = SmallRound.State.Spell;
    this._trigger.Receive((DragonKnightPacket) new DragonKnightPacketSpell((long) this.GUID, this._trigger.PlayerId)
    {
      SkillId = this.SkillId,
      IsHeal = this.GetData<bool>("isHeal"),
      TargetIsEnemy = this.GetData<bool>("targetIsEnemy")
    });
  }

  public void NormalAttack()
  {
    this._state = SmallRound.State.Attacking;
    this._trigger.Receive((DragonKnightPacket) new DragonKnightPacketAttack((long) this.GUID, this._trigger.PlayerId));
  }

  public void UnderAttack()
  {
    if (this._targets == null || this._targets.Count == 0)
    {
      this.Finish();
    }
    else
    {
      if (this._underAttackFlag)
        return;
      this._state = SmallRound.State.UnderAttack;
      this._underAttackFlag = false;
      for (int index = 0; index < this._targets.Count; ++index)
      {
        RoundResult result = this.GetResult(this._targets[index].PlayerId);
        this._targets[index].Receive(this.GetDragonKnightPacket(this._targets[index].PlayerId, result));
      }
      RoundResult result1 = this.GetResult(this.Trigger.PlayerId);
      if (result1 != null && result1.Type == RoundResult.ResultType.Rebound)
        this.Trigger.Receive((DragonKnightPacket) new DragonKnightPacketRebound((long) this.RoundID, this.Trigger.PlayerId, result1));
      this._timeStamp = Time.time;
    }
  }

  private DragonKnightPacket GetDragonKnightPacket(long playerId, RoundResult result)
  {
    return this.SkillId <= 0L || this.GetData<bool>("targetIsEnemy") ? (DragonKnightPacket) new DragonKnightPacketUnderAttack((long) this.GUID, playerId, result) : (DragonKnightPacket) new DragonKnightPacketEnhance((long) this.GUID, playerId, result);
  }

  private RoundResult GetResult(long playerId)
  {
    for (int index = 0; index < this.Results.Count; ++index)
    {
      if (this.Results[index].Target.PlayerId == playerId)
        return this.Results[index];
    }
    return (RoundResult) null;
  }

  public void Destroy()
  {
    this.Clear();
  }

  private void Reset()
  {
    this._trigger = (RoundPlayer) null;
    this._targets = (List<RoundPlayer>) null;
    this._results = (List<RoundResult>) null;
    this._isFinish = false;
    this._isRunning = false;
    this._finishFlag = 0;
    this._time = 1f;
    this._timeStamp = 0.0f;
    this._flag = false;
    this._skillId = -1L;
    this._type = SmallRound.RoundType.None;
  }

  public int GUID { get; set; }

  private void Clear()
  {
    this.Reset();
    this.OnClear();
  }

  public override string ToString()
  {
    return string.Format("[BaseRound: SkillId={0}, Trigger={1}, Targets={2}, Results={3}, Type={4}, IsFinish={5}, IsRunning={6}]", (object) this.SkillId, (object) this.Trigger, (object) this.Targets, (object) this.Results, (object) this.Type, (object) this.IsFinish, (object) this.IsRunning);
  }

  protected virtual void OnClear()
  {
  }

  private enum State
  {
    None,
    Init,
    Attacking,
    Spell,
    Hit,
    UnderAttack,
    Finish,
  }

  public enum RoundType
  {
    None,
    Attack,
    Skill,
  }
}
