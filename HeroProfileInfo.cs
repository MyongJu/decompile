﻿// Decompiled with JetBrains decompiler
// Type: HeroProfileInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class HeroProfileInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "gender")]
  public string gender;
  [Config(Name = "initial_name")]
  public string initialName;
  [Config(Name = "image")]
  public int imageIndex;
  [Config(Name = "cost_id")]
  public int costId;
  [Config(Name = "cost_value")]
  public int costValue;
  [Config(Name = "is_new")]
  public int isNew;
  [Config(Name = "is_locked")]
  public int isLocked;

  public string Image
  {
    get
    {
      return "player_portrait_" + this.imageIndex.ToString();
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/Hero/" + this.Image;
    }
  }

  public bool IsNew
  {
    get
    {
      return this.isNew == 1;
    }
  }

  public bool IsLocked
  {
    get
    {
      return this.isLocked == 1;
    }
  }
}
