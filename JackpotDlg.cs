﻿// Decompiled with JetBrains decompiler
// Type: JackpotDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UI;
using UnityEngine;

public class JackpotDlg : UI.Dialog
{
  private Dictionary<int, JackpotRankSlotRenderer> itemDict = new Dictionary<int, JackpotRankSlotRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  private int stopSlotIndex = -1;
  private float stopTimeDuration = 1.05f;
  private int[] stopSlotFlag = new int[5];
  private bool rewardCollected = true;
  public UILabel jackpot;
  public UILabel noWinHistory;
  public UILabel costGoldCount;
  public UIButton propsBetButton;
  public UIButton coinsBetButton;
  public UITexture[] itemSlot;
  public GameObject[] itemStopEffect;
  public GameObject[] itemSingleMultiEffect;
  public GameObject[] itemFiveMultiEffect;
  public GameObject crystalBallEffect;
  public GameObject bigWinCoinEffect;
  public GameObject bigWinFireworkEffect;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private int curWinType;
  private int curWinRewardId;
  private Color costGoldTextColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.costGoldTextColor = this.costGoldCount.color;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearStatus();
    this.ClearData();
    this.ClearSlotData();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    if (!this.rewardCollected)
      this.ShowWinCollectEffect(this.curWinType, this.curWinRewardId);
    JackpotPayload.Instance.UpdateDataByCache();
  }

  public void OnPropsBetBtnPressed()
  {
    this.GambleJackpot("other");
  }

  public void OnCoinsBetBtnPressed()
  {
    if (PlayerData.inst.userData.currency.gold < (long) ConfigManager.inst.DB_CasinoJackpot.CostCoins2Play)
      UIManager.inst.OpenPopup("Vault/VaultConfirmationPopup", (Popup.PopupParameter) new VaultConfirmationPopup.Parameter()
      {
        confirmType = VaultConfirmationPopup.ConfirmType.TO_EARN
      });
    else
      this.GambleJackpot("gold");
  }

  private void GambleJackpot(string betType)
  {
    CasinoFunHelper.Instance.GambleJackpot(betType, (System.Action<int, int>) ((winType, winRewardId) =>
    {
      this.curWinType = winType;
      this.curWinRewardId = winRewardId;
      this.PlayJackpot(this.curWinType, this.curWinRewardId);
    }));
  }

  private void ClearData()
  {
    using (Dictionary<int, JackpotRankSlotRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, JackpotRankSlotRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void ClearSlotData()
  {
    for (int index = 0; index < this.itemSlot.Length; ++index)
      BuilderFactory.Instance.Release((UIWidget) this.itemSlot[index]);
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private JackpotRankSlotRenderer CreateItemRenderer(JackpotRankInfo jri, bool showDividingLine)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    JackpotRankSlotRenderer component = gameObject.GetComponent<JackpotRankSlotRenderer>();
    component.SetData(jri, showDividingLine);
    return component;
  }

  private void UpdateUI()
  {
    this.ClearData();
    this.UpdataNPC();
    this.CheckBetButtonStatus(true);
    this.ShowCurrentJackpot(true, 0.0f);
    this.ShowRankingContent();
    this.ShowJackpotDefaultContent();
  }

  private void UpdataNPC()
  {
    Transform transform = this.transform.Find("Content/TableZone/npc");
    if (!((UnityEngine.Object) transform != (UnityEngine.Object) null))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    Utils.SetPortrait(component, "Texture/STATIC_TEXTURE/tutorial_npc_03");
  }

  private void ShowRankingContent()
  {
    List<JackpotRankInfo> jackpotRankInfoList = JackpotPayload.Instance.JackpotRankInfoList;
    if (jackpotRankInfoList != null && jackpotRankInfoList.Count == 0)
      jackpotRankInfoList = JackpotPayload.Instance.LastJackpotRankInfoList;
    if (jackpotRankInfoList != null)
    {
      for (int key = 0; key < jackpotRankInfoList.Count; ++key)
      {
        JackpotRankSlotRenderer itemRenderer = this.CreateItemRenderer(jackpotRankInfoList[key], key != jackpotRankInfoList.Count - 1);
        this.itemDict.Add(key, itemRenderer);
      }
    }
    NGUITools.SetActive(this.noWinHistory.gameObject, jackpotRankInfoList == null || jackpotRankInfoList != null && jackpotRankInfoList.Count <= 0);
    this.Reposition();
  }

  private void ShowCurrentJackpot(bool slam = true, float rollupTime = 0.0f)
  {
    JackpotPayload.Instance.ShowCurrentJackpot(this.jackpot, slam, rollupTime);
  }

  private void OnJackpotDataUpdated(Hashtable data)
  {
    if (data == null)
      return;
    int outData = -1;
    DatabaseTools.UpdateData(data, "type", ref outData);
    this.ShowCurrentJackpot(false, outData == -1 ? 4.5f : 0.0f);
    if (data[(object) "top1_list"] == null || outData == 3 || !JackpotPayload.Instance.NeedRefreshRankContent)
      return;
    this.StartCoroutine(this.UpdateJackpotContent());
  }

  [DebuggerHidden]
  private IEnumerator UpdateJackpotContent()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JackpotDlg.\u003CUpdateJackpotContent\u003Ec__Iterator74()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void PlayJackpot(int winType = 0, int winRewardId = 0)
  {
    this.StartCoroutine(this.Play2WinJackpot(winType, winRewardId));
  }

  [DebuggerHidden]
  private IEnumerator Play2WinJackpot(int winType, int winRewardId)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JackpotDlg.\u003CPlay2WinJackpot\u003Ec__Iterator75()
    {
      winType = winType,
      winRewardId = winRewardId,
      \u003C\u0024\u003EwinType = winType,
      \u003C\u0024\u003EwinRewardId = winRewardId,
      \u003C\u003Ef__this = this
    };
  }

  private void ShowItemInfo(KeyValuePair<int, List<int>> pair, int slot, int itemIndex)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(pair.Value[itemIndex]);
    string str1 = "icon_jackpot_gold";
    string str2 = !((UnityEngine.Object) this.itemSlot[slot].mainTexture != (UnityEngine.Object) null) ? string.Empty : this.itemSlot[slot].mainTexture.name;
    bool flag1 = itemIndex == pair.Value.Count - 1;
    if (this.stopSlotFlag[slot] == 1 && (itemStaticInfo != null && itemStaticInfo.Image == str2 || str1 == str2))
      return;
    string str3 = string.Empty;
    if (itemStaticInfo != null)
      str3 = itemStaticInfo.ImagePath;
    else if (pair.Value[itemIndex] == -1)
      str3 = "Texture/ItemIcons/" + str1;
    if (!string.IsNullOrEmpty(str3))
    {
      bool flag2 = File.Exists("Assets/__RawArtData/Resources/" + str3 + ".png");
      BuilderFactory instance = BuilderFactory.Instance;
      bool flag3 = !flag2;
      UITexture uiTexture = this.itemSlot[slot];
      string path = str3;
      // ISSUE: variable of the null type
      __Null local = null;
      int num1 = flag3 ? 1 : 0;
      int num2 = 0;
      string empty = string.Empty;
      instance.HandyBuild((UIWidget) uiTexture, path, (System.Action<bool>) local, num1 != 0, num2 != 0, empty);
    }
    if (!flag1)
      return;
    this.stopSlotFlag[slot] = 1;
    this.ShowItemStopEffect(slot, true);
    AudioManager.Instance.StopSound("sfx_city_jackpot_start");
    if (pair.Value[itemIndex] != -1)
      AudioManager.Instance.StopAndPlaySound("sfx_city_jackpot_item");
    else
      AudioManager.Instance.StopAndPlaySound("sfx_city_jackpot_item_special");
  }

  private void ShowWinCollectEffect(int winType, int winRewardId)
  {
    this.rewardCollected = true;
    RewardsCollectionAnimator.Instance.Clear();
    ItemRewardInfo.Data data1 = new ItemRewardInfo.Data();
    ItemStaticInfo baseRewardInfo = JackpotPayload.Instance.BaseRewardInfo;
    if (baseRewardInfo != null)
    {
      data1.icon = baseRewardInfo.ImagePath;
      data1.count = (float) JackpotPayload.Instance.BaseRewardCount;
      RewardsCollectionAnimator.Instance.items.Add(data1);
    }
    if (winType == 0)
    {
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }
    else
    {
      int num;
      switch (winType)
      {
        case 1:
          num = 1;
          break;
        case 2:
          num = 5;
          break;
        default:
          return;
      }
      ItemRewardInfo.Data data2 = new ItemRewardInfo.Data();
      ResRewardsInfo.Data data3 = new ResRewardsInfo.Data();
      data2.count = (float) (data3.count = 1);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(winRewardId);
      if (itemStaticInfo == null)
        return;
      ItemBag.ItemType type = itemStaticInfo.Type;
      data2.icon = itemStaticInfo.ImagePath;
      for (int index = 0; index < num; ++index)
        RewardsCollectionAnimator.Instance.items.Add(data2);
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }
  }

  private void ClearStatus()
  {
    for (int index = 0; index < this.stopSlotFlag.Length; ++index)
      this.stopSlotFlag[index] = 0;
    for (int index = 0; index < this.itemStopEffect.Length; ++index)
    {
      NGUITools.SetActive(this.itemStopEffect[index], false);
      NGUITools.SetActive(this.itemSingleMultiEffect[index], false);
      NGUITools.SetActive(this.itemFiveMultiEffect[index], false);
    }
    NGUITools.SetActive(this.crystalBallEffect, false);
    NGUITools.SetActive(this.bigWinCoinEffect, false);
    NGUITools.SetActive(this.bigWinFireworkEffect, false);
  }

  private void ShowItemStopEffect(int slot, bool show = true)
  {
    NGUITools.SetActive(this.itemStopEffect[slot], show);
  }

  private void ShowItemWinEffect(int winType, int winRewardId, List<KeyValuePair<int, List<int>>> jackpotData)
  {
    int jackpotSlotCount = ConfigManager.inst.DB_CasinoJackpot.JackpotSlotCount;
    for (int i = 0; i < jackpotSlotCount; ++i)
    {
      KeyValuePair<int, List<int>> keyValuePair = jackpotData.Find((Predicate<KeyValuePair<int, List<int>>>) (x => x.Key == i));
      if (keyValuePair.Value[keyValuePair.Value.Count - 1] == winRewardId)
      {
        switch (winType)
        {
          case 1:
            NGUITools.SetActive(this.itemSingleMultiEffect[keyValuePair.Key], true);
            continue;
          case 2:
            NGUITools.SetActive(this.itemFiveMultiEffect[keyValuePair.Key], true);
            continue;
          case 3:
            NGUITools.SetActive(this.bigWinCoinEffect, true);
            NGUITools.SetActive(this.bigWinFireworkEffect, true);
            this.ClearData();
            this.ShowRankingContent();
            continue;
          default:
            continue;
        }
      }
    }
    switch (winType)
    {
      case 1:
      case 2:
        AudioManager.Instance.StopAndPlaySound("sfx_city_jackpot_win");
        break;
      case 3:
        AudioManager.Instance.StopAndPlaySound("sfx_city_jackpot_win_special");
        break;
    }
  }

  private void ShowCrystalEffect(bool show = true)
  {
    NGUITools.SetActive(this.crystalBallEffect, show);
  }

  [DebuggerHidden]
  private IEnumerator PrepareStopJackpot()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new JackpotDlg.\u003CPrepareStopJackpot\u003Ec__Iterator76()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowJackpotDefaultContent()
  {
    List<KeyValuePair<int, List<int>>> jackpotFakeSavedData = CasinoFunHelper.Instance.JackpotFakeSavedData;
    int jackpotSlotCount = ConfigManager.inst.DB_CasinoJackpot.JackpotSlotCount;
    int itemEachSlotCount = ConfigManager.inst.DB_CasinoJackpot.ItemEachSlotCount;
    for (int i = 0; i < jackpotSlotCount; ++i)
    {
      KeyValuePair<int, List<int>> keyValuePair = jackpotFakeSavedData.Find((Predicate<KeyValuePair<int, List<int>>>) (x => x.Key == i));
      int index = keyValuePair.Value.Count - 1;
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(keyValuePair.Value[index]);
      if (itemStaticInfo != null)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemSlot[i], itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      else if (keyValuePair.Value[index] == -1)
        BuilderFactory.Instance.HandyBuild((UIWidget) this.itemSlot[i], "Texture/ItemIcons/icon_jackpot_gold", (System.Action<bool>) null, true, false, string.Empty);
    }
  }

  private void CheckBetButtonStatus(bool enable = true)
  {
    if ((UnityEngine.Object) this.propsBetButton == (UnityEngine.Object) null || (UnityEngine.Object) this.coinsBetButton == (UnityEngine.Object) null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_jackpot_coin");
    if (itemStaticInfo != null)
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(itemStaticInfo.internalId);
      this.propsBetButton.isEnabled = consumableItemData != null && consumableItemData.quantity >= ConfigManager.inst.DB_CasinoJackpot.CostProps2Play && enable;
    }
    this.coinsBetButton.isEnabled = enable;
    this.costGoldCount.text = Utils.FormatThousands(ConfigManager.inst.DB_CasinoJackpot.CostCoins2Play.ToString());
    if (PlayerData.inst.userData.currency.gold < (long) ConfigManager.inst.DB_CasinoJackpot.CostCoins2Play)
      this.costGoldCount.color = Color.red;
    else
      this.costGoldCount.color = this.costGoldTextColor;
  }

  private void CheckJackpotRankStatus()
  {
    JackpotPayload.Instance.UpdateDataByCache();
  }

  private void AddEventHandler()
  {
    JackpotPayload.Instance.onJackpotDataUpdated -= new System.Action<Hashtable>(this.OnJackpotDataUpdated);
    JackpotPayload.Instance.onJackpotDataUpdated += new System.Action<Hashtable>(this.OnJackpotDataUpdated);
  }

  private void RemoveEventHandler()
  {
    JackpotPayload.Instance.onJackpotDataUpdated -= new System.Action<Hashtable>(this.OnJackpotDataUpdated);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool activeOpened;
  }
}
