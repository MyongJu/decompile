﻿// Decompiled with JetBrains decompiler
// Type: RoundPlayerEvil
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class RoundPlayerEvil : RoundPlayer
{
  public override void Shutdown()
  {
  }

  public override RoundPlayer.Camp PlayerCamp
  {
    get
    {
      return RoundPlayer.Camp.Evil;
    }
  }

  public override void InitializeStateMachine()
  {
    this.StateMacine.AddState((IState) new GuardStateIdleEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateDeathEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateDeathEndEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateAttackEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateUnderAttackEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateSpellEvil((RoundPlayer) this));
    this.StateMacine.AddState((IState) new GuardStateEnhanceEvil((RoundPlayer) this));
  }
}
