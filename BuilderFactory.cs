﻿// Decompiled with JetBrains decompiler
// Type: BuilderFactory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuilderFactory : MonoBehaviour
{
  public string MissingSpriteName = "missingSprite";
  private Dictionary<UIWidget, IBuilder> _builders = new Dictionary<UIWidget, IBuilder>();
  private Dictionary<string, Texture> _customMissingIconDict = new Dictionary<string, Texture>();
  private Stack<string> dialogstack = new Stack<string>();
  private Stack<string> popupstack = new Stack<string>();
  private Dictionary<string, List<UIWidget>> dialogwidgetcollection = new Dictionary<string, List<UIWidget>>();
  private Dictionary<string, List<UIWidget>> popupwidgetcollection = new Dictionary<string, List<UIWidget>>();
  public UIAtlas MissingAtlas;
  public Texture MissingTexture;
  private static BuilderFactory _instance;
  private GameObject _advancedItemIcon;

  private void Awake()
  {
    BuilderFactory._instance = this;
  }

  public static BuilderFactory Instance
  {
    get
    {
      return BuilderFactory._instance;
    }
  }

  public IBuilder Build(UIWidget uiWidget, string path, System.Action<bool> callBack = null, bool needtempicon = true, bool useWidgetSetting = false, bool needReplace = true, string customMissingIconPath = "")
  {
    if (!(bool) ((UnityEngine.Object) uiWidget) || string.IsNullOrEmpty(path))
    {
      if (callBack != null)
        callBack(false);
      return (IBuilder) null;
    }
    AdvancedItemIcon[] componentsInChildren = uiWidget.transform.GetComponentsInChildren<AdvancedItemIcon>(true);
    if (needReplace && path.Contains("Texture/ItemIcons/"))
    {
      string str = path.Remove(0, "Texture/ItemIcons/".Length);
      if (ConfigManager.inst.DB_Items.Contains(str))
      {
        UITexture uiTexture = uiWidget as UITexture;
        if ((UnityEngine.Object) uiTexture != (UnityEngine.Object) null)
          uiTexture.mainTexture = (Texture) null;
        if (componentsInChildren != null && componentsInChildren.Length > 0)
        {
          componentsInChildren[0].SetItemData(str, (System.Action<bool>) null, true, false);
        }
        else
        {
          if ((UnityEngine.Object) null == (UnityEngine.Object) this._advancedItemIcon)
            this._advancedItemIcon = AssetManager.Instance.HandyLoad("Prefab/UI/Common/AdvancedItemIcon", (System.Type) null) as GameObject;
          Utils.DuplicateGOB(this._advancedItemIcon, uiWidget.transform).GetComponent<AdvancedItemIcon>().SetItemData(str, (System.Action<bool>) null, true, false);
        }
        return (IBuilder) null;
      }
      if (componentsInChildren != null && componentsInChildren.Length > 0)
        NGUITools.Destroy((UnityEngine.Object) componentsInChildren[0].gameObject);
    }
    else if (componentsInChildren != null && componentsInChildren.Length > 0)
      NGUITools.Destroy((UnityEngine.Object) componentsInChildren[0].gameObject);
    if (this._builders.ContainsKey(uiWidget))
    {
      if (this._builders[uiWidget].OnCompleteHandler != null)
        this._builders[uiWidget].OnCompleteHandler(false);
      this.Release(uiWidget);
    }
    IBuilder builder = this.Create(uiWidget);
    if (builder != null)
    {
      builder.OnCompleteHandler = callBack;
      builder.needTempIcon = needtempicon;
      builder.useWidgetSetting = useWidgetSetting;
      if (this._customMissingIconDict.ContainsKey(customMissingIconPath))
      {
        builder.CustomMissingTexture = this._customMissingIconDict[customMissingIconPath];
      }
      else
      {
        builder.CustomMissingTexture = AssetManager.Instance.Load(customMissingIconPath, typeof (Texture)) as Texture;
        this._customMissingIconDict.Add(customMissingIconPath, builder.CustomMissingTexture);
      }
      builder.Build(path);
      return builder;
    }
    D.error((object) ("can not build  " + ((object) uiWidget).GetType().ToString() + " type!"));
    return (IBuilder) null;
  }

  public IBuilder HandyBuild(UIWidget uiWidget, string path, System.Action<bool> callBack = null, bool needtempicon = true, bool useWidgetSetting = false, string customMissingIconPath = "")
  {
    Stack<string> stringStack = (Stack<string>) null;
    Dictionary<string, List<UIWidget>> dictionary = (Dictionary<string, List<UIWidget>>) null;
    bool flag1 = false;
    if (this.dialogstack.Count > 0)
    {
      string pname = this.dialogstack.Peek();
      flag1 = (UnityEngine.Object) null != (UnityEngine.Object) this.HasParentByName(uiWidget.transform, pname);
    }
    bool flag2 = false;
    if (this.popupstack.Count > 0)
    {
      this.popupstack.Peek();
      flag2 = (UnityEngine.Object) null != (UnityEngine.Object) this.HasParentByName(uiWidget.transform, this.popupstack.Peek());
    }
    if (flag1)
    {
      stringStack = this.dialogstack;
      dictionary = this.dialogwidgetcollection;
    }
    if (flag2)
    {
      stringStack = this.popupstack;
      dictionary = this.popupwidgetcollection;
    }
    if (stringStack != null)
    {
      string key = stringStack.Peek();
      List<UIWidget> uiWidgetList = (List<UIWidget>) null;
      if (dictionary.TryGetValue(key, out uiWidgetList))
        uiWidgetList.Add(uiWidget);
      else
        Debug.LogWarning((object) ("can not find collection " + key));
    }
    string customMissingIconPath1 = customMissingIconPath;
    return this.Build(uiWidget, path, callBack, needtempicon, useWidgetSetting, true, customMissingIconPath1);
  }

  internal Transform HasParentByName(Transform transform, string pname)
  {
    UIControler[] componentsInParent = transform.GetComponentsInParent<UIControler>(true);
    if (componentsInParent.Length != 0)
    {
      string str = componentsInParent[0].name.Replace("(Clone)", string.Empty);
      if (pname.Contains(str))
        return componentsInParent[0].transform;
    }
    return (Transform) null;
  }

  public void Release(UIWidget uiWidget)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) uiWidget)
      return;
    AdvancedItemIcon[] componentsInChildren = uiWidget.GetComponentsInChildren<AdvancedItemIcon>(true);
    if (componentsInChildren.Length != 0)
    {
      componentsInChildren[0].Release();
      componentsInChildren[0].gameObject.SetActive(false);
      componentsInChildren[0].transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChildren[0].gameObject);
    }
    else
    {
      if (!this._builders.ContainsKey(uiWidget))
        return;
      this._builders[uiWidget].Release();
      this._builders.Remove(uiWidget);
    }
  }

  protected IBuilder Create(UIWidget uiWidget)
  {
    BaseBuilder baseBuilder = (BaseBuilder) null;
    if (uiWidget is UISprite)
      baseBuilder = (BaseBuilder) new UISpriteBuilder();
    else if (uiWidget is UITexture)
      baseBuilder = (BaseBuilder) new UITextureBuilder();
    else if (uiWidget is UI2DSprite)
      baseBuilder = (BaseBuilder) new UI2DSpriteBuilder();
    if (baseBuilder == null)
      return (IBuilder) null;
    baseBuilder.Target = uiWidget.gameObject;
    baseBuilder.MissingAtlas = this.MissingAtlas;
    baseBuilder.MissingSpriteName = this.MissingSpriteName;
    baseBuilder.MissingTexture = this.MissingTexture;
    if (!this._builders.ContainsKey(uiWidget))
      this._builders.Add(uiWidget, (IBuilder) baseBuilder);
    return (IBuilder) baseBuilder;
  }

  internal void Register(string uiname, BuilderFactory.UIType type)
  {
    Stack<string> stringStack;
    Dictionary<string, List<UIWidget>> dictionary;
    if (type == BuilderFactory.UIType.dialog)
    {
      stringStack = this.dialogstack;
      dictionary = this.dialogwidgetcollection;
    }
    else
    {
      stringStack = this.popupstack;
      dictionary = this.popupwidgetcollection;
    }
    stringStack.Push(uiname);
    if (dictionary.ContainsKey(uiname))
      return;
    dictionary.Add(uiname, new List<UIWidget>());
  }

  internal void Unregister(string uiname, BuilderFactory.UIType type)
  {
    Stack<string> stringStack;
    Dictionary<string, List<UIWidget>> dictionary;
    if (type == BuilderFactory.UIType.dialog)
    {
      stringStack = this.dialogstack;
      dictionary = this.dialogwidgetcollection;
    }
    else
    {
      stringStack = this.popupstack;
      dictionary = this.popupwidgetcollection;
    }
    string key = stringStack.Pop();
    if (key == uiname)
    {
      List<UIWidget> uiWidgetList = (List<UIWidget>) null;
      if (dictionary.TryGetValue(key, out uiWidgetList))
      {
        using (List<UIWidget>.Enumerator enumerator = uiWidgetList.GetEnumerator())
        {
          while (enumerator.MoveNext())
            this.Release(enumerator.Current);
        }
        uiWidgetList.Clear();
        dictionary.Remove(key);
      }
      else
        Debug.LogWarning((object) ("something wrong happened " + uiname));
    }
    else
      Debug.LogWarning((object) ("wrong dialog stack " + key + " " + uiname));
  }

  public void LogCurrentStack()
  {
    using (Stack<string>.Enumerator enumerator1 = this.dialogstack.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        string current1 = enumerator1.Current;
        List<UIWidget> uiWidgetList = (List<UIWidget>) null;
        if (this.dialogwidgetcollection.TryGetValue(current1, out uiWidgetList))
        {
          using (List<UIWidget>.Enumerator enumerator2 = uiWidgetList.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              UIWidget current2 = enumerator2.Current;
            }
          }
        }
      }
    }
    using (Stack<string>.Enumerator enumerator1 = this.popupstack.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        string current1 = enumerator1.Current;
        List<UIWidget> uiWidgetList = (List<UIWidget>) null;
        if (this.popupwidgetcollection.TryGetValue(current1, out uiWidgetList))
        {
          using (List<UIWidget>.Enumerator enumerator2 = uiWidgetList.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              UIWidget current2 = enumerator2.Current;
            }
          }
        }
      }
    }
  }

  public enum UIType
  {
    dialog,
    popup,
  }
}
