﻿// Decompiled with JetBrains decompiler
// Type: ConfigTavernWheelGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigTavernWheelGroup
{
  private Dictionary<string, TavernWheelGroupInfo> m_DataByID;
  private Dictionary<int, TavernWheelGroupInfo> m_DataByInternalID;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<TavernWheelGroupInfo, string>(result as Hashtable, "id", out this.m_DataByID, out this.m_DataByInternalID);
  }

  public TavernWheelGroupInfo Get(string id)
  {
    TavernWheelGroupInfo tavernWheelGroupInfo;
    this.m_DataByID.TryGetValue(id, out tavernWheelGroupInfo);
    return tavernWheelGroupInfo;
  }

  public TavernWheelGroupInfo Get(int internalId)
  {
    TavernWheelGroupInfo tavernWheelGroupInfo;
    this.m_DataByInternalID.TryGetValue(internalId, out tavernWheelGroupInfo);
    return tavernWheelGroupInfo;
  }

  public void Clear()
  {
    if (this.m_DataByID != null)
    {
      this.m_DataByID.Clear();
      this.m_DataByID = (Dictionary<string, TavernWheelGroupInfo>) null;
    }
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
    this.m_DataByInternalID = (Dictionary<int, TavernWheelGroupInfo>) null;
  }
}
