﻿// Decompiled with JetBrains decompiler
// Type: ConfigPlayerProfile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigPlayerProfile
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigPlayerProfileInfo> datas;
  private Dictionary<int, ConfigPlayerProfileInfo> dicByUniqueId;
  private Dictionary<string, List<ConfigPlayerProfileInfo>> categoryMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigPlayerProfileInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.categoryMap = new Dictionary<string, List<ConfigPlayerProfileInfo>>();
    if (this.dicByUniqueId == null)
      return;
    Dictionary<int, ConfigPlayerProfileInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this.categoryMap.ContainsKey(enumerator.Current.category))
        this.categoryMap.Add(enumerator.Current.category, new List<ConfigPlayerProfileInfo>());
      this.categoryMap[enumerator.Current.category].Add(enumerator.Current);
    }
  }

  public List<ConfigPlayerProfileInfo> GetPlayerProfileInfoByCategory(string category)
  {
    return this.categoryMap[category];
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId != null)
      this.dicByUniqueId.Clear();
    if (this.categoryMap == null)
      return;
    this.categoryMap.Clear();
  }
}
