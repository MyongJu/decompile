﻿// Decompiled with JetBrains decompiler
// Type: BuildingHintManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class BuildingHintManager : MonoBehaviour
{
  private const string path = "Prefab/UI/Dialogs/Tutorial/";
  public const string talkmode = "TalkMode";
  private const string recordtime = "idleBuildingShowTime";
  public const string recordcount = "idleBuildingShowCount";
  private static BuildingHintManager _instance;
  private int refreshtime;
  private int currenttick;
  private int strongholdlevel;
  private int maxcount;
  private bool hashint;

  public static BuildingHintManager Instance
  {
    get
    {
      if ((UnityEngine.Object) BuildingHintManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        BuildingHintManager._instance = gameObject.AddComponent<BuildingHintManager>();
        gameObject.name = nameof (BuildingHintManager);
      }
      return BuildingHintManager._instance;
    }
  }

  public void Init()
  {
    this.strongholdlevel = ConfigManager.inst.DB_GameConfig.GetData("building_idle_max_level").ValueInt;
    this.maxcount = ConfigManager.inst.DB_GameConfig.GetData("building_idle_max_times").ValueInt;
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    this.refreshtime = ConfigManager.inst.DB_GameConfig.GetData("building_idle_interval").ValueInt;
  }

  public void Dispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public bool Hashint
  {
    get
    {
      return this.hashint;
    }
    set
    {
      this.hashint = value;
    }
  }

  public void CheckIdleBuildings()
  {
    int index = 0;
    BuildingControllerNew bc = (BuildingControllerNew) null;
    for (; index < CitadelSystem.inst.GobBuildings.Count; ++index)
    {
      GameObject gobBuilding = CitadelSystem.inst.GobBuildings[index];
      if (!((UnityEngine.Object) null == (UnityEngine.Object) gobBuilding))
      {
        bc = gobBuilding.GetComponent<BuildingControllerNew>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) bc && bc.IsIdleState())
        {
          BuildingIdleInfo bii = ConfigManager.inst.DB_BuildingIdle.GetBuildingIdleInfoByBuildingType(bc.mBuildingItem.mType);
          CityCircleBtnPara para = new CityCircleBtnPara(AtlasType.Gui_2, "btn_idle", "id_train", new EventDelegate((EventDelegate.Callback) (() =>
          {
            this.hashint = false;
            UIManager.inst.OpenPopup("BuildingHintPopup", (Popup.PopupParameter) new BuildingHintPopup.Parameter()
            {
              buildingType = bii.buildingtype
            });
            bc.RefreshStats();
          })), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          CollectionUIManager.Instance.OpenCollectionUI(bc.transform, para, CollectionUI.CollectionType.IdleHint);
          this.hashint = true;
          break;
        }
      }
    }
    if (!this.hashint)
      return;
    CitadelSystem.inst.GobBuildings.RemoveAt(index);
    CitadelSystem.inst.GobBuildings.Add(bc.gameObject);
  }

  private void OnSecond(int time)
  {
    ++this.currenttick;
    if (this.currenttick != this.refreshtime)
      return;
    if (this.NeedCheckState())
      this.CheckIdleBuildings();
    this.currenttick = 0;
  }

  private bool NeedCheckState()
  {
    bool flag1 = this.strongholdlevel > CitadelSystem.inst.MStronghold.mBuildingItem.mLevel;
    bool flag2;
    if (!PlayerPrefsEx.HasKey("idleBuildingShowTime"))
    {
      PlayerPrefsEx.SetString("idleBuildingShowTime", NetServerTime.inst.UpdateTime.ToString());
      PlayerPrefsEx.SetInt("idleBuildingShowCount", 0);
      flag2 = true;
    }
    else if (NetServerTime.inst.IsToday(double.Parse(PlayerPrefsEx.GetString("idleBuildingShowTime"))))
    {
      flag2 = PlayerPrefsEx.GetInt("idleBuildingShowCount") < this.maxcount;
    }
    else
    {
      PlayerPrefsEx.SetString("idleBuildingShowTime", NetServerTime.inst.UpdateTime.ToString());
      PlayerPrefsEx.SetInt("idleBuildingShowCount", 0);
      flag2 = true;
    }
    if (flag1 && flag2)
      return !this.hashint;
    return false;
  }
}
