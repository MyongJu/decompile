﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomSetting
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatRoomSetting : MonoBehaviour
{
  private ChatRoomSetting.Data data = new ChatRoomSetting.Data();
  private bool _isOpen;
  private bool _isSearched;
  [SerializeField]
  private ChatRoomSetting.Panel panel;

  private bool isOpen
  {
    get
    {
      return this._isOpen;
    }
    set
    {
      this._isOpen = value;
      if (value)
        this.panel.privateSwitchSprite.spriteName = "slider_01";
      else
        this.panel.privateSwitchSprite.spriteName = "slider_02";
    }
  }

  private bool isSearched
  {
    get
    {
      return this._isSearched;
    }
    set
    {
      this._isSearched = value;
      if (value)
        this.panel.searchSwitchSprite.spriteName = "slider_01";
      else
        this.panel.searchSwitchSprite.spriteName = "slider_02";
    }
  }

  public void Setup(ChatRoomData roomData)
  {
    this.data.roomData = roomData;
    this.isOpen = this.data.roomData.isOpen;
    this.isSearched = this.data.roomData.isSearch;
    Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = roomData.members.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == PlayerData.inst.uid)
        this.data.curMember = enumerator.Current;
    }
    if (this.data.curMember.title > ChatRoomMember.Title.member)
    {
      this.panel.title.text = Utils.XLAT("chat_uppercase_settings_title");
      this.panel.welInput.enabled = true;
      this.panel.welBoxCollider.enabled = true;
      this.panel.BT_private.enabled = true;
      this.panel.BT_search.enabled = true;
    }
    else
    {
      this.panel.title.text = Utils.XLAT("chat_view_welcome_message_settings_title");
      this.panel.welInput.enabled = false;
      this.panel.welBoxCollider.enabled = false;
      this.panel.BT_private.enabled = false;
      this.panel.BT_search.enabled = false;
    }
    this.panel.welInput.value = string.Empty;
    this.panel.welText.text = this.data.roomData.roomTitle;
  }

  public void OnOpenClick()
  {
    this.isOpen = !this.isOpen;
  }

  public void OnSearchClick()
  {
    this.isSearched = !this.isSearched;
  }

  public void OnDoneClick()
  {
    if (this.data.curMember.title <= ChatRoomMember.Title.member)
      return;
    MessageHub.inst.GetPortByAction("Chat:setupRoom").SendRequest(Utils.Hash((object) "room_id", (object) this.data.roomData.roomId, (object) "title", (object) this.panel.welText.text, (object) "is_open", (object) (!this.isOpen ? 2 : 1), (object) "is_search", (object) (!this.isSearched ? 2 : 1)), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    }), true);
  }

  public void Reset()
  {
    this.panel.title = this.transform.Find("Background/Title_Bar").gameObject.GetComponent<UILabel>();
    this.panel.welMsgTitle = this.transform.Find("Welcome_Message_Container/Welcome_Label").gameObject.GetComponent<UILabel>();
    this.panel.welText = this.transform.Find("Welcome_Message_Container/Top_Header/Input/Text").gameObject.GetComponent<UILabel>();
    GameObject gameObject = this.transform.Find("Welcome_Message_Container/Top_Header/Input").gameObject;
    this.panel.welInput = gameObject.GetComponent<UIInput>();
    this.panel.welBoxCollider = gameObject.GetComponent<BoxCollider>();
    this.panel.BT_private = this.transform.Find("PrivateSetting").gameObject.GetComponent<UIButton>();
    this.panel.privateSwitchSprite = this.transform.Find("PrivateSetting/Switch").gameObject.GetComponent<UISprite>();
    this.panel.BT_search = this.transform.Find("SearchedSetting").gameObject.GetComponent<UIButton>();
    this.panel.searchSwitchSprite = this.transform.Find("SearchedSetting/Switch").gameObject.GetComponent<UISprite>();
    this.panel.BT_done = this.transform.Find("Btn_Done").gameObject.GetComponent<UIButton>();
  }

  public class Data
  {
    public ChatRoomData roomData;
    public ChatRoomMember curMember;
  }

  [Serializable]
  public class Panel
  {
    public UILabel title;
    public UILabel welMsgTitle;
    public UILabel welText;
    public UIInput welInput;
    public BoxCollider welBoxCollider;
    public UISprite privateSwitchSprite;
    public UIButton BT_private;
    public UISprite searchSwitchSprite;
    public UIButton BT_search;
    public UIButton BT_done;
  }
}
