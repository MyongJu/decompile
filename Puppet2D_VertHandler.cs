﻿// Decompiled with JetBrains decompiler
// Type: Puppet2D_VertHandler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class Puppet2D_VertHandler : MonoBehaviour
{
  private Mesh mesh;
  private Vector3[] verts;
  private Vector3 vertPos;
  private GameObject[] handles;

  private void OnEnable()
  {
    this.mesh = this.GetComponent<MeshFilter>().sharedMesh;
    this.verts = this.mesh.vertices;
    foreach (Vector3 vert in this.verts)
    {
      this.vertPos = this.transform.TransformPoint(vert);
      GameObject gameObject = new GameObject("handle")
      {
        transform = {
          position = this.vertPos,
          parent = this.transform
        },
        tag = "handle"
      };
      MonoBehaviour.print((object) this.vertPos);
    }
  }

  private void Update()
  {
    this.handles = GameObject.FindGameObjectsWithTag("handle");
    for (int index = 0; index < this.verts.Length; ++index)
      this.verts[index] = this.handles[index].transform.localPosition;
    this.mesh.vertices = this.verts;
    this.mesh.RecalculateBounds();
    this.mesh.RecalculateNormals();
  }
}
