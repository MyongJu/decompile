﻿// Decompiled with JetBrains decompiler
// Type: ConfigPlayerTalent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigPlayerTalent
{
  private ConfigParseEx<PlayerTalentInfo> _parse = new ConfigParseEx<PlayerTalentInfo>();
  private ConfigFieldCache<int> _treeIdCache = new ConfigFieldCache<int>();
  private Dictionary<int, List<PlayerTalentInfo>> _treeCache;

  public void BuildDB(object res)
  {
    this._parse.Initialize(res as Hashtable);
    this._treeIdCache.Initialize((ConfigRawData) this._parse, "tree_id");
    this._treeCache = new Dictionary<int, List<PlayerTalentInfo>>();
  }

  private List<PlayerTalentInfo> GetTalentListByTreeId(int talentTreeId)
  {
    List<PlayerTalentInfo> playerTalentInfoList;
    if (!this._treeCache.TryGetValue(talentTreeId, out playerTalentInfoList))
    {
      playerTalentInfoList = new List<PlayerTalentInfo>();
      List<int> internalIds = this._parse.InternalIds;
      for (int index = 0; index < internalIds.Count; ++index)
      {
        int internalId = internalIds[index];
        if (this._treeIdCache.Get(internalId, 0) == talentTreeId)
        {
          PlayerTalentInfo playerTalentInfo = this.GetPlayerTalentInfo(internalId);
          playerTalentInfoList.Add(playerTalentInfo);
        }
      }
      playerTalentInfoList.Sort((Comparison<PlayerTalentInfo>) ((x, y) => y.level.CompareTo(x.level)));
      for (int index = 0; index < playerTalentInfoList.Count; ++index)
        playerTalentInfoList[index].maxLevel = playerTalentInfoList.Count;
      this._treeCache.Add(talentTreeId, playerTalentInfoList);
    }
    return playerTalentInfoList;
  }

  public PlayerTalentInfo GetMaxLevelTalentByTalentTreeId(int talentTreeId)
  {
    List<PlayerTalentInfo> talentListByTreeId = this.GetTalentListByTreeId(talentTreeId);
    if (talentListByTreeId.Count > 0)
      return talentListByTreeId[0];
    return (PlayerTalentInfo) null;
  }

  public PlayerTalentInfo GetCurrentLevelTalentByTalentTreeId(int talentTreeId)
  {
    List<PlayerTalentInfo> talentListByTreeId = this.GetTalentListByTreeId(talentTreeId);
    for (int index = 0; index < talentListByTreeId.Count; ++index)
    {
      if (DBManager.inst.DB_Talent.IsTalentOwned(talentListByTreeId[index].internalId))
        return talentListByTreeId[index];
    }
    return (PlayerTalentInfo) null;
  }

  public Requirements GetRequirementsByTalentTreeId(int talentTreeId)
  {
    List<PlayerTalentInfo> talentListByTreeId = this.GetTalentListByTreeId(talentTreeId);
    return talentListByTreeId[talentListByTreeId.Count - 1].require;
  }

  public string GetBenefitByInternalId(int internalid)
  {
    return this.GetBenefitByStringId(this.GetPlayerTalentInfo(internalid).ID);
  }

  public string GetBenefitByStringId(string id)
  {
    string str = (string) null;
    PlayerTalentInfo playerTalentInfo1 = this.GetPlayerTalentInfo(id);
    string id1 = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(playerTalentInfo1.talentTreeInternalId).ID;
    if (playerTalentInfo1.benefit.GetBenefitsPairs().Count < 1)
      return "0";
    switch (playerTalentInfo1.benefit.GetBenefitsPairs()[0].type)
    {
      case PropertyDefinition.FormatType.Integer:
        int num1 = 0;
        for (int index = 1; index <= playerTalentInfo1.level; ++index)
        {
          PlayerTalentInfo playerTalentInfo2 = this.GetPlayerTalentInfo(id1 + "_" + (object) index);
          num1 += (int) playerTalentInfo2.benefit.GetBenefitsPairs()[0].value;
        }
        str = num1.ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        float num2 = 0.0f;
        for (int index = 1; index <= playerTalentInfo1.level; ++index)
        {
          PlayerTalentInfo playerTalentInfo2 = this.GetPlayerTalentInfo(id1 + "_" + (object) index);
          num2 += playerTalentInfo2.benefit.GetBenefitsPairs()[0].value;
        }
        str = ((float) (int) ((double) num2 * 1000.0 + 0.5) / 1000f).ToString();
        break;
    }
    return str;
  }

  public PropertyDefinition.FormatType GetBenefitTypeByStringId(string id)
  {
    PlayerTalentInfo playerTalentInfo = this.GetPlayerTalentInfo(id);
    string id1 = ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(playerTalentInfo.talentTreeInternalId).ID;
    if (playerTalentInfo.benefit.GetBenefitsPairs().Count < 1)
      return PropertyDefinition.FormatType.Invalid;
    return playerTalentInfo.benefit.GetBenefitsPairs()[0].type;
  }

  public string GetIncreaseBenefitByInternalId(int internalid)
  {
    return this.GetIncreaseBenefitByStringId(this.GetPlayerTalentInfo(internalid).ID);
  }

  public string GetIncreaseBenefitByStringId(string id)
  {
    string str = (string) null;
    PlayerTalentInfo playerTalentInfo = this.GetPlayerTalentInfo(id);
    if (playerTalentInfo.benefit.GetBenefitsPairs().Count < 1)
      return "0";
    Benefits.BenefitValuePair benefitsPair = playerTalentInfo.benefit.GetBenefitsPairs()[0];
    switch (benefitsPair.type)
    {
      case PropertyDefinition.FormatType.Integer:
        str = ((int) benefitsPair.value).ToString();
        break;
      case PropertyDefinition.FormatType.Percent:
        str = benefitsPair.value.ToString();
        break;
    }
    return str;
  }

  public void Clear()
  {
  }

  public PlayerTalentInfo GetPlayerTalentInfo(int internalId)
  {
    PlayerTalentInfo playerTalentInfo = this._parse.Get(internalId);
    if (playerTalentInfo != null)
      playerTalentInfo.internalId = internalId;
    return playerTalentInfo;
  }

  public PlayerTalentInfo GetPlayerTalentInfo(string id)
  {
    return this.GetPlayerTalentInfo(this._parse.s2i(id));
  }

  public bool IsTalentTreeRequirementsComplete(int talentTreeId)
  {
    Requirements requirementsByTalentTreeId = this.GetRequirementsByTalentTreeId(talentTreeId);
    if (requirementsByTalentTreeId == null || requirementsByTalentTreeId.requires == null || requirementsByTalentTreeId.requires.Count == 0)
      return true;
    Dictionary<string, int>.KeyCollection.Enumerator enumerator = requirementsByTalentTreeId.requires.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (DBManager.inst.DB_Talent.IsTalentOwned(int.Parse(enumerator.Current)))
        return true;
    }
    return false;
  }
}
