﻿// Decompiled with JetBrains decompiler
// Type: ButtonBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof (UIGrid))]
public class ButtonBar : MonoBehaviour
{
  [NonSerialized]
  public static int groupSeed = 100;
  public int count = 2;
  [NonSerialized]
  public int group = -1;
  public Color normalColor = Color.gray;
  public Color selectedColor = Color.red;
  private Color _currentNomalColor = Color.gray;
  private Color _currentSelectedColor = Color.red;
  public List<EventDelegate> onChange = new List<EventDelegate>();
  private Stack<GameObject> pools = new Stack<GameObject>();
  public UIToggle toggleItemPrefab;
  private List<Transform> _children;
  private int _selectedIndex;
  private UIGrid grid;
  private int _currentCount;
  private UIToggle _currentPrefab;

  private void Start()
  {
    if (this.group == -1)
    {
      this.group = ButtonBar.groupSeed;
      ++ButtonBar.groupSeed;
    }
    this.grid = this.gameObject.GetComponent<UIGrid>();
    this.grid.arrangement = UIGrid.Arrangement.Horizontal;
    this.grid.hideInactive = true;
    this._children = new List<Transform>();
  }

  private void ReCreate()
  {
    if ((UnityEngine.Object) this.toggleItemPrefab == (UnityEngine.Object) null)
      return;
    this.Clear();
    this._currentPrefab = this.toggleItemPrefab;
    for (int index = 0; index < this.count; ++index)
    {
      UIToggle component = this.GetItem().GetComponent<UIToggle>();
      component.onChange = this.onChange;
      component.group = this.group;
    }
    this._children = this.grid.GetChildList();
    this._currentCount = this.count;
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.ResetSelected();
  }

  private GameObject GetItem()
  {
    GameObject gameObject;
    if (this.pools.Count > 0)
    {
      while (this.pools.Count > 0)
      {
        GameObject go = this.pools.Pop();
        if ((UnityEngine.Object) go != (UnityEngine.Object) null)
        {
          NGUITools.SetActive(go, true);
          return go;
        }
      }
      gameObject = NGUITools.AddChild(this.gameObject, this.toggleItemPrefab.gameObject);
    }
    else
      gameObject = NGUITools.AddChild(this.gameObject, this.toggleItemPrefab.gameObject);
    return gameObject;
  }

  public void SetButtonLabelColor(int index, Color color)
  {
    if (index >= this._children.Count)
      return;
    UILabel componentInChildren = this._children[index].gameObject.GetComponentInChildren<UILabel>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.color = color;
  }

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      if (this._selectedIndex != value && value < this._children.Count)
        this._selectedIndex = value;
      this.ResetSelected();
    }
  }

  private UIToggle GetToggle(Transform trans)
  {
    return trans.gameObject.GetComponent<UIToggle>();
  }

  private void ResetSelected()
  {
    for (int index = 0; index < this._children.Count; ++index)
    {
      this.GetToggle(this._children[index]).Set(false);
      this.SetButtonLabelColor(index, this.normalColor);
    }
    UIToggle toggle = this.GetToggle(this._children[this._selectedIndex]);
    this.SetButtonLabelColor(this._selectedIndex, this.selectedColor);
    toggle.Set(true);
    this._currentNomalColor = this.normalColor;
    this._currentSelectedColor = this.selectedColor;
  }

  private void Clear()
  {
    for (int index = 0; index < this._children.Count; ++index)
    {
      UIToggle toggle = this.GetToggle(this._children[index]);
      if (NGUITools.GetActive((Behaviour) toggle))
      {
        NGUITools.SetActive(toggle.gameObject, false);
        this.pools.Push(toggle.gameObject);
      }
    }
    this._children.Clear();
  }

  public void Dispose()
  {
    this.onChange.Clear();
    this._children.Clear();
    this.pools.Clear();
    this.toggleItemPrefab = (UIToggle) null;
    this._currentPrefab = (UIToggle) null;
  }

  private void Update()
  {
    if ((UnityEngine.Object) this.toggleItemPrefab == (UnityEngine.Object) null || this.count == this._currentCount && !(this._currentNomalColor != this.normalColor) && (!(this._currentSelectedColor != this.selectedColor) && !((UnityEngine.Object) this._currentPrefab != (UnityEngine.Object) this.toggleItemPrefab)))
      return;
    this.ReCreate();
  }
}
