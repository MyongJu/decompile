﻿// Decompiled with JetBrains decompiler
// Type: EquipmentEnhanceBenefitComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentEnhanceBenefitComponent : ComponentRenderBase
{
  public UITexture icon;
  public UILabel descriptionLabel;
  public UILabel valueLabel;

  public override void Init()
  {
    EquipmentEnhanceBenefitComponentData data = this.data as EquipmentEnhanceBenefitComponentData;
    this.descriptionLabel.text = data.currentLevel.propertyDefinition.Name;
    this.valueLabel.text = data.nextLevel == null ? data.currentLevel.propertyDefinition.ConvertToDisplayString((double) data.currentLevel.value, true, true) : data.currentLevel.propertyDefinition.ConvertToDisplayString((double) data.currentLevel.value, false, true) + data.currentLevel.propertyDefinition.ConvertToDisplayString((double) data.nextLevel.value - (double) data.currentLevel.value, true, true);
    BuilderFactory.Instance.Build((UIWidget) this.icon, data.currentLevel.propertyDefinition.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }
}
