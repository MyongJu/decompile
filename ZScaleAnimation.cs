﻿// Decompiled with JetBrains decompiler
// Type: ZScaleAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ZScaleAnimation : MonoBehaviour
{
  public float cameraRalationRatio;
  public float ScaleMin;
  public float ScalseMax;

  public Vector3 GetScale()
  {
    return Mathf.Clamp((float) ((0.0 - (double) this.transform.position.z) / 500.0) * this.cameraRalationRatio, this.ScaleMin, this.ScalseMax) * Vector3.one;
  }
}
