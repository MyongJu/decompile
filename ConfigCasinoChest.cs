﻿// Decompiled with JetBrains decompiler
// Type: ConfigCasinoChest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigCasinoChest
{
  private List<CasinoChestInfo> chestInfoList = new List<CasinoChestInfo>();
  private Dictionary<string, CasinoChestInfo> datas;
  private Dictionary<int, CasinoChestInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<CasinoChestInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, CasinoChestInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.chestInfoList.Add(enumerator.Current);
    }
    this.chestInfoList.Sort(new Comparison<CasinoChestInfo>(this.SortByID));
  }

  public List<CasinoChestInfo> GetCasinoChestInfoList()
  {
    return this.chestInfoList;
  }

  public CasinoChestInfo GetItem(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (CasinoChestInfo) null;
  }

  public CasinoChestInfo GetItemByIndex(int index)
  {
    if (this.chestInfoList[index] != null)
      return this.chestInfoList[index];
    return (CasinoChestInfo) null;
  }

  public int SortByID(CasinoChestInfo a, CasinoChestInfo b)
  {
    return int.Parse(a.id).CompareTo(int.Parse(b.id));
  }
}
