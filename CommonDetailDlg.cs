﻿// Decompiled with JetBrains decompiler
// Type: CommonDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class CommonDetailDlg : Popup
{
  public UILabel title;
  public UILabel detail;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CommonDetailDlg.Parameter parameter = orgParam as CommonDetailDlg.Parameter;
    this.title.text = parameter.title;
    this.detail.text = parameter.detail;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string title;
    public string detail;
  }
}
