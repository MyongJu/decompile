﻿// Decompiled with JetBrains decompiler
// Type: PopupEmbassyDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PopupEmbassyDlg : UI.Dialog
{
  [SerializeField]
  private PopupEmbassyDlg.Panel panel;
  private PopupEmbassyDlg.SelectType curSelectedType;
  private bool summeryScrollStoppedFlag;

  public void SelectSummary()
  {
    this.curSelectedType = PopupEmbassyDlg.SelectType.summary;
    this.panel.tagContainer.gameObject.SetActive(true);
    this.panel.TAB_summary.gameObject.SetActive(true);
    this.panel.TAB_specific.gameObject.SetActive(false);
    this.panel.summaryContainer.gameObject.SetActive(true);
    this.panel.specificContainer.gameObject.SetActive(false);
    this.panel.JoinAllianceContainer.gameObject.SetActive(false);
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    this.panel.summaryReiforcementContainer.gameObject.SetActive(true);
    Dictionary<string, int> dictionary1 = new Dictionary<string, int>();
    int num1 = 0;
    for (int index1 = 0; index1 < targetListByType.Count; ++index1)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(targetListByType[index1]);
      if (marchData != null && (marchData.troopsInfo.troops != null && marchData.state == MarchData.MarchState.reinforcing))
      {
        using (Dictionary<string, Unit>.KeyCollection.Enumerator enumerator = marchData.troopsInfo.troops.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            if (dictionary1.ContainsKey(current))
            {
              Dictionary<string, int> dictionary2;
              string index2;
              int num2 = (dictionary2 = dictionary1)[index2 = current] + marchData.troopsInfo.troops[current].Count;
              dictionary2[index2] = num2;
            }
            else
              dictionary1.Add(current, marchData.troopsInfo.troops[current].Count);
            num1 += marchData.troopsInfo.troops[current].Count;
          }
        }
      }
    }
    List<KeyValuePair<Unit_StatisticsInfo, int>> keyValuePairList = new List<KeyValuePair<Unit_StatisticsInfo, int>>();
    Dictionary<string, int>.KeyCollection.Enumerator enumerator1 = dictionary1.Keys.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(enumerator1.Current);
      if (data != null)
        keyValuePairList.Add(new KeyValuePair<Unit_StatisticsInfo, int>(data, dictionary1[enumerator1.Current]));
    }
    keyValuePairList.Sort((Comparison<KeyValuePair<Unit_StatisticsInfo, int>>) ((a, b) =>
    {
      if (a.Key.Troop_Tier == b.Key.Troop_Tier)
        return a.Key.Priority.CompareTo(b.Key.Priority);
      return b.Key.Troop_Tier.CompareTo(a.Key.Troop_Tier);
    }));
    for (int index = 0; index < this.panel.summaryTabel.transform.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.summaryTabel.transform.GetChild(index).gameObject);
    for (int index = 0; index < keyValuePairList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.summaryTabel.gameObject, this.panel.orgSummaryEmbassySlot.gameObject);
      gameObject.name = string.Format("slot_{0}", (object) (100 + index));
      EmbassySlotBarDetail component = gameObject.GetComponent<EmbassySlotBarDetail>();
      component.Setup(keyValuePairList[index].Key.ID, keyValuePairList[index].Value);
      component.gameObject.SetActive(true);
    }
    BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData("embassy", DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).buildings.level_embassy);
    this.panel.summaryTroopsTotalCount.text = string.Format("{0}/{1}", (object) Utils.ConvertNumberToNormalString(num1), (object) ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data1.Benefit_Value_1, "calc_city_reinforcement_capacity"));
    this.panel.summaryTabel.repositionNow = true;
    if (num1 == 0)
    {
      this.panel.summaryNoReiforcementContainer.gameObject.SetActive(true);
      this.panel.summaryNoReinforceCount.text = string.Format("0 / {0}", (object) Utils.ConvertNumberToNormalString(ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) data1.Benefit_Value_1, "calc_city_reinforcement_capacity")));
      this.panel.summaryReiforcementContainer.gameObject.SetActive(false);
    }
    else
    {
      this.panel.summaryNoReiforcementContainer.gameObject.SetActive(false);
      this.panel.summaryReiforcementContainer.gameObject.SetActive(true);
    }
  }

  private void SelectSummeryScrollResetStoped()
  {
    this.panel.summaryTabel.repositionNow = true;
    if (!this.summeryScrollStoppedFlag)
      return;
    this.panel.specificScorllView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.SelectSummeryScrollResetStoped);
    this.summeryScrollStoppedFlag = false;
  }

  private void OnExpandMarkClick(bool markExpanded)
  {
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      this.panel.specificToopsTable.repositionNow = true;
      this.panel.specificToopsTable.Reposition();
    }));
  }

  public void freshInfo()
  {
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    for (int index = 0; index < this.panel.specificToopsTable.transform.childCount; ++index)
    {
      this.panel.specificToopsTable.transform.GetChild(index).gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.panel.specificToopsTable.transform.GetChild(index).gameObject);
    }
    int num = (int) ((double) this.panel.orgEmbassySlotBar.GetComponent<UIWidget>().height * 0.5);
    for (int index = 0; index < targetListByType.Count; ++index)
    {
      if (DBManager.inst.DB_March.Get(targetListByType[index]).state == MarchData.MarchState.reinforcing)
      {
        GameObject gameObject = NGUITools.AddChild(this.panel.specificToopsTable.gameObject, this.panel.orgEmbassySlotBar.gameObject);
        gameObject.name = "slot_" + (object) (1000 + index);
        gameObject.transform.localPosition = new Vector3(0.0f, (float) ((index * 2 - 1) * num), 0.0f);
        gameObject.SetActive(true);
        EmbassySlotBar component = gameObject.GetComponent<EmbassySlotBar>();
        component.onSendTroopsHomeClick += new System.Action<long>(this.SendTroopHome);
        component.onExpandMarkClick += new System.Action<bool>(this.OnExpandMarkClick);
        component.Setup(index + 1, targetListByType[index]);
      }
    }
    Utils.ExecuteInSecs(0.05f, (System.Action) (() =>
    {
      this.panel.specificToopsTable.repositionNow = true;
      this.panel.specificToopsTable.Reposition();
      this.panel.specificScorllView.ResetPosition();
    }));
  }

  public void SelectSpecific()
  {
    this.curSelectedType = PopupEmbassyDlg.SelectType.specific;
    this.panel.tagContainer.gameObject.SetActive(true);
    this.panel.TAB_summary.gameObject.SetActive(false);
    this.panel.TAB_specific.gameObject.SetActive(true);
    this.panel.summaryContainer.gameObject.SetActive(false);
    this.panel.specificContainer.gameObject.SetActive(true);
    this.panel.JoinAllianceContainer.gameObject.SetActive(false);
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.freshInfo();
    }), true);
  }

  public void SelectJoinAlliance()
  {
    this.curSelectedType = PopupEmbassyDlg.SelectType.specific;
    this.panel.tagContainer.gameObject.SetActive(false);
    this.panel.summaryContainer.gameObject.SetActive(false);
    this.panel.specificContainer.gameObject.SetActive(true);
    this.panel.JoinAllianceContainer.gameObject.SetActive(true);
  }

  public void OnJoinAllianceClick()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnHelpClick()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_dragon_skill"
    });
  }

  public void SendTroopHome(long marchUid)
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      title = Utils.XLAT("id_uppercase_confirm"),
      description = Utils.XLAT("embassy_reinforcement_send_back_description"),
      leftButtonText = Utils.XLAT("id_uppercase_no"),
      leftButtonClickEvent = (System.Action) null,
      rightButtonText = Utils.XLAT("id_uppercase_yes"),
      rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("City:sendEmbassyTroopsBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId, (object) "back_uid", (object) marchUid), (System.Action<bool, object>) null, true))
    });
  }

  public void SendAllTroopHome()
  {
    UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
    {
      setType = (DoubleButtonPopup.SetType.title | DoubleButtonPopup.SetType.description | DoubleButtonPopup.SetType.leftButtonEvent | DoubleButtonPopup.SetType.leftButtonText | DoubleButtonPopup.SetType.rightButtonEvent | DoubleButtonPopup.SetType.rightButtonText),
      title = Utils.XLAT("id_uppercase_confirm"),
      description = Utils.XLAT("embassy_reinforcement_send_back_description"),
      leftButtonText = Utils.XLAT("id_uppercase_no"),
      leftButtonClickEvent = (System.Action) null,
      rightButtonText = Utils.XLAT("id_uppercase_yes"),
      rightButtonClickEvent = (System.Action) (() => MessageHub.inst.GetPortByAction("City:sendEmbassyAllTroopsBack").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "city_id", (object) PlayerData.inst.cityId), (System.Action<bool, object>) null, true))
    });
  }

  public void OnTagSummaryClick()
  {
    this.SelectSummary();
  }

  public void OnTagSpecificClick()
  {
    this.SelectSpecific();
  }

  public void ClickBackButton()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void ClickCloseButton()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnSendAllTroopsHomeClick()
  {
    this.SendAllTroopHome();
  }

  public void OnMarchDataChanged(long marchId)
  {
    switch (this.curSelectedType)
    {
      case PopupEmbassyDlg.SelectType.summary:
        this.SelectSummary();
        break;
      case PopupEmbassyDlg.SelectType.specific:
        this.SelectSpecific();
        break;
    }
  }

  public void OnEnable()
  {
    DBManager.inst.DB_March.onDataChanged += new System.Action<long>(this.OnMarchDataChanged);
  }

  public void OnDisable()
  {
    if (!GameEngine.IsAvailable || !GameEngine.IsReady() || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_March.onDataChanged -= new System.Action<long>(this.OnMarchDataChanged);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (PlayerData.inst.allianceId == 0L)
    {
      this.SelectJoinAlliance();
    }
    else
    {
      this.SelectSummary();
      Hashtable postData = new Hashtable();
      postData[(object) "uid"] = (object) PlayerData.inst.uid;
      postData[(object) "opp_uid"] = (object) PlayerData.inst.uid;
      MessageHub.inst.GetPortByAction("PVP:getReinforceList").SendRequest(postData, (System.Action<bool, object>) null, true);
    }
  }

  protected enum SelectType
  {
    summary,
    specific,
    joinAlliance,
  }

  [Serializable]
  protected class Panel
  {
    public const string EMBASSY_ID_PRE = "embassy_";
    [HideInInspector]
    public bool moreInformationFlag;
    public Transform summaryContainer;
    public UIScrollView summaryScrollView;
    public UITable summaryTabel;
    public UILabel summaryTroopsTotalCount;
    public EmbassySlotBarDetail orgSummaryEmbassySlot;
    public Transform summaryNoReiforcementContainer;
    public UILabel summaryNoReinforceCount;
    public Transform summaryReiforcementContainer;
    public Transform specificContainer;
    public Transform specificTroopsContainer;
    public UIScrollView specificScorllView;
    public UITable specificToopsTable;
    public EmbassySlotBar orgEmbassySlotBar;
    public Transform tagContainer;
    public Transform TAB_summary;
    public Transform TAB_specific;
    public Transform JoinAllianceContainer;
  }
}
