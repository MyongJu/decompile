﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("LZMA#")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Igor Pavlov")]
[assembly: AssemblyProduct("LZMA# SDK")]
[assembly: AssemblyCopyright("Copyright @ Igor Pavlov 1999-2004")]
[assembly: AssemblyTrademark("")]
[assembly: Extension]
[assembly: InternalsVisibleTo("Facebook.Unity.Tests")]
