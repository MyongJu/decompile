﻿// Decompiled with JetBrains decompiler
// Type: CityBuildingDims
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class CityBuildingDims
{
  private static Hashtable dimx = new Hashtable();
  private static Hashtable dimy = new Hashtable();
  private static bool _inited = false;

  private static void init()
  {
    CityBuildingDims._inited = true;
    CityBuildingDims.dimx.Add((object) "altar_l1", (object) 312);
    CityBuildingDims.dimy.Add((object) "altar_l1", (object) 265);
    CityBuildingDims.dimx.Add((object) "barracks_l1", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l1", (object) 220);
    CityBuildingDims.dimx.Add((object) "barracks_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l2", (object) 225);
    CityBuildingDims.dimx.Add((object) "barracks_l3", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l3", (object) 263);
    CityBuildingDims.dimx.Add((object) "barracks_l4", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l4", (object) 263);
    CityBuildingDims.dimx.Add((object) "barracks_l5", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l5", (object) 263);
    CityBuildingDims.dimx.Add((object) "barracks_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l6", (object) 276);
    CityBuildingDims.dimx.Add((object) "barracks_l7", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l7", (object) 276);
    CityBuildingDims.dimx.Add((object) "barracks_l8", (object) 361);
    CityBuildingDims.dimy.Add((object) "barracks_l8", (object) 276);
    CityBuildingDims.dimx.Add((object) "embassy_l1", (object) 329);
    CityBuildingDims.dimy.Add((object) "embassy_l1", (object) 230);
    CityBuildingDims.dimx.Add((object) "embassy_l2", (object) 345);
    CityBuildingDims.dimy.Add((object) "embassy_l2", (object) 230);
    CityBuildingDims.dimx.Add((object) "embassy_l3", (object) 344);
    CityBuildingDims.dimy.Add((object) "embassy_l3", (object) 246);
    CityBuildingDims.dimx.Add((object) "embassy_l4", (object) 345);
    CityBuildingDims.dimy.Add((object) "embassy_l4", (object) 249);
    CityBuildingDims.dimx.Add((object) "embassy_l5", (object) 343);
    CityBuildingDims.dimy.Add((object) "embassy_l5", (object) 262);
    CityBuildingDims.dimx.Add((object) "embassy_l6", (object) 352);
    CityBuildingDims.dimy.Add((object) "embassy_l6", (object) 262);
    CityBuildingDims.dimx.Add((object) "embassy_l7", (object) 352);
    CityBuildingDims.dimy.Add((object) "embassy_l7", (object) 270);
    CityBuildingDims.dimx.Add((object) "embassy_l8", (object) 353);
    CityBuildingDims.dimy.Add((object) "embassy_l8", (object) 290);
    CityBuildingDims.dimx.Add((object) "farm_l1", (object) 339);
    CityBuildingDims.dimy.Add((object) "farm_l1", (object) 254);
    CityBuildingDims.dimx.Add((object) "farm_l2", (object) 373);
    CityBuildingDims.dimy.Add((object) "farm_l2", (object) 258);
    CityBuildingDims.dimx.Add((object) "farm_l3", (object) 373);
    CityBuildingDims.dimy.Add((object) "farm_l3", (object) 277);
    CityBuildingDims.dimx.Add((object) "farm_l4", (object) 374);
    CityBuildingDims.dimy.Add((object) "farm_l4", (object) 293);
    CityBuildingDims.dimx.Add((object) "farm_l5", (object) 375);
    CityBuildingDims.dimy.Add((object) "farm_l5", (object) 295);
    CityBuildingDims.dimx.Add((object) "farm_l6", (object) 375);
    CityBuildingDims.dimy.Add((object) "farm_l6", (object) 295);
    CityBuildingDims.dimx.Add((object) "farm_l7", (object) 375);
    CityBuildingDims.dimy.Add((object) "farm_l7", (object) 306);
    CityBuildingDims.dimx.Add((object) "farm_l8", (object) 375);
    CityBuildingDims.dimy.Add((object) "farm_l8", (object) 317);
    CityBuildingDims.dimx.Add((object) "forge_l1", (object) 307);
    CityBuildingDims.dimy.Add((object) "forge_l1", (object) 254);
    CityBuildingDims.dimx.Add((object) "forge_l2", (object) 309);
    CityBuildingDims.dimy.Add((object) "forge_l2", (object) (int) byte.MaxValue);
    CityBuildingDims.dimx.Add((object) "forge_l3", (object) 309);
    CityBuildingDims.dimy.Add((object) "forge_l3", (object) 257);
    CityBuildingDims.dimx.Add((object) "forge_l4", (object) 309);
    CityBuildingDims.dimy.Add((object) "forge_l4", (object) 257);
    CityBuildingDims.dimx.Add((object) "forge_l5", (object) 309);
    CityBuildingDims.dimy.Add((object) "forge_l5", (object) 267);
    CityBuildingDims.dimx.Add((object) "forge_l6", (object) 329);
    CityBuildingDims.dimy.Add((object) "forge_l6", (object) 285);
    CityBuildingDims.dimx.Add((object) "forge_l7", (object) 329);
    CityBuildingDims.dimy.Add((object) "forge_l7", (object) 286);
    CityBuildingDims.dimx.Add((object) "forge_l8", (object) 329);
    CityBuildingDims.dimy.Add((object) "forge_l8", (object) 285);
    CityBuildingDims.dimx.Add((object) "hero_tower_l1", (object) 323);
    CityBuildingDims.dimy.Add((object) "hero_tower_l1", (object) 187);
    CityBuildingDims.dimx.Add((object) "hero_tower_l2", (object) 326);
    CityBuildingDims.dimy.Add((object) "hero_tower_l2", (object) 221);
    CityBuildingDims.dimx.Add((object) "hero_tower_l3", (object) 334);
    CityBuildingDims.dimy.Add((object) "hero_tower_l3", (object) 274);
    CityBuildingDims.dimx.Add((object) "hero_tower_l4", (object) 351);
    CityBuildingDims.dimy.Add((object) "hero_tower_l4", (object) 285);
    CityBuildingDims.dimx.Add((object) "hero_tower_l5", (object) 349);
    CityBuildingDims.dimy.Add((object) "hero_tower_l5", (object) 303);
    CityBuildingDims.dimx.Add((object) "hero_tower_l6", (object) 357);
    CityBuildingDims.dimy.Add((object) "hero_tower_l6", (object) 337);
    CityBuildingDims.dimx.Add((object) "hero_tower_l7", (object) 360);
    CityBuildingDims.dimy.Add((object) "hero_tower_l7", (object) 335);
    CityBuildingDims.dimx.Add((object) "hero_tower_l8", (object) 359);
    CityBuildingDims.dimy.Add((object) "hero_tower_l8", (object) 334);
    CityBuildingDims.dimx.Add((object) "hospital_l1", (object) 317);
    CityBuildingDims.dimy.Add((object) "hospital_l1", (object) 250);
    CityBuildingDims.dimx.Add((object) "hospital_l2", (object) 352);
    CityBuildingDims.dimy.Add((object) "hospital_l2", (object) 259);
    CityBuildingDims.dimx.Add((object) "hospital_l3", (object) 376);
    CityBuildingDims.dimy.Add((object) "hospital_l3", (object) 261);
    CityBuildingDims.dimx.Add((object) "hospital_l4", (object) 384);
    CityBuildingDims.dimy.Add((object) "hospital_l4", (object) 262);
    CityBuildingDims.dimx.Add((object) "hospital_l5", (object) 384);
    CityBuildingDims.dimy.Add((object) "hospital_l5", (object) 262);
    CityBuildingDims.dimx.Add((object) "hospital_l6", (object) 384);
    CityBuildingDims.dimy.Add((object) "hospital_l6", (object) 295);
    CityBuildingDims.dimx.Add((object) "hospital_l7", (object) 384);
    CityBuildingDims.dimy.Add((object) "hospital_l7", (object) 311);
    CityBuildingDims.dimx.Add((object) "hospital_l8", (object) 384);
    CityBuildingDims.dimy.Add((object) "hospital_l8", (object) 311);
    CityBuildingDims.dimx.Add((object) "house_l1", (object) 330);
    CityBuildingDims.dimy.Add((object) "house_l1", (object) 249);
    CityBuildingDims.dimx.Add((object) "house_l2", (object) 365);
    CityBuildingDims.dimy.Add((object) "house_l2", (object) 250);
    CityBuildingDims.dimx.Add((object) "house_l3", (object) 357);
    CityBuildingDims.dimy.Add((object) "house_l3", (object) 250);
    CityBuildingDims.dimx.Add((object) "house_l4", (object) 358);
    CityBuildingDims.dimy.Add((object) "house_l4", (object) 259);
    CityBuildingDims.dimx.Add((object) "house_l5", (object) 358);
    CityBuildingDims.dimy.Add((object) "house_l5", (object) 254);
    CityBuildingDims.dimx.Add((object) "house_l6", (object) 366);
    CityBuildingDims.dimy.Add((object) "house_l6", (object) (int) byte.MaxValue);
    CityBuildingDims.dimx.Add((object) "house_l7", (object) 365);
    CityBuildingDims.dimy.Add((object) "house_l7", (object) 277);
    CityBuildingDims.dimx.Add((object) "house_l8", (object) 378);
    CityBuildingDims.dimy.Add((object) "house_l8", (object) 295);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l1", (object) 378);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l1", (object) 284);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l2", (object) 378);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l2", (object) 284);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l3", (object) 378);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l3", (object) 284);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l4", (object) 378);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l4", (object) 284);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l5", (object) 372);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l5", (object) 283);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l6", (object) 375);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l6", (object) 283);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l7", (object) 382);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l7", (object) 292);
    CityBuildingDims.dimx.Add((object) "lumber_mill_l8", (object) 383);
    CityBuildingDims.dimy.Add((object) "lumber_mill_l8", (object) 295);
    CityBuildingDims.dimx.Add((object) "marketplace_l1", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l1", (object) 239);
    CityBuildingDims.dimx.Add((object) "marketplace_l2", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l2", (object) 260);
    CityBuildingDims.dimx.Add((object) "marketplace_l3", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l3", (object) 269);
    CityBuildingDims.dimx.Add((object) "marketplace_l4", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l4", (object) 290);
    CityBuildingDims.dimx.Add((object) "marketplace_l5", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l5", (object) 290);
    CityBuildingDims.dimx.Add((object) "marketplace_l6", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l6", (object) 304);
    CityBuildingDims.dimx.Add((object) "marketplace_l7", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l7", (object) 332);
    CityBuildingDims.dimx.Add((object) "marketplace_l8", (object) 351);
    CityBuildingDims.dimy.Add((object) "marketplace_l8", (object) 335);
    CityBuildingDims.dimx.Add((object) "mine_l1", (object) 328);
    CityBuildingDims.dimy.Add((object) "mine_l1", (object) 214);
    CityBuildingDims.dimx.Add((object) "mine_l2", (object) 347);
    CityBuildingDims.dimy.Add((object) "mine_l2", (object) 214);
    CityBuildingDims.dimx.Add((object) "mine_l3", (object) 347);
    CityBuildingDims.dimy.Add((object) "mine_l3", (object) 223);
    CityBuildingDims.dimx.Add((object) "mine_l4", (object) 347);
    CityBuildingDims.dimy.Add((object) "mine_l4", (object) 223);
    CityBuildingDims.dimx.Add((object) "mine_l5", (object) 347);
    CityBuildingDims.dimy.Add((object) "mine_l5", (object) 243);
    CityBuildingDims.dimx.Add((object) "mine_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "mine_l6", (object) 277);
    CityBuildingDims.dimx.Add((object) "mine_l7", (object) 347);
    CityBuildingDims.dimy.Add((object) "mine_l7", (object) 277);
    CityBuildingDims.dimx.Add((object) "mine_l8", (object) 350);
    CityBuildingDims.dimy.Add((object) "mine_l8", (object) 277);
    CityBuildingDims.dimx.Add((object) "prison_l1", (object) 338);
    CityBuildingDims.dimy.Add((object) "prison_l1", (object) 289);
    CityBuildingDims.dimx.Add((object) "prison_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "prison_l2", (object) 290);
    CityBuildingDims.dimx.Add((object) "prison_l3", (object) 360);
    CityBuildingDims.dimy.Add((object) "prison_l3", (object) 291);
    CityBuildingDims.dimx.Add((object) "prison_l4", (object) 365);
    CityBuildingDims.dimy.Add((object) "prison_l4", (object) 290);
    CityBuildingDims.dimx.Add((object) "prison_l5", (object) 385);
    CityBuildingDims.dimy.Add((object) "prison_l5", (object) 298);
    CityBuildingDims.dimx.Add((object) "prison_l6", (object) 393);
    CityBuildingDims.dimy.Add((object) "prison_l6", (object) 296);
    CityBuildingDims.dimx.Add((object) "prison_l7", (object) 391);
    CityBuildingDims.dimy.Add((object) "prison_l7", (object) 296);
    CityBuildingDims.dimx.Add((object) "prison_l8", (object) 393);
    CityBuildingDims.dimy.Add((object) "prison_l8", (object) 299);
    CityBuildingDims.dimx.Add((object) "pve_portal_l1", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l1", (object) 247);
    CityBuildingDims.dimx.Add((object) "pve_portal_l2", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l2", (object) 278);
    CityBuildingDims.dimx.Add((object) "pve_portal_l3", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l3", (object) 282);
    CityBuildingDims.dimx.Add((object) "pve_portal_l4", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l4", (object) 287);
    CityBuildingDims.dimx.Add((object) "pve_portal_l5", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l5", (object) 309);
    CityBuildingDims.dimx.Add((object) "pve_portal_l6", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l6", (object) 327);
    CityBuildingDims.dimx.Add((object) "pve_portal_l7", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l7", (object) 336);
    CityBuildingDims.dimx.Add((object) "pve_portal_l8", (object) 268);
    CityBuildingDims.dimy.Add((object) "pve_portal_l8", (object) 362);
    CityBuildingDims.dimx.Add((object) "range_l1", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l1", (object) 220);
    CityBuildingDims.dimx.Add((object) "range_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l2", (object) 225);
    CityBuildingDims.dimx.Add((object) "range_l3", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l3", (object) 263);
    CityBuildingDims.dimx.Add((object) "range_l4", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l4", (object) 263);
    CityBuildingDims.dimx.Add((object) "range_l5", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l5", (object) 263);
    CityBuildingDims.dimx.Add((object) "range_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l6", (object) 276);
    CityBuildingDims.dimx.Add((object) "range_l7", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l7", (object) 276);
    CityBuildingDims.dimx.Add((object) "range_l8", (object) 361);
    CityBuildingDims.dimy.Add((object) "range_l8", (object) 276);
    CityBuildingDims.dimx.Add((object) "sanctum_l1", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l1", (object) 220);
    CityBuildingDims.dimx.Add((object) "sanctum_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l2", (object) 225);
    CityBuildingDims.dimx.Add((object) "sanctum_l3", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l3", (object) 263);
    CityBuildingDims.dimx.Add((object) "sanctum_l4", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l4", (object) 263);
    CityBuildingDims.dimx.Add((object) "sanctum_l5", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l5", (object) 263);
    CityBuildingDims.dimx.Add((object) "sanctum_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l6", (object) 276);
    CityBuildingDims.dimx.Add((object) "sanctum_l7", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l7", (object) 276);
    CityBuildingDims.dimx.Add((object) "sanctum_l8", (object) 361);
    CityBuildingDims.dimy.Add((object) "sanctum_l8", (object) 276);
    CityBuildingDims.dimx.Add((object) "stables_l1", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l1", (object) 220);
    CityBuildingDims.dimx.Add((object) "stables_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l2", (object) 225);
    CityBuildingDims.dimx.Add((object) "stables_l3", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l3", (object) 263);
    CityBuildingDims.dimx.Add((object) "stables_l4", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l4", (object) 263);
    CityBuildingDims.dimx.Add((object) "stables_l5", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l5", (object) 263);
    CityBuildingDims.dimx.Add((object) "stables_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l6", (object) 276);
    CityBuildingDims.dimx.Add((object) "stables_l7", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l7", (object) 276);
    CityBuildingDims.dimx.Add((object) "stables_l8", (object) 361);
    CityBuildingDims.dimy.Add((object) "stables_l8", (object) 276);
    CityBuildingDims.dimx.Add((object) "storehouse_l1", (object) 354);
    CityBuildingDims.dimy.Add((object) "storehouse_l1", (object) 206);
    CityBuildingDims.dimx.Add((object) "storehouse_l2", (object) 349);
    CityBuildingDims.dimy.Add((object) "storehouse_l2", (object) 206);
    CityBuildingDims.dimx.Add((object) "storehouse_l3", (object) 350);
    CityBuildingDims.dimy.Add((object) "storehouse_l3", (object) 211);
    CityBuildingDims.dimx.Add((object) "storehouse_l4", (object) 347);
    CityBuildingDims.dimy.Add((object) "storehouse_l4", (object) 230);
    CityBuildingDims.dimx.Add((object) "storehouse_l5", (object) 347);
    CityBuildingDims.dimy.Add((object) "storehouse_l5", (object) 228);
    CityBuildingDims.dimx.Add((object) "storehouse_l6", (object) 345);
    CityBuildingDims.dimy.Add((object) "storehouse_l6", (object) 228);
    CityBuildingDims.dimx.Add((object) "storehouse_l7", (object) 345);
    CityBuildingDims.dimy.Add((object) "storehouse_l7", (object) 236);
    CityBuildingDims.dimx.Add((object) "storehouse_l8", (object) 345);
    CityBuildingDims.dimy.Add((object) "storehouse_l8", (object) 242);
    CityBuildingDims.dimx.Add((object) "stronghold_front", (object) 54);
    CityBuildingDims.dimy.Add((object) "stronghold_front", (object) 52);
    CityBuildingDims.dimx.Add((object) "stronghold_l1", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l1", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l2", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l2", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l3", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l3", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l4", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l4", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l5", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l5", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l6", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l6", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l7", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l7", (object) 786);
    CityBuildingDims.dimx.Add((object) "stronghold_l8", (object) 870);
    CityBuildingDims.dimy.Add((object) "stronghold_l8", (object) 786);
    CityBuildingDims.dimx.Add((object) "university_l1", (object) 321);
    CityBuildingDims.dimy.Add((object) "university_l1", (object) 224);
    CityBuildingDims.dimx.Add((object) "university_l2", (object) 320);
    CityBuildingDims.dimy.Add((object) "university_l2", (object) 251);
    CityBuildingDims.dimx.Add((object) "university_l3", (object) 337);
    CityBuildingDims.dimy.Add((object) "university_l3", (object) 250);
    CityBuildingDims.dimx.Add((object) "university_l4", (object) 340);
    CityBuildingDims.dimy.Add((object) "university_l4", (object) 252);
    CityBuildingDims.dimx.Add((object) "university_l5", (object) 348);
    CityBuildingDims.dimy.Add((object) "university_l5", (object) (int) byte.MaxValue);
    CityBuildingDims.dimx.Add((object) "university_l6", (object) 335);
    CityBuildingDims.dimy.Add((object) "university_l6", (object) 284);
    CityBuildingDims.dimx.Add((object) "university_l7", (object) 336);
    CityBuildingDims.dimy.Add((object) "university_l7", (object) 282);
    CityBuildingDims.dimx.Add((object) "university_l8", (object) 345);
    CityBuildingDims.dimy.Add((object) "university_l8", (object) 282);
    CityBuildingDims.dimx.Add((object) "walls_l1", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l1", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l2", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l2", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l3", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l3", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l4", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l4", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l5", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l5", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l6", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l6", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l7", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l7", (object) 480);
    CityBuildingDims.dimx.Add((object) "walls_l8", (object) 367);
    CityBuildingDims.dimy.Add((object) "walls_l8", (object) 480);
    CityBuildingDims.dimx.Add((object) "war_rally_l1", (object) 336);
    CityBuildingDims.dimy.Add((object) "war_rally_l1", (object) 231);
    CityBuildingDims.dimx.Add((object) "war_rally_l2", (object) 334);
    CityBuildingDims.dimy.Add((object) "war_rally_l2", (object) 245);
    CityBuildingDims.dimx.Add((object) "war_rally_l3", (object) 332);
    CityBuildingDims.dimy.Add((object) "war_rally_l3", (object) 253);
    CityBuildingDims.dimx.Add((object) "war_rally_l4", (object) 336);
    CityBuildingDims.dimy.Add((object) "war_rally_l4", (object) 286);
    CityBuildingDims.dimx.Add((object) "war_rally_l5", (object) 332);
    CityBuildingDims.dimy.Add((object) "war_rally_l5", (object) 285);
    CityBuildingDims.dimx.Add((object) "war_rally_l6", (object) 351);
    CityBuildingDims.dimy.Add((object) "war_rally_l6", (object) 324);
    CityBuildingDims.dimx.Add((object) "war_rally_l7", (object) 363);
    CityBuildingDims.dimy.Add((object) "war_rally_l7", (object) 326);
    CityBuildingDims.dimx.Add((object) "war_rally_l8", (object) 363);
    CityBuildingDims.dimy.Add((object) "war_rally_l8", (object) 328);
    CityBuildingDims.dimx.Add((object) "watchtower_l1", (object) 320);
    CityBuildingDims.dimy.Add((object) "watchtower_l1", (object) 260);
    CityBuildingDims.dimx.Add((object) "watchtower_l2", (object) 319);
    CityBuildingDims.dimy.Add((object) "watchtower_l2", (object) 271);
    CityBuildingDims.dimx.Add((object) "watchtower_l3", (object) 324);
    CityBuildingDims.dimy.Add((object) "watchtower_l3", (object) 291);
    CityBuildingDims.dimx.Add((object) "watchtower_l4", (object) 336);
    CityBuildingDims.dimy.Add((object) "watchtower_l4", (object) 311);
    CityBuildingDims.dimx.Add((object) "watchtower_l5", (object) 347);
    CityBuildingDims.dimy.Add((object) "watchtower_l5", (object) 331);
    CityBuildingDims.dimx.Add((object) "watchtower_l6", (object) 354);
    CityBuildingDims.dimy.Add((object) "watchtower_l6", (object) 359);
    CityBuildingDims.dimx.Add((object) "watchtower_l7", (object) 366);
    CityBuildingDims.dimy.Add((object) "watchtower_l7", (object) 367);
    CityBuildingDims.dimx.Add((object) "watchtower_l8", (object) 385);
    CityBuildingDims.dimy.Add((object) "watchtower_l8", (object) 372);
    CityBuildingDims.dimx.Add((object) "workshop_l1", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l1", (object) 220);
    CityBuildingDims.dimx.Add((object) "workshop_l2", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l2", (object) 225);
    CityBuildingDims.dimx.Add((object) "workshop_l3", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l3", (object) 263);
    CityBuildingDims.dimx.Add((object) "workshop_l4", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l4", (object) 263);
    CityBuildingDims.dimx.Add((object) "workshop_l5", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l5", (object) 263);
    CityBuildingDims.dimx.Add((object) "workshop_l6", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l6", (object) 276);
    CityBuildingDims.dimx.Add((object) "workshop_l7", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l7", (object) 276);
    CityBuildingDims.dimx.Add((object) "workshop_l8", (object) 361);
    CityBuildingDims.dimy.Add((object) "workshop_l8", (object) 276);
  }

  public static void Get(string filename, out int width, out int height)
  {
    if (!CityBuildingDims._inited)
      CityBuildingDims.init();
    if (CityBuildingDims.dimx.Contains((object) filename))
    {
      width = int.Parse(CityBuildingDims.dimx[(object) filename].ToString());
      height = int.Parse(CityBuildingDims.dimy[(object) filename].ToString());
    }
    else
    {
      width = 0;
      height = 0;
    }
  }
}
