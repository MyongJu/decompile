﻿// Decompiled with JetBrains decompiler
// Type: AllianceRecommendMailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class AllianceRecommendMailPopup : BaseReportPopup
{
  private AllianceSearchItemData _itemData = new AllianceSearchItemData();
  public UILabel allianceName;
  public UILabel allianceLanguage;
  public UILabel allianceMembers;
  public UILabel mailContent;
  public AllianceSymbol allianceSymbol;
  public UIButton allianceJoinButton;
  private Hashtable _paramData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    BaseReportPopup.Parameter parameter = orgParam as BaseReportPopup.Parameter;
    if (parameter != null && parameter.hashtable != null)
      this._paramData = parameter.hashtable[(object) "data"] as Hashtable;
    this.AddEventHandler();
    this.CheckJoinButtonStatus();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void OnJoinBtnPressed()
  {
    MessageHub.inst.GetPortByAction("Alliance:getAllianceDetailInfo").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) this._itemData.allianceID
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.SendJoinRequest();
    }), true);
  }

  private void SendJoinRequest()
  {
    AllianceManager.Instance.JoinAlliance(this._itemData.allianceID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_join"), new Dictionary<string, string>()
      {
        {
          "Alliance_Name",
          DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).allianceName
        }
      }));
    }));
  }

  private void UpdateUI()
  {
    if (this._paramData == null || this._paramData[(object) "alliance_data"] == null || this._itemData == null)
      return;
    this._itemData.Decode(this._paramData[(object) "alliance_data"]);
    this.allianceName.text = string.Format("[{0}]{1}", (object) this._itemData.tag, (object) this._itemData.name);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para.Add("0", Language.Instance.GetLanguageName(this._itemData.language));
    this.allianceLanguage.text = ScriptLocalization.GetWithPara("id_language_lang", para, true);
    this.allianceMembers.text = string.Format("{0} ", (object) Utils.XLAT("alliance_members")) + string.Format("{0}/{1}", (object) this._itemData.memberCount, (object) this._itemData.memberLimit);
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("join_alliance_reward_gold");
    if (data != null)
    {
      para.Clear();
      para.Add("0", data.ValueString);
      this.mailContent.text = ScriptLocalization.GetWithPara("mail_subject_recommend_alliance", para, true);
    }
    this.allianceSymbol.SetSymbols(this._itemData.symbolCode);
  }

  private void OnAllianceDateUpdate(long allianceId)
  {
    if (allianceId == PlayerData.inst.allianceId)
      this.allianceJoinButton.isEnabled = false;
    this.UpdateUI();
  }

  private void CheckJoinButtonStatus()
  {
    this.allianceJoinButton.isEnabled = PlayerData.inst.allianceId <= 0L;
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Alliance.onDataUpdate += new System.Action<long>(this.OnAllianceDateUpdate);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Alliance.onDataUpdate -= new System.Action<long>(this.OnAllianceDateUpdate);
  }
}
