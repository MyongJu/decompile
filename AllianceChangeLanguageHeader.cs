﻿// Decompiled with JetBrains decompiler
// Type: AllianceChangeLanguageHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceChangeLanguageHeader : AllianceCustomizeHeader
{
  private List<UIToggle> m_Languages = new List<UIToggle>();
  public GameObject m_ItemPrefab;
  public UIGrid m_Grid;
  public UITable m_Table;
  private int m_LanguageIndex;
  private bool m_Init;

  protected override void UpdatePanel()
  {
    base.UpdatePanel();
    this.UpdateLanguages();
  }

  private void UpdateLanguages()
  {
    if (!this.m_Init)
    {
      this.m_Init = true;
      List<string> communication = Language.Instance.Communication;
      for (int index = 0; index < communication.Count; ++index)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
        gameObject.SetActive(true);
        gameObject.transform.parent = this.m_Grid.transform;
        gameObject.transform.localScale = Vector3.one;
        gameObject.GetComponent<AllianceLanguageRenderer>().Set(communication[index]);
        this.m_Languages.Add(gameObject.GetComponent<UIToggle>());
      }
    }
    this.m_Grid.Reposition();
    this.m_Table.onReposition = new UITable.OnReposition(this.OnReposition);
    this.m_Table.Reposition();
  }

  private void OnReposition()
  {
    this.FireChangedEvent();
  }

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.SetLanguage(this.allianceData.language);
    this.m_LanguageIndex = this.GetLanguageIndex();
  }

  public void OnToggleChanged()
  {
    int languageIndex = this.GetLanguageIndex();
    if (this.m_LanguageIndex == languageIndex || languageIndex == -1)
      return;
    this.m_LanguageIndex = languageIndex;
    AllianceManager.Instance.SetupAllianceInfo(this.GetLanguage(this.m_LanguageIndex), this.allianceData.publicMessage, this.allianceData.isPirvate, new System.Action<bool, object>(this.SetupAllianceCallback));
  }

  private void SetupAllianceCallback(bool ret, object data)
  {
    if (!ret)
      return;
    UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_language_changed"), (System.Action) null, 4f, false);
  }

  private void SetLanguage(string locale)
  {
    for (int index = 0; index < this.m_Languages.Count; ++index)
      this.m_Languages[index].value = this.m_Languages[index].GetComponent<AllianceLanguageRenderer>().m_Locale == locale;
  }

  private string GetLanguage(int index)
  {
    return this.m_Languages[index].GetComponent<AllianceLanguageRenderer>().m_Locale;
  }

  private int GetLanguageIndex()
  {
    for (int index = 0; index < this.m_Languages.Count; ++index)
    {
      if (this.m_Languages[index].value)
        return index;
    }
    return -1;
  }
}
