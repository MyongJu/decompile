﻿// Decompiled with JetBrains decompiler
// Type: GuardStateEnhanceEvil
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class GuardStateEnhanceEvil : AbstractGuardState
{
  public GuardStateEnhanceEvil(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_enhance";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
  }

  public override void OnProcess()
  {
    if (this.Player.Animation.isPlaying)
      return;
    this.Player.StateMacine.SetState("guard_idle", (Hashtable) null);
    BattleManager.Instance.SendPacket((DragonKnightPacket) new DragonKnightPacketFinish(this.Player.PlayerId));
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
  }
}
