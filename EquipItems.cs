﻿// Decompiled with JetBrains decompiler
// Type: EquipItems
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Examples/Equip Items")]
public class EquipItems : MonoBehaviour
{
  public int[] itemIDs;

  private void Start()
  {
    if (this.itemIDs != null && this.itemIDs.Length > 0)
    {
      InvEquipment invEquipment = this.GetComponent<InvEquipment>();
      if ((Object) invEquipment == (Object) null)
        invEquipment = this.gameObject.AddComponent<InvEquipment>();
      int max = 12;
      int index = 0;
      for (int length = this.itemIDs.Length; index < length; ++index)
      {
        int itemId = this.itemIDs[index];
        InvBaseItem byId = InvDatabase.FindByID(itemId);
        if (byId != null)
          invEquipment.Equip(new InvGameItem(itemId, byId)
          {
            quality = (InvGameItem.Quality) Random.Range(0, max),
            itemLevel = NGUITools.RandomRange(byId.minItemLevel, byId.maxItemLevel)
          });
        else
          Debug.LogWarning((object) ("Can't resolve the item ID of " + (object) itemId));
      }
    }
    Object.Destroy((Object) this);
  }
}
