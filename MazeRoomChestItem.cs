﻿// Decompiled with JetBrains decompiler
// Type: MazeRoomChestItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class MazeRoomChestItem : MonoBehaviour, IRoomEventProcessor
{
  [SerializeField]
  protected float _chestOpenAnimationTime = 0.6f;
  [SerializeField]
  protected Transform _chestPositionLeft;
  [SerializeField]
  protected Transform _chestPositionCenter;
  protected GameObject _gameObjectChest;
  protected Room _currentRoom;
  private bool _animationPlaying;

  public void SetEnabled(bool enabled)
  {
    this.gameObject.SetActive(enabled);
  }

  protected void SetChestInfo(Room room)
  {
    this._animationPlaying = false;
    this._currentRoom = room;
    if ((bool) ((UnityEngine.Object) this._gameObjectChest))
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._gameObjectChest);
      this._gameObjectChest = (GameObject) null;
    }
    if (this._currentRoom.AllChest.Count <= 0)
      return;
    RoomChest roomChest = this._currentRoom.AllChest[0];
    DungeonChestStaticInfo data = ConfigManager.inst.DB_DungeonChest.GetData(roomChest.chestId);
    if (data != null)
    {
      UnityEngine.Object original = AssetManager.Instance.Load(data.ImageFullPath, (System.Type) null);
      if ((bool) original)
      {
        Transform parent = !this._currentRoom.IsExit ? this._chestPositionCenter : this._chestPositionLeft;
        this._gameObjectChest = UnityEngine.Object.Instantiate(original) as GameObject;
        this._gameObjectChest.transform.SetParent(parent);
        this._gameObjectChest.transform.localScale = Vector3.one;
        this._gameObjectChest.transform.localPosition = Vector3.zero;
        UIButton component1 = this._gameObjectChest.GetComponent<UIButton>();
        if ((bool) ((UnityEngine.Object) component1))
          component1.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnClicked)));
        Animator component2 = this._gameObjectChest.GetComponent<Animator>();
        if ((bool) ((UnityEngine.Object) component2))
          component2.enabled = false;
        AssetManager.Instance.UnLoadAsset(data.ImageFullPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }
      else
        D.error((object) ("can not find chest prefab : " + data.ImageFullPath));
    }
    else
      D.error((object) ("can not find chest config : " + (object) roomChest.chestId));
  }

  public void OnClicked()
  {
    if (this._animationPlaying || this._currentRoom.AllChest.Count <= 0)
      return;
    this.PlayOpenAnimation();
  }

  protected void PlayOpenAnimation()
  {
    this._animationPlaying = true;
    Animator component = this._gameObjectChest.GetComponent<Animator>();
    if ((bool) ((UnityEngine.Object) component))
      component.enabled = true;
    this.StartCoroutine(this.DelayCallCoroutine(this._chestOpenAnimationTime, new System.Action(this.OnPlayOpenAnimationCompleted)));
    AudioManager.Instance.PlaySound("sfx_dragon_knight_open_chest", false);
  }

  [DebuggerHidden]
  protected IEnumerator DelayCallCoroutine(float delayTime, System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MazeRoomChestItem.\u003CDelayCallCoroutine\u003Ec__Iterator5A()
    {
      delayTime = delayTime,
      callback = callback,
      \u003C\u0024\u003EdelayTime = delayTime,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  protected void OnPlayOpenAnimationCompleted()
  {
    this._animationPlaying = false;
    this.SetChestInfo(this._currentRoom);
    UIManager.inst.OpenPopup("DragonKnight/DungeonOpenChestPopup", (Popup.PopupParameter) new DungeonOpenChestPopup.Parameter()
    {
      roomChest = this._currentRoom.AllChest[0]
    });
  }

  public bool IsSkipAble()
  {
    return true;
  }

  public void ProcessRoomEvent(Room room)
  {
    this._currentRoom = room;
    if (room.AllChest.Count > 0)
    {
      this.SetEnabled(true);
      this.SetChestInfo(room);
    }
    else
      D.error((object) "there is no buff in this room");
  }
}
