﻿// Decompiled with JetBrains decompiler
// Type: IAPSuccessPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class IAPSuccessPopup : BaseReportPopup
{
  private List<RewardRender> rewardsrenders = new List<RewardRender>();
  public UITable rewardsTable;
  public UITable rewardsContentTable;
  public UITable contentTable;
  public UITable messageTable;
  public GameObject rewardRender;
  public UILabel messageLabel;
  private IAPSuccessPopup.IAPSuccessData iapd;

  private void UpdateUI()
  {
    this.UpdateReward();
    this.UpdateMessage();
  }

  private void UpdateMessage()
  {
    this.messageLabel.text = this.maildata.GetBodyString();
  }

  private void UpdateReward()
  {
    this.Clean();
    this.rewardsContentTable.gameObject.SetActive(true);
    this.UpdateItems();
    this.UpdateGold();
    this.rewardsTable.repositionNow = true;
    this.contentTable.repositionNow = true;
  }

  private void UpdateItems()
  {
    using (Dictionary<string, int>.Enumerator enumerator = this.iapd.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(this.rewardsTable.gameObject, this.rewardRender);
        gameObject.SetActive(true);
        RewardRender component = gameObject.GetComponent<RewardRender>();
        component.Feed(new SystemMessage.ItemReward()
        {
          internalID = int.Parse(current.Key),
          amount = current.Value
        });
        this.rewardsrenders.Add(component);
      }
    }
  }

  private void UpdateGold()
  {
    if (this.iapd.gold_number == 0)
      return;
    GameObject gameObject = NGUITools.AddChild(this.rewardsTable.gameObject, this.rewardRender);
    gameObject.SetActive(true);
    RewardRender component = gameObject.GetComponent<RewardRender>();
    component.Feed(new SystemMessage.GoldReward()
    {
      amount = this.iapd.gold_number
    });
    this.rewardsrenders.Add(component);
  }

  private void Clean()
  {
    using (List<RewardRender>.Enumerator enumerator = this.rewardsrenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.rewardsrenders.Clear();
  }

  protected virtual void AddEventHandlers()
  {
    this.rewardsTable.onReposition += new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.onReposition += new UITable.OnReposition(this.HandleRewardsContentTableReposition);
  }

  private void RemoveEventHandlers()
  {
    this.rewardsTable.onReposition = (UITable.OnReposition) null;
  }

  protected void HandleBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  protected void HandleCloseBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  protected void HandleRewardsTableReposition()
  {
    this.rewardsTable.onReposition -= new UITable.OnReposition(this.HandleRewardsTableReposition);
    this.rewardsContentTable.repositionNow = true;
    this.messageTable.repositionNow = true;
  }

  protected void HandleRewardsContentTableReposition()
  {
    this.rewardsContentTable.onReposition -= new UITable.OnReposition(this.HandleRewardsContentTableReposition);
    Utils.ExecuteAtTheEndOfFrame(new System.Action(this.RepositionContentTable));
  }

  private void RepositionContentTable()
  {
    this.contentTable.repositionNow = true;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.iapd = JsonReader.Deserialize<IAPSuccessPopup.IAPSuccessData>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.UpdateUI();
    this.AddEventHandlers();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.OnClose(orgParam);
    this.RemoveEventHandlers();
    this.CancelInvoke();
  }

  public class IAPSuccessData
  {
    public Dictionary<string, int> items = new Dictionary<string, int>();
    public int gold_number;
  }
}
