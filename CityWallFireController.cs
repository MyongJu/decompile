﻿// Decompiled with JetBrains decompiler
// Type: CityWallFireController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class CityWallFireController : MonoBehaviour
{
  public GameObject m_FireEffect;
  public GameObject m_WallCrack;
  private GameObject m_FireEffectInstance;

  private void Update()
  {
    if (!this.CanDoAction())
      return;
    JobHandle singleJobByClass = JobManager.Instance.GetSingleJobByClass(JobEvent.JOB_WALL_ONFIRE);
    CityData playerCityData = PlayerData.inst.playerCityData;
    bool flag1 = (singleJobByClass == null ? 0 : singleJobByClass.EndTime()) - NetServerTime.inst.ServerTimestamp > 0;
    if (flag1)
    {
      if ((Object) this.m_FireEffectInstance == (Object) null)
      {
        this.m_FireEffectInstance = Object.Instantiate<GameObject>(this.m_FireEffect);
        this.m_FireEffectInstance.transform.parent = CitadelSystem.inst.gameObject.transform;
      }
    }
    else
    {
      Object.Destroy((Object) this.m_FireEffectInstance);
      this.m_FireEffectInstance = (GameObject) null;
    }
    UIManager.inst.publicHUD.mCityButtons.mBurningButton.SetActive(flag1);
    int num = singleJobByClass == null || singleJobByClass.IsFinished() ? 0 : NetServerTime.inst.ServerTimestamp - singleJobByClass.StartTime();
    double burningSpeedInSecond = AllianceBuildUtils.BurningSpeedInSecond;
    double wallDefenceLimit = Utils.GetWallDefenceLimit();
    bool flag2 = (double) playerCityData.defenseValue - burningSpeedInSecond * (double) num < wallDefenceLimit;
    this.m_WallCrack.SetActive(flag2);
    BuildingControllerWall buildingByType = CitadelSystem.inst.GetBuildingByType("walls") as BuildingControllerWall;
    if (!((Object) null != (Object) buildingByType))
      return;
    Transform crack = buildingByType.crack;
    if (!((Object) crack != (Object) null))
      return;
    crack.gameObject.SetActive(flag2);
  }

  public void OnWallCrackClick()
  {
    if (!this.CanDoAction())
      return;
    CitadelSystem.inst.OnWallDefensePressed();
  }

  private bool CanDoAction()
  {
    if (GameEngine.IsAvailable && !GameEngine.IsShuttingDown)
      return GameEngine.IsReady();
    return false;
  }
}
