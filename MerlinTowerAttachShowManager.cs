﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerAttachShowManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class MerlinTowerAttachShowManager : MonoBehaviour
{
  private const string ATTACK_VFX_PATH = "Prefab/MerlinTower/fx_crystalmonster_bingci_attack";
  private const string boardpath = "Prefab/AnniversaryAttackShow/MerlinTowerAttackShowBaseboard";
  private const string terrainpath = "Prefab/AnniversaryAttackShow/AttackShowTerrain";
  private const string monsterPath = "Prefab/AnniversaryAttackShow/AttackShowMonster";
  private static MerlinTowerAttachShowManager _instance;
  private bool isRunning;
  private MerlinTowerAttackShowPara _para;
  private GameObject monsterModel;
  private GameObject exploreFx;
  private bool isWin;
  private Coroutine _attackShowCoroutine;
  private UserMerlinTowerData towerData;
  public MerlinTowerAttackShowController board;
  private GameObject skipBtn;
  private AttackShowTargetMonster _attackShowTargetMonster;
  private ExpAnimationEventCallback _animationEventHandler;
  public System.Action onAttackShowTerminate;
  public System.Action onAttackShowBegin;

  public static MerlinTowerAttachShowManager Instance
  {
    get
    {
      if ((UnityEngine.Object) MerlinTowerAttachShowManager._instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject();
        MerlinTowerAttachShowManager._instance = gameObject.AddComponent<MerlinTowerAttachShowManager>();
        gameObject.AddComponent<DontDestroy>();
        gameObject.name = nameof (MerlinTowerAttachShowManager);
        if ((UnityEngine.Object) null == (UnityEngine.Object) MerlinTowerAttachShowManager._instance)
          throw new ArgumentException("MerlinTowerAttachShowManager hasn't been created yet.");
      }
      return MerlinTowerAttachShowManager._instance;
    }
  }

  public bool IsRunning
  {
    get
    {
      return this.isRunning;
    }
  }

  public MerlinTowerAttackShowPara Param
  {
    get
    {
      return this._para;
    }
  }

  public bool IsWin
  {
    get
    {
      return this.isWin;
    }
    set
    {
      this.isWin = value;
    }
  }

  public void Startup(MerlinTowerAttackShowPara para, UserMerlinTowerData data)
  {
    if (para == null)
      return;
    this._para = para;
    this.towerData = data;
    this.isRunning = true;
    this.PlayAttackShow();
  }

  public void Terminate()
  {
    this.isRunning = false;
    AudioManager.Instance.PlayBGM("bgm_city2");
    if ((UnityEngine.Object) null != (UnityEngine.Object) this._attackShowTargetMonster)
      this._attackShowTargetMonster.Dispose();
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.board)
    {
      this.board.Dispose();
      UnityEngine.Object.Destroy((UnityEngine.Object) this.board.gameObject);
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.skipBtn)
    {
      this.skipBtn.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.skipBtn);
    }
    if (this.onAttackShowTerminate == null)
      return;
    this.onAttackShowTerminate();
  }

  public void PlayAttackShow()
  {
    AssetManager.Instance.LoadAsync("Prefab/AnniversaryAttackShow/MerlinTowerAttackShowBaseboard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      GameObject go = UnityEngine.Object.Instantiate(obj) as GameObject;
      AssetManager.Instance.UnLoadAsset("Prefab/AnniversaryAttackShow/MerlinTowerAttackShowBaseboard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      this.board = go.GetComponent<MerlinTowerAttackShowController>();
      this.board.GetComponentInChildren<Camera>();
      this._attackShowCoroutine = Utils.ExecuteInSecs(this.board.battletime, (System.Action) (() =>
      {
        if (this.isWin)
        {
          this.PlayMonsterAnimation("Death");
          Utils.ExecuteInSecs(2f, (System.Action) (() => UIManager.inst.CloseTopPopup((Popup.PopupParameter) null)));
        }
        else
          UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
      }));
      if (SettingManager.Instance.IsEnvironmentOff)
        ;
      AssetManager.Instance.LoadAsync("Prefab/AnniversaryAttackShow/AttackShowMonster", (System.Action<UnityEngine.Object, bool>) ((objcity, retcity) =>
      {
        GameObject gameObject = Utils.DuplicateGOB(objcity as GameObject, go.transform);
        gameObject.transform.localScale = Vector3.one;
        this._attackShowTargetMonster = gameObject.GetComponentInChildren<AttackShowTargetMonster>();
        Utils.SetLayer(gameObject.GetComponentInChildren<Camera>().gameObject, LayerMask.NameToLayer("DragonBattleHUD"));
        this._attackShowTargetMonster.FeedData(this._para);
        string modelPath = this.towerData.CurrentTowerMainInfo.ModelPath;
        AssetManager.Instance.LoadAsync(modelPath, (System.Action<UnityEngine.Object, bool>) ((objMonster, retMonster) =>
        {
          this.monsterModel = Utils.DuplicateGOB(objMonster as GameObject, this._attackShowTargetMonster.monsterNode.transform);
          this.monsterModel.transform.localPosition = Vector3.zero;
          this.monsterModel.transform.localScale = Vector3.one;
          this.monsterModel.transform.localRotation = Quaternion.identity;
          this.exploreFx = this.AttachAttackVfx(this.monsterModel);
          this.PlayMonsterAnimation("Standby");
          AssetManager.Instance.UnLoadAsset(modelPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }), (System.Type) null);
        this.board.PlayAction(1);
        AssetManager.Instance.UnLoadAsset("Prefab/AnniversaryAttackShow/AttackShowMonster", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }), (System.Type) null);
      if (this.onAttackShowBegin == null)
        return;
      this.onAttackShowBegin();
    }), (System.Type) null);
  }

  private void AnimationEventCallback(AnimationEvent e)
  {
    if (!(e.animatorClipInfo.clip.name == "Attack"))
      return;
    this._animationEventHandler.gameObject.SetActive(true);
  }

  private GameObject AttachAttackVfx(GameObject parent)
  {
    GameObject original = AssetManager.Instance.HandyLoad("Prefab/MerlinTower/fx_crystalmonster_bingci_attack", (System.Type) null) as GameObject;
    if ((UnityEngine.Object) null == (UnityEngine.Object) original)
      return (GameObject) null;
    GameObject go = UnityEngine.Object.Instantiate<GameObject>(original);
    go.gameObject.SetActive(false);
    go.transform.parent = parent.transform;
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    go.transform.localRotation = Quaternion.identity;
    Utils.SetLayer(go, parent.layer);
    return go;
  }

  public void ShowExploreFx()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.exploreFx))
      return;
    this.exploreFx.SetActive(false);
    this.exploreFx.SetActive(true);
  }

  public void HideExploreFx()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.exploreFx))
      return;
    this.exploreFx.SetActive(false);
  }

  public void StopAttackShowCoroutine()
  {
    if (this._attackShowCoroutine == null)
      return;
    Utils.StopCoroutine(this._attackShowCoroutine);
    this._attackShowCoroutine = (Coroutine) null;
  }

  public void PlayMonsterAnimation(string clip)
  {
    UnityEngine.Animation componentInChildren = this.monsterModel.GetComponentInChildren<UnityEngine.Animation>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.Play(clip);
  }
}
