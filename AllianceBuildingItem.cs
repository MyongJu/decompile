﻿// Decompiled with JetBrains decompiler
// Type: AllianceBuildingItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceBuildingItem : MonoBehaviour
{
  private Dictionary<int, string> allIconpath = new Dictionary<int, string>()
  {
    {
      0,
      "Texture/Alliance/tiles_alliance_fortress"
    },
    {
      1,
      "Texture/Alliance/tiles_alliance_turret"
    },
    {
      2,
      "Texture/Alliance/tiles_alliance_storehouse"
    },
    {
      3,
      "Texture/Alliance/tiles_alliance_farm"
    },
    {
      4,
      "Texture/Alliance/tiles_alliance_sawmill"
    },
    {
      5,
      "Texture/Alliance/tiles_alliance_mine"
    },
    {
      6,
      "Texture/Alliance/tiles_alliance_house"
    },
    {
      7,
      "Texture/Alliance/tiles_alliance_portal"
    },
    {
      8,
      "Texture/Alliance/tiles_alliance_hospital"
    },
    {
      9,
      "Texture/Alliance/tiles_alliance_dragon_altar"
    }
  };
  public Icon icon;
  public UILabel stateLabel;
  public UILabel lockLabel;
  public UIButton button;
  public UILabel coordinatesLabel;
  public UILabel durability;
  public GameObject iconContainer;
  public UILabel fortressName;
  public GameObject durabilityRoot;
  public GameObject descRoot;
  public UILabel descLabel;
  public UILabel buttonLabel;
  private bool isInit;
  private int buildingConfigId;
  private AllianceCityDlg.Parameter m_Parameter;

  public Coordinate PlaceLocation
  {
    get
    {
      if (PlayerData.inst.allianceData != null && !PlayerData.inst.allianceData.FortressContainsCoordinate(PlayerData.inst.CityData.Location))
      {
        AllianceFortressData myMainFortress = DBManager.inst.DB_AllianceFortress.GetMyMainFortress();
        if (myMainFortress != null)
          return myMainFortress.Location;
      }
      return PlayerData.inst.CityData.Location;
    }
  }

  public int BuildingConfigId
  {
    get
    {
      return this.buildingConfigId;
    }
  }

  public void SetData(int buildingConfigId, AllianceCityDlg.Parameter parameter)
  {
    this.buildingConfigId = buildingConfigId;
    this.m_Parameter = parameter;
    this.Refresh();
    if (!AllianceBuildUtils.CheckBuildIsPlace(buildingConfigId))
      return;
    this.AddEventHandler();
  }

  public void Clear()
  {
    this.icon.Dispose();
    this.isInit = false;
    this.RemoveEventHandler();
  }

  private string getIconPathByAllianceBuildingType(int type)
  {
    string str = "Texture/Alliance/tiles_alliance_fortress";
    this.allIconpath.TryGetValue(type, out str);
    return str;
  }

  private void Refresh()
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.buildingConfigId);
    if (buildingStaticInfo == null)
      return;
    bool redColor = false;
    string buildStateDesc = AllianceBuildUtils.GetBuildStateDesc(this.buildingConfigId, ref redColor);
    NGUITools.SetActive(this.lockLabel.gameObject, redColor);
    NGUITools.SetActive(this.stateLabel.gameObject, !redColor);
    if (redColor)
      this.lockLabel.text = buildStateDesc;
    else
      this.stateLabel.text = buildStateDesc;
    this.coordinatesLabel.text = AllianceBuildUtils.GetBuildCoordinatesDesc(this.buildingConfigId);
    NGUITools.SetActive(this.coordinatesLabel.gameObject, AllianceBuildUtils.CheckBuildIsPlace(this.buildingConfigId));
    NGUITools.SetActive(this.button.gameObject, AllianceBuildUtils.CheckBuildCanBuild(this.buildingConfigId));
    this.buttonLabel.text = !AllianceBuildUtils.CheckIsClearAwway(this.buildingConfigId) ? Utils.XLAT("id_uppercase_place") : Utils.XLAT("alliance_building_uppercase_relocate");
    string buildDesc = AllianceBuildUtils.GetBuildDesc(this.buildingConfigId);
    if (!string.IsNullOrEmpty(buildDesc))
    {
      this.descRoot.SetActive(true);
      this.durabilityRoot.SetActive(false);
      this.descLabel.text = buildDesc;
    }
    else
    {
      this.descRoot.SetActive(false);
      this.durabilityRoot.SetActive(true);
      this.durability.text = AllianceBuildUtils.CalcMyAllianceBuildingDurability(this.buildingConfigId).ToString();
    }
    if (!this.isInit)
    {
      this.isInit = true;
      this.icon.SetData(this.getIconPathByAllianceBuildingType(buildingStaticInfo.type), new string[1]
      {
        AllianceBuildUtils.GetBuildName(this.buildingConfigId)
      });
    }
    this.fortressName.text = AllianceBuildUtils.GetBuildName(this.buildingConfigId);
    if (AllianceBuildUtils.CheckBuildIsLock(this.buildingConfigId) || AllianceBuildUtils.CheckBuildingIsFreeze(this.buildingConfigId))
      GreyUtility.Grey(this.iconContainer);
    else
      GreyUtility.Normal(this.iconContainer);
  }

  public void OnGotoClick()
  {
    if (!AllianceBuildUtils.CheckBuildIsPlace(this.buildingConfigId))
      return;
    Coordinate buildCoordinate = AllianceBuildUtils.GetBuildCoordinate(this.buildingConfigId);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    LinkerHub.Instance.LHGotoTile(new LPTile()
    {
      k = buildCoordinate.K,
      x = buildCoordinate.X,
      y = buildCoordinate.Y
    });
  }

  public void OnBuildingTexturePressed()
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.buildingConfigId);
    if (buildingStaticInfo == null || buildingStaticInfo.type != 7 || !ActivityManager.Intance.isPortalExist)
      return;
    UIManager.inst.OpenDlg("Activity/ActivityMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnBuildBtPressHandler()
  {
    AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(this.buildingConfigId);
    if (buildingStaticInfo == null)
      return;
    if (buildingStaticInfo.type == 0 || buildingStaticInfo.type == 1 || buildingStaticInfo.type == 8)
    {
      if (!Utils.IsR5Member())
      {
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_fort_only_leader_can_place", true), (System.Action) null, 4f, false);
        return;
      }
    }
    else if (!Utils.IsR5Member() && !Utils.IsR4Member())
    {
      string content = ScriptLocalization.Get("toast_alliance_building_place_rank_not_enough", true);
      if (buildingStaticInfo.type == 7)
        content = ScriptLocalization.Get("toast_alliance_building_place_leader_only", true);
      UIManager.inst.toast.Show(content, (System.Action) null, 4f, false);
      return;
    }
    if (MapUtils.IsPitWorld(PlayerData.inst.CityData.Location.K))
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("exception_1008010", true), (System.Action) null, 4f, false);
    }
    else
    {
      switch (buildingStaticInfo.type)
      {
        case 0:
          this.PlaceFortress();
          break;
        case 1:
          this.PlaceTurret();
          break;
        case 2:
          this.PlaceWareHouse();
          break;
        case 3:
        case 4:
        case 5:
        case 6:
          this.PlaceSuperMine(buildingStaticInfo.type);
          break;
        case 7:
          this.PlaceAlliancePortal();
          break;
        case 8:
          this.PlaceHospital();
          break;
        case 9:
          this.PlaceAllianceAltar();
          break;
      }
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
  }

  private void PlaceFortress()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_FORTRESS, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_FORTRESS, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceTurret()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterPlaceTurretMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterPlaceTurretMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceWareHouse()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_WAREHOUSE, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_WAREHOUSE, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceSuperMine(int type)
  {
    TeleportMode teleportMode = TeleportMode.PLACE_FARM;
    switch (type)
    {
      case 3:
        teleportMode = TeleportMode.PLACE_FARM;
        break;
      case 4:
        teleportMode = TeleportMode.PLACE_SAWMILL;
        break;
      case 5:
        teleportMode = TeleportMode.PLACE_MINE;
        break;
      case 6:
        teleportMode = TeleportMode.PLACE_HOUSE;
        break;
    }
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, teleportMode, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, teleportMode, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceAllianceAltar()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_DRAGON_ALTAR, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_DRAGON_ALTAR, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceAlliancePortal()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_PORTAL, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_PORTAL, this.buildingConfigId, this.m_Parameter == null);
  }

  private void PlaceHospital()
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
      UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
      {
        GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
        PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_HOSPITAL, this.buildingConfigId, this.m_Parameter == null);
      }));
    else
      PVPSystem.Instance.Map.EnterTeleportMode(this.m_Parameter == null ? this.PlaceLocation : this.m_Parameter.targetLocation, TeleportMode.PLACE_HOSPITAL, this.buildingConfigId, this.m_Parameter == null);
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateDurability);
  }

  private void UpdateDurability(int time)
  {
    this.Refresh();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateDurability);
  }
}
