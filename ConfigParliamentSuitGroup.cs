﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentSuitGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentSuitGroup
{
  private List<ParliamentSuitGroupInfo> _parliamentSuitGroupInfoList = new List<ParliamentSuitGroupInfo>();
  private Dictionary<string, ParliamentSuitGroupInfo> _datas;
  private Dictionary<int, ParliamentSuitGroupInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentSuitGroupInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentSuitGroupInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentSuitGroupInfoList.Add(enumerator.Current);
    }
  }

  public List<ParliamentSuitGroupInfo> GetParliamentSuitGroupInfoList()
  {
    return this._parliamentSuitGroupInfoList;
  }

  public ParliamentSuitGroupInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentSuitGroupInfo) null;
  }

  public ParliamentSuitGroupInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentSuitGroupInfo) null;
  }
}
