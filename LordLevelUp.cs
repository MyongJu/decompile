﻿// Decompiled with JetBrains decompiler
// Type: LordLevelUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class LordLevelUp : Popup
{
  public int curLevel;
  [SerializeField]
  private LordLevelUp.Panel panel;

  public void OnCloseButtonClick()
  {
    this.StartCoroutine(this.PlayCloseAnimation());
    this.OnAnimationFinished();
  }

  [DebuggerHidden]
  private IEnumerator PlayCloseAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LordLevelUp.\u003CPlayCloseAnimation\u003Ec__Iterator73()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnAnimationFinished()
  {
    UIManager.inst.ClosePopup(this.ID, new Popup.PopupParameter());
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TutorialManager.Instance.Pause();
    AudioManager.Instance.PlaySound("sfx_ui_levelup", false);
    this.curLevel = (orgParam as LordLevelUp.Parameter).level;
    HeroPointData heroPointData = ConfigManager.inst.DB_Hero_Point.GetHeroPointData(this.curLevel.ToString());
    if (heroPointData == null)
      return;
    this.panel.level.text = ScriptLocalization.Get("barracks_02_uppercase_level", true) + " " + heroPointData.Level;
    this.panel.power.text = "+" + (object) heroPointData.Power;
    this.panel.talentpoint.text = "+" + (object) heroPointData.SkillPoints;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.lordimage, PlayerData.inst.userData.PortraitPath, (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.headerbg, "Texture/lord_level_up_banner", (System.Action<bool>) null, true, false, string.Empty);
    if (heroPointData.Rewards.rewards.Count != 6)
      Debug.LogWarning((object) "wrong rewards count!!!");
    int index = 0;
    using (Dictionary<string, int>.Enumerator enumerator = heroPointData.Rewards.rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        int internalId = int.Parse(current.Key);
        int num = current.Value;
        ItemRewardInfo.Data d = new ItemRewardInfo.Data();
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
        d.name = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
        d.icon = itemStaticInfo.ImagePath;
        d.count = (float) num;
        d.quality = itemStaticInfo.Quality;
        d.itemId = itemStaticInfo.internalId;
        this.panel.items[index].SeedData(d);
        ++index;
      }
    }
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "uid"] = (object) PlayerData.inst.uid;
    hashtable[(object) "value"] = (object) this.curLevel;
    hashtable[(object) "name"] = (object) "last_notify_level";
    DBManager.inst.DB_Stats.Update((object) hashtable, (long) (NetServerTime.inst.UpdateTime * 1000.0));
    MessageHub.inst.GetPortByAction("Hero:notifyLastLevel").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid, (object) "level", (object) this.curLevel), (System.Action<bool, object>) null, true);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    BuilderFactory.Instance.Release((UIWidget) this.panel.lordimage);
    TutorialManager.Instance.Resume();
    if (orgParam == null)
      return;
    CitadelSystem.inst.OpenLordLevelUpPopUp();
  }

  [Serializable]
  protected class Panel
  {
    public List<ItemRewardInfo> items = new List<ItemRewardInfo>(6);
    public UILabel level;
    public UILabel power;
    public UILabel talentpoint;
    public UITexture lordimage;
    public UITexture headerbg;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int level;
  }
}
