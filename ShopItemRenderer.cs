﻿// Decompiled with JetBrains decompiler
// Type: ShopItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class ShopItemRenderer : Icon
{
  public bool CanRevert = true;
  public UIButton buyButton;
  public UILabel priceLabel;
  public UISprite selectedBg;
  private bool _selected;
  public System.Action<ShopItemRenderer> OnSelectedHandler;
  public System.Action<int> OnCustomBuy;
  public UITexture background;

  public bool Selected
  {
    get
    {
      return this._selected;
    }
    set
    {
      if (this._selected == value)
        return;
      this._selected = value;
      NGUITools.SetActive(this.selectedBg.gameObject, this._selected);
    }
  }

  public void SetScrollView(UIScrollView scroll)
  {
    foreach (UIDragScrollView componentsInChild in this.gameObject.GetComponentsInChildren<UIDragScrollView>())
      componentsInChild.scrollView = scroll;
  }

  public void OnViewDetailHandler()
  {
    if (!this.CanRevert && this.Selected)
      return;
    int data = (int) (this.data as IconData).Data;
    this.Selected = !this.Selected;
    if (this.OnSelectedHandler == null)
      return;
    this.OnSelectedHandler(this);
  }

  public void OnBuyHandler()
  {
    if (ItemBag.Instance.GetShopItemPrice(this.Shop_ID) > PlayerData.inst.hostPlayer.Currency)
      Utils.ShowNotEnoughGoldTip();
    else if (this.OnCustomBuy != null)
      this.OnCustomBuy(this.Shop_ID);
    else
      UIManager.inst.OpenPopup("ItemUseOrBuyPopup", (Popup.PopupParameter) new ItemUseOrBuyPopup.Parameter()
      {
        shop_id = this.Shop_ID,
        type = ItemUseOrBuyPopup.Parameter.Type.BuyItem
      });
  }

  public int Shop_ID
  {
    get
    {
      IconData data = this.data as IconData;
      if (data != null)
        return (int) data.Data;
      return 0;
    }
  }

  public override void SetData(string[] contents, string path)
  {
    base.SetData(contents, path);
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId((int) (this.data as IconData).Data);
    int shopItemPrice = ItemBag.Instance.GetShopItemPrice(dataByInternalId.ID);
    Utils.SetItemName(this.labels[0], dataByInternalId.Item_InternalId);
    Utils.SetPriceToLabel(this.priceLabel, shopItemPrice);
    Utils.SetItemBackground(this.background, dataByInternalId.Item_InternalId);
    NGUITools.SetActive(this.selectedBg.gameObject, false);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.OnSelectedHandler = (System.Action<ShopItemRenderer>) null;
  }
}
