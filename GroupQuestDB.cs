﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestDB
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class GroupQuestDB
{
  private Dictionary<long, GroupQuestData> _datas = new Dictionary<long, GroupQuestData>();
  private List<GroupQuestData> _commonQuests = new List<GroupQuestData>();
  public System.Action<long> onDataChanged;
  public System.Action<long> onDataCreated;
  public System.Action<long> onDataUpdated;
  public System.Action<long> onDataRemove;
  public System.Action<long> onDataRemoved;
  private GroupQuestData _recommendQuest;

  public GroupQuestData RecommendQuest
  {
    get
    {
      Dictionary<long, GroupQuestData>.Enumerator enumerator = this._datas.GetEnumerator();
      this._recommendQuest = (GroupQuestData) null;
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Type == 1)
          this._recommendQuest = enumerator.Current.Value;
      }
      return this._recommendQuest;
    }
  }

  public int CompletedCount
  {
    get
    {
      int num = 0;
      Dictionary<long, GroupQuestData>.Enumerator enumerator = this._datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Status == 1)
          ++num;
      }
      return num + DBManager.inst.DB_Local_TimerQuest.GetCompleteCount();
    }
  }

  public List<GroupQuestData> CommonQuests
  {
    get
    {
      Dictionary<long, GroupQuestData>.Enumerator enumerator = this._datas.GetEnumerator();
      this._commonQuests.Clear();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.Type == 2)
          this._commonQuests.Add(enumerator.Current.Value);
      }
      return this._commonQuests;
    }
  }

  public void Publish_onDataChanged(long internalId)
  {
    if (this.onDataChanged == null)
      return;
    this.onDataChanged(internalId);
  }

  public void Publish_onDataCreated(long internalId)
  {
    if (this.onDataCreated != null)
      this.onDataCreated(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_OnDataUpdated(long internalId)
  {
    if (this.onDataUpdated != null)
      this.onDataUpdated(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_onDataRemove(long internalId)
  {
    if (this.onDataRemove != null)
      this.onDataRemove(internalId);
    this.Publish_onDataChanged(internalId);
  }

  public void Publish_onDataRemoved(long internalId)
  {
    if (this.onDataRemoved == null)
      return;
    this.onDataRemoved(internalId);
  }

  public bool Add(object orgData, long updateTime)
  {
    if (orgData == null)
      return false;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null)
      return false;
    GroupQuestData groupQuestData = new GroupQuestData();
    if (!groupQuestData.Decode((object) hashtable, updateTime) || groupQuestData.QuestID == (long) GroupQuestData.INVALID_ID || this._datas.ContainsKey(groupQuestData.QuestID))
      return false;
    this._datas.Add(groupQuestData.QuestID, groupQuestData);
    this.Publish_onDataCreated(groupQuestData.QuestID);
    return true;
  }

  public bool Update(object orgData, long updateTime)
  {
    if (orgData == null)
      return false;
    Hashtable hashtable = orgData as Hashtable;
    if (hashtable == null || !hashtable.ContainsKey((object) "uid") || long.Parse(hashtable[(object) "uid"].ToString()) != PlayerData.inst.uid)
      return false;
    long internalId = QuestData2.GetInternalId((object) hashtable);
    if (internalId == (long) QuestData2.INVALID_ID)
      return false;
    if (!this._datas.ContainsKey(internalId))
      return this.Add(orgData, updateTime);
    if (!this._datas[internalId].Decode((object) hashtable, updateTime))
      return false;
    this.Publish_OnDataUpdated(internalId);
    return true;
  }

  public void UpdateDatas(object orgDatas, long updateTime)
  {
    ArrayList arrayList = orgDatas as ArrayList;
    if (arrayList == null)
      return;
    int index = 0;
    for (int count = arrayList.Count; index < count; ++index)
      this.Update(arrayList[index], updateTime);
  }

  public GroupQuestData Get(long key)
  {
    if (this._datas.ContainsKey(key))
      return this._datas[key];
    return (GroupQuestData) null;
  }

  public void RemoveDatas(object orgDatas, long updateTime)
  {
    ArrayList arrayList = orgDatas as ArrayList;
    if (arrayList == null)
      return;
    int index = 0;
    for (int count = arrayList.Count; index < count; ++index)
      this.Remove(int.Parse((arrayList[index] as Hashtable)[(object) "quest_id"].ToString()), updateTime);
  }

  public bool Remove(int internalId, long updateTime)
  {
    if (!this._datas.ContainsKey((long) internalId) || !this._datas[(long) internalId].CheckUpdateTime(updateTime))
      return false;
    this.Publish_onDataRemove((long) internalId);
    this._datas.Remove((long) internalId);
    this.Publish_onDataRemoved((long) internalId);
    return true;
  }

  public string GetQuestImage(long questID)
  {
    GroupQuestData data = this._datas[questID];
    string str = string.Empty;
    string category = data.Category;
    if (category != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (GroupQuestDB.\u003C\u003Ef__switch\u0024map6D == null)
      {
        // ISSUE: reference to a compiler-generated field
        GroupQuestDB.\u003C\u003Ef__switch\u0024map6D = new Dictionary<string, int>(12)
        {
          {
            "build",
            0
          },
          {
            "train",
            1
          },
          {
            "pve",
            2
          },
          {
            "pvp",
            3
          },
          {
            "research",
            4
          },
          {
            "alliance_join",
            5
          },
          {
            "alliance_help",
            5
          },
          {
            "resource_collect",
            6
          },
          {
            "resource_rate",
            6
          },
          {
            "legend_recruit",
            7
          },
          {
            "legend_skill_up",
            7
          },
          {
            "legend_xp_item",
            7
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (GroupQuestDB.\u003C\u003Ef__switch\u0024map6D.TryGetValue(category, out num))
      {
        switch (num)
        {
          case 0:
            str = "icon_my_city";
            break;
          case 1:
            str = "icon_troop";
            break;
          case 2:
            str = "icon_event";
            break;
          case 3:
            str = "icon_troop";
            break;
          case 4:
            str = "icon_research";
            break;
          case 5:
            str = "icon_alliance";
            break;
          case 6:
            str = "icon_my_city";
            break;
          case 7:
            str = "icon_my_city";
            break;
        }
      }
    }
    return str;
  }

  public Dictionary<long, GroupQuestData> Datas
  {
    get
    {
      return this._datas;
    }
  }

  public void ShowInfo()
  {
  }
}
