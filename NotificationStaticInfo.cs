﻿// Decompiled with JetBrains decompiler
// Type: NotificationStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class NotificationStaticInfo
{
  [Config(Name = "localization")]
  public string LocalizationKey = "NoHadDefaultKey";
  [Config(Name = "sound")]
  public string SoundName = string.Empty;
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public int type;
  private bool soundInit;

  public string LocalMessage
  {
    get
    {
      return ScriptLocalization.Get(this.LocalizationKey, false);
    }
  }

  public string LocalSoundName
  {
    get
    {
      if (!this.soundInit)
      {
        this.soundInit = true;
        string[] strArray = this.SoundName.Split('.');
        if (strArray.Length > 0 && Application.platform == RuntimePlatform.Android)
          this.SoundName = strArray[0];
      }
      return this.SoundName;
    }
  }

  public string GetLocalMessage(Dictionary<string, string> dict)
  {
    return ScriptLocalization.GetWithPara(this.LocalizationKey, dict, false);
  }

  public struct NoficationType
  {
    public const int NONE = 0;
    public const int WAR = 1;
    public const int TAIN_OR_RESEARCH = 2;
    public const int MAIL = 3;
    public const int ALLIANCE = 4;
    public const int OTHER = 5;
    public const int ALLIANCE_CHAT = 6;
    public const int GVE = 7;
    public const int AUCTION = 8;
    public const int WORLD_AUCTION = 10;
    public const int MERLIN_TOWER = 11;
  }

  public struct NoficationID
  {
    public const string BUILD_FINISH = "build_finish";
    public const string RESEARCH_FINISH = "research_finish";
    public const string TRAIN_FINISH = "train_finish";
    public const string TRAIN_TRAP_FINISH = "train_trap_finish";
    public const string TROOP_BACK = "troop_back";
    public const string EQUIP_PRODUCED = "equip_produced";
    public const string TIMER_CHEST = "timer_chest";
    public const string RESOURCE_BUILDING_FULL = "resource_building_full";
    public const string PROTECTION_TIME_UP = "protection_time_up";
    public const string BE_SCOUTED = "be_scouted";
    public const string BE_RALLY_TARGET = "be_rally_target";
    public const string ALLIANCE_MEMBER_RALLY = "alliance_member_rally";
    public const string MAIL_RECEIVED = "mail_received";
    public const string ALLIANCE_ACTIVITY = "alliance_activity";
    public const string ALLIANCE_R4_CHAT = "alliance_r4_chat";
    public const string FORGE_FINISH = "equip_produced";
    public const string DRAGON = "dragon_ready";
    public const string DEFAULT = "dragon_ready";
    public const string ABYSS_START = "abyss_start";
  }
}
