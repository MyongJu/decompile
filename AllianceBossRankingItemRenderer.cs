﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossRankingItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UnityEngine;

public class AllianceBossRankingItemRenderer : MonoBehaviour
{
  public UILabel rankedTimeLabel;
  public UITexture rankTexture;
  public AllianceBossRankingItemRenderer.ItemInfo subItemInfo;
  private int rank;
  private int rankedTime;
  private long uid;
  private long score;

  public void SetData(int rank, long uid, long score, int rankedTime)
  {
    this.rank = rank;
    this.uid = uid;
    this.score = score;
    this.rankedTime = rankedTime;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.rankedTimeLabel.text = this.rankedTime <= 0 ? string.Empty : Utils.FormatTimeYYYYMMDD((long) this.rankedTime, "/");
    this.subItemInfo.displayedNameLabel.text = string.Empty;
    UserData userData = DBManager.inst.DB_User.Get(this.uid);
    string str = userData == null ? string.Empty : userData.userName;
    this.subItemInfo.displayedNameLabel.text = !string.IsNullOrEmpty(str) ? str : "~";
    this.subItemInfo.rankScoreLabel.text = Utils.FormatThousands(this.score.ToString());
    if (string.IsNullOrEmpty(this.subItemInfo.displayedNameLabel.text))
      return;
    this.subItemInfo.rankNumberLabel.text = "NO." + (object) this.rank;
    this.subItemInfo.rankNumberLabel.transform.parent.gameObject.SetActive(true);
    if (this.rank <= 3)
    {
      NGUITools.SetActive(this.subItemInfo.rankNumberLabel.gameObject, false);
      NGUITools.SetActive(this.rankTexture.gameObject, true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.rankTexture, "Texture/LeaderboardIcons/icon_no" + (object) this.rank, (System.Action<bool>) null, true, false, string.Empty);
    }
    else
    {
      NGUITools.SetActive(this.subItemInfo.rankNumberLabel.gameObject, true);
      NGUITools.SetActive(this.rankTexture.gameObject, false);
    }
  }

  [Serializable]
  public class ItemInfo
  {
    public UILabel rankNumberLabel;
    public UILabel displayedNameLabel;
    public UILabel rankScoreLabel;
  }
}
