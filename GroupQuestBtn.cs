﻿// Decompiled with JetBrains decompiler
// Type: GroupQuestBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class GroupQuestBtn : MonoBehaviour
{
  public GameObject tip;
  public UILabel tipLabel;
  public UILabel progressLabel;
  public GameObject notifiction;

  public void Init()
  {
    if (DBManager.inst.DB_Quest.GroupQuests.Count > 0)
    {
      this.gameObject.SetActive(true);
      this.AddEventHandler();
      this.ShowTip();
    }
    else
      this.gameObject.SetActive(false);
  }

  public void Dispose()
  {
    this.RemoveEventHandler();
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved -= new System.Action<long>(this.UpdateUI);
    DBManager.inst.DB_Quest.onDataChanged -= new System.Action<long>(this.UpdateUI);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Quest.onDataRemoved += new System.Action<long>(this.UpdateUI);
    DBManager.inst.DB_Quest.onDataChanged += new System.Action<long>(this.UpdateUI);
  }

  private void UpdateUI(long questId)
  {
    if (DBManager.inst.DB_Quest.GroupQuests.Count == 0)
    {
      this.gameObject.SetActive(false);
      this.RemoveEventHandler();
    }
    else
      this.ShowTip();
  }

  public void ShowTip()
  {
    this.tip.SetActive(true);
    this.notifiction.SetActive(DBManager.inst.DB_Quest.IsGroupQuestsComplete());
    this.progressLabel.text = string.Format("{0}/{1}", (object) DBManager.inst.DB_Quest.GroupQuestCompleteCount(), (object) DBManager.inst.DB_Quest.GroupQuests.Count);
    QuestData2 completeGroupQuest = DBManager.inst.DB_Quest.FirstUnCompleteGroupQuest;
    if (completeGroupQuest != null)
    {
      completeGroupQuest.Present.Refresh();
      this.tipLabel.text = Utils.XLAT(completeGroupQuest.Info.Loc_Name);
      this.Invoke("HideTip", 5f);
    }
    else
      this.HideTip();
  }

  public void ShowTip(QuestData2 quest)
  {
    this.tip.SetActive(true);
    this.progressLabel.text = string.Format("{0}/{1}", (object) DBManager.inst.DB_Quest.GroupQuestCompleteCount(), (object) DBManager.inst.DB_Quest.GroupQuests.Count);
    if (quest != null)
    {
      quest.Present.Refresh();
      this.tipLabel.text = Utils.XLAT(quest.Info.Loc_Name);
      this.Invoke("HideTip", 5f);
    }
    else
      this.HideTip();
  }

  private void HideTip()
  {
    this.tip.SetActive(false);
  }
}
