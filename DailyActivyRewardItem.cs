﻿// Decompiled with JetBrains decompiler
// Type: DailyActivyRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DailyActivyRewardItem : MonoBehaviour
{
  public UITexture itemIcon;
  public UITexture background;
  public UISprite resourceIcon;
  public UILabel count;
  private int _itemId;

  public void OnShowTip()
  {
    Utils.ShowItemTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void SetItem(int itemId, int count)
  {
    this._itemId = itemId;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    this.count.text = "X " + count.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemBackground(this.background, itemId);
    NGUITools.SetActive(this.resourceIcon.gameObject, false);
    NGUITools.SetActive(this.itemIcon.gameObject, true);
  }

  public void SetResource(string resourcName, int count)
  {
    this.count.text = "X " + Utils.FormatThousands(count.ToString());
    this.resourceIcon.spriteName = Utils.GetResoureSpriteName(resourcName);
    NGUITools.SetActive(this.resourceIcon.gameObject, true);
    NGUITools.SetActive(this.itemIcon.gameObject, false);
  }

  public void SetDragonEXP(int count)
  {
    this.count.text = "X " + Utils.FormatThousands(count.ToString());
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIcon, string.Empty, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.resourceIcon.gameObject, false);
    NGUITools.SetActive(this.itemIcon.gameObject, true);
  }

  public void SetLordEXP(int count)
  {
    this.count.text = "X " + Utils.FormatThousands(count.ToString());
    this.resourceIcon.spriteName = "icon_player_profile_exp";
    NGUITools.SetActive(this.resourceIcon.gameObject, true);
    NGUITools.SetActive(this.itemIcon.gameObject, false);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.itemIcon);
  }
}
