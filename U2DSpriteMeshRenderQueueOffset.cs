﻿// Decompiled with JetBrains decompiler
// Type: U2DSpriteMeshRenderQueueOffset
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class U2DSpriteMeshRenderQueueOffset : MonoBehaviour
{
  public U2DSpriteMesh m_Source;
  public Renderer m_Target;
  public int m_Offset;

  private void Update()
  {
    if (!((Object) this.m_Target != (Object) null) || !((Object) this.m_Source != (Object) null))
      return;
    this.m_Target.sharedMaterial.renderQueue = this.m_Source.material.renderQueue + this.m_Offset;
  }
}
