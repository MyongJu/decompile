﻿// Decompiled with JetBrains decompiler
// Type: DKArenaActivityData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class DKArenaActivityData : ActivityBaseData
{
  private long _nextTime;

  public override ActivityBaseData.ActivityType Type
  {
    get
    {
      return ActivityBaseData.ActivityType.DKArena;
    }
  }

  public long NextTime
  {
    get
    {
      return this._nextTime;
    }
  }

  public override int RemainTime
  {
    get
    {
      if (this.IsStart())
        return this.EndTime - NetServerTime.inst.ServerTimestamp;
      return (int) this.NextTime - NetServerTime.inst.ServerTimestamp;
    }
  }

  public override void Decode(Hashtable source)
  {
    base.Decode(source);
    string empty = string.Empty;
    if (!source.ContainsKey((object) "next_start") || source[(object) "next_start"] == null)
      return;
    long.TryParse(source[(object) "next_start"].ToString(), out this._nextTime);
  }
}
