﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsRewardSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinTrialsRewardSlot : MonoBehaviour
{
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UILabel _itemName;
  [SerializeField]
  private UILabel _itemCount;
  private int _itemId;
  private int _count;
  private bool _isCoin;

  public void SetData(int itemId, int count, bool isCoin = false)
  {
    this._itemId = itemId;
    this._count = count;
    this._isCoin = isCoin;
    if ((bool) ((Object) this._itemCount))
      this._itemCount.text = count.ToString();
    if (isCoin)
    {
      this._itemIcon.SetData(MerlinTrialsPayload.Instance.MerlinCoinImagePath);
    }
    else
    {
      this._itemIcon.SetData(itemId, string.Empty, true);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
      if (itemStaticInfo == null || !((Object) this._itemName != (Object) null))
        return;
      this._itemName.text = itemStaticInfo.LocName;
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._itemName, itemStaticInfo.Quality);
    }
  }

  public void OnItemPress()
  {
    if (this._isCoin)
      Utils.DelayShowCustomTip(new ItemTipCustomInfo.ItemTipData(string.Empty, MerlinTrialsPayload.Instance.MerlinCoinName, MerlinTrialsPayload.Instance.MerlinCoinDescription, MerlinTrialsPayload.Instance.MerlinCoinImagePath, string.Empty), this.transform);
    else
      Utils.DelayShowTip(this._itemId, this.transform, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if (this._isCoin)
      Utils.ShowCustomItemTip(new ItemTipCustomInfo.ItemTipData(string.Empty, MerlinTrialsPayload.Instance.MerlinCoinName, MerlinTrialsPayload.Instance.MerlinCoinDescription, MerlinTrialsPayload.Instance.MerlinCoinImagePath, string.Empty), this.transform);
    else
      Utils.ShowItemTip(this._itemId, this.transform, 0L, 0L, 0);
  }
}
