﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxWith2ButtonsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MessageBoxWith2ButtonsPopup : Popup
{
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_LeftBtnLabel;
  [SerializeField]
  private UILabel m_RightBtnLabel;
  [SerializeField]
  private UIButton m_CloseButton;
  private MessageBoxWith2ButtonsPopup.Parameter parameter;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    this.parameter = orgParam as MessageBoxWith2ButtonsPopup.Parameter;
    this.m_TitleLabel.text = this.parameter.titleString;
    this.m_ContentLabel.text = this.parameter.contentString;
    this.m_LeftBtnLabel.text = this.parameter.leftLabelString;
    this.m_RightBtnLabel.text = this.parameter.rightLabelString;
  }

  public void OnLeftBtnClick()
  {
    if (this.parameter.onLeftBtnClick != null)
      this.parameter.onLeftBtnClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnRightBtnClick()
  {
    if (this.parameter.onRightBtnClick != null)
      this.parameter.onRightBtnClick();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleString;
    public string contentString;
    public string leftLabelString;
    public string rightLabelString;
    public System.Action onLeftBtnClick;
    public System.Action onRightBtnClick;
  }
}
