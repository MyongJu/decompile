﻿// Decompiled with JetBrains decompiler
// Type: SQWanIapAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Funplus.Abstract;
using HSMiniJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UI;
using UnityEngine;

public class SQWanIapAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  private List<Dictionary<string, object>> allProductData = new List<Dictionary<string, object>>();
  private const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  private const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  private const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  private const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  private const string ERROR_MESSAGE = "{errorCode:9999, errorMsg:\"\", errorLocalizedMsg:\"\"}";
  private const string PLUGIN_VERSION = "1.0";
  private const string COUNTRY_CODE = "CN";
  private static SQWanIapAndroid _instance;
  private SQWanHelper _monoHelper;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;

  public SQWanIapAndroid()
  {
    this._monoHelper = GameObject.Find("SQWanHelper").GetComponent<SQWanHelper>();
    this._monoHelper.PaymentFailureHandler = new System.Action(this.OnPaymentFailure);
    this._monoHelper.PaymentSuccessHandler = new System.Action(this.OnPaymentSuccess);
  }

  public static SQWanIapAndroid Instance
  {
    get
    {
      if (SQWanIapAndroid._instance == null)
      {
        lock (SQWanIapAndroid.locker)
          SQWanIapAndroid._instance = new SQWanIapAndroid();
      }
      return SQWanIapAndroid._instance;
    }
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  public GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    SQWanSdkManager.Log(nameof (StartHelper));
    if (this.BuildProductListFromConfigFiles())
      return;
    this.BuildProductListFromCode();
  }

  private void BuildProductListFromCode()
  {
    SQWanSdkManager.Log(nameof (BuildProductListFromCode));
    this.allProductData.Clear();
    using (Dictionary<string, double>.Enumerator enumerator = new Dictionary<string, double>()
    {
      {
        "com.funplus.koa.month0",
        30.0
      },
      {
        "com.funplus.koa.month1",
        68.0
      },
      {
        "com.funplus.koa.month2",
        128.0
      },
      {
        "com.funplus.koa.gold0",
        6.0
      },
      {
        "com.funplus.koa.gold1",
        30.0
      },
      {
        "com.funplus.koa.gold2",
        68.0
      },
      {
        "com.funplus.koa.gold3",
        128.0
      },
      {
        "com.funplus.koa.gold4",
        328.0
      },
      {
        "com.funplus.koa.gold5",
        648.0
      },
      {
        "com.funplus.koa.subscription0",
        198.0
      },
      {
        "com.funplus.koa.cn.dailyrecharge0",
        30.0
      },
      {
        "com.funplus.koa.cn.dailyrecharge1",
        68.0
      },
      {
        "com.funplus.koa.cn.dailyrecharge2",
        128.0
      }
    }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, double> current = enumerator.Current;
        this.allProductData.Add(new Dictionary<string, object>()
        {
          {
            "productId",
            (object) current.Key
          },
          {
            "title",
            (object) string.Empty
          },
          {
            "description",
            (object) string.Empty
          },
          {
            "price_currency_code",
            (object) "¥"
          },
          {
            "price",
            (object) string.Format("¥ {0}", (object) current.Value)
          },
          {
            "price_amount",
            (object) current.Value.ToString()
          },
          {
            "price_amount_micros",
            (object) (long) (1000000.0 * current.Value)
          }
        });
      }
    }
    string str = Json.Serialize((object) this.allProductData);
    this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
    SQWanSdkManager.Log("37wan product list(by code): " + str);
  }

  private bool BuildProductListFromConfigFiles()
  {
    SQWanSdkManager.Log(nameof (BuildProductListFromConfigFiles));
    if (!File.Exists(NetApi.inst.BuildLocalPath("iap_platform.json")) || !File.Exists(NetApi.inst.BuildLocalPath("iap_price_default.json")))
      return false;
    this.allProductData.Clear();
    List<IAPPlatformInfo> all = ConfigManager.inst.DB_IAPPlatform.GetAll("cn37");
    SQWanSdkManager.Log("all 37wan platform info size: " + (object) all.Count);
    using (List<IAPPlatformInfo>.Enumerator enumerator = all.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IAPPlatformInfo current = enumerator.Current;
        ConfigIAPFakePriceInfo iapFakePriceInfo = ConfigManager.inst.DB_IAPPrice.Get(current.pay_id);
        if (iapFakePriceInfo == null)
        {
          SQWanSdkManager.Log(string.Format("cannot find product default price : {0}", (object) current.pay_id));
        }
        else
        {
          double cny = iapFakePriceInfo.cny;
          long num = (long) (cny * 1000000.0);
          Dictionary<string, object> dictionary = new Dictionary<string, object>()
          {
            {
              "productId",
              (object) current.product_id
            },
            {
              "title",
              (object) string.Empty
            },
            {
              "description",
              (object) string.Empty
            },
            {
              "price_currency_code",
              (object) "¥"
            },
            {
              "price",
              (object) string.Format("¥ {0}", (object) (int) cny)
            },
            {
              "price_amount",
              (object) string.Format("{0}", (object) (int) cny)
            },
            {
              "price_amount_micros",
              (object) num
            }
          };
          SQWanSdkManager.Log(string.Format("Add product data, curreny_code {0}, price {1}", (object) dictionary["price_currency_code"].ToString(), (object) dictionary["price"].ToString()));
          this.allProductData.Add(dictionary);
        }
      }
    }
    string str = Json.Serialize((object) this.allProductData);
    this.TargetGameObject.BroadcastMessage("OnPaymentInitializeSuccess", (object) str);
    SQWanSdkManager.Log("37wan product list(by config): " + str);
    return this.allProductData.Count > 0;
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  private string CurrecyCode
  {
    get
    {
      return GlobalizationCodeConverter.CountryCodeToCurrencyCode("CN");
    }
  }

  private int GetProductPrice(string productId)
  {
    int index = 0;
    for (int count = this.allProductData.Count; index < count; ++index)
    {
      Dictionary<string, object> dictionary = this.allProductData[index];
      if (dictionary[nameof (productId)].ToString() == productId)
        return int.Parse(dictionary["price_amount"] as string);
    }
    SQWanSdkManager.Log(string.Format("The product id cannot find price, {0}", (object) productId));
    return 0;
  }

  private string GetProductName(string productId)
  {
    return string.Format("{0}元的商品", (object) this.GetProductPrice(productId));
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    SQWanSdkManager.Log(string.Format("Buy {0} {1} {2}", (object) productId, (object) serverId, (object) throughCargo));
    this.RequestInitTid(productId, serverId, throughCargo);
  }

  private void RequestInitTid(string productId, string serverId, string throughCargo)
  {
    GameObject gameObject = new GameObject();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    HttpPostProcessor httpPostProcessor = gameObject.AddComponent<HttpPostProcessor>();
    Hashtable hashtable = new Hashtable();
    hashtable.Add((object) "product_id", (object) productId);
    hashtable.Add((object) "through_cargo", (object) throughCargo);
    hashtable.Add((object) "appservid", (object) serverId);
    hashtable.Add((object) "appid", (object) this.AppId);
    hashtable[(object) "uid"] = (object) PlayerData.inst.uid;
    hashtable[(object) "amount"] = (object) this.GetProductPrice(productId);
    hashtable[(object) "country_code"] = (object) "CN";
    hashtable[(object) "currency_code"] = (object) this.CurrecyCode;
    hashtable[(object) "plugion_version"] = (object) "1.0";
    httpPostProcessor.DoPost(this.InitTidUrl, hashtable, hashtable, new HttpPostProcessor.PostResultCallback(this.OnInitTidCallback));
  }

  private void OnInitTidCallback(bool result, Hashtable data, Hashtable userData)
  {
    SQWanSdkManager.Log(string.Format("OnInitTidCallback {0} {1} {2}", (object) result, data == null ? (object) "null" : (object) Utils.Object2Json((object) data), userData == null ? (object) "null" : (object) Utils.Object2Json((object) userData)));
    if (result && data != null && userData != null)
    {
      SQWanSdkManager.Log("OnInitTidCallback success");
      string empty1 = string.Empty;
      if (data.ContainsKey((object) "status"))
        empty1 = data[(object) "status"] as string;
      if (empty1 == "OK")
      {
        string empty2 = string.Empty;
        if (data.ContainsKey((object) "tid"))
          empty2 = data[(object) "tid"] as string;
        SQWanSdkManager.Log("OnInitTidCallback status ok, tid: " + empty2);
        string empty3 = string.Empty;
        if (userData.ContainsKey((object) "product_id"))
          empty3 = userData[(object) "product_id"] as string;
        SQWanSdkManager.Instance.CallPay(empty2, this.GetProductName(empty3), "金币", string.Format("{0}", (object) PlayerData.inst.userData.world_id), string.Format("王国：{0}", (object) PlayerData.inst.userData.world_id), string.Empty, string.Format("{0}", (object) PlayerData.inst.uid), PlayerData.inst.userData.userName, CityManager.inst.mStronghold.mLevel, this.GetProductPrice(empty3), 10);
      }
      else
      {
        string empty2 = string.Empty;
        if (data.ContainsKey((object) "reason"))
          empty2 = data[(object) "reason"] as string;
        SQWanSdkManager.Log("OnInitTidCallback status error, reason: " + empty2);
      }
    }
    else
      SQWanSdkManager.Log("OnInitTidCallback fail");
  }

  private string Generate37OrderId()
  {
    return string.Format("{0}_{1}", (object) PlayerData.inst.uid, (object) NetServerTime.inst.LocalTimestamp);
  }

  public void OnPaymentSuccess()
  {
    SQWanSdkManager.Log("Pay success");
    UIManager.inst.HideManualBlocker();
  }

  public void OnPaymentFailure()
  {
    SQWanSdkManager.Log("Pay failure");
    UIManager.inst.HideManualBlocker();
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  private string AppId
  {
    get
    {
      return FunplusSdk.Instance.Environment == "sandbox" ? "156" : "10";
    }
  }

  private string InitTidUrl
  {
    get
    {
      if (!string.IsNullOrEmpty(PaymentManager.Instance.PaymentUrl))
        return PaymentManager.Instance.PaymentUrl + "/payment/cn37_init_tid/";
      return FunplusSdk.Instance.Environment == "sandbox" ? "https://payment-sandbox.funplusgame.com/payment/cn37_init_tid/" : "https://payment-cn.campfiregames.cn/payment/cn37_init_tid/";
    }
  }
}
