﻿// Decompiled with JetBrains decompiler
// Type: AllianceQuestItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceQuestItemRenderer : MonoBehaviour
{
  private GameObjectPool pool = new GameObjectPool();
  private List<QuestRewardSlotEmpire> rewards = new List<QuestRewardSlotEmpire>();
  public QuestIconRenderer render;
  public UILabel questName;
  public UITable rewardContainer;
  public UIButton start;
  public UIButton claim;
  public UIButton speedUp;
  public UIProgressBar progressBar;
  public UILabel remainTime;
  public UILabel totalTime;
  public QuestRewardSlotEmpire rewardItemPrefab;
  private long questId;
  public GameObject normalContent;
  public GameObject openCopntent;
  private int _selectedIndex;
  public UIScrollView scroll;
  public UITable tabel;
  public GameObject itemRoot;
  private bool init;

  public int SelectedIndex
  {
    get
    {
      return this._selectedIndex;
    }
    set
    {
      if (this._selectedIndex == value)
        return;
      this._selectedIndex = value;
      this.ResetView();
    }
  }

  public void SetData(long questId)
  {
    this.questId = questId;
    this.Clear();
    this.UpdateUI();
  }

  private void ResetView()
  {
    NGUITools.SetActive(this.normalContent, false);
    NGUITools.SetActive(this.openCopntent, false);
    if (this.SelectedIndex == 0)
      NGUITools.SetActive(this.normalContent, true);
    else
      NGUITools.SetActive(this.openCopntent, true);
  }

  private void UpdateUI()
  {
    if (!this.init)
    {
      this.init = true;
      this.pool.Initialize(this.rewardItemPrefab.gameObject, this.itemRoot);
    }
    this.UpdateUIState();
    this.RefreshNormal();
    this.AddEventHandler();
    this.RefreshProgressBar(0);
  }

  private void UpdateUIState()
  {
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(this.questId);
    AllianceQuestUIPresent present = timerQuestData.Present as AllianceQuestUIPresent;
    present.Refresh();
    if (timerQuestData.status == TimerQuestData.QuestStatus.STATUS_COLLECTED)
    {
      NGUITools.SetActive(this.start.gameObject, false);
      NGUITools.SetActive(this.speedUp.gameObject, false);
      NGUITools.SetActive(this.totalTime.gameObject, false);
      NGUITools.SetActive(this.progressBar.gameObject, false);
      NGUITools.SetActive(this.claim.gameObject, false);
    }
    else
    {
      NGUITools.SetActive(this.start.gameObject, !present.NeedUpgardeBar);
      NGUITools.SetActive(this.speedUp.gameObject, present.NeedUpgardeBar);
      NGUITools.SetActive(this.progressBar.gameObject, present.NeedUpgardeBar);
      NGUITools.SetActive(this.claim.gameObject, timerQuestData.status == TimerQuestData.QuestStatus.STATUS_COMPLETED);
      NGUITools.SetActive(this.start.gameObject, timerQuestData.status == TimerQuestData.QuestStatus.STATUS_INACTIVE);
      NGUITools.SetActive(this.totalTime.gameObject, timerQuestData.status == TimerQuestData.QuestStatus.STATUS_INACTIVE);
    }
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshProgressBar);
    if (timerQuestData.status == TimerQuestData.QuestStatus.STATUS_ACTIVE)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.RefreshProgressBar);
    this.ResetStartButton(PlayerData.inst.QuestManager.GetStartingAllianceQuest());
  }

  private void ResetStartButton(long startQuestId)
  {
    if (startQuestId == -1L)
      this.start.isEnabled = true;
    else
      this.start.isEnabled = startQuestId == this.questId;
  }

  private void RefreshNormal()
  {
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(this.questId);
    AllianceQuestInfo allianceQuestInfo = ConfigManager.inst.DB_AllianceQuest.Get(timerQuestData.internalId);
    QuestRewardAgent rewardAgent = timerQuestData.RewardAgent;
    this.questName.text = allianceQuestInfo.Name;
    this.render.SetData(this.questId);
    this.totalTime.text = Utils.FormatTime(allianceQuestInfo.Duration_Time, false, false, true);
    this.CreateRewardItems();
  }

  private void CreateRewardItems()
  {
    QuestRewardAgent rewardAgent = Utils.GetRewardAgent(this.questId);
    for (int index = 0; index < rewardAgent.Rewards.Count; ++index)
    {
      QuestReward reward = rewardAgent.Rewards[index];
      GameObject gameObject = this.pool.AddChild(this.rewardContainer.gameObject);
      gameObject.SetActive(true);
      QuestRewardSlotEmpire component = gameObject.GetComponent<QuestRewardSlotEmpire>();
      component.Setup(reward);
      this.rewards.Add(component);
    }
    this.rewardContainer.repositionNow = true;
    this.rewardContainer.Reposition();
    this.scroll.ResetPosition();
  }

  public void OpenOrCloseItem()
  {
    this.SelectedIndex = this.SelectedIndex != 0 ? 0 : 1;
    this.rewardContainer.Reposition();
    this.tabel.repositionNow = true;
  }

  public void StartQuest()
  {
    MessageHub.inst.GetPortByAction("Quest:startTimerQuest").SendRequest(Utils.Hash((object) "quest_id", (object) this.questId), new System.Action<bool, object>(this.StartCallBack), true);
  }

  private void StartCallBack(bool success, object result)
  {
    if (!success)
      return;
    PlayerData.inst.QuestManager.Public_StartAllianceQuest(this.questId);
  }

  public void ClaimQuest()
  {
    PlayerData.inst.QuestManager.ClaimQuest(this.questId);
  }

  public void SpeedUpQuest()
  {
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(this.questId);
    UIManager.inst.OpenPopup("SpeedUpPopup", (Popup.PopupParameter) new SpeedUpPopUp.Parameter()
    {
      jobId = timerQuestData.jobId,
      speedupType = ItemBag.ItemType.speedup
    });
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Local_TimerQuest.onDataUpdated += new System.Action<long>(this.UpdateHandler);
    PlayerData.inst.QuestManager.OnAllianceQuestStart += new System.Action<long>(this.ResetStartButton);
    PlayerData.inst.QuestManager.OnAllianceQuestEnd += new System.Action<long>(this.ResetStartButton);
  }

  private void UpdateHandler(long questId)
  {
    if (questId != this.questId)
      return;
    this.UpdateUIState();
  }

  private void RefreshProgressBar(int time)
  {
    TimerQuestData timerQuestData = DBManager.inst.DB_Local_TimerQuest.Get(this.questId);
    if (timerQuestData == null)
      return;
    AllianceQuestUIPresent present = timerQuestData.Present as AllianceQuestUIPresent;
    present.Refresh();
    this.remainTime.text = present.GetProgressContent();
    this.progressBar.value = present.GetProgressBarValue();
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshProgressBar);
    DBManager.inst.DB_Local_TimerQuest.onDataUpdated -= new System.Action<long>(this.UpdateHandler);
    PlayerData.inst.QuestManager.OnAllianceQuestStart -= new System.Action<long>(this.ResetStartButton);
    PlayerData.inst.QuestManager.OnAllianceQuestEnd -= new System.Action<long>(this.ResetStartButton);
  }

  public void Clear()
  {
    for (int index = 0; index < this.rewards.Count; ++index)
      this.pool.Release(this.rewards[index].gameObject);
    this.rewards.Clear();
    this.render.Clear();
    this.RemoveEventHandler();
  }
}
