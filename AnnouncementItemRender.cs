﻿// Decompiled with JetBrains decompiler
// Type: AnnouncementItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnnouncementItemRender : MonoBehaviour
{
  private const string IMAGE_PATH = "Texture/GUI_Textures/Recharge_NPC";
  public UILabel announcement;
  public UITexture bgImage;
  public UILabel goToButton;
  public UIScrollView scrollView;
  public UIButton buttonObj;
  private string content;
  private string buttonDes;
  private string linkUrl;

  public void FeedData(AnnouncementData data)
  {
    if (data == null)
      return;
    this.content = data.content;
    this.buttonDes = data.buttonDes;
    this.linkUrl = data.linkUrl;
  }

  public void Show()
  {
    NGUITools.SetActive(this.gameObject, true);
    this.UpdateUI();
  }

  public void Hide()
  {
    NGUITools.SetActive(this.gameObject, false);
  }

  public void OnLinkBtnClicked()
  {
    Application.OpenURL(this.linkUrl);
  }

  public void ClearData()
  {
    this.content = (string) null;
    this.buttonDes = (string) null;
    this.linkUrl = (string) null;
  }

  private void UpdateUI()
  {
    this.announcement.text = this.content;
    if (string.IsNullOrEmpty(this.buttonDes))
    {
      this.buttonObj.gameObject.SetActive(false);
    }
    else
    {
      this.buttonObj.gameObject.SetActive(true);
      this.goToButton.text = this.buttonDes;
    }
    this.LoadTexture();
    this.scrollView.ResetPosition();
  }

  private void LoadTexture()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.bgImage, "Texture/GUI_Textures/Recharge_NPC", (System.Action<bool>) null, false, true, string.Empty);
  }
}
