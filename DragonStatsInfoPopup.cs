﻿// Decompiled with JetBrains decompiler
// Type: DragonStatsInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DragonStatsInfoPopup : Popup
{
  public static string DRAGON_STATS_INFO_POPUP = "Dragon/DragonStatsInfoPopup";
  public UILabel boostLabel1;
  public UILabel boostLabel2;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI((orgParam as DragonStatsInfoPopup.Parameter).dragonInfo);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  private void UpdateUI(DragonInfo dragonInfo)
  {
    ConfigDragonTendencyBoostInfo data = ConfigManager.inst.DB_ConfigDragonTendencyBoost.GetData(PlayerData.inst.dragonData.tendency);
    this.boostLabel1.text = string.Format("{0:P2}", (object) dragonInfo.level_boost) + this.ConfigBoost(data.dark_boost);
    this.boostLabel2.text = string.Format("{0:P2}", (object) dragonInfo.level_boost) + this.ConfigBoost(data.light_boost);
  }

  private string ConfigBoost(string boost)
  {
    if ((double) float.Parse(boost) >= 0.0)
      return "[00FF00FF]+" + string.Format("{0:P2}", (object) boost);
    return "[FF0000FF]-" + string.Format("{0:P2}", (object) boost);
  }

  public void OnCloseButtonClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public DragonInfo dragonInfo;
  }
}
