﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillConfigSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DragonSkillConfigSlot : MonoBehaviour
{
  public UITexture m_SkillIcon;
  public UILabel m_SkillLevel;
  public GameObject m_Equipped;
  public UILabel m_extraLev;
  public GameObject m_rootStarLevel;
  public UILabel m_labelStarLevel;
  private string m_Usablity;
  private OnDragonSkillEquipCallback m_OnEquip;
  private ConfigDragonSkillGroupInfo m_DragonSkillGroup;
  private ConfigDragonSkillMainInfo m_DragonSkill;
  private Coroutine m_DoubleClickRoutine;

  public void SetData(string usability, ConfigDragonSkillGroupInfo dragonSkillGroupInfo, OnDragonSkillEquipCallback onEquip)
  {
    this.m_Usablity = usability;
    this.m_DragonSkillGroup = dragonSkillGroupInfo;
    this.m_DragonSkill = PlayerData.inst.dragonController.GetLearnedTopLevelSkillOfGroup(this.m_DragonSkillGroup.internalId);
    this.m_OnEquip = onEquip;
    this.UpdateUI();
  }

  public void OnSingleClicked()
  {
    if (this.m_DoubleClickRoutine != null)
    {
      this.StopCoroutine(this.m_DoubleClickRoutine);
      this.m_DoubleClickRoutine = (Coroutine) null;
    }
    this.m_DoubleClickRoutine = this.StartCoroutine(this.SingleClickTimeout());
  }

  [DebuggerHidden]
  private IEnumerator SingleClickTimeout()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonSkillConfigSlot.\u003CSingleClickTimeout\u003Ec__Iterator53()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnDoubleClicked()
  {
    if (this.m_DoubleClickRoutine != null)
    {
      this.StopCoroutine(this.m_DoubleClickRoutine);
      this.m_DoubleClickRoutine = (Coroutine) null;
    }
    if (this.m_OnEquip == null)
      return;
    this.m_OnEquip(this.m_DragonSkill.internalId);
  }

  public void UpdateUI()
  {
    this.m_rootStarLevel.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_SkillIcon, this.m_DragonSkillGroup.IconPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_SkillLevel.text = this.m_DragonSkill.level.ToString();
    int extarLevel = DBManager.inst.DB_Artifact.GetExtarLevel(PlayerData.inst.uid, this.m_DragonSkillGroup.internalId);
    this.m_extraLev.text = extarLevel <= 0 ? string.Empty : "+" + (object) extarLevel;
    this.m_Equipped.SetActive(PlayerData.inst.dragonData.Skills.IsSetupSkill(this.m_Usablity, this.m_DragonSkill.internalId));
    if (PlayerData.inst.dragonData == null)
      return;
    int skillGroupStarLevel = PlayerData.inst.dragonData.GetSkillGroupStarLevel(this.m_DragonSkill.group_id);
    if (skillGroupStarLevel <= 0)
      return;
    this.m_rootStarLevel.SetActive(true);
    this.m_labelStarLevel.text = skillGroupStarLevel.ToString();
  }
}
