﻿// Decompiled with JetBrains decompiler
// Type: PlayerModeratorInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class PlayerModeratorInfo
{
  private const string dataKey = "moderator";
  private const string uidStr = "uid";
  private const string worldIdStr = "world_id";
  private const string modnameStr = "mod_name";
  private const string modlangStr = "mod_language";
  public long uid;
  public int world_id;
  public string mod_name;
  public string mod_language;
  public bool IsModerator;
  private bool openModMode;

  public bool OpenModMode
  {
    get
    {
      return this.openModMode;
    }
    set
    {
      this.openModMode = value;
    }
  }

  public void InitFromHashtable(Hashtable playerrootdata)
  {
    if (!playerrootdata.ContainsKey((object) "moderator"))
      return;
    ArrayList arrayList = playerrootdata[(object) "moderator"] as ArrayList;
    if (arrayList.Count <= 0)
      return;
    Hashtable inData = arrayList[0] as Hashtable;
    DatabaseTools.UpdateData(inData, "uid", ref this.uid);
    DatabaseTools.UpdateData(inData, "world_id", ref this.world_id);
    DatabaseTools.UpdateData(inData, "mod_name", ref this.mod_name);
    DatabaseTools.UpdateData(inData, "mod_language", ref this.mod_language);
    this.IsModerator = true;
  }

  public void Init()
  {
    MessageHub.inst.GetPortByAction("moderator").AddEvent(new System.Action<object>(this.OnModeratorInfoChanged));
  }

  private void OnModeratorInfoChanged(object data)
  {
    Hashtable inData = data as Hashtable;
    long outData1 = 0;
    DatabaseTools.UpdateData(inData, "uid", ref outData1);
    if (outData1 != PlayerData.inst.hostPlayer.uid)
      return;
    int outData2 = 0;
    DatabaseTools.UpdateData(inData, "mod_status", ref outData2);
    bool flag = outData2 != 0;
    if (flag != this.IsModerator)
      PlayerData.inst.mail.DeleteMailsByCategoryLocal(MailCategory.Mod);
    this.IsModerator = flag;
    DatabaseTools.UpdateData(inData, "world_id", ref this.world_id);
    DatabaseTools.UpdateData(inData, "mod_name", ref this.mod_name);
    DatabaseTools.UpdateData(inData, "mod_language", ref this.mod_language);
  }
}
