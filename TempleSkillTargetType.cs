﻿// Decompiled with JetBrains decompiler
// Type: TempleSkillTargetType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TempleSkillTargetType
{
  public const string SELF_ALLIANCE = "self_alliance";
  public const string TARGET_ALLIANCE = "target_alliance";
  public const string OPS_ALLIANCE_TEMPLE = "ops_alliance_temple";
  public const string OPS_CITY = "ops_city";
  public const string AREA = "area";
  public const string SELF_HIVE_OPS_CITY = "self_hive_ops_city";
}
