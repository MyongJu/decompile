﻿// Decompiled with JetBrains decompiler
// Type: TimerChestManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimerChestManager : MonoBehaviour
{
  private readonly string[] TimeChestDisplaySelections = new string[3]
  {
    "month_card",
    "iap_package",
    "artifact_limit"
  };
  public const string MonthCardCode = "month_card";
  public const string IAPCode = "iap_package";
  public const string ArtifactLimitCode = "artifact_limit";
  public TimerChestAnimControler ct_anim;
  private int _refreshTime;
  private TimerChestPopup.Parameter popupParam;
  private bool hasListenGrabDataChange;
  private int[] grabCodeSeletionsMap;
  private double[] selectionsRate;
  private string[] packageGroupIDsMap;
  private bool _giftUpdated;
  private bool _popupHiding;
  private bool hasSetEvent;
  public string openingDialog;
  private TimerChestManager.Data data;

  public event System.Action<int> OnRefreshTimeChange;

  public int refreshTime
  {
    get
    {
      return this._refreshTime;
    }
    private set
    {
      if (this._refreshTime == value)
        return;
      this._refreshTime = value;
    }
  }

  public void refreshChest()
  {
    this.refreshTime = DBManager.inst.DB_Local_TimerChest.chestUpdateTime;
  }

  public int leftTime
  {
    get
    {
      if (this.refreshTime > NetServerTime.inst.ServerTimestamp)
        return this.refreshTime - NetServerTime.inst.ServerTimestamp;
      return 0;
    }
  }

  public bool popupHiding
  {
    get
    {
      return this._popupHiding;
    }
    set
    {
      if (this._popupHiding == value)
        return;
      this._popupHiding = value;
      if (value)
      {
        this.hasSetEvent = false;
        Oscillator.Instance.updateEvent += new System.Action<double>(this.waitingPopupShow);
      }
      else
        Oscillator.Instance.updateEvent -= new System.Action<double>(this.waitingPopupShow);
    }
  }

  public void waitingPopupShow(double time)
  {
    if (UIManager.inst.IsTheCurrentDialog(this.openingDialog) && !this.hasSetEvent)
    {
      UI.Dialog dlg = UIManager.inst.GetDlg<UI.Dialog>(this.openingDialog);
      if ((UnityEngine.Object) dlg == (UnityEngine.Object) null)
      {
        Debug.Log((object) this.openingDialog);
        return;
      }
      UIButton btBack = dlg.uiManagerPanel.BT_Back;
      if ((UnityEngine.Object) btBack != (UnityEngine.Object) null)
      {
        btBack.gameObject.SetActive(true);
        btBack.onClick.Clear();
        btBack.onClick.Add(new EventDelegate((MonoBehaviour) this, "ReturnToTimerChest"));
      }
      this.hasSetEvent = true;
    }
    else if (!UIManager.inst.IsTheCurrentDialog(this.openingDialog) && this.hasSetEvent)
      this.hasSetEvent = false;
    if (!UIManager.inst.IsDialogClear)
      return;
    this.popupHiding = false;
  }

  private void updatePackageGroupIDsMap()
  {
    List<IAPStorePackage> availablePackages = IAPStorePackagePayload.Instance.GetAvailablePackages();
    int length = 0;
    List<IAPStorePackage>.Enumerator enumerator1 = availablePackages.GetEnumerator();
    while (enumerator1.MoveNext())
    {
      IAPPackageGroupInfo packageGroupInfo = ConfigManager.inst.DB_IAPPackageGroup.Get(enumerator1.Current.group.id);
      length += packageGroupInfo != null ? packageGroupInfo.chestDisplayRate : 1;
    }
    this.packageGroupIDsMap = new string[length];
    int index = 0;
    List<IAPStorePackage>.Enumerator enumerator2 = availablePackages.GetEnumerator();
    while (enumerator2.MoveNext())
    {
      IAPPackageGroupInfo packageGroupInfo = ConfigManager.inst.DB_IAPPackageGroup.Get(enumerator2.Current.group.id);
      int num1 = packageGroupInfo != null ? packageGroupInfo.chestDisplayRate : 1;
      int num2 = 0;
      while (num2 < num1)
      {
        this.packageGroupIDsMap[index] = enumerator2.Current.group.id;
        ++num2;
        ++index;
      }
    }
  }

  private string getNextGrabSelection()
  {
    double num1 = (double) Utils.RndRange(0.0f, 1f);
    double num2 = 0.0;
    MonthCardData monthCardData = DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid);
    string str = "artifact_limit";
    for (int index = 0; index < this.selectionsRate.Length; ++index)
    {
      double num3 = num2;
      num2 += this.selectionsRate[index];
      if (num1 >= num3 && num1 < num2)
      {
        string displaySelection = this.TimeChestDisplaySelections[this.grabCodeSeletionsMap[index]];
        if ((!(displaySelection == "month_card") || NetServerTime.inst.UpdateTime >= monthCardData.expiration_date) && (!(displaySelection == "iap_package") || this.packageGroupIDsMap.Length > 0))
          str = displaySelection;
      }
    }
    return str;
  }

  private void InitPackageInfo()
  {
    double num1 = 0.0;
    List<double> doubleList = new List<double>();
    List<int> intList = new List<int>();
    for (int index = 0; index < this.TimeChestDisplaySelections.Length; ++index)
    {
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData(string.Format("timer_chest_display_{0}_rate", (object) this.TimeChestDisplaySelections[index]));
      if (data != null && data.ValueString != "0" && data.ValueString != "0.0")
      {
        doubleList.Add(data.Value);
        intList.Add(index);
        num1 += data.Value;
      }
    }
    double num2 = num1 != 0.0 ? num1 : 1.0;
    this.selectionsRate = doubleList.ToArray();
    this.grabCodeSeletionsMap = intList.ToArray();
    for (int index = 0; index < this.selectionsRate.Length; ++index)
      this.selectionsRate[index] /= num2;
    this.updatePackageGroupIDsMap();
  }

  public void OnGrabDataChange()
  {
    this.UpdateGrab();
  }

  public TimerChestPopup.Parameter UpdateGrab()
  {
    this.updatePackageGroupIDsMap();
    this.popupParam.packageGroup = (IAPPackageGroupInfo) null;
    this.popupParam.grabCode = this.getNextGrabSelection();
    if (this.popupParam.grabCode == "iap_package" && this.packageGroupIDsMap.Length > 0)
      this.popupParam.packageGroup = ConfigManager.inst.DB_IAPPackageGroup.Get(this.packageGroupIDsMap[Utils.RndRange(0, this.packageGroupIDsMap.Length)]);
    return this.popupParam;
  }

  private string GetPackageIdByGroup(IAPPackageGroupInfo group)
  {
    if (group == null || string.IsNullOrEmpty(group.id))
      return string.Empty;
    string id = group.id;
    IAPStorePackage iapStorePackage = (IAPStorePackage) null;
    List<IAPStorePackage>.Enumerator enumerator = IAPStorePackagePayload.Instance.GetAvailablePackages().GetEnumerator();
    while (enumerator.MoveNext())
    {
      iapStorePackage = enumerator.Current;
      if (string.Compare(iapStorePackage.group.id, id) == 0)
        return iapStorePackage.id;
    }
    if (iapStorePackage != null)
      return iapStorePackage.id;
    return string.Empty;
  }

  private void PushTimerChestClickBI()
  {
    TimerChestManager.TimerChestShowBIItem timerChestShowBiItem = new TimerChestManager.TimerChestShowBIItem();
    timerChestShowBiItem.d_c1 = new TimerChestManager.TimerChestShowBICell();
    timerChestShowBiItem.d_c1.key = "grab_type";
    timerChestShowBiItem.d_c1.value = this.popupParam.grabCode;
    timerChestShowBiItem.d_c2 = new TimerChestManager.TimerChestShowBICell();
    timerChestShowBiItem.d_c2.key = "package_id";
    timerChestShowBiItem.d_c2.value = this.GetPackageIdByGroup(this.popupParam.packageGroup);
    string properties = Utils.Object2Json((object) timerChestShowBiItem);
    if (Application.isEditor || !FunplusSdk.Instance.IsSdkInstalled())
      return;
    FunplusBi.Instance.TraceEvent("timer_chest_click", properties);
  }

  public void Init()
  {
    if (this.ct_anim == null)
      this.ct_anim = new TimerChestAnimControler();
    this.refreshTime = DBManager.inst.DB_Local_TimerChest.chestUpdateTime;
    this.popupParam = new TimerChestPopup.Parameter();
    this.popupParam.manager = this;
    this._giftUpdated = false;
    this.InitPackageInfo();
    if (this.hasListenGrabDataChange)
      return;
    IAPStorePackagePayload.Instance.OnPackageDataChanged += new System.Action(this.OnGrabDataChange);
    this.hasListenGrabDataChange = true;
  }

  public bool isPrepare
  {
    get
    {
      return this.refreshTime < NetServerTime.inst.ServerTimestamp;
    }
  }

  public void resetRefreshTime()
  {
    this._giftUpdated = false;
    this.refreshTime = DBManager.inst.DB_Local_TimerChest.chestUpdateTime;
  }

  public void ReturnToTimerChest()
  {
    this.popupHiding = false;
    UIManager.inst.OpenPopup("TimerChestPopup", (Popup.PopupParameter) this.popupParam);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OpenTimerChestPopup()
  {
    this.UpdateGrab();
    this.UpdateChestGift();
    this.PushTimerChestClickBI();
  }

  private void UpdateChestGift()
  {
    MessageHub.inst.GetPortByAction("Gift:getTimerChestInfo").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnGiftUpdate), true);
  }

  private void OnGiftUpdate(bool result, object orgParam)
  {
    this._giftUpdated = false;
    if (!result)
      return;
    Hashtable hashtable = orgParam as Hashtable;
    if (hashtable == null)
      return;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    if (!enumerator.MoveNext())
      return;
    int result1 = 0;
    int result2 = 0;
    if (!int.TryParse(enumerator.Current.ToString(), out result1) || !int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
      return;
    this.popupParam.giftId = result1;
    this.popupParam.giftCount = result2;
    this.popupHiding = false;
    UIManager.inst.OpenPopup("TimerChestPopup", (Popup.PopupParameter) this.popupParam);
    this._giftUpdated = true;
  }

  public void ClaimChest()
  {
    if (!this.isPrepare)
      return;
    MessageHub.inst.GetPortByAction("Gift:openTimerChest").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnClaimCallback), true);
  }

  private void OnClaimCallback(bool result, object orgHt)
  {
    if (result)
    {
      Hashtable hashtable = orgHt as Hashtable;
      if (hashtable == null)
        return;
      IEnumerator enumerator = hashtable.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        int result1 = 0;
        int result2 = 0;
        if (int.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
          UIManager.inst.OpenPopup("TimerChestConfirmPopup", (Popup.PopupParameter) new TimerChestConfirmPopup.Parameter()
          {
            itemId = result1,
            itemCount = result2
          });
      }
      this._giftUpdated = false;
    }
    this.refreshTime = DBManager.inst.DB_Local_TimerChest.chestUpdateTime;
    if (this.OnRefreshTimeChange == null)
      return;
    this.OnRefreshTimeChange(this.refreshTime);
  }

  public void OnDataChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this.refreshTime = PlayerData.inst.userData.timeChestUpdateTime;
  }

  private class TimerChestShowBICell
  {
    public string key;
    public string value;
  }

  private class TimerChestShowBIItem
  {
    public TimerChestManager.TimerChestShowBICell d_c1;
    public TimerChestManager.TimerChestShowBICell d_c2;
  }

  public class Data
  {
    public int refreshCount;
    public int updateTime;
  }
}
