﻿// Decompiled with JetBrains decompiler
// Type: AllianceWar_MainDlg_Skill_Defense_Slot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class AllianceWar_MainDlg_Skill_Defense_Slot : AllianceWar_MainDlg_Skill_Slot
{
  public override void OnSkillButtonClick()
  {
    if (!this._data.dataExist)
      return;
    TempleSkillInfo bySkillType = ConfigManager.inst.DB_TempleSkill.GetBySkillType("interrupt");
    if (bySkillType == null)
      return;
    UIManager.inst.OpenPopup("Alliance/DragonAltarSkillInfoPopup", (Popup.PopupParameter) new SkillInfoPopup.Parameter()
    {
      itemId = bySkillType.internalId,
      attackingAllianceId = this._data.magic.AllianceId
    });
  }

  public override void OnSecond(int serverTime)
  {
    if (!this._data.dataExist)
      return;
    if (this._data.skillInfo.MaxDonation != 0)
    {
      this.panel.BT_PowerUp.gameObject.SetActive(true);
      this.panel.powerProgress.gameObject.SetActive(true);
      this.panel.powerProgress.value = (float) this._data.magic.Donation / (float) this._data.skillInfo.MaxDonation;
      this.panel.powerText.text = string.Format("{0} {1}/{2}({3}%)", (object) Utils.XLAT("alliance_altar_spell_power"), (object) this._data.magic.Donation.ToString(), (object) this._data.skillInfo.MaxDonation, (object) (float) ((double) this.panel.powerProgress.value * 100.0));
    }
    else
    {
      this.panel.BT_PowerUp.gameObject.SetActive(false);
      this.panel.powerProgress.gameObject.SetActive(false);
    }
    int time = (int) this._data.magic.TimerEnd - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
    {
      this.panel.chargingTimeProgress.value = (float) (1.0 - (double) time / (double) this._data.skillInfo.DonationTime);
      this.panel.chargingTimeText.text = string.Format("{0} {1}", (object) Utils.XLAT("alliance_altar_charge_time_remaining"), (object) Utils.FormatTime1(time));
    }
    else
    {
      this.panel.chargingTimeProgress.value = 1f;
      this.panel.chargingTimeText.text = string.Empty;
    }
  }
}
