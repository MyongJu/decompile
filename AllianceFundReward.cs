﻿// Decompiled with JetBrains decompiler
// Type: AllianceFundReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceFundReward : QuestReward
{
  public readonly int fund;

  public AllianceFundReward(int inExp)
  {
    this.fund = inExp;
  }

  public int activeValue
  {
    get
    {
      return this.fund;
    }
  }

  public override int GetValue()
  {
    return this.fund;
  }

  public override void Claim()
  {
    base.Claim();
  }

  public override string GetRewardIconName()
  {
    return Utils.GetUITextPath() + "alliancequest_reward_fund";
  }

  public override string GetRewardTypeName()
  {
    return ScriptLocalization.Get("alliance_fund", true);
  }

  public override string GetValueText()
  {
    return this.activeValue.ToString();
  }
}
