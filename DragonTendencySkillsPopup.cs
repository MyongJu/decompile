﻿// Decompiled with JetBrains decompiler
// Type: DragonTendencySkillsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonTendencySkillsPopup : Popup
{
  private List<DragonTendencySkillItemRenderer> m_ItemList = new List<DragonTendencySkillItemRenderer>();
  private List<DragonTendencySkillLevelRenderer> m_LevelList = new List<DragonTendencySkillLevelRenderer>();
  public GameObject m_ItemPrefab;
  public UIGrid m_Grid;
  public UILabel m_SkillName1;
  public UILabel m_SkillName2;
  public UILabel m_SkillLevel;
  public UILabel m_SkillBonus;
  public UILabel m_SkillDescription;
  public UILabel m_RequirementType;
  public UILabel m_RequirementTypeTitle;
  public UILabel m_SkillRequirement;
  public GameObject m_ActivePanel;
  public GameObject m_NotActivePanel;
  public UIScrollView m_LevelScrollView;
  public UIGrid m_LevelGrid;
  public GameObject m_OddPrefab;
  public GameObject m_EvenPrefab;
  public GameObject m_CurrentPrefab;
  public GameObject m_OddWithoutRequirementPrefab;
  public GameObject m_EvenWithoutRequirementPrefab;
  public GameObject m_CurrentWithoutRequirementPrefab;
  public GameObject m_rootCanActive;
  public GameObject m_rootCannotActive;
  public GameObject m_rootRequirement;
  public GameObject _rootTitleWithRequirement;
  public GameObject _rootTitleWithoutRequirement;
  private DragonTendencySkillsPopup.Parameter m_Parameter;
  private int m_SelectedIndex;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as DragonTendencySkillsPopup.Parameter;
    this.m_SelectedIndex = -1;
    this.m_OddPrefab.SetActive(false);
    this.m_EvenPrefab.SetActive(false);
    this.m_CurrentPrefab.SetActive(false);
    this.m_OddWithoutRequirementPrefab.SetActive(false);
    this.m_EvenWithoutRequirementPrefab.SetActive(false);
    this.m_CurrentWithoutRequirementPrefab.SetActive(false);
    this.m_ItemPrefab.SetActive(false);
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearData();
    this.ClearLevelList();
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
  }

  private void OnDragonDataChanged(long dragonId)
  {
    if (dragonId != PlayerData.inst.uid)
      return;
    this.UpdateCurrentSelect();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ClearData()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      DragonTendencySkillItemRenderer skillItemRenderer = this.m_ItemList[index];
      skillItemRenderer.gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) skillItemRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  private void UpdateUI()
  {
    this.ClearData();
    int tendencySkillCount = DragonUtils.GetDragonTendencySkillCount();
    for (int index = 0; index < tendencySkillCount; ++index)
    {
      ConfigDragonTendencyBoostInfo dragonTendencyBoost = ConfigManager.inst.DB_ConfigDragonTendencyBoost.Get(DragonUtils.DRAGON_TENDENCY_SKILL_IDS[index]);
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.transform.parent = this.m_Grid.transform;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(true);
      DragonTendencySkillItemRenderer component = gameObject.GetComponent<DragonTendencySkillItemRenderer>();
      component.SetData(dragonTendencyBoost, this.m_Parameter.dragonData);
      this.m_ItemList.Add(component);
    }
    this.m_Grid.Reposition();
    int index1 = DragonUtils.GetDragonTendencySkillIDIndex(this.m_Parameter.dragonData);
    if (this.m_Parameter.startIndex != -1)
      index1 = this.m_Parameter.startIndex;
    UIToggle[] componentsInChildren = this.m_ItemList[index1].GetComponentsInChildren<UIToggle>(true);
    if (componentsInChildren.Length <= 0)
      return;
    componentsInChildren[0].Set(true);
  }

  private void UpdateDetail(ConfigDragonTendencyBoostInfo boostInfo)
  {
    using (List<DragonTendencySkillItemRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateUI();
    }
    this.m_SkillName1.text = Utils.XLAT(boostInfo.localization);
    this.m_SkillName2.text = Utils.XLAT(boostInfo.localization);
    this.m_SkillDescription.text = Utils.XLAT(boostInfo.skill_description);
    DragonInfo dragonInfoByLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonInfoByLevel(this.m_Parameter.dragonData.Level);
    this.m_SkillLevel.text = string.Format("Lv.{0}", (object) this.m_Parameter.dragonData.Level);
    float displayValue = boostInfo.display_value;
    this.m_SkillBonus.text = ((double) dragonInfoByLevel.tendency_skill_rate * (double) displayValue * 100.0).ToString() + "%";
    bool flag1 = DragonUtils.GetDragonTendencySkillID(this.m_Parameter.dragonData) == boostInfo.ID;
    this.m_ActivePanel.SetActive(flag1);
    this.m_NotActivePanel.SetActive(!flag1);
    bool flag2 = DragonUtils.IsFormationEnabled(boostInfo.ID);
    this.m_rootCanActive.SetActive(flag2);
    this.m_rootCannotActive.SetActive(!flag2);
    int dark;
    int light;
    DragonUtils.GetFormationRequirement(boostInfo.ID, out dark, out light);
    int num1 = dark <= 0 ? light : dark;
    int num2 = dark <= 0 ? this.m_Parameter.dragonData.Skill_cost.totalLightPoint : this.m_Parameter.dragonData.Skill_cost.totalDarkPoint;
    bool flag3 = false;
    bool flag4 = true;
    if (boostInfo.ID == "dark_2" || boostInfo.ID == "dark_1")
      flag3 = true;
    else if (!(boostInfo.ID == "light_2") && !(boostInfo.ID == "light_1"))
      flag4 = false;
    if (flag4)
    {
      this.m_RequirementType.text = !flag3 ? ScriptLocalization.Get("dragon_skill_guardian_points_needed", true) : ScriptLocalization.Get("dragon_skill_assault_points_needed", true);
      this.m_RequirementTypeTitle.text = !flag3 ? ScriptLocalization.Get("dragon_skill_required_guardian_name", true) : ScriptLocalization.Get("dragon_skill_required_assault_name", true);
    }
    this.m_rootRequirement.SetActive(flag4);
    this.m_SkillRequirement.text = !flag2 ? string.Format("[FF0000FF]{0}[-][B3B3B3FF]/{1}[-]", (object) num2, (object) num1) : string.Format("[B3B3B3FF]{0}/{1}[-]", (object) num2, (object) num1);
    this.ClearLevelList();
    this._rootTitleWithoutRequirement.SetActive(boostInfo.ID == "normal");
    this._rootTitleWithRequirement.SetActive(boostInfo.ID != "normal");
    int tendencySkillLevel1 = dragonInfoByLevel.tendency_skill_level;
    int tendencySkillLevelCount = ConfigManager.inst.DB_ConfigDragon.GetDragonTendencySkillLevelCount();
    for (int index = 0; index < tendencySkillLevelCount; ++index)
    {
      int tendencySkillLevel2 = index + 1;
      GameObject gameObject = tendencySkillLevel1 != tendencySkillLevel2 ? ((index & 1) != 0 ? UnityEngine.Object.Instantiate<GameObject>(!(boostInfo.ID == "normal") ? this.m_OddPrefab : this.m_OddWithoutRequirementPrefab) : UnityEngine.Object.Instantiate<GameObject>(!(boostInfo.ID == "normal") ? this.m_EvenPrefab : this.m_EvenWithoutRequirementPrefab)) : UnityEngine.Object.Instantiate<GameObject>(!(boostInfo.ID == "normal") ? this.m_CurrentPrefab : this.m_CurrentWithoutRequirementPrefab);
      if ((bool) ((UnityEngine.Object) gameObject))
      {
        gameObject.transform.parent = this.m_LevelGrid.transform;
        gameObject.transform.localScale = Vector3.one;
        gameObject.SetActive(true);
        DragonInfo skillUnlockLevel = ConfigManager.inst.DB_ConfigDragon.GetDragonTendencySkillUnlockLevel(tendencySkillLevel2);
        DragonTendencySkillLevelRenderer component = gameObject.GetComponent<DragonTendencySkillLevelRenderer>();
        DragonUtils.GetFormationRequirement(boostInfo.ID, skillUnlockLevel.level, out dark, out light);
        component.SetData(skillUnlockLevel.level, dark <= 0 ? light : dark, skillUnlockLevel.tendency_skill_rate * displayValue);
        this.m_LevelList.Add(component);
      }
    }
    this.m_LevelGrid.Reposition();
    this.m_LevelScrollView.ResetPosition();
  }

  private void ClearLevelList()
  {
    for (int index = 0; index < this.m_LevelList.Count; ++index)
    {
      DragonTendencySkillLevelRenderer level = this.m_LevelList[index];
      level.gameObject.SetActive(false);
      level.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) level.gameObject);
    }
    this.m_LevelList.Clear();
  }

  public void OnToggleChanged()
  {
    this.UpdateCurrentSelect();
  }

  protected void UpdateCurrentSelect()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == -1)
      return;
    this.m_SelectedIndex = selectedIndex;
    this.UpdateDetail(this.m_ItemList[this.m_SelectedIndex].tendencyBoost);
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.m_ItemList.Count; ++index)
    {
      UIToggle[] componentsInChildren = this.m_ItemList[index].GetComponentsInChildren<UIToggle>(true);
      if (componentsInChildren.Length > 0 && componentsInChildren[0].value)
        return index;
    }
    return 2;
  }

  public void OnButtonActiveClicked()
  {
    if (this.m_SelectedIndex == -1 || this.m_SelectedIndex >= this.m_ItemList.Count)
      return;
    ConfigDragonTendencyBoostInfo tendencyBoost = this.m_ItemList[this.m_SelectedIndex].tendencyBoost;
    if (tendencyBoost == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "tendency"] = (object) DragonUtils.GetTendency(tendencyBoost.ID);
    RequestManager.inst.SendRequest("Dragon:changeTendency", postData, (System.Action<bool, object>) null, true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int startIndex = -1;
    public DragonData dragonData;
  }
}
