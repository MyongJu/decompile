﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityCurrentStepDetail
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PersonalActivityCurrentStepDetail : MonoBehaviour
{
  private Vector3 startPoint = Vector3.zero;
  public UISlider progressSlider;
  public int widhtSize;
  public UILabel[] scores;
  private bool init;
  public TargetScoreDividerItem scoreItem;
  public PersonalActivityRewardContainer containter;
  private ActivityBaseData data;

  public void Clear()
  {
    this.containter.Clear();
  }

  public void UpdateUI(ActivityBaseData data)
  {
    if (this.init)
      return;
    this.startPoint.x = -765f;
    this.startPoint.y = 314f;
    PersonalActivityData personalActivityData = data as PersonalActivityData;
    this.progressSlider.value = personalActivityData.Progress;
    int count = personalActivityData.CurrentRoundTargetScores.Count;
    for (int index = 0; index < count; ++index)
    {
      TargetScoreDividerItem scoreDividerItem = this.GetItem();
      scoreDividerItem.SetData(personalActivityData.CurrentRoundTargetScores[index], index == count - 1);
      Vector3 localPosition = scoreDividerItem.transform.localPosition;
      localPosition.x = this.startPoint.x + (float) this.widhtSize * ((float) (index + 1) / (float) count);
      localPosition.y = this.startPoint.y;
      scoreDividerItem.transform.localPosition = localPosition;
    }
    this.containter.Refresh(data);
    this.init = true;
  }

  private TargetScoreDividerItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.gameObject, this.scoreItem.gameObject);
    NGUITools.SetActive(go, true);
    return go.GetComponent<TargetScoreDividerItem>();
  }

  public void OnViewPlayerScore()
  {
  }

  public void OnHowToGetPoints()
  {
  }
}
