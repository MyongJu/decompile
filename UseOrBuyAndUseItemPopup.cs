﻿// Decompiled with JetBrains decompiler
// Type: UseOrBuyAndUseItemPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class UseOrBuyAndUseItemPopup : Popup
{
  public UILabel titleLabel;
  public UILabel itemNameLabel;
  public UILabel itemDescLabel;
  public UITexture itemIconTexture;
  public UIButton actionBtn;
  public UITexture btnItemIconTexture;
  public GameObject goldGameobject;
  public UILabel ownedValueLabel;
  public UILabel useValueLabel;
  public UILabel actionBtnLabel;
  private UseOrBuyAndUseItemPopup.Parameter m_Parameter;
  private ItemStaticInfo itemStaticInfo;
  private ShopStaticInfo shopStaticInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_Parameter = orgParam as UseOrBuyAndUseItemPopup.Parameter;
    this.itemStaticInfo = this.m_Parameter.itemStaticInfo;
    this.shopStaticInfo = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(this.itemStaticInfo.internalId);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.titleLabel.text = this.m_Parameter.titleText;
    this.actionBtnLabel.text = this.m_Parameter.btnText;
    this.itemNameLabel.text = this.itemStaticInfo.LocName;
    this.itemDescLabel.text = this.itemStaticInfo.LocDescription;
    int itemCount = ItemBag.Instance.GetItemCount(this.itemStaticInfo.internalId);
    this.ownedValueLabel.text = (itemCount > 0 ? (object) "[00FF00FF]" : (object) "[FF0000FF]").ToString() + (object) itemCount + "[-]";
    BuilderFactory.Instance.HandyBuild((UIWidget) this.itemIconTexture, this.m_Parameter.itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    if (itemCount <= 0)
    {
      this.btnItemIconTexture.gameObject.SetActive(false);
      this.goldGameobject.SetActive(true);
      Utils.SetPriceToLabel(this.useValueLabel, ItemBag.Instance.GetShopItemPrice(this.shopStaticInfo.ID));
      this.actionBtn.onClick.Clear();
      this.actionBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnGetAndUseBtnPressed)));
    }
    else
    {
      this.useValueLabel.color = Color.white;
      this.useValueLabel.text = "1";
      this.btnItemIconTexture.gameObject.SetActive(true);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.btnItemIconTexture, this.m_Parameter.itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.goldGameobject.SetActive(false);
      this.actionBtn.onClick.Clear();
      this.actionBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnUseBtnPressed)));
    }
  }

  public void OnUseBtnPressed()
  {
    ItemBag.Instance.UseItem(this.itemStaticInfo.internalId, 1, this.m_Parameter.param, (System.Action<bool, object>) ((arg1, arg2) => this.OnUse(arg1, arg2)));
  }

  public void OnGetAndUseBtnPressed()
  {
    if (ItemBag.Instance.GetShopItemPrice(this.shopStaticInfo.ID) > PlayerData.inst.hostPlayer.Currency)
    {
      Utils.ShowNotEnoughGoldTip();
      this.OnCloseButtonClick();
      if (this.m_Parameter.goldNotEnoughCallback == null)
        return;
      this.m_Parameter.goldNotEnoughCallback();
    }
    else
      ItemBag.Instance.BuyAndUseShopItem(this.shopStaticInfo.ID, this.m_Parameter.param, (System.Action<bool, object>) ((arg1, arg2) => this.OnUse(arg1, arg2)), 1);
  }

  public void OnUse(bool arg1, object arg2)
  {
    if (this.m_Parameter.callback != null)
      this.m_Parameter.callback(arg1, arg2);
    this.OnCloseButtonClick();
  }

  public void OnCloseButtonClick()
  {
    BuilderFactory.Instance.Release((UIWidget) this.btnItemIconTexture);
    BuilderFactory.Instance.Release((UIWidget) this.itemIconTexture);
    this.actionBtn.onClick.Clear();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleText;
    public string btnText;
    public Hashtable param;
    public ItemStaticInfo itemStaticInfo;
    public System.Action<bool, object> callback;
    public System.Action goldNotEnoughCallback;
  }
}
