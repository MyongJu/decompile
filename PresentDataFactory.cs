﻿// Decompiled with JetBrains decompiler
// Type: PresentDataFactory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class PresentDataFactory
{
  private static PresentDataFactory _instance;

  public static PresentDataFactory Instance
  {
    get
    {
      if (PresentDataFactory._instance == null)
        PresentDataFactory._instance = new PresentDataFactory();
      return PresentDataFactory._instance;
    }
  }

  public bool IsFinish(PresentData data)
  {
    bool flag = false;
    switch (data.Type)
    {
      case PresentData.DataType.Quest:
        flag = DBManager.inst.DB_Quest.Get(data.DataID).Status == 1;
        break;
      case PresentData.DataType.Achievement:
        flag = DBManager.inst.DB_Achievements.Get((int) data.DataID).State == 1;
        break;
    }
    return flag;
  }

  public PresentData CreateData(PresentData.DataType type, int configId, long dataId)
  {
    PresentData presentData = (PresentData) null;
    switch (type)
    {
      case PresentData.DataType.Quest:
        presentData = this.CreateQuestData(configId);
        presentData.RewardIcon = this.GetRewardIcon(dataId);
        break;
      case PresentData.DataType.Achievement:
        presentData = this.CreateAchievementData(configId);
        break;
    }
    if (presentData != null)
    {
      presentData.Type = type;
      presentData.DataID = dataId;
      presentData.ConfigID = configId;
    }
    return presentData;
  }

  private PresentData CreateQuestData(int configId)
  {
    PresentData presentData = new PresentData();
    QuestInfo data = ConfigManager.inst.DB_Quest.GetData(configId);
    presentData.Count = data.Value;
    presentData.Image = data.Image;
    presentData.Formula = data.Formula;
    presentData.ImageParentPath = "Texture/QuestIcons/";
    return presentData;
  }

  private string GetRewardIcon(long dataID)
  {
    QuestRewardAgent rewardAgent = Utils.GetRewardAgent(dataID);
    string str = string.Empty;
    List<QuestReward>.Enumerator enumerator = rewardAgent.Rewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current is ConsumableReward)
      {
        str = enumerator.Current.GetRewardIconName();
        break;
      }
    }
    if (string.IsNullOrEmpty(str))
      str = !rewardAgent.HadSilverReward() ? Utils.GetUITextPath() + rewardAgent.MaxResouceIcon : Utils.GetUITextPath() + rewardAgent.SilverIcon;
    return str;
  }

  private PresentData CreateAchievementData(int configId)
  {
    PresentData presentData = new PresentData();
    AchievementInfo data1 = ConfigManager.inst.DB_Achievement.GetData(configId);
    AchievementMainInfo data2 = ConfigManager.inst.DB_AchievementMain.GetData(data1.GroupID);
    presentData.Count = data1.Value;
    presentData.Image = data2.Image;
    presentData.Formula = data1.Formula;
    presentData.ImageParentPath = "Texture/Achievement/";
    return presentData;
  }
}
