﻿// Decompiled with JetBrains decompiler
// Type: BuildQueueIcon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class BuildQueueIcon : MonoBehaviour
{
  protected BuildQueueIcon.State state = BuildQueueIcon.State.IDLE;
  public UI2DSprite progress;
  public UILabel content;
  public UILabel time;
  public GameObject buildingIconParent;
  public BuildQueueIcon.BuildQueueIconType type;
  protected JobHandle jobHandle;
  protected Animator anim;
  protected QuestTipDlg questTipDlg;

  private void Awake()
  {
    this.anim = this.GetComponent<Animator>();
  }

  private void OnEnable()
  {
    this.ChangeState(this.state);
  }

  public virtual void Init()
  {
    UILabel time = this.time;
    string empty = string.Empty;
    this.content.text = empty;
    string str = empty;
    time.text = str;
    this.progress.gameObject.SetActive(false);
    this.ChangeState(BuildQueueIcon.State.IDLE);
  }

  protected void ChangeState(BuildQueueIcon.State target)
  {
    this.state = target;
    if (!(bool) ((UnityEngine.Object) this.anim))
      return;
    this.anim.SetInteger("State", (int) this.state);
  }

  private void LaterCall()
  {
    if (!this.enabled)
      return;
    this.anim.SetInteger("State", (int) this.state);
  }

  public void Dispose()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  [DebuggerHidden]
  public IEnumerator StartJob(JobHandle jobHandle)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BuildQueueIcon.\u003CStartJob\u003Ec__Iterator33()
    {
      jobHandle = jobHandle,
      \u003C\u0024\u003EjobHandle = jobHandle,
      \u003C\u003Ef__this = this
    };
  }

  private void AddBuildIcon()
  {
    if (this.jobHandle == null || this.jobHandle.Data == null)
      return;
    Hashtable data = this.jobHandle.Data as Hashtable;
    if (data != null && data.ContainsKey((object) "building_id"))
    {
      string s = data[(object) "building_id"].ToString();
      CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(long.Parse(s));
      if (buildingFromId == null)
        return;
      GameObject prefab = AssetManager.Instance.HandyLoad("Prefab/UI/BuildingPrefab/ui_" + ConfigManager.inst.DB_Building.GetData(buildingFromId.mType, buildingFromId.mLevel).Building_ImagePath + "_l1", (System.Type) null) as GameObject;
      if (!((UnityEngine.Object) prefab != (UnityEngine.Object) null))
        return;
      for (int index = 0; index < this.buildingIconParent.transform.childCount; ++index)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.buildingIconParent.transform.GetChild(index).gameObject);
      NGUITools.AddChild(this.buildingIconParent, prefab).transform.localScale = Vector3.one * 0.5f;
    }
    else
      D.error((object) "jobHander's data should have building_id");
  }

  private void OnJobRemove(long jobID)
  {
    if (this.jobHandle == null || jobID != this.jobHandle.GetJobID())
      return;
    this.Finish();
  }

  private void OnSecond(int timestamp)
  {
    this.UpdateUI(this.jobHandle);
  }

  public virtual void UpdateUI(JobHandle jobHandle)
  {
    this.progress.gameObject.SetActive(true);
    this.progress.fillAmount = (float) (1.0 - (double) jobHandle.LeftTime() / (double) jobHandle.Duration());
    if (!jobHandle.IsFinished() && jobHandle.LeftTime() <= CityManager.inst.GetFreeSpeedUpTime())
    {
      this.state = BuildQueueIcon.State.FREE;
      this.content.text = Utils.XLAT("id_uppercase_free");
      this.progress.color = new Color(0.454902f, 0.3686275f, 1f);
    }
    else
    {
      this.content.text = string.Empty;
      this.progress.color = new Color(0.0f, 0.3529412f, 0.4627451f);
    }
    if (jobHandle.LeftTime() < 0)
      return;
    this.time.text = Utils.FormatTime(jobHandle.LeftTime(), false, false, true);
  }

  public virtual void Finish()
  {
    UILabel time = this.time;
    string empty = string.Empty;
    this.content.text = empty;
    string str = empty;
    time.text = str;
    this.progress.gameObject.SetActive(false);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    this.ChangeState(BuildQueueIcon.State.IDLE);
  }

  public virtual void OnClick()
  {
    if (this.state == BuildQueueIcon.State.IDLE)
    {
      if ((UnityEngine.Object) this.questTipDlg == (UnityEngine.Object) null)
        this.questTipDlg = UnityEngine.Object.FindObjectOfType<QuestTipDlg>();
      if (!((UnityEngine.Object) this.questTipDlg != (UnityEngine.Object) null))
        return;
      this.questTipDlg.OnClickHandler();
    }
    else if (this.state == BuildQueueIcon.State.FREE)
    {
      long job_id = this.jobHandle.GetJobID();
      BuildingController buildingController = this.FocusBuilding();
      if ((UnityEngine.Object) null != (UnityEngine.Object) buildingController)
      {
        CityTimerbar componentInChildren = buildingController.GetComponentInChildren<CityTimerbar>();
        if ((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren)
          job_id = componentInChildren.FreeEndBuild();
      }
      if (job_id == 0L)
        return;
      ItemBag.Instance.UseFreeSpeedup(job_id, (System.Action<bool, object>) null);
    }
    else
    {
      if (this.state != BuildQueueIcon.State.BUSY)
        return;
      this.FocusBuilding();
    }
  }

  protected BuildingController FocusBuilding()
  {
    if (this.jobHandle != null && this.jobHandle.Data != null)
    {
      Hashtable data = this.jobHandle.Data as Hashtable;
      if (data != null)
      {
        if (data.ContainsKey((object) "building_id"))
        {
          string s = data[(object) "building_id"].ToString();
          CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(long.Parse(s));
          if (buildingFromId != null)
          {
            BuildingController fromBuildingItem = CitadelSystem.inst.GetBuildControllerFromBuildingItem(buildingFromId);
            if ((UnityEngine.Object) fromBuildingItem != (UnityEngine.Object) null)
            {
              UIManager.inst.cityCamera.FocusTarget(fromBuildingItem.transform, CityCamera.FocusMode.Center);
              return fromBuildingItem;
            }
          }
        }
        else
          D.error((object) "jobhanle doesn't have building_id in data");
      }
    }
    else
      D.error((object) "jobhandle or jobhandle's data is null");
    return (BuildingController) null;
  }

  public enum State
  {
    NONE,
    IDLE,
    BUSY,
    FREE,
  }

  public enum BuildQueueIconType
  {
    NORMAL,
    ADDITION,
  }
}
