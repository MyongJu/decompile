﻿// Decompiled with JetBrains decompiler
// Type: WorldBossDetaillDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class WorldBossDetaillDlg : UI.Dialog
{
  private int totalTime = 1;
  public UILabel title;
  public UILabel desc;
  public UITexture monsterIcon;
  public RewardNormalItem[] rewardItems;
  public UILabel spirit;
  public UIButton attackBt;
  public UILabel remainTimeLabel;
  public PVEMonsterPreview previewPrefab;
  private Coordinate _targetLocation;
  private TileData _tileData;
  private GameObject monster;
  private PVEMonsterPreview preview;
  public UIProgressBar progress;
  public UILabel progressValue;
  private bool _open;
  private int remainTime;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    WorldBossDetaillDlg.Parameter parameter = orgParam as WorldBossDetaillDlg.Parameter;
    if (orgParam != null)
    {
      this._targetLocation = parameter.coordinate;
      this._tileData = PVPMapData.MapData.GetReferenceAt(this._targetLocation);
    }
    this.monster = UnityEngine.Object.Instantiate<GameObject>(this.previewPrefab.gameObject);
    NGUITools.SetActive(this.monster, true);
    this.preview = this.monster.GetComponent<PVEMonsterPreview>();
    NGUITools.SetLayer(this.monster, LayerMask.NameToLayer("UI3D"));
    this.preview.InitializeCharacter(this._tileData);
    this.UpdateUI();
    this.AddEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_WorldBossDB.onDataRemoved += new System.Action(this.OnRemoveData);
  }

  private void OnRemoveData()
  {
    if (DBManager.inst.DB_WorldBossDB.Get(this._tileData.ResourceId) != null)
      return;
    this.OnCloseHandler();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void OnSecond(int serverTime)
  {
    --this.remainTime;
    WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(this._tileData.ResourceId);
    if (worldBossData == null)
    {
      this.RemoveEventHandler();
      this.OnCloseHandler();
    }
    else
    {
      this.progress.value = worldBossData.Progress;
      this.progressValue.text = worldBossData.HP.ToString() + "%";
      if (this.remainTime < 0)
      {
        this.remainTime = 0;
        this.OnCloseHandler();
        this.progress.value = 0.0f;
      }
      else
        this.remainTimeLabel.text = Utils.FormatTime(this.remainTime, false, false, true);
    }
  }

  private void CheckAttack()
  {
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnInfoHandler()
  {
    UIManager.inst.OpenPopup("InfoKingdom", (Popup.PopupParameter) new InfoKingdom.Parameter()
    {
      initIndex = 1
    });
  }

  private void Clear()
  {
    for (int index = 0; index < this.rewardItems.Length; ++index)
      this.rewardItems[index].Clear();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.monster);
    this.preview = (PVEMonsterPreview) null;
  }

  private void RemoveEventHandler()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    DBManager.inst.DB_WorldBossDB.onDataRemoved -= new System.Action(this.OnRemoveData);
  }

  private void UpdateUI()
  {
    if (this._tileData == null)
      return;
    this.attackBt.isEnabled = !PlayerData.inst.userData.IsForeigner && this._tileData.Location.K == PlayerData.inst.playerCityData.cityLocation.K;
    WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(this._tileData.ResourceId);
    WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
    if (worldBossData == null)
      return;
    int num = NetServerTime.inst.ServerTimestamp - (int) worldBossData.placeTime;
    this.totalTime = data.DispearTime;
    this.remainTime = this.totalTime - num;
    this.remainTimeLabel.text = Utils.FormatTime(this.remainTime, false, false, true);
    this.progress.value = worldBossData.Progress;
    this.progressValue.text = worldBossData.HP.ToString() + "%";
    this.desc.text = data.LocalDesc;
    if ((UnityEngine.Object) this.title != (UnityEngine.Object) null)
    {
      string localName = data.LocalName;
      this.title.text = ScriptLocalization.GetWithPara("id_lv_num", new Dictionary<string, string>()
      {
        {
          "0",
          data.Level.ToString()
        }
      }, true) + " " + localName;
    }
    this.spirit.text = data.StaminaCost.ToString();
    this.rewardItems[0].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.DisplayItemName_1, true), Utils.GetUITextPath() + data.DisplayItemImage_1);
    this.rewardItems[1].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.DisplayItemName_2, true), Utils.GetUITextPath() + data.DisplayItemImage_2);
    this.rewardItems[2].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.DisplayItemName_3, true), Utils.GetUITextPath() + data.DisplayItemImage_3);
    this.rewardItems[3].SetData(RewardNormalItem.RewardType.item, ScriptLocalization.Get(data.DisplayItemName_4, true), Utils.GetUITextPath() + data.DisplayItemImage_4);
    this.rewardItems[4].SetData(RewardNormalItem.RewardType.item, string.Empty, Utils.GetUITextPath() + "item_mystery_box");
    if (DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId).spirit < data.StaminaCost)
      this.spirit.color = Color.red;
    else
      this.spirit.color = Color.white;
    this._open = PlayerData.inst.allianceId > 0L;
  }

  public void AttackHandler()
  {
    if (PlayerData.inst.allianceId <= 0L)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_not_join_alliance", true), (System.Action) null, 4f, false);
    else if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      DB.HeroData heroData = DBManager.inst.DB_hero.Get((long) PlayerData.inst.cityId);
      WorldBossData worldBossData = DBManager.inst.DB_WorldBossDB.Get(this._tileData.ResourceId);
      WorldBossStaticInfo data = ConfigManager.inst.DB_WorldBoss.GetData(worldBossData.configId);
      if (worldBossData == null)
        return;
      int staminaCost = data.StaminaCost;
      if (heroData.spirit < staminaCost)
        UIManager.inst.toast.Show(ScriptLocalization.Get("monster_attack_not_enough_stamina_description", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
        {
          location = this._targetLocation,
          marchType = MarchAllocDlg.MarchType.startGveRally,
          bossIndex = worldBossData.bossId,
          energyCost = data.StaminaCost
        }, 1 != 0, 1 != 0, 1 != 0);
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate coordinate;
  }
}
