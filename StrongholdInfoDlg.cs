﻿// Decompiled with JetBrains decompiler
// Type: StrongholdInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class StrongholdInfoDlg : UI.Dialog
{
  public StrongholdInfoDlg.CombatBonuses mCombatBonuses = new StrongholdInfoDlg.CombatBonuses();
  public StrongholdInfoDlg.MyHospital mMyHospital = new StrongholdInfoDlg.MyHospital();
  public StrongholdInfoDlg.DetailsCityPanel mDetailsCityPanel = new StrongholdInfoDlg.DetailsCityPanel();
  public StrongholdInfoDlg.DetailsCombatPanel mDetailsCombatPanel = new StrongholdInfoDlg.DetailsCombatPanel();
  public StrongholdInfoDlg.DetailsResourcePanel mFoodResourcePanel = new StrongholdInfoDlg.DetailsResourcePanel();
  public StrongholdInfoDlg.DetailsResourcePanel mSilverResourcePanel = new StrongholdInfoDlg.DetailsResourcePanel();
  public StrongholdInfoDlg.DetailsResourcePanel mWoodResourcePanel = new StrongholdInfoDlg.DetailsResourcePanel();
  public StrongholdInfoDlg.DetailsResourcePanel mOreResourcePanel = new StrongholdInfoDlg.DetailsResourcePanel();
  public StrongholdInfoDlg.ResourcesSubPage mResourcesSubPage = new StrongholdInfoDlg.ResourcesSubPage();
  private List<GameObject> items = new List<GameObject>();
  public UILabel mRunesLabel;
  public UILabel mSilverLabel;
  public GameObject mCombatPage;
  public GameObject mDetailsPage;
  public GameObject mOverviewPage;
  public GameObject mResourcesPage;
  public GameObject mBuildingsPage;
  public GameObject mBuildingStatsItemPrefab;
  public GameObject mBuildingItemHeaderPrefab;
  public GameObject mBuildingItemPrefab;
  public GameObject mCityPanel;
  public GameObject mCombatPanel;
  public GameObject mFoodPanel;
  public GameObject mWoodPanel;
  public GameObject mSilverPanel;
  public GameObject mOrePanel;
  public UILabel mStrongholdLevel;
  public UILabel mOverviewStrongholdLevel;
  public UITexture mIcon;
  public GameObject mRenameCityPopup;

  public void Show()
  {
    this.mBuildingsPage.SetActive(false);
    this.mOverviewPage.SetActive(false);
    this.mDetailsPage.SetActive(true);
    float y1 = this.mBuildingStatsItemPrefab.transform.localPosition.y;
    float height1 = (float) this.mBuildingStatsItemPrefab.GetComponent<UIWidget>().height;
    using (List<GameObject>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current);
    }
    this.items.Clear();
    int maxLevelByType = ConfigManager.inst.DB_Building.GetMaxLevelByType("stronghold");
    for (int index = 1; index <= maxLevelByType; ++index)
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData("stronghold_" + index.ToString());
      GameObject gob = Utils.DuplicateGOB(this.mBuildingStatsItemPrefab);
      gob.SetActive(true);
      Utils.SetLPY(gob, y1);
      y1 -= height1;
      int benefitValue1 = (int) data.Benefit_Value_1;
      int benefitValue2 = (int) data.Benefit_Value_2;
      gob.GetComponent<BuildInfoResourceStatItem>().SetDetails(index.ToString(), benefitValue1, benefitValue2);
      this.items.Add(gob);
    }
    ArrayList ruralBuildings = new ArrayList();
    ArrayList urbanBuildings = new ArrayList();
    CityManager.inst.GetBuildingLevelData(ruralBuildings, urbanBuildings);
    this.mBuildingItemHeaderPrefab.GetComponent<LabelBinder>().SetLabels((object) Utils.XLAT("id_strongholdinfo_uppercase_urban_buildings"));
    float y2 = this.mBuildingItemPrefab.transform.localPosition.y;
    float height2 = (float) this.mBuildingItemPrefab.GetComponent<UIWidget>().height;
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in urbanBuildings)
    {
      GameObject gob = Utils.DuplicateGOB(this.mBuildingItemPrefab);
      Utils.SetLPY(gob, y2);
      y2 -= height2;
      gob.SetActive(true);
      gob.GetComponent<StrongholdBuildingInfoItem>().SetDetails(Utils.XLAT(buildingLevelInfo.mName), buildingLevelInfo.mLevel);
      this.items.Add(gob);
    }
    float newY1 = y2 - 100f;
    GameObject gob1 = Utils.DuplicateGOB(this.mBuildingItemHeaderPrefab);
    gob1.GetComponent<LabelBinder>().SetLabels((object) Utils.XLAT("id_strongholdinfo_uppercase_rural_buildings"));
    Utils.SetLPY(gob1, newY1);
    float newY2 = newY1 - (float) gob1.GetComponent<UIWidget>().height;
    this.items.Add(gob1);
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in ruralBuildings)
    {
      GameObject gob2 = Utils.DuplicateGOB(this.mBuildingItemPrefab);
      Utils.SetLPY(gob2, newY2);
      newY2 -= height2;
      gob2.SetActive(true);
      gob2.GetComponent<StrongholdBuildingInfoItem>().SetDetails(Utils.XLAT(buildingLevelInfo.mName), buildingLevelInfo.mLevel);
      this.items.Add(gob2);
    }
    string str = "[aaaaaa]K:[cc0000] " + CityManager.inst.Location.K.ToString() + " [aaaaaa]X:[cc0000] " + CityManager.inst.Location.X.ToString() + " [aaaaaa]Y:[cc0000] " + CityManager.inst.Location.Y.ToString();
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.mCityPanel.GetComponent<StrongholdMainPanelItem>().SetDetails(Utils.XLAT("City Name") + ": [aaaaaa]" + CityManager.inst.GetName(), Utils.XLAT("Kingdom") + ": [aaaaaa]" + Utils.XLAT(ConfigManager.inst.DB_KingdomDefinitions[CityManager.inst.Location.K.ToString()].Name), Utils.XLAT("Location") + ": [aaaaaa]" + str);
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    int num1 = 0;
    using (List<long>.Enumerator enumerator = targetListByType.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        num1 += (int) current;
      }
    }
    long num2 = cityData.cityTroops.totalTroopsCount + cityData.cityTroops.totalTrapsCount + (long) num1;
    long num3 = cityData.cityTroops.totalTroopsCount + cityData.cityTroops.totalTrapsCount;
    this.mCombatPanel.GetComponent<StrongholdMainPanelItem>().SetDetails("Total Troops: [aaaaaa]" + Utils.ConvertNumberToNormalString(num2), "Upkeep: [cc0000]" + Utils.ConvertNumberToNormalString(cityData.resources.totalUpkeepsInSecond), "You Own: [aaaaaa]" + Utils.ConvertNumberToNormalString(num3));
    this.mFoodPanel.GetComponent<StrongholdMainPanelItem>().SetDetails("Overall Income: [aaaaaa]" + Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.FOOD).ToString()), "Upkeep: [cc0000]" + Utils.FormatThousands(cityData.resources.totalUpkeepsInSecond.ToString()), "You Own: [aaaaaa]" + Utils.FormatThousands(cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD).ToString()));
    this.mWoodPanel.GetComponent<StrongholdMainPanelItem>().SetDetails("Overall Income: [aaaaaa]" + Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.WOOD).ToString()), "Upkeep: [cc0000]???", "You Own: [aaaaaa]" + Utils.FormatThousands(cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD).ToString()));
    this.mSilverPanel.GetComponent<StrongholdMainPanelItem>().SetDetails("Overall Income: [aaaaaa]" + Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.SILVER).ToString()), "Upkeep: [cc0000]???", "You Own: [aaaaaa]" + Utils.FormatThousands(cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER).ToString()));
    this.mOrePanel.GetComponent<StrongholdMainPanelItem>().SetDetails("Overall Income: [aaaaaa]" + Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.ORE).ToString()), "Upkeep: [cc0000]???", "You Own: [aaaaaa]" + Utils.FormatThousands(cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE).ToString()));
    this.mDetailsCombatPanel.mTotalTroops.text = "Total Troops: " + this.mCombatBonuses.mTotalTroops.text;
    int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor("stronghold");
    this.mStrongholdLevel.text = string.Format("LV. {0}[aaaaaa]/30", (object) buildingLevelFor);
    this.mOverviewStrongholdLevel.text = string.Format("LV. {0}[aaaaaa]/30", (object) buildingLevelFor);
    string filename = "stronghold_l" + CityManager.inst.GetIconLevelFromBuildingLevel(buildingLevelFor, (string) null).ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.mIcon, "Texture/Buildings/" + filename, (System.Action<bool>) null, true, false, string.Empty);
    int width = 0;
    int height3 = 0;
    CityBuildingDims.Get(filename, out width, out height3);
    this.mIcon.width = width;
    this.mIcon.height = height3;
    GameEngine.Instance.events.OnCityRenamed += new System.Action<string>(this.OnCityRenamed);
  }

  private void OnTabSelected(int selectedTab)
  {
    switch (selectedTab)
    {
      case 0:
        this.mOverviewPage.SetActive(true);
        this.mDetailsPage.SetActive(false);
        this.mResourcesPage.SetActive(false);
        this.mCombatPage.SetActive(false);
        break;
      case 1:
        this.mDetailsPage.SetActive(true);
        this.mOverviewPage.SetActive(false);
        this.mCombatPage.SetActive(false);
        this.mResourcesPage.SetActive(false);
        break;
    }
  }

  public void OnChangeCityNameBtnPressed()
  {
  }

  public void OnCombatPanelPressed()
  {
    this.mDetailsPage.SetActive(false);
    this.mCombatPage.SetActive(true);
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData("stronghold", CityManager.inst.GetHighestBuildingLevelFor("stronghold"));
    this.mCombatBonuses.mTotalTroops.text = Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.totalTroopsCount.ToString());
    this.mCombatBonuses.mTroopMarches.text = data.Benefit_Value_2.ToString();
    this.mCombatBonuses.mTroopsPerMarch.text = data.Benefit_Value_1.ToString();
    double benefitValue1 = ConfigManager.inst.DB_Building.GetData("walls", CityManager.inst.GetHighestBuildingLevelFor("walls")).Benefit_Value_1;
    this.mCombatBonuses.mTraps.text = Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.totalTrapsCount.ToString());
    this.mCombatBonuses.mWallTrapCapacity.text = Utils.FormatThousands(benefitValue1.ToString());
    this.mCombatBonuses.mUpKeep.text = Utils.FormatThousands(cityData.resources.totalUpkeepsInSecond.ToString());
    List<long> targetListByType = DBManager.inst.DB_March.GetTargetListByType(MarchData.MarchType.reinforce);
    int num = 0;
    using (List<long>.Enumerator enumerator = targetListByType.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        long current = enumerator.Current;
        num += (int) current;
      }
    }
    this.mCombatBonuses.mReinforcement.text = Utils.ConvertNumberToNormalString(num);
    foreach (UnityEngine.Object @object in this.mMyHospital.mTroopInfo)
      UnityEngine.Object.Destroy(@object);
    this.mMyHospital.mTroopInfo.Clear();
    this.mMyHospital.mHospitalTable.Reposition();
    this.mCombatBonuses.mHospitalized.text = Utils.FormatThousands(cityData.hospitalTroops.totalTroopsCount.ToString());
    this.mMyHospital.mWounded.text = Utils.FormatThousands(cityData.hospitalTroops.totalTroopsCount.ToString());
  }

  public void PropergateBuildingList(string buildingType)
  {
    using (List<GameObject>.Enumerator enumerator = this.mResourcesSubPage.mBuildingItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current);
    }
    this.mResourcesSubPage.mBuildingItems.Clear();
    ArrayList buildings = new ArrayList();
    CityManager.inst.GetBuildingLevelDataOfType(buildingType, buildings);
    float y = this.mResourcesSubPage.mResourceBuildingPrefab.transform.localPosition.y;
    float height = (float) this.mResourcesSubPage.mResourceBuildingPrefab.GetComponent<UIWidget>().height;
    foreach (StrongholdInfoDlg.BuildingLevelInfo buildingLevelInfo in buildings)
    {
      GameObject gob = Utils.DuplicateGOB(this.mResourcesSubPage.mResourceBuildingPrefab);
      gob.SetActive(true);
      Utils.SetLPY(gob, y);
      y -= height;
      gob.GetComponent<BuildInfoResourceStatItem>().SetDetails(buildingLevelInfo.mName, buildingLevelInfo.mProductionRate, buildingLevelInfo.mLevel);
      this.mResourcesSubPage.mBuildingItems.Add(gob);
    }
  }

  public void OnOverviewBtnPressed()
  {
    this.mBuildingsPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
  }

  public void OnStatsBtnPressed()
  {
    this.mOverviewPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
  }

  public void OnCityPanelPressed()
  {
  }

  public void OnFoodPanelPressed()
  {
    this.mResourcesPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
    this.mResourcesSubPage.mTitle.text = Utils.XLAT("id_strongholdinfo_food_bonuses");
    this.mResourcesSubPage.mSubTitle.text = Utils.XLAT("id_strongholdinfo_all_current_food_bonuses");
    this.mResourcesSubPage.mOwnedTitle.text = Utils.XLAT("id_strongholdinfo_farms_owned");
    this.mResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("food");
    this.mResourcesSubPage.mUpKeep.text = string.Format("[D00000]-{0}[-]", (object) Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).resources.totalUpkeepsInSecond.ToString()));
    this.mResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.FOOD).ToString());
    this.mResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.FOOD).ToString());
    this.mResourcesSubPage.mVIPResouceBonus.text = "???";
    this.mResourcesSubPage.mHeroResourceBonus.text = "???";
    this.mResourcesSubPage.mItemResourceBonus.text = "???";
    this.mResourcesSubPage.mResearchBonus.text = "???";
    this.mResourcesSubPage.mTotalBonus.text = "???";
    this.PropergateBuildingList("farm");
  }

  public void OnWoodPanelPressed()
  {
    this.mResourcesPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
    this.mResourcesSubPage.mTitle.text = Utils.XLAT("id_strongholdinfo_wood_bonuses");
    this.mResourcesSubPage.mSubTitle.text = Utils.XLAT("id_strongholdinfo_all_current_wood_bonuses");
    this.mResourcesSubPage.mOwnedTitle.text = Utils.XLAT("id_strongholdinfo_lumber_mills_owned");
    this.mResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("wood");
    this.mResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.WOOD).ToString());
    this.mResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.WOOD).ToString());
    this.mResourcesSubPage.mUpKeep.text = "0";
    this.mResourcesSubPage.mVIPResouceBonus.text = "???";
    this.mResourcesSubPage.mHeroResourceBonus.text = "???";
    this.mResourcesSubPage.mItemResourceBonus.text = "???";
    this.mResourcesSubPage.mResearchBonus.text = "???";
    this.mResourcesSubPage.mTotalBonus.text = "???";
    this.PropergateBuildingList("lumber_mill");
  }

  public void OnSilverPanelPressed()
  {
    this.mResourcesPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
    this.mResourcesSubPage.mTitle.text = Utils.XLAT("id_strongholdinfo_silver_bonuses");
    this.mResourcesSubPage.mSubTitle.text = Utils.XLAT("id_strongholdinfo_all_current_silver_bonuses");
    this.mResourcesSubPage.mOwnedTitle.text = Utils.XLAT("id_strongholdinfo_houses_owned");
    this.mResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("silver");
    this.mResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.SILVER).ToString());
    this.mResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.SILVER).ToString());
    this.mResourcesSubPage.mUpKeep.text = "0";
    this.mResourcesSubPage.mVIPResouceBonus.text = "???";
    this.mResourcesSubPage.mHeroResourceBonus.text = "???";
    this.mResourcesSubPage.mItemResourceBonus.text = "???";
    this.mResourcesSubPage.mResearchBonus.text = "???";
    this.mResourcesSubPage.mTotalBonus.text = "???";
    this.PropergateBuildingList("house");
  }

  public void OnOrePanelPressed()
  {
    this.mResourcesPage.SetActive(true);
    this.mDetailsPage.SetActive(false);
    this.mResourcesSubPage.mTitle.text = Utils.XLAT("id_strongholdinfo_ore_bonuses");
    this.mResourcesSubPage.mSubTitle.text = Utils.XLAT("id_strongholdinfo_all_current_ore_bonuses");
    this.mResourcesSubPage.mOwnedTitle.text = Utils.XLAT("id_strongholdinfo_mines_owned");
    this.mResourcesSubPage.mResourceIcon.spriteName = ConfigManager.inst.DB_Lookup.GetImage("ore");
    this.mResourcesSubPage.mOverallIncome.text = Utils.FormatThousands(CityManager.inst.GetTotalIncomeForResourceOfType(CityManager.ResourceTypes.ORE).ToString());
    this.mResourcesSubPage.mCityCapacity.text = Utils.FormatThousands(CityManager.inst.GetTotalCapacityForResourceOfType(CityManager.ResourceTypes.ORE).ToString());
    this.mResourcesSubPage.mUpKeep.text = "0";
    this.mResourcesSubPage.mVIPResouceBonus.text = "???";
    this.mResourcesSubPage.mHeroResourceBonus.text = "???";
    this.mResourcesSubPage.mItemResourceBonus.text = "???";
    this.mResourcesSubPage.mResearchBonus.text = "???";
    this.mResourcesSubPage.mTotalBonus.text = "???";
    this.PropergateBuildingList("mine");
  }

  public void OnClosePanelBtnPressed()
  {
    this.mDetailsPage.SetActive(true);
    this.mResourcesPage.SetActive(false);
    this.mCombatPage.SetActive(false);
    this.mOverviewPage.SetActive(false);
    this.mBuildingsPage.SetActive(false);
  }

  public void OnCloseBtnPressed()
  {
    GameEngine.Instance.events.OnCityRenamed -= new System.Action<string>(this.OnCityRenamed);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnRenameCityBtnPressed()
  {
    this.mRenameCityPopup.SetActive(true);
    this.mRenameCityPopup.GetComponent<RenameCity>().SetDetails();
  }

  public void OnCityRenamed(string newName)
  {
    this.mDetailsCityPanel.mCityName.text = Utils.XLAT("City Name") + ": [aaaaaa]" + newName;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.Show();
  }

  public class BuildingLevelInfo
  {
    public string mName = string.Empty;
    public int mProductionRate = -1;
    public int mLevel;

    public BuildingLevelInfo(string name, int productionRate, int level)
    {
      this.mName = name;
      this.mProductionRate = productionRate;
      this.mLevel = level;
    }
  }

  [Serializable]
  public class CombatBonuses
  {
    public UILabel mTotalTroops;
    public UILabel mTroopMarches;
    public UILabel mTroopsPerMarch;
    public UILabel mMaxResourceLoad;
    public UILabel mUpKeep;
    public UILabel mTraps;
    public UILabel mWallTrapCapacity;
    public UILabel mReinforcement;
    public UILabel mHospitalized;
  }

  [Serializable]
  public class MyHospital
  {
    [HideInInspector]
    public ArrayList mTroopInfo = new ArrayList();
    public UILabel mWounded;
    public UITable mHospitalTable;
    public GameObject mTroopItemPrefab;
  }

  [Serializable]
  public class DetailsCityPanel
  {
    public UILabel mCityName;
    public UILabel mKingdom;
    public UILabel mLocation;
  }

  [Serializable]
  public class DetailsCombatPanel
  {
    public UILabel mTotalTroops;
    public UILabel mTotalMarches;
    public UILabel mTroopsPerMarch;
    public UILabel mMaxResourceLoad;
  }

  [Serializable]
  public class DetailsResourcePanel
  {
    public UILabel mOverallIncome;
    public UILabel mUpKeep;
    public UILabel mYouOwn;
    public UILabel mCityCapacity;
  }

  [Serializable]
  public class ResourcesSubPage
  {
    [HideInInspector]
    public List<GameObject> mBuildingItems = new List<GameObject>();
    public UISprite mResourceIcon;
    public UILabel mTitle;
    public UILabel mSubTitle;
    public UILabel mOverallIncome;
    public UILabel mYouOwn;
    public UILabel mCityCapacity;
    public UILabel mUpKeep;
    public UILabel mVIPResouceBonus;
    public UILabel mHeroResourceBonus;
    public UILabel mItemResourceBonus;
    public UILabel mResearchBonus;
    public UILabel mTotalBonus;
    public UILabel mOwnedTitle;
    public GameObject mResourceBuildingPrefab;
    public UIWidget mResourceBuildingContainer;
  }
}
