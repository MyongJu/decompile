﻿// Decompiled with JetBrains decompiler
// Type: KingdomTouchCircleBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomTouchCircleBtn : CityTouchCircleBtn
{
  public override void Init(CityCircleBtnPara para)
  {
    base.Init(para);
    if (!para.isActive)
      return;
    this.et.onClick.Add(new EventDelegate((MonoBehaviour) KingdomTouchCircle.Instance, "Close"));
  }

  protected override void Awake()
  {
    base.Awake();
  }

  private void Update()
  {
  }
}
