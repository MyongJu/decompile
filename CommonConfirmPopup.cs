﻿// Decompiled with JetBrains decompiler
// Type: CommonConfirmPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class CommonConfirmPopup : Popup
{
  [SerializeField]
  private UILabel panelTitle;
  [SerializeField]
  private UILabel panelContent;
  [SerializeField]
  private UILabel confirmButtonText;
  private string _title;
  private string _content;
  private string _confirmButtonText;
  private System.Action _onOkayCallback;
  private System.Action _onCloseCallback;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CommonConfirmPopup.Parameter parameter = orgParam as CommonConfirmPopup.Parameter;
    if (parameter != null)
    {
      this._title = parameter.title;
      this._content = parameter.content;
      this._confirmButtonText = parameter.confirmButtonText;
      this._onOkayCallback = parameter.onOkayCallback;
      this._onCloseCallback = parameter.onCloseCallback;
    }
    this.UpdateUI();
  }

  public void OnOkayButtonClicked()
  {
    if (this._onOkayCallback != null)
      this._onOkayCallback();
    this.OnCloseButtonClicked();
  }

  public void OnCloseButtonClicked()
  {
    if (this._onCloseCallback != null)
      this._onCloseCallback();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.panelTitle.text = this._title;
    this.panelContent.text = this._content;
    this.confirmButtonText.text = this._confirmButtonText;
  }

  public class Parameter : Popup.PopupParameter
  {
    public string title;
    public string content;
    public string confirmButtonText;
    public System.Action onOkayCallback;
    public System.Action onCloseCallback;
  }
}
