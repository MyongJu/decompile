﻿// Decompiled with JetBrains decompiler
// Type: DummyPoint
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DummyPoint : MonoBehaviour, IVfxTarget
{
  public static DummyPoint Template;

  public static DummyPoint Create(Vector3 position, Transform parent = null)
  {
    if ((Object) DummyPoint.Template == (Object) null)
    {
      GameObject gameObject = new GameObject();
      gameObject.name = "DummyTemplate";
      gameObject.transform.position = Vector3.zero;
      DummyPoint.Template = gameObject.AddComponent<DummyPoint>();
    }
    if ((Object) parent == (Object) null)
      parent = DummyPoint.Template.transform;
    GameObject gameObject1 = PrefabManagerEx.Instance.Spawn(DummyPoint.Template.gameObject, parent);
    gameObject1.transform.position = position;
    return gameObject1.GetComponent<DummyPoint>();
  }

  public static void Destory(DummyPoint point)
  {
    if ((Object) point == (Object) null)
      return;
    PrefabManagerEx.Instance.Destroy(point.gameObject);
  }

  public Transform GetDummyPoint(string key)
  {
    return this.transform;
  }

  Transform IVfxTarget.get_transform()
  {
    return this.transform;
  }
}
