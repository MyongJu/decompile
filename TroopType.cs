﻿// Decompiled with JetBrains decompiler
// Type: TroopType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public enum TroopType
{
  class_infantry,
  class_ranged,
  class_cavalry,
  class_artyclose,
  class_artyfar,
  count,
  class_trap,
  invalid,
}
