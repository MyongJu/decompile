﻿// Decompiled with JetBrains decompiler
// Type: PitExploreRankSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PitExploreRankSlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelRank;
  [SerializeField]
  private UILabel _labelRankName;
  [SerializeField]
  private UILabel _labelKingdomFrom;
  [SerializeField]
  private UILabel _labelRankScore;
  [SerializeField]
  private UISprite[] _normalFrameArray;
  [SerializeField]
  private UISprite[] _rankTopThreeArray;
  [SerializeField]
  private UITexture _rankTopThreeTexture;
  private int _rank;
  private PitExplorePayload.PitRankDataWithTime _pitRankDataWithTime;

  public void SetData(int rank, PitExplorePayload.PitRankDataWithTime pitRankDataWithTime)
  {
    this._rank = rank;
    this._pitRankDataWithTime = pitRankDataWithTime;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._pitRankDataWithTime == null)
      return;
    this.ResetBackgroundFrame();
    this._labelRankName.text = string.Format(!string.IsNullOrEmpty(this._pitRankDataWithTime.pitRankData.Acronym) ? "[{0}]{1}" : "{1}", (object) this._pitRankDataWithTime.pitRankData.Acronym, (object) this._pitRankDataWithTime.pitRankData.Name);
    this._labelKingdomFrom.text = string.Format("{0} #{1}", (object) Utils.XLAT("id_kingdom"), (object) this._pitRankDataWithTime.pitRankData.WorldId);
    this._labelRankScore.text = Utils.FormatThousands(this._pitRankDataWithTime.pitRankData.Score.ToString());
    if (this._rank <= 3)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this._rankTopThreeTexture, "Texture/LeaderboardIcons/icon_no" + (object) this._rank, (System.Action<bool>) null, true, false, string.Empty);
      NGUITools.SetActive(this._labelRank.gameObject, false);
      NGUITools.SetActive(this._rankTopThreeTexture.gameObject, true);
      if (this._rank <= 0 || this._rankTopThreeArray == null || this._rankTopThreeArray.Length != 3)
        return;
      NGUITools.SetActive(this._rankTopThreeArray[this._rank - 1].gameObject, true);
    }
    else
    {
      this._labelRank.text = string.Format("NO.{0}", (object) this._rank);
      NGUITools.SetActive(this._labelRank.gameObject, true);
      NGUITools.SetActive(this._rankTopThreeTexture.gameObject, false);
      if (this._normalFrameArray == null || this._normalFrameArray.Length != 2)
        return;
      NGUITools.SetActive(this._normalFrameArray[this._rank % 2].gameObject, true);
    }
  }

  private void ResetBackgroundFrame()
  {
    foreach (Component normalFrame in this._normalFrameArray)
      NGUITools.SetActive(normalFrame.gameObject, false);
    foreach (Component rankTopThree in this._rankTopThreeArray)
      NGUITools.SetActive(rankTopThree.gameObject, false);
  }
}
