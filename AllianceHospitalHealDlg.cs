﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalHealDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceHospitalHealDlg : UI.Dialog
{
  protected List<AllianceHealItem> m_allHealItems = new List<AllianceHealItem>();
  protected Dictionary<string, int> m_selectCounter = new Dictionary<string, int>();
  protected bool m_selectAll = true;
  [SerializeField]
  protected AllianceHealItem m_healItemTemplate;
  [SerializeField]
  protected UIScrollView m_healItemScrollView;
  [SerializeField]
  protected UIGrid m_healItemGrid;
  [SerializeField]
  protected UILabel m_capicityLabel;
  [SerializeField]
  protected UISlider m_capicitySlider;
  [SerializeField]
  protected GameObject m_rootEmptyTip;
  [SerializeField]
  protected UILabel m_honorLabel;
  [SerializeField]
  protected UILabel m_selectedCountLabel;
  [SerializeField]
  protected GameObject m_rootNotUnderHealing;
  [SerializeField]
  protected UILabel m_instantHealCostLabel;
  [SerializeField]
  protected UILabel m_healTimeLabel;
  [SerializeField]
  protected UIButton m_healButton;
  [SerializeField]
  protected UIButton m_instantHealButton;
  [SerializeField]
  protected UISlider m_healingSlider;
  [SerializeField]
  protected GameObject m_rootUnderHealing;
  [SerializeField]
  protected UILabel m_leftHealingTimeLabel;
  [SerializeField]
  protected UILabel m_instantFinishHealingCost;
  [SerializeField]
  protected UILabel m_warning;

  protected bool UnderHealing
  {
    get
    {
      AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
      if (myHospitalData == null)
        return false;
      return myHospitalData.MyJobId != 0L;
    }
  }

  protected int HealTimeCost
  {
    get
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
          if (data != null)
          {
            int finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(current.Value * data.Heal_Time, data.HEAL_TIME_CAL_LEY);
            num += ConfigManager.inst.DB_BenefitCalc.GetFinalData(finalData, "calc_alliance_hospital_healing_time");
          }
        }
      }
      return num;
    }
  }

  protected int HonorCost
  {
    get
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(current.Key);
          if (data != null)
            num += Mathf.CeilToInt((float) current.Value * data.Heal_Honor_Benifit);
        }
      }
      return num;
    }
  }

  protected int TotalHonor
  {
    get
    {
      return (int) PlayerData.inst.userData.currency.honor;
    }
  }

  protected int SelectCount
  {
    get
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          num += current.Value;
        }
      }
      return num;
    }
  }

  protected int TroopsCapicityInAllianceHospital
  {
    get
    {
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(ConfigManager.inst.DB_AllianceBuildings.AllianceHospitalConfigId);
      if (buildingStaticInfo == null)
        return 0;
      return ConfigManager.inst.DB_BenefitCalc.GetFinalData((int) buildingStaticInfo.Param1, "calc_alliance_hospital_capacity");
    }
  }

  protected int TroopsInAllianceHospital
  {
    get
    {
      AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
      if (myHospitalData == null)
        return 0;
      Dictionary<string, AllianceHospitalHealingData> myHealingData = myHospitalData.GetMyHealingData();
      if (myHealingData == null)
        return 0;
      int num = 0;
      using (Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = myHealingData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, AllianceHospitalHealingData> current = enumerator.Current;
          num += (int) current.Value.total;
        }
      }
      return num;
    }
  }

  protected int LeftHealingTime
  {
    get
    {
      AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
      if (myHospitalData != null && myHospitalData.MyJobId != 0L)
      {
        JobHandle job = JobManager.Instance.GetJob(myHospitalData.MyJobId);
        if (job != null)
          return job.LeftTime();
      }
      return 0;
    }
  }

  protected float HealingProgress
  {
    get
    {
      AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
      if (myHospitalData != null && myHospitalData.MyJobId != 0L)
      {
        JobHandle job = JobManager.Instance.GetJob(myHospitalData.MyJobId);
        if (job != null)
          return Mathf.Clamp((float) (1.0 - (double) job.LeftTime() / (double) job.Duration()), 0.0f, 1f);
      }
      return 0.0f;
    }
  }

  protected int InstantFinishHealingCost
  {
    get
    {
      return ItemBag.CalculateCost(new Hashtable()
      {
        {
          (object) "time",
          (object) this.LeftHealingTime
        }
      });
    }
  }

  protected int InstantHealCost
  {
    get
    {
      return this.HealCostForHonor + ItemBag.CalculateCost(new Hashtable()
      {
        {
          (object) "time",
          (object) this.HealTimeCost
        }
      });
    }
  }

  protected int HealCostForHonor
  {
    get
    {
      return (int) Mathf.Ceil((float) Mathf.Max(0, this.HonorCost - this.TotalHonor) / (float) ConfigManager.inst.DB_GameConfig.GetData("honor_per_gold").ValueInt);
    }
  }

  public void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.SecondTick);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    MessageHub.inst.GetPortByAction("Alliance:dismissHospitalTroops").AddEvent(new System.Action<object>(this.OnDismissTroops));
  }

  public void OnDisable()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.SecondTick);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    MessageHub.inst.GetPortByAction("Alliance:dismissHospitalTroops").RemoveEvent(new System.Action<object>(this.OnDismissTroops));
  }

  protected void OnDismissTroops(object data)
  {
    this.UpdateUI(true);
  }

  protected void OnJobCompleted(object data)
  {
    this.UpdateUI(true);
  }

  protected void SecondTick(int delta)
  {
    this.UpdateUI(false);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI(true);
    if (this.UnderHealing)
      return;
    this.ToggleQuickSelect();
  }

  protected void UpdateUI(bool rebuildList = true)
  {
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData == null)
    {
      D.error((object) "there is no hospital data");
    }
    else
    {
      this.m_warning.gameObject.SetActive(myHospitalData.ClearAway != 0);
      this.m_selectedCountLabel.text = this.SelectCount.ToString();
      this.m_rootUnderHealing.SetActive(this.UnderHealing);
      this.m_rootNotUnderHealing.SetActive(!this.UnderHealing);
      this.m_capicityLabel.text = string.Format("{0}/{1}", (object) this.TroopsInAllianceHospital, (object) this.TroopsCapicityInAllianceHospital);
      this.m_capicitySlider.value = Mathf.Min((float) this.TroopsInAllianceHospital / (float) this.TroopsCapicityInAllianceHospital, 1f);
      if (!this.UnderHealing)
      {
        this.m_honorLabel.text = string.Format(this.HonorCost <= this.TotalHonor ? "{0}/{1}" : "[ff0000]{0}[-]/{1}", (object) this.HonorCost, (object) this.TotalHonor);
        this.m_instantHealCostLabel.text = this.InstantHealCost.ToString();
        this.m_healTimeLabel.text = Utils.FormatTime(this.HealTimeCost, false, false, true);
        this.m_healButton.isEnabled = this.SelectCount > 0;
        this.m_instantHealButton.isEnabled = this.SelectCount > 0;
      }
      else
      {
        this.m_leftHealingTimeLabel.text = Utils.FormatTime(this.LeftHealingTime, false, false, true);
        this.m_healingSlider.value = this.HealingProgress;
        this.m_instantFinishHealingCost.text = this.InstantFinishHealingCost.ToString();
      }
      if (!rebuildList)
        return;
      this.UpdateHealList();
    }
  }

  protected void SetSelectCounter(string unitId, int count)
  {
    if (this.m_selectCounter.ContainsKey(unitId))
      this.m_selectCounter[unitId] = count;
    else
      this.m_selectCounter.Add(unitId, count);
  }

  protected void ClearSelectCounter()
  {
    this.m_selectCounter.Clear();
  }

  protected void UpdateHealList()
  {
    this.ClearSelectCounter();
    this.RemoveAllHealItems();
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData == null)
      return;
    Dictionary<string, AllianceHospitalHealingData> myHealingData = myHospitalData.GetMyHealingData();
    if (myHealingData == null || myHealingData.Count <= 0)
    {
      this.m_rootEmptyTip.SetActive(true);
    }
    else
    {
      this.m_rootEmptyTip.SetActive(false);
      using (Dictionary<string, AllianceHospitalHealingData>.Enumerator enumerator = myHealingData.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, AllianceHospitalHealingData> current = enumerator.Current;
          if (current.Value.total > 0L)
          {
            AllianceHealItem healItem = this.CreateHealItem();
            healItem.SetData(current.Key, (int) current.Value.healing, (int) current.Value.total, this.UnderHealing, DissmissHealTroopPopup.HospitalType.AllianceHospital);
            this.SetSelectCounter(current.Key, (int) current.Value.healing);
            healItem.OnValueChanged += new System.Action<AllianceHealItem>(this.OnValueChanged);
          }
        }
      }
      this.m_allHealItems.Sort((Comparison<AllianceHealItem>) ((a, b) =>
      {
        Unit_StatisticsInfo unitInfo1 = a.UnitInfo;
        Unit_StatisticsInfo unitInfo2 = b.UnitInfo;
        if (unitInfo1 == null || unitInfo2 == null)
          return 0;
        if (unitInfo1.Troop_Tier == unitInfo2.Troop_Tier)
          return unitInfo1.Priority.CompareTo(unitInfo2.Priority);
        return unitInfo2.Troop_Tier.CompareTo(unitInfo1.Troop_Tier);
      }));
      this.AttachAllHealItems();
      this.m_healItemGrid.repositionNow = true;
      this.m_healItemGrid.Reposition();
      this.m_healItemScrollView.ResetPosition();
      this.m_healItemScrollView.currentMomentum = Vector3.zero;
    }
  }

  protected void OnValueChanged(AllianceHealItem healItem)
  {
    this.SetSelectCounter(healItem.GetUnitId(), healItem.CurrenCount);
    this.UpdateUI(false);
  }

  protected void RemoveAllHealItems()
  {
    using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.m_allHealItems.Clear();
  }

  protected AllianceHealItem CreateHealItem()
  {
    AllianceHealItem component = UnityEngine.Object.Instantiate<GameObject>(this.m_healItemTemplate.gameObject).GetComponent<AllianceHealItem>();
    this.m_allHealItems.Add(component);
    return component;
  }

  protected void AttachAllHealItems()
  {
    using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.gameObject;
        gameObject.SetActive(true);
        gameObject.transform.SetParent(this.m_healItemGrid.transform);
        gameObject.transform.localScale = Vector3.one;
      }
    }
  }

  protected void ToggleQuickSelect()
  {
    this.m_selectAll = !this.m_selectAll;
    if (this.m_selectAll)
    {
      using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceHealItem current = enumerator.Current;
          current.CurrenCount = current.MaxCount;
        }
      }
    }
    else
    {
      int totalHonor = this.TotalHonor;
      using (List<AllianceHealItem>.Enumerator enumerator = this.m_allHealItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AllianceHealItem current = enumerator.Current;
          int num = Mathf.Min(current.MaxCount, (int) ((double) totalHonor / (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.GetUnitId()).Heal_Honor_Benifit));
          totalHonor -= (int) ((double) num * (double) ConfigManager.inst.DB_Unit_Statistics.GetData(current.GetUnitId()).Heal_Honor_Benifit);
          current.CurrenCount = num;
        }
      }
    }
  }

  public void OnQuickSelectClicked()
  {
    this.ToggleQuickSelect();
  }

  public void OnHealButtonClicked()
  {
    if (this.HealCostForHonor > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = new Dictionary<string, int>(),
        lackRes = new Dictionary<string, long>()
        {
          {
            "Texture/Alliance/alliance_honor",
            (long) (this.HonorCost - this.TotalHonor)
          }
        },
        requireRes = new Hashtable(),
        action = Utils.XLAT("id_uppercase_instant_heal"),
        gold = this.HealCostForHonor,
        buyResourCallBack = new System.Action<int>(this.SendHealRequestMessage),
        title = ScriptLocalization.Get("heal_notenoughresource_title", true),
        content = ScriptLocalization.Get("heal_notenoughresource_description", true)
      });
    else
      this.SendHealRequestMessage(0);
  }

  public void SendHealRequestMessage(int gold)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    Hashtable hashtable = new Hashtable();
    using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (current.Value > 0)
          hashtable.Add((object) current.Key, (object) current.Value);
      }
    }
    postData[(object) "troops"] = (object) hashtable;
    postData[(object) nameof (gold)] = (object) gold;
    postData[(object) "instant"] = (object) false;
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:healTroops").SendRequest(postData, new System.Action<bool, object>(this.OnHealResultReceived), true);
  }

  public void OnHealResultReceived(bool result, object data)
  {
    if (!result)
      return;
    this.OnCloseButtonPress();
  }

  public void OnInstantHealButtonClicked()
  {
    string withPara = ScriptLocalization.GetWithPara("confirm_gold_spend_heal_instant_desc", new Dictionary<string, string>()
    {
      {
        "num",
        this.InstantHealCost.ToString()
      }
    }, true);
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = (System.Action) (() => this.OnConfirmedInstantHeal()),
      description = withPara,
      goldNum = this.InstantHealCost
    });
  }

  protected void OnConfirmedInstantHeal()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "city_id"] = (object) CityManager.inst.GetCityID();
    Hashtable hashtable = new Hashtable();
    using (Dictionary<string, int>.Enumerator enumerator = this.m_selectCounter.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, int> current = enumerator.Current;
        if (current.Value > 0)
          hashtable.Add((object) current.Key, (object) current.Value);
      }
    }
    postData[(object) "troops"] = (object) hashtable;
    postData[(object) "instant"] = (object) true;
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:healTroops").SendRequest(postData, new System.Action<bool, object>(this.OnInstantHealReceived), true);
  }

  protected void OnInstantHealReceived(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
    this.OnCloseButtonPress();
  }

  public void OnCancelHealingButtonClicked()
  {
    (UIManager.inst.OpenPopup("CancelHealConfirmPopup", (Popup.PopupParameter) null) as CancelHealConfirmPopup).Init(new System.Action(this.OnCancelHealConfirmed));
  }

  public void OnCancelHealConfirmed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:cancelHealing").SendRequest(postData, new System.Action<bool, object>(this.OnCancelHealReceived), true);
  }

  protected void OnCancelHealReceived(bool result, object data)
  {
    if (!result)
      return;
    this.UpdateUI(true);
  }

  public void OnInstantFinishHealingButtonClicked()
  {
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData == null || myHospitalData.MyJobId == 0L)
      return;
    int finishHealingCost = this.InstantFinishHealingCost;
    UIManager.inst.OpenPopup("GoldConsumePopup", (Popup.PopupParameter) new GoldConsumePopup.Parameter()
    {
      confirmButtonClickEvent = new System.Action(this.OnConfirmInstantFinishHealing),
      lefttime = this.LeftHealingTime,
      goldNum = finishHealingCost,
      freetime = 0,
      description = ScriptLocalization.GetWithPara("confirm_gold_spend_cooldown_instant_desc", new Dictionary<string, string>()
      {
        {
          "num",
          finishHealingCost.ToString()
        }
      }, true)
    });
  }

  protected void OnConfirmInstantFinishHealing()
  {
    AllianceHospitalData myHospitalData = DBManager.inst.DB_AllianceHospital.GetMyHospitalData();
    if (myHospitalData == null || myHospitalData.MyJobId == 0L)
      return;
    int finishHealingCost = this.InstantFinishHealingCost;
    if (finishHealingCost <= 0)
      return;
    if (finishHealingCost > PlayerData.inst.hostPlayer.Currency)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_insufficient_gold_tip", true), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) myHospitalData.MyJobId
        },
        {
          (object) "gold",
          (object) finishHealingCost
        }
      }, new System.Action<bool, object>(this.OnInstantFinishHealingReceived), true);
  }

  protected void OnInstantFinishHealingReceived(bool result, object data)
  {
    if (!result)
      return;
    UIManager.inst.ShowBasicToast("toast_heal_timer_complete");
    this.UpdateUI(true);
  }
}
