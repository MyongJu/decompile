﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DragonKnightSystem
{
  private static DragonKnightSystem _instance;
  private GameObject _root;
  private RoomManager _roomManager;
  private DragonKnightController _controller;

  private DragonKnightSystem()
  {
  }

  public RoomManager RoomManager
  {
    get
    {
      return this._roomManager;
    }
  }

  public DragonKnightController Controller
  {
    get
    {
      return this._controller;
    }
  }

  public static DragonKnightSystem Instance
  {
    get
    {
      if (DragonKnightSystem._instance == null)
        DragonKnightSystem._instance = new DragonKnightSystem();
      return DragonKnightSystem._instance;
    }
  }

  public bool IsReady
  {
    get
    {
      return (bool) ((UnityEngine.Object) this._root);
    }
  }

  public void Setup()
  {
    this._roomManager = new RoomManager();
    this._roomManager.Init();
    string[] cachedBundleNames = DragonKnightBundleConfiguration.Instance.GetNonCachedBundleNames();
    LoadingScreenDivide.Instance.Show(cachedBundleNames.Length + 1);
    DragonKnightAssetLoader.Instance.Initialize(cachedBundleNames, (System.Action) (() =>
    {
      string path = "Prefab/DragonKnight/Objects/System/DragonKnightSystem";
      AssetManager.Instance.LoadAsync(path, (System.Action<UnityEngine.Object, bool>) ((o, ret) =>
      {
        if (ret)
        {
          this._root = UnityEngine.Object.Instantiate(o, Vector3.zero, Quaternion.identity) as GameObject;
          this._controller = this._root.GetComponent<DragonKnightController>();
          this._controller.RoomHud.Show();
          this.Show();
          AudioManager.Instance.PlayBGM("bgm_dungeon");
          this.Controller.BattleHud.HideSpeedAndQuickBattle();
          this.Controller.BattleHud.MyHUD.Reset();
          this.Controller.BattleHud.DisableSkillBar();
          this._roomManager.StartSearch();
          LoadingScreenDivide.Instance.Add();
          LoadingScreenDivide.Instance.ShowTip(Utils.XLAT("load_scene_tip"));
          if (DragonKnightUtils.DungeonRaidJob != null)
            UIManager.inst.OpenPopup("DragonKnight/DungeonCanelPopup", (Popup.PopupParameter) null);
          else if (DragonKnightUtils.GetDungeonData().RaidData.Count > 0)
            UIManager.inst.OpenPopup("DragonKnight/DungeonMopupCompletePopup", (Popup.PopupParameter) null);
          if (this._roomManager.CurrentRoom.RoomType != "arena")
          {
            if (NewTutorial.skipTutorial)
              return;
            string recordPoint = NewTutorial.Instance.GetRecordPoint("Tutorial_hole_dragon_rider");
            if ("finished" != recordPoint)
            {
              NewTutorial.Instance.InitTutorial("Tutorial_hole_dragon_rider");
              NewTutorial.Instance.LoadTutorialData(recordPoint);
            }
          }
        }
        AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }), (System.Type) null);
    }));
  }

  public void Shutdown()
  {
    this.Hide();
    this._roomManager = (RoomManager) null;
    this._controller = (DragonKnightController) null;
    if (this.IsReady)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this._root);
      this._root = (GameObject) null;
    }
    BattleManager.Instance.Shutdown();
    AudioManager.Instance.StopCurrentBGM();
  }

  public void Show()
  {
    if (this.IsReady)
      this._root.SetActive(true);
    AudioManager.Instance.StopCurrentBGM();
    UIManager.inst.publicHUD.SwitchToPart(GAME_LOCATION.DRAGON_KNIGHT);
  }

  public void Hide()
  {
    if (this.IsReady)
      this._root.SetActive(false);
    AudioManager.Instance.StopCurrentBGM();
  }
}
