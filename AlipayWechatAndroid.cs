﻿// Decompiled with JetBrains decompiler
// Type: AlipayWechatAndroid
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using UI;
using UnityEngine;

public class AlipayWechatAndroid : BasePaymentWrapper
{
  private static readonly object locker = new object();
  private const string FUNC_PAYMENT_INITIALIZE_SUCCESS = "OnPaymentInitializeSuccess";
  private const string FUNC_PAYMENT_INITIALIZE_ERROR = "OnPaymentInitializeError";
  private const string FUNC_NAME_PURCHASE_ERROR = "OnPaymentPurchaseError";
  private const string FUNC_NAME_PAYMENT_PURCHASE_SUCCESS = "OnPaymentPurchaseSuccess";
  private static AlipayWechatAndroid _instance;
  private string _targetGameObjectName;
  private GameObject _targetGameObject;

  public static AlipayWechatAndroid Instance
  {
    get
    {
      if (AlipayWechatAndroid._instance == null)
      {
        lock (AlipayWechatAndroid.locker)
          AlipayWechatAndroid._instance = new AlipayWechatAndroid();
      }
      return AlipayWechatAndroid._instance;
    }
  }

  private GameObject TargetGameObject
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this._targetGameObject))
        this._targetGameObject = GameObject.Find(this._targetGameObjectName);
      return this._targetGameObject;
    }
  }

  public override void SetGameObject(string gameObjectName)
  {
    this._targetGameObjectName = gameObjectName;
    AlipayAndroid.Instance.SetGameObject(gameObjectName);
    WechatPayAndroid.Instance.SetGameObject(gameObjectName);
  }

  public override void SetCurrencyWhitelist(string whitelist)
  {
  }

  public override bool GetSubsRenewing(string productId)
  {
    return false;
  }

  public override bool CanCheckSubs()
  {
    return false;
  }

  public override bool CanMakePurchases()
  {
    return true;
  }

  public override void StartHelper()
  {
    AlipayAndroid.Instance.StartHelper();
    WechatPayAndroid.Instance.StartHelper();
  }

  public override void Buy(string productId, string throughCargo)
  {
    D.error((object) "have not implement");
  }

  public override void Buy(string productId, string serverId, string throughCargo)
  {
    UIManager.inst.HideManualBlocker();
    UIManager.inst.OpenPopup("IAP/IAPPaymentModePopup", (Popup.PopupParameter) new IapPaymentModePopup.Parameter()
    {
      Callback = (System.Action<IapPaymentModePopup.PaymentMode>) (paymentMode =>
      {
        UIManager.inst.ShowManualBlocker();
        switch (paymentMode)
        {
          case IapPaymentModePopup.PaymentMode.None:
            this.TargetGameObject.BroadcastMessage("OnPaymentPurchaseError", (object) string.Empty);
            break;
          case IapPaymentModePopup.PaymentMode.Alipay:
            AlipayAndroid.Instance.Buy(productId, serverId, throughCargo);
            break;
          case IapPaymentModePopup.PaymentMode.Wechat:
            WechatPayAndroid.Instance.Buy(productId, serverId, throughCargo);
            break;
        }
      })
    });
  }
}
