﻿// Decompiled with JetBrains decompiler
// Type: AllianceDiplomacyDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceDiplomacyDlg : UI.Dialog
{
  public ArrayList mFriendItems = new ArrayList();
  public ArrayList mEnemyItems = new ArrayList();
  public ArrayList mInvitesItems = new ArrayList();
  private List<GameObject> m_SearchList = new List<GameObject>();
  public GameObject m_SearchPage;
  public GameObject m_EnemiesPage;
  public GameObject m_FriendsPage;
  public GameObject m_InvitesRecieverPage;
  public GameObject m_InvitesSenderPage;
  public PageTabsMonitor m_Tabs4;
  public PageTabsMonitor m_Tabs2;
  public UIInput m_SearchInput;
  public UIButton m_SearchButton;
  public GameObject m_SearchItemPrefab;
  public GameObject mFriendItemPrefab;
  public GameObject mEnemyItemPrefab;
  public GameObject mNoEnemiesItem;
  public GameObject mNoFriendsItem;
  public GameObject mNoInvitesItem;
  public GameObject mNoSentInvitesItem;
  public GameObject mReceivedInviteItemPrefab;
  public UIScrollView m_SearchScrollView;
  public UIScrollView m_EnemyScrollView;
  public UIScrollView m_FriendScrollView;
  public UIScrollView m_InviteScrollView;
  public UITable m_SearchTable;
  public UITable m_FriendTable;
  public UITable m_EnemyTable;
  public UITable m_InviteTable;
  public GameObject m_Badge;
  public UILabel m_BadgeCount;
  private PageTabsMonitor mTabs;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    this.RegisterListeners();
    if (DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.Get(PlayerData.inst.uid).title == 6)
    {
      this.mTabs = this.m_Tabs4;
      this.mTabs.gameObject.SetActive(true);
      this.m_Tabs2.gameObject.SetActive(false);
      this.mTabs.onTabSelected += new System.Action<int>(this.OnTabSelected);
      this.mTabs.SetCurrentTab(0, true);
      this.m_SearchInput.defaultText = "TAP HERE TO SEARCH";
      this.m_SearchInput.value = string.Empty;
      this.m_SearchButton.isEnabled = false;
    }
    else
    {
      this.mTabs = this.m_Tabs2;
      this.mTabs.gameObject.SetActive(true);
      this.m_Tabs4.gameObject.SetActive(false);
      this.mTabs.onTabSelected += new System.Action<int>(this.OnTabSelected);
      this.mTabs.SetCurrentTab(3, true);
    }
    this.Refresh();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    this.UnregisterListeners();
  }

  private void RegisterListeners()
  {
    MessageHub.inst.GetPortByAction("alliance_friend_invite").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("add_alliance_enemy").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("revoke_friend_invite").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("accept_friend_invite").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("deny_friend_invite").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("be_alliance_enemy").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("neutralize_alliance").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("del_alliance_diplomacy").AddEvent(new System.Action<object>(this.OnDiplomacyChanged));
  }

  private void UnregisterListeners()
  {
    MessageHub.inst.GetPortByAction("alliance_friend_invite").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("add_alliance_enemy").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("revoke_friend_invite").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("accept_friend_invite").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("deny_friend_invite").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("be_alliance_enemy").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("neutralize_alliance").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
    MessageHub.inst.GetPortByAction("del_alliance_diplomacy").RemoveEvent(new System.Action<object>(this.OnDiplomacyChanged));
  }

  private void OnDiplomacyChanged(object data)
  {
    this.Refresh();
  }

  private void Refresh()
  {
    this.SearchUpdate();
    this.FriendsUpdate();
    this.InvitesUpdate();
    this.EnemiesUpdate();
  }

  private void HidePages()
  {
    this.m_SearchPage.SetActive(false);
    this.m_EnemiesPage.SetActive(false);
    this.m_FriendsPage.SetActive(false);
    this.m_InvitesRecieverPage.SetActive(false);
    this.m_InvitesSenderPage.SetActive(false);
  }

  private void ClearSearchList()
  {
    using (List<GameObject>.Enumerator enumerator = this.m_SearchList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current);
      }
    }
    this.m_SearchList.Clear();
  }

  private void OnTabSelected(int index)
  {
    switch (index)
    {
      case 0:
        this.HidePages();
        this.m_SearchPage.SetActive(true);
        this.SearchUpdate();
        break;
      case 1:
        this.HidePages();
        this.m_FriendsPage.SetActive(true);
        this.FriendsUpdate();
        break;
      case 2:
        this.HidePages();
        this.m_InvitesRecieverPage.SetActive(true);
        this.InvitesUpdate();
        break;
      case 3:
        this.HidePages();
        this.m_EnemiesPage.SetActive(true);
        this.EnemiesUpdate();
        break;
    }
  }

  private void SearchUpdate()
  {
    int count = this.m_SearchList.Count;
    for (int index = 0; index < count; ++index)
    {
      AllianceDiplomacySearchItem component = this.m_SearchList[index].GetComponent<AllianceDiplomacySearchItem>();
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(component.allianceId);
      if (allianceData != null)
      {
        AllianceDiplomacyData allianceDiplomacyData = PlayerData.inst.allianceData.diplomacy.Get(allianceData.allianceId);
        int diplomacy = allianceDiplomacyData != null ? allianceDiplomacyData.status : 0;
        component.SetDetails((int) allianceData.allianceId, allianceData.allianceSymbolCode, allianceData.allianceName, allianceData.allianceAcronym, allianceData.memberCount, allianceData.memberMax, (int) allianceData.giftPoints, allianceData.power, allianceData.allianceLevel, 0.0f, diplomacy, new System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction>(this.onStateChanged));
      }
    }
  }

  public void onSearchInputChanged()
  {
    this.m_SearchButton.isEnabled = this.m_SearchInput.value.Length >= 3;
  }

  public void OnSearch()
  {
    this.ClearSearchList();
    MessageHub.inst.GetPortByAction("Alliance:searchAllianceByUser").SendRequest(new Hashtable()
    {
      {
        (object) "context",
        (object) this.m_SearchInput.value
      }
    }, new System.Action<bool, object>(this.DiplomacySearchCallback), true);
  }

  private void DiplomacySearchCallback(bool ret, object data)
  {
    this.ClearSearchList();
    ArrayList arrayList = data as ArrayList;
    if (arrayList == null)
      return;
    int num = Mathf.Min(arrayList.Count, 20);
    for (int index = 0; index < num; ++index)
    {
      Hashtable hashtable = arrayList[index] as Hashtable;
      AllianceData allianceData = new AllianceData();
      allianceData.Decode(arrayList[index], (long) Utils.ClientSideUnixUTCSeconds);
      AllianceDiplomacyData allianceDiplomacyData = PlayerData.inst.allianceData.diplomacy.Get(allianceData.allianceId);
      int diplomacy = allianceDiplomacyData != null ? allianceDiplomacyData.status : 0;
      AllianceDiplomacySearchItem component = Utils.DuplicateGOB(this.m_SearchItemPrefab).GetComponent<AllianceDiplomacySearchItem>();
      component.gameObject.SetActive(true);
      component.SetDetails((int) allianceData.allianceId, allianceData.allianceSymbolCode, allianceData.allianceName, allianceData.allianceAcronym, allianceData.memberCount, allianceData.memberMax, (int) allianceData.giftPoints, allianceData.power, allianceData.allianceLevel, 0.0f, diplomacy, new System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction>(this.onStateChanged));
      this.m_SearchList.Add(component.gameObject);
    }
    this.m_SearchTable.Reposition();
    this.m_SearchScrollView.ResetPosition();
  }

  public void onStateChanged(int allianceID, AllianceDiplomacySearchItem.DiplomacyAction action)
  {
    switch (action)
    {
      case AllianceDiplomacySearchItem.DiplomacyAction.FRIEND:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "send", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.DiplomacySendInviteCallback), true);
        break;
      case AllianceDiplomacySearchItem.DiplomacyAction.NEUTRAL:
        MessageHub.inst.GetPortByAction("Alliance:setDiplomacyRelationship").SendRequest(Utils.Hash((object) "relationship", (object) "neutral", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.ErrorHandler), true);
        break;
      case AllianceDiplomacySearchItem.DiplomacyAction.ENEMY:
        MessageHub.inst.GetPortByAction("Alliance:setDiplomacyRelationship").SendRequest(Utils.Hash((object) "relationship", (object) "enemy", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.ErrorHandler), true);
        break;
    }
  }

  private void DiplomacySendInviteCallback(bool ret, object data)
  {
    if (ret)
    {
      UIManager.inst.toast.Show("Friendship invite sent.", (System.Action) null, 4f, false);
      this.Refresh();
    }
    else
      this.ErrorHandler(ret, data);
  }

  private void FriendsUpdate()
  {
    foreach (GameObject mFriendItem in this.mFriendItems)
    {
      mFriendItem.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) mFriendItem);
    }
    this.mFriendItems.Clear();
    Vector3 localPosition = this.mEnemyItemPrefab.transform.localPosition;
    AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    int num1 = 0;
    float num2 = 0.0f;
    using (Dictionary<long, AllianceDiplomacyData>.ValueCollection.Enumerator enumerator = allianceData1.diplomacy.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceDiplomacyData current = enumerator.Current;
        if (current.isFriends)
        {
          int oppAllianceId = (int) current.oppAllianceId;
          AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get((long) oppAllianceId);
          string allianceName = allianceData2.allianceName;
          string allianceAcronym = allianceData2.allianceAcronym;
          int allianceSymbolCode = allianceData2.allianceSymbolCode;
          int allianceLevel = allianceData2.allianceLevel;
          long power = allianceData2.power;
          int memberCount = allianceData2.memberCount;
          int memberMax = allianceData2.memberMax;
          AllianceDiplomacySearchItem component = Utils.DuplicateGOB(this.mFriendItemPrefab).GetComponent<AllianceDiplomacySearchItem>();
          component.gameObject.SetActive(true);
          component.SetDetails(oppAllianceId, allianceSymbolCode, allianceName, allianceAcronym, memberCount, memberMax, (int) allianceData2.giftPoints, power, allianceLevel, 0.0f, current.status, new System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction>(this.onFriendStateChanged));
          component.transform.localPosition = localPosition + new Vector3(0.0f, num2 * -435f, 0.0f);
          this.mFriendItems.Add((object) component.gameObject);
          ++num2;
          ++num1;
        }
      }
    }
    this.mNoFriendsItem.SetActive(num1 == 0);
    this.m_FriendTable.Reposition();
    this.m_FriendScrollView.ResetPosition();
  }

  public void onFriendStateChanged(int allianceID, AllianceDiplomacySearchItem.DiplomacyAction action)
  {
    switch (action)
    {
      case AllianceDiplomacySearchItem.DiplomacyAction.NEUTRAL:
        MessageHub.inst.GetPortByAction("Alliance:setDiplomacyRelationship").SendRequest(Utils.Hash((object) "relationship", (object) "neutral", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.DiplomacyFriendStateCallback), true);
        break;
      case AllianceDiplomacySearchItem.DiplomacyAction.ENEMY:
        MessageHub.inst.GetPortByAction("Alliance:setDiplomacyRelationship").SendRequest(Utils.Hash((object) "relationship", (object) "enemy", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.DiplomacyFriendStateCallback), true);
        break;
    }
  }

  private void DiplomacyFriendStateCallback(bool ret, object data)
  {
    if (ret)
      this.FriendsUpdate();
    else
      this.ErrorHandler(ret, data);
  }

  private void InvitesUpdate()
  {
    foreach (GameObject mInvitesItem in this.mInvitesItems)
    {
      mInvitesItem.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) mInvitesItem);
    }
    this.mInvitesItems.Clear();
    this.mNoInvitesItem.SetActive(false);
    this.mNoSentInvitesItem.SetActive(false);
    AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    int num = 0;
    using (Dictionary<long, AllianceDiplomacyData>.ValueCollection.Enumerator enumerator = allianceData1.diplomacy.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceDiplomacyData current = enumerator.Current;
        if (current.isInvited)
        {
          int oppAllianceId = (int) current.oppAllianceId;
          AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get((long) oppAllianceId);
          string allianceName = allianceData2.allianceName;
          string allianceAcronym = allianceData2.allianceAcronym;
          int allianceLevel = allianceData2.allianceLevel;
          long power = allianceData2.power;
          int memberCount = allianceData2.memberCount;
          int memberMax = allianceData2.memberMax;
          string diplomacy = !current.isFriends ? (!current.isEnemy ? "icon_neutral" : "icon_enemy") : "icon_friend";
          int levelByPoints = ConfigManager.inst.DB_Gift_Level.GetLevelByPoints((int) allianceData2.giftPoints);
          AllianceDiplomacyInviteItem component = Utils.DuplicateGOB(this.mReceivedInviteItemPrefab).GetComponent<AllianceDiplomacyInviteItem>();
          component.gameObject.SetActive(true);
          component.SetDetails(false, oppAllianceId, string.Empty, allianceName, allianceAcronym, memberCount.ToString(), memberMax.ToString(), levelByPoints.ToString(), power.ToString(), allianceLevel.ToString(), 0.0f, diplomacy, new System.Action<int, AllianceDiplomacyInviteItem.DiplomacyInviteAction>(this.onInviteStateChanged));
          this.mInvitesItems.Add((object) component.gameObject);
          ++num;
        }
      }
    }
    this.mNoInvitesItem.SetActive(num == 0);
    this.m_Badge.gameObject.SetActive(num > 0);
    this.m_BadgeCount.text = num.ToString();
    this.m_InviteTable.Reposition();
    this.m_InviteScrollView.ResetPosition();
  }

  public void onInviteStateChanged(int allianceID, AllianceDiplomacyInviteItem.DiplomacyInviteAction action)
  {
    switch (action)
    {
      case AllianceDiplomacyInviteItem.DiplomacyInviteAction.ACCEPT:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "accept", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.InviteCallback), true);
        break;
      case AllianceDiplomacyInviteItem.DiplomacyInviteAction.DENY:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "deny", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.InviteCallback), true);
        break;
      case AllianceDiplomacyInviteItem.DiplomacyInviteAction.MAKE_ENEMY:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "makeenemy", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.InviteCallback), true);
        break;
      case AllianceDiplomacyInviteItem.DiplomacyInviteAction.REVOKE:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "revoke", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.InviteCallback), true);
        break;
    }
  }

  private void InviteCallback(bool ret, object data)
  {
    this.InvitesUpdate();
  }

  private void EnemiesUpdate()
  {
    foreach (GameObject mEnemyItem in this.mEnemyItems)
    {
      mEnemyItem.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) mEnemyItem);
    }
    this.mEnemyItems.Clear();
    this.mNoEnemiesItem.SetActive(false);
    AllianceData allianceData1 = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    int num = 0;
    using (Dictionary<long, AllianceDiplomacyData>.ValueCollection.Enumerator enumerator = allianceData1.diplomacy.datas.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceDiplomacyData current = enumerator.Current;
        if (current != null && current.isEnemy)
        {
          int oppAllianceId = (int) current.oppAllianceId;
          AllianceData allianceData2 = DBManager.inst.DB_Alliance.Get((long) oppAllianceId);
          if (allianceData2 != null)
          {
            string allianceName = allianceData2.allianceName;
            string allianceAcronym = allianceData2.allianceAcronym;
            int allianceSymbolCode = allianceData2.allianceSymbolCode;
            int allianceLevel = allianceData2.allianceLevel;
            long power = allianceData2.power;
            int memberCount = allianceData2.memberCount;
            int memberMax = allianceData2.memberMax;
            AllianceDiplomacySearchItem component = Utils.DuplicateGOB(this.mEnemyItemPrefab).GetComponent<AllianceDiplomacySearchItem>();
            component.gameObject.SetActive(true);
            component.SetDetails(oppAllianceId, allianceSymbolCode, allianceName, allianceAcronym, memberCount, memberMax, (int) allianceData2.giftPoints, power, allianceLevel, 0.0f, current.status, new System.Action<int, AllianceDiplomacySearchItem.DiplomacyAction>(this.onEnemyStateChanged));
            this.mEnemyItems.Add((object) component.gameObject);
            ++num;
          }
        }
      }
    }
    this.mNoEnemiesItem.SetActive(num == 0);
    this.m_EnemyTable.Reposition();
    this.m_EnemyScrollView.ResetPosition();
  }

  public void onEnemyStateChanged(int allianceID, AllianceDiplomacySearchItem.DiplomacyAction action)
  {
    switch (action)
    {
      case AllianceDiplomacySearchItem.DiplomacyAction.FRIEND:
        MessageHub.inst.GetPortByAction("Alliance:manageDiplomacyInvite").SendRequest(Utils.Hash((object) "operate", (object) "send", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.DiplomacySendInviteCallback), true);
        break;
      case AllianceDiplomacySearchItem.DiplomacyAction.NEUTRAL:
        MessageHub.inst.GetPortByAction("Alliance:setDiplomacyRelationship").SendRequest(Utils.Hash((object) "relationship", (object) "neutral", (object) "alliance_id", (object) allianceID), new System.Action<bool, object>(this.DiplomacyEnemyStateCallback), true);
        break;
    }
  }

  private void DiplomacyEnemyStateCallback(bool ret, object data)
  {
    if (ret)
      this.EnemiesUpdate();
    else
      this.ErrorHandler(ret, data);
  }

  private void ErrorHandler(bool ret, object data)
  {
    if (ret)
      return;
    this.Refresh();
  }
}
