﻿// Decompiled with JetBrains decompiler
// Type: VaultDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class VaultDlg : UI.Dialog
{
  private Dictionary<int, VaultSlotItemRenderer> itemDict = new Dictionary<int, VaultSlotItemRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UITexture bkgTexture;
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private System.Action func;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    VaultDlg.Parameter parameter = orgParam as VaultDlg.Parameter;
    if (parameter != null && parameter.func != null)
      this.func = parameter.func;
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.bkgTexture, "Texture/GUI_Textures/vault_top_banner", (System.Action<bool>) null, true, true, string.Empty);
    this.ShowVaultContent();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (this.func != null)
      this.func();
    this.ClearData();
  }

  private void UpdateUI()
  {
    this.ClearData();
    this.ShowVaultContent();
  }

  private void ShowVaultContent()
  {
    List<TreasuryInfo> treasuryInfoList = ConfigManager.inst.DB_Treasury.GetTreasuryInfoList();
    if (treasuryInfoList != null)
    {
      for (int index = 0; index < treasuryInfoList.Count; ++index)
      {
        VaultSlotItemRenderer itemRenderer = this.CreateItemRenderer(treasuryInfoList[index]);
        this.itemDict.Add(treasuryInfoList[index].internalId, itemRenderer);
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, VaultSlotItemRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, VaultSlotItemRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private VaultSlotItemRenderer CreateItemRenderer(TreasuryInfo treasuryInfo)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    VaultSlotItemRenderer component = gameObject.GetComponent<VaultSlotItemRenderer>();
    component.SetData(treasuryInfo);
    return component;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public System.Action func;
  }
}
