﻿// Decompiled with JetBrains decompiler
// Type: ArtifactSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class ArtifactSlot : MonoBehaviour
{
  [SerializeField]
  private bool _interactiveAble = true;
  private const int WONDER_ARTIFACT_SLOT_INDEX = 1;
  [SerializeField]
  private GameObject _rootEquipedState;
  [SerializeField]
  private GameObject _rootUnequpedState;
  [SerializeField]
  private GameObject _rootTagEquipTip;
  [SerializeField]
  private GameObject _rootTagEquipable;
  [SerializeField]
  private UITexture _artifactIcon;
  [SerializeField]
  private int _artifactSlotIndex;
  protected long _userId;

  protected bool IsSlotForWonder
  {
    get
    {
      return 1 == this._artifactSlotIndex;
    }
  }

  public void RefreshForPlayer(long uesrId)
  {
    this._userId = uesrId;
    DB.HeroData heroData = DBManager.inst.DB_hero.Get(this._userId);
    if (heroData == null)
      return;
    ArtifactData artifactBySlotIndex = heroData.GetArtifactBySlotIndex(this._artifactSlotIndex);
    if (artifactBySlotIndex != null)
    {
      ArtifactInfo artifactInfo = ConfigManager.inst.DB_Artifact.Get(artifactBySlotIndex.ArtifactId);
      if (artifactInfo != null)
        BuilderFactory.Instance.HandyBuild((UIWidget) this._artifactIcon, artifactInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this._rootEquipedState.SetActive(artifactBySlotIndex != null);
    this._rootUnequpedState.SetActive(artifactBySlotIndex == null);
    if (artifactBySlotIndex == null)
    {
      if (this._interactiveAble && (long) PlayerData.inst.cityId == this._userId && !this.IsSlotForWonder)
      {
        CityData cityData = DBManager.inst.DB_City.Get(this._userId);
        if (cityData != null)
          this._rootTagEquipTip.SetActive(cityData.GetAllUnequipedArtifacts(this._artifactSlotIndex).Count > 0);
      }
      else
        this._rootTagEquipTip.SetActive(false);
    }
    this._rootTagEquipable.SetActive(!this.IsSlotForWonder);
    if (this.IsSlotForWonder && artifactBySlotIndex == null)
      this.gameObject.SetActive(false);
    else
      this.gameObject.SetActive(true);
  }

  public void OnClicked()
  {
    ArtifactData artifactBySlotIndex = DBManager.inst.DB_hero.Get(this._userId).GetArtifactBySlotIndex(this._artifactSlotIndex);
    if (this.IsSlotForWonder)
    {
      if (artifactBySlotIndex == null)
        return;
      UIManager.inst.OpenPopup("Wonder/KingdomArtifactPopup", (Popup.PopupParameter) new KingdomArtifactPopup.Parameter()
      {
        artifactId = artifactBySlotIndex.ArtifactId
      });
    }
    else if (this._userId != (long) PlayerData.inst.cityId || !this._interactiveAble)
    {
      if (artifactBySlotIndex == null)
        return;
      UIManager.inst.OpenPopup("Artifact/ArtifactDetailsPopup", (Popup.PopupParameter) new ArtifactDetailsPopup.Parameter()
      {
        artifactId = artifactBySlotIndex.ArtifactId
      });
    }
    else
      UIManager.inst.OpenDlg("PlayerProfile/PlayerArtifactDlg", (UI.Dialog.DialogParameter) new PlayerArtifactDialog.Parameter()
      {
        ArtifactSlotIndex = this._artifactSlotIndex
      }, 1 != 0, 1 != 0, 1 != 0);
  }
}
