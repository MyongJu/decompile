﻿// Decompiled with JetBrains decompiler
// Type: AllianceApplication
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UI;

public class AllianceApplication : Popup
{
  public UIInput m_MessageInput;
  public UIButton m_SendButton;
  private long m_AllianceID;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_AllianceID = (orgParam as AllianceApplication.Parameter).allianceId;
    this.m_SendButton.isEnabled = false;
    this.m_MessageInput.defaultText = ScriptLocalization.Get("alliance_application_message_placeholder", true);
    this.m_MessageInput.value = string.Empty;
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnSendClicked()
  {
    this.OnCloseClicked();
    AllianceManager.Instance.ApplyComment(this.m_AllianceID, this.m_MessageInput.value, (System.Action<bool, object>) null);
  }

  private void Update()
  {
    this.m_SendButton.isEnabled = this.m_MessageInput.value.Length > 0;
  }

  public class Parameter : Popup.PopupParameter
  {
    public long allianceId;
  }
}
