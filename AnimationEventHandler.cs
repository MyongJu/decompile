﻿// Decompiled with JetBrains decompiler
// Type: AnimationEventHandler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationEventHandler : MonoBehaviour
{
  public const int EndEventId = 2147483647;
  public const string FunctionName = "AnimationEventCallback";

  public event System.Action<AnimationEvent> OnAnimationEvent;

  private void AnimationEventCallback(AnimationEvent e)
  {
    if (!(bool) ((UnityEngine.Object) this.gameObject) || !this.enabled || this.OnAnimationEvent == null)
      return;
    this.OnAnimationEvent(e);
  }
}
