﻿// Decompiled with JetBrains decompiler
// Type: AllianceTreasuryVaultPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class AllianceTreasuryVaultPayload
{
  protected List<AllianceTreasuryVaultData> _allSendedChestData = new List<AllianceTreasuryVaultData>();
  private static AllianceTreasuryVaultPayload _instance;
  public int AllianceTreasuryPoint;

  public static AllianceTreasuryVaultPayload Instance
  {
    get
    {
      if (AllianceTreasuryVaultPayload._instance == null)
        AllianceTreasuryVaultPayload._instance = new AllianceTreasuryVaultPayload();
      return AllianceTreasuryVaultPayload._instance;
    }
  }

  public List<AllianceTreasuryVaultData> AllSendedChestData
  {
    get
    {
      return this._allSendedChestData;
    }
  }

  public int GetUnOpenedChestCount()
  {
    int num = 0;
    using (List<AllianceTreasuryVaultData>.Enumerator enumerator = this._allSendedChestData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceTreasuryVaultData current = enumerator.Current;
        if (current.status != "done" && !current.HaveClaimed(PlayerData.inst.uid))
          ++num;
      }
    }
    return num;
  }

  public void LoadTreasuryList(System.Action callback)
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      }
    };
    RequestManager.inst.SendRequest("TreasuryVault:getTreasuryVaultList", postData, (System.Action<bool, object>) ((result, data) =>
    {
      this._allSendedChestData.Clear();
      if (result)
      {
        Hashtable hashtable1 = data as Hashtable;
        if (hashtable1 != null && hashtable1.ContainsKey((object) "list"))
        {
          ArrayList arrayList = hashtable1[(object) "list"] as ArrayList;
          if (arrayList != null)
          {
            foreach (object obj in arrayList)
            {
              Hashtable hashtable2 = obj as Hashtable;
              if (hashtable2 != null)
              {
                AllianceTreasuryVaultData treasuryVaultData = new AllianceTreasuryVaultData();
                treasuryVaultData.Decode(hashtable2);
                this._allSendedChestData.Add(treasuryVaultData);
              }
            }
          }
        }
        if (hashtable1 != null && hashtable1.ContainsKey((object) "alliance_treasury_vault_count"))
        {
          int result1 = 0;
          int.TryParse(hashtable1[(object) "alliance_treasury_vault_count"].ToString(), out result1);
          AllianceTreasuryCountManager.Instance.SetUnopendChestCount(result1);
        }
        this._allSendedChestData.Sort();
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  public void LoadAllianceTreasuryPoint(System.Action callback)
  {
    Hashtable postData = new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      }
    };
    RequestManager.inst.SendRequest("Alliance:getAllianceTreasuryPoint", postData, (System.Action<bool, object>) ((result, data) =>
    {
      this._allSendedChestData.Clear();
      if (result)
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "treasury_point"))
          int.TryParse(hashtable[(object) "treasury_point"].ToString(), out this.AllianceTreasuryPoint);
      }
      if (callback == null)
        return;
      callback();
    }), true);
  }

  public bool TryRemoveExpired()
  {
    bool flag = false;
    for (int index = this._allSendedChestData.Count - 1; index >= 0; --index)
    {
      if (this._allSendedChestData[index].expireTime < NetServerTime.inst.ServerTimestamp)
      {
        this._allSendedChestData.RemoveAt(index);
        flag = true;
      }
    }
    return flag;
  }
}
