﻿// Decompiled with JetBrains decompiler
// Type: BuildingGloryMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class BuildingGloryMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "group_id")]
  public int groupId;
  [Config(Name = "req_buildings_id")]
  public int reqBuildingId;
  [Config(Name = "next_id")]
  public int nextId;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "power")]
  public long power;
  [Config(Name = "req_item_id")]
  public int reqItemId;
  [Config(Name = "req_item_value")]
  public int reqItemCount;
  [Config(Name = "link_iap_package_group_id")]
  public int iapPackageId;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefits;

  public int ReqItemCount
  {
    get
    {
      int finalData = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.reqItemCount, "calc_building_glory_material_1_cost_decrease");
      if (finalData <= 0)
        return 0;
      return finalData;
    }
  }

  public bool IsMaxGlory
  {
    get
    {
      return this.nextId <= 0;
    }
  }

  public BuildingGloryMainInfo NextBuildingGloryMainInfo
  {
    get
    {
      return ConfigManager.inst.DB_BuildingGloryMain.Get(this.nextId);
    }
  }

  public int ReqBuildingId
  {
    get
    {
      return this.reqBuildingId;
    }
  }

  public int ReqBuildingMinLevel
  {
    get
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.ReqBuildingId);
      if (data != null)
        return data.Building_Lvl;
      return 1;
    }
  }

  public string BuildingType
  {
    get
    {
      BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.ReqBuildingId);
      if (data != null)
        return data.Type;
      return string.Empty;
    }
  }

  public int BuildingGloryMaxLevel
  {
    get
    {
      int num = 0;
      List<BuildingGloryMainInfo> gloryMainInfoList = ConfigManager.inst.DB_BuildingGloryMain.GetBuildingGloryMainInfoList();
      if (gloryMainInfoList != null)
      {
        List<BuildingGloryMainInfo> all = gloryMainInfoList.FindAll((Predicate<BuildingGloryMainInfo>) (x => x.BuildingType == this.BuildingType));
        if (all != null)
        {
          for (int index = 0; index < all.Count; ++index)
          {
            if (num < all[index].level)
              num = all[index].level;
          }
        }
      }
      return num;
    }
  }
}
