﻿// Decompiled with JetBrains decompiler
// Type: AllianceBossResultPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using UI;

public class AllianceBossResultPopup : BaseReportPopup
{
  private const string bossiconpath = "Texture/AllianceBoss/";
  private AllianceBossResultContent abrc;
  public UILabel body;
  public Icon content;
  public UILabel hint;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/AllianceBoss/mail_report_alliance_boss", (System.Action<bool>) null, true, false, string.Empty);
    this.body.text = this.param.mail.GetBodyString();
    AllianceBossInfo allianceBossInfo = ConfigManager.inst.DB_AllianceBoss.Get(this.abrc.boss_id);
    string str = ScriptLocalization.Get(allianceBossInfo.name, true);
    this.content.SetData(allianceBossInfo.ImagePath, str, this.abrc.boss_total_hp.ToString(), this.abrc.rally_times.ToString(), this.abrc.boss_lost_hp.ToString());
    if (this.abrc.is_win == 1)
      this.hint.gameObject.SetActive(false);
    else
      this.hint.gameObject.SetActive(true);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.abrc = JsonReader.Deserialize<AllianceBossResultContent>(Utils.Object2Json((object) this.param.hashtable));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
