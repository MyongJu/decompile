﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryRewardContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AnniversaryRewardContainer : MonoBehaviour
{
  private List<Icon> _icons = new List<Icon>();
  public UILabel title;
  public UITable grid;
  public Icon itemPrefab;
  public UIScrollView scrollView;

  public void SetData(string title, Reward reward, int factor = 1)
  {
    this.title.text = title;
    if (reward == null)
      return;
    List<Reward.RewardsValuePair> rewards = reward.GetRewards();
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      this.GetIcon().FeedData((IComponentData) new IconData(ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID).ImagePath, new string[1]
      {
        (rewardsValuePair.value * factor).ToString()
      })
      {
        Data = (object) rewardsValuePair.internalID
      });
    }
    this.grid.Reposition();
  }

  public void Clear()
  {
    for (int index = 0; index < this._icons.Count; ++index)
      this._icons[index].Dispose();
    this._icons.Clear();
  }

  private Icon GetIcon()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    Vector3 localScale = go.transform.localScale;
    localScale.x = localScale.y = 0.75f;
    go.transform.localScale = localScale;
    NGUITools.SetActive(go, true);
    Icon component = go.GetComponent<Icon>();
    component.StartAutoTip();
    component.StopAutoFillName();
    this._icons.Add(component);
    return component;
  }
}
