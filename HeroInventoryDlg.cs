﻿// Decompiled with JetBrains decompiler
// Type: HeroInventoryDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroInventoryDlg : UI.Dialog
{
  private Dictionary<int, HeroInventorySlot> _itemDict = new Dictionary<int, HeroInventorySlot>();
  private Dictionary<int, HeroInventoryFragmentSlot> _itemFragmentDict = new Dictionary<int, HeroInventoryFragmentSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private int _tabPickedIndex = -1;
  private const int HERO_TITLES_COUNT = 8;
  [SerializeField]
  private UIScrollView _heroScrollView;
  [SerializeField]
  private UITable _heroTable;
  [SerializeField]
  private GameObject _heroItemPrefab;
  [SerializeField]
  private GameObject _heroItemRoot;
  [SerializeField]
  private UILabel _noHeroHintText;
  [SerializeField]
  private UIScrollView _heroFragmentScrollView;
  [SerializeField]
  private UITable _heroFragmentTable;
  [SerializeField]
  private GameObject _heroFragmentPrefab;
  [SerializeField]
  private GameObject _heroFragmentRoot;
  [SerializeField]
  private UILabel _noHeroFragmentHintText;
  [SerializeField]
  private GameObject _leftHeroFragmentRoot;
  [SerializeField]
  private ParliamentHeroItem _leftHeroFragmentItem;
  [SerializeField]
  private UILabel _leftHeroFragmentName;
  [SerializeField]
  private UILabel _leftHeroFragmentDesc;
  [SerializeField]
  private GameObject _heroInfoRoot;
  [SerializeField]
  private GameObject _heroFragmentInfoRoot;
  [SerializeField]
  private EquipmentForgeTabBar _tabBar;
  private bool _topShowSummonableHero;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    HeroInventoryDlg.Parameter parameter = orgParam as HeroInventoryDlg.Parameter;
    if (parameter != null)
      this._topShowSummonableHero = parameter.topShowSummonableHero;
    this.Initialize();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearAllData();
  }

  private void Initialize()
  {
    this._tabBar.SelectedIndex = this._tabPickedIndex < 0 ? 0 : this._tabPickedIndex;
    this.HandleTabSelected(this._tabBar.SelectedIndex);
    this.AddEventHandler();
    if (!this._topShowSummonableHero)
      return;
    this._topShowSummonableHero = false;
    this.CheckSummonableHero2TopShow();
  }

  private void CheckSummonableHero2TopShow()
  {
    using (Dictionary<int, HeroInventorySlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, HeroInventorySlot> current = enumerator.Current;
        if (current.Value.HeroSummonable)
        {
          float num = 50f;
          SpringPanel.Begin(this._heroScrollView.gameObject, new Vector3(this._heroScrollView.transform.localPosition.x, -(current.Value.transform.localPosition.y - this._itemDict[0].transform.localPosition.y + num)), 13f);
          break;
        }
      }
    }
  }

  private void HandleTabSelected(int index)
  {
    if (this._tabBar.SelectedIndex > 8)
      this.ShowHeroFragment();
    else
      this.ShowHeroContent(this._tabBar.SelectedIndex);
    this._tabPickedIndex = index;
  }

  private void UpdateUI()
  {
    this.ShowHeroContent(0);
  }

  private void ShowHeroContent(int heroTitlePosition = 0)
  {
    this._itemPool.Initialize(this._heroItemPrefab, this._heroItemRoot);
    NGUITools.SetActive(this._heroInfoRoot, true);
    NGUITools.SetActive(this._heroFragmentInfoRoot, false);
    this.ClearAllData();
    List<ParliamentHeroInfo> parliamentHeroInfoList1 = ConfigManager.inst.DB_ParliamentHero.GetParliamentHeroInfoList();
    List<ParliamentHeroInfo> parliamentHeroInfoList2 = heroTitlePosition > 0 ? parliamentHeroInfoList1.FindAll((Predicate<ParliamentHeroInfo>) (x => x.ParliamentPosition == heroTitlePosition)) : parliamentHeroInfoList1;
    List<LegendCardData> cardDataListByUid = DBManager.inst.DB_LegendCard.GetLegendCardDataListByUid(PlayerData.inst.uid);
    if (parliamentHeroInfoList2 != null)
    {
      for (int key = 0; key < parliamentHeroInfoList2.Count; ++key)
      {
        ParliamentHeroInfo heroInfo = parliamentHeroInfoList2[key];
        LegendCardData legendCardData = cardDataListByUid == null ? (LegendCardData) null : cardDataListByUid.Find((Predicate<LegendCardData>) (x => x.LegendId == heroInfo.internalId));
        if (legendCardData != null)
        {
          HeroInventorySlot itemRenderer = this.CreateItemRenderer(legendCardData);
          this._itemDict.Add(key, itemRenderer);
        }
        else if (heroInfo.ChipCount > 0)
        {
          HeroInventorySlot itemRenderer = this.CreateItemRenderer(heroInfo);
          this._itemDict.Add(key, itemRenderer);
        }
      }
    }
    this._noHeroHintText.text = heroTitlePosition > 0 ? Utils.XLAT("hero_no_heroes_type_description") : Utils.XLAT("hero_no_heroes_description");
    NGUITools.SetActive(this._noHeroHintText.transform.parent.gameObject, this._itemDict.Count <= 0);
    this.Reposition(true);
  }

  private void ShowHeroFragment()
  {
    this._itemPool.Initialize(this._heroFragmentPrefab, this._heroFragmentRoot);
    NGUITools.SetActive(this._heroInfoRoot, false);
    NGUITools.SetActive(this._heroFragmentInfoRoot, true);
    NGUITools.SetActive(this._leftHeroFragmentRoot, false);
    this.ClearAllData();
    List<ParliamentHeroInfo> parliamentHeroInfoList = ConfigManager.inst.DB_ParliamentHero.GetParliamentHeroInfoList();
    if (parliamentHeroInfoList != null && parliamentHeroInfoList.Count > 0)
    {
      parliamentHeroInfoList.Sort((Comparison<ParliamentHeroInfo>) ((a, b) =>
      {
        if (b.quality.CompareTo(a.quality) != 0)
          return b.quality.CompareTo(a.quality);
        return a.internalId.CompareTo(b.internalId);
      }));
      bool flag = false;
      for (int key = 0; key < parliamentHeroInfoList.Count; ++key)
      {
        if (parliamentHeroInfoList[key].ChipCount > 0)
        {
          HeroInventoryFragmentSlot fragmentItemRenderer = this.CreateFragmentItemRenderer(parliamentHeroInfoList[key]);
          this._itemFragmentDict.Add(key, fragmentItemRenderer);
          if (!flag)
            this.ShowFirstFragment(parliamentHeroInfoList[key]);
          flag = true;
        }
      }
    }
    NGUITools.SetActive(this._noHeroFragmentHintText.transform.parent.gameObject, this._itemFragmentDict.Count <= 0);
    this.Reposition(false);
  }

  private void ClearAllData()
  {
    this.ClearData(true);
    this.ClearData(false);
  }

  private void ClearData(bool isHeroCard = true)
  {
    if (isHeroCard)
    {
      using (Dictionary<int, HeroInventorySlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameObject gameObject = enumerator.Current.Value.gameObject;
          gameObject.SetActive(false);
          this._itemPool.Release(gameObject);
        }
      }
      this._itemDict.Clear();
    }
    else
    {
      using (Dictionary<int, HeroInventoryFragmentSlot>.Enumerator enumerator = this._itemFragmentDict.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, HeroInventoryFragmentSlot> current = enumerator.Current;
          current.Value.OnFragmentPressed -= new System.Action<ParliamentHeroInfo>(this.OnFragmentPressed);
          GameObject gameObject = current.Value.gameObject;
          gameObject.SetActive(false);
          this._itemPool.Release(gameObject);
        }
      }
      this._itemFragmentDict.Clear();
    }
    this._itemPool.Clear();
  }

  private void Reposition(bool isHeroCard = true)
  {
    if (isHeroCard)
    {
      this._heroTable.repositionNow = true;
      this._heroTable.Reposition();
      this._heroScrollView.ResetPosition();
      this._heroScrollView.gameObject.SetActive(false);
      this._heroScrollView.gameObject.SetActive(true);
    }
    else
    {
      this._heroFragmentTable.repositionNow = true;
      this._heroFragmentTable.Reposition();
      this._heroFragmentScrollView.ResetPosition();
      this._heroFragmentScrollView.gameObject.SetActive(false);
      this._heroFragmentScrollView.gameObject.SetActive(true);
    }
  }

  private HeroInventorySlot CreateItemRenderer(LegendCardData legendCardData)
  {
    GameObject gameObject = this._itemPool.AddChild(this._heroTable.gameObject);
    gameObject.SetActive(true);
    HeroInventorySlot component = gameObject.GetComponent<HeroInventorySlot>();
    component.SetData(legendCardData);
    return component;
  }

  private HeroInventorySlot CreateItemRenderer(ParliamentHeroInfo heroInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this._heroTable.gameObject);
    gameObject.SetActive(true);
    HeroInventorySlot component = gameObject.GetComponent<HeroInventorySlot>();
    component.SetData(heroInfo);
    return component;
  }

  private HeroInventoryFragmentSlot CreateFragmentItemRenderer(ParliamentHeroInfo heroInfo)
  {
    GameObject gameObject = this._itemPool.AddChild(this._heroFragmentTable.gameObject);
    gameObject.SetActive(true);
    HeroInventoryFragmentSlot component = gameObject.GetComponent<HeroInventoryFragmentSlot>();
    component.SetData(heroInfo);
    component.OnFragmentPressed += new System.Action<ParliamentHeroInfo>(this.OnFragmentPressed);
    return component;
  }

  private void ShowFirstFragment(ParliamentHeroInfo heroInfo)
  {
    if (heroInfo == null)
      return;
    this.OnFragmentPressed(heroInfo);
  }

  private void OnFragmentPressed(ParliamentHeroInfo heroInfo)
  {
    using (Dictionary<int, HeroInventoryFragmentSlot>.Enumerator enumerator = this._itemFragmentDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.RefreshUI(heroInfo);
    }
    if (heroInfo == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(heroInfo.chipItem);
    if (itemStaticInfo != null)
    {
      this._leftHeroFragmentName.text = itemStaticInfo.LocName;
      this._leftHeroFragmentName.color = heroInfo.QualityColor;
      this._leftHeroFragmentDesc.text = itemStaticInfo.LocDescription;
    }
    this._leftHeroFragmentItem.SetFragmentData(heroInfo, false);
    NGUITools.SetActive(this._leftHeroFragmentRoot, true);
  }

  private void OnLegendCardDataUpdated(LegendCardData legendCardData)
  {
    this.HandleTabSelected(this._tabBar.SelectedIndex);
  }

  private void AddEventHandler()
  {
    this._tabBar.OnSelectedHandler += new System.Action<int>(this.HandleTabSelected);
    DBManager.inst.DB_LegendCard.onDataUpdate += new System.Action<LegendCardData>(this.OnLegendCardDataUpdated);
  }

  private void RemoveEventHandler()
  {
    this._tabBar.OnSelectedHandler -= new System.Action<int>(this.HandleTabSelected);
    DBManager.inst.DB_LegendCard.onDataUpdate -= new System.Action<LegendCardData>(this.OnLegendCardDataUpdated);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool topShowSummonableHero;
  }
}
