﻿// Decompiled with JetBrains decompiler
// Type: CustomIconLoader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CustomIconLoader : MonoBehaviour
{
  protected Queue<CustomIconLoadRequest> m_allLoadRequest = new Queue<CustomIconLoadRequest>();
  protected List<CustomIconLoadRequest> m_allCompleteRequest = new List<CustomIconLoadRequest>();
  protected Dictionary<string, Texture> m_allTexture = new Dictionary<string, Texture>();
  protected Dictionary<string, int> m_allTextureRefCounter = new Dictionary<string, int>();
  protected CustomIconCache m_iconCache = new CustomIconCache();
  protected List<CustomIconLoadRequest> m_allDelayedLoadRequest = new List<CustomIconLoadRequest>();
  private const float TIME_OUT_TIME = 10f;
  protected static CustomIconLoader ms_instance;
  protected bool m_loading;

  public static CustomIconLoader Instance
  {
    get
    {
      if ((Object) CustomIconLoader.ms_instance == (Object) null)
      {
        GameObject gameObject = new GameObject("Instance_CustomIconLoader");
        Object.DontDestroyOnLoad((Object) gameObject);
        CustomIconLoader.ms_instance = gameObject.AddComponent<CustomIconLoader>();
      }
      return CustomIconLoader.ms_instance;
    }
  }

  protected Texture TryGetTexture(string url)
  {
    Texture texture = (Texture) null;
    this.m_allTexture.TryGetValue(url, out texture);
    return texture;
  }

  protected void AddTextureRef(string url, Texture texture)
  {
    if (!this.m_allTexture.ContainsKey(url))
      this.m_allTexture.Add(url, texture);
    if (!this.m_allTextureRefCounter.ContainsKey(url))
      this.m_allTextureRefCounter.Add(url, 0);
    Dictionary<string, int> textureRefCounter;
    string index;
    (textureRefCounter = this.m_allTextureRefCounter)[index = url] = textureRefCounter[index] + 1;
  }

  protected void RemoveTextureRef(string url)
  {
    if (!this.m_allTextureRefCounter.ContainsKey(url))
      this.m_allTextureRefCounter.Add(url, 0);
    Dictionary<string, int> textureRefCounter;
    string index;
    (textureRefCounter = this.m_allTextureRefCounter)[index = url] = textureRefCounter[index] - 1;
    if (this.m_allTextureRefCounter[url] > 0 || !this.m_allTexture.ContainsKey(url))
      return;
    Texture texture = this.m_allTexture[url];
    if ((bool) ((Object) texture))
      Object.Destroy((Object) texture);
    this.m_allTexture.Remove(url);
  }

  public void requestCustomIcon(Material targetMaterial, string localPath, string remoteIcon, bool logRequestInfo = false)
  {
    CustomIconLoadRequest customIconLoadRequest = new CustomIconLoadRequest(targetMaterial, localPath, remoteIcon, !this.m_iconCache.isIconCached(remoteIcon));
    if (logRequestInfo)
      customIconLoadRequest.DebugOutput();
    using (Queue<CustomIconLoadRequest>.Enumerator enumerator = this.m_allLoadRequest.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CustomIconLoadRequest current = enumerator.Current;
        if ((bool) ((Object) current.TargetMaterial) && (Object) current.TargetMaterial == (Object) targetMaterial)
          current.RequestCanceled = true;
      }
    }
    if (!(bool) ((Object) targetMaterial) || string.IsNullOrEmpty(remoteIcon))
      return;
    this.m_allLoadRequest.Enqueue(customIconLoadRequest);
  }

  public void requestCustomIcon(UITexture targetUITexture, string localPath, string remoteIcon, bool logRequestInfo = false)
  {
    CustomIconLoadRequest customIconLoadRequest = new CustomIconLoadRequest(targetUITexture, localPath, remoteIcon, !this.m_iconCache.isIconCached(remoteIcon));
    if (logRequestInfo)
      customIconLoadRequest.DebugOutput();
    using (Queue<CustomIconLoadRequest>.Enumerator enumerator = this.m_allLoadRequest.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CustomIconLoadRequest current = enumerator.Current;
        if ((bool) ((Object) current.TargetUITexture) && (Object) current.TargetUITexture == (Object) targetUITexture)
          current.RequestCanceled = true;
      }
    }
    if (!(bool) ((Object) targetUITexture) || string.IsNullOrEmpty(remoteIcon))
      return;
    this.m_allLoadRequest.Enqueue(customIconLoadRequest);
  }

  private void Update()
  {
    this.m_allDelayedLoadRequest.Clear();
    while (this.m_allLoadRequest.Count > 0)
    {
      CustomIconLoadRequest customIconLoadRequest = this.m_allLoadRequest.Dequeue();
      if (customIconLoadRequest.isRequestValid())
      {
        if (customIconLoadRequest.LocalIconSetted)
        {
          Texture texture1 = this.TryGetTexture(customIconLoadRequest.Url);
          if ((bool) ((Object) texture1))
          {
            customIconLoadRequest.texture = texture1;
            this.AddTextureRef(customIconLoadRequest.Url, texture1);
            this.m_allCompleteRequest.Add(customIconLoadRequest);
          }
          else
          {
            Texture texture2 = this.m_iconCache.tryGet(customIconLoadRequest.Url);
            if ((bool) ((Object) texture2))
            {
              customIconLoadRequest.texture = texture2;
              this.AddTextureRef(customIconLoadRequest.Url, texture2);
              this.m_allCompleteRequest.Add(customIconLoadRequest);
            }
            else
              this.m_allDelayedLoadRequest.Add(customIconLoadRequest);
          }
        }
        else
          this.m_allDelayedLoadRequest.Add(customIconLoadRequest);
      }
    }
    using (List<CustomIconLoadRequest>.Enumerator enumerator = this.m_allDelayedLoadRequest.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_allLoadRequest.Enqueue(enumerator.Current);
    }
    this.m_allDelayedLoadRequest.Clear();
    while (!this.m_loading && this.m_allLoadRequest.Count > 0)
    {
      CustomIconLoadRequest loadRequest = this.m_allLoadRequest.Dequeue();
      if (loadRequest.isRequestValid())
      {
        if (loadRequest.LocalIconSetted)
          this.StartCoroutine(this.loadCoroutine(loadRequest));
        else
          this.m_allDelayedLoadRequest.Add(loadRequest);
      }
    }
    using (List<CustomIconLoadRequest>.Enumerator enumerator = this.m_allDelayedLoadRequest.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.m_allLoadRequest.Enqueue(enumerator.Current);
    }
    this.m_allDelayedLoadRequest.Clear();
    this.tryReleaseNoRefTexture();
  }

  [DebuggerHidden]
  private IEnumerator loadCoroutine(CustomIconLoadRequest loadRequest)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CustomIconLoader.\u003CloadCoroutine\u003Ec__Iterator6()
    {
      loadRequest = loadRequest,
      \u003C\u0024\u003EloadRequest = loadRequest,
      \u003C\u003Ef__this = this
    };
  }

  protected void tryReleaseNoRefTexture()
  {
    for (int index = this.m_allCompleteRequest.Count - 1; index >= 0; --index)
    {
      CustomIconLoadRequest customIconLoadRequest = this.m_allCompleteRequest[index];
      if (!customIconLoadRequest.isRequestValid())
      {
        this.RemoveTextureRef(customIconLoadRequest.Url);
        this.m_allCompleteRequest.RemoveAt(index);
      }
    }
  }
}
