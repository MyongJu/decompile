﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressBenefitRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceFortressBenefitRenderer : MonoBehaviour
{
  public UILabel m_Title;
  public UILabel m_Content;
  public UITexture m_Icon;

  public void SetData(string title, string content, string image)
  {
    this.m_Title.text = title;
    this.m_Content.text = content;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, image, (System.Action<bool>) null, true, false, string.Empty);
  }
}
