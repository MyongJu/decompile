﻿// Decompiled with JetBrains decompiler
// Type: AllianceStoreHistoryPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceStoreHistoryPopup : Popup
{
  private List<AllianceStoreHistoryRenderer> m_ItemList = new List<AllianceStoreHistoryRenderer>();
  public GameObject m_ItemPrefab;
  public GameObject m_NoRecord;
  public UILabel m_Title;
  public UITable m_Table;
  public UIScrollView m_ScrollView;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceStoreHistoryPopup.Parameter parameter = orgParam as AllianceStoreHistoryPopup.Parameter;
    ArrayList payload = parameter.payload as ArrayList;
    this.m_NoRecord.SetActive(payload.Count == 0);
    this.m_Title.text = parameter.type != AllianceStoreType.Fund ? Utils.XLAT("alliance_store_honor_history_title") : Utils.XLAT("alliance_store_fund_history_title");
    for (int index = 0; index < payload.Count; ++index)
    {
      Hashtable itemData = payload[index] as Hashtable;
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Table.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      AllianceStoreHistoryRenderer component = gameObject.GetComponent<AllianceStoreHistoryRenderer>();
      component.SetData(parameter.type, itemData);
      this.m_ItemList.Add(component);
    }
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      try
      {
        this.m_Table.Reposition();
        this.m_ScrollView.ResetPosition();
      }
      catch
      {
      }
    }));
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public object payload;
    public AllianceStoreType type;
  }
}
