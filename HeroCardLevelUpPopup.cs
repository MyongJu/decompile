﻿// Decompiled with JetBrains decompiler
// Type: HeroCardLevelUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroCardLevelUpPopup : Popup
{
  protected List<HeroUseExpItemComponent> expItemComponents = new List<HeroUseExpItemComponent>();
  [SerializeField]
  private HeroLevelUpInfoComponent _HeroLevelUpInfo;
  [SerializeField]
  private HeroUseExpItemComponent _Template;
  [SerializeField]
  private UIGrid _Grid;
  [SerializeField]
  private GameObject _NoItemsHintGo;
  [SerializeField]
  private UIButton _ConfirmButton;
  [SerializeField]
  private ParticleSystem _Particle;
  protected LegendCardData legendCardData;
  protected bool isDirty;
  protected long overflowExp;
  protected bool isReachMax;

  private void InitParams(HeroCardLevelUpPopup.Parameter param)
  {
    if (param == null)
      D.error((object) "HeroCardLevelUpPopup InitParams error, param is null");
    else
      this.legendCardData = param.legendCardData;
  }

  private void UpdateUI()
  {
    this._HeroLevelUpInfo.FeedData((IComponentData) new HeroLevelUpInfoComponentData(this.legendCardData));
    List<ItemStaticInfo> itemListByType = ConfigManager.inst.DB_Items.GetItemListByType(ItemBag.ItemType.parliament_hero_exp);
    itemListByType.Sort((Comparison<ItemStaticInfo>) ((x, y) => x.Priority.CompareTo(y.Priority)));
    UIUtils.CleanGrid(this._Grid);
    this.expItemComponents.Clear();
    bool state = true;
    int index = 0;
    for (int count = itemListByType.Count; index < count; ++index)
    {
      ItemStaticInfo itemInfo = itemListByType[index];
      if (DBManager.inst.DB_Item.GetQuantity(itemInfo.internalId) > 0)
      {
        GameObject go = NGUITools.AddChild(this._Grid.gameObject, this._Template.gameObject);
        NGUITools.SetActive(go, true);
        HeroUseExpItemComponent component = go.GetComponent<HeroUseExpItemComponent>();
        this.expItemComponents.Add(component);
        component.FeedData((IComponentData) new HeroUseExpItemComponentData(itemInfo));
        component.NotifyValueChanged += new System.Action<int>(this.OnSelectedItemCountChanged);
        component.reachMaxCounter = new HeroUseExpItemComponent.GetReachMaxItemCount(this.GetReachMaxItemCount);
        state = false;
      }
    }
    NGUITools.SetActive(this._NoItemsHintGo, state);
    this.UpdateConfirmButtonState(0L);
    this._Grid.Reposition();
  }

  private void UpdateConfirmButtonState(long OfferedExp = 0)
  {
    this._ConfirmButton.isEnabled = OfferedExp > 0L;
  }

  private int GetReachMaxItemCount(ItemStaticInfo selectedItemInfo)
  {
    if (selectedItemInfo == null)
      return 0;
    long num1 = 0;
    using (List<HeroUseExpItemComponent>.Enumerator enumerator = this.expItemComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroUseExpItemComponent current = enumerator.Current;
        ItemStaticInfo itemInfo = current.ItemInfo;
        if (itemInfo.internalId != selectedItemInfo.internalId)
        {
          int selectedCount = current.SelectedCount;
          long num2 = (long) itemInfo.Value * (long) selectedCount;
          num1 += num2;
        }
      }
    }
    int heroMaxLevel = HeroCardUtils.GetHeroMaxLevel(this.legendCardData.LegendId);
    long num3 = 0;
    for (int level = 1; level < heroMaxLevel; ++level)
      num3 += HeroCardUtils.GetRequiredExpToNextLevel(this.legendCardData.LegendId, level);
    return Mathf.Max(0, Mathf.CeilToInt((float) (num3 - num1 - this.legendCardData.XP) / selectedItemInfo.Value));
  }

  private void OnSelectedItemCountChanged(int count)
  {
    long num1 = 0;
    using (List<HeroUseExpItemComponent>.Enumerator enumerator = this.expItemComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroUseExpItemComponent current = enumerator.Current;
        long num2 = (long) current.ItemInfo.Value * (long) current.SelectedCount;
        num1 += num2;
      }
    }
    this.isReachMax = false;
    this.overflowExp = 0L;
    this._HeroLevelUpInfo.AddExp(num1, ref this.isReachMax, ref this.overflowExp);
    this.UpdateConfirmButtonState(num1);
  }

  private void Update()
  {
    if (!this.isDirty)
      return;
    this.isDirty = false;
    this.UpdateUI();
  }

  public void OnHeroCardDataChanged(LegendCardData data)
  {
    this.isDirty = true;
  }

  public void OnExpItemUpdate(int itemId)
  {
    this.isDirty = true;
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnConfirmButtonClicked()
  {
    if (this.isReachMax && this.overflowExp > 0L)
      UIManager.inst.OpenPopup("MessageBoxWith2ButtonsPopup", (Popup.PopupParameter) new MessageBoxWith2ButtonsPopup.Parameter()
      {
        titleString = Utils.XLAT("account_warning_title"),
        contentString = ScriptLocalization.GetWithPara("hero_xp_level_upgrade_max_description", new Dictionary<string, string>()
        {
          {
            "0",
            this.overflowExp.ToString()
          }
        }, true),
        leftLabelString = Utils.XLAT("id_uppercase_confirm"),
        rightLabelString = Utils.XLAT("id_uppercase_cancel"),
        onLeftBtnClick = (System.Action) (() => this.UseItems())
      });
    else
      this.UseItems();
  }

  private void UseItems()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "uid"] = (object) PlayerData.inst.uid;
    postData[(object) "legend_id"] = (object) this.legendCardData.LegendId;
    Hashtable hashtable = new Hashtable();
    postData[(object) "items"] = (object) hashtable;
    bool flag = false;
    using (List<HeroUseExpItemComponent>.Enumerator enumerator = this.expItemComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HeroUseExpItemComponent current = enumerator.Current;
        ItemStaticInfo itemInfo = current.ItemInfo;
        int selectedCount = current.SelectedCount;
        if (itemInfo != null)
        {
          hashtable[(object) itemInfo.internalId.ToString()] = (object) selectedCount;
          flag = true;
        }
      }
    }
    if (!flag)
      return;
    this.PlayExpEffect();
    MessageHub.inst.GetPortByAction("Item:useMultiConsumableItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) => {}), true);
  }

  private void PlayExpEffect()
  {
    if ((UnityEngine.Object) this._Particle == (UnityEngine.Object) null)
      return;
    NGUITools.SetActive(this._Particle.gameObject, true);
    this._Particle.Clear();
    this._Particle.Play(true);
    AudioManager.Instance.StopAndPlaySound("sfx_city_casino_turn");
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.InitParams(orgParam as HeroCardLevelUpPopup.Parameter);
    DBManager.inst.DB_LegendCard.onDataChanged += new System.Action<LegendCardData>(this.OnHeroCardDataChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnExpItemUpdate);
  }

  private void Dispose()
  {
    DBManager.inst.DB_LegendCard.onDataChanged -= new System.Action<LegendCardData>(this.OnHeroCardDataChanged);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnExpItemUpdate);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public LegendCardData legendCardData;
  }
}
