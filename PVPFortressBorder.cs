﻿// Decompiled with JetBrains decompiler
// Type: PVPFortressBorder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPFortressBorder
{
  private ZoneBorderMesh m_StaticBorderMesh;
  private ZoneBorderMesh m_DynamicBorderMesh;
  private ZoneBorderData m_ZoneBorderData;

  public ZoneBorderData ZoneBorderData
  {
    get
    {
      return this.m_ZoneBorderData;
    }
  }

  public void Dispose()
  {
    this.m_ZoneBorderData = (ZoneBorderData) null;
    this.DestroyStaticZoneBorderMesh();
    this.DestroyDynamicZoneBorderMesh();
  }

  private void DestroyStaticZoneBorderMesh()
  {
    if (!(bool) ((UnityEngine.Object) this.m_StaticBorderMesh))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_StaticBorderMesh.gameObject);
    this.m_StaticBorderMesh = (ZoneBorderMesh) null;
  }

  private void DestroyDynamicZoneBorderMesh()
  {
    if (!(bool) ((UnityEngine.Object) this.m_DynamicBorderMesh))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_DynamicBorderMesh.gameObject);
    this.m_DynamicBorderMesh = (ZoneBorderMesh) null;
  }

  public void BuildZoneBorderMesh(ZoneBorderData borderData)
  {
    this.m_ZoneBorderData = borderData;
    this.InitStaticMeshOnce();
    this.m_StaticBorderMesh.BuildMesh(this.m_ZoneBorderData);
    this.UpdateZoneBorderColor();
  }

  private void InitStaticMeshOnce()
  {
    if ((bool) ((UnityEngine.Object) this.m_StaticBorderMesh))
      return;
    GameObject gameObject = Utils.DuplicateGOB(AssetManager.Instance.HandyLoad("Prefab/Kingdom/FORTRESS_BORDER", (System.Type) null) as GameObject, PVPSystem.Instance.Map.Fortress);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    this.m_StaticBorderMesh = gameObject.GetComponent<ZoneBorderMesh>();
  }

  public void UpdateZoneBorderColor()
  {
    Color white = Color.white;
    long allianceId = PlayerData.inst.allianceId;
    Color color;
    if (allianceId == 0L)
    {
      this.DestroyDynamicZoneBorderMesh();
      color = Color.red;
    }
    else if (allianceId == this.m_ZoneBorderData.AllianceID)
    {
      this.BuildDynamicMesh(this.m_ZoneBorderData);
      color = Color.green;
    }
    else
    {
      this.DestroyDynamicZoneBorderMesh();
      color = Color.red;
    }
    if ((bool) ((UnityEngine.Object) this.m_StaticBorderMesh))
      this.m_StaticBorderMesh.SetColor(color);
    if (!(bool) ((UnityEngine.Object) this.m_DynamicBorderMesh))
      return;
    this.m_DynamicBorderMesh.SetColor(color);
  }

  private void BuildDynamicMesh(ZoneBorderData zoneBorderData)
  {
    if (!(bool) ((UnityEngine.Object) this.m_DynamicBorderMesh))
    {
      GameObject gameObject = Utils.DuplicateGOB(AssetManager.Instance.HandyLoad("Prefab/Kingdom/FORTRESS_BORDER_DYNAMIC", (System.Type) null) as GameObject, PVPSystem.Instance.Map.Fortress);
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
      this.m_DynamicBorderMesh = gameObject.GetComponent<ZoneBorderMesh>();
    }
    this.m_DynamicBorderMesh.BuildMesh(this.m_ZoneBorderData);
  }
}
