﻿// Decompiled with JetBrains decompiler
// Type: PitExploreSignUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class PitExploreSignUpPopup : Popup
{
  private Dictionary<int, PitExploreSignUpSlot> _itemDict = new Dictionary<int, PitExploreSignUpSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private Dictionary<string, string> _tempParam = new Dictionary<string, string>();
  [SerializeField]
  private UILabel _nextSessionStartTime;
  [SerializeField]
  private UILabel _nextSessionSignUpMembers;
  [SerializeField]
  private UILabel _nextSessionAllianceMembers;
  [SerializeField]
  private UILabel _signupNeedGoldAmount;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;
  private int _signupCount;
  private List<long> _signupAllianceMembers;
  private Color _signupFeeTextColor;
  private System.Action<bool, object> _signedCallback;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    PitExploreSignUpPopup.Parameter parameter = orgParam as PitExploreSignUpPopup.Parameter;
    if (parameter != null)
    {
      this._signupCount = parameter.signupCount;
      this._signupAllianceMembers = parameter.signupAllianceMembers;
      this._signedCallback = parameter.signedCallback;
    }
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this._signupFeeTextColor = this._signupNeedGoldAmount.color;
    this.AddEventHandler();
    this.OnSecondEvent(0);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    this.RemoveEventHandler();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnSignUpBtnPressed()
  {
    GameConfigInfo data1 = ConfigManager.inst.DB_GameConfig.GetData("abyss_sign_up_level_min");
    if (data1 != null && data1.ValueInt > PlayerData.inst.CityData.mStronghold.mLevel)
      UIManager.inst.toast.Show(ScriptLocalization.GetWithPara("stronghold_level_too_low_num", new Dictionary<string, string>()
      {
        {
          "0",
          data1.ValueInt.ToString()
        }
      }, true), (System.Action) null, 4f, true);
    else
      MessageHub.inst.GetPortByAction("Abyss:register").SendRequest(new Hashtable()
      {
        {
          (object) "round",
          (object) (PitExplorePayload.Instance.CurrentActivityRound + 1)
        }
      }, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        PitExplorePayload.Instance.Decode(data as Hashtable);
        if (this._signedCallback != null)
          this._signedCallback(ret, data);
        this.OnCloseBtnPressed();
      }), true);
  }

  private void UpdateUI()
  {
    this.UpdateMainUI();
    this.UpdateLeftUI();
    this.UpdateSignupButtonStatus();
  }

  private void UpdateSignupButtonStatus()
  {
    UIButton componentInParent = this._signupNeedGoldAmount.transform.GetComponentInParent<UIButton>();
    if (!(bool) ((UnityEngine.Object) componentInParent))
      return;
    componentInParent.isEnabled = PitExplorePayload.Instance.CanSignup;
  }

  private void UpdateMainUI()
  {
    this._signupNeedGoldAmount.text = Utils.FormatThousands(PitExplorePayload.Instance.NeedGoldAmount2Signup.ToString());
    if (PlayerData.inst.userData.currency.gold < (long) PitExplorePayload.Instance.NeedGoldAmount2Signup)
      this._signupNeedGoldAmount.color = Color.red;
    else
      this._signupNeedGoldAmount.color = this._signupFeeTextColor;
    string timeHhmm1 = Utils.GetTimeHHMM(Utils.FormatTimeForSystemSetting((long) (PitExplorePayload.Instance.NextRoundStartTime + NetServerTime.inst.ServerTimestamp)));
    string timeHhmm2 = Utils.GetTimeHHMM(Utils.FormatTimeForSystemSetting((long) (PitExplorePayload.Instance.NextRoundStartTime + PitExplorePayload.Instance.ActivityData.Duration + NetServerTime.inst.ServerTimestamp)));
    this._tempParam.Clear();
    this._tempParam.Add("0", timeHhmm1 + " - " + timeHhmm2);
    this._nextSessionStartTime.text = ScriptLocalization.GetWithPara("event_fire_kingdom_next_round", this._tempParam, true);
    this._tempParam.Clear();
    this._tempParam.Add("0", string.Format("{0}/{1}", (object) this._signupCount.ToString(), (object) PitExplorePayload.Instance.SignupMaxMembers.ToString()));
    this._nextSessionSignUpMembers.text = ScriptLocalization.GetWithPara("event_fire_kingdom_next_round_participants", this._tempParam, true);
    this._tempParam.Clear();
    this._tempParam.Add("0", this._signupAllianceMembers == null ? "0" : this._signupAllianceMembers.Count.ToString());
    this._nextSessionAllianceMembers.text = ScriptLocalization.GetWithPara("event_fire_kingdom_next_round_alliances_num", this._tempParam, true);
  }

  private void UpdateLeftUI()
  {
    this.ClearData();
    if (this._signupAllianceMembers != null && this._signupAllianceMembers.Count > 0)
    {
      long id = this._signupAllianceMembers.Find((Predicate<long>) (x => x != PlayerData.inst.uid));
      if (id > 0L && DBManager.inst.DB_User.Get(id) == null)
      {
        MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendLoader(new Hashtable()
        {
          {
            (object) "alliance_id",
            (object) PlayerData.inst.allianceId
          }
        }, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          this.CreateItemRenderer();
          this.Reposition();
        }), true, false);
        return;
      }
      this.CreateItemRenderer();
    }
    this.Reposition();
  }

  private void CreateItemRenderer()
  {
    for (int key = 0; key < this._signupAllianceMembers.Count; ++key)
    {
      UserData userData = DBManager.inst.DB_User.Get(this._signupAllianceMembers[key]);
      if (userData != null && PlayerData.inst.allianceData != null)
      {
        PitExploreSignUpSlot itemRenderer = this.CreateItemRenderer(string.Format("[{0}]{1}", (object) PlayerData.inst.allianceData.allianceAcronym, (object) userData.userName));
        this._itemDict.Add(key, itemRenderer);
      }
    }
  }

  private void ClearData()
  {
    using (Dictionary<int, PitExploreSignUpSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
    this._scrollView.gameObject.SetActive(false);
    this._scrollView.gameObject.SetActive(true);
  }

  private PitExploreSignUpSlot CreateItemRenderer(string applicant)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    PitExploreSignUpSlot component = gameObject.GetComponent<PitExploreSignUpSlot>();
    component.SetData(applicant);
    return component;
  }

  private void OnSecondEvent(int time)
  {
    this.UpdateMainUI();
    this.UpdateSignupButtonStatus();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void RemoveEventHandler()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int signupCount;
    public List<long> signupAllianceMembers;
    public System.Action<bool, object> signedCallback;
  }
}
