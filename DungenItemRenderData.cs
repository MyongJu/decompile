﻿// Decompiled with JetBrains decompiler
// Type: DungenItemRenderData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class DungenItemRenderData : IComponentData
{
  public DragonKnightDungeonData.BagInfo bagInfo;
  public int index;
  public bool locked;

  public DungenItemRenderData(DragonKnightDungeonData.BagInfo bagInfo, int index, bool locked)
  {
    this.bagInfo = bagInfo;
    this.index = index;
    this.locked = locked;
  }
}
