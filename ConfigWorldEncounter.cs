﻿// Decompiled with JetBrains decompiler
// Type: ConfigWorldEncounter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigWorldEncounter
{
  private static Dictionary<string, string> _type2path = new Dictionary<string, string>();
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, WorldEncounterData> datas;
  private Dictionary<long, WorldEncounterData> dicByUniqueId;

  public static string GetMonsterPrefabPath(string type)
  {
    if (!ConfigWorldEncounter._type2path.ContainsKey(type))
      ConfigWorldEncounter._type2path[type] = "Prefab/Monster/" + type;
    return ConfigWorldEncounter._type2path[type];
  }

  public void BuildDB(object res)
  {
    this.parse.Parse<WorldEncounterData, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public WorldEncounterData GetData(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (WorldEncounterData) null;
  }

  public WorldEncounterData GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (WorldEncounterData) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, WorldEncounterData>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, WorldEncounterData>) null;
  }
}
