﻿// Decompiled with JetBrains decompiler
// Type: AesCoder
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Security.Cryptography;

public class AesCoder
{
  private static Aes _aes;
  private static ICryptoTransform _encryptor;
  private static ICryptoTransform _decryptor;

  public static byte ConvertHex2Byte(string code, int offset)
  {
    return (byte) (Convert.ToInt32(code[offset].ToString(), 16) << 4 | Convert.ToInt32(code[offset + 1].ToString(), 16));
  }

  public static void Initialize()
  {
    string code1 = "fdf5c0b0abcad201cc6da19341aca2f5";
    string code2 = "11e8a9fed7984633af90048a6dacd752";
    byte[] numArray1 = new byte[16];
    byte[] numArray2 = new byte[16];
    int offset = 0;
    while (offset < code1.Length)
    {
      numArray1[offset / 2] = AesCoder.ConvertHex2Byte(code1, offset);
      numArray2[offset / 2] = AesCoder.ConvertHex2Byte(code2, offset);
      offset += 2;
    }
    AesCoder._aes = Aes.Create();
    if (AesCoder._aes == null)
      AesCoder._aes = (Aes) new AesManaged();
    if (AesCoder._aes == null)
      throw new Exception("[Encryptor]Aes Create Fail.");
    AesCoder._aes.KeySize = 128;
    AesCoder._aes.BlockSize = 128;
    AesCoder._aes.Key = numArray1;
    AesCoder._aes.IV = numArray2;
    AesCoder._aes.Padding = PaddingMode.PKCS7;
    AesCoder._aes.Mode = CipherMode.ECB;
    AesCoder._encryptor = AesCoder._aes.CreateEncryptor();
    AesCoder._decryptor = AesCoder._aes.CreateDecryptor();
  }

  public static void Finalize()
  {
    AesCoder._encryptor = (ICryptoTransform) null;
    AesCoder._decryptor = (ICryptoTransform) null;
    if (AesCoder._aes == null)
      return;
    AesCoder._aes.Clear();
    AesCoder._aes = (Aes) null;
  }

  public static byte[] Encode(string plainText)
  {
    if (plainText == null || plainText.Length <= 0)
      throw new ArgumentNullException(nameof (plainText));
    using (MemoryStream memoryStream = new MemoryStream())
    {
      using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, AesCoder._encryptor, CryptoStreamMode.Write))
      {
        using (StreamWriter streamWriter = new StreamWriter((Stream) cryptoStream))
          streamWriter.Write(plainText);
        return memoryStream.ToArray();
      }
    }
  }

  public static string Decode(byte[] cipherText)
  {
    if (cipherText == null || cipherText.Length <= 0)
      throw new ArgumentNullException(nameof (cipherText));
    using (MemoryStream memoryStream = new MemoryStream(cipherText))
    {
      using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, AesCoder._decryptor, CryptoStreamMode.Read))
      {
        using (StreamReader streamReader = new StreamReader((Stream) cryptoStream))
          return streamReader.ReadToEnd();
      }
    }
  }

  public static byte[] Decode2Bytes(byte[] cipherData)
  {
    if (cipherData == null || cipherData.Length <= 0)
      throw new ArgumentNullException(nameof (cipherData));
    using (MemoryStream memoryStream = new MemoryStream())
    {
      using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, AesCoder._decryptor, CryptoStreamMode.Write))
      {
        cryptoStream.Write(cipherData, 0, cipherData.Length);
        cryptoStream.FlushFinalBlock();
        return memoryStream.ToArray();
      }
    }
  }
}
