﻿// Decompiled with JetBrains decompiler
// Type: AllianceManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class AllianceManager
{
  private AllianceManager.AllianceActiveRecommendationType _activeRecommendationType = AllianceManager.AllianceActiveRecommendationType.INVALID;
  private bool _joinedAllianceBefore = true;
  private static AllianceManager m_Instance;

  private AllianceManager()
  {
  }

  public event System.Action<object> onPushAcceptApply;

  public event System.Action<object> onPushTransferHighlord;

  public event System.Action<object> onPushEmpowerTempHighlord;

  public event System.Action<object> onPushJoin;

  public event System.Action<object> onPushLeave;

  public event System.Action<object> onPushDisban;

  public event System.Action<object> onPushSetRank;

  public event System.Action<object> onPushModify;

  public event System.Action<object> onPushLevelUpgrade;

  public event System.Action<object> onPushHelpRequest;

  public event System.Action<object> onPushHelpResponse;

  public event System.Action<object> onPushHelpComplete;

  public event System.Action<object> onPushRevokeTempHighlord;

  public event System.Action<object> onPushTempHighlordTimeup;

  public event System.Action<object> onPushDenyApplication;

  public event System.Action<object> onPushInvitation;

  public event System.Action<object> onPushRevokeInvitation;

  public event System.Action onJoin;

  public event System.Action onLeave;

  public event System.Action onHelpChanged;

  public event System.Action onInviteChanged;

  public AllianceManager.AllianceActiveRecommendationType ActiveRecommendationType
  {
    get
    {
      return this._activeRecommendationType;
    }
    set
    {
      this._activeRecommendationType = value;
    }
  }

  public bool CanShowAllianceJoinEntrance
  {
    get
    {
      return PlayerData.inst.allianceId <= 0L && !this._joinedAllianceBefore && this._activeRecommendationType == AllianceManager.AllianceActiveRecommendationType.ENTRY_HINT;
    }
  }

  public static AllianceManager Instance
  {
    get
    {
      if (AllianceManager.m_Instance == null)
        AllianceManager.m_Instance = new AllianceManager();
      return AllianceManager.m_Instance;
    }
  }

  public void Initialize()
  {
    MessageHub.inst.GetPortByAction("accept_apply_alliance").AddEvent(new System.Action<object>(this.OnPushAcceptApply));
    MessageHub.inst.GetPortByAction("transfer_highlord").AddEvent(new System.Action<object>(this.OnPushTransferHighlord));
    MessageHub.inst.GetPortByAction("empower_temp_highlord").AddEvent(new System.Action<object>(this.OnPushEmpowerTempHighlord));
    MessageHub.inst.GetPortByAction("join_alliance").AddEvent(new System.Action<object>(this.OnPushJoin));
    MessageHub.inst.GetPortByAction("leave_alliance").AddEvent(new System.Action<object>(this.OnPushLeave));
    MessageHub.inst.GetPortByAction("alliance_disband").AddEvent(new System.Action<object>(this.OnPushDisban));
    MessageHub.inst.GetPortByAction("alliance_set_rank").AddEvent(new System.Action<object>(this.OnPushSetRank));
    MessageHub.inst.GetPortByAction("alliance_modify").AddEvent(new System.Action<object>(this.OnPushModify));
    MessageHub.inst.GetPortByAction("alliance_level_upgrade").AddEvent(new System.Action<object>(this.OnPushLevelUpgrade));
    MessageHub.inst.GetPortByAction("alliance_help_request").AddEvent(new System.Action<object>(this.OnPushHelpRequest));
    MessageHub.inst.GetPortByAction("alliance_help_response").AddEvent(new System.Action<object>(this.OnPushHelpResponse));
    MessageHub.inst.GetPortByAction("alliance_help_complete").AddEvent(new System.Action<object>(this.OnPushHelpComplete));
    MessageHub.inst.GetPortByAction("revoke_temp_highlord").AddEvent(new System.Action<object>(this.OnPushRevokeTempHighlord));
    MessageHub.inst.GetPortByAction("temp_highlord_timeup").AddEvent(new System.Action<object>(this.OnPushTempHighlordTimeup));
    MessageHub.inst.GetPortByAction("deny_apply_alliance").AddEvent(new System.Action<object>(this.OnPushDenyApplication));
    MessageHub.inst.GetPortByAction("alliance_invite").AddEvent(new System.Action<object>(this.OnPushInvitation));
    MessageHub.inst.GetPortByAction("alliance_revoke_invite").AddEvent(new System.Action<object>(this.OnPushRevokeInvitation));
    DBManager.inst.DB_Alliance.onHelpRemoved += new System.Action(this.OnHelpChanged);
  }

  public void Dispose()
  {
    MessageHub.inst.GetPortByAction("accept_apply_alliance").RemoveEvent(new System.Action<object>(this.OnPushAcceptApply));
    MessageHub.inst.GetPortByAction("transfer_highlord").RemoveEvent(new System.Action<object>(this.OnPushTransferHighlord));
    MessageHub.inst.GetPortByAction("empower_temp_highlord").RemoveEvent(new System.Action<object>(this.OnPushEmpowerTempHighlord));
    MessageHub.inst.GetPortByAction("join_alliance").RemoveEvent(new System.Action<object>(this.OnPushJoin));
    MessageHub.inst.GetPortByAction("leave_alliance").RemoveEvent(new System.Action<object>(this.OnPushLeave));
    MessageHub.inst.GetPortByAction("alliance_disband").RemoveEvent(new System.Action<object>(this.OnPushDisban));
    MessageHub.inst.GetPortByAction("alliance_set_rank").RemoveEvent(new System.Action<object>(this.OnPushSetRank));
    MessageHub.inst.GetPortByAction("alliance_modify").RemoveEvent(new System.Action<object>(this.OnPushModify));
    MessageHub.inst.GetPortByAction("alliance_level_upgrade").RemoveEvent(new System.Action<object>(this.OnPushLevelUpgrade));
    MessageHub.inst.GetPortByAction("alliance_help_request").RemoveEvent(new System.Action<object>(this.OnPushHelpRequest));
    MessageHub.inst.GetPortByAction("alliance_help_response").RemoveEvent(new System.Action<object>(this.OnPushHelpResponse));
    MessageHub.inst.GetPortByAction("alliance_help_complete").RemoveEvent(new System.Action<object>(this.OnPushHelpComplete));
    MessageHub.inst.GetPortByAction("revoke_temp_highlord").RemoveEvent(new System.Action<object>(this.OnPushRevokeTempHighlord));
    MessageHub.inst.GetPortByAction("temp_highlord_timeup").RemoveEvent(new System.Action<object>(this.OnPushTempHighlordTimeup));
    MessageHub.inst.GetPortByAction("deny_apply_alliance").RemoveEvent(new System.Action<object>(this.OnPushDenyApplication));
    MessageHub.inst.GetPortByAction("alliance_invite").RemoveEvent(new System.Action<object>(this.OnPushInvitation));
    MessageHub.inst.GetPortByAction("alliance_revoke_invite").RemoveEvent(new System.Action<object>(this.OnPushRevokeInvitation));
    DBManager.inst.DB_Alliance.onHelpRemoved -= new System.Action(this.OnHelpChanged);
    this.onPushAcceptApply = (System.Action<object>) null;
    this.onPushTransferHighlord = (System.Action<object>) null;
    this.onPushEmpowerTempHighlord = (System.Action<object>) null;
    this.onPushJoin = (System.Action<object>) null;
    this.onPushLeave = (System.Action<object>) null;
    this.onPushDisban = (System.Action<object>) null;
    this.onPushSetRank = (System.Action<object>) null;
    this.onPushModify = (System.Action<object>) null;
    this.onPushLevelUpgrade = (System.Action<object>) null;
    this.onPushHelpRequest = (System.Action<object>) null;
    this.onPushHelpResponse = (System.Action<object>) null;
    this.onPushHelpComplete = (System.Action<object>) null;
    this.onPushRevokeTempHighlord = (System.Action<object>) null;
    this.onPushTempHighlordTimeup = (System.Action<object>) null;
    this.onPushDenyApplication = (System.Action<object>) null;
    this.onPushInvitation = (System.Action<object>) null;
    this.onPushRevokeInvitation = (System.Action<object>) null;
    this.onJoin = (System.Action) null;
    this.onLeave = (System.Action) null;
    this.onHelpChanged = (System.Action) null;
    this.onInviteChanged = (System.Action) null;
  }

  public void CreateAlliance(string name, string acronym, string message, string language, int symbol, bool isPrivate, string powerLimit, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:createAlliance").SendRequest(new Hashtable()
    {
      {
        (object) nameof (name),
        (object) name
      },
      {
        (object) nameof (acronym),
        (object) acronym
      },
      {
        (object) "symbol_code",
        (object) symbol
      },
      {
        (object) nameof (language),
        (object) language
      },
      {
        (object) "public_message",
        (object) message
      },
      {
        (object) "is_private",
        (object) (!isPrivate ? 0 : 1)
      },
      {
        (object) "power_limit",
        (object) powerLimit
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.OnJoin();
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void JoinAlliance(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:joinAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        PlayerData.inst.allianceId = allianceId;
        this.OnJoin();
        ChatMessageManager.SendJoinAllianceMessage();
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void DisbanAlliance(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:disbandAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.OnLeave();
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void LeaveAlliance(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:leaveAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.OnLeave();
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void AcceptInvitation(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:denyOrAcceptInviteByUser").SendRequest(new Hashtable()
    {
      {
        (object) "type",
        (object) "accept"
      },
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        PlayerData.inst.allianceId = allianceId;
        DBManager.inst.DB_AllianceInviteApply.RemoveByAllianceId(allianceId);
        this.OnJoin();
        this.OnInviteChanged();
        ChatMessageManager.SendJoinAllianceMessage();
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void DenyInvitation(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:denyOrAcceptInviteByUser").SendRequest(new Hashtable()
    {
      {
        (object) "type",
        (object) "deny"
      },
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        DBManager.inst.DB_AllianceInviteApply.RemoveByAllianceId(allianceId);
        this.OnInviteChanged();
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Apply(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:applyAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (PlayerData.inst.allianceId != 0L)
      {
        DBManager.inst.DB_AllianceInviteApply.RemoveByAllianceId(allianceId);
        this.OnJoin();
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void HelpAll(System.Action<bool, object> callback)
  {
    AllianceData alliance = PlayerData.inst.allianceData;
    if (alliance == null)
      return;
    alliance.helps.accessible = false;
    alliance.helps.Clear();
    MessageHub.inst.GetPortByAction("Alliance:helpMembers").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      alliance.helps.accessible = true;
      if (ret)
        this.OnHelpChanged();
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void RevokeApplication(long allianceId, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:revokeApplyByUser").SendRequest(new Hashtable()
    {
      {
        (object) "apply_uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "alliance_id",
        (object) allianceId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        DBManager.inst.DB_AllianceInviteApply.RemoveByAllianceId(allianceId);
        this.OnInviteChanged();
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void AcceptOrDenyApplication(long allianceId, long uid, bool accept, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:AcceptOrDenyApplyByAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "type",
        !accept ? (object) "deny" : (object) nameof (accept)
      },
      {
        (object) "alliance_id",
        (object) allianceId
      },
      {
        (object) "apply_uid",
        (object) uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
      {
        if (int.Parse((data as Hashtable)[(object) "errno"].ToString()) == 1004090)
          DBManager.inst.DB_AllianceInviteApply.Remove(allianceId, uid);
      }
      else
      {
        Hashtable hashtable = data as Hashtable;
        if (hashtable != null && hashtable.ContainsKey((object) "type") && (hashtable.ContainsKey((object) "username") && hashtable[(object) "type"].ToString() == nameof (accept)))
          ChatMessageManager.SendAcceptAllianceInviteMessage(hashtable[(object) "username"].ToString());
      }
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void PromoteOrDemote(long memberId, int title, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    postData[(object) "member_uid"] = (object) memberId;
    postData[(object) nameof (title)] = (object) title;
    MessageHub.inst.GetPortByAction("Alliance:setMemberRank").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.OnPushSetRank(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Kick(long memberUid, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    postData[(object) "kick_uid"] = (object) memberUid;
    MessageHub.inst.GetPortByAction("Alliance:kickUserFromAlliance").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void TransferLeadership(long uid, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "to_uid"] = (object) uid;
    MessageHub.inst.GetPortByAction("Alliance:transferHighlord").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void SendInvite(long uid, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:allianceSendInvite").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) "invite_uid",
        (object) uid
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ApplyComment(long alliance_id, string comment, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (alliance_id)] = (object) alliance_id;
    postData[(object) nameof (comment)] = (object) comment;
    MessageHub.inst.GetPortByAction("Alliance:applyComment").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void Customize(string name, string tag, int symbol, string language, string message, long powerLimit, bool isPrivate, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:setupAlliance").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) nameof (name),
        (object) name
      },
      {
        (object) "acronym",
        (object) tag
      },
      {
        (object) "symbol_code",
        (object) symbol
      },
      {
        (object) nameof (language),
        (object) language
      },
      {
        (object) "public_message",
        (object) message
      },
      {
        (object) "power_limit",
        (object) powerLimit.ToString()
      },
      {
        (object) "is_private",
        !isPrivate ? (object) "0" : (object) "1"
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void SetupAllianceInfo(string language, string message, bool isPrivate, System.Action<bool, object> callback)
  {
    MessageHub.inst.GetPortByAction("Alliance:setupAllianceInfo").SendRequest(new Hashtable()
    {
      {
        (object) "alliance_id",
        (object) PlayerData.inst.allianceId
      },
      {
        (object) nameof (language),
        (object) language
      },
      {
        (object) "public_message",
        (object) message
      },
      {
        (object) "is_private",
        !isPrivate ? (object) "0" : (object) "1"
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ChangeAllianceName(string allianceName, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    postData[(object) "name"] = (object) allianceName;
    MessageHub.inst.GetPortByAction("Alliance:editAllianceName").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ChangeAllianceTag(string acronym, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    postData[(object) nameof (acronym)] = (object) acronym;
    MessageHub.inst.GetPortByAction("Alliance:editAllianceAcronym").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ChangeJoinLimits(long cityLevelLimit, long powerLimit, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "power_limit"] = (object) powerLimit;
    postData[(object) "level_limit"] = (object) cityLevelLimit;
    MessageHub.inst.GetPortByAction("Alliance:setAllianceJoinLimit").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void ChangeRosterNames(string lordName, string dukeName, string baronName, string knightName, string vassalName, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) nameof (lordName)] = (object) lordName;
    postData[(object) nameof (dukeName)] = (object) dukeName;
    postData[(object) nameof (baronName)] = (object) baronName;
    postData[(object) nameof (knightName)] = (object) knightName;
    postData[(object) nameof (vassalName)] = (object) vassalName;
    MessageHub.inst.GetPortByAction("Alliance:setRosterName").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  public void AskHelp(long jobId, System.Action<bool, object> callback)
  {
    Hashtable postData = new Hashtable();
    postData[(object) "job_id"] = (object) jobId;
    MessageHub.inst.GetPortByAction("Alliance:askForHelp").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }

  private void OnJoin()
  {
    DBManager.inst.DB_Alliance.RemoveRosterByUid(PlayerData.inst.uid, 1);
    DBManager.inst.DB_Alliance.RemoveRosterByUid(PlayerData.inst.uid, 2);
    TrackEvent.Instance.Trace("join_alliance", TrackEvent.Type.Facebook);
    TrackEvent.Instance.TraceEventToFB("Join Group");
    if (this.onJoin != null)
      this.onJoin();
    MessageHub.inst.GetPortByAction("Alliance:getAllianceInfo").SendLoader((Hashtable) null, (System.Action<bool, object>) null, true, false);
  }

  private void OnPushAcceptApply(object data)
  {
    MessageHub.inst.GetPortByAction("Alliance:getAllianceInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) ((_param1, _param2) =>
    {
      if (this.onPushAcceptApply != null)
        this.onPushAcceptApply(data);
      this.OnJoin();
    }), true);
  }

  private void OnPushTransferHighlord(object data)
  {
    MessageHub.inst.GetPortByAction("Alliance:getAllianceInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
    if (this.onPushTransferHighlord == null)
      return;
    this.onPushTransferHighlord(data);
  }

  private void OnPushEmpowerTempHighlord(object data)
  {
    MessageHub.inst.GetPortByAction("Alliance:getAllianceInfo").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
    if (this.onPushEmpowerTempHighlord == null)
      return;
    this.onPushEmpowerTempHighlord(data);
  }

  public void OnGameModeReady()
  {
    MessageHub.inst.GetPortByAction("Alliance:checkFirstJoinAlliance").SendLoader((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      if (inData == null)
        return;
      DatabaseTools.UpdateData(inData, "alliance_joined_before", ref this._joinedAllianceBefore);
    }), true, false);
  }

  private void OnPushJoin(object data)
  {
    if (this.onPushJoin == null)
      return;
    this.onPushJoin(data);
  }

  private void OnPushLeave(object data)
  {
    if (this.onPushLeave != null)
      this.onPushLeave(data);
    if (PlayerData.inst.allianceId != 0L)
      return;
    this.OnLeave();
  }

  private void OnLeave()
  {
    PlayerData.inst.allianceId = 0L;
    DBManager.inst.ClearAllianceData();
    AllianceTreasuryCountManager.Instance.SetUnopendChestCount(0);
    if (this.onLeave == null)
      return;
    this.onLeave();
  }

  private void OnPushDisban(object data)
  {
    if (this.onPushDisban != null)
      this.onPushDisban(data);
    this.OnLeave();
  }

  private void OnPushSetRank(object data)
  {
    if (this.onPushSetRank == null)
      return;
    this.onPushSetRank(data);
  }

  private void OnPushModify(object data)
  {
    if (this.onPushModify == null)
      return;
    this.onPushModify(data);
  }

  private void OnPushLevelUpgrade(object data)
  {
    if (this.onPushLevelUpgrade == null)
      return;
    this.onPushLevelUpgrade(data);
  }

  private void OnPushHelpRequest(object data)
  {
    if (this.onPushHelpRequest != null)
      this.onPushHelpRequest(data);
    this.OnHelpChanged();
  }

  private void OnPushHelpResponse(object data)
  {
    if (this.onPushHelpResponse != null)
      this.onPushHelpResponse(data);
    this.OnHelpChanged();
  }

  private void OnPushHelpComplete(object data)
  {
    if (this.onPushHelpComplete != null)
      this.onPushHelpComplete(data);
    this.OnHelpChanged();
  }

  private void OnPushRevokeTempHighlord(object data)
  {
    if (this.onPushRevokeTempHighlord == null)
      return;
    this.onPushRevokeTempHighlord(data);
  }

  private void OnPushTempHighlordTimeup(object data)
  {
    if (this.onPushTempHighlordTimeup == null)
      return;
    this.onPushTempHighlordTimeup(data);
  }

  private void OnPushDenyApplication(object data)
  {
    if (this.onPushDenyApplication == null)
      return;
    this.onPushDenyApplication(data);
  }

  private void OnPushInvitation(object data)
  {
    if (this.onPushInvitation != null)
      this.onPushInvitation(data);
    this.OnInviteChanged();
  }

  private void OnPushRevokeInvitation(object data)
  {
    if (this.onPushRevokeInvitation != null)
      this.onPushRevokeInvitation(data);
    this.OnInviteChanged();
  }

  private void OnInviteChanged()
  {
    if (this.onInviteChanged == null)
      return;
    this.onInviteChanged();
  }

  private void OnHelpChanged()
  {
    if (this.onHelpChanged == null)
      return;
    this.onHelpChanged();
  }

  public enum AllianceActiveRecommendationType
  {
    INVALID = -1,
    NORMAL = 0,
    MAIL_SYSTEM = 1,
    MAIL_ALLIANCE = 2,
    NO_HINT = 3,
    ENTRY_HINT = 4,
  }
}
