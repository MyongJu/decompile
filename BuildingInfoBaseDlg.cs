﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoBaseDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoBaseDlg : UI.Dialog
{
  private const string CANCEL_BUILD_POPUP_ICON = "Texture/PopupIcon/Cancel_Build_Icon";
  public UIWidget pivotWidget;
  public UILabel titleLabel;
  public UILabel levelLabel;
  public UILabel descriptionLabel;
  public UILabel subTitleLabel;
  public UIGrid bottomBtnGrid;
  public UIButton leftBtn;
  public UILabel leftBtnLabel;
  public UIButton rightBtn;
  public UILabel rightBtnLabel;
  protected BuildingInfo buildingInfo;
  protected CityManager.BuildingItem buildingItem;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.buildingItem = (orgParam as BuildingInfoBaseDlg.Parameter).buildingItem;
    this.buildingInfo = ConfigManager.inst.DB_Building.GetData(this.buildingItem.mType + "_" + this.buildingItem.mLevel.ToString());
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    CitadelSystem.inst.FocusConstructionBuilding(BuildingController.mSelectedBuildingController.gameObject, this.pivotWidget, 0.0f);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
  }

  public virtual void OnMoreInfoBtnClick()
  {
    UIManager.inst.OpenPopup("BuildingInfo/BuildingInfoMoreInfoBasePopup", (Popup.PopupParameter) new BuildingInfoMoreInfoBasePopup.Parameter()
    {
      info = this.buildingInfo
    });
  }

  protected virtual void UpdateUI()
  {
    this.UpdateBasic();
    this.UpdateBtnState();
  }

  protected virtual void DrawBenefit(double benifitValue, UILabel nameLabel, UILabel valueLabel, int benefitInternalID, string baseBenefitID, string caclID, UITexture texture = null)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitInternalID];
    nameLabel.text = dbProperty.Name;
    if ((UnityEngine.Object) texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.HandyBuild((UIWidget) texture, dbProperty.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float num = (float) benifitValue;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(DBManager.inst.DB_Local_Benefit.Get(baseBenefitID).total, caclID);
    string str = (double) finalData <= 0.0 ? "-" : string.Empty;
    if (dbProperty.Format == PropertyDefinition.FormatType.Percent)
      valueLabel.text = dbProperty.ConvertToDisplayString((double) num, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - num), true, true);
    else
      valueLabel.text = dbProperty.ConvertToDisplayString((double) num, false, true) + str + dbProperty.ConvertToDisplayString((double) Math.Abs(finalData - num), true, true);
  }

  protected virtual void UpdateBasic()
  {
    this.titleLabel.text = Utils.XLAT(this.buildingInfo.Building_LOC_ID);
    this.levelLabel.text = string.Format("LV {0}[aaaaaa]/{1}", (object) this.buildingInfo.Building_Lvl, (object) ConfigManager.inst._DB_Building.GetMaxLevelByType(this.buildingInfo.Type));
    this.descriptionLabel.text = Utils.XLAT(this.buildingInfo.Type + "_description");
    this.subTitleLabel.text = Utils.XLAT(this.buildingInfo.Type + "_name");
  }

  protected virtual void UpdateBtnState()
  {
    this.leftBtn.onClick.Clear();
    if (this.buildingItem.mBuildingJobID > 0L && JobManager.Instance.GetJob(this.buildingItem.mBuildingJobID) != null)
    {
      this.leftBtn.gameObject.SetActive(true);
      if (JobManager.Instance.GetJob(this.buildingItem.mBuildingJobID).GetJobEvent() == JobEvent.JOB_BUILDING_DECONSTRUCTION)
      {
        this.leftBtnLabel.text = Utils.XLAT("construction_uppercase_cancel_deconstruction");
        this.leftBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCancelDeConstructionClick)));
      }
      else if (JobManager.Instance.GetJob(this.buildingItem.mBuildingJobID).GetJobEvent() == JobEvent.JOB_BUILDING_CONSTRUCTION)
      {
        this.leftBtnLabel.text = Utils.XLAT("construction_uppercase_cancel_upgrade");
        this.leftBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCancelConstructionClick)));
      }
    }
    else if (this.buildingInfo.Destrotyed == 1)
    {
      this.leftBtn.gameObject.SetActive(true);
      this.leftBtnLabel.text = Utils.XLAT("id_uppercase_destroy");
      this.leftBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnDeconstructionClick)));
    }
    else
      this.leftBtn.gameObject.SetActive(false);
    this.rightBtn.isEnabled = true;
    this.rightBtnLabel.text = Utils.XLAT("id_uppercase_more_info");
    this.bottomBtnGrid.repositionNow = true;
  }

  private void OnDeconstructionClick()
  {
    UIManager.inst.OpenPopup("BuildingInfo/DestroyConfirmPopup", (Popup.PopupParameter) new DestroyConfirmPopup.Parameter()
    {
      info = this.buildingInfo,
      normalConfirm = new System.Action(this.OnFinalDestroyCheckNormal),
      instanceConfirm = new System.Action(this.OnFinalDestroyCheckInstance)
    });
  }

  private void OnCancelConstructionClick()
  {
    PopupManager.Instance.Open<MessageBoxWithIconAnd2Buttons>("MessageBoxWithIconAnd2Buttons").Initialize(Utils.XLAT("construction_uppercase_cancel_upgrade"), ScriptLocalization.GetWithPara("construction_cancel_upgrade_description", new Dictionary<string, string>()
    {
      {
        "building_name",
        Utils.XLAT(this.buildingInfo.Building_LOC_ID)
      }
    }, true), Utils.XLAT("construction_cancel_upgrade_warning"), Utils.XLAT("id_uppercase_yes"), Utils.XLAT("id_uppercase_no"), new System.Action(this.OnCancelConstructionYes), (System.Action) null, "Texture/PopupIcon/Cancel_Build_Icon");
  }

  private void OnCancelConstructionYes()
  {
    CityManager.inst.CancelBuild(this.buildingItem.mID, new System.Action(this.CancelFinished));
  }

  private void OnCancelDeConstructionClick()
  {
    CityManager.inst.CancelDeconstruct(this.buildingItem.mID, new System.Action(this.CancelFinished));
  }

  private void CancelFinished()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void OnFinalDestroyCheckNormal()
  {
    UIManager.inst.OpenPopup("BuildingInfo/DestroyFinalConfirmPopup", (Popup.PopupParameter) new FinalDeconstructConfirmPopup.Parameter()
    {
      pinfo = this.buildingInfo,
      bInstant = false,
      instanceConfirm = new System.Action<bool>(this.OnDestoryConfirmed)
    });
  }

  private void OnFinalDestroyCheckInstance()
  {
    UIManager.inst.OpenPopup("BuildingInfo/DestroyFinalConfirmPopup", (Popup.PopupParameter) new FinalDeconstructConfirmPopup.Parameter()
    {
      pinfo = this.buildingInfo,
      bInstant = true,
      instanceConfirm = new System.Action<bool>(this.OnDestoryConfirmed)
    });
  }

  private void OnDestoryConfirmed(bool inst)
  {
    if (inst)
      this.OnInstDestroyBtnPressed();
    else
      this.OnDestroyBtnPressed();
  }

  private void OnInstDestroyBtnPressed()
  {
    CityManager.inst.DeconstructBuilding(this.buildingItem.mID, 0, (System.Action) (() =>
    {
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }));
  }

  private void OnDestroyBtnPressed()
  {
    double buildTime = this.buildingInfo.Build_Time / 2.0;
    if (BuildQueueManager.Instance.IsBuildQueueFull(buildTime))
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopData("shopitem_build_queue_middle").Item_InternalId);
      BuildQueueManager.Instance.GetMoreQueueTime(new BuildQueuePopup.Parameter()
      {
        showLimit = true,
        needTimeString = Utils.FormatTime((int) buildTime, false, false, true),
        remainTimeString = Utils.FormatTime(BuildQueueManager.Instance.GetRemainQueueTime(), false, false, true),
        buyCount = Mathf.Max(1, Mathf.CeilToInt((float) ((int) buildTime - BuildQueueManager.Instance.GetRemainQueueTime()) / itemStaticInfo.Value))
      });
    }
    else
      CityManager.inst.DeconstructBuilding(this.buildingItem.mID, (int) this.buildingInfo.Build_Time / 2, (System.Action) (() =>
      {
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }));
  }

  private void OnSpeedupConfirmYes()
  {
    CityManager.BuildingItem fluxBuilding = CityManager.inst.FindFluxBuilding();
    if (fluxBuilding == null)
      return;
    UIManager.inst.OpenPopup("SpeedUpPopup", (Popup.PopupParameter) new SpeedUpPopUp.Parameter()
    {
      jobId = fluxBuilding.mBuildingJobID,
      speedupType = ItemBag.ItemType.speedup,
      timerType = TimerType.TIMER_UPGRADE
    });
  }

  private void OnCancelBuildConfirmed()
  {
    CityManager.inst.CancelBuild(this.buildingItem.mID, new System.Action(this.CancelFinished));
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || !int.TryParse(hashtable[(object) "event_type"].ToString(), out result1) || (result1 != 30 && result1 != 31 || long.Parse(hashtable[(object) "building_id"].ToString()) != this.buildingItem.mID))
      return;
    this.UpdateUI();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public CityManager.BuildingItem buildingItem;
  }
}
