﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AltarSummonDlg : UI.Dialog
{
  private HeroRecruitPayloadData payloadData = new HeroRecruitPayloadData();
  [SerializeField]
  private AltarSummonOrbitController _orbitController;
  [SerializeField]
  private UIButton _claimButton;
  [SerializeField]
  private UIButton _closeButton;
  [SerializeField]
  private GameObject _resultContainer;
  [SerializeField]
  private HeroCardRotator _rotator;
  [SerializeField]
  private ParticleSystem _particleSystem;
  private int itemId;
  private int rewardId;
  private List<int> selectedIds;

  private void Init()
  {
    this._orbitController.Init(this.itemId);
    this._orbitController.onAnimationStartHandler = new System.Action(this.OnAnimationStart);
    this._orbitController.onAnimationEndHandler = new System.Action(this.OnAnimationEnd);
    this._orbitController.onItemClickHandler = new System.Action(this.OnItemClick);
  }

  private void Dispose()
  {
    this._orbitController.onAnimationStartHandler = (System.Action) null;
    this._orbitController.onAnimationEndHandler = (System.Action) null;
    this._orbitController.onItemClickHandler = (System.Action) null;
  }

  private void UpdateUI()
  {
    this._orbitController.Status = AltarSummonOrbitController.OrbitStatus.Idle;
  }

  public void OnSummonButtonClicked()
  {
    if (this.selectedIds == null || this.selectedIds.Count == 0)
      UIManager.inst.toast.Show(Utils.XLAT("toast_summon_rewards_altar_rewards_not_selected"), (System.Action) null, 4f, false);
    else
      MessageHub.inst.GetPortByAction("Item:useConsumableItem").SendRequest(new Hashtable()
      {
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        },
        {
          (object) "item_id",
          (object) this.itemId
        },
        {
          (object) "city_id",
          (object) PlayerData.inst.cityId
        },
        {
          (object) "amount",
          (object) 1
        },
        {
          (object) "rewards",
          (object) this.selectedIds
        }
      }, (System.Action<bool, object>) ((ret, orgData) =>
      {
        if (!ret)
          return;
        this.payloadData.Decode(orgData);
        this._orbitController.Status = AltarSummonOrbitController.OrbitStatus.Animating;
      }), true);
  }

  public void OnCloseDialogClicked()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void OnAnimationStart()
  {
    this._claimButton.gameObject.SetActive(false);
  }

  private void OnAnimationEnd()
  {
    this._resultContainer.gameObject.SetActive(true);
    this._closeButton.gameObject.SetActive(false);
    this.PlayRotateAnimation();
  }

  private void PlayRotateAnimation()
  {
    this._particleSystem.Clear();
    this._particleSystem.Play(true);
    this._rotator.Play(this.payloadData, new System.Action(this.OnRotateAnimationEnd));
  }

  private void OnRotateAnimationEnd()
  {
    this._closeButton.gameObject.SetActive(true);
  }

  public void OnItemClick()
  {
    UIManager.inst.OpenPopup("AltarSummon/AltarSummonChoosePopup", (Popup.PopupParameter) new AltarSummonChoosePopup.Parameter()
    {
      itemId = this.itemId,
      selectedIds = this.selectedIds,
      onRewardsChosen = new System.Action<List<int>>(this.OnRewardsChosen)
    });
  }

  private void OnRewardsChosen(List<int> rewardIds)
  {
    this._orbitController.UpdateUI(rewardIds);
    this.selectedIds = rewardIds;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemId = (orgParam as AltarSummonDlg.Parameter).itemId;
    this.Init();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Dispose();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int itemId;
  }
}
