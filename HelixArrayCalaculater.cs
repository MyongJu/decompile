﻿// Decompiled with JetBrains decompiler
// Type: HelixArrayCalaculater
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public static class HelixArrayCalaculater
{
  public static int GetIndex(int x, int y)
  {
    int val1 = Math.Abs(x);
    int val2 = Math.Abs(y);
    int num1 = Math.Max(val1, val2);
    int edgeCount = HelixArrayCalaculater.GetEdgeCount(num1);
    int num2 = edgeCount * edgeCount;
    int num3 = num2 - (edgeCount - 1);
    int num4 = num3 - (edgeCount - 1);
    int num5 = num4 - (edgeCount - 1);
    WorldCoordinate worldCoordinate1 = new WorldCoordinate(num1, num1);
    WorldCoordinate worldCoordinate2 = new WorldCoordinate(-num1, num1);
    WorldCoordinate worldCoordinate3 = new WorldCoordinate(-num1, -num1);
    WorldCoordinate worldCoordinate4 = new WorldCoordinate(num1, -num1);
    if (val1 == val2)
    {
      if (x == 0)
        return 1;
      if (x > 0 && y > 0)
        return num2;
      if (x > 0 && y < 0)
        return num5;
      if (x < 0 && y < 0)
        return num4;
      if (x < 0 && y > 0)
        return num3;
      throw new IndexOutOfRangeException("should not go here.");
    }
    if (val1 > val2)
    {
      if (x > 0)
        return num5 - (y - worldCoordinate4.Y);
      return num3 - (worldCoordinate2.Y - y);
    }
    if (y > 0)
      return num2 - (worldCoordinate1.X - x);
    return num4 - (x - worldCoordinate3.X);
  }

  public static WorldCoordinate GetCoordinate(int index)
  {
    WorldCoordinate worldCoordinate = new WorldCoordinate();
    int loopIndex = HelixArrayCalaculater.GetLoopIndex(index);
    if (loopIndex == 0)
      return worldCoordinate;
    return HelixArrayCalaculater.GetResult(index, loopIndex);
  }

  public static int GetLoopIndex(int index)
  {
    return Mathf.CeilToInt((float) (((double) Mathf.Sqrt((float) index) - 1.0) / 2.0));
  }

  public static int GetLoopSum(int loopIndex)
  {
    return (2 * loopIndex + 1) * (2 * loopIndex + 1);
  }

  public static int GetEdgeCount(int loopIndex)
  {
    return 2 * loopIndex + 1;
  }

  public static int GetLoopCount(int loopIndex)
  {
    if (loopIndex == 0)
      return 1;
    return loopIndex * 8;
  }

  private static WorldCoordinate GetResult(int index, int loopIndex)
  {
    WorldCoordinate worldCoordinate = new WorldCoordinate();
    int loopSum = HelixArrayCalaculater.GetLoopSum(loopIndex - 1);
    int num1 = index - loopSum;
    int loopCount = HelixArrayCalaculater.GetLoopCount(loopIndex);
    if (loopCount <= 0)
      return new WorldCoordinate();
    int num2 = num1 * 4 / loopCount;
    int num3 = (int) ((double) loopCount * 0.25);
    int num4 = num1 % num3;
    switch (num2)
    {
      case 0:
      case 4:
        worldCoordinate.X = loopIndex;
        worldCoordinate.Y = loopIndex - num4;
        break;
      case 1:
        worldCoordinate.X = loopIndex - num4;
        worldCoordinate.Y = -loopIndex;
        break;
      case 2:
        worldCoordinate.X = -loopIndex;
        worldCoordinate.Y = -loopIndex + num4;
        break;
      case 3:
        worldCoordinate.X = -loopIndex + num4;
        worldCoordinate.Y = loopIndex;
        break;
    }
    return worldCoordinate;
  }
}
