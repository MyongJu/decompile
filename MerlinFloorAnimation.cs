﻿// Decompiled with JetBrains decompiler
// Type: MerlinFloorAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MerlinFloorAnimation : MonoBehaviour
{
  private const float ANIM_DURATION = 0.3f;
  private const float ORIGION_Y = 472f;
  private const float HEIGHT = 966f;
  [SerializeField]
  private MerlinFloorItem _sourceRoomItem;
  [SerializeField]
  private MerlinFloorItem _targetRoomItem;
  private string _sourceRoomResource;
  private string _targetRoomResource;

  private void CreateFloorItem(UserMerlinTowerData userMerlinTowerData, ref string currentResource, ref MerlinFloorItem roomItem)
  {
    if (!((UnityEngine.Object) roomItem == (UnityEngine.Object) null))
      return;
    currentResource = "Prefab/MerlinTower/MerlinFloor_1";
    if ((bool) ((UnityEngine.Object) roomItem))
      UnityEngine.Object.Destroy((UnityEngine.Object) roomItem.gameObject);
    GameObject go = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(currentResource, (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset(currentResource, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    go.transform.SetParent(this.transform);
    go.transform.localPosition = Vector3.zero;
    go.transform.localScale = Vector3.one;
    NGUITools.SetLayer(go, this.gameObject.layer);
    roomItem = go.GetComponent<MerlinFloorItem>();
    roomItem.FitHorizonal();
  }

  public Vector3 GetDoorPosition()
  {
    if ((UnityEngine.Object) this._targetRoomItem != (UnityEngine.Object) null)
      return this._targetRoomItem.DoorPosition;
    return Vector3.zero;
  }

  public void SetCurrentRoomData(UserMerlinTowerData userMerlinTowerData)
  {
    this.CreateFloorItem(userMerlinTowerData, ref this._targetRoomResource, ref this._targetRoomItem);
    this._targetRoomItem.SetRoom(userMerlinTowerData);
  }

  public void PlayAnimation(System.Action callback = null)
  {
    this.CreateFloorItem(MerlinTowerPayload.Instance.UserData, ref this._sourceRoomResource, ref this._sourceRoomItem);
    this.CreateFloorItem(MerlinTowerPayload.Instance.UserData, ref this._targetRoomResource, ref this._targetRoomItem);
    this._sourceRoomItem.SetRoom(MerlinTowerPayload.Instance.UserData);
    this._targetRoomItem.SetRoom(MerlinTowerPayload.Instance.UserData);
    AudioManager.Instance.PlaySound("sfx_dragon_knight_move", false);
    this.PlayUpAnimation();
    this.StartCoroutine(this.AnimationCallbackCoroutine(callback));
  }

  [DebuggerHidden]
  private IEnumerator AnimationCallbackCoroutine(System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MerlinFloorAnimation.\u003CAnimationCallbackCoroutine\u003Ec__Iterator84()
    {
      callback = callback,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  public MerlinFloorItem GetTargetFloorItem()
  {
    return this._targetRoomItem;
  }

  public void SetSourceRoomEnabled(bool enabled)
  {
    if (!(bool) ((UnityEngine.Object) this._sourceRoomItem))
      return;
    this._sourceRoomItem.gameObject.SetActive(enabled);
  }

  protected void PlayUpAnimation()
  {
    this.SetSourceRoomEnabled(true);
    this._sourceRoomItem.MoveTo(0.0f, 472f, 0.0f, -494f, 0.3f);
    this._sourceRoomItem.SetAlpha(1f);
    this._sourceRoomItem.SetScale(1f);
    this._targetRoomItem.MoveTo(0.0f, 1438f, 0.0f, 472f, 0.3f);
    this._targetRoomItem.SetAlpha(1f);
    this._targetRoomItem.SetScale(1f);
  }
}
