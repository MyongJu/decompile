﻿// Decompiled with JetBrains decompiler
// Type: ItemsForShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ItemsForShop : MonoBehaviour
{
  private Dictionary<int, bool> isInits = new Dictionary<int, bool>();
  private Dictionary<int, List<int>> datas = new Dictionary<int, List<int>>();
  private List<ShopItemGroupRenderer> groupList = new List<ShopItemGroupRenderer>();
  private GameObjectPool pools = new GameObjectPool();
  private bool init;
  public IconGroup group;
  public UIScrollView scrollView;
  public UIGrid content;
  public ShopItemGroupRenderer groupPrefab;
  public System.Action<int> OnItemSelected;
  public Icon shopItemDetail;
  private ItemBag.ItemType type;
  private int shopId;
  private int itemId;

  private void InitGroup(GameObject go, int realIndex)
  {
    realIndex = Mathf.Abs(realIndex);
    ShopItemGroupRenderer component = go.GetComponent<ShopItemGroupRenderer>();
    component.OnCustomBuy = new System.Action<int>(this.OnBuyHandler);
    if (component.Selected)
    {
      component.HideHightLight();
      NGUITools.SetActive(this.shopItemDetail.gameObject, false);
    }
    if (!this.datas.ContainsKey(realIndex))
      return;
    List<int> data = this.datas[realIndex];
    component.SetData(data.ToArray());
  }

  private void OnBuyHandler(int shopId)
  {
    string decoration = PlayerData.inst.playerCityData.decoration;
    this.shopId = shopId;
    ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId);
    if (dataByInternalId == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
    if (itemStaticInfo == null)
      return;
    bool flag = false;
    string Term = "stronghold_appearance_confirm_description";
    if (!string.IsNullOrEmpty(itemStaticInfo.Param1))
    {
      if (!string.IsNullOrEmpty(decoration))
      {
        if (decoration != itemStaticInfo.Param1)
          flag = true;
      }
      else
      {
        Term = "stronghold_appearance1_confirm_description";
        flag = true;
      }
    }
    if (flag)
      this.ShowWarn(ScriptLocalization.Get(Term, true));
    else
      this.BuyAndUse();
  }

  private void ShowWarn(string content)
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("id_uppercase_warning", true),
      content = content,
      yes = ScriptLocalization.Get("id_uppercase_confirm", true),
      no = ScriptLocalization.Get("id_uppercase_cancel", true),
      yesCallback = new System.Action(this.BuyAndUse),
      noCallback = (System.Action) null
    });
  }

  private void BuyAndUse()
  {
    ItemBag.Instance.BuyAndUseShopItem(this.shopId, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_stronghold_appearance_buy_success", true), (System.Action) null, 4f, false);
    }), (Hashtable) null, 1);
  }

  private void Start()
  {
    this.Init();
  }

  private void Init()
  {
    if (this.init)
      return;
    this.init = true;
    this.pools.Initialize(this.groupPrefab.gameObject, this.gameObject);
  }

  public void Dispose()
  {
    this.group.Dispose();
    this.shopItemDetail.Dispose();
    this.init = false;
    this.pools.Clear();
  }

  private void OnSelectedHandler(int index)
  {
    this.UpdateUI();
  }

  private void Clear()
  {
    for (int index = 0; index < this.groupList.Count; ++index)
      this.groupList[index].Dispose();
    this.groupList.Clear();
  }

  public void SetData(ItemBag.ItemType type)
  {
    this.type = type;
    this.Refresh();
  }

  public void Refresh()
  {
    this.Init();
    this.OnSelectedHandler(0);
  }

  private void SetView()
  {
    NGUITools.SetActive(this.scrollView.gameObject, true);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.shopItemDetail.gameObject, false);
    this.CreateDatas();
    this.SetView();
    if (!this.isInits.ContainsKey(0))
    {
      this.isInits.Add(0, true);
      for (int index = 0; index < this.datas.Count; ++index)
      {
        if (this.datas.ContainsKey(index))
          this.InitGroup(this.GetGroup().gameObject, index);
      }
    }
    this.content.Reposition();
    this.scrollView.ResetPosition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
  }

  private ShopItemGroupRenderer GetGroup()
  {
    GameObject go = NGUITools.AddChild(this.content.gameObject, this.groupPrefab.gameObject);
    ShopItemGroupRenderer component = go.GetComponent<ShopItemGroupRenderer>();
    component.OnSelectedHandler = new System.Action<bool, int>(this.OnViewShopItemDetail);
    this.groupList.Add(component);
    NGUITools.SetActive(go, true);
    return component;
  }

  private void OnSelectedHandler(ShopItemRenderer selected)
  {
    for (int index = 0; index < this.group.TotalIcons.Count; ++index)
    {
      ShopItemRenderer totalIcon = this.group.TotalIcons[index] as ShopItemRenderer;
      if ((UnityEngine.Object) selected != (UnityEngine.Object) totalIcon)
        totalIcon.Selected = false;
    }
  }

  private void OnViewShopItemDetail(bool view, int shopId)
  {
    if (this.OnItemSelected != null)
    {
      ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId);
      if (dataByInternalId != null)
        this.OnItemSelected(dataByInternalId.Item_InternalId);
    }
    NGUITools.SetActive(this.shopItemDetail.gameObject, view);
    this.shopItemDetail.FeedData((IComponentData) this.GetIconData(shopId));
    for (int index = 0; index < this.groupList.Count; ++index)
    {
      if (!this.groupList[index].Contains(shopId))
        this.groupList[index].HideHightLight();
    }
  }

  private IconData GetIconData(int shopId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(shopId).Item_InternalId);
    IconData iconData = new IconData();
    iconData.image = itemStaticInfo.ImagePath;
    iconData.Data = (object) shopId;
    iconData.contents = new string[2];
    iconData.contents[0] = itemStaticInfo.LocName;
    iconData.contents[1] = itemStaticInfo.LocDescription;
    return iconData;
  }

  private void CreateDatas()
  {
    this.datas.Clear();
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", this.type);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    List<int> intList = new List<int>();
    int key = 0;
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
    {
      if (intList.Count < 2)
      {
        intList.Add(currentSaleShopItems[index].internalId);
        if (intList.Count == 2)
        {
          this.datas.Add(key, intList);
          ++key;
          intList = new List<int>();
        }
      }
    }
    if (currentSaleShopItems.Count % 2 <= 0)
      return;
    this.datas.Add(key, intList);
  }

  private List<IconData> GetIconDatas()
  {
    List<ShopStaticInfo> currentSaleShopItems = ItemBag.Instance.GetCurrentSaleShopItems("shop", this.type);
    currentSaleShopItems.Sort(new Comparison<ShopStaticInfo>(this.CompareShopItem));
    List<IconData> iconDataList = new List<IconData>();
    for (int index = 0; index < currentSaleShopItems.Count; ++index)
      iconDataList.Add(this.GetIconData(currentSaleShopItems[index].internalId));
    return iconDataList;
  }

  private int CompareShopItem(ShopStaticInfo a, ShopStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }
}
