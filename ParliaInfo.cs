﻿// Decompiled with JetBrains decompiler
// Type: ParliaInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParliaInfo : MonoBehaviour
{
  private List<ParliamentHeroItem> _AllParliamentHeroItem = new List<ParliamentHeroItem>();
  public GameObject parliaContainer;
  public GameObject scaleContainer;
  public UILabel legendname;
  public UILabel level;
  public UILabel username;
  public UILabel score;

  public void SeedData(Hashtable myLegend, string _username)
  {
    this.CreateParliament(myLegend, _username);
  }

  public void CreateParliament(Hashtable myLegend, string _username)
  {
    ParliamentHeroItem heroBookItem = this.CreateHeroBookItem(AssetManager.Instance.HandyLoad("Prefab/UI/Common/ParliamentHeroItem", (System.Type) null) as GameObject);
    LegendCardData legendCardData = new LegendCardData();
    legendCardData.Decode((object) myLegend, 0L);
    heroBookItem.SetData(legendCardData);
    ParliamentInfo parliamentInfo = ConfigManager.inst.DB_Parliament.Get(ConfigManager.inst.DB_ParliamentHero.Get(legendCardData.LegendId).ParliamentPosition.ToString());
    if (parliamentInfo != null)
      this.username.text = parliamentInfo.Name;
    this.legendname.text = legendCardData.HeroName;
    this.score.text = ScriptLocalization.GetWithPara("hero_score_level_num", new Dictionary<string, string>()
    {
      {
        "0",
        legendCardData.Score.ToString()
      }
    }, 1 != 0);
    heroBookItem.ShowlabelLevel(true);
  }

  protected ParliamentHeroItem CreateHeroBookItem(GameObject _HeroBookItemTemplate)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(_HeroBookItemTemplate);
    ParliamentHeroItem component = gameObject.GetComponent<ParliamentHeroItem>();
    this._AllParliamentHeroItem.Add(component);
    gameObject.transform.SetParent(this.scaleContainer.transform);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    return component;
  }

  public void Reset()
  {
    foreach (ParliamentHeroItem componentsInChild in this.scaleContainer.GetComponentsInChildren<ParliamentHeroItem>(true))
    {
      componentsInChild.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    }
  }

  public class Data
  {
    public Hashtable myLegend;
  }
}
