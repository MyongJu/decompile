﻿// Decompiled with JetBrains decompiler
// Type: KingdomWarActivityUIData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class KingdomWarActivityUIData : ActivityBaseUIData
{
  private KingdomWarActivityData _kingdomData;

  public override ActivityBaseUIData.ActivityType Type
  {
    get
    {
      return ActivityBaseUIData.ActivityType.KingdomWar;
    }
  }

  public override string EventKey
  {
    get
    {
      return "kingdom_war_activity";
    }
  }

  private KingdomWarActivityData KingdomData
  {
    get
    {
      if (this._kingdomData == null)
        this._kingdomData = this.Data as KingdomWarActivityData;
      return this._kingdomData;
    }
  }

  public override void DisplayActivityDetail()
  {
    UIManager.inst.OpenPopup("Activity/KingdomWarEventPopup", (Popup.PopupParameter) new KingdomWarPopup.Parameter()
    {
      data = this.KingdomData
    });
  }

  public override IconData GetUnstartIconData()
  {
    return base.GetUnstartIconData();
  }

  public override IconData GetNormalIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/icon_cross_kingdom_war";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("throne_invasion_title");
    iconData.contents[1] = Utils.XLAT("throne_invasion_prepare_one_liner");
    iconData.Data = (object) this;
    return iconData;
  }

  public override IconData GetADIconData()
  {
    IconData iconData = new IconData();
    iconData.image = "Texture/Events/alliance_boss_image_big";
    iconData.contents = new string[2];
    iconData.contents[0] = Utils.XLAT("alliance_portal_challenge_title");
    iconData.contents[1] = Utils.XLAT("alliance_portal_challenge_one_line_description");
    return iconData;
  }

  public override int GetRemainTime()
  {
    return this.KingdomData.RemainTime;
  }

  public override string TimePre
  {
    get
    {
      return this.KingdomData.IsInWar ? "throne_invasion_started" : "throne_invasion_not_started";
    }
  }
}
