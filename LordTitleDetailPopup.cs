﻿// Decompiled with JetBrains decompiler
// Type: LordTitleDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class LordTitleDetailPopup : Popup
{
  private Dictionary<int, LordTitleBenefitSolt> _itemDict = new Dictionary<int, LordTitleBenefitSolt>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UITexture _textureAvatorIcon;
  [SerializeField]
  private UILabel _labelAvatorName;
  [SerializeField]
  private UILabel _labelAvatorDescription;
  [SerializeField]
  private UIButton _buttonActivateAvator;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UIGrid _grid;
  [SerializeField]
  private GameObject _itemPrefab;
  private int _lordTitleId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this._itemPool.Initialize(this._itemPrefab, this._grid.gameObject);
    LordTitleDetailPopup.Parameter parameter = orgParam as LordTitleDetailPopup.Parameter;
    if (parameter != null)
      this._lordTitleId = parameter.lordTitleId;
    this.UpdateMainUI();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnActivateAvatorClick()
  {
    LordTitlePayload.Instance.ActivateLordTitle(this._lordTitleId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.OnCloseClick();
    }));
  }

  public void OnViewTitleClick()
  {
    this.OnCloseClick();
    UIManager.inst.OpenDlg("LordTitle/LordTitleDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void UpdateMainUI()
  {
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._lordTitleId);
    if (lordTitleMainInfo != null)
    {
      MarksmanPayload.Instance.FeedMarksmanTexture(this._textureAvatorIcon, lordTitleMainInfo.AvatorIconPath);
      this._labelAvatorName.text = lordTitleMainInfo.LocName + LordTitlePayload.Instance.GetAvatorNameTimeSuffix(this._lordTitleId, true);
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelAvatorName, lordTitleMainInfo.quality);
      this._labelAvatorDescription.text = lordTitleMainInfo.LocDescription;
    }
    this._buttonActivateAvator.isEnabled = !LordTitlePayload.Instance.IsAvatorLocked(this._lordTitleId);
  }

  private void UpdateUI()
  {
    this.ClearData();
    LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._lordTitleId);
    if (lordTitleMainInfo != null && lordTitleMainInfo.benefits != null && lordTitleMainInfo.benefits.benefits != null)
    {
      using (Dictionary<string, float>.Enumerator enumerator = lordTitleMainInfo.benefits.benefits.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, float> current = enumerator.Current;
          int result = 0;
          int.TryParse(current.Key, out result);
          if (result > 0)
          {
            LordTitleBenefitSolt slot = this.GenerateSlot(result, current.Value);
            if (!this._itemDict.ContainsKey(result))
              this._itemDict.Add(result, slot);
          }
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, LordTitleBenefitSolt>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._grid.repositionNow = true;
    this._grid.Reposition();
    this._scrollView.ResetPosition();
  }

  private LordTitleBenefitSolt GenerateSlot(int benefitId, float benefitValue)
  {
    GameObject gameObject = this._itemPool.AddChild(this._grid.gameObject);
    gameObject.SetActive(true);
    LordTitleBenefitSolt component = gameObject.GetComponent<LordTitleBenefitSolt>();
    component.SetData(benefitId, benefitValue);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int lordTitleId;
  }
}
