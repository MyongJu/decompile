﻿// Decompiled with JetBrains decompiler
// Type: SamsungIap.Model.ErrorVo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace SamsungIap.Model
{
  [Serializable]
  public class ErrorVo
  {
    public int mErrorCode;
    public string mErrorString;
    public string mExtraString;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[ErrorVo] ");
      stringBuilder.Append("mErrorCode: " + (object) this.mErrorCode + " ");
      stringBuilder.Append("mErrorString: " + this.mErrorString + " ");
      stringBuilder.Append("mExtraString: " + this.mExtraString + " ");
      return stringBuilder.ToString();
    }
  }
}
