﻿// Decompiled with JetBrains decompiler
// Type: SamsungIap.Model.ItemVo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace SamsungIap.Model
{
  [Serializable]
  public class ItemVo : BaseVo
  {
    public string mType;
    public string mSubscriptionDurationUnit;
    public string mSubscriptionDurationMultiplier;
    public string mJsonString;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[ItemVo] ");
      stringBuilder.Append("mItemId: " + this.mItemId + " ");
      stringBuilder.Append("mItemName: " + this.mItemName + " ");
      stringBuilder.Append("mItemPrice: " + (object) this.mItemPrice + " ");
      stringBuilder.Append("mItemPriceString: " + this.mItemPriceString + " ");
      stringBuilder.Append("mCurrencyUnit: " + this.mCurrencyUnit + " ");
      stringBuilder.Append("mCurrencyCode: " + this.mCurrencyCode + " ");
      stringBuilder.Append("mItemDesc: " + this.mItemDesc + " ");
      stringBuilder.Append("mItemImageUrl: " + this.mItemImageUrl + " ");
      stringBuilder.Append("mItemDownloadUrl: " + this.mItemDownloadUrl + " ");
      stringBuilder.Append("mType: " + this.mType + " ");
      stringBuilder.Append("mSubscriptionDurationUnit: " + this.mSubscriptionDurationUnit + " ");
      stringBuilder.Append("mSubscriptionDurationMultiplier: " + this.mSubscriptionDurationMultiplier + " ");
      stringBuilder.Append("mJsonString: " + this.mJsonString + " ");
      return stringBuilder.ToString();
    }
  }
}
