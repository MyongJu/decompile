﻿// Decompiled with JetBrains decompiler
// Type: SamsungIap.Model.VerificationVo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace SamsungIap.Model
{
  [Serializable]
  public class VerificationVo
  {
    public string mItemId;
    public string mItemName;
    public string mItemDesc;
    public string mPurchaseDate;
    public string mPaymentId;
    public string mPaymentAmount;
    public string mStatus;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[VerificationVo] ");
      stringBuilder.Append("mItemId: " + this.mItemId + " ");
      stringBuilder.Append("mItemName: " + this.mItemName + " ");
      stringBuilder.Append("mItemDesc: " + this.mItemDesc + " ");
      stringBuilder.Append("mPurchaseDate: " + this.mPurchaseDate + " ");
      stringBuilder.Append("mPaymentId: " + this.mPaymentId + " ");
      stringBuilder.Append("mPaymentAmount: " + this.mPaymentAmount + " ");
      stringBuilder.Append("mStatus: " + this.mItemDesc + " ");
      return stringBuilder.ToString();
    }
  }
}
