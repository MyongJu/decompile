﻿// Decompiled with JetBrains decompiler
// Type: VaultConfirmationPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VaultConfirmationPopup : Popup
{
  public UILabel btnText;
  public UILabel titleText;
  public UILabel content;
  public GameObject oneButtonRootNode;
  public GameObject twoButtonRootNode;
  private int slotId;
  private VaultConfirmationPopup.ConfirmType confirmType;
  private System.Action func;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    VaultConfirmationPopup.Parameter parameter = orgParam as VaultConfirmationPopup.Parameter;
    if (parameter != null)
    {
      this.slotId = parameter.slotId;
      this.confirmType = parameter.confirmType;
      this.func = parameter.func;
    }
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCancelBtnPressed()
  {
    this.OnCloseBtnPressed();
  }

  public void OnOkayBtnPressed()
  {
    VaultFunHelper.Instance.DrawGold(true, (System.Action) null);
    this.OnCloseBtnPressed();
  }

  public void OnEarnBtnPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this.oneButtonRootNode, false);
    NGUITools.SetActive(this.twoButtonRootNode, false);
    switch (this.confirmType)
    {
      case VaultConfirmationPopup.ConfirmType.TO_UNLOCK:
        this.btnText.text = ScriptLocalization.Get("vault_go_to_unlock_button", true);
        TreasuryInfo treasuryInfo = ConfigManager.inst.DB_Treasury.Get(this.slotId);
        Dictionary<string, string> para = new Dictionary<string, string>();
        if (treasuryInfo != null)
        {
          para.Add("1", treasuryInfo.duration.ToString());
          this.content.text = (double) treasuryInfo.duration <= 1.0 ? ScriptLocalization.GetWithPara("vault_unlock_description", para, true) : ScriptLocalization.GetWithPara("vault_unlock_plural_description", para, true);
        }
        NGUITools.SetActive(this.oneButtonRootNode, true);
        break;
      case VaultConfirmationPopup.ConfirmType.TO_WITHDRAW_WITHOUT_INTEREST:
        this.content.text = Utils.XLAT("vault_withdrawal_early_warning");
        NGUITools.SetActive(this.twoButtonRootNode, true);
        break;
      case VaultConfirmationPopup.ConfirmType.TO_EARN:
        this.btnText.text = Utils.XLAT("vault_gold_topup_button");
        this.content.text = Utils.XLAT("vault_gold_not_enough_description");
        NGUITools.SetActive(this.oneButtonRootNode, true);
        break;
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public int slotId;
    public VaultConfirmationPopup.ConfirmType confirmType;
    public System.Action func;
  }

  public enum ConfirmType
  {
    TO_UNLOCK,
    TO_WITHDRAW_WITHOUT_INTEREST,
    TO_EARN,
  }
}
