﻿// Decompiled with JetBrains decompiler
// Type: TempleSkillInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class TempleSkillInfo
{
  public int internalId;
  [Config(Name = "group")]
  public string Group;
  [Config(Name = "type")]
  public string Type;
  [Config(Name = "tier")]
  public int Tier;
  [Config(Name = "unlock_level")]
  public int UnlockLevel;
  [Config(Name = "spend")]
  public int spend;
  [Config(Name = "cd_time")]
  public float _CdTimeInMinute;
  public int CdTime;
  [Config(Name = "need_donation")]
  public int NeedDonation;
  [Config(Name = "donation_time")]
  public float _DonationTimeInMinute;
  public int DonationTime;
  [Config(Name = "max_donation")]
  public int MaxDonation;
  [Config(Name = "target_type")]
  public string TargetType;
  [Config(Name = "target_area")]
  public int TargetArea;
  [Config(Name = "effect_time_min")]
  public float _EffectTimeMinInMinute;
  public int EffectTimeMin;
  [Config(Name = "effect_time_max")]
  public float _EffectTimeMaxInMinute;
  public int EffectTimeMax;
  [Config(Name = "param_1_min")]
  public float Param1Min;
  [Config(Name = "param_1_max")]
  public float Param1Max;
  [Config(Name = "param_2_min")]
  public float Param2Min;
  [Config(Name = "param_2_max")]
  public float Param2Max;
  [Config(Name = "name")]
  public string NameKey;
  [Config(Name = "description")]
  public string DescriptionKey;
  [Config(Name = "icon")]
  public string Icon;
  [Config(Name = "is_harmful")]
  public int IsHarmfull;
  [Config(Name = "display_effect")]
  public string DisplayEffect;
  [Config(Name = "display_effect_buff")]
  public string DisplayBuff;

  public string Name
  {
    get
    {
      if (this.NameKey == null)
        return "NULL";
      return ScriptLocalization.Get(this.NameKey, true);
    }
  }

  public string Desc
  {
    get
    {
      if (this.DescriptionKey == null)
        return "NULL";
      return ScriptLocalization.Get(this.DescriptionKey, true);
    }
  }

  public string IconPath
  {
    get
    {
      return string.Format("Texture/DragonAltarSkills/{0}", (object) this.Icon);
    }
  }

  public bool NeedSelection
  {
    get
    {
      string targetType = this.TargetType;
      if (targetType != null)
      {
        if (TempleSkillInfo.\u003C\u003Ef__switch\u0024map49 == null)
          TempleSkillInfo.\u003C\u003Ef__switch\u0024map49 = new Dictionary<string, int>(6)
          {
            {
              "ops_alliance_temple",
              0
            },
            {
              "ops_city",
              0
            },
            {
              "area",
              0
            },
            {
              "target_alliance",
              0
            },
            {
              "target",
              0
            },
            {
              "self_hive_ops_city",
              0
            }
          };
        int num;
        if (TempleSkillInfo.\u003C\u003Ef__switch\u0024map49.TryGetValue(targetType, out num) && num == 0)
          return true;
      }
      return false;
    }
  }

  public bool MeetCondition(TileData tile)
  {
    string targetType = this.TargetType;
    if (targetType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (TempleSkillInfo.\u003C\u003Ef__switch\u0024map4A == null)
      {
        // ISSUE: reference to a compiler-generated field
        TempleSkillInfo.\u003C\u003Ef__switch\u0024map4A = new Dictionary<string, int>(6)
        {
          {
            "ops_city",
            0
          },
          {
            "self_hive_ops_city",
            1
          },
          {
            "ops_alliance_temple",
            2
          },
          {
            "area",
            3
          },
          {
            "target_alliance",
            4
          },
          {
            "target",
            5
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (TempleSkillInfo.\u003C\u003Ef__switch\u0024map4A.TryGetValue(targetType, out num))
      {
        switch (num)
        {
          case 0:
            return tile.TileType == TileType.City;
          case 1:
            return tile.TileType == TileType.City;
          case 2:
            return tile.TileType == TileType.AllianceTemple;
          case 3:
            return true;
          case 4:
            if (tile.TileType == TileType.City)
              return true;
            break;
          case 5:
            if (tile.TileType == TileType.City)
              return true;
            break;
        }
      }
    }
    return false;
  }
}
