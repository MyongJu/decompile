﻿// Decompiled with JetBrains decompiler
// Type: QuestManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager
{
  private long _recommendQuest = -1;
  public Dictionary<string, long> datas;

  public event System.Action<long> OnQuestCompleted;

  public event System.Action<long> OnRecommendQuestChanged;

  public event System.Action<int> OnQuestCompletedCountChanged;

  public event System.Action<QuestRewardAgent> OnQuestClaimed;

  public event System.Action<long> OnAllianceQuestStart;

  public event System.Action<long> OnAllianceQuestEnd;

  public Dictionary<string, long>.KeyCollection keys
  {
    get
    {
      return this.datas.Keys;
    }
  }

  public long RecommendQuestID
  {
    get
    {
      return this._recommendQuest;
    }
  }

  public long GetStartingAllianceQuest()
  {
    List<long> totalQuests = DBManager.inst.DB_Local_TimerQuest.GetTotalQuests();
    long num = -1;
    List<long>.Enumerator enumerator = totalQuests.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (DBManager.inst.DB_Local_TimerQuest.Get(enumerator.Current).status == TimerQuestData.QuestStatus.STATUS_ACTIVE)
        num = enumerator.Current;
    }
    return num;
  }

  public void Public_StartAllianceQuest(long questId)
  {
    if (this.OnAllianceQuestStart == null)
      return;
    this.OnAllianceQuestStart(questId);
  }

  public void Public_AllianceQuestEnd(long questId)
  {
    if (this.OnAllianceQuestEnd == null)
      return;
    this.OnAllianceQuestEnd(questId);
  }

  public void Init()
  {
    this.RemoveEventHandler();
    DBManager.inst.DB_Quest.onDataChanged += new System.Action<long>(this.UpdateHandler);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Quest.onDataChanged += new System.Action<long>(this.UpdateHandler);
  }

  private void UpdateHandler(long questId)
  {
    this.NoticeQuestFinish(DBManager.inst.DB_Quest.Get(questId));
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_Quest.onDataChanged -= new System.Action<long>(this.UpdateHandler);
  }

  public void LoadData(object data)
  {
  }

  private void ResetRecommendedQuest()
  {
    QuestData2 recommendQuest = DBManager.inst.DB_Quest.RecommendQuest;
    this._recommendQuest = -1L;
    if (recommendQuest != null && this._recommendQuest != recommendQuest.QuestID)
      this._recommendQuest = recommendQuest.QuestID;
    if (this.OnRecommendQuestChanged == null)
      return;
    this.OnRecommendQuestChanged(this._recommendQuest);
  }

  public void ClaimQuest(long questId)
  {
    QuestData2 quest = DBManager.inst.DB_Quest.Get(questId);
    if (quest.Status != 1)
      return;
    string action = string.Empty;
    TimerQuestData time = DBManager.inst.DB_Local_TimerQuest.Get(questId);
    if (quest != null)
    {
      switch (quest.Type)
      {
        case 1:
        case 2:
          action = "Quest:collectEmpireQuestReward";
          break;
      }
      quest.Claim();
    }
    else if (time != null)
      action = "Quest:collectTimerQuestReward";
    MessageHub.inst.GetPortByAction(action).SendRequest(Utils.Hash((object) "quest_id", (object) questId, (object) "city_id", (object) PlayerData.inst.cityId), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      QuestRewardAgent agent = (QuestRewardAgent) null;
      if (quest != null)
        agent = quest.RewardAgent;
      else if (time != null)
        agent = time.RewardAgent;
      this.SendClaimEventHandler(agent);
      if (this.OnQuestCompletedCountChanged != null)
        this.OnQuestCompletedCountChanged(this.GetCompleteQuestsNumber());
      this.ResetRecommendedQuest();
      AudioManager.Instance.PlaySound("sfx_claim", false);
      RewardsCollectionAnimator.Instance.Clear();
      QuestInfo data = ConfigManager.inst.DB_Quest.GetData((int) questId);
      if (data.Reward_ID_1 != 0)
      {
        int rewardId1 = data.Reward_ID_1;
        int rewardValue1 = data.Reward_Value_1;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardId1);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = (float) rewardValue1,
          icon = itemStaticInfo.ImagePath
        });
      }
      if (data.Reward_ID_2 != 0)
      {
        int rewardId2 = data.Reward_ID_2;
        int rewardValue2 = data.Reward_Value_2;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardId2);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = (float) rewardValue2,
          icon = itemStaticInfo.ImagePath
        });
      }
      if (data.Food_Payout > 0)
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = data.Food_Payout,
          rt = ResRewardsInfo.ResType.Food
        });
      if (data.Silver_Payout > 0)
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = data.Silver_Payout,
          rt = ResRewardsInfo.ResType.Silver
        });
      if (data.Ore_Payout > 0)
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = data.Ore_Payout,
          rt = ResRewardsInfo.ResType.Ore
        });
      if (data.Wood_Payout > 0)
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = data.Wood_Payout,
          rt = ResRewardsInfo.ResType.Wood
        });
      if (data.Gold_PayOut > 0)
        RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
        {
          count = data.Gold_PayOut,
          rt = ResRewardsInfo.ResType.Coin
        });
      RewardsCollectionAnimator.Instance.CollectResource(false);
      RewardsCollectionAnimator.Instance.CollectItems(false);
    }), true);
  }

  public void ClaimGroupQuest(System.Action onSuccess)
  {
    List<QuestData2> groupQuests = DBManager.inst.DB_Quest.GroupQuests;
    if (groupQuests.Count < 1)
      return;
    List<QuestReward> reward = QuestGroupReward.CreateReward((int) groupQuests[0].QuestID);
    if (!DBManager.inst.DB_Quest.IsGroupQuestsComplete())
      return;
    MessageHub.inst.GetPortByAction("Quest:collectGroupQuestReward").SendRequest(Utils.Hash((object) "group_id", (object) DBManager.inst.DB_Quest.CurrentGroupQuestID), (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      AudioManager.Instance.PlaySound("sfx_claim", false);
      RewardsCollectionAnimator.Instance.Clear();
      for (int index = 0; index < reward.Count; ++index)
      {
        ConsumableReward consumableReward = reward[index] as ConsumableReward;
        int internalId = consumableReward.consumableItem.internalId;
        int num = consumableReward.value;
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          count = (float) num,
          icon = itemStaticInfo.ImagePath
        });
      }
      RewardsCollectionAnimator.Instance.CollectResource(false);
      RewardsCollectionAnimator.Instance.CollectItems(false);
      if (onSuccess == null)
        return;
      onSuccess();
    }), true);
  }

  public void SendClaimEventHandler(QuestRewardAgent agent)
  {
    if (this.OnQuestClaimed == null)
      return;
    this.OnQuestClaimed(agent);
  }

  public void PlayEffect(int questId, Transform target, bool playMax = true)
  {
  }

  public void RemoveQuestFromObserver(QuestData2 quest)
  {
  }

  public long GetValue(string key)
  {
    if (key == null)
      return long.MinValue;
    if (this.datas.ContainsKey(key))
      return this.datas[key];
    this.datas.Add(key, 0L);
    return 0;
  }

  public bool SetValue(string key, long value)
  {
    if (!this.datas.ContainsKey(key))
      return false;
    this.datas[key] = value;
    return true;
  }

  public int GetCompleteQuestsNumber()
  {
    return DBManager.inst.DB_Quest.CompletedCount;
  }

  public void NoticeQuestFinish(QuestData2 quest)
  {
    if (quest.Status != 1)
      return;
    new Hashtable()
    {
      {
        (object) "quest_id",
        (object) quest.QuestID
      }
    };
    if (this.OnQuestCompleted != null)
      this.OnQuestCompleted(quest.QuestID);
    if (this.OnQuestCompletedCountChanged != null)
      this.OnQuestCompletedCountChanged(this.GetCompleteQuestsNumber());
    if (quest.QuestID != this._recommendQuest)
      return;
    this.ResetRecommendedQuest();
  }

  public QuestInfo GetData(int internalId)
  {
    return ConfigManager.inst.DB_Quest.GetData(internalId) ?? (QuestInfo) ConfigManager.inst.DB_QuestGroupMain.GetData(internalId);
  }
}
