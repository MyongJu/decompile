﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsMonsterDetailPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MerlinTrialsMonsterDetailPopup : Popup
{
  public UITexture _image;
  public UILabel _troops;
  public UILabel _tips;
  public UILabel _monsterName;
  public GameObject _attackGo;
  public GameObject _checkGo;
  private int _battleId;
  private System.Action<int> _onFocus;
  private System.Action<int> _onBattleFinished;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this._battleId = (orgParam as MerlinTrialsMonsterDetailPopup.Parameter).battleId;
    this._onFocus = (orgParam as MerlinTrialsMonsterDetailPopup.Parameter).onFocus;
    this._onBattleFinished = (orgParam as MerlinTrialsMonsterDetailPopup.Parameter).onBattleFinished;
  }

  public override void OnShow(UIControler.UIParameter orgParam = null)
  {
    base.OnShow(orgParam);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam = null)
  {
    base.OnHide(orgParam);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    MerlinTrialsMainInfo merlinTrialsMainInfo = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._image, merlinTrialsMainInfo.MonsterImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this._troops.text = Utils.XLAT("trial_challenge_troops_num", (object) "0", (object) merlinTrialsMainInfo.initMarchCapacity);
    this._monsterName.text = merlinTrialsMainInfo.LocName;
    switch (MerlinTrialsHelper.inst.GetMonsterStatus(this._battleId))
    {
      case MerlinMonsterStatus.can_attack:
        this._tips.text = string.Empty;
        this._attackGo.SetActive(true);
        this._checkGo.SetActive(false);
        break;
      case MerlinMonsterStatus.defeated:
        this._tips.text = Utils.XLAT("trial_challenge_completed_button");
        this._attackGo.SetActive(true);
        this._checkGo.SetActive(false);
        break;
      case MerlinMonsterStatus.locked:
        this._tips.text = Utils.XLAT("trial_challenge_tip");
        this._attackGo.SetActive(false);
        this._checkGo.SetActive(true);
        break;
    }
  }

  public void OnCloseClick()
  {
    this.Close();
  }

  public void OnAttackClick()
  {
    if (!MerlinTrialsHelper.inst.IsOpenDay(this._battleId))
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_trial_not_open"), (System.Action) null, 4f, false);
    }
    else
    {
      this.Close();
      UIManager.inst.OpenDlg("MerlinTrials/MerlinTrialsMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTrialsMarchAllocDialog.Parameter()
      {
        battleId = this._battleId,
        onBattleFinishCallback = new System.Action<int>(this.OnBattleFinished)
      }, 1 != 0, 1 != 0, 1 != 0);
    }
  }

  public void OnCheckClick()
  {
    if (this._onFocus == null)
      return;
    MerlinTrialsMainInfo merlinTrialsMainInfo1 = ConfigManager.inst.DB_MerlinTrialsMain.Get(this._battleId);
    if (merlinTrialsMainInfo1 == null)
      return;
    MerlinTrialsMainInfo merlinTrialsMainInfo2 = MerlinTrialsHelper.inst.LatestAvaliableTrialConfig(merlinTrialsMainInfo1.trialId);
    if (merlinTrialsMainInfo2 == null)
      return;
    this._onFocus(merlinTrialsMainInfo2.internalId);
    this.Close();
  }

  private void Close()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void OnBattleFinished(int battleId)
  {
    if (this._onBattleFinished == null)
      return;
    this._onBattleFinished(battleId);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int battleId;
    public System.Action<int> onFocus;
    public System.Action<int> onBattleFinished;
  }
}
