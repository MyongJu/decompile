﻿// Decompiled with JetBrains decompiler
// Type: DKArenaScoreRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DKArenaScoreRewardsPopup : Popup
{
  private List<DKArenaScoreRewardItem> list = new List<DKArenaScoreRewardItem>();
  public UILabel title;
  public UILabel score;
  public UIGrid grid;
  public UIScrollView scrollView;
  public DKArenaScoreRewardItem itemPrefab;

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("event_uppercase_score_rewards", true);
    List<DKArenaTitleReward> totals = ConfigManager.inst.DB_DKArenaTitleReward.GetTotals();
    Dictionary<string, string> para = new Dictionary<string, string>();
    long score = DBManager.inst.DB_DKArenaDB.GetCurrentData().score;
    para.Add("0", score.ToString());
    this.score.text = ScriptLocalization.GetWithPara("event_current_score_num", para, true);
    long maxScore = DBManager.inst.DB_DKArenaDB.GetCurrentData().maxScore;
    for (int index = 0; index < totals.Count; ++index)
    {
      if (totals[index].Rewards != null && totals[index].Rewards.rewards.Count > 0)
        this.GetItem().SetData(totals[index].internalId, maxScore);
    }
    this.grid.Reposition();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private DKArenaScoreRewardItem GetItem()
  {
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.itemPrefab.gameObject);
    DKArenaScoreRewardItem component = go.GetComponent<DKArenaScoreRewardItem>();
    component.OnRefreshDelegate = new System.Action(this.OnRefresh);
    NGUITools.SetActive(go, true);
    this.list.Add(component);
    return component;
  }

  public void OnRefresh()
  {
    this.Dispose();
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
    this.AddEventHandler();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
  }

  private void Dispose()
  {
    for (int index = 0; index < this.list.Count; ++index)
    {
      this.list[index].Dispose();
      NGUITools.SetActive(this.list[index].gameObject, false);
    }
    this.list.Clear();
  }
}
