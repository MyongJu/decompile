﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightTalentBuff
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightTalentBuff
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, DragonKnightTalentBuffInfo> _datas;
  private Dictionary<int, DragonKnightTalentBuffInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<DragonKnightTalentBuffInfo, string>(res as Hashtable, "ID", out this._datas, out this._dicByUniqueId);
  }

  public void Clear()
  {
    if (this._datas != null)
    {
      this._datas.Clear();
      this._datas = (Dictionary<string, DragonKnightTalentBuffInfo>) null;
    }
    if (this._dicByUniqueId == null)
      return;
    this._dicByUniqueId.Clear();
    this._dicByUniqueId = (Dictionary<int, DragonKnightTalentBuffInfo>) null;
  }

  public bool Contains(int internalId)
  {
    return this._dicByUniqueId.ContainsKey(internalId);
  }

  public bool Contains(string id)
  {
    return this._datas.ContainsKey(id);
  }

  public DragonKnightTalentBuffInfo GetData(int internalId)
  {
    if (this._dicByUniqueId != null && this._dicByUniqueId.ContainsKey(internalId))
      return this._dicByUniqueId[internalId];
    return (DragonKnightTalentBuffInfo) null;
  }

  public DragonKnightTalentBuffInfo GetData(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (DragonKnightTalentBuffInfo) null;
  }
}
