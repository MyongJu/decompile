﻿// Decompiled with JetBrains decompiler
// Type: DWRequirement
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class DWRequirement
{
  private Dictionary<DWRequirement.RequirementType, int> requires = new Dictionary<DWRequirement.RequirementType, int>();

  public DWRequirement(Dictionary<DWRequirement.RequirementType, int> requires = null)
  {
    if (requires == null)
      return;
    this.requires = requires;
  }

  public void AddRequire(DWRequirement.RequirementType type, int amount)
  {
    this.requires.Add(type, amount);
  }

  public Dictionary<DWRequirement.RequirementType, int> Requirements
  {
    get
    {
      return this.requires;
    }
  }

  public enum RequirementType
  {
    NONE,
    FOOD,
    WOOD,
    SILVER,
    ORE,
    BUILDING,
    ITEM,
    QUEUE,
    LIMIT,
    GOLD,
  }
}
