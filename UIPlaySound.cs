﻿// Decompiled with JetBrains decompiler
// Type: UIPlaySound
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Play Sound")]
public class UIPlaySound : MonoBehaviour
{
  [Range(0.0f, 1f)]
  public float volume = 1f;
  [Range(0.0f, 2f)]
  public float pitch = 1f;
  public AudioClip audioClip;
  public UIPlaySound.Trigger trigger;
  private bool mIsOver;

  private bool canPlay
  {
    get
    {
      if (!this.enabled)
        return false;
      UIButton component = this.GetComponent<UIButton>();
      if (!((Object) component == (Object) null))
        return component.isEnabled;
      return true;
    }
  }

  private void OnEnable()
  {
    if (this.trigger != UIPlaySound.Trigger.OnEnable)
      return;
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  private void OnDisable()
  {
    if (this.trigger != UIPlaySound.Trigger.OnDisable)
      return;
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  private void OnHover(bool isOver)
  {
    if (this.trigger == UIPlaySound.Trigger.OnMouseOver)
    {
      if (this.mIsOver == isOver)
        return;
      this.mIsOver = isOver;
    }
    if (!this.canPlay || (!isOver || this.trigger != UIPlaySound.Trigger.OnMouseOver) && (isOver || this.trigger != UIPlaySound.Trigger.OnMouseOut))
      return;
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  private void OnPress(bool isPressed)
  {
    if (this.trigger == UIPlaySound.Trigger.OnPress)
    {
      if (this.mIsOver == isPressed)
        return;
      this.mIsOver = isPressed;
    }
    if (!this.canPlay || (!isPressed || this.trigger != UIPlaySound.Trigger.OnPress) && (isPressed || this.trigger != UIPlaySound.Trigger.OnRelease))
      return;
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  private void OnClick()
  {
    if (!this.canPlay || this.trigger != UIPlaySound.Trigger.OnClick)
      return;
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  private void OnSelect(bool isSelected)
  {
    if (!this.canPlay || isSelected && UICamera.currentScheme != UICamera.ControlScheme.Controller)
      return;
    this.OnHover(isSelected);
  }

  public void Play()
  {
    NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
  }

  public enum Trigger
  {
    OnClick,
    OnMouseOver,
    OnMouseOut,
    OnPress,
    OnRelease,
    Custom,
    OnEnable,
    OnDisable,
  }
}
