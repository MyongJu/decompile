﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.TSocketClient
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using System.Net.Sockets;

namespace Rtm.Rpc
{
  public class TSocketClient
  {
    public static TDispatchClient dispatcher = new TDispatchClient();
    private int connectionTimeout = 20;
    private bool autoDispatch;
    private TcpClient client;
    private string host;
    private int port;
    private int timeout;
    private NetworkStream stream;

    public TSocketClient(TcpClient client)
    {
      this.client = client;
      if (!this.IsOpen)
        return;
      this.stream = client.GetStream();
    }

    public TSocketClient(string hostport)
      : this(hostport.Split(':')[0], int.Parse(hostport.Split(':')[1]))
    {
    }

    public TSocketClient(string host, int port)
      : this(host, port, 0)
    {
    }

    public TSocketClient()
      : this(0)
    {
    }

    public TSocketClient(int timeout)
    {
      this.autoDispatch = true;
      this.timeout = timeout;
      this.InitSocket();
    }

    protected TSocketClient(string host, int port, int timeout)
    {
      this.host = host;
      this.port = port;
      this.timeout = timeout;
      this.InitSocket();
    }

    public void SetConnectionTimeout(int connTimeout)
    {
      this.connectionTimeout = connTimeout;
    }

    private void InitSocket()
    {
      this.client = new TcpClient();
      TcpClient client = this.client;
      int timeout = this.timeout;
      this.client.SendTimeout = timeout;
      int num = timeout;
      client.ReceiveTimeout = num;
    }

    public int Timeout
    {
      get
      {
        return this.timeout;
      }
      set
      {
        TcpClient client = this.client;
        int num1 = this.timeout = value;
        this.client.SendTimeout = num1;
        int num2 = num1;
        client.ReceiveTimeout = num2;
      }
    }

    public TcpClient TcpClient
    {
      get
      {
        return this.client;
      }
    }

    public string Host
    {
      get
      {
        return this.host;
      }
    }

    public int Port
    {
      get
      {
        return this.port;
      }
    }

    public int Read(byte[] buf, int off, int len)
    {
      int num1 = 0;
      while (num1 < len)
      {
        int num2 = this.stream.Read(buf, off + num1, len - num1);
        if (num2 <= 0)
        {
          this.Close();
          throw new Exception("Cannot read, Remote side has closed");
        }
        num1 += num2;
      }
      return num1;
    }

    public void Write(byte[] buffer, int offset, int size)
    {
      this.stream.Write(buffer, offset, size);
    }

    public void Flush()
    {
      this.stream.Flush();
    }

    public bool IsOpen
    {
      get
      {
        if (this.client == null)
          return false;
        return this.client.Connected;
      }
    }

    public void Open()
    {
      if (this.IsOpen)
        throw new Exception("Socket already connected");
      if (!this.autoDispatch)
      {
        if (string.IsNullOrEmpty(this.host))
          throw new Exception("Cannot open null host");
        if (this.port <= 0)
          throw new Exception("Cannot open without port");
      }
      else
      {
        string str = TSocketClient.dispatcher.Get("rtmGated");
        this.host = str.Split(':')[0];
        this.port = int.Parse(str.Split(':')[1]);
      }
      if (this.client == null)
        this.InitSocket();
      try
      {
        IAsyncResult asyncResult = this.client.BeginConnect(this.host, this.port, (AsyncCallback) null, (object) null);
        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds((double) this.connectionTimeout)))
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection timeout");
        }
        try
        {
          this.client.EndConnect(asyncResult);
          this.stream = this.client.GetStream();
        }
        catch
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection error");
        }
      }
      catch
      {
        IAsyncResult asyncResult;
        try
        {
          IPHostEntry hostEntry = Dns.GetHostEntry(this.host);
          if (hostEntry.AddressList.Length <= 0)
            throw new Exception("Connection error");
          this.client = new TcpClient(AddressFamily.InterNetworkV6);
          string ipv6 = hostEntry.AddressList[0].ToString();
          if (hostEntry.AddressList[0].AddressFamily.ToString() != ProtocolFamily.InterNetworkV6.ToString())
            ipv6 = CommonService.ConvertToIpv6(hostEntry.AddressList[0].ToString());
          asyncResult = this.client.BeginConnect(ipv6, this.port, (AsyncCallback) null, (object) null);
        }
        catch
        {
          throw new Exception("Connection error");
        }
        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds((double) this.connectionTimeout)))
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection timeout");
        }
        try
        {
          this.client.EndConnect(asyncResult);
          this.stream = this.client.GetStream();
        }
        catch
        {
          this.client.Close();
          this.client = (TcpClient) null;
          throw new Exception("Connection error");
        }
      }
    }

    public void Close()
    {
      if (this.client == null)
        return;
      this.stream.Close();
      this.client.Close();
      this.client = (TcpClient) null;
    }
  }
}
