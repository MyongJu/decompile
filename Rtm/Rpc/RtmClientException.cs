﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.RtmClientException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using System;
using Thrift;

namespace Rtm.Rpc
{
  public class RtmClientException : Exception
  {
    protected int code;
    protected string reason;

    protected RtmClientException(int code, string msg)
      : base(msg)
    {
      this.code = code;
      this.reason = msg;
    }

    public RtmClientException(RtmClientException.EType code, string msg)
      : this((int) (7000 + code), msg)
    {
    }

    public RtmClientException()
      : this(0, "Unknown Exception")
    {
    }

    public RtmClientException(rtmGatedException e)
      : this(8000 + e.Code, e.Reason)
    {
    }

    public RtmClientException(TApplicationException e)
      : this((int) (9000 + e.type), e.Message)
    {
    }

    public int Code
    {
      get
      {
        return this.code;
      }
      set
      {
        this.code = value;
      }
    }

    public string Reason
    {
      get
      {
        return this.reason;
      }
      set
      {
        this.reason = value;
      }
    }

    public enum EType
    {
      EConnClosed,
    }
  }
}
