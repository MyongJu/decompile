﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.TRtmClient
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc.Adapter;
using Rtm.Rpc.Container;
using System;
using System.Threading;
using Thrift.Protocol;
using Thrift.Transport;

namespace Rtm.Rpc
{
  public class TRtmClient
  {
    public TThriftClientImpl client;
    private TThriftClientProcessImpl thriftProcessor;
    private TThriftClientCallbackAdapter.Processor callbackProcessor;
    private TAsyncManager asyncMan;

    public TRtmClient(TSocketClient socketClient, RtmClientProcessor iface)
      : this(socketClient, iface, 30000, 5000, 7000)
    {
    }

    public TRtmClient(TSocketClient socketClient, RtmClientProcessor iface, int timeout, int pingInterval, int maxReceiveInterval)
    {
      this.thriftProcessor = new TThriftClientProcessImpl((TThriftClientProcessAdapter) iface);
      this.callbackProcessor = new TThriftClientCallbackAdapter.Processor();
      this.asyncMan = new TAsyncManager(socketClient, new ServerFunc(this._Processor), timeout, pingInterval, maxReceiveInterval);
      this.client = new TThriftClientImpl(new PacketBase(this.asyncMan));
    }

    public TRtmClient(string host, RtmClientProcessor iface)
      : this(host, iface, 30000, 5000, 7000)
    {
    }

    public TRtmClient(string host, RtmClientProcessor iface, int timeout, int pingInterval, int maxReceiveInterval)
      : this(new TSocketClient(host), iface, timeout, pingInterval, maxReceiveInterval)
    {
    }

    public void Connect()
    {
      this.asyncMan.Start();
    }

    public void AsyncConnect(RtmConnectionCallback cb)
    {
      new Thread(new ThreadStart(new TRtmClient.ConnectionThreadWithState(this.asyncMan, cb).ThreadProc)).Start();
    }

    public void Close()
    {
      this.asyncMan.Stop();
    }

    public bool Connected
    {
      get
      {
        return this.asyncMan.Connected;
      }
    }

    protected void _Processor(PacketBase pkg)
    {
      TBinaryProtocol tbinaryProtocol = new TBinaryProtocol((TTransport) pkg);
      switch (pkg.flags)
      {
        case 9:
          this.thriftProcessor.Process((TProtocol) tbinaryProtocol, (TProtocol) tbinaryProtocol);
          break;
        case 999:
          this.thriftProcessor.EventConnectionClosed();
          break;
        default:
          if (pkg.cb != null)
          {
            this.callbackProcessor.Process((TProtocol) tbinaryProtocol, pkg);
            break;
          }
          break;
      }
      pkg.Clear();
      pkg = (PacketBase) null;
    }

    protected class ConnectionThreadWithState
    {
      private RtmConnectionCallback cb;
      private TAsyncManager asyncMan;

      public ConnectionThreadWithState(TAsyncManager asyncMan, RtmConnectionCallback cb)
      {
        this.cb = cb;
        this.asyncMan = asyncMan;
      }

      public void ThreadProc()
      {
        try
        {
          this.asyncMan.Start();
          this.cb(true, "RTM Connected");
        }
        catch (Exception ex)
        {
          this.cb(false, ex.Message.ToString());
        }
      }
    }
  }
}
