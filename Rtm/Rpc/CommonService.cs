﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.CommonService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Security.Cryptography;
using System.Text;

namespace Rtm.Rpc
{
  public static class CommonService
  {
    public const short TRANSPORT_CALL = 1;
    public const short TRANSPORT_REPLAY = 2;
    public const short TRANSPORT_ONEWAY = 4;
    public const short TRANSPORT_PUSH = 9;
    public const short TRANSPORT_EVENT_CLOSED = 999;

    public static byte[] GetI16Byte(short s)
    {
      return new byte[2]
      {
        (byte) ((int) byte.MaxValue & (int) s >> 8),
        (byte) ((uint) byte.MaxValue & (uint) s)
      };
    }

    public static byte[] GetI32Byte(int i32)
    {
      return new byte[4]
      {
        (byte) ((int) byte.MaxValue & i32 >> 24),
        (byte) ((int) byte.MaxValue & i32 >> 16),
        (byte) ((int) byte.MaxValue & i32 >> 8),
        (byte) ((int) byte.MaxValue & i32)
      };
    }

    public static short ReadI16(byte[] i16in)
    {
      return (short) (((int) i16in[0] & (int) byte.MaxValue) << 8 | (int) i16in[1] & (int) byte.MaxValue);
    }

    public static short ReadI16(byte[] i16in, int off)
    {
      return (short) (((int) i16in[off] & (int) byte.MaxValue) << 8 | (int) i16in[off + 1] & (int) byte.MaxValue);
    }

    public static int ReadI32(byte[] i32in)
    {
      return ((int) i32in[0] & (int) byte.MaxValue) << 24 | ((int) i32in[1] & (int) byte.MaxValue) << 16 | ((int) i32in[2] & (int) byte.MaxValue) << 8 | (int) i32in[3] & (int) byte.MaxValue;
    }

    public static int ReadI32(byte[] i32in, int off)
    {
      return ((int) i32in[off] & (int) byte.MaxValue) << 24 | ((int) i32in[off + 1] & (int) byte.MaxValue) << 16 | ((int) i32in[off + 2] & (int) byte.MaxValue) << 8 | (int) i32in[off + 3] & (int) byte.MaxValue;
    }

    public static long GenID()
    {
      int num1 = 8;
      char[] chArray = new char[62];
      char[] charArray = "1234567890".ToCharArray();
      byte[] data1 = new byte[1];
      RNGCryptoServiceProvider cryptoServiceProvider = new RNGCryptoServiceProvider();
      cryptoServiceProvider.GetNonZeroBytes(data1);
      int capacity = num1;
      byte[] data2 = new byte[capacity];
      cryptoServiceProvider.GetNonZeroBytes(data2);
      StringBuilder stringBuilder = new StringBuilder(capacity);
      foreach (byte num2 in data2)
        stringBuilder.Append(charArray[(int) num2 % (charArray.Length - 1)]);
      return long.Parse(((long) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds).ToString() + stringBuilder.ToString());
    }

    public static string ConvertToIpv6(string ip)
    {
      string[] strArray = ip.Split('.');
      if (strArray.Length != 4)
        return string.Empty;
      foreach (string s in strArray)
      {
        if (int.Parse(s) > (int) byte.MaxValue)
          return string.Empty;
      }
      return "64:ff9b::" + Convert.ToString(int.Parse(strArray[0]) * 256 + int.Parse(strArray[1]), 16) + ":" + Convert.ToString(int.Parse(strArray[2]) * 256 + int.Parse(strArray[3]), 16);
    }
  }
}
