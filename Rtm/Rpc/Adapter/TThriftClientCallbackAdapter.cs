﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter.TThriftClientCallbackAdapter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Rtm.Rpc.Container;
using System.Collections.Generic;
using System.IO;
using Thrift;
using Thrift.Protocol;

namespace Rtm.Rpc.Adapter
{
  public class TThriftClientCallbackAdapter : rtmGatedService
  {
    public class Processor
    {
      protected Dictionary<string, TThriftClientCallbackAdapter.Processor.ProcessFunction> processMap_ = new Dictionary<string, TThriftClientCallbackAdapter.Processor.ProcessFunction>();

      public Processor()
      {
        this.processMap_["auth"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_auth_processor);
        this.processMap_["add_variable"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_add_variable_processor);
        this.processMap_["send_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_send_msg_processor);
        this.processMap_["send_msgs"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_send_msgs_processor);
        this.processMap_["send_group_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_send_group_msg_processor);
        this.processMap_["p"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_p_processor);
        this.processMap_["bye"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_bye_processor);
        this.processMap_["friend_changed"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_friend_changed_processor);
        this.processMap_["create_group"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_create_group_processor);
        this.processMap_["join_group"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_join_group_processor);
        this.processMap_["delete_group"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_delete_group_processor);
        this.processMap_["group_changed"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_group_changed_processor);
        this.processMap_["check_offline_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_check_offline_msg_processor);
        this.processMap_["get_history_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_history_msg_processor);
        this.processMap_["get_history_msg_new"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_history_msg_new_processor);
        this.processMap_["get_p2p_history_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_p2p_history_msg_processor);
        this.processMap_["get_p2p_history_msg_new"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_p2p_history_msg_new_processor);
        this.processMap_["get_group_history_msg_new"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_group_history_msg_new_processor);
        this.processMap_["get_online_users"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_online_users_processor);
        this.processMap_["get_group_online_users"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_get_group_online_users_processor);
        this.processMap_["send_broadcast_group_msg"] = new TThriftClientCallbackAdapter.Processor.ProcessFunction(this.recv_send_broadcast_group_msg_processor);
      }

      public bool Process(TProtocol iprot, PacketBase pkg)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          TThriftClientCallbackAdapter.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
            return true;
          if (tmessage.Type == TMessageType.Exception)
          {
            TApplicationException x = TApplicationException.Read(iprot);
            iprot.ReadMessageEnd();
            processFunction(iprot, pkg.cb, x);
          }
          else
            processFunction(iprot, pkg.cb, (TApplicationException) null);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public bool recv_auth(TProtocol iprot)
      {
        rtmGatedService.auth_result authResult = new rtmGatedService.auth_result();
        authResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (authResult.__isset.success)
          return authResult.Success;
        if (authResult.__isset.ex)
          throw authResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "auth failed: unknown result");
      }

      public void recv_auth_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.AuthException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.AuthCallback(this.recv_auth(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.AuthException(new RtmClientException(ex));
          }
        }
      }

      public void recv_add_variable(TProtocol iprot)
      {
        rtmGatedService.add_variable_result addVariableResult = new rtmGatedService.add_variable_result();
        addVariableResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (addVariableResult.__isset.ex)
          throw addVariableResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "add_variable failed: unknown result");
      }

      public void recv_add_variable_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.SetConfigException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.SetConfigCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.SetConfigException(new RtmClientException(ex));
          }
        }
      }

      public int recv_send_msg(TProtocol iprot)
      {
        rtmGatedService.send_msg_result sendMsgResult = new rtmGatedService.send_msg_result();
        sendMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (sendMsgResult.__isset.success)
          return sendMsgResult.Success;
        if (sendMsgResult.__isset.ex)
          throw sendMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_msg failed: unknown result");
      }

      public void recv_send_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.SendException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.SendCallback(this.recv_send_msg(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.SendException(new RtmClientException(ex));
          }
        }
      }

      public int recv_send_msgs(TProtocol iprot)
      {
        rtmGatedService.send_msgs_result sendMsgsResult = new rtmGatedService.send_msgs_result();
        sendMsgsResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (sendMsgsResult.__isset.success)
          return sendMsgsResult.Success;
        if (sendMsgsResult.__isset.ex)
          throw sendMsgsResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_msgs failed: unknown result");
      }

      public void recv_send_msgs_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.SendMultiException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.SendMultiCallback(this.recv_send_msgs(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.SendMultiException(new RtmClientException(ex));
          }
        }
      }

      public int recv_send_group_msg(TProtocol iprot)
      {
        rtmGatedService.send_group_msg_result sendGroupMsgResult = new rtmGatedService.send_group_msg_result();
        sendGroupMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (sendGroupMsgResult.__isset.success)
          return sendGroupMsgResult.Success;
        if (sendGroupMsgResult.__isset.ex)
          throw sendGroupMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_group_msg failed: unknown result");
      }

      public void recv_send_group_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.SendGroupException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.SendGroupCallback(this.recv_send_group_msg(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.SendGroupException(new RtmClientException(ex));
          }
        }
      }

      public void recv_p(TProtocol iprot)
      {
        new rtmGatedService.p_result().Read(iprot);
        iprot.ReadMessageEnd();
      }

      public void recv_p_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.PingException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.PingCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.PingException(new RtmClientException(ex));
          }
        }
      }

      public void recv_bye(TProtocol iprot)
      {
        rtmGatedService.bye_result byeResult = new rtmGatedService.bye_result();
        byeResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (byeResult.__isset.ex)
          throw byeResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "bye failed: unknown result");
      }

      public void recv_bye_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.ByeException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.ByeCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.ByeException(new RtmClientException(ex));
          }
        }
      }

      public void recv_friend_changed(TProtocol iprot)
      {
        rtmGatedService.friend_changed_result friendChangedResult = new rtmGatedService.friend_changed_result();
        friendChangedResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (friendChangedResult.__isset.ex)
          throw friendChangedResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "friend_changed failed: unknown result");
      }

      public void recv_friend_changed_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.FriendChangeException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.FriendChangeCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.FriendChangeException(new RtmClientException(ex));
          }
        }
      }

      public long recv_create_group(TProtocol iprot)
      {
        rtmGatedService.create_group_result createGroupResult = new rtmGatedService.create_group_result();
        createGroupResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (createGroupResult.__isset.success)
          return createGroupResult.Success;
        if (createGroupResult.__isset.ex)
          throw createGroupResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "create_group failed: unknown result");
      }

      public void recv_create_group_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.GroupCreateException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.GroupCreateCallback(this.recv_create_group(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.GroupCreateException(new RtmClientException(ex));
          }
        }
      }

      public long recv_join_group(TProtocol iprot)
      {
        rtmGatedService.join_group_result joinGroupResult = new rtmGatedService.join_group_result();
        joinGroupResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (joinGroupResult.__isset.success)
          return joinGroupResult.Success;
        if (joinGroupResult.__isset.ex)
          throw joinGroupResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "join_group failed: unknown result");
      }

      public void recv_join_group_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.CreateJoinGroupException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.CreateJoinGroupCallback(this.recv_join_group(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.CreateJoinGroupException(new RtmClientException(ex));
          }
        }
      }

      public void recv_delete_group(TProtocol iprot)
      {
        rtmGatedService.delete_group_result deleteGroupResult = new rtmGatedService.delete_group_result();
        deleteGroupResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (deleteGroupResult.__isset.ex)
          throw deleteGroupResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "delete_group failed: unknown result");
      }

      public void recv_delete_group_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.GroupDelException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.GroupDelCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.GroupDelException(new RtmClientException(ex));
          }
        }
      }

      public void recv_group_changed(TProtocol iprot)
      {
        rtmGatedService.group_changed_result groupChangedResult = new rtmGatedService.group_changed_result();
        groupChangedResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (groupChangedResult.__isset.ex)
          throw groupChangedResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "group_changed failed: unknown result");
      }

      public void recv_group_changed_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.GroupChangeException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.GroupChangeCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.GroupChangeException(new RtmClientException(ex));
          }
        }
      }

      public List<MsgNum> recv_check_offline_msg(TProtocol iprot)
      {
        rtmGatedService.check_offline_msg_result offlineMsgResult = new rtmGatedService.check_offline_msg_result();
        offlineMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (offlineMsgResult.__isset.success)
          return offlineMsgResult.Success;
        if (offlineMsgResult.__isset.ex)
          throw offlineMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "check_offline_msg failed: unknown result");
      }

      public void recv_check_offline_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.CheckUnreadException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.CheckUnreadCallback(this.recv_check_offline_msg(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.CheckUnreadException(new RtmClientException(ex));
          }
        }
      }

      public MsgResult recv_get_history_msg(TProtocol iprot)
      {
        rtmGatedService.get_history_msg_result historyMsgResult = new rtmGatedService.get_history_msg_result();
        historyMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (historyMsgResult.__isset.success)
          return historyMsgResult.Success;
        if (historyMsgResult.__isset.ex)
          throw historyMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_history_msg failed: unknown result");
      }

      public void recv_get_history_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.HistoryMsgException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.HistoryMsgCallback(this.recv_get_history_msg(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.HistoryMsgException(new RtmClientException(ex));
          }
        }
      }

      public MsgResult recv_get_history_msg_new(TProtocol iprot)
      {
        rtmGatedService.get_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_history_msg_new_result();
        historyMsgNewResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_history_msg_new failed: unknown result");
      }

      public void recv_get_history_msg_new_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.HistoryMsgNewException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.HistoryMsgNewCallback(this.recv_get_history_msg_new(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.HistoryMsgNewException(new RtmClientException(ex));
          }
        }
      }

      public MsgResult recv_get_p2p_history_msg(TProtocol iprot)
      {
        rtmGatedService.get_p2p_history_msg_result historyMsgResult = new rtmGatedService.get_p2p_history_msg_result();
        historyMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (historyMsgResult.__isset.success)
          return historyMsgResult.Success;
        if (historyMsgResult.__isset.ex)
          throw historyMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_p2p_history_msg failed: unknown result");
      }

      public void recv_get_p2p_history_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.HistoryMsgP2PException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.HistoryMsgP2PCallback(this.recv_get_p2p_history_msg(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.HistoryMsgP2PException(new RtmClientException(ex));
          }
        }
      }

      public MsgResultP2P recv_get_p2p_history_msg_new(TProtocol iprot)
      {
        rtmGatedService.get_p2p_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_p2p_history_msg_new_result();
        historyMsgNewResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_p2p_history_msg_new failed: unknown result");
      }

      public void recv_get_p2p_history_msg_new_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.HistoryMsgP2PNewException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.HistoryMsgP2PNewCallback(this.recv_get_p2p_history_msg_new(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.HistoryMsgP2PNewException(new RtmClientException(ex));
          }
        }
      }

      public MsgResultGroup recv_get_group_history_msg_new(TProtocol iprot)
      {
        rtmGatedService.get_group_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_group_history_msg_new_result();
        historyMsgNewResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_group_history_msg_new failed: unknown result");
      }

      public void recv_get_group_history_msg_new_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.HistoryMsgGroupNewException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.HistoryMsgGroupNewCallback(this.recv_get_group_history_msg_new(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.HistoryMsgGroupNewException(new RtmClientException(ex));
          }
        }
      }

      public List<long> recv_get_online_users(TProtocol iprot)
      {
        rtmGatedService.get_online_users_result onlineUsersResult = new rtmGatedService.get_online_users_result();
        onlineUsersResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (onlineUsersResult.__isset.success)
          return onlineUsersResult.Success;
        if (onlineUsersResult.__isset.ex)
          throw onlineUsersResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_online_users failed: unknown result");
      }

      public void recv_get_online_users_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.GetOnlineUsersException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.GetOnlineUsersCallback(this.recv_get_online_users(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.GetOnlineUsersException(new RtmClientException(ex));
          }
        }
      }

      public List<long> recv_get_group_online_users(TProtocol iprot)
      {
        rtmGatedService.get_group_online_users_result onlineUsersResult = new rtmGatedService.get_group_online_users_result();
        onlineUsersResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (onlineUsersResult.__isset.success)
          return onlineUsersResult.Success;
        if (onlineUsersResult.__isset.ex)
          throw onlineUsersResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_group_online_users failed: unknown result");
      }

      public void recv_get_group_online_users_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.GetGroupOnlineUsersException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.GetGroupOnlineUsersCallback(this.recv_get_group_online_users(iprot));
          }
          catch (rtmGatedException ex)
          {
            cb.GetGroupOnlineUsersException(new RtmClientException(ex));
          }
        }
      }

      public void recv_send_broadcast_group_msg(TProtocol iprot)
      {
        rtmGatedService.send_broadcast_group_msg_result broadcastGroupMsgResult = new rtmGatedService.send_broadcast_group_msg_result();
        broadcastGroupMsgResult.Read(iprot);
        iprot.ReadMessageEnd();
        if (broadcastGroupMsgResult.__isset.ex)
          throw broadcastGroupMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_broadcast_group_msg failed: unknown result");
      }

      public void recv_send_broadcast_group_msg_processor(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x)
      {
        if (x != null)
        {
          cb.SendBroadcastGroupMsgException(new RtmClientException(x));
        }
        else
        {
          try
          {
            cb.SendBroadcastGroupMsgCallback();
          }
          catch (rtmGatedException ex)
          {
            cb.SendBroadcastGroupMsgException(new RtmClientException(ex));
          }
        }
      }

      protected delegate void ProcessFunction(TProtocol iprot, _RtmClientCallBack cb, TApplicationException x);
    }
  }
}
