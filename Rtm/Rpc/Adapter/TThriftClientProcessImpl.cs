﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter.TThriftClientProcessImpl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;

namespace Rtm.Rpc.Adapter
{
  public class TThriftClientProcessImpl : rtmGatedPushService.Processor
  {
    protected TThriftClientProcessAdapter adapterIface;

    public TThriftClientProcessImpl(TThriftClientProcessAdapter iface)
      : base((rtmGatedPushService.Iface) iface)
    {
      this.adapterIface = iface;
    }

    public void EventConnectionClosed()
    {
      this.adapterIface.EventConnectionClosed();
    }
  }
}
