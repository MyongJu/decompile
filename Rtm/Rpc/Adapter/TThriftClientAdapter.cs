﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter.TThriftClientAdapter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Rtm.Rpc.Container;
using System.Collections.Generic;
using Thrift.Protocol;
using Thrift.Transport;

namespace Rtm.Rpc.Adapter
{
  public class TThriftClientAdapter : rtmGatedService.Client
  {
    private object writeLocker = new object();
    private PacketBase pkg;

    public TThriftClientAdapter(PacketBase pkg)
      : base((TProtocol) new TBinaryProtocol((TTransport) pkg))
    {
      this.pkg = pkg;
    }

    public int auth(int project_id, long uid, string auth_token, string version, Dictionary<string, string> kv)
    {
      return this.auth(project_id, uid, auth_token, version, kv, (_RtmClientCallBack) null);
    }

    public int auth(int project_id, long uid, string auth_token, string version, Dictionary<string, string> kv, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "Auth";
        seqNum = this.pkg.seqNum;
        this.send_auth(project_id, uid, auth_token, version, kv);
      }
      return seqNum;
    }

    public int add_variable(Dictionary<string, string> kv)
    {
      return this.add_variable(kv, (_RtmClientCallBack) null);
    }

    public int add_variable(Dictionary<string, string> kv, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "SetConfig";
        seqNum = this.pkg.seqNum;
        this.send_add_variable(kv);
      }
      return seqNum;
    }

    public new int send_msg(long to, long mid, byte mtype, string message)
    {
      return this.send_msg(to, mid, mtype, message, (_RtmClientCallBack) null);
    }

    public int send_msg(long to, long mid, byte mtype, string message, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "Send";
        seqNum = this.pkg.seqNum;
        this.send_send_msg(to, mid, mtype, message);
      }
      return seqNum;
    }

    public new int send_msgs(List<long> tos, long mid, byte mtype, string message)
    {
      return this.send_msgs(tos, mid, mtype, message, (_RtmClientCallBack) null);
    }

    public int send_msgs(List<long> tos, long mid, byte mtype, string message, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "SendMulti";
        seqNum = this.pkg.seqNum;
        this.send_send_msgs(tos, mid, mtype, message);
      }
      return seqNum;
    }

    public new int send_group_msg(long group_id, long mid, byte mtype, string message)
    {
      return this.send_group_msg(group_id, mid, mtype, message, (_RtmClientCallBack) null);
    }

    public int send_group_msg(long group_id, long mid, byte mtype, string message, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "SendGroup";
        seqNum = this.pkg.seqNum;
        this.send_send_group_msg(group_id, mid, mtype, message);
      }
      return seqNum;
    }

    public int p()
    {
      return this.p((_RtmClientCallBack) null);
    }

    public int p(_RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "Ping";
        seqNum = this.pkg.seqNum;
        this.send_p();
      }
      return seqNum;
    }

    public int bye()
    {
      return this.bye((_RtmClientCallBack) null);
    }

    public int bye(_RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "Bye";
        seqNum = this.pkg.seqNum;
        this.send_bye();
      }
      return seqNum;
    }

    public int friend_changed(byte otype, List<friendPair> uid_pair)
    {
      return this.friend_changed(otype, uid_pair, (_RtmClientCallBack) null);
    }

    public int friend_changed(byte otype, List<friendPair> uid_pair, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "FriendChange";
        seqNum = this.pkg.seqNum;
        this.send_friend_changed(otype, uid_pair);
      }
      return seqNum;
    }

    public int create_group(long group_id, string group_name)
    {
      return this.create_group(group_id, group_name, (_RtmClientCallBack) null);
    }

    public int create_group(long group_id, string group_name, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "GroupCreate";
        seqNum = this.pkg.seqNum;
        this.send_create_group(group_id, group_name);
      }
      return seqNum;
    }

    public int join_group(long group_id)
    {
      return this.join_group(group_id, (_RtmClientCallBack) null);
    }

    public int join_group(long group_id, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "CreateJoinGroup";
        seqNum = this.pkg.seqNum;
        this.send_join_group(group_id);
      }
      return seqNum;
    }

    public int delete_group(long group_id)
    {
      return this.delete_group(group_id, (_RtmClientCallBack) null);
    }

    public int delete_group(long group_id, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "GroupDel";
        seqNum = this.pkg.seqNum;
        this.send_delete_group(group_id);
      }
      return seqNum;
    }

    public int group_changed(byte otype, long group_id, List<long> uids)
    {
      return this.group_changed(otype, group_id, uids, (_RtmClientCallBack) null);
    }

    public int group_changed(byte otype, long group_id, List<long> uids, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "GroupChange";
        seqNum = this.pkg.seqNum;
        this.send_group_changed(otype, group_id, uids);
      }
      return seqNum;
    }

    public int check_offline_msg()
    {
      return this.check_offline_msg((_RtmClientCallBack) null);
    }

    public int check_offline_msg(_RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "CheckUnread";
        seqNum = this.pkg.seqNum;
        this.send_check_offline_msg();
      }
      return seqNum;
    }

    public int get_history_msg(MsgParam param_list)
    {
      return this.get_history_msg(param_list, (_RtmClientCallBack) null);
    }

    public int get_history_msg(MsgParam param_list, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "HistoryMsg";
        seqNum = this.pkg.seqNum;
        this.send_get_history_msg(param_list);
      }
      return seqNum;
    }

    public int get_history_msg_new(MsgParamNew param_list)
    {
      return this.get_history_msg_new(param_list, (_RtmClientCallBack) null);
    }

    public int get_history_msg_new(MsgParamNew param_list, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "HistoryMsgNew";
        seqNum = this.pkg.seqNum;
        this.send_get_history_msg_new(param_list);
      }
      return seqNum;
    }

    public int get_p2p_history_msg(MsgParam param_list)
    {
      return this.get_p2p_history_msg(param_list, (_RtmClientCallBack) null);
    }

    public int get_p2p_history_msg(MsgParam param_list, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "HistoryMsgP2P";
        seqNum = this.pkg.seqNum;
        this.send_get_p2p_history_msg(param_list);
      }
      return seqNum;
    }

    public int get_p2p_history_msg_new(MsgParamP2P param_list)
    {
      return this.get_p2p_history_msg_new(param_list, (_RtmClientCallBack) null);
    }

    public int get_p2p_history_msg_new(MsgParamP2P param_list, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "HistoryMsgP2PNew";
        seqNum = this.pkg.seqNum;
        this.send_get_p2p_history_msg_new(param_list);
      }
      return seqNum;
    }

    public int get_group_history_msg_new(MsgParamGroup param_list)
    {
      return this.get_group_history_msg_new(param_list, (_RtmClientCallBack) null);
    }

    public int get_group_history_msg_new(MsgParamGroup param_list, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "HistoryMsgGroupNew";
        seqNum = this.pkg.seqNum;
        this.send_get_group_history_msg_new(param_list);
      }
      return seqNum;
    }

    public int get_online_users(List<long> uids)
    {
      return this.get_online_users(uids, (_RtmClientCallBack) null);
    }

    public int get_online_users(List<long> uids, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "GetOnlineUsers";
        seqNum = this.pkg.seqNum;
        this.send_get_online_users(uids);
      }
      return seqNum;
    }

    public int get_group_online_users(long group_id)
    {
      return this.get_group_online_users(group_id, (_RtmClientCallBack) null);
    }

    public int get_group_online_users(long group_id, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "GetGroupOnlineUsers";
        seqNum = this.pkg.seqNum;
        this.send_get_group_online_users(group_id);
      }
      return seqNum;
    }

    public int send_broadcast_group_msg(long group_id, long mid, byte mtype, string message)
    {
      return this.send_broadcast_group_msg(group_id, mid, mtype, message, (_RtmClientCallBack) null);
    }

    public int send_broadcast_group_msg(long group_id, long mid, byte mtype, string message, _RtmClientCallBack cb)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 1;
        this.pkg.cb = cb;
        this.pkg.CallName = "SendBroadcastGroupMsg";
        seqNum = this.pkg.seqNum;
        this.send_send_broadcast_group_msg(group_id, mid, mtype, message);
      }
      return seqNum;
    }

    public int send_note(long to, byte mtype, string message)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 4;
        this.pkg.cb = (_RtmClientCallBack) null;
        seqNum = this.pkg.seqNum;
        this.send_send_note(to, mtype, message);
      }
      return seqNum;
    }

    public int send_notes(List<long> tos, byte mtype, string message)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 4;
        this.pkg.cb = (_RtmClientCallBack) null;
        seqNum = this.pkg.seqNum;
        this.send_send_notes(tos, mtype, message);
      }
      return seqNum;
    }

    public int send_group_note(long group_id, byte mtype, string message)
    {
      int seqNum;
      lock (this.writeLocker)
      {
        this.pkg.flags = (short) 4;
        this.pkg.cb = (_RtmClientCallBack) null;
        seqNum = this.pkg.seqNum;
        this.send_send_group_note(group_id, mtype, message);
      }
      return seqNum;
    }
  }
}
