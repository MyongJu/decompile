﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter._RtmClientCallBack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using System.Collections.Generic;

namespace Rtm.Rpc.Adapter
{
  public class _RtmClientCallBack
  {
    public virtual void AuthCallback(bool result)
    {
    }

    public virtual void AuthException(RtmClientException e)
    {
    }

    public virtual void SetConfigCallback()
    {
    }

    public virtual void SetConfigException(RtmClientException e)
    {
    }

    public virtual void SendCallback(int result)
    {
    }

    public virtual void SendException(RtmClientException e)
    {
    }

    public virtual void SendMultiCallback(int result)
    {
    }

    public virtual void SendMultiException(RtmClientException e)
    {
    }

    public virtual void SendGroupCallback(int result)
    {
    }

    public virtual void SendGroupException(RtmClientException e)
    {
    }

    public virtual void PingCallback()
    {
    }

    public virtual void PingException(RtmClientException e)
    {
    }

    public virtual void ByeCallback()
    {
    }

    public virtual void ByeException(RtmClientException e)
    {
    }

    public virtual void FriendChangeCallback()
    {
    }

    public virtual void FriendChangeException(RtmClientException e)
    {
    }

    public virtual void GroupCreateCallback(long result)
    {
    }

    public virtual void GroupCreateException(RtmClientException e)
    {
    }

    public virtual void CreateJoinGroupCallback(long result)
    {
    }

    public virtual void CreateJoinGroupException(RtmClientException e)
    {
    }

    public virtual void GroupDelCallback()
    {
    }

    public virtual void GroupDelException(RtmClientException e)
    {
    }

    public virtual void GroupChangeCallback()
    {
    }

    public virtual void GroupChangeException(RtmClientException e)
    {
    }

    public virtual void CheckUnreadCallback(List<MsgNum> result)
    {
    }

    public virtual void CheckUnreadException(RtmClientException e)
    {
    }

    public virtual void HistoryMsgCallback(MsgResult result)
    {
    }

    public virtual void HistoryMsgException(RtmClientException e)
    {
    }

    public virtual void HistoryMsgNewCallback(MsgResult result)
    {
    }

    public virtual void HistoryMsgNewException(RtmClientException e)
    {
    }

    public virtual void HistoryMsgP2PCallback(MsgResult result)
    {
    }

    public virtual void HistoryMsgP2PException(RtmClientException e)
    {
    }

    public virtual void HistoryMsgP2PNewCallback(MsgResultP2P result)
    {
    }

    public virtual void HistoryMsgP2PNewException(RtmClientException e)
    {
    }

    public virtual void HistoryMsgGroupNewCallback(MsgResultGroup result)
    {
    }

    public virtual void HistoryMsgGroupNewException(RtmClientException e)
    {
    }

    public virtual void GetOnlineUsersCallback(List<long> result)
    {
    }

    public virtual void GetOnlineUsersException(RtmClientException e)
    {
    }

    public virtual void GetGroupOnlineUsersCallback(List<long> result)
    {
    }

    public virtual void GetGroupOnlineUsersException(RtmClientException e)
    {
    }

    public virtual void SendBroadcastGroupMsgCallback()
    {
    }

    public virtual void SendBroadcastGroupMsgException(RtmClientException e)
    {
    }
  }
}
