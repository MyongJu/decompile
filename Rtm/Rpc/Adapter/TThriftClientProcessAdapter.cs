﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter.TThriftClientProcessAdapter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using System.Collections.Generic;

namespace Rtm.Rpc.Adapter
{
  public abstract class TThriftClientProcessAdapter : rtmGatedPushService.Iface
  {
    public virtual void EventConnectionClosed()
    {
    }

    public virtual void Bye()
    {
    }

    public void bye()
    {
      this.Bye();
    }

    public virtual void KickedOut()
    {
    }

    public void kickout()
    {
      this.KickedOut();
    }

    public virtual void RecvNotify(byte mtype, long from, string message, int time)
    {
    }

    public void push_note(byte mtype, long from, string message, int time)
    {
      this.RecvNotify(mtype, from, message, time);
    }

    public virtual void RecvNotifyMulti(List<long> members, byte mtype, long from, string message, int time)
    {
    }

    public void push_notes(List<long> members, byte mtype, long from, string message, int time)
    {
      this.RecvNotifyMulti(members, mtype, from, message, time);
    }

    public virtual void RecvNotifyGroup(byte mtype, long group_id, long from, string message, int time)
    {
    }

    public void push_group_note(byte mtype, long group_id, long from, string message, int time)
    {
      this.RecvNotifyGroup(mtype, group_id, from, message, time);
    }

    public virtual void RecvMessage(long from, byte mtype, string message, long mid, int time)
    {
    }

    public void push_msg(long from, byte mtype, string message, long mid, int time)
    {
      this.RecvMessage(from, mtype, message, mid, time);
    }

    public virtual void RecvMessageMulti(List<long> members, long from, byte mtype, string message, long mid, int time)
    {
    }

    public void push_msgs(List<long> members, long from, byte mtype, string message, long mid, int time)
    {
      this.RecvMessageMulti(members, from, mtype, message, mid, time);
    }

    public virtual void RecvMessageGroup(long group_id, long from, byte mtype, string message, long mid, int time)
    {
    }

    public void push_group_msg(long group_id, long from, byte mtype, string message, long mid, int time)
    {
      this.RecvMessageGroup(group_id, from, mtype, message, mid, time);
    }

    public virtual void UserStatus(Dictionary<long, bool> status)
    {
    }

    public void user_status(Dictionary<long, bool> status)
    {
      this.UserStatus(status);
    }

    public virtual void RecvReceipt(long from, long mid, byte state)
    {
    }

    public void msg_receipt(long from, long mid, byte state)
    {
      this.RecvReceipt(from, mid, state);
    }

    public virtual void RecvMessageBroadcast(long from, byte mtype, string message, long mid, int time)
    {
    }

    public void push_broadcast_msg(long from, byte mtype, string message, long mid, int time)
    {
      this.RecvMessageBroadcast(from, mtype, message, mid, time);
    }

    public virtual void RecvNotifyBroadcast(long from, byte mtype, string message, int time)
    {
    }

    public void push_broadcast_note(long from, byte mtype, string message, int time)
    {
      this.RecvNotifyBroadcast(from, mtype, message, time);
    }
  }
}
