﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Adapter.TThriftClientImpl
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Rtm.Rpc.Container;
using System.Collections.Generic;

namespace Rtm.Rpc.Adapter
{
  public class TThriftClientImpl
  {
    public static string version = "c#/unity,1.0 beta,20141223";
    public Dictionary<string, string> settings = new Dictionary<string, string>();
    private TThriftClientAdapter adp;

    public TThriftClientImpl(PacketBase pkg)
    {
      this.adp = new TThriftClientAdapter(pkg);
    }

    public int Auth(int project_id, long uid, string auth_token, _RtmClientCallBack cb)
    {
      return this.adp.auth(project_id, uid, auth_token, TThriftClientImpl.version, this.settings, cb);
    }

    public int Ping(_RtmClientCallBack cb)
    {
      return this.adp.p(cb);
    }

    public int Logout(_RtmClientCallBack cb)
    {
      return this.adp.bye(cb);
    }

    public int GetOnlineUsers(List<long> uids, _RtmClientCallBack cb)
    {
      return this.adp.get_online_users(uids, cb);
    }

    public int Send(long to, byte mtype, string message, _RtmClientCallBack cb)
    {
      return this.adp.send_msg(to, CommonService.GenID(), mtype, message, cb);
    }

    public int SendMulti(List<long> tos, byte mtype, string message, _RtmClientCallBack cb)
    {
      return this.adp.send_msgs(tos, CommonService.GenID(), mtype, message, cb);
    }

    public int SendGroup(long to, byte mtype, string message, _RtmClientCallBack cb)
    {
      return this.adp.send_group_msg(to, CommonService.GenID(), mtype, message, cb);
    }

    public int Notify(long to, byte mtype, string message)
    {
      return this.adp.send_note(to, mtype, message);
    }

    public int NotifyMulti(List<long> tos, byte mtype, string message)
    {
      return this.adp.send_notes(tos, mtype, message);
    }

    public int NotifyGroup(long to, byte mtype, string message)
    {
      return this.adp.send_group_note(to, mtype, message);
    }

    public int FriendAdd(List<friendPair> uid_pair, _RtmClientCallBack cb)
    {
      return this.adp.friend_changed((byte) 1, uid_pair, cb);
    }

    public int FriendRemove(List<friendPair> uid_pair, _RtmClientCallBack cb)
    {
      return this.adp.friend_changed((byte) 2, uid_pair, cb);
    }

    public int GroupCreate(_RtmClientCallBack cb)
    {
      return this.adp.create_group(0L, string.Empty, cb);
    }

    public int GroupCreate(long group_id, _RtmClientCallBack cb)
    {
      return this.adp.create_group(group_id, string.Empty, cb);
    }

    public int GroupDel(long group_id, _RtmClientCallBack cb)
    {
      return this.adp.delete_group(group_id, cb);
    }

    public int GroupJoin(long group_id, List<long> uids, _RtmClientCallBack cb)
    {
      return this.adp.group_changed((byte) 1, group_id, uids, cb);
    }

    public int GroupCreateJoin(long group_id, _RtmClientCallBack cb)
    {
      return this.adp.join_group(group_id, cb);
    }

    public int GroupLeave(long group_id, List<long> uids, _RtmClientCallBack cb)
    {
      return this.adp.group_changed((byte) 2, group_id, uids, cb);
    }

    public int GetGroupOnlineUsers(long group_id, _RtmClientCallBack cb)
    {
      return this.adp.get_group_online_users(group_id, cb);
    }

    public int CheckUnread(_RtmClientCallBack cb)
    {
      return this.adp.check_offline_msg(cb);
    }

    public int HistoryMsg(MsgParam param_list, _RtmClientCallBack cb)
    {
      return this.adp.get_history_msg(param_list, cb);
    }

    public int HistoryMsgNew(MsgParamNew param_list, _RtmClientCallBack cb)
    {
      return this.adp.get_history_msg_new(param_list, cb);
    }

    public int HistoryMsgP2P(MsgParam param_list, _RtmClientCallBack cb)
    {
      return this.adp.get_p2p_history_msg(param_list, cb);
    }

    public int HistoryMsgP2PNew(MsgParamP2P param_list, _RtmClientCallBack cb)
    {
      return this.adp.get_p2p_history_msg_new(param_list, cb);
    }

    public int HistoryMsgGroupNew(MsgParamGroup param_list, _RtmClientCallBack cb)
    {
      return this.adp.get_group_history_msg_new(param_list, cb);
    }
  }
}
