﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.TAsyncManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc.Container;
using System;
using System.Threading;
using System.Timers;

namespace Rtm.Rpc
{
  public class TAsyncManager
  {
    private static byte[] RAW_PING_BYTES = new byte[32]
    {
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 28,
      (byte) 15,
      byte.MaxValue,
      (byte) 0,
      (byte) 1,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 1,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 128,
      (byte) 1,
      (byte) 0,
      (byte) 1,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 1,
      (byte) 112,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 0,
      (byte) 0
    };
    private object opLocker = new object();
    private object connCloseLocker = new object();
    private object sendLocker = new object();
    private ManualResetEvent stopSignal = new ManualResetEvent(false);
    private AutoResetEvent pingStoppedSignal = new AutoResetEvent(true);
    private AutoResetEvent recvStoppedSignal = new AutoResetEvent(true);
    private byte[] i16in = new byte[2];
    private byte[] i32in = new byte[4];
    private TSocketClient socketClient;
    private Thread receiveThreadHandler;
    private Thread pingThreadHandler;
    private int maxPos;
    private ServerFunc ReceiveCallback;
    private CallbackDictionary cbContainer;
    protected int aliveTimeout;
    private long lastReceiveTime;
    private System.Timers.Timer timer;
    private int maxReceiveTime;

    public TAsyncManager(TSocketClient socketClient, ServerFunc ReceiveCallback)
      : this(socketClient, ReceiveCallback, 0, 5000, 7000)
    {
      this.timer = new System.Timers.Timer();
    }

    public TAsyncManager(TSocketClient socketClient, ServerFunc ReceiveCallback, int timeout, int pingInterval, int maxReceiveInterval)
    {
      this.timer = new System.Timers.Timer();
      this.ReceiveCallback = ReceiveCallback;
      this.socketClient = socketClient;
      this.socketClient.Timeout = timeout;
      this.aliveTimeout = pingInterval;
      this.maxReceiveTime = maxReceiveInterval;
      this.lastReceiveTime = this.getMillisecond();
      this.cbContainer = new CallbackDictionary();
    }

    private long getMillisecond()
    {
      return (long) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }

    private short ReadI16()
    {
      this.socketClient.Read(this.i16in, 0, 2);
      return (short) (((int) this.i16in[0] & (int) byte.MaxValue) << 8 | (int) this.i16in[1] & (int) byte.MaxValue);
    }

    private int ReadI32()
    {
      this.socketClient.Read(this.i32in, 0, 4);
      return ((int) this.i32in[0] & (int) byte.MaxValue) << 24 | ((int) this.i32in[1] & (int) byte.MaxValue) << 16 | ((int) this.i32in[2] & (int) byte.MaxValue) << 8 | (int) this.i32in[3] & (int) byte.MaxValue;
    }

    private PacketBase ReadPackage()
    {
      PacketBase packetBase = new PacketBase(this);
      this.maxPos = this.ReadI32();
      byte[] numArray = new byte[this.maxPos];
      this.socketClient.Read(numArray, 0, numArray.Length);
      packetBase.inBuffer.Write(this.i32in, 0, 4);
      packetBase.inBuffer.Write(numArray, 0, numArray.Length);
      packetBase.DecodePackage();
      packetBase.cb = this.cbContainer.Get(packetBase.seqNum);
      this.lastReceiveTime = this.getMillisecond();
      return packetBase;
    }

    public bool Connected
    {
      get
      {
        return this.socketClient.IsOpen;
      }
    }

    private void ReceiveThread()
    {
      try
      {
        while (!this.stopSignal.WaitOne(0))
          this.ReceiveCallback(this.ReadPackage());
      }
      catch (Exception ex)
      {
        this.StopSignal();
      }
      this.cbContainer.ExceptionFlush();
      this.ReceiveCallback(new PacketBase()
      {
        flags = (short) 999
      });
      this.recvStoppedSignal.Set();
    }

    private void PingThread()
    {
      int millisecondsTimeout = this.aliveTimeout > 0 ? this.aliveTimeout : -1;
      try
      {
        while (!this.stopSignal.WaitOne(millisecondsTimeout))
        {
          lock (this.sendLocker)
          {
            this.socketClient.Write(TAsyncManager.RAW_PING_BYTES, 0, TAsyncManager.RAW_PING_BYTES.Length);
            this.socketClient.Flush();
          }
        }
      }
      catch (Exception ex)
      {
        this.StopSignal();
      }
      this.pingStoppedSignal.Set();
    }

    private void startCheckLastPing()
    {
      this.timer.Elapsed += new ElapsedEventHandler(this.checkLastPing);
      this.timer.Interval = (double) (this.aliveTimeout / 2);
      this.timer.Start();
      this.timer.Enabled = true;
    }

    private void checkLastPing(object source, ElapsedEventArgs e)
    {
      if (this.getMillisecond() - this.lastReceiveTime <= (long) this.maxReceiveTime)
        return;
      this.StopSignal();
    }

    public void Start()
    {
      lock (this.opLocker)
      {
        if (this.socketClient.IsOpen)
          return;
        this.recvStoppedSignal.WaitOne();
        this.pingStoppedSignal.WaitOne();
        try
        {
          this.socketClient.Open();
        }
        catch (Exception ex)
        {
          this.recvStoppedSignal.Set();
          this.pingStoppedSignal.Set();
          throw ex;
        }
        this.stopSignal.Reset();
        this.receiveThreadHandler = new Thread(new ThreadStart(this.ReceiveThread));
        this.receiveThreadHandler.Start();
        this.pingThreadHandler = new Thread(new ThreadStart(this.PingThread));
        this.pingThreadHandler.Start();
        this.startCheckLastPing();
      }
    }

    private void StopSignal()
    {
      this.timer.Stop();
      lock (this.connCloseLocker)
      {
        this.stopSignal.Set();
        this.socketClient.Close();
      }
    }

    public void Stop()
    {
      lock (this.opLocker)
      {
        this.StopSignal();
        if (this.receiveThreadHandler != null)
          this.receiveThreadHandler.Join();
        if (this.pingThreadHandler == null)
          return;
        this.pingThreadHandler.Join();
      }
    }

    public void InvokeException(PacketBase pkg)
    {
      new Thread(new ThreadStart(new TAsyncManager.ExceptionInvokeThread(pkg).ThreadProc)).Start();
    }

    public void Send(PacketBase pkg)
    {
      pkg.EncodePackage();
      byte[] array = pkg.outBuffer.ToArray();
      try
      {
        lock (this.sendLocker)
        {
          this.socketClient.Write(array, 0, array.Length);
          this.socketClient.Flush();
          if (pkg.cb != null)
            this.cbContainer.Put(pkg.seqNum, pkg.CallName, pkg.cb);
          pkg.Clear();
        }
      }
      catch (Exception ex)
      {
        this.InvokeException(pkg);
      }
    }

    protected class ExceptionInvokeThread
    {
      private PacketBase pkg;

      public ExceptionInvokeThread(PacketBase pkg)
      {
        this.pkg = pkg;
      }

      public void ThreadProc()
      {
        CallbackDictionary.ProcPackageWithException(this.pkg.CallName, this.pkg.cb);
        this.pkg.Clear();
      }
    }
  }
}
