﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.TDispatchClient
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Thrift.Protocol;
using Thrift.Transport;

namespace Rtm.Rpc
{
  public class TDispatchClient
  {
    private const string DISPATCH_TYPE = "rtmGated";
    private const string DISPATCH_PROTO = "tcp";
    private const string DISPATCH_VERSION = "1.2";
    private string dispServer;
    private int dispPort;
    private TTransport socket;
    private DispatchService.Client cli;

    public TDispatchClient()
      : this("dispatch.test.rtm.infra.funplus.net")
    {
    }

    public TDispatchClient(string host)
      : this(host, 13011)
    {
    }

    public TDispatchClient(string host, int port)
    {
      this.dispServer = host;
      this.dispPort = port;
      this.socket = (TTransport) new TSocket(this.dispServer, this.dispPort);
      this.cli = new DispatchService.Client((TProtocol) new TBinaryProtocol(this.socket));
    }

    public TDispatchClient(string host, int port, int connectionTimeout)
    {
      this.dispServer = host;
      this.dispPort = port;
      this.socket = (TTransport) new TSocket(this.dispServer, this.dispPort, 0, connectionTimeout);
      this.cli = new DispatchService.Client((TProtocol) new TBinaryProtocol(this.socket));
    }

    public string Get(string type = "rtmGated")
    {
      if (!this.socket.IsOpen)
        this.socket.Open();
      string str = this.cli.which(type, "tcp", "1.2");
      this.socket.Close();
      return str;
    }
  }
}
