﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Container.CallbackDictionary
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc.Adapter;
using System.Collections.Generic;

namespace Rtm.Rpc.Container
{
  public class CallbackDictionary
  {
    private object accessLock = new object();
    private Dictionary<int, CallbackDictionary.aCallback> dict;

    public CallbackDictionary()
    {
      this.dict = new Dictionary<int, CallbackDictionary.aCallback>();
    }

    public void Put(int seqId, string name, _RtmClientCallBack cb)
    {
      CallbackDictionary.aCallback aCallback = new CallbackDictionary.aCallback(name, cb);
      lock (this.accessLock)
        this.dict.Add(seqId, aCallback);
    }

    public _RtmClientCallBack Get(int seqId)
    {
      _RtmClientCallBack rtmClientCallBack;
      lock (this.accessLock)
      {
        CallbackDictionary.aCallback aCallback;
        if (!this.dict.TryGetValue(seqId, out aCallback))
        {
          rtmClientCallBack = (_RtmClientCallBack) null;
        }
        else
        {
          this.dict.Remove(seqId);
          rtmClientCallBack = aCallback.cb;
        }
      }
      return rtmClientCallBack;
    }

    public void ExceptionFlush()
    {
      lock (this.accessLock)
      {
        using (Dictionary<int, CallbackDictionary.aCallback>.ValueCollection.Enumerator enumerator = this.dict.Values.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            CallbackDictionary.aCallback current = enumerator.Current;
            current.cb.GetType().GetMethod(current.InvokeName + "Exception").Invoke((object) current.cb, new object[1]
            {
              (object) new RtmClientException(RtmClientException.EType.EConnClosed, "Since the RTM connection has broken, we have to flush all on-the-fly requests with exception.")
            });
          }
        }
        this.dict.Clear();
      }
    }

    public static void ProcPackageWithException(string name, _RtmClientCallBack cb)
    {
      cb.GetType().GetMethod(name + "Exception").Invoke((object) cb, new object[1]
      {
        (object) new RtmClientException(RtmClientException.EType.EConnClosed, "The RTM connection was broken, you can not send any command before reconnect again.")
      });
    }

    private struct aCallback
    {
      public string InvokeName;
      public _RtmClientCallBack cb;

      public aCallback(string n, _RtmClientCallBack cb)
      {
        this.InvokeName = n;
        this.cb = cb;
      }
    }
  }
}
