﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Rpc.Container.PacketBase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc.Adapter;
using System;
using System.IO;
using Thrift.Transport;

namespace Rtm.Rpc.Container
{
  public class PacketBase : TTransport
  {
    public MemoryStream inBuffer = new MemoryStream();
    public MemoryStream outBuffer = new MemoryStream();
    public string CallName = string.Empty;
    public short headerMagic = 4095;
    public short flags = 1;
    public int seqNum = 1;
    public short headerSize = 1;
    private TAsyncManager asyncman;
    private bool autoFlush;
    public _RtmClientCallBack cb;

    public PacketBase()
    {
    }

    public PacketBase(TAsyncManager asyncman)
    {
      this.asyncman = asyncman;
      this.autoFlush = true;
    }

    public override bool IsOpen
    {
      get
      {
        return true;
      }
    }

    public override void Open()
    {
    }

    public override void Close()
    {
    }

    public override int Read(byte[] buf, int off, int len)
    {
      return this.inBuffer.Read(buf, off, len);
    }

    public override void Write(byte[] buf, int off, int len)
    {
      this.outBuffer.Write(buf, off, len);
    }

    public void EncodePackage()
    {
      byte[] array = this.outBuffer.ToArray();
      byte[] buffer = new byte[18 + array.Length];
      Buffer.BlockCopy((Array) CommonService.GetI32Byte(14 + array.Length), 0, (Array) buffer, 0, 4);
      Buffer.BlockCopy((Array) CommonService.GetI16Byte(this.headerMagic), 0, (Array) buffer, 4, 2);
      if ((int) this.flags == 4)
      {
        Buffer.BlockCopy((Array) CommonService.GetI16Byte((short) 1), 0, (Array) buffer, 6, 2);
        Buffer.BlockCopy((Array) new byte[4]
        {
          byte.MaxValue,
          byte.MaxValue,
          byte.MaxValue,
          byte.MaxValue
        }, 0, (Array) buffer, 8, 4);
      }
      else
      {
        Buffer.BlockCopy((Array) CommonService.GetI16Byte(this.flags), 0, (Array) buffer, 6, 2);
        Buffer.BlockCopy((Array) CommonService.GetI32Byte(this.seqNum), 0, (Array) buffer, 8, 4);
      }
      Buffer.BlockCopy((Array) CommonService.GetI16Byte(this.headerSize), 0, (Array) buffer, 12, 2);
      Buffer.BlockCopy((Array) CommonService.GetI32Byte(0), 0, (Array) buffer, 14, 4);
      Buffer.BlockCopy((Array) array, 0, (Array) buffer, 18, array.Length);
      this.outBuffer.SetLength(0L);
      this.outBuffer.Position = 0L;
      this.outBuffer.Write(buffer, 0, buffer.Length);
      this.outBuffer.Position = 0L;
    }

    public void DecodePackage()
    {
      byte[] array = this.inBuffer.ToArray();
      this.headerMagic = CommonService.ReadI16(array, 4);
      this.flags = CommonService.ReadI16(array, 6);
      this.seqNum = CommonService.ReadI32(array, 8);
      this.headerSize = CommonService.ReadI16(array, 12);
      this.inBuffer.SetLength(0L);
      this.inBuffer.Position = 0L;
      for (int index = 0; index < array.Length - 14 - (int) this.headerSize * 4; ++index)
        this.inBuffer.WriteByte(array[14 + index + (int) this.headerSize * 4]);
      this.inBuffer.Position = 0L;
    }

    private PacketBase Clone()
    {
      PacketBase packetBase = new PacketBase();
      packetBase.seqNum = this.seqNum;
      packetBase.headerSize = this.headerSize;
      packetBase.headerMagic = this.headerMagic;
      packetBase.flags = this.flags;
      packetBase.CallName = this.CallName;
      packetBase.cb = this.cb;
      this.outBuffer.Position = 0L;
      byte[] array1 = this.outBuffer.ToArray();
      packetBase.outBuffer.Write(array1, 0, array1.Length);
      packetBase.outBuffer.Position = 0L;
      this.inBuffer.Position = 0L;
      byte[] array2 = this.inBuffer.ToArray();
      packetBase.inBuffer.Write(array2, 0, array2.Length);
      packetBase.inBuffer.Position = 0L;
      return packetBase;
    }

    public override void Flush()
    {
      if (!this.autoFlush)
        return;
      this.asyncman.Send(this.Clone());
      this.cb = (_RtmClientCallBack) null;
      this.outBuffer.SetLength(0L);
      this.inBuffer.SetLength(0L);
      this.outBuffer.Position = 0L;
      this.inBuffer.Position = 0L;
      ++this.seqNum;
    }

    public void Clear()
    {
      this.cb = (_RtmClientCallBack) null;
      this.asyncman = (TAsyncManager) null;
      this.outBuffer.SetLength(0L);
      this.inBuffer.SetLength(0L);
      this.outBuffer.Close();
      this.inBuffer.Close();
    }
  }
}
