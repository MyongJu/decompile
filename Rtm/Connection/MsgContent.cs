﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.MsgContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class MsgContent : TBase
  {
    private string body;
    private int mtime;
    private byte mtype;
    private long uid;
    public MsgContent.Isset __isset;

    public string Body
    {
      get
      {
        return this.body;
      }
      set
      {
        this.__isset.body = true;
        this.body = value;
      }
    }

    public int Mtime
    {
      get
      {
        return this.mtime;
      }
      set
      {
        this.__isset.mtime = true;
        this.mtime = value;
      }
    }

    public byte Mtype
    {
      get
      {
        return this.mtype;
      }
      set
      {
        this.__isset.mtype = true;
        this.mtype = value;
      }
    }

    public long Uid
    {
      get
      {
        return this.uid;
      }
      set
      {
        this.__isset.uid = true;
        this.uid = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.String)
              {
                this.body = iprot.ReadString();
                this.__isset.body = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I32)
              {
                this.mtime = iprot.ReadI32();
                this.__isset.mtime = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 3:
              if (tfield.Type == TType.Byte)
              {
                this.mtype = iprot.ReadByte();
                this.__isset.mtype = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 4:
              if (tfield.Type == TType.I64)
              {
                this.uid = iprot.ReadI64();
                this.__isset.uid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (MsgContent));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.body != null && this.__isset.body)
      {
        field.Name = "body";
        field.Type = TType.String;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(this.body);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.mtime)
      {
        field.Name = "mtime";
        field.Type = TType.I32;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.mtime);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.mtype)
      {
        field.Name = "mtype";
        field.Type = TType.Byte;
        field.ID = (short) 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteByte(this.mtype);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.uid)
      {
        field.Name = "uid";
        field.Type = TType.I64;
        field.ID = (short) 4;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.uid);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("MsgContent(");
      stringBuilder.Append("body: ");
      stringBuilder.Append(this.body);
      stringBuilder.Append(",mtime: ");
      stringBuilder.Append(this.mtime);
      stringBuilder.Append(",mtype: ");
      stringBuilder.Append(this.mtype);
      stringBuilder.Append(",uid: ");
      stringBuilder.Append(this.uid);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool body;
      public bool mtime;
      public bool mtype;
      public bool uid;
    }
  }
}
