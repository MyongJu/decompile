﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.MsgParamP2P
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Collections;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class MsgParamP2P : TBase
  {
    private long from_uid;
    private int num;
    private byte direction;
    private long last_id;
    private long local_mid;
    private int local_time;
    private long local_id;
    private THashSet<byte> mtypes;
    public MsgParamP2P.Isset __isset;

    public long From_uid
    {
      get
      {
        return this.from_uid;
      }
      set
      {
        this.__isset.from_uid = true;
        this.from_uid = value;
      }
    }

    public int Num
    {
      get
      {
        return this.num;
      }
      set
      {
        this.__isset.num = true;
        this.num = value;
      }
    }

    public byte Direction
    {
      get
      {
        return this.direction;
      }
      set
      {
        this.__isset.direction = true;
        this.direction = value;
      }
    }

    public long Last_id
    {
      get
      {
        return this.last_id;
      }
      set
      {
        this.__isset.last_id = true;
        this.last_id = value;
      }
    }

    public long Local_mid
    {
      get
      {
        return this.local_mid;
      }
      set
      {
        this.__isset.local_mid = true;
        this.local_mid = value;
      }
    }

    public int Local_time
    {
      get
      {
        return this.local_time;
      }
      set
      {
        this.__isset.local_time = true;
        this.local_time = value;
      }
    }

    public long Local_id
    {
      get
      {
        return this.local_id;
      }
      set
      {
        this.__isset.local_id = true;
        this.local_id = value;
      }
    }

    public THashSet<byte> Mtypes
    {
      get
      {
        return this.mtypes;
      }
      set
      {
        this.__isset.mtypes = true;
        this.mtypes = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.I64)
              {
                this.from_uid = iprot.ReadI64();
                this.__isset.from_uid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I32)
              {
                this.num = iprot.ReadI32();
                this.__isset.num = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 3:
              if (tfield.Type == TType.Byte)
              {
                this.direction = iprot.ReadByte();
                this.__isset.direction = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 4:
              if (tfield.Type == TType.I64)
              {
                this.last_id = iprot.ReadI64();
                this.__isset.last_id = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 5:
              if (tfield.Type == TType.I64)
              {
                this.local_mid = iprot.ReadI64();
                this.__isset.local_mid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 6:
              if (tfield.Type == TType.I32)
              {
                this.local_time = iprot.ReadI32();
                this.__isset.local_time = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 7:
              if (tfield.Type == TType.I64)
              {
                this.local_id = iprot.ReadI64();
                this.__isset.local_id = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 8:
              if (tfield.Type == TType.Set)
              {
                this.mtypes = new THashSet<byte>();
                TSet tset = iprot.ReadSetBegin();
                for (int index = 0; index < tset.Count; ++index)
                  this.mtypes.Add(iprot.ReadByte());
                iprot.ReadSetEnd();
                this.__isset.mtypes = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (MsgParamP2P));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.from_uid)
      {
        field.Name = "from_uid";
        field.Type = TType.I64;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.from_uid);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.num)
      {
        field.Name = "num";
        field.Type = TType.I32;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.num);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.direction)
      {
        field.Name = "direction";
        field.Type = TType.Byte;
        field.ID = (short) 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteByte(this.direction);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.last_id)
      {
        field.Name = "last_id";
        field.Type = TType.I64;
        field.ID = (short) 4;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.last_id);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.local_mid)
      {
        field.Name = "local_mid";
        field.Type = TType.I64;
        field.ID = (short) 5;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.local_mid);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.local_time)
      {
        field.Name = "local_time";
        field.Type = TType.I32;
        field.ID = (short) 6;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.local_time);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.local_id)
      {
        field.Name = "local_id";
        field.Type = TType.I64;
        field.ID = (short) 7;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.local_id);
        oprot.WriteFieldEnd();
      }
      if (this.mtypes != null && this.__isset.mtypes)
      {
        field.Name = "mtypes";
        field.Type = TType.Set;
        field.ID = (short) 8;
        oprot.WriteFieldBegin(field);
        oprot.WriteSetBegin(new TSet(TType.Byte, this.mtypes.Count));
        foreach (byte mtype in this.mtypes)
        {
          oprot.WriteByte(mtype);
          oprot.WriteSetEnd();
        }
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("MsgParamP2P(");
      stringBuilder.Append("from_uid: ");
      stringBuilder.Append(this.from_uid);
      stringBuilder.Append(",num: ");
      stringBuilder.Append(this.num);
      stringBuilder.Append(",direction: ");
      stringBuilder.Append(this.direction);
      stringBuilder.Append(",last_id: ");
      stringBuilder.Append(this.last_id);
      stringBuilder.Append(",local_mid: ");
      stringBuilder.Append(this.local_mid);
      stringBuilder.Append(",local_time: ");
      stringBuilder.Append(this.local_time);
      stringBuilder.Append(",local_id: ");
      stringBuilder.Append(this.local_id);
      stringBuilder.Append(",mtypes: ");
      stringBuilder.Append((object) this.mtypes);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool from_uid;
      public bool num;
      public bool direction;
      public bool last_id;
      public bool local_mid;
      public bool local_time;
      public bool local_id;
      public bool mtypes;
    }
  }
}
