﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.rtmGatedService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Thrift;
using Thrift.Protocol;

namespace Rtm.Connection
{
  public class rtmGatedService
  {
    public interface Iface : FunplusService.Iface
    {
      bool auth(int project_id, long uid, string auth_token, string version, Dictionary<string, string> kv);

      void add_variable(Dictionary<string, string> kv);

      int send_msg(long to, long mid, byte mtype, string message);

      int send_msgs(List<long> tos, long mid, byte mtype, string message);

      int send_group_msg(long group_id, long mid, byte mtype, string message);

      void p();

      void send_note(long to, byte mtype, string message);

      void send_notes(List<long> tos, byte mtype, string message);

      void send_group_note(long group_id, byte mtype, string message);

      void bye();

      void friend_changed(byte otype, List<friendPair> uid_pair);

      long create_group(long group_id, string group_name);

      long join_group(long group_id);

      void delete_group(long group_id);

      void group_changed(byte otype, long group_id, List<long> uids);

      List<MsgNum> check_offline_msg();

      MsgResult get_history_msg(MsgParam param_list);

      MsgResult get_history_msg_new(MsgParamNew param_list);

      MsgResult get_p2p_history_msg(MsgParam param_list);

      MsgResultP2P get_p2p_history_msg_new(MsgParamP2P param_list);

      MsgResultGroup get_group_history_msg_new(MsgParamGroup param_list);

      List<long> get_online_users(List<long> uids);

      List<long> get_group_online_users(long group_id);

      void send_broadcast_group_msg(long group_id, long mid, byte mtype, string message);
    }

    public class Client : FunplusService.Client, FunplusService.Iface, rtmGatedService.Iface
    {
      public Client(TProtocol prot)
        : this(prot, prot)
      {
      }

      public Client(TProtocol iprot, TProtocol oprot)
        : base(iprot, oprot)
      {
      }

      public bool auth(int project_id, long uid, string auth_token, string version, Dictionary<string, string> kv)
      {
        this.send_auth(project_id, uid, auth_token, version, kv);
        return this.recv_auth();
      }

      public void send_auth(int project_id, long uid, string auth_token, string version, Dictionary<string, string> kv)
      {
        this.oprot_.WriteMessageBegin(new TMessage("auth", TMessageType.Call, this.seqid_));
        new rtmGatedService.auth_args()
        {
          Project_id = project_id,
          Uid = uid,
          Auth_token = auth_token,
          Version = version,
          Kv = kv
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public bool recv_auth()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.auth_result authResult = new rtmGatedService.auth_result();
        authResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (authResult.__isset.success)
          return authResult.Success;
        if (authResult.__isset.ex)
          throw authResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "auth failed: unknown result");
      }

      public void add_variable(Dictionary<string, string> kv)
      {
        this.send_add_variable(kv);
        this.recv_add_variable();
      }

      public void send_add_variable(Dictionary<string, string> kv)
      {
        this.oprot_.WriteMessageBegin(new TMessage("add_variable", TMessageType.Call, this.seqid_));
        new rtmGatedService.add_variable_args() { Kv = kv }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_add_variable()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.add_variable_result addVariableResult = new rtmGatedService.add_variable_result();
        addVariableResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (addVariableResult.__isset.ex)
          throw addVariableResult.Ex;
      }

      public int send_msg(long to, long mid, byte mtype, string message)
      {
        this.send_send_msg(to, mid, mtype, message);
        return this.recv_send_msg();
      }

      public void send_send_msg(long to, long mid, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_msg_args()
        {
          To = to,
          Mid = mid,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public int recv_send_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.send_msg_result sendMsgResult = new rtmGatedService.send_msg_result();
        sendMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (sendMsgResult.__isset.success)
          return sendMsgResult.Success;
        if (sendMsgResult.__isset.ex)
          throw sendMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_msg failed: unknown result");
      }

      public int send_msgs(List<long> tos, long mid, byte mtype, string message)
      {
        this.send_send_msgs(tos, mid, mtype, message);
        return this.recv_send_msgs();
      }

      public void send_send_msgs(List<long> tos, long mid, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_msgs", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_msgs_args()
        {
          Tos = tos,
          Mid = mid,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public int recv_send_msgs()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.send_msgs_result sendMsgsResult = new rtmGatedService.send_msgs_result();
        sendMsgsResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (sendMsgsResult.__isset.success)
          return sendMsgsResult.Success;
        if (sendMsgsResult.__isset.ex)
          throw sendMsgsResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_msgs failed: unknown result");
      }

      public int send_group_msg(long group_id, long mid, byte mtype, string message)
      {
        this.send_send_group_msg(group_id, mid, mtype, message);
        return this.recv_send_group_msg();
      }

      public void send_send_group_msg(long group_id, long mid, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_group_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_group_msg_args()
        {
          Group_id = group_id,
          Mid = mid,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public int recv_send_group_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.send_group_msg_result sendGroupMsgResult = new rtmGatedService.send_group_msg_result();
        sendGroupMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (sendGroupMsgResult.__isset.success)
          return sendGroupMsgResult.Success;
        if (sendGroupMsgResult.__isset.ex)
          throw sendGroupMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "send_group_msg failed: unknown result");
      }

      public void p()
      {
        this.send_p();
        this.recv_p();
      }

      public void send_p()
      {
        this.oprot_.WriteMessageBegin(new TMessage("p", TMessageType.Call, this.seqid_));
        new rtmGatedService.p_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_p()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedService.p_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void send_note(long to, byte mtype, string message)
      {
        this.send_send_note(to, mtype, message);
      }

      public void send_send_note(long to, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_note", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_note_args()
        {
          To = to,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void send_notes(List<long> tos, byte mtype, string message)
      {
        this.send_send_notes(tos, mtype, message);
      }

      public void send_send_notes(List<long> tos, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_notes", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_notes_args()
        {
          Tos = tos,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void send_group_note(long group_id, byte mtype, string message)
      {
        this.send_send_group_note(group_id, mtype, message);
      }

      public void send_send_group_note(long group_id, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_group_note", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_group_note_args()
        {
          Group_id = group_id,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void bye()
      {
        this.send_bye();
        this.recv_bye();
      }

      public void send_bye()
      {
        this.oprot_.WriteMessageBegin(new TMessage("bye", TMessageType.Call, this.seqid_));
        new rtmGatedService.bye_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_bye()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.bye_result byeResult = new rtmGatedService.bye_result();
        byeResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (byeResult.__isset.ex)
          throw byeResult.Ex;
      }

      public void friend_changed(byte otype, List<friendPair> uid_pair)
      {
        this.send_friend_changed(otype, uid_pair);
        this.recv_friend_changed();
      }

      public void send_friend_changed(byte otype, List<friendPair> uid_pair)
      {
        this.oprot_.WriteMessageBegin(new TMessage("friend_changed", TMessageType.Call, this.seqid_));
        new rtmGatedService.friend_changed_args()
        {
          Otype = otype,
          Uid_pair = uid_pair
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_friend_changed()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.friend_changed_result friendChangedResult = new rtmGatedService.friend_changed_result();
        friendChangedResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (friendChangedResult.__isset.ex)
          throw friendChangedResult.Ex;
      }

      public long create_group(long group_id, string group_name)
      {
        this.send_create_group(group_id, group_name);
        return this.recv_create_group();
      }

      public void send_create_group(long group_id, string group_name)
      {
        this.oprot_.WriteMessageBegin(new TMessage("create_group", TMessageType.Call, this.seqid_));
        new rtmGatedService.create_group_args()
        {
          Group_id = group_id,
          Group_name = group_name
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public long recv_create_group()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.create_group_result createGroupResult = new rtmGatedService.create_group_result();
        createGroupResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (createGroupResult.__isset.success)
          return createGroupResult.Success;
        if (createGroupResult.__isset.ex)
          throw createGroupResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "create_group failed: unknown result");
      }

      public long join_group(long group_id)
      {
        this.send_join_group(group_id);
        return this.recv_join_group();
      }

      public void send_join_group(long group_id)
      {
        this.oprot_.WriteMessageBegin(new TMessage("join_group", TMessageType.Call, this.seqid_));
        new rtmGatedService.join_group_args()
        {
          Group_id = group_id
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public long recv_join_group()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.join_group_result joinGroupResult = new rtmGatedService.join_group_result();
        joinGroupResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (joinGroupResult.__isset.success)
          return joinGroupResult.Success;
        if (joinGroupResult.__isset.ex)
          throw joinGroupResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "join_group failed: unknown result");
      }

      public void delete_group(long group_id)
      {
        this.send_delete_group(group_id);
        this.recv_delete_group();
      }

      public void send_delete_group(long group_id)
      {
        this.oprot_.WriteMessageBegin(new TMessage("delete_group", TMessageType.Call, this.seqid_));
        new rtmGatedService.delete_group_args()
        {
          Group_id = group_id
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_delete_group()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.delete_group_result deleteGroupResult = new rtmGatedService.delete_group_result();
        deleteGroupResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (deleteGroupResult.__isset.ex)
          throw deleteGroupResult.Ex;
      }

      public void group_changed(byte otype, long group_id, List<long> uids)
      {
        this.send_group_changed(otype, group_id, uids);
        this.recv_group_changed();
      }

      public void send_group_changed(byte otype, long group_id, List<long> uids)
      {
        this.oprot_.WriteMessageBegin(new TMessage("group_changed", TMessageType.Call, this.seqid_));
        new rtmGatedService.group_changed_args()
        {
          Otype = otype,
          Group_id = group_id,
          Uids = uids
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_group_changed()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.group_changed_result groupChangedResult = new rtmGatedService.group_changed_result();
        groupChangedResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (groupChangedResult.__isset.ex)
          throw groupChangedResult.Ex;
      }

      public List<MsgNum> check_offline_msg()
      {
        this.send_check_offline_msg();
        return this.recv_check_offline_msg();
      }

      public void send_check_offline_msg()
      {
        this.oprot_.WriteMessageBegin(new TMessage("check_offline_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.check_offline_msg_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public List<MsgNum> recv_check_offline_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.check_offline_msg_result offlineMsgResult = new rtmGatedService.check_offline_msg_result();
        offlineMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (offlineMsgResult.__isset.success)
          return offlineMsgResult.Success;
        if (offlineMsgResult.__isset.ex)
          throw offlineMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "check_offline_msg failed: unknown result");
      }

      public MsgResult get_history_msg(MsgParam param_list)
      {
        this.send_get_history_msg(param_list);
        return this.recv_get_history_msg();
      }

      public void send_get_history_msg(MsgParam param_list)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_history_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_history_msg_args()
        {
          Param_list = param_list
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public MsgResult recv_get_history_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_history_msg_result historyMsgResult = new rtmGatedService.get_history_msg_result();
        historyMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (historyMsgResult.__isset.success)
          return historyMsgResult.Success;
        if (historyMsgResult.__isset.ex)
          throw historyMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_history_msg failed: unknown result");
      }

      public MsgResult get_history_msg_new(MsgParamNew param_list)
      {
        this.send_get_history_msg_new(param_list);
        return this.recv_get_history_msg_new();
      }

      public void send_get_history_msg_new(MsgParamNew param_list)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_history_msg_new", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_history_msg_new_args()
        {
          Param_list = param_list
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public MsgResult recv_get_history_msg_new()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_history_msg_new_result();
        historyMsgNewResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_history_msg_new failed: unknown result");
      }

      public MsgResult get_p2p_history_msg(MsgParam param_list)
      {
        this.send_get_p2p_history_msg(param_list);
        return this.recv_get_p2p_history_msg();
      }

      public void send_get_p2p_history_msg(MsgParam param_list)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_p2p_history_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_p2p_history_msg_args()
        {
          Param_list = param_list
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public MsgResult recv_get_p2p_history_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_p2p_history_msg_result historyMsgResult = new rtmGatedService.get_p2p_history_msg_result();
        historyMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (historyMsgResult.__isset.success)
          return historyMsgResult.Success;
        if (historyMsgResult.__isset.ex)
          throw historyMsgResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_p2p_history_msg failed: unknown result");
      }

      public MsgResultP2P get_p2p_history_msg_new(MsgParamP2P param_list)
      {
        this.send_get_p2p_history_msg_new(param_list);
        return this.recv_get_p2p_history_msg_new();
      }

      public void send_get_p2p_history_msg_new(MsgParamP2P param_list)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_p2p_history_msg_new", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_p2p_history_msg_new_args()
        {
          Param_list = param_list
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public MsgResultP2P recv_get_p2p_history_msg_new()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_p2p_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_p2p_history_msg_new_result();
        historyMsgNewResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_p2p_history_msg_new failed: unknown result");
      }

      public MsgResultGroup get_group_history_msg_new(MsgParamGroup param_list)
      {
        this.send_get_group_history_msg_new(param_list);
        return this.recv_get_group_history_msg_new();
      }

      public void send_get_group_history_msg_new(MsgParamGroup param_list)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_group_history_msg_new", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_group_history_msg_new_args()
        {
          Param_list = param_list
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public MsgResultGroup recv_get_group_history_msg_new()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_group_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_group_history_msg_new_result();
        historyMsgNewResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (historyMsgNewResult.__isset.success)
          return historyMsgNewResult.Success;
        if (historyMsgNewResult.__isset.ex)
          throw historyMsgNewResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_group_history_msg_new failed: unknown result");
      }

      public List<long> get_online_users(List<long> uids)
      {
        this.send_get_online_users(uids);
        return this.recv_get_online_users();
      }

      public void send_get_online_users(List<long> uids)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_online_users", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_online_users_args()
        {
          Uids = uids
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public List<long> recv_get_online_users()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_online_users_result onlineUsersResult = new rtmGatedService.get_online_users_result();
        onlineUsersResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (onlineUsersResult.__isset.success)
          return onlineUsersResult.Success;
        if (onlineUsersResult.__isset.ex)
          throw onlineUsersResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_online_users failed: unknown result");
      }

      public List<long> get_group_online_users(long group_id)
      {
        this.send_get_group_online_users(group_id);
        return this.recv_get_group_online_users();
      }

      public void send_get_group_online_users(long group_id)
      {
        this.oprot_.WriteMessageBegin(new TMessage("get_group_online_users", TMessageType.Call, this.seqid_));
        new rtmGatedService.get_group_online_users_args()
        {
          Group_id = group_id
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public List<long> recv_get_group_online_users()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.get_group_online_users_result onlineUsersResult = new rtmGatedService.get_group_online_users_result();
        onlineUsersResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (onlineUsersResult.__isset.success)
          return onlineUsersResult.Success;
        if (onlineUsersResult.__isset.ex)
          throw onlineUsersResult.Ex;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "get_group_online_users failed: unknown result");
      }

      public void send_broadcast_group_msg(long group_id, long mid, byte mtype, string message)
      {
        this.send_send_broadcast_group_msg(group_id, mid, mtype, message);
        this.recv_send_broadcast_group_msg();
      }

      public void send_send_broadcast_group_msg(long group_id, long mid, byte mtype, string message)
      {
        this.oprot_.WriteMessageBegin(new TMessage("send_broadcast_group_msg", TMessageType.Call, this.seqid_));
        new rtmGatedService.send_broadcast_group_msg_args()
        {
          Group_id = group_id,
          Mid = mid,
          Mtype = mtype,
          Message = message
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_send_broadcast_group_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        rtmGatedService.send_broadcast_group_msg_result broadcastGroupMsgResult = new rtmGatedService.send_broadcast_group_msg_result();
        broadcastGroupMsgResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (broadcastGroupMsgResult.__isset.ex)
          throw broadcastGroupMsgResult.Ex;
      }
    }

    public class Processor : FunplusService.Processor, TProcessor
    {
      private rtmGatedService.Iface iface_;

      public Processor(rtmGatedService.Iface iface)
        : base((FunplusService.Iface) iface)
      {
        this.iface_ = iface;
        this.processMap_["auth"] = new FunplusService.Processor.ProcessFunction(this.auth_Process);
        this.processMap_["add_variable"] = new FunplusService.Processor.ProcessFunction(this.add_variable_Process);
        this.processMap_["send_msg"] = new FunplusService.Processor.ProcessFunction(this.send_msg_Process);
        this.processMap_["send_msgs"] = new FunplusService.Processor.ProcessFunction(this.send_msgs_Process);
        this.processMap_["send_group_msg"] = new FunplusService.Processor.ProcessFunction(this.send_group_msg_Process);
        this.processMap_["p"] = new FunplusService.Processor.ProcessFunction(this.p_Process);
        this.processMap_["send_note"] = new FunplusService.Processor.ProcessFunction(this.send_note_Process);
        this.processMap_["send_notes"] = new FunplusService.Processor.ProcessFunction(this.send_notes_Process);
        this.processMap_["send_group_note"] = new FunplusService.Processor.ProcessFunction(this.send_group_note_Process);
        this.processMap_["bye"] = new FunplusService.Processor.ProcessFunction(this.bye_Process);
        this.processMap_["friend_changed"] = new FunplusService.Processor.ProcessFunction(this.friend_changed_Process);
        this.processMap_["create_group"] = new FunplusService.Processor.ProcessFunction(this.create_group_Process);
        this.processMap_["join_group"] = new FunplusService.Processor.ProcessFunction(this.join_group_Process);
        this.processMap_["delete_group"] = new FunplusService.Processor.ProcessFunction(this.delete_group_Process);
        this.processMap_["group_changed"] = new FunplusService.Processor.ProcessFunction(this.group_changed_Process);
        this.processMap_["check_offline_msg"] = new FunplusService.Processor.ProcessFunction(this.check_offline_msg_Process);
        this.processMap_["get_history_msg"] = new FunplusService.Processor.ProcessFunction(this.get_history_msg_Process);
        this.processMap_["get_history_msg_new"] = new FunplusService.Processor.ProcessFunction(this.get_history_msg_new_Process);
        this.processMap_["get_p2p_history_msg"] = new FunplusService.Processor.ProcessFunction(this.get_p2p_history_msg_Process);
        this.processMap_["get_p2p_history_msg_new"] = new FunplusService.Processor.ProcessFunction(this.get_p2p_history_msg_new_Process);
        this.processMap_["get_group_history_msg_new"] = new FunplusService.Processor.ProcessFunction(this.get_group_history_msg_new_Process);
        this.processMap_["get_online_users"] = new FunplusService.Processor.ProcessFunction(this.get_online_users_Process);
        this.processMap_["get_group_online_users"] = new FunplusService.Processor.ProcessFunction(this.get_group_online_users_Process);
        this.processMap_["send_broadcast_group_msg"] = new FunplusService.Processor.ProcessFunction(this.send_broadcast_group_msg_Process);
      }

      public new bool Process(TProtocol iprot, TProtocol oprot)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          FunplusService.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
          {
            TProtocolUtil.Skip(iprot, TType.Struct);
            iprot.ReadMessageEnd();
            TApplicationException tapplicationException = new TApplicationException(TApplicationException.ExceptionType.UnknownMethod, "Invalid method name: '" + tmessage.Name + "'");
            oprot.WriteMessageBegin(new TMessage(tmessage.Name, TMessageType.Exception, tmessage.SeqID));
            tapplicationException.Write(oprot);
            oprot.WriteMessageEnd();
            oprot.Transport.Flush();
            return true;
          }
          processFunction(tmessage.SeqID, iprot, oprot);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public void auth_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.auth_args authArgs = new rtmGatedService.auth_args();
        authArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.auth_result authResult = new rtmGatedService.auth_result();
        try
        {
          authResult.Success = this.iface_.auth(authArgs.Project_id, authArgs.Uid, authArgs.Auth_token, authArgs.Version, authArgs.Kv);
        }
        catch (rtmGatedException ex)
        {
          authResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("auth", TMessageType.Reply, seqid));
        authResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void add_variable_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.add_variable_args addVariableArgs = new rtmGatedService.add_variable_args();
        addVariableArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.add_variable_result addVariableResult = new rtmGatedService.add_variable_result();
        try
        {
          this.iface_.add_variable(addVariableArgs.Kv);
        }
        catch (rtmGatedException ex)
        {
          addVariableResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("add_variable", TMessageType.Reply, seqid));
        addVariableResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void send_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_msg_args sendMsgArgs = new rtmGatedService.send_msg_args();
        sendMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.send_msg_result sendMsgResult = new rtmGatedService.send_msg_result();
        try
        {
          sendMsgResult.Success = this.iface_.send_msg(sendMsgArgs.To, sendMsgArgs.Mid, sendMsgArgs.Mtype, sendMsgArgs.Message);
        }
        catch (rtmGatedException ex)
        {
          sendMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("send_msg", TMessageType.Reply, seqid));
        sendMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void send_msgs_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_msgs_args sendMsgsArgs = new rtmGatedService.send_msgs_args();
        sendMsgsArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.send_msgs_result sendMsgsResult = new rtmGatedService.send_msgs_result();
        try
        {
          sendMsgsResult.Success = this.iface_.send_msgs(sendMsgsArgs.Tos, sendMsgsArgs.Mid, sendMsgsArgs.Mtype, sendMsgsArgs.Message);
        }
        catch (rtmGatedException ex)
        {
          sendMsgsResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("send_msgs", TMessageType.Reply, seqid));
        sendMsgsResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void send_group_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_group_msg_args sendGroupMsgArgs = new rtmGatedService.send_group_msg_args();
        sendGroupMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.send_group_msg_result sendGroupMsgResult = new rtmGatedService.send_group_msg_result();
        try
        {
          sendGroupMsgResult.Success = this.iface_.send_group_msg(sendGroupMsgArgs.Group_id, sendGroupMsgArgs.Mid, sendGroupMsgArgs.Mtype, sendGroupMsgArgs.Message);
        }
        catch (rtmGatedException ex)
        {
          sendGroupMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("send_group_msg", TMessageType.Reply, seqid));
        sendGroupMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void p_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new rtmGatedService.p_args().Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.p_result pResult = new rtmGatedService.p_result();
        this.iface_.p();
        oprot.WriteMessageBegin(new TMessage("p", TMessageType.Reply, seqid));
        pResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void send_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_note_args sendNoteArgs = new rtmGatedService.send_note_args();
        sendNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.send_note(sendNoteArgs.To, sendNoteArgs.Mtype, sendNoteArgs.Message);
      }

      public void send_notes_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_notes_args sendNotesArgs = new rtmGatedService.send_notes_args();
        sendNotesArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.send_notes(sendNotesArgs.Tos, sendNotesArgs.Mtype, sendNotesArgs.Message);
      }

      public void send_group_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_group_note_args sendGroupNoteArgs = new rtmGatedService.send_group_note_args();
        sendGroupNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.send_group_note(sendGroupNoteArgs.Group_id, sendGroupNoteArgs.Mtype, sendGroupNoteArgs.Message);
      }

      public void bye_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new rtmGatedService.bye_args().Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.bye_result byeResult = new rtmGatedService.bye_result();
        try
        {
          this.iface_.bye();
        }
        catch (rtmGatedException ex)
        {
          byeResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("bye", TMessageType.Reply, seqid));
        byeResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void friend_changed_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.friend_changed_args friendChangedArgs = new rtmGatedService.friend_changed_args();
        friendChangedArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.friend_changed_result friendChangedResult = new rtmGatedService.friend_changed_result();
        try
        {
          this.iface_.friend_changed(friendChangedArgs.Otype, friendChangedArgs.Uid_pair);
        }
        catch (rtmGatedException ex)
        {
          friendChangedResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("friend_changed", TMessageType.Reply, seqid));
        friendChangedResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void create_group_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.create_group_args createGroupArgs = new rtmGatedService.create_group_args();
        createGroupArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.create_group_result createGroupResult = new rtmGatedService.create_group_result();
        try
        {
          createGroupResult.Success = this.iface_.create_group(createGroupArgs.Group_id, createGroupArgs.Group_name);
        }
        catch (rtmGatedException ex)
        {
          createGroupResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("create_group", TMessageType.Reply, seqid));
        createGroupResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void join_group_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.join_group_args joinGroupArgs = new rtmGatedService.join_group_args();
        joinGroupArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.join_group_result joinGroupResult = new rtmGatedService.join_group_result();
        try
        {
          joinGroupResult.Success = this.iface_.join_group(joinGroupArgs.Group_id);
        }
        catch (rtmGatedException ex)
        {
          joinGroupResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("join_group", TMessageType.Reply, seqid));
        joinGroupResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void delete_group_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.delete_group_args deleteGroupArgs = new rtmGatedService.delete_group_args();
        deleteGroupArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.delete_group_result deleteGroupResult = new rtmGatedService.delete_group_result();
        try
        {
          this.iface_.delete_group(deleteGroupArgs.Group_id);
        }
        catch (rtmGatedException ex)
        {
          deleteGroupResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("delete_group", TMessageType.Reply, seqid));
        deleteGroupResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void group_changed_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.group_changed_args groupChangedArgs = new rtmGatedService.group_changed_args();
        groupChangedArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.group_changed_result groupChangedResult = new rtmGatedService.group_changed_result();
        try
        {
          this.iface_.group_changed(groupChangedArgs.Otype, groupChangedArgs.Group_id, groupChangedArgs.Uids);
        }
        catch (rtmGatedException ex)
        {
          groupChangedResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("group_changed", TMessageType.Reply, seqid));
        groupChangedResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void check_offline_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new rtmGatedService.check_offline_msg_args().Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.check_offline_msg_result offlineMsgResult = new rtmGatedService.check_offline_msg_result();
        try
        {
          offlineMsgResult.Success = this.iface_.check_offline_msg();
        }
        catch (rtmGatedException ex)
        {
          offlineMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("check_offline_msg", TMessageType.Reply, seqid));
        offlineMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_history_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_history_msg_args getHistoryMsgArgs = new rtmGatedService.get_history_msg_args();
        getHistoryMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_history_msg_result historyMsgResult = new rtmGatedService.get_history_msg_result();
        try
        {
          historyMsgResult.Success = this.iface_.get_history_msg(getHistoryMsgArgs.Param_list);
        }
        catch (rtmGatedException ex)
        {
          historyMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_history_msg", TMessageType.Reply, seqid));
        historyMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_history_msg_new_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_history_msg_new_args historyMsgNewArgs = new rtmGatedService.get_history_msg_new_args();
        historyMsgNewArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_history_msg_new_result();
        try
        {
          historyMsgNewResult.Success = this.iface_.get_history_msg_new(historyMsgNewArgs.Param_list);
        }
        catch (rtmGatedException ex)
        {
          historyMsgNewResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_history_msg_new", TMessageType.Reply, seqid));
        historyMsgNewResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_p2p_history_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_p2p_history_msg_args p2pHistoryMsgArgs = new rtmGatedService.get_p2p_history_msg_args();
        p2pHistoryMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_p2p_history_msg_result historyMsgResult = new rtmGatedService.get_p2p_history_msg_result();
        try
        {
          historyMsgResult.Success = this.iface_.get_p2p_history_msg(p2pHistoryMsgArgs.Param_list);
        }
        catch (rtmGatedException ex)
        {
          historyMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_p2p_history_msg", TMessageType.Reply, seqid));
        historyMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_p2p_history_msg_new_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_p2p_history_msg_new_args historyMsgNewArgs = new rtmGatedService.get_p2p_history_msg_new_args();
        historyMsgNewArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_p2p_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_p2p_history_msg_new_result();
        try
        {
          historyMsgNewResult.Success = this.iface_.get_p2p_history_msg_new(historyMsgNewArgs.Param_list);
        }
        catch (rtmGatedException ex)
        {
          historyMsgNewResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_p2p_history_msg_new", TMessageType.Reply, seqid));
        historyMsgNewResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_group_history_msg_new_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_group_history_msg_new_args historyMsgNewArgs = new rtmGatedService.get_group_history_msg_new_args();
        historyMsgNewArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_group_history_msg_new_result historyMsgNewResult = new rtmGatedService.get_group_history_msg_new_result();
        try
        {
          historyMsgNewResult.Success = this.iface_.get_group_history_msg_new(historyMsgNewArgs.Param_list);
        }
        catch (rtmGatedException ex)
        {
          historyMsgNewResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_group_history_msg_new", TMessageType.Reply, seqid));
        historyMsgNewResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_online_users_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_online_users_args getOnlineUsersArgs = new rtmGatedService.get_online_users_args();
        getOnlineUsersArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_online_users_result onlineUsersResult = new rtmGatedService.get_online_users_result();
        try
        {
          onlineUsersResult.Success = this.iface_.get_online_users(getOnlineUsersArgs.Uids);
        }
        catch (rtmGatedException ex)
        {
          onlineUsersResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_online_users", TMessageType.Reply, seqid));
        onlineUsersResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void get_group_online_users_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.get_group_online_users_args groupOnlineUsersArgs = new rtmGatedService.get_group_online_users_args();
        groupOnlineUsersArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.get_group_online_users_result onlineUsersResult = new rtmGatedService.get_group_online_users_result();
        try
        {
          onlineUsersResult.Success = this.iface_.get_group_online_users(groupOnlineUsersArgs.Group_id);
        }
        catch (rtmGatedException ex)
        {
          onlineUsersResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("get_group_online_users", TMessageType.Reply, seqid));
        onlineUsersResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void send_broadcast_group_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedService.send_broadcast_group_msg_args broadcastGroupMsgArgs = new rtmGatedService.send_broadcast_group_msg_args();
        broadcastGroupMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedService.send_broadcast_group_msg_result broadcastGroupMsgResult = new rtmGatedService.send_broadcast_group_msg_result();
        try
        {
          this.iface_.send_broadcast_group_msg(broadcastGroupMsgArgs.Group_id, broadcastGroupMsgArgs.Mid, broadcastGroupMsgArgs.Mtype, broadcastGroupMsgArgs.Message);
        }
        catch (rtmGatedException ex)
        {
          broadcastGroupMsgResult.Ex = ex;
        }
        oprot.WriteMessageBegin(new TMessage("send_broadcast_group_msg", TMessageType.Reply, seqid));
        broadcastGroupMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }
    }

    [Serializable]
    public class auth_args : TBase
    {
      private int project_id;
      private long uid;
      private string auth_token;
      private string version;
      private Dictionary<string, string> kv;
      public rtmGatedService.auth_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long Uid
      {
        get
        {
          return this.uid;
        }
        set
        {
          this.__isset.uid = true;
          this.uid = value;
        }
      }

      public string Auth_token
      {
        get
        {
          return this.auth_token;
        }
        set
        {
          this.__isset.auth_token = true;
          this.auth_token = value;
        }
      }

      public string Version
      {
        get
        {
          return this.version;
        }
        set
        {
          this.__isset.version = true;
          this.version = value;
        }
      }

      public Dictionary<string, string> Kv
      {
        get
        {
          return this.kv;
        }
        set
        {
          this.__isset.kv = true;
          this.kv = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.uid = iprot.ReadI64();
                  this.__isset.uid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.auth_token = iprot.ReadString();
                  this.__isset.auth_token = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.version = iprot.ReadString();
                  this.__isset.version = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.Map)
                {
                  this.kv = new Dictionary<string, string>();
                  TMap tmap = iprot.ReadMapBegin();
                  for (int index = 0; index < tmap.Count; ++index)
                    this.kv[iprot.ReadString()] = iprot.ReadString();
                  iprot.ReadMapEnd();
                  this.__isset.kv = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (auth_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.uid)
        {
          field.Name = "uid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.uid);
          oprot.WriteFieldEnd();
        }
        if (this.auth_token != null && this.__isset.auth_token)
        {
          field.Name = "auth_token";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.auth_token);
          oprot.WriteFieldEnd();
        }
        if (this.version != null && this.__isset.version)
        {
          field.Name = "version";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.version);
          oprot.WriteFieldEnd();
        }
        if (this.kv != null && this.__isset.kv)
        {
          field.Name = "kv";
          field.Type = TType.Map;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteMapBegin(new TMap(TType.String, TType.String, this.kv.Count));
          using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.kv.Keys.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              string current = enumerator.Current;
              oprot.WriteString(current);
              oprot.WriteString(this.kv[current]);
              oprot.WriteMapEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("auth_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",uid: ");
        stringBuilder.Append(this.uid);
        stringBuilder.Append(",auth_token: ");
        stringBuilder.Append(this.auth_token);
        stringBuilder.Append(",version: ");
        stringBuilder.Append(this.version);
        stringBuilder.Append(",kv: ");
        stringBuilder.Append((object) this.kv);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool uid;
        public bool auth_token;
        public bool version;
        public bool kv;
      }
    }

    [Serializable]
    public class auth_result : TBase
    {
      private bool success;
      private rtmGatedException ex;
      public rtmGatedService.auth_result.Isset __isset;

      public bool Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Bool)
                {
                  this.success = iprot.ReadBool();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (auth_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.Bool;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteBool(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("auth_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class add_variable_args : TBase
    {
      private Dictionary<string, string> kv;
      public rtmGatedService.add_variable_args.Isset __isset;

      public Dictionary<string, string> Kv
      {
        get
        {
          return this.kv;
        }
        set
        {
          this.__isset.kv = true;
          this.kv = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Map)
              {
                this.kv = new Dictionary<string, string>();
                TMap tmap = iprot.ReadMapBegin();
                for (int index = 0; index < tmap.Count; ++index)
                  this.kv[iprot.ReadString()] = iprot.ReadString();
                iprot.ReadMapEnd();
                this.__isset.kv = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (add_variable_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.kv != null && this.__isset.kv)
        {
          field.Name = "kv";
          field.Type = TType.Map;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteMapBegin(new TMap(TType.String, TType.String, this.kv.Count));
          using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.kv.Keys.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              string current = enumerator.Current;
              oprot.WriteString(current);
              oprot.WriteString(this.kv[current]);
              oprot.WriteMapEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("add_variable_args(");
        stringBuilder.Append("kv: ");
        stringBuilder.Append((object) this.kv);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool kv;
      }
    }

    [Serializable]
    public class add_variable_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.add_variable_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (add_variable_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("add_variable_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }

    [Serializable]
    public class send_msg_args : TBase
    {
      private long to;
      private long mid;
      private byte mtype;
      private string message;
      public rtmGatedService.send_msg_args.Isset __isset;

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_msg_args(");
        stringBuilder.Append("to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool to;
        public bool mid;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_msg_result : TBase
    {
      private int success;
      private rtmGatedException ex;
      public rtmGatedService.send_msg_result.Isset __isset;

      public int Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.I32)
                {
                  this.success = iprot.ReadI32();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.I32;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_msg_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class send_msgs_args : TBase
    {
      private List<long> tos;
      private long mid;
      private byte mtype;
      private string message;
      public rtmGatedService.send_msgs_args.Isset __isset;

      public List<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.List)
                {
                  this.tos = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_msgs_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.List;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.tos.Count));
          using (List<long>.Enumerator enumerator = this.tos.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_msgs_args(");
        stringBuilder.Append("tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool tos;
        public bool mid;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_msgs_result : TBase
    {
      private int success;
      private rtmGatedException ex;
      public rtmGatedService.send_msgs_result.Isset __isset;

      public int Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.I32)
                {
                  this.success = iprot.ReadI32();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_msgs_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.I32;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_msgs_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class send_group_msg_args : TBase
    {
      private long group_id;
      private long mid;
      private byte mtype;
      private string message;
      public rtmGatedService.send_group_msg_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_group_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_group_msg_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
        public bool mid;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_group_msg_result : TBase
    {
      private int success;
      private rtmGatedException ex;
      public rtmGatedService.send_group_msg_result.Isset __isset;

      public int Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.I32)
                {
                  this.success = iprot.ReadI32();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_group_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.I32;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_group_msg_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class p_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (p_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("p_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class p_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (p_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("p_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class send_note_args : TBase
    {
      private long to;
      private byte mtype;
      private string message;
      public rtmGatedService.send_note_args.Isset __isset;

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_note_args(");
        stringBuilder.Append("to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool to;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_notes_args : TBase
    {
      private List<long> tos;
      private byte mtype;
      private string message;
      public rtmGatedService.send_notes_args.Isset __isset;

      public List<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.List)
                {
                  this.tos = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_notes_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.List;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.tos.Count));
          using (List<long>.Enumerator enumerator = this.tos.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_notes_args(");
        stringBuilder.Append("tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool tos;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_group_note_args : TBase
    {
      private long group_id;
      private byte mtype;
      private string message;
      public rtmGatedService.send_group_note_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_group_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_group_note_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class bye_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (bye_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("bye_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class bye_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.bye_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (bye_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("bye_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }

    [Serializable]
    public class friend_changed_args : TBase
    {
      private byte otype;
      private List<friendPair> uid_pair;
      public rtmGatedService.friend_changed_args.Isset __isset;

      public byte Otype
      {
        get
        {
          return this.otype;
        }
        set
        {
          this.__isset.otype = true;
          this.otype = value;
        }
      }

      public List<friendPair> Uid_pair
      {
        get
        {
          return this.uid_pair;
        }
        set
        {
          this.__isset.uid_pair = true;
          this.uid_pair = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.Byte)
                {
                  this.otype = iprot.ReadByte();
                  this.__isset.otype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.List)
                {
                  this.uid_pair = new List<friendPair>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                  {
                    friendPair friendPair1 = new friendPair();
                    friendPair friendPair2 = new friendPair();
                    friendPair2.Read(iprot);
                    this.uid_pair.Add(friendPair2);
                  }
                  iprot.ReadListEnd();
                  this.__isset.uid_pair = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (friend_changed_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.otype)
        {
          field.Name = "otype";
          field.Type = TType.Byte;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.otype);
          oprot.WriteFieldEnd();
        }
        if (this.uid_pair != null && this.__isset.uid_pair)
        {
          field.Name = "uid_pair";
          field.Type = TType.List;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.Struct, this.uid_pair.Count));
          using (List<friendPair>.Enumerator enumerator = this.uid_pair.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              enumerator.Current.Write(oprot);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("friend_changed_args(");
        stringBuilder.Append("otype: ");
        stringBuilder.Append(this.otype);
        stringBuilder.Append(",uid_pair: ");
        stringBuilder.Append((object) this.uid_pair);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool otype;
        public bool uid_pair;
      }
    }

    [Serializable]
    public class friend_changed_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.friend_changed_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (friend_changed_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("friend_changed_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }

    [Serializable]
    public class create_group_args : TBase
    {
      private long group_id;
      private string group_name;
      public rtmGatedService.create_group_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public string Group_name
      {
        get
        {
          return this.group_name;
        }
        set
        {
          this.__isset.group_name = true;
          this.group_name = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.String)
                {
                  this.group_name = iprot.ReadString();
                  this.__isset.group_name = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (create_group_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.group_name != null && this.__isset.group_name)
        {
          field.Name = "group_name";
          field.Type = TType.String;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.group_name);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("create_group_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",group_name: ");
        stringBuilder.Append(this.group_name);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
        public bool group_name;
      }
    }

    [Serializable]
    public class create_group_result : TBase
    {
      private long success;
      private rtmGatedException ex;
      public rtmGatedService.create_group_result.Isset __isset;

      public long Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.I64)
                {
                  this.success = iprot.ReadI64();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (create_group_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.I64;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("create_group_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class join_group_args : TBase
    {
      private long group_id;
      public rtmGatedService.join_group_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.I64)
              {
                this.group_id = iprot.ReadI64();
                this.__isset.group_id = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (join_group_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("join_group_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
      }
    }

    [Serializable]
    public class join_group_result : TBase
    {
      private long success;
      private rtmGatedException ex;
      public rtmGatedService.join_group_result.Isset __isset;

      public long Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.I64)
                {
                  this.success = iprot.ReadI64();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (join_group_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.I64;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.success);
          oprot.WriteFieldEnd();
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("join_group_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class delete_group_args : TBase
    {
      private long group_id;
      public rtmGatedService.delete_group_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.I64)
              {
                this.group_id = iprot.ReadI64();
                this.__isset.group_id = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (delete_group_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("delete_group_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
      }
    }

    [Serializable]
    public class delete_group_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.delete_group_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (delete_group_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("delete_group_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }

    [Serializable]
    public class group_changed_args : TBase
    {
      private byte otype;
      private long group_id;
      private List<long> uids;
      public rtmGatedService.group_changed_args.Isset __isset;

      public byte Otype
      {
        get
        {
          return this.otype;
        }
        set
        {
          this.__isset.otype = true;
          this.otype = value;
        }
      }

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public List<long> Uids
      {
        get
        {
          return this.uids;
        }
        set
        {
          this.__isset.uids = true;
          this.uids = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.Byte)
                {
                  this.otype = iprot.ReadByte();
                  this.__isset.otype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.List)
                {
                  this.uids = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.uids.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.uids = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (group_changed_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.otype)
        {
          field.Name = "otype";
          field.Type = TType.Byte;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.otype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.uids != null && this.__isset.uids)
        {
          field.Name = "uids";
          field.Type = TType.List;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.uids.Count));
          using (List<long>.Enumerator enumerator = this.uids.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("group_changed_args(");
        stringBuilder.Append("otype: ");
        stringBuilder.Append(this.otype);
        stringBuilder.Append(",group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",uids: ");
        stringBuilder.Append((object) this.uids);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool otype;
        public bool group_id;
        public bool uids;
      }
    }

    [Serializable]
    public class group_changed_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.group_changed_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (group_changed_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("group_changed_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }

    [Serializable]
    public class check_offline_msg_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (check_offline_msg_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("check_offline_msg_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class check_offline_msg_result : TBase
    {
      private List<MsgNum> success;
      private rtmGatedException ex;
      public rtmGatedService.check_offline_msg_result.Isset __isset;

      public List<MsgNum> Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.List)
                {
                  this.success = new List<MsgNum>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                  {
                    MsgNum msgNum1 = new MsgNum();
                    MsgNum msgNum2 = new MsgNum();
                    msgNum2.Read(iprot);
                    this.success.Add(msgNum2);
                  }
                  iprot.ReadListEnd();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (check_offline_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.List;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            oprot.WriteListBegin(new TList(TType.Struct, this.success.Count));
            using (List<MsgNum>.Enumerator enumerator = this.success.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                enumerator.Current.Write(oprot);
                oprot.WriteListEnd();
              }
            }
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("check_offline_msg_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append((object) this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_history_msg_args : TBase
    {
      private MsgParam param_list;
      public rtmGatedService.get_history_msg_args.Isset __isset;

      public MsgParam Param_list
      {
        get
        {
          return this.param_list;
        }
        set
        {
          this.__isset.param_list = true;
          this.param_list = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.param_list = new MsgParam();
                this.param_list.Read(iprot);
                this.__isset.param_list = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_history_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.param_list != null && this.__isset.param_list)
        {
          field.Name = "param_list";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.param_list.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_history_msg_args(");
        stringBuilder.Append("param_list: ");
        stringBuilder.Append(this.param_list != null ? this.param_list.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool param_list;
      }
    }

    [Serializable]
    public class get_history_msg_result : TBase
    {
      private MsgResult success;
      private rtmGatedException ex;
      public rtmGatedService.get_history_msg_result.Isset __isset;

      public MsgResult Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Struct)
                {
                  this.success = new MsgResult();
                  this.success.Read(iprot);
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_history_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.Struct;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            this.success.Write(oprot);
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_history_msg_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success != null ? this.success.ToString() : "<null>");
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_history_msg_new_args : TBase
    {
      private MsgParamNew param_list;
      public rtmGatedService.get_history_msg_new_args.Isset __isset;

      public MsgParamNew Param_list
      {
        get
        {
          return this.param_list;
        }
        set
        {
          this.__isset.param_list = true;
          this.param_list = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.param_list = new MsgParamNew();
                this.param_list.Read(iprot);
                this.__isset.param_list = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_history_msg_new_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.param_list != null && this.__isset.param_list)
        {
          field.Name = "param_list";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.param_list.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_history_msg_new_args(");
        stringBuilder.Append("param_list: ");
        stringBuilder.Append(this.param_list != null ? this.param_list.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool param_list;
      }
    }

    [Serializable]
    public class get_history_msg_new_result : TBase
    {
      private MsgResult success;
      private rtmGatedException ex;
      public rtmGatedService.get_history_msg_new_result.Isset __isset;

      public MsgResult Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Struct)
                {
                  this.success = new MsgResult();
                  this.success.Read(iprot);
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_history_msg_new_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.Struct;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            this.success.Write(oprot);
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_history_msg_new_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success != null ? this.success.ToString() : "<null>");
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_p2p_history_msg_args : TBase
    {
      private MsgParam param_list;
      public rtmGatedService.get_p2p_history_msg_args.Isset __isset;

      public MsgParam Param_list
      {
        get
        {
          return this.param_list;
        }
        set
        {
          this.__isset.param_list = true;
          this.param_list = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.param_list = new MsgParam();
                this.param_list.Read(iprot);
                this.__isset.param_list = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_p2p_history_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.param_list != null && this.__isset.param_list)
        {
          field.Name = "param_list";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.param_list.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_p2p_history_msg_args(");
        stringBuilder.Append("param_list: ");
        stringBuilder.Append(this.param_list != null ? this.param_list.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool param_list;
      }
    }

    [Serializable]
    public class get_p2p_history_msg_result : TBase
    {
      private MsgResult success;
      private rtmGatedException ex;
      public rtmGatedService.get_p2p_history_msg_result.Isset __isset;

      public MsgResult Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Struct)
                {
                  this.success = new MsgResult();
                  this.success.Read(iprot);
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_p2p_history_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.Struct;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            this.success.Write(oprot);
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_p2p_history_msg_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success != null ? this.success.ToString() : "<null>");
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_p2p_history_msg_new_args : TBase
    {
      private MsgParamP2P param_list;
      public rtmGatedService.get_p2p_history_msg_new_args.Isset __isset;

      public MsgParamP2P Param_list
      {
        get
        {
          return this.param_list;
        }
        set
        {
          this.__isset.param_list = true;
          this.param_list = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.param_list = new MsgParamP2P();
                this.param_list.Read(iprot);
                this.__isset.param_list = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_p2p_history_msg_new_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.param_list != null && this.__isset.param_list)
        {
          field.Name = "param_list";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.param_list.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_p2p_history_msg_new_args(");
        stringBuilder.Append("param_list: ");
        stringBuilder.Append(this.param_list != null ? this.param_list.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool param_list;
      }
    }

    [Serializable]
    public class get_p2p_history_msg_new_result : TBase
    {
      private MsgResultP2P success;
      private rtmGatedException ex;
      public rtmGatedService.get_p2p_history_msg_new_result.Isset __isset;

      public MsgResultP2P Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Struct)
                {
                  this.success = new MsgResultP2P();
                  this.success.Read(iprot);
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_p2p_history_msg_new_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.Struct;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            this.success.Write(oprot);
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_p2p_history_msg_new_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success != null ? this.success.ToString() : "<null>");
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_group_history_msg_new_args : TBase
    {
      private MsgParamGroup param_list;
      public rtmGatedService.get_group_history_msg_new_args.Isset __isset;

      public MsgParamGroup Param_list
      {
        get
        {
          return this.param_list;
        }
        set
        {
          this.__isset.param_list = true;
          this.param_list = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.param_list = new MsgParamGroup();
                this.param_list.Read(iprot);
                this.__isset.param_list = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_group_history_msg_new_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.param_list != null && this.__isset.param_list)
        {
          field.Name = "param_list";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.param_list.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_group_history_msg_new_args(");
        stringBuilder.Append("param_list: ");
        stringBuilder.Append(this.param_list != null ? this.param_list.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool param_list;
      }
    }

    [Serializable]
    public class get_group_history_msg_new_result : TBase
    {
      private MsgResultGroup success;
      private rtmGatedException ex;
      public rtmGatedService.get_group_history_msg_new_result.Isset __isset;

      public MsgResultGroup Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.Struct)
                {
                  this.success = new MsgResultGroup();
                  this.success.Read(iprot);
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_group_history_msg_new_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.Struct;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            this.success.Write(oprot);
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_group_history_msg_new_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success != null ? this.success.ToString() : "<null>");
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_online_users_args : TBase
    {
      private List<long> uids;
      public rtmGatedService.get_online_users_args.Isset __isset;

      public List<long> Uids
      {
        get
        {
          return this.uids;
        }
        set
        {
          this.__isset.uids = true;
          this.uids = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.List)
              {
                this.uids = new List<long>();
                TList tlist = iprot.ReadListBegin();
                for (int index = 0; index < tlist.Count; ++index)
                  this.uids.Add(iprot.ReadI64());
                iprot.ReadListEnd();
                this.__isset.uids = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_online_users_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.uids != null && this.__isset.uids)
        {
          field.Name = "uids";
          field.Type = TType.List;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.uids.Count));
          using (List<long>.Enumerator enumerator = this.uids.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_online_users_args(");
        stringBuilder.Append("uids: ");
        stringBuilder.Append((object) this.uids);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool uids;
      }
    }

    [Serializable]
    public class get_online_users_result : TBase
    {
      private List<long> success;
      private rtmGatedException ex;
      public rtmGatedService.get_online_users_result.Isset __isset;

      public List<long> Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.List)
                {
                  this.success = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.success.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_online_users_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.List;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            oprot.WriteListBegin(new TList(TType.I64, this.success.Count));
            using (List<long>.Enumerator enumerator = this.success.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                long current = enumerator.Current;
                oprot.WriteI64(current);
                oprot.WriteListEnd();
              }
            }
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_online_users_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append((object) this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class get_group_online_users_args : TBase
    {
      private long group_id;
      public rtmGatedService.get_group_online_users_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.I64)
              {
                this.group_id = iprot.ReadI64();
                this.__isset.group_id = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_group_online_users_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_group_online_users_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
      }
    }

    [Serializable]
    public class get_group_online_users_result : TBase
    {
      private List<long> success;
      private rtmGatedException ex;
      public rtmGatedService.get_group_online_users_result.Isset __isset;

      public List<long> Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 0:
                if (tfield.Type == TType.List)
                {
                  this.success = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.success.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.success = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 1:
                if (tfield.Type == TType.Struct)
                {
                  this.ex = new rtmGatedException();
                  this.ex.Read(iprot);
                  this.__isset.ex = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (get_group_online_users_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          if (this.success != null)
          {
            field.Name = "success";
            field.Type = TType.List;
            field.ID = (short) 0;
            oprot.WriteFieldBegin(field);
            oprot.WriteListBegin(new TList(TType.I64, this.success.Count));
            using (List<long>.Enumerator enumerator = this.success.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                long current = enumerator.Current;
                oprot.WriteI64(current);
                oprot.WriteListEnd();
              }
            }
            oprot.WriteFieldEnd();
          }
        }
        else if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("get_group_online_users_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append((object) this.success);
        stringBuilder.Append(",ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
        public bool ex;
      }
    }

    [Serializable]
    public class send_broadcast_group_msg_args : TBase
    {
      private long group_id;
      private long mid;
      private byte mtype;
      private string message;
      public rtmGatedService.send_broadcast_group_msg_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_broadcast_group_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_broadcast_group_msg_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
        public bool mid;
        public bool mtype;
        public bool message;
      }
    }

    [Serializable]
    public class send_broadcast_group_msg_result : TBase
    {
      private rtmGatedException ex;
      public rtmGatedService.send_broadcast_group_msg_result.Isset __isset;

      public rtmGatedException Ex
      {
        get
        {
          return this.ex;
        }
        set
        {
          this.__isset.ex = true;
          this.ex = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Struct)
              {
                this.ex = new rtmGatedException();
                this.ex.Read(iprot);
                this.__isset.ex = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (send_broadcast_group_msg_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.ex && this.ex != null)
        {
          field.Name = "ex";
          field.Type = TType.Struct;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          this.ex.Write(oprot);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("send_broadcast_group_msg_result(");
        stringBuilder.Append("ex: ");
        stringBuilder.Append(this.ex != null ? this.ex.ToString() : "<null>");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool ex;
      }
    }
  }
}
