﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.rtmSrvPushService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Thrift;
using Thrift.Collections;
using Thrift.Protocol;

namespace Rtm.Connection
{
  public class rtmSrvPushService
  {
    public interface Iface
    {
      void drop(int project_id, long to);

      void kickout(int project_id, long to, string traceInfo);

      void push_note(int project_id, long to, byte mtype, long from, string message, int time);

      void push_notes(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time);

      void push_group_note(int project_id, THashSet<long> to, byte mtype, long group_id, long from, string message, int time);

      void push_broadcast_note(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time);

      void push_msg(int project_id, long to, long from, byte mtype, string message, long mid, int time, string traceInfo);

      void push_msgs(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo);

      void push_group_msg(int project_id, THashSet<long> to, long group_id, long from, byte mtype, string message, long mid, int time, string traceInfo);

      void user_status(int project_id, Dictionary<long, Dictionary<long, bool>> status);

      void msg_receipt(int project_id, long to, long from, long mid, byte state);

      void push_broadcast_msg(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo);

      void push_group_broadcast_msg(int project_id, long from, long group_id, byte mtype, string message, long mid, int time, string traceInfo);
    }

    public class Client : rtmSrvPushService.Iface
    {
      protected TProtocol iprot_;
      protected TProtocol oprot_;
      protected int seqid_;

      public Client(TProtocol prot)
        : this(prot, prot)
      {
      }

      public Client(TProtocol iprot, TProtocol oprot)
      {
        this.iprot_ = iprot;
        this.oprot_ = oprot;
      }

      public TProtocol InputProtocol
      {
        get
        {
          return this.iprot_;
        }
      }

      public TProtocol OutputProtocol
      {
        get
        {
          return this.oprot_;
        }
      }

      public void drop(int project_id, long to)
      {
        this.send_drop(project_id, to);
      }

      public void send_drop(int project_id, long to)
      {
        this.oprot_.WriteMessageBegin(new TMessage("drop", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.drop_args()
        {
          Project_id = project_id,
          To = to
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void kickout(int project_id, long to, string traceInfo)
      {
        this.send_kickout(project_id, to, traceInfo);
      }

      public void send_kickout(int project_id, long to, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("kickout", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.kickout_args()
        {
          Project_id = project_id,
          To = to,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_note(int project_id, long to, byte mtype, long from, string message, int time)
      {
        this.send_push_note(project_id, to, mtype, from, message, time);
      }

      public void send_push_note(int project_id, long to, byte mtype, long from, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_note", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_note_args()
        {
          Project_id = project_id,
          To = to,
          Mtype = mtype,
          From = from,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_notes(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time)
      {
        this.send_push_notes(project_id, tos, from, mtype, message, time);
      }

      public void send_push_notes(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_notes", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_notes_args()
        {
          Project_id = project_id,
          Tos = tos,
          From = from,
          Mtype = mtype,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_group_note(int project_id, THashSet<long> to, byte mtype, long group_id, long from, string message, int time)
      {
        this.send_push_group_note(project_id, to, mtype, group_id, from, message, time);
      }

      public void send_push_group_note(int project_id, THashSet<long> to, byte mtype, long group_id, long from, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_group_note", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_group_note_args()
        {
          Project_id = project_id,
          To = to,
          Mtype = mtype,
          Group_id = group_id,
          From = from,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_broadcast_note(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time)
      {
        this.send_push_broadcast_note(project_id, tos, from, mtype, message, time);
      }

      public void send_push_broadcast_note(int project_id, THashSet<long> tos, long from, byte mtype, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_broadcast_note", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_broadcast_note_args()
        {
          Project_id = project_id,
          Tos = tos,
          From = from,
          Mtype = mtype,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_msg(int project_id, long to, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.send_push_msg(project_id, to, from, mtype, message, mid, time, traceInfo);
        this.recv_push_msg();
      }

      public void send_push_msg(int project_id, long to, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_msg", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_msg_args()
        {
          Project_id = project_id,
          To = to,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmSrvPushService.push_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_msgs(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.send_push_msgs(project_id, tos, from, mtype, message, mid, time, traceInfo);
        this.recv_push_msgs();
      }

      public void send_push_msgs(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_msgs", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_msgs_args()
        {
          Project_id = project_id,
          Tos = tos,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_msgs()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmSrvPushService.push_msgs_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_group_msg(int project_id, THashSet<long> to, long group_id, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.send_push_group_msg(project_id, to, group_id, from, mtype, message, mid, time, traceInfo);
        this.recv_push_group_msg();
      }

      public void send_push_group_msg(int project_id, THashSet<long> to, long group_id, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_group_msg", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_group_msg_args()
        {
          Project_id = project_id,
          To = to,
          Group_id = group_id,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_group_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmSrvPushService.push_group_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void user_status(int project_id, Dictionary<long, Dictionary<long, bool>> status)
      {
        this.send_user_status(project_id, status);
      }

      public void send_user_status(int project_id, Dictionary<long, Dictionary<long, bool>> status)
      {
        this.oprot_.WriteMessageBegin(new TMessage("user_status", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.user_status_args()
        {
          Project_id = project_id,
          Status = status
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void msg_receipt(int project_id, long to, long from, long mid, byte state)
      {
        this.send_msg_receipt(project_id, to, from, mid, state);
      }

      public void send_msg_receipt(int project_id, long to, long from, long mid, byte state)
      {
        this.oprot_.WriteMessageBegin(new TMessage("msg_receipt", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.msg_receipt_args()
        {
          Project_id = project_id,
          To = to,
          From = from,
          Mid = mid,
          State = state
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_broadcast_msg(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.send_push_broadcast_msg(project_id, tos, from, mtype, message, mid, time, traceInfo);
        this.recv_push_broadcast_msg();
      }

      public void send_push_broadcast_msg(int project_id, THashSet<long> tos, long from, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_broadcast_msg", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_broadcast_msg_args()
        {
          Project_id = project_id,
          Tos = tos,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_broadcast_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmSrvPushService.push_broadcast_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_group_broadcast_msg(int project_id, long from, long group_id, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.send_push_group_broadcast_msg(project_id, from, group_id, mtype, message, mid, time, traceInfo);
        this.recv_push_group_broadcast_msg();
      }

      public void send_push_group_broadcast_msg(int project_id, long from, long group_id, byte mtype, string message, long mid, int time, string traceInfo)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_group_broadcast_msg", TMessageType.Call, this.seqid_));
        new rtmSrvPushService.push_group_broadcast_msg_args()
        {
          Project_id = project_id,
          From = from,
          Group_id = group_id,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time,
          TraceInfo = traceInfo
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_group_broadcast_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmSrvPushService.push_group_broadcast_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }
    }

    public class Processor : TProcessor
    {
      protected Dictionary<string, rtmSrvPushService.Processor.ProcessFunction> processMap_ = new Dictionary<string, rtmSrvPushService.Processor.ProcessFunction>();
      private rtmSrvPushService.Iface iface_;

      public Processor(rtmSrvPushService.Iface iface)
      {
        this.iface_ = iface;
        this.processMap_["drop"] = new rtmSrvPushService.Processor.ProcessFunction(this.drop_Process);
        this.processMap_["kickout"] = new rtmSrvPushService.Processor.ProcessFunction(this.kickout_Process);
        this.processMap_["push_note"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_note_Process);
        this.processMap_["push_notes"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_notes_Process);
        this.processMap_["push_group_note"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_group_note_Process);
        this.processMap_["push_broadcast_note"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_broadcast_note_Process);
        this.processMap_["push_msg"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_msg_Process);
        this.processMap_["push_msgs"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_msgs_Process);
        this.processMap_["push_group_msg"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_group_msg_Process);
        this.processMap_["user_status"] = new rtmSrvPushService.Processor.ProcessFunction(this.user_status_Process);
        this.processMap_["msg_receipt"] = new rtmSrvPushService.Processor.ProcessFunction(this.msg_receipt_Process);
        this.processMap_["push_broadcast_msg"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_broadcast_msg_Process);
        this.processMap_["push_group_broadcast_msg"] = new rtmSrvPushService.Processor.ProcessFunction(this.push_group_broadcast_msg_Process);
      }

      public bool Process(TProtocol iprot, TProtocol oprot)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          rtmSrvPushService.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
          {
            TProtocolUtil.Skip(iprot, TType.Struct);
            iprot.ReadMessageEnd();
            TApplicationException tapplicationException = new TApplicationException(TApplicationException.ExceptionType.UnknownMethod, "Invalid method name: '" + tmessage.Name + "'");
            oprot.WriteMessageBegin(new TMessage(tmessage.Name, TMessageType.Exception, tmessage.SeqID));
            tapplicationException.Write(oprot);
            oprot.WriteMessageEnd();
            oprot.Transport.Flush();
            return true;
          }
          processFunction(tmessage.SeqID, iprot, oprot);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public void drop_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.drop_args dropArgs = new rtmSrvPushService.drop_args();
        dropArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.drop(dropArgs.Project_id, dropArgs.To);
      }

      public void kickout_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.kickout_args kickoutArgs = new rtmSrvPushService.kickout_args();
        kickoutArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.kickout(kickoutArgs.Project_id, kickoutArgs.To, kickoutArgs.TraceInfo);
      }

      public void push_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_note_args pushNoteArgs = new rtmSrvPushService.push_note_args();
        pushNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_note(pushNoteArgs.Project_id, pushNoteArgs.To, pushNoteArgs.Mtype, pushNoteArgs.From, pushNoteArgs.Message, pushNoteArgs.Time);
      }

      public void push_notes_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_notes_args pushNotesArgs = new rtmSrvPushService.push_notes_args();
        pushNotesArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_notes(pushNotesArgs.Project_id, pushNotesArgs.Tos, pushNotesArgs.From, pushNotesArgs.Mtype, pushNotesArgs.Message, pushNotesArgs.Time);
      }

      public void push_group_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_group_note_args pushGroupNoteArgs = new rtmSrvPushService.push_group_note_args();
        pushGroupNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_group_note(pushGroupNoteArgs.Project_id, pushGroupNoteArgs.To, pushGroupNoteArgs.Mtype, pushGroupNoteArgs.Group_id, pushGroupNoteArgs.From, pushGroupNoteArgs.Message, pushGroupNoteArgs.Time);
      }

      public void push_broadcast_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_broadcast_note_args broadcastNoteArgs = new rtmSrvPushService.push_broadcast_note_args();
        broadcastNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_broadcast_note(broadcastNoteArgs.Project_id, broadcastNoteArgs.Tos, broadcastNoteArgs.From, broadcastNoteArgs.Mtype, broadcastNoteArgs.Message, broadcastNoteArgs.Time);
      }

      public void push_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_msg_args pushMsgArgs = new rtmSrvPushService.push_msg_args();
        pushMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmSrvPushService.push_msg_result pushMsgResult = new rtmSrvPushService.push_msg_result();
        this.iface_.push_msg(pushMsgArgs.Project_id, pushMsgArgs.To, pushMsgArgs.From, pushMsgArgs.Mtype, pushMsgArgs.Message, pushMsgArgs.Mid, pushMsgArgs.Time, pushMsgArgs.TraceInfo);
        oprot.WriteMessageBegin(new TMessage("push_msg", TMessageType.Reply, seqid));
        pushMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_msgs_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_msgs_args pushMsgsArgs = new rtmSrvPushService.push_msgs_args();
        pushMsgsArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmSrvPushService.push_msgs_result pushMsgsResult = new rtmSrvPushService.push_msgs_result();
        this.iface_.push_msgs(pushMsgsArgs.Project_id, pushMsgsArgs.Tos, pushMsgsArgs.From, pushMsgsArgs.Mtype, pushMsgsArgs.Message, pushMsgsArgs.Mid, pushMsgsArgs.Time, pushMsgsArgs.TraceInfo);
        oprot.WriteMessageBegin(new TMessage("push_msgs", TMessageType.Reply, seqid));
        pushMsgsResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_group_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_group_msg_args pushGroupMsgArgs = new rtmSrvPushService.push_group_msg_args();
        pushGroupMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmSrvPushService.push_group_msg_result pushGroupMsgResult = new rtmSrvPushService.push_group_msg_result();
        this.iface_.push_group_msg(pushGroupMsgArgs.Project_id, pushGroupMsgArgs.To, pushGroupMsgArgs.Group_id, pushGroupMsgArgs.From, pushGroupMsgArgs.Mtype, pushGroupMsgArgs.Message, pushGroupMsgArgs.Mid, pushGroupMsgArgs.Time, pushGroupMsgArgs.TraceInfo);
        oprot.WriteMessageBegin(new TMessage("push_group_msg", TMessageType.Reply, seqid));
        pushGroupMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void user_status_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.user_status_args userStatusArgs = new rtmSrvPushService.user_status_args();
        userStatusArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.user_status(userStatusArgs.Project_id, userStatusArgs.Status);
      }

      public void msg_receipt_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.msg_receipt_args msgReceiptArgs = new rtmSrvPushService.msg_receipt_args();
        msgReceiptArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.msg_receipt(msgReceiptArgs.Project_id, msgReceiptArgs.To, msgReceiptArgs.From, msgReceiptArgs.Mid, msgReceiptArgs.State);
      }

      public void push_broadcast_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_broadcast_msg_args broadcastMsgArgs = new rtmSrvPushService.push_broadcast_msg_args();
        broadcastMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmSrvPushService.push_broadcast_msg_result broadcastMsgResult = new rtmSrvPushService.push_broadcast_msg_result();
        this.iface_.push_broadcast_msg(broadcastMsgArgs.Project_id, broadcastMsgArgs.Tos, broadcastMsgArgs.From, broadcastMsgArgs.Mtype, broadcastMsgArgs.Message, broadcastMsgArgs.Mid, broadcastMsgArgs.Time, broadcastMsgArgs.TraceInfo);
        oprot.WriteMessageBegin(new TMessage("push_broadcast_msg", TMessageType.Reply, seqid));
        broadcastMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_group_broadcast_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmSrvPushService.push_group_broadcast_msg_args broadcastMsgArgs = new rtmSrvPushService.push_group_broadcast_msg_args();
        broadcastMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmSrvPushService.push_group_broadcast_msg_result broadcastMsgResult = new rtmSrvPushService.push_group_broadcast_msg_result();
        this.iface_.push_group_broadcast_msg(broadcastMsgArgs.Project_id, broadcastMsgArgs.From, broadcastMsgArgs.Group_id, broadcastMsgArgs.Mtype, broadcastMsgArgs.Message, broadcastMsgArgs.Mid, broadcastMsgArgs.Time, broadcastMsgArgs.TraceInfo);
        oprot.WriteMessageBegin(new TMessage("push_group_broadcast_msg", TMessageType.Reply, seqid));
        broadcastMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      protected delegate void ProcessFunction(int seqid, TProtocol iprot, TProtocol oprot);
    }

    [Serializable]
    public class drop_args : TBase
    {
      private int project_id;
      private long to;
      public rtmSrvPushService.drop_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (drop_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("drop_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
      }
    }

    [Serializable]
    public class kickout_args : TBase
    {
      private int project_id;
      private long to;
      private string traceInfo;
      public rtmSrvPushService.kickout_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (kickout_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("kickout_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_note_args : TBase
    {
      private int project_id;
      private long to;
      private byte mtype;
      private long from;
      private string message;
      private int time;
      public rtmSrvPushService.push_note_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_note_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool mtype;
        public bool from;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_notes_args : TBase
    {
      private int project_id;
      private THashSet<long> tos;
      private long from;
      private byte mtype;
      private string message;
      private int time;
      public rtmSrvPushService.push_notes_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.tos = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_notes_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.tos.Count));
          foreach (long to in this.tos)
          {
            oprot.WriteI64(to);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_notes_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool tos;
        public bool from;
        public bool mtype;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_group_note_args : TBase
    {
      private int project_id;
      private THashSet<long> to;
      private byte mtype;
      private long group_id;
      private long from;
      private string message;
      private int time;
      public rtmSrvPushService.push_group_note_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.to = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.to.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.to != null && this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.to.Count));
          foreach (long i64 in this.to)
          {
            oprot.WriteI64(i64);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_note_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append((object) this.to);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool mtype;
        public bool group_id;
        public bool from;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_broadcast_note_args : TBase
    {
      private int project_id;
      private THashSet<long> tos;
      private long from;
      private byte mtype;
      private string message;
      private int time;
      public rtmSrvPushService.push_broadcast_note_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.tos = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.tos.Count));
          foreach (long to in this.tos)
          {
            oprot.WriteI64(to);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_note_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool tos;
        public bool from;
        public bool mtype;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_msg_args : TBase
    {
      private int project_id;
      private long to;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      private string traceInfo;
      public rtmSrvPushService.push_msg_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 8:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 8;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msg_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_msgs_args : TBase
    {
      private int project_id;
      private THashSet<long> tos;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      private string traceInfo;
      public rtmSrvPushService.push_msgs_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.tos = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 8:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msgs_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.tos.Count));
          foreach (long to in this.tos)
          {
            oprot.WriteI64(to);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 8;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msgs_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool tos;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_msgs_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msgs_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msgs_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_group_msg_args : TBase
    {
      private int project_id;
      private THashSet<long> to;
      private long group_id;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      private string traceInfo;
      public rtmSrvPushService.push_group_msg_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.to = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.to.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 8:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 9:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.to != null && this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.to.Count));
          foreach (long i64 in this.to)
          {
            oprot.WriteI64(i64);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 8;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 9;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_msg_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append((object) this.to);
        stringBuilder.Append(",group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool group_id;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_group_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class user_status_args : TBase
    {
      private int project_id;
      private Dictionary<long, Dictionary<long, bool>> status;
      public rtmSrvPushService.user_status_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public Dictionary<long, Dictionary<long, bool>> Status
      {
        get
        {
          return this.status;
        }
        set
        {
          this.__isset.status = true;
          this.status = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Map)
                {
                  this.status = new Dictionary<long, Dictionary<long, bool>>();
                  TMap tmap1 = iprot.ReadMapBegin();
                  for (int index1 = 0; index1 < tmap1.Count; ++index1)
                  {
                    long index2 = iprot.ReadI64();
                    Dictionary<long, bool> dictionary = new Dictionary<long, bool>();
                    TMap tmap2 = iprot.ReadMapBegin();
                    for (int index3 = 0; index3 < tmap2.Count; ++index3)
                    {
                      long index4 = iprot.ReadI64();
                      bool flag = iprot.ReadBool();
                      dictionary[index4] = flag;
                    }
                    iprot.ReadMapEnd();
                    this.status[index2] = dictionary;
                  }
                  iprot.ReadMapEnd();
                  this.__isset.status = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (user_status_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.status != null && this.__isset.status)
        {
          field.Name = "status";
          field.Type = TType.Map;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteMapBegin(new TMap(TType.I64, TType.Map, this.status.Count));
          using (Dictionary<long, Dictionary<long, bool>>.KeyCollection.Enumerator enumerator1 = this.status.Keys.GetEnumerator())
          {
            while (enumerator1.MoveNext())
            {
              long current1 = enumerator1.Current;
              oprot.WriteI64(current1);
              oprot.WriteMapBegin(new TMap(TType.I64, TType.Bool, this.status[current1].Count));
              using (Dictionary<long, bool>.KeyCollection.Enumerator enumerator2 = this.status[current1].Keys.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                {
                  long current2 = enumerator2.Current;
                  oprot.WriteI64(current2);
                  oprot.WriteBool(this.status[current1][current2]);
                  oprot.WriteMapEnd();
                }
              }
              oprot.WriteMapEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("user_status_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",status: ");
        stringBuilder.Append((object) this.status);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool status;
      }
    }

    [Serializable]
    public class msg_receipt_args : TBase
    {
      private int project_id;
      private long to;
      private long from;
      private long mid;
      private byte state;
      public rtmSrvPushService.msg_receipt_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long To
      {
        get
        {
          return this.to;
        }
        set
        {
          this.__isset.to = true;
          this.to = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte State
      {
        get
        {
          return this.state;
        }
        set
        {
          this.__isset.state = true;
          this.state = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.to = iprot.ReadI64();
                  this.__isset.to = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.Byte)
                {
                  this.state = iprot.ReadByte();
                  this.__isset.state = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (msg_receipt_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.to)
        {
          field.Name = "to";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.to);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.state)
        {
          field.Name = "state";
          field.Type = TType.Byte;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.state);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("msg_receipt_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",to: ");
        stringBuilder.Append(this.to);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",state: ");
        stringBuilder.Append(this.state);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool to;
        public bool from;
        public bool mid;
        public bool state;
      }
    }

    [Serializable]
    public class push_broadcast_msg_args : TBase
    {
      private int project_id;
      private THashSet<long> tos;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      private string traceInfo;
      public rtmSrvPushService.push_broadcast_msg_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public THashSet<long> Tos
      {
        get
        {
          return this.tos;
        }
        set
        {
          this.__isset.tos = true;
          this.tos = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Set)
                {
                  this.tos = new THashSet<long>();
                  TSet tset = iprot.ReadSetBegin();
                  for (int index = 0; index < tset.Count; ++index)
                    this.tos.Add(iprot.ReadI64());
                  iprot.ReadSetEnd();
                  this.__isset.tos = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 8:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.tos != null && this.__isset.tos)
        {
          field.Name = "tos";
          field.Type = TType.Set;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteSetBegin(new TSet(TType.I64, this.tos.Count));
          foreach (long to in this.tos)
          {
            oprot.WriteI64(to);
            oprot.WriteSetEnd();
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 8;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_msg_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",tos: ");
        stringBuilder.Append((object) this.tos);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool tos;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_broadcast_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_group_broadcast_msg_args : TBase
    {
      private int project_id;
      private long from;
      private long group_id;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      private string traceInfo;
      public rtmSrvPushService.push_group_broadcast_msg_args.Isset __isset;

      public int Project_id
      {
        get
        {
          return this.project_id;
        }
        set
        {
          this.__isset.project_id = true;
          this.project_id = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public string TraceInfo
      {
        get
        {
          return this.traceInfo;
        }
        set
        {
          this.__isset.traceInfo = true;
          this.traceInfo = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I32)
                {
                  this.project_id = iprot.ReadI32();
                  this.__isset.project_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 7:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 8:
                if (tfield.Type == TType.String)
                {
                  this.traceInfo = iprot.ReadString();
                  this.__isset.traceInfo = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_broadcast_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.project_id)
        {
          field.Name = "project_id";
          field.Type = TType.I32;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.project_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 7;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        if (this.traceInfo != null && this.__isset.traceInfo)
        {
          field.Name = "traceInfo";
          field.Type = TType.String;
          field.ID = (short) 8;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.traceInfo);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_broadcast_msg_args(");
        stringBuilder.Append("project_id: ");
        stringBuilder.Append(this.project_id);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(",traceInfo: ");
        stringBuilder.Append(this.traceInfo);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool project_id;
        public bool from;
        public bool group_id;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
        public bool traceInfo;
      }
    }

    [Serializable]
    public class push_group_broadcast_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_broadcast_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_broadcast_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }
  }
}
