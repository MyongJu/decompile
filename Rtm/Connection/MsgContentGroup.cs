﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.MsgContentGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class MsgContentGroup : TBase
  {
    private long id;
    private long from_uid;
    private long mid;
    private byte mtype;
    private string msg;
    private int mtime;
    public MsgContentGroup.Isset __isset;

    public long Id
    {
      get
      {
        return this.id;
      }
      set
      {
        this.__isset.id = true;
        this.id = value;
      }
    }

    public long From_uid
    {
      get
      {
        return this.from_uid;
      }
      set
      {
        this.__isset.from_uid = true;
        this.from_uid = value;
      }
    }

    public long Mid
    {
      get
      {
        return this.mid;
      }
      set
      {
        this.__isset.mid = true;
        this.mid = value;
      }
    }

    public byte Mtype
    {
      get
      {
        return this.mtype;
      }
      set
      {
        this.__isset.mtype = true;
        this.mtype = value;
      }
    }

    public string Msg
    {
      get
      {
        return this.msg;
      }
      set
      {
        this.__isset.msg = true;
        this.msg = value;
      }
    }

    public int Mtime
    {
      get
      {
        return this.mtime;
      }
      set
      {
        this.__isset.mtime = true;
        this.mtime = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.I64)
              {
                this.id = iprot.ReadI64();
                this.__isset.id = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I64)
              {
                this.from_uid = iprot.ReadI64();
                this.__isset.from_uid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 3:
              if (tfield.Type == TType.I64)
              {
                this.mid = iprot.ReadI64();
                this.__isset.mid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 4:
              if (tfield.Type == TType.Byte)
              {
                this.mtype = iprot.ReadByte();
                this.__isset.mtype = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 5:
              if (tfield.Type == TType.String)
              {
                this.msg = iprot.ReadString();
                this.__isset.msg = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 6:
              if (tfield.Type == TType.I32)
              {
                this.mtime = iprot.ReadI32();
                this.__isset.mtime = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (MsgContentGroup));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.id)
      {
        field.Name = "id";
        field.Type = TType.I64;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.id);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.from_uid)
      {
        field.Name = "from_uid";
        field.Type = TType.I64;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.from_uid);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.mid)
      {
        field.Name = "mid";
        field.Type = TType.I64;
        field.ID = (short) 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.mid);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.mtype)
      {
        field.Name = "mtype";
        field.Type = TType.Byte;
        field.ID = (short) 4;
        oprot.WriteFieldBegin(field);
        oprot.WriteByte(this.mtype);
        oprot.WriteFieldEnd();
      }
      if (this.msg != null && this.__isset.msg)
      {
        field.Name = "msg";
        field.Type = TType.String;
        field.ID = (short) 5;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(this.msg);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.mtime)
      {
        field.Name = "mtime";
        field.Type = TType.I32;
        field.ID = (short) 6;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.mtime);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("MsgContentGroup(");
      stringBuilder.Append("id: ");
      stringBuilder.Append(this.id);
      stringBuilder.Append(",from_uid: ");
      stringBuilder.Append(this.from_uid);
      stringBuilder.Append(",mid: ");
      stringBuilder.Append(this.mid);
      stringBuilder.Append(",mtype: ");
      stringBuilder.Append(this.mtype);
      stringBuilder.Append(",msg: ");
      stringBuilder.Append(this.msg);
      stringBuilder.Append(",mtime: ");
      stringBuilder.Append(this.mtime);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool id;
      public bool from_uid;
      public bool mid;
      public bool mtype;
      public bool msg;
      public bool mtime;
    }
  }
}
