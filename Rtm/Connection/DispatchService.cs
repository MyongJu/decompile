﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.DispatchService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Text;
using Thrift;
using Thrift.Protocol;

namespace Rtm.Connection
{
  public class DispatchService
  {
    public interface Iface : FunplusService.Iface
    {
      string which(string what, string proto, string version);
    }

    public class Client : FunplusService.Client, DispatchService.Iface, FunplusService.Iface
    {
      public Client(TProtocol prot)
        : this(prot, prot)
      {
      }

      public Client(TProtocol iprot, TProtocol oprot)
        : base(iprot, oprot)
      {
      }

      public string which(string what, string proto, string version)
      {
        this.send_which(what, proto, version);
        return this.recv_which();
      }

      public void send_which(string what, string proto, string version)
      {
        this.oprot_.WriteMessageBegin(new TMessage("which", TMessageType.Call, this.seqid_));
        new DispatchService.which_args()
        {
          What = what,
          Proto = proto,
          Version = version
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public string recv_which()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        DispatchService.which_result whichResult = new DispatchService.which_result();
        whichResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (whichResult.__isset.success)
          return whichResult.Success;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "which failed: unknown result");
      }
    }

    public class Processor : FunplusService.Processor, TProcessor
    {
      private DispatchService.Iface iface_;

      public Processor(DispatchService.Iface iface)
        : base((FunplusService.Iface) iface)
      {
        this.iface_ = iface;
        this.processMap_["which"] = new FunplusService.Processor.ProcessFunction(this.which_Process);
      }

      public new bool Process(TProtocol iprot, TProtocol oprot)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          FunplusService.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
          {
            TProtocolUtil.Skip(iprot, TType.Struct);
            iprot.ReadMessageEnd();
            TApplicationException tapplicationException = new TApplicationException(TApplicationException.ExceptionType.UnknownMethod, "Invalid method name: '" + tmessage.Name + "'");
            oprot.WriteMessageBegin(new TMessage(tmessage.Name, TMessageType.Exception, tmessage.SeqID));
            tapplicationException.Write(oprot);
            oprot.WriteMessageEnd();
            oprot.Transport.Flush();
            return true;
          }
          processFunction(tmessage.SeqID, iprot, oprot);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public void which_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        DispatchService.which_args whichArgs = new DispatchService.which_args();
        whichArgs.Read(iprot);
        iprot.ReadMessageEnd();
        DispatchService.which_result whichResult = new DispatchService.which_result();
        whichResult.Success = this.iface_.which(whichArgs.What, whichArgs.Proto, whichArgs.Version);
        oprot.WriteMessageBegin(new TMessage("which", TMessageType.Reply, seqid));
        whichResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }
    }

    [Serializable]
    public class which_args : TBase
    {
      private string what;
      private string proto;
      private string version;
      public DispatchService.which_args.Isset __isset;

      public string What
      {
        get
        {
          return this.what;
        }
        set
        {
          this.__isset.what = true;
          this.what = value;
        }
      }

      public string Proto
      {
        get
        {
          return this.proto;
        }
        set
        {
          this.__isset.proto = true;
          this.proto = value;
        }
      }

      public string Version
      {
        get
        {
          return this.version;
        }
        set
        {
          this.__isset.version = true;
          this.version = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.String)
                {
                  this.what = iprot.ReadString();
                  this.__isset.what = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.String)
                {
                  this.proto = iprot.ReadString();
                  this.__isset.proto = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.version = iprot.ReadString();
                  this.__isset.version = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (which_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.what != null && this.__isset.what)
        {
          field.Name = "what";
          field.Type = TType.String;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.what);
          oprot.WriteFieldEnd();
        }
        if (this.proto != null && this.__isset.proto)
        {
          field.Name = "proto";
          field.Type = TType.String;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.proto);
          oprot.WriteFieldEnd();
        }
        if (this.version != null && this.__isset.version)
        {
          field.Name = "version";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.version);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("which_args(");
        stringBuilder.Append("what: ");
        stringBuilder.Append(this.what);
        stringBuilder.Append(",proto: ");
        stringBuilder.Append(this.proto);
        stringBuilder.Append(",version: ");
        stringBuilder.Append(this.version);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool what;
        public bool proto;
        public bool version;
      }
    }

    [Serializable]
    public class which_result : TBase
    {
      private string success;
      public DispatchService.which_result.Isset __isset;

      public string Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 0)
            {
              if (tfield.Type == TType.String)
              {
                this.success = iprot.ReadString();
                this.__isset.success = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (which_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success && this.success != null)
        {
          field.Name = "success";
          field.Type = TType.String;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.success);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("which_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
      }
    }
  }
}
