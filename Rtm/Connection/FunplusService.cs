﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.FunplusService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Thrift;
using Thrift.Protocol;

namespace Rtm.Connection
{
  public class FunplusService
  {
    public interface Iface
    {
      bool status();

      Dictionary<string, string> infos();

      void tune(string key, string value);
    }

    public class Client : FunplusService.Iface
    {
      protected TProtocol iprot_;
      protected TProtocol oprot_;
      protected int seqid_;

      public Client(TProtocol prot)
        : this(prot, prot)
      {
      }

      public Client(TProtocol iprot, TProtocol oprot)
      {
        this.iprot_ = iprot;
        this.oprot_ = oprot;
      }

      public TProtocol InputProtocol
      {
        get
        {
          return this.iprot_;
        }
      }

      public TProtocol OutputProtocol
      {
        get
        {
          return this.oprot_;
        }
      }

      public bool status()
      {
        this.send_status();
        return this.recv_status();
      }

      public void send_status()
      {
        this.oprot_.WriteMessageBegin(new TMessage("status", TMessageType.Call, this.seqid_));
        new FunplusService.status_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public bool recv_status()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        FunplusService.status_result statusResult = new FunplusService.status_result();
        statusResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (statusResult.__isset.success)
          return statusResult.Success;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "status failed: unknown result");
      }

      public Dictionary<string, string> infos()
      {
        this.send_infos();
        return this.recv_infos();
      }

      public void send_infos()
      {
        this.oprot_.WriteMessageBegin(new TMessage("infos", TMessageType.Call, this.seqid_));
        new FunplusService.infos_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public Dictionary<string, string> recv_infos()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        FunplusService.infos_result infosResult = new FunplusService.infos_result();
        infosResult.Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
        if (infosResult.__isset.success)
          return infosResult.Success;
        throw new TApplicationException(TApplicationException.ExceptionType.MissingResult, "infos failed: unknown result");
      }

      public void tune(string key, string value)
      {
        this.send_tune(key, value);
        this.recv_tune();
      }

      public void send_tune(string key, string value)
      {
        this.oprot_.WriteMessageBegin(new TMessage("tune", TMessageType.Call, this.seqid_));
        new FunplusService.tune_args()
        {
          Key = key,
          Value = value
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_tune()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new FunplusService.tune_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }
    }

    public class Processor : TProcessor
    {
      protected Dictionary<string, FunplusService.Processor.ProcessFunction> processMap_ = new Dictionary<string, FunplusService.Processor.ProcessFunction>();
      private FunplusService.Iface iface_;

      public Processor(FunplusService.Iface iface)
      {
        this.iface_ = iface;
        this.processMap_["status"] = new FunplusService.Processor.ProcessFunction(this.status_Process);
        this.processMap_["infos"] = new FunplusService.Processor.ProcessFunction(this.infos_Process);
        this.processMap_["tune"] = new FunplusService.Processor.ProcessFunction(this.tune_Process);
      }

      public bool Process(TProtocol iprot, TProtocol oprot)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          FunplusService.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
          {
            TProtocolUtil.Skip(iprot, TType.Struct);
            iprot.ReadMessageEnd();
            TApplicationException tapplicationException = new TApplicationException(TApplicationException.ExceptionType.UnknownMethod, "Invalid method name: '" + tmessage.Name + "'");
            oprot.WriteMessageBegin(new TMessage(tmessage.Name, TMessageType.Exception, tmessage.SeqID));
            tapplicationException.Write(oprot);
            oprot.WriteMessageEnd();
            oprot.Transport.Flush();
            return true;
          }
          processFunction(tmessage.SeqID, iprot, oprot);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public void status_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new FunplusService.status_args().Read(iprot);
        iprot.ReadMessageEnd();
        FunplusService.status_result statusResult = new FunplusService.status_result();
        statusResult.Success = this.iface_.status();
        oprot.WriteMessageBegin(new TMessage("status", TMessageType.Reply, seqid));
        statusResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void infos_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new FunplusService.infos_args().Read(iprot);
        iprot.ReadMessageEnd();
        FunplusService.infos_result infosResult = new FunplusService.infos_result();
        infosResult.Success = this.iface_.infos();
        oprot.WriteMessageBegin(new TMessage("infos", TMessageType.Reply, seqid));
        infosResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void tune_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        FunplusService.tune_args tuneArgs = new FunplusService.tune_args();
        tuneArgs.Read(iprot);
        iprot.ReadMessageEnd();
        FunplusService.tune_result tuneResult = new FunplusService.tune_result();
        this.iface_.tune(tuneArgs.Key, tuneArgs.Value);
        oprot.WriteMessageBegin(new TMessage("tune", TMessageType.Reply, seqid));
        tuneResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      protected delegate void ProcessFunction(int seqid, TProtocol iprot, TProtocol oprot);
    }

    [Serializable]
    public class status_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (status_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("status_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class status_result : TBase
    {
      private bool success;
      public FunplusService.status_result.Isset __isset;

      public bool Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 0)
            {
              if (tfield.Type == TType.Bool)
              {
                this.success = iprot.ReadBool();
                this.__isset.success = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (status_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success)
        {
          field.Name = "success";
          field.Type = TType.Bool;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteBool(this.success);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("status_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append(this.success);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
      }
    }

    [Serializable]
    public class infos_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (infos_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("infos_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class infos_result : TBase
    {
      private Dictionary<string, string> success;
      public FunplusService.infos_result.Isset __isset;

      public Dictionary<string, string> Success
      {
        get
        {
          return this.success;
        }
        set
        {
          this.__isset.success = true;
          this.success = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 0)
            {
              if (tfield.Type == TType.Map)
              {
                this.success = new Dictionary<string, string>();
                TMap tmap = iprot.ReadMapBegin();
                for (int index = 0; index < tmap.Count; ++index)
                  this.success[iprot.ReadString()] = iprot.ReadString();
                iprot.ReadMapEnd();
                this.__isset.success = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (infos_result));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.success && this.success != null)
        {
          field.Name = "success";
          field.Type = TType.Map;
          field.ID = (short) 0;
          oprot.WriteFieldBegin(field);
          oprot.WriteMapBegin(new TMap(TType.String, TType.String, this.success.Count));
          using (Dictionary<string, string>.KeyCollection.Enumerator enumerator = this.success.Keys.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              string current = enumerator.Current;
              oprot.WriteString(current);
              oprot.WriteString(this.success[current]);
              oprot.WriteMapEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("infos_result(");
        stringBuilder.Append("success: ");
        stringBuilder.Append((object) this.success);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool success;
      }
    }

    [Serializable]
    public class tune_args : TBase
    {
      private string key;
      private string value;
      public FunplusService.tune_args.Isset __isset;

      public string Key
      {
        get
        {
          return this.key;
        }
        set
        {
          this.__isset.key = true;
          this.key = value;
        }
      }

      public string Value
      {
        get
        {
          return this.value;
        }
        set
        {
          this.__isset.value = true;
          this.value = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.String)
                {
                  this.key = iprot.ReadString();
                  this.__isset.key = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.String)
                {
                  this.value = iprot.ReadString();
                  this.__isset.value = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (tune_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.key != null && this.__isset.key)
        {
          field.Name = "key";
          field.Type = TType.String;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.key);
          oprot.WriteFieldEnd();
        }
        if (this.value != null && this.__isset.value)
        {
          field.Name = "value";
          field.Type = TType.String;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.value);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("tune_args(");
        stringBuilder.Append("key: ");
        stringBuilder.Append(this.key);
        stringBuilder.Append(",value: ");
        stringBuilder.Append(this.value);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool key;
        public bool value;
      }
    }

    [Serializable]
    public class tune_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (tune_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("tune_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }
  }
}
