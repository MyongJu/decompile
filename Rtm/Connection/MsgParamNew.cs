﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.MsgParamNew
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Collections;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class MsgParamNew : TBase
  {
    private byte msg_type;
    private long from_xid;
    private int num;
    private long offset;
    private THashSet<byte> mtypes;
    public MsgParamNew.Isset __isset;

    public byte Msg_type
    {
      get
      {
        return this.msg_type;
      }
      set
      {
        this.__isset.msg_type = true;
        this.msg_type = value;
      }
    }

    public long From_xid
    {
      get
      {
        return this.from_xid;
      }
      set
      {
        this.__isset.from_xid = true;
        this.from_xid = value;
      }
    }

    public int Num
    {
      get
      {
        return this.num;
      }
      set
      {
        this.__isset.num = true;
        this.num = value;
      }
    }

    public long Offset
    {
      get
      {
        return this.offset;
      }
      set
      {
        this.__isset.offset = true;
        this.offset = value;
      }
    }

    public THashSet<byte> Mtypes
    {
      get
      {
        return this.mtypes;
      }
      set
      {
        this.__isset.mtypes = true;
        this.mtypes = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.Byte)
              {
                this.msg_type = iprot.ReadByte();
                this.__isset.msg_type = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I64)
              {
                this.from_xid = iprot.ReadI64();
                this.__isset.from_xid = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 3:
              if (tfield.Type == TType.I32)
              {
                this.num = iprot.ReadI32();
                this.__isset.num = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 4:
              if (tfield.Type == TType.I64)
              {
                this.offset = iprot.ReadI64();
                this.__isset.offset = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 5:
              if (tfield.Type == TType.Set)
              {
                this.mtypes = new THashSet<byte>();
                TSet tset = iprot.ReadSetBegin();
                for (int index = 0; index < tset.Count; ++index)
                  this.mtypes.Add(iprot.ReadByte());
                iprot.ReadSetEnd();
                this.__isset.mtypes = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (MsgParamNew));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.msg_type)
      {
        field.Name = "msg_type";
        field.Type = TType.Byte;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteByte(this.msg_type);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.from_xid)
      {
        field.Name = "from_xid";
        field.Type = TType.I64;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.from_xid);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.num)
      {
        field.Name = "num";
        field.Type = TType.I32;
        field.ID = (short) 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.num);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.offset)
      {
        field.Name = "offset";
        field.Type = TType.I64;
        field.ID = (short) 4;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.offset);
        oprot.WriteFieldEnd();
      }
      if (this.mtypes != null && this.__isset.mtypes)
      {
        field.Name = "mtypes";
        field.Type = TType.Set;
        field.ID = (short) 5;
        oprot.WriteFieldBegin(field);
        oprot.WriteSetBegin(new TSet(TType.Byte, this.mtypes.Count));
        foreach (byte mtype in this.mtypes)
        {
          oprot.WriteByte(mtype);
          oprot.WriteSetEnd();
        }
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("MsgParamNew(");
      stringBuilder.Append("msg_type: ");
      stringBuilder.Append(this.msg_type);
      stringBuilder.Append(",from_xid: ");
      stringBuilder.Append(this.from_xid);
      stringBuilder.Append(",num: ");
      stringBuilder.Append(this.num);
      stringBuilder.Append(",offset: ");
      stringBuilder.Append(this.offset);
      stringBuilder.Append(",mtypes: ");
      stringBuilder.Append((object) this.mtypes);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool msg_type;
      public bool from_xid;
      public bool num;
      public bool offset;
      public bool mtypes;
    }
  }
}
