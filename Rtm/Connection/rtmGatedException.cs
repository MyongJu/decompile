﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.rtmGatedException
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class rtmGatedException : Exception, TBase
  {
    private int code;
    private string reason;
    public rtmGatedException.Isset __isset;

    public int Code
    {
      get
      {
        return this.code;
      }
      set
      {
        this.__isset.code = true;
        this.code = value;
      }
    }

    public string Reason
    {
      get
      {
        return this.reason;
      }
      set
      {
        this.__isset.reason = true;
        this.reason = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.I32)
              {
                this.code = iprot.ReadI32();
                this.__isset.code = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.String)
              {
                this.reason = iprot.ReadString();
                this.__isset.reason = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (rtmGatedException));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.code)
      {
        field.Name = "code";
        field.Type = TType.I32;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.code);
        oprot.WriteFieldEnd();
      }
      if (this.reason != null && this.__isset.reason)
      {
        field.Name = "reason";
        field.Type = TType.String;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(this.reason);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("rtmGatedException(");
      stringBuilder.Append("code: ");
      stringBuilder.Append(this.code);
      stringBuilder.Append(",reason: ");
      stringBuilder.Append(this.reason);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool code;
      public bool reason;
    }
  }
}
