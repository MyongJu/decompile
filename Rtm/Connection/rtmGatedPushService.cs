﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.rtmGatedPushService
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Thrift;
using Thrift.Protocol;

namespace Rtm.Connection
{
  public class rtmGatedPushService
  {
    public interface Iface
    {
      void bye();

      void kickout();

      void push_note(byte mtype, long from, string message, int time);

      void push_notes(List<long> members, byte mtype, long from, string message, int time);

      void push_group_note(byte mtype, long group_id, long from, string message, int time);

      void push_msg(long from, byte mtype, string message, long mid, int time);

      void push_msgs(List<long> members, long from, byte mtype, string message, long mid, int time);

      void push_group_msg(long group_id, long from, byte mtype, string message, long mid, int time);

      void user_status(Dictionary<long, bool> status);

      void msg_receipt(long from, long mid, byte state);

      void push_broadcast_msg(long from, byte mtype, string message, long mid, int time);

      void push_broadcast_note(long from, byte mtype, string message, int time);
    }

    public class Client : rtmGatedPushService.Iface
    {
      protected TProtocol iprot_;
      protected TProtocol oprot_;
      protected int seqid_;

      public Client(TProtocol prot)
        : this(prot, prot)
      {
      }

      public Client(TProtocol iprot, TProtocol oprot)
      {
        this.iprot_ = iprot;
        this.oprot_ = oprot;
      }

      public TProtocol InputProtocol
      {
        get
        {
          return this.iprot_;
        }
      }

      public TProtocol OutputProtocol
      {
        get
        {
          return this.oprot_;
        }
      }

      public void bye()
      {
        this.send_bye();
        this.recv_bye();
      }

      public void send_bye()
      {
        this.oprot_.WriteMessageBegin(new TMessage("bye", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.bye_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_bye()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.bye_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void kickout()
      {
        this.send_kickout();
        this.recv_kickout();
      }

      public void send_kickout()
      {
        this.oprot_.WriteMessageBegin(new TMessage("kickout", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.kickout_args().Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_kickout()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.kickout_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_note(byte mtype, long from, string message, int time)
      {
        this.send_push_note(mtype, from, message, time);
      }

      public void send_push_note(byte mtype, long from, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_note", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_note_args()
        {
          Mtype = mtype,
          From = from,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_notes(List<long> members, byte mtype, long from, string message, int time)
      {
        this.send_push_notes(members, mtype, from, message, time);
      }

      public void send_push_notes(List<long> members, byte mtype, long from, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_notes", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_notes_args()
        {
          Members = members,
          Mtype = mtype,
          From = from,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_group_note(byte mtype, long group_id, long from, string message, int time)
      {
        this.send_push_group_note(mtype, group_id, from, message, time);
      }

      public void send_push_group_note(byte mtype, long group_id, long from, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_group_note", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_group_note_args()
        {
          Mtype = mtype,
          Group_id = group_id,
          From = from,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_msg(long from, byte mtype, string message, long mid, int time)
      {
        this.send_push_msg(from, mtype, message, mid, time);
        this.recv_push_msg();
      }

      public void send_push_msg(long from, byte mtype, string message, long mid, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_msg", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_msg_args()
        {
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.push_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_msgs(List<long> members, long from, byte mtype, string message, long mid, int time)
      {
        this.send_push_msgs(members, from, mtype, message, mid, time);
        this.recv_push_msgs();
      }

      public void send_push_msgs(List<long> members, long from, byte mtype, string message, long mid, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_msgs", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_msgs_args()
        {
          Members = members,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_msgs()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.push_msgs_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_group_msg(long group_id, long from, byte mtype, string message, long mid, int time)
      {
        this.send_push_group_msg(group_id, from, mtype, message, mid, time);
        this.recv_push_group_msg();
      }

      public void send_push_group_msg(long group_id, long from, byte mtype, string message, long mid, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_group_msg", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_group_msg_args()
        {
          Group_id = group_id,
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_group_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.push_group_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void user_status(Dictionary<long, bool> status)
      {
        this.send_user_status(status);
      }

      public void send_user_status(Dictionary<long, bool> status)
      {
        this.oprot_.WriteMessageBegin(new TMessage("user_status", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.user_status_args()
        {
          Status = status
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void msg_receipt(long from, long mid, byte state)
      {
        this.send_msg_receipt(from, mid, state);
      }

      public void send_msg_receipt(long from, long mid, byte state)
      {
        this.oprot_.WriteMessageBegin(new TMessage("msg_receipt", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.msg_receipt_args()
        {
          From = from,
          Mid = mid,
          State = state
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void push_broadcast_msg(long from, byte mtype, string message, long mid, int time)
      {
        this.send_push_broadcast_msg(from, mtype, message, mid, time);
        this.recv_push_broadcast_msg();
      }

      public void send_push_broadcast_msg(long from, byte mtype, string message, long mid, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_broadcast_msg", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_broadcast_msg_args()
        {
          From = from,
          Mtype = mtype,
          Message = message,
          Mid = mid,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }

      public void recv_push_broadcast_msg()
      {
        if (this.iprot_.ReadMessageBegin().Type == TMessageType.Exception)
        {
          TApplicationException tapplicationException = TApplicationException.Read(this.iprot_);
          this.iprot_.ReadMessageEnd();
          throw tapplicationException;
        }
        new rtmGatedPushService.push_broadcast_msg_result().Read(this.iprot_);
        this.iprot_.ReadMessageEnd();
      }

      public void push_broadcast_note(long from, byte mtype, string message, int time)
      {
        this.send_push_broadcast_note(from, mtype, message, time);
      }

      public void send_push_broadcast_note(long from, byte mtype, string message, int time)
      {
        this.oprot_.WriteMessageBegin(new TMessage("push_broadcast_note", TMessageType.Call, this.seqid_));
        new rtmGatedPushService.push_broadcast_note_args()
        {
          From = from,
          Mtype = mtype,
          Message = message,
          Time = time
        }.Write(this.oprot_);
        this.oprot_.WriteMessageEnd();
        this.oprot_.Transport.Flush();
      }
    }

    public class Processor : TProcessor
    {
      protected Dictionary<string, rtmGatedPushService.Processor.ProcessFunction> processMap_ = new Dictionary<string, rtmGatedPushService.Processor.ProcessFunction>();
      private rtmGatedPushService.Iface iface_;

      public Processor(rtmGatedPushService.Iface iface)
      {
        this.iface_ = iface;
        this.processMap_["bye"] = new rtmGatedPushService.Processor.ProcessFunction(this.bye_Process);
        this.processMap_["kickout"] = new rtmGatedPushService.Processor.ProcessFunction(this.kickout_Process);
        this.processMap_["push_note"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_note_Process);
        this.processMap_["push_notes"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_notes_Process);
        this.processMap_["push_group_note"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_group_note_Process);
        this.processMap_["push_msg"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_msg_Process);
        this.processMap_["push_msgs"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_msgs_Process);
        this.processMap_["push_group_msg"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_group_msg_Process);
        this.processMap_["user_status"] = new rtmGatedPushService.Processor.ProcessFunction(this.user_status_Process);
        this.processMap_["msg_receipt"] = new rtmGatedPushService.Processor.ProcessFunction(this.msg_receipt_Process);
        this.processMap_["push_broadcast_msg"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_broadcast_msg_Process);
        this.processMap_["push_broadcast_note"] = new rtmGatedPushService.Processor.ProcessFunction(this.push_broadcast_note_Process);
      }

      public bool Process(TProtocol iprot, TProtocol oprot)
      {
        try
        {
          TMessage tmessage = iprot.ReadMessageBegin();
          rtmGatedPushService.Processor.ProcessFunction processFunction;
          this.processMap_.TryGetValue(tmessage.Name, out processFunction);
          if (processFunction == null)
          {
            TProtocolUtil.Skip(iprot, TType.Struct);
            iprot.ReadMessageEnd();
            TApplicationException tapplicationException = new TApplicationException(TApplicationException.ExceptionType.UnknownMethod, "Invalid method name: '" + tmessage.Name + "'");
            oprot.WriteMessageBegin(new TMessage(tmessage.Name, TMessageType.Exception, tmessage.SeqID));
            tapplicationException.Write(oprot);
            oprot.WriteMessageEnd();
            oprot.Transport.Flush();
            return true;
          }
          processFunction(tmessage.SeqID, iprot, oprot);
        }
        catch (IOException ex)
        {
          return false;
        }
        return true;
      }

      public void bye_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new rtmGatedPushService.bye_args().Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.bye_result byeResult = new rtmGatedPushService.bye_result();
        this.iface_.bye();
        oprot.WriteMessageBegin(new TMessage("bye", TMessageType.Reply, seqid));
        byeResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void kickout_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        new rtmGatedPushService.kickout_args().Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.kickout_result kickoutResult = new rtmGatedPushService.kickout_result();
        this.iface_.kickout();
        oprot.WriteMessageBegin(new TMessage("kickout", TMessageType.Reply, seqid));
        kickoutResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_note_args pushNoteArgs = new rtmGatedPushService.push_note_args();
        pushNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_note(pushNoteArgs.Mtype, pushNoteArgs.From, pushNoteArgs.Message, pushNoteArgs.Time);
      }

      public void push_notes_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_notes_args pushNotesArgs = new rtmGatedPushService.push_notes_args();
        pushNotesArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_notes(pushNotesArgs.Members, pushNotesArgs.Mtype, pushNotesArgs.From, pushNotesArgs.Message, pushNotesArgs.Time);
      }

      public void push_group_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_group_note_args pushGroupNoteArgs = new rtmGatedPushService.push_group_note_args();
        pushGroupNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_group_note(pushGroupNoteArgs.Mtype, pushGroupNoteArgs.Group_id, pushGroupNoteArgs.From, pushGroupNoteArgs.Message, pushGroupNoteArgs.Time);
      }

      public void push_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_msg_args pushMsgArgs = new rtmGatedPushService.push_msg_args();
        pushMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.push_msg_result pushMsgResult = new rtmGatedPushService.push_msg_result();
        this.iface_.push_msg(pushMsgArgs.From, pushMsgArgs.Mtype, pushMsgArgs.Message, pushMsgArgs.Mid, pushMsgArgs.Time);
        oprot.WriteMessageBegin(new TMessage("push_msg", TMessageType.Reply, seqid));
        pushMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_msgs_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_msgs_args pushMsgsArgs = new rtmGatedPushService.push_msgs_args();
        pushMsgsArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.push_msgs_result pushMsgsResult = new rtmGatedPushService.push_msgs_result();
        this.iface_.push_msgs(pushMsgsArgs.Members, pushMsgsArgs.From, pushMsgsArgs.Mtype, pushMsgsArgs.Message, pushMsgsArgs.Mid, pushMsgsArgs.Time);
        oprot.WriteMessageBegin(new TMessage("push_msgs", TMessageType.Reply, seqid));
        pushMsgsResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_group_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_group_msg_args pushGroupMsgArgs = new rtmGatedPushService.push_group_msg_args();
        pushGroupMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.push_group_msg_result pushGroupMsgResult = new rtmGatedPushService.push_group_msg_result();
        this.iface_.push_group_msg(pushGroupMsgArgs.Group_id, pushGroupMsgArgs.From, pushGroupMsgArgs.Mtype, pushGroupMsgArgs.Message, pushGroupMsgArgs.Mid, pushGroupMsgArgs.Time);
        oprot.WriteMessageBegin(new TMessage("push_group_msg", TMessageType.Reply, seqid));
        pushGroupMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void user_status_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.user_status_args userStatusArgs = new rtmGatedPushService.user_status_args();
        userStatusArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.user_status(userStatusArgs.Status);
      }

      public void msg_receipt_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.msg_receipt_args msgReceiptArgs = new rtmGatedPushService.msg_receipt_args();
        msgReceiptArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.msg_receipt(msgReceiptArgs.From, msgReceiptArgs.Mid, msgReceiptArgs.State);
      }

      public void push_broadcast_msg_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_broadcast_msg_args broadcastMsgArgs = new rtmGatedPushService.push_broadcast_msg_args();
        broadcastMsgArgs.Read(iprot);
        iprot.ReadMessageEnd();
        rtmGatedPushService.push_broadcast_msg_result broadcastMsgResult = new rtmGatedPushService.push_broadcast_msg_result();
        this.iface_.push_broadcast_msg(broadcastMsgArgs.From, broadcastMsgArgs.Mtype, broadcastMsgArgs.Message, broadcastMsgArgs.Mid, broadcastMsgArgs.Time);
        oprot.WriteMessageBegin(new TMessage("push_broadcast_msg", TMessageType.Reply, seqid));
        broadcastMsgResult.Write(oprot);
        oprot.WriteMessageEnd();
        oprot.Transport.Flush();
      }

      public void push_broadcast_note_Process(int seqid, TProtocol iprot, TProtocol oprot)
      {
        rtmGatedPushService.push_broadcast_note_args broadcastNoteArgs = new rtmGatedPushService.push_broadcast_note_args();
        broadcastNoteArgs.Read(iprot);
        iprot.ReadMessageEnd();
        this.iface_.push_broadcast_note(broadcastNoteArgs.From, broadcastNoteArgs.Mtype, broadcastNoteArgs.Message, broadcastNoteArgs.Time);
      }

      protected delegate void ProcessFunction(int seqid, TProtocol iprot, TProtocol oprot);
    }

    [Serializable]
    public class bye_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (bye_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("bye_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class bye_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (bye_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("bye_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class kickout_args : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (kickout_args));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("kickout_args(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class kickout_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (kickout_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("kickout_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_note_args : TBase
    {
      private byte mtype;
      private long from;
      private string message;
      private int time;
      public rtmGatedPushService.push_note_args.Isset __isset;

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_note_args(");
        stringBuilder.Append("mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool mtype;
        public bool from;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_notes_args : TBase
    {
      private List<long> members;
      private byte mtype;
      private long from;
      private string message;
      private int time;
      public rtmGatedPushService.push_notes_args.Isset __isset;

      public List<long> Members
      {
        get
        {
          return this.members;
        }
        set
        {
          this.__isset.members = true;
          this.members = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.List)
                {
                  this.members = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.members.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.members = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_notes_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.members != null && this.__isset.members)
        {
          field.Name = "members";
          field.Type = TType.List;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.members.Count));
          using (List<long>.Enumerator enumerator = this.members.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_notes_args(");
        stringBuilder.Append("members: ");
        stringBuilder.Append((object) this.members);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool members;
        public bool mtype;
        public bool from;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_group_note_args : TBase
    {
      private byte mtype;
      private long group_id;
      private long from;
      private string message;
      private int time;
      public rtmGatedPushService.push_group_note_args.Isset __isset;

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_note_args(");
        stringBuilder.Append("mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool mtype;
        public bool group_id;
        public bool from;
        public bool message;
        public bool time;
      }
    }

    [Serializable]
    public class push_msg_args : TBase
    {
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      public rtmGatedPushService.push_msg_args.Isset __isset;

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msg_args(");
        stringBuilder.Append("from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
      }
    }

    [Serializable]
    public class push_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_msgs_args : TBase
    {
      private List<long> members;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      public rtmGatedPushService.push_msgs_args.Isset __isset;

      public List<long> Members
      {
        get
        {
          return this.members;
        }
        set
        {
          this.__isset.members = true;
          this.members = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.List)
                {
                  this.members = new List<long>();
                  TList tlist = iprot.ReadListBegin();
                  for (int index = 0; index < tlist.Count; ++index)
                    this.members.Add(iprot.ReadI64());
                  iprot.ReadListEnd();
                  this.__isset.members = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msgs_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.members != null && this.__isset.members)
        {
          field.Name = "members";
          field.Type = TType.List;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteListBegin(new TList(TType.I64, this.members.Count));
          using (List<long>.Enumerator enumerator = this.members.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteListEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msgs_args(");
        stringBuilder.Append("members: ");
        stringBuilder.Append((object) this.members);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool members;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
      }
    }

    [Serializable]
    public class push_msgs_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_msgs_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_msgs_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_group_msg_args : TBase
    {
      private long group_id;
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      public rtmGatedPushService.push_group_msg_args.Isset __isset;

      public long Group_id
      {
        get
        {
          return this.group_id;
        }
        set
        {
          this.__isset.group_id = true;
          this.group_id = value;
        }
      }

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.group_id = iprot.ReadI64();
                  this.__isset.group_id = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 6:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.group_id)
        {
          field.Name = "group_id";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.group_id);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 6;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_msg_args(");
        stringBuilder.Append("group_id: ");
        stringBuilder.Append(this.group_id);
        stringBuilder.Append(",from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool group_id;
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
      }
    }

    [Serializable]
    public class push_group_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_group_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_group_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class user_status_args : TBase
    {
      private Dictionary<long, bool> status;
      public rtmGatedPushService.user_status_args.Isset __isset;

      public Dictionary<long, bool> Status
      {
        get
        {
          return this.status;
        }
        set
        {
          this.__isset.status = true;
          this.status = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            if ((int) tfield.ID == 1)
            {
              if (tfield.Type == TType.Map)
              {
                this.status = new Dictionary<long, bool>();
                TMap tmap = iprot.ReadMapBegin();
                for (int index = 0; index < tmap.Count; ++index)
                  this.status[iprot.ReadI64()] = iprot.ReadBool();
                iprot.ReadMapEnd();
                this.__isset.status = true;
              }
              else
                TProtocolUtil.Skip(iprot, tfield.Type);
            }
            else
              TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (user_status_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.status != null && this.__isset.status)
        {
          field.Name = "status";
          field.Type = TType.Map;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteMapBegin(new TMap(TType.I64, TType.Bool, this.status.Count));
          using (Dictionary<long, bool>.KeyCollection.Enumerator enumerator = this.status.Keys.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              long current = enumerator.Current;
              oprot.WriteI64(current);
              oprot.WriteBool(this.status[current]);
              oprot.WriteMapEnd();
            }
          }
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("user_status_args(");
        stringBuilder.Append("status: ");
        stringBuilder.Append((object) this.status);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool status;
      }
    }

    [Serializable]
    public class msg_receipt_args : TBase
    {
      private long from;
      private long mid;
      private byte state;
      public rtmGatedPushService.msg_receipt_args.Isset __isset;

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public byte State
      {
        get
        {
          return this.state;
        }
        set
        {
          this.__isset.state = true;
          this.state = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.Byte)
                {
                  this.state = iprot.ReadByte();
                  this.__isset.state = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (msg_receipt_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.state)
        {
          field.Name = "state";
          field.Type = TType.Byte;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.state);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("msg_receipt_args(");
        stringBuilder.Append("from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",state: ");
        stringBuilder.Append(this.state);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool from;
        public bool mid;
        public bool state;
      }
    }

    [Serializable]
    public class push_broadcast_msg_args : TBase
    {
      private long from;
      private byte mtype;
      private string message;
      private long mid;
      private int time;
      public rtmGatedPushService.push_broadcast_msg_args.Isset __isset;

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public long Mid
      {
        get
        {
          return this.mid;
        }
        set
        {
          this.__isset.mid = true;
          this.mid = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I64)
                {
                  this.mid = iprot.ReadI64();
                  this.__isset.mid = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 5:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_msg_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mid)
        {
          field.Name = "mid";
          field.Type = TType.I64;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.mid);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 5;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_msg_args(");
        stringBuilder.Append("from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",mid: ");
        stringBuilder.Append(this.mid);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool from;
        public bool mtype;
        public bool message;
        public bool mid;
        public bool time;
      }
    }

    [Serializable]
    public class push_broadcast_msg_result : TBase
    {
      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            short id = tfield.ID;
            TProtocolUtil.Skip(iprot, tfield.Type);
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_msg_result));
        oprot.WriteStructBegin(struc);
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_msg_result(");
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }
    }

    [Serializable]
    public class push_broadcast_note_args : TBase
    {
      private long from;
      private byte mtype;
      private string message;
      private int time;
      public rtmGatedPushService.push_broadcast_note_args.Isset __isset;

      public long From
      {
        get
        {
          return this.from;
        }
        set
        {
          this.__isset.from = true;
          this.from = value;
        }
      }

      public byte Mtype
      {
        get
        {
          return this.mtype;
        }
        set
        {
          this.__isset.mtype = true;
          this.mtype = value;
        }
      }

      public string Message
      {
        get
        {
          return this.message;
        }
        set
        {
          this.__isset.message = true;
          this.message = value;
        }
      }

      public int Time
      {
        get
        {
          return this.time;
        }
        set
        {
          this.__isset.time = true;
          this.time = value;
        }
      }

      public void Read(TProtocol iprot)
      {
        iprot.ReadStructBegin();
        while (true)
        {
          TField tfield = iprot.ReadFieldBegin();
          if (tfield.Type != TType.Stop)
          {
            switch (tfield.ID)
            {
              case 1:
                if (tfield.Type == TType.I64)
                {
                  this.from = iprot.ReadI64();
                  this.__isset.from = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 2:
                if (tfield.Type == TType.Byte)
                {
                  this.mtype = iprot.ReadByte();
                  this.__isset.mtype = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 3:
                if (tfield.Type == TType.String)
                {
                  this.message = iprot.ReadString();
                  this.__isset.message = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              case 4:
                if (tfield.Type == TType.I32)
                {
                  this.time = iprot.ReadI32();
                  this.__isset.time = true;
                  break;
                }
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
              default:
                TProtocolUtil.Skip(iprot, tfield.Type);
                break;
            }
            iprot.ReadFieldEnd();
          }
          else
            break;
        }
        iprot.ReadStructEnd();
      }

      public void Write(TProtocol oprot)
      {
        TStruct struc = new TStruct(nameof (push_broadcast_note_args));
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        if (this.__isset.from)
        {
          field.Name = "from";
          field.Type = TType.I64;
          field.ID = (short) 1;
          oprot.WriteFieldBegin(field);
          oprot.WriteI64(this.from);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.mtype)
        {
          field.Name = "mtype";
          field.Type = TType.Byte;
          field.ID = (short) 2;
          oprot.WriteFieldBegin(field);
          oprot.WriteByte(this.mtype);
          oprot.WriteFieldEnd();
        }
        if (this.message != null && this.__isset.message)
        {
          field.Name = "message";
          field.Type = TType.String;
          field.ID = (short) 3;
          oprot.WriteFieldBegin(field);
          oprot.WriteString(this.message);
          oprot.WriteFieldEnd();
        }
        if (this.__isset.time)
        {
          field.Name = "time";
          field.Type = TType.I32;
          field.ID = (short) 4;
          oprot.WriteFieldBegin(field);
          oprot.WriteI32(this.time);
          oprot.WriteFieldEnd();
        }
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder("push_broadcast_note_args(");
        stringBuilder.Append("from: ");
        stringBuilder.Append(this.from);
        stringBuilder.Append(",mtype: ");
        stringBuilder.Append(this.mtype);
        stringBuilder.Append(",message: ");
        stringBuilder.Append(this.message);
        stringBuilder.Append(",time: ");
        stringBuilder.Append(this.time);
        stringBuilder.Append(")");
        return stringBuilder.ToString();
      }

      [Serializable]
      public struct Isset
      {
        public bool from;
        public bool mtype;
        public bool message;
        public bool time;
      }
    }
  }
}
