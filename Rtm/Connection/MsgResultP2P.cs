﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.MsgResultP2P
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class MsgResultP2P : TBase
  {
    private int num;
    private long last_id;
    private List<MsgContentP2P> msgs;
    public MsgResultP2P.Isset __isset;

    public int Num
    {
      get
      {
        return this.num;
      }
      set
      {
        this.__isset.num = true;
        this.num = value;
      }
    }

    public long Last_id
    {
      get
      {
        return this.last_id;
      }
      set
      {
        this.__isset.last_id = true;
        this.last_id = value;
      }
    }

    public List<MsgContentP2P> Msgs
    {
      get
      {
        return this.msgs;
      }
      set
      {
        this.__isset.msgs = true;
        this.msgs = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.I32)
              {
                this.num = iprot.ReadI32();
                this.__isset.num = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I64)
              {
                this.last_id = iprot.ReadI64();
                this.__isset.last_id = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 3:
              if (tfield.Type == TType.List)
              {
                this.msgs = new List<MsgContentP2P>();
                TList tlist = iprot.ReadListBegin();
                for (int index = 0; index < tlist.Count; ++index)
                {
                  MsgContentP2P msgContentP2P1 = new MsgContentP2P();
                  MsgContentP2P msgContentP2P2 = new MsgContentP2P();
                  msgContentP2P2.Read(iprot);
                  this.msgs.Add(msgContentP2P2);
                }
                iprot.ReadListEnd();
                this.__isset.msgs = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (MsgResultP2P));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.num)
      {
        field.Name = "num";
        field.Type = TType.I32;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(this.num);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.last_id)
      {
        field.Name = "last_id";
        field.Type = TType.I64;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.last_id);
        oprot.WriteFieldEnd();
      }
      if (this.msgs != null && this.__isset.msgs)
      {
        field.Name = "msgs";
        field.Type = TType.List;
        field.ID = (short) 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteListBegin(new TList(TType.Struct, this.msgs.Count));
        using (List<MsgContentP2P>.Enumerator enumerator = this.msgs.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            enumerator.Current.Write(oprot);
            oprot.WriteListEnd();
          }
        }
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("MsgResultP2P(");
      stringBuilder.Append("num: ");
      stringBuilder.Append(this.num);
      stringBuilder.Append(",last_id: ");
      stringBuilder.Append(this.last_id);
      stringBuilder.Append(",msgs: ");
      stringBuilder.Append((object) this.msgs);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool num;
      public bool last_id;
      public bool msgs;
    }
  }
}
