﻿// Decompiled with JetBrains decompiler
// Type: Rtm.Connection.friendPair
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using Thrift.Protocol;

namespace Rtm.Connection
{
  [Serializable]
  public class friendPair : TBase
  {
    private long uidA;
    private long uidB;
    public friendPair.Isset __isset;

    public long UidA
    {
      get
      {
        return this.uidA;
      }
      set
      {
        this.__isset.uidA = true;
        this.uidA = value;
      }
    }

    public long UidB
    {
      get
      {
        return this.uidB;
      }
      set
      {
        this.__isset.uidB = true;
        this.uidB = value;
      }
    }

    public void Read(TProtocol iprot)
    {
      iprot.ReadStructBegin();
      while (true)
      {
        TField tfield = iprot.ReadFieldBegin();
        if (tfield.Type != TType.Stop)
        {
          switch (tfield.ID)
          {
            case 1:
              if (tfield.Type == TType.I64)
              {
                this.uidA = iprot.ReadI64();
                this.__isset.uidA = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            case 2:
              if (tfield.Type == TType.I64)
              {
                this.uidB = iprot.ReadI64();
                this.__isset.uidB = true;
                break;
              }
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
            default:
              TProtocolUtil.Skip(iprot, tfield.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        else
          break;
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot)
    {
      TStruct struc = new TStruct(nameof (friendPair));
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (this.__isset.uidA)
      {
        field.Name = "uidA";
        field.Type = TType.I64;
        field.ID = (short) 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.uidA);
        oprot.WriteFieldEnd();
      }
      if (this.__isset.uidB)
      {
        field.Name = "uidB";
        field.Type = TType.I64;
        field.ID = (short) 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(this.uidB);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("friendPair(");
      stringBuilder.Append("uidA: ");
      stringBuilder.Append(this.uidA);
      stringBuilder.Append(",uidB: ");
      stringBuilder.Append(this.uidB);
      stringBuilder.Append(")");
      return stringBuilder.ToString();
    }

    [Serializable]
    public struct Isset
    {
      public bool uidA;
      public bool uidB;
    }
  }
}
