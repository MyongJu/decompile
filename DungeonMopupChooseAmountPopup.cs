﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupChooseAmountPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class DungeonMopupChooseAmountPopup : Popup
{
  public UITexture _itemIcon;
  public UILabel _itemName;
  public UILabel _itemDesc;
  public UILabel _itemValue;
  public UILabel _unitWeight;
  public UILabel _totalWeight;
  public UILabel _maxCount;
  public UISlider _slider;
  public UIInput _selectedCount;
  public UIButton _placeButton;
  private bool _sliding;
  private DungeonMopupChooseAmountPopup.Parameter _param;
  private ItemStaticInfo _itemInfo;
  private int _maxItemCount;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._param = orgParam as DungeonMopupChooseAmountPopup.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this._itemInfo = ConfigManager.inst.DB_Items.GetItem(this._param.itemId);
    this._maxItemCount = Mathf.Min(this._param.quantity, this._param.maxWeight / this._itemInfo.DungeonWeight);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._itemIcon, this._itemInfo.ImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this._itemName.text = this._itemInfo.LocName;
    this._itemDesc.text = this._itemInfo.LocDescription;
    this._unitWeight.text = Utils.FormatThousands(this._itemInfo.DungeonWeight.ToString());
    this._itemValue.text = "x" + this._param.quantity.ToString();
    this._totalWeight.text = DragonKnightUtils.GetDungeonData().CurrentBagCapacity.ToString() + "/" + (object) PlayerData.inst.dragonKnightData.Capacity;
    this._slider.value = 0.0f;
    this._slider.numberOfSteps = this._maxItemCount + 1;
    this._selectedCount.value = "0";
    this._maxCount.text = "/" + this._maxItemCount.ToString();
    this._placeButton.isEnabled = false;
  }

  private void UpdateSlider(int result)
  {
    result = Mathf.Max(0, Mathf.Min(result, this._maxItemCount));
    this._slider.value = (float) result / (float) this._maxItemCount;
  }

  public void OnMinusClick()
  {
    this.UpdateSlider(Mathf.RoundToInt(this._slider.value * (float) this._maxItemCount) - 1);
  }

  public void OnPlusClick()
  {
    this.UpdateSlider(Mathf.RoundToInt(this._slider.value * (float) this._maxItemCount) + 1);
  }

  public void OnSliderChanged()
  {
    int num1 = Mathf.RoundToInt(this._slider.value * (float) this._maxItemCount);
    this._sliding = true;
    this._selectedCount.value = num1.ToString();
    this._sliding = false;
    int num2 = num1 * this._itemInfo.DungeonWeight;
    long capacity = PlayerData.inst.dragonKnightData.Capacity;
    int currentBagCapacity = DragonKnightUtils.GetDungeonData().CurrentBagCapacity;
    this._totalWeight.text = (num2 + currentBagCapacity).ToString() + "/" + (object) capacity;
    this._totalWeight.color = (long) (num2 + currentBagCapacity) >= capacity ? Color.red : Color.white;
    this._placeButton.isEnabled = num1 > 0;
  }

  public void OnInputChanged()
  {
    if (this._sliding)
      return;
    int result1;
    int.TryParse(this._selectedCount.value, out result1);
    int result2 = Mathf.Min(result1, this._maxItemCount);
    this._selectedCount.value = result2.ToString();
    this.UpdateSlider(result2);
  }

  public void OnPlaceClick()
  {
    int result = Mathf.RoundToInt(this._slider.value * (float) this._maxItemCount);
    Hashtable postData = new Hashtable();
    postData[(object) "item_id"] = (object) this._itemInfo.internalId;
    postData[(object) "num"] = (object) result;
    MessageHub.inst.GetPortByAction("DragonKnight:getRaidChestItem").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      ItemRewardInfo.Data data1 = new ItemRewardInfo.Data();
      data1.icon = this._itemInfo.ImagePath;
      data1.count = (float) result;
      RewardsCollectionAnimator.Instance.Clear();
      RewardsCollectionAnimator.Instance.items.Add(data1);
      RewardsCollectionAnimator.Instance.CollectItems(true);
      DragonKnightSystem.Instance.RoomManager.SyncMazeData(DragonKnightUtils.GetDungeonData());
      this.OnCloseClick();
    }), true);
  }

  public void OnCloseClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public int quantity;
    public int maxWeight;
  }
}
