﻿// Decompiled with JetBrains decompiler
// Type: ConfigAchievementMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAchievementMain
{
  private ConfigParse parse = new ConfigParse();
  public Dictionary<string, AchievementMainInfo> datas;
  private Dictionary<int, AchievementMainInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AchievementMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AchievementMainInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AchievementMainInfo) null;
  }

  public AchievementMainInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AchievementMainInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AchievementMainInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, AchievementMainInfo>) null;
  }
}
