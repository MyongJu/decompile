﻿// Decompiled with JetBrains decompiler
// Type: TradingHallBuyGoodView
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class TradingHallBuyGoodView : MonoBehaviour
{
  private bool _resetScrollView = true;
  private Dictionary<int, TradingHallBuyGoodSlot> _buyItemDict = new Dictionary<int, TradingHallBuyGoodSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private UILabel _labelItemAveragePrice;
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  private GameObject _noGood2Show;
  [SerializeField]
  private GameObject _noSellRecordNode;
  [SerializeField]
  private UIScrollView _buyScrollView;
  [SerializeField]
  private UITable _buyTable;
  [SerializeField]
  private GameObject _buyItemPrefab;
  [SerializeField]
  private GameObject _buyItemRoot;
  private TradingHallData.BuyGoodData _buyGoodData;
  private TradingHallCatalog _catalog;

  public void OnItemInfoClick()
  {
    if (this._buyGoodData == null)
      return;
    Utils.ShowItemTip(this._buyGoodData.ItemId, this._itemIconRenderer.transform, (long) this._buyGoodData.EquipmentId, 0L, this._buyGoodData.EquipmentEnhanced);
  }

  public void UpdateUI(TradingHallData.BuyGoodData buyGoodData, TradingHallCatalog catalog)
  {
    this._buyGoodData = buyGoodData;
    this._catalog = catalog;
    TradingHallPayload.Instance.OnBuyDataUpdated -= new System.Action(this.OnBuyDataUpdated);
    TradingHallPayload.Instance.OnBuyDataUpdated += new System.Action(this.OnBuyDataUpdated);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._buyGoodData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._buyGoodData.ItemId);
    if (itemStaticInfo != null)
    {
      this._labelItemName.text = itemStaticInfo.LocName;
      this._labelEquipmentEnhanced.text = this._buyGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._buyGoodData.EquipmentEnhanced.ToString();
      this._labelItemName.text += this._labelEquipmentEnhanced.text;
      this._labelItemAveragePrice.text = Utils.FormatThousands(this._buyGoodData.ItemAveragePrice.ToString());
      NGUITools.SetActive(this._labelItemAveragePrice.transform.parent.gameObject, this._buyGoodData.ItemAveragePrice > 0);
      NGUITools.SetActive(this._noSellRecordNode, this._buyGoodData.ItemAveragePrice <= 0);
      EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
      this._itemIconRenderer.SetData(this._buyGoodData.ItemId, string.Empty, true);
      if (this._buyGoodData.ItemTotalCount <= 0L)
        GreyUtility.Grey(this._itemIconRenderer.gameObject);
      else if (this._buyGoodData.ThisGoodAvailable)
        GreyUtility.Normal(this._itemIconRenderer.gameObject);
      else
        GreyUtility.Custom(this._itemIconRenderer.gameObject, new Color(1f, 0.3137255f, 0.3137255f, 1f));
    }
    this.ShowBuyContent();
  }

  public void ClearBuyGoodData()
  {
    using (Dictionary<int, TradingHallBuyGoodSlot>.Enumerator enumerator = this._buyItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, TradingHallBuyGoodSlot> current = enumerator.Current;
        current.Value.OnBuyGoodSuccess -= new System.Action(this.OnBuyGoodSuccess);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._buyItemDict.Clear();
    this._itemPool.Clear();
    TradingHallPayload.Instance.OnBuyDataUpdated -= new System.Action(this.OnBuyDataUpdated);
  }

  private void RepositionBuyGood()
  {
    this._buyTable.repositionNow = true;
    this._buyTable.Reposition();
    if (!this._resetScrollView)
      return;
    this._buyScrollView.ResetPosition();
    this._buyScrollView.gameObject.SetActive(false);
    this._buyScrollView.gameObject.SetActive(true);
  }

  private TradingHallBuyGoodSlot GenerateBuyGoodSlot(TradingHallData.BuyGoodData buyGoodData, int itemUnitPrice, int itemCount, int buyIndex)
  {
    this._itemPool.Initialize(this._buyItemPrefab, this._buyItemRoot);
    GameObject go = this._itemPool.AddChild(this._buyTable.gameObject);
    NGUITools.SetActive(go, true);
    TradingHallBuyGoodSlot component = go.GetComponent<TradingHallBuyGoodSlot>();
    component.SetData(buyGoodData, itemUnitPrice, itemCount, buyIndex, this._catalog);
    component.OnBuyGoodSuccess += new System.Action(this.OnBuyGoodSuccess);
    return component;
  }

  private void OnBuyGoodSuccess()
  {
    if (this._buyGoodData == null)
      return;
    this._resetScrollView = false;
    TradingHallData.BuyGoodData buyGoodDataById = TradingHallPayload.Instance.GetBuyGoodDataById(this._buyGoodData.ExchangeId);
    if (buyGoodDataById != null && buyGoodDataById.ExchangeId > 0)
      this.UpdateUI(buyGoodDataById, this._catalog);
    this._resetScrollView = true;
  }

  private void ShowBuyContent()
  {
    this.ClearBuyGoodData();
    if (this._buyGoodData != null && this._buyGoodData.ItemSaleUnitPrices != null)
    {
      int key = 0;
      this._buyGoodData.ItemSaleUnitPrices.Sort((Comparison<KeyValuePair<int, int>>) ((x, y) => x.Key.CompareTo(y.Key)));
      using (List<KeyValuePair<int, int>>.Enumerator enumerator = this._buyGoodData.ItemSaleUnitPrices.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, int> current = enumerator.Current;
          TradingHallBuyGoodSlot buyGoodSlot = this.GenerateBuyGoodSlot(this._buyGoodData, current.Key, current.Value, ++key);
          this._buyItemDict.Add(key, buyGoodSlot);
        }
      }
    }
    NGUITools.SetActive(this._noGood2Show, this._buyGoodData == null || this._buyGoodData.ItemSaleUnitPrices == null || this._buyGoodData.ItemSaleUnitPrices.Count <= 0);
    this.RepositionBuyGood();
  }

  private void OnBuyDataUpdated()
  {
    if (this._buyGoodData == null)
      return;
    TradingHallData.BuyGoodData buyGoodData = TradingHallPayload.Instance.TradingData.BuyGoodDataList.Find((Predicate<TradingHallData.BuyGoodData>) (x => x.ExchangeId == this._buyGoodData.ExchangeId));
    if (buyGoodData == null || buyGoodData.ExchangeId <= 0)
      buyGoodData = new TradingHallData.BuyGoodData(this._buyGoodData.ExchangeId);
    this.UpdateUI(buyGoodData, this._catalog);
  }
}
