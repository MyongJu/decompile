﻿// Decompiled with JetBrains decompiler
// Type: DKMopupTimerBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DKMopupTimerBar : MonoBehaviour
{
  public UILabel _remaining;
  public UISlider _progress;
  private JobHandle _job;

  public void SetJob(JobHandle job)
  {
    this._job = job;
    this.OnSecond(0);
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int secs)
  {
    this._remaining.text = Utils.FormatTime(this._job.LeftTime(), false, false, true);
    this._progress.value = Mathf.Clamp((float) (1.0 - (double) this._job.LeftTime() / (double) this._job.Duration()), 0.0f, 1f);
  }
}
