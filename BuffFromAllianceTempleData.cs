﻿// Decompiled with JetBrains decompiler
// Type: BuffFromAllianceTempleData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;

public class BuffFromAllianceTempleData : BuffUIData
{
  public override void Refresh()
  {
    TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById((int) this.Data);
    string type = byId.Type;
    if (type != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (BuffFromAllianceTempleData.\u003C\u003Ef__switch\u0024map2D == null)
      {
        // ISSUE: reference to a compiler-generated field
        BuffFromAllianceTempleData.\u003C\u003Ef__switch\u0024map2D = new Dictionary<string, int>(4)
        {
          {
            "heal_speed",
            0
          },
          {
            "research_speed",
            1
          },
          {
            "building_speed",
            2
          },
          {
            "gather_speed",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (BuffFromAllianceTempleData.\u003C\u003Ef__switch\u0024map2D.TryGetValue(type, out num))
      {
        switch (num)
        {
          case 0:
            this.Name = ScriptLocalization.Get("boost_type_spell_heal", true);
            this.Desc = ScriptLocalization.Get("boost_type_spell_heal_description", true);
            break;
          case 1:
            this.Name = ScriptLocalization.Get("boost_type_spell_research", true);
            this.Desc = ScriptLocalization.Get("boost_type_spell_research_description", true);
            break;
          case 2:
            this.Name = ScriptLocalization.Get("boost_type_spell_build", true);
            this.Desc = ScriptLocalization.Get("boost_type_spell_build_description", true);
            break;
          case 3:
            this.Name = ScriptLocalization.Get("boost_type_spell_gather", true);
            this.Desc = ScriptLocalization.Get("boost_type_spell_gather_description", true);
            break;
        }
      }
    }
    this.IconPath = byId.IconPath;
  }

  public override bool ListenerClick()
  {
    if (PlayerData.inst.allianceId < 0L)
      return false;
    AllianceTempleData allianceTempleData = DBManager.inst.DB_AllianceTemple.GetMyAllianceTempleData();
    return allianceTempleData != null && allianceTempleData.IsComplete;
  }

  public override int RemainTime()
  {
    if (!this.IsRunning())
      return 0;
    int data = (int) this.Data;
    return (int) this.GetMagice().TimerEnd - NetServerTime.inst.ServerTimestamp;
  }

  public override bool IsRunning()
  {
    return this.GetMagice() != null;
  }

  private AllianceMagicData GetMagice()
  {
    int data = (int) this.Data;
    AllianceMagicData allianceMagicData = DBManager.inst.DB_AllianceMagic.GetBySkillConfigId(data) ?? DBManager.inst.DB_AllianceMagic.GetBySkillType(ConfigManager.inst.DB_TempleSkill.GetById(data).Type);
    if (allianceMagicData != null && allianceMagicData.AllianceId != PlayerData.inst.allianceId)
      return (AllianceMagicData) null;
    return allianceMagicData;
  }

  public override float GetProgressValue()
  {
    if (!this.IsRunning())
      return 0.0f;
    return (float) ((int) this.GetMagice().TimerEnd - NetServerTime.inst.ServerTimestamp) / (float) ConfigManager.inst.DB_TempleSkill.GetById((int) this.Data).DonationTime;
  }
}
