﻿// Decompiled with JetBrains decompiler
// Type: ConfigItemCompose
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigItemCompose
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ItemComposeInfo> datas;
  private Dictionary<long, ItemComposeInfo> dicByUniqueId;

  public object ParseDict(object rs)
  {
    Hashtable hashtable = rs as Hashtable;
    Dictionary<long, int> dictionary = new Dictionary<long, int>();
    if (hashtable == null)
      return (object) dictionary;
    IEnumerator enumerator = hashtable.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      long result1;
      int result2;
      if (long.TryParse(enumerator.Current.ToString(), out result1) && int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
        dictionary.Add(result1, result2);
    }
    return (object) dictionary;
  }

  public void BuildDB(object res)
  {
    this.parse.AddParseFunc("ParseComposeItems", new ParseFunc(this.ParseDict));
    this.parse.Parse<ItemComposeInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ItemComposeInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<long, ItemComposeInfo>) null;
  }

  public ItemComposeInfo GetItemComposeInfo(long internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (ItemComposeInfo) null;
  }

  public ItemComposeInfo GetItemComposeInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ItemComposeInfo) null;
  }

  internal long CanCompose(long item_id)
  {
    using (Dictionary<string, ItemComposeInfo>.ValueCollection.Enumerator enumerator1 = this.datas.Values.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        ItemComposeInfo current1 = enumerator1.Current;
        using (Dictionary<long, int>.KeyCollection.Enumerator enumerator2 = current1.ComposeItems.Keys.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            long current2 = enumerator2.Current;
            if (item_id == current2)
              return current1.internalId;
          }
        }
      }
    }
    return -1;
  }
}
