﻿// Decompiled with JetBrains decompiler
// Type: SuperLoginChristmasEveGiftPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;

public class SuperLoginChristmasEveGiftPopup : Popup
{
  public UIButton receiveGiftButton;
  public UITexture giftTexture;
  public UILabel giftGreetings;
  private SuperLoginMainInfo _mainInfo;
  private System.Action<int> _giftReceived;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    SuperLoginChristmasEveGiftPopup.Parameter parameter = orgParam as SuperLoginChristmasEveGiftPopup.Parameter;
    if (parameter != null)
    {
      this._mainInfo = parameter.mainInfo;
      this._giftReceived = parameter.onGiftReceived;
    }
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnClaimBtnPressed()
  {
    if (this._mainInfo == null)
      return;
    SuperLoginPayload.Instance.ReceiveDailyReward(this._mainInfo.day, "super_log_in_christmas", (System.Action<bool, object>) ((ret, datd) =>
    {
      if (!ret)
        return;
      if (this._giftReceived != null)
        this._giftReceived(this._mainInfo.day);
      this.ShowGiftReceivedEffect();
      this.OnCloseBtnPressed();
    }));
  }

  private void ShowGiftReceivedEffect()
  {
    if (this._mainInfo == null || this._mainInfo.Rewards == null)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    Dictionary<string, int>.Enumerator enumerator = this._mainInfo.Rewards.rewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int result = 0;
      int.TryParse(enumerator.Current.Key, out result);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(result);
      if (result > 0 && itemStaticInfo != null)
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          icon = itemStaticInfo.ImagePath,
          count = (float) enumerator.Current.Value
        });
    }
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void UpdateUI()
  {
    if (this._mainInfo == null)
      return;
    this.receiveGiftButton.isEnabled = this._mainInfo.RewardState == SuperLoginPayload.DailyRewardState.CAN_GET;
    this.giftGreetings.text = this._mainInfo.Description;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.giftTexture, this._mainInfo.RewardIconPath, (System.Action<bool>) null, true, false, string.Empty);
  }

  public class Parameter : Popup.PopupParameter
  {
    public SuperLoginMainInfo mainInfo;
    public System.Action<int> onGiftReceived;
  }
}
