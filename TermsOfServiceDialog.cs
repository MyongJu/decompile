﻿// Decompiled with JetBrains decompiler
// Type: TermsOfServiceDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class TermsOfServiceDialog : UI.Dialog
{
  public const string Dialog_Type = "Community/TermsOfServiceDialog";
  public UILabel titleLabel;
  public UILabel descLabel;
  public UITable table;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.UpdateUI(orgParam as TermsOfServiceDialog.Parameter);
  }

  private void UpdateUI(TermsOfServiceDialog.Parameter param)
  {
    if (param == null)
      return;
    if (param.title != null && param.title.Length > 0)
      this.titleLabel.text = Utils.XLAT(param.title);
    UIUtils.ClearTable(this.table);
    NGUITools.SetActive(this.descLabel.gameObject, false);
    if (param.description == null || param.description.Length <= 0)
      return;
    string[] strArray = Utils.XLAT(param.description).Split('\n');
    List<string> stringList = new List<string>();
    StringBuilder stringBuilder = new StringBuilder();
    foreach (string str in strArray)
    {
      stringBuilder.Append(str).Append('\n');
      if (stringBuilder.Length > 2000)
      {
        stringList.Add(stringBuilder.ToString());
        stringBuilder = new StringBuilder();
      }
    }
    stringList.Add(stringBuilder.ToString());
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        GameObject go = NGUITools.AddChild(this.table.gameObject, this.descLabel.gameObject);
        NGUITools.SetActive(go, true);
        go.GetComponent<UILabel>().text = current;
      }
    }
    this.table.Reposition();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public string title;
    public string description;
  }
}
