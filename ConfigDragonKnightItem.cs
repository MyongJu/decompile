﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightItem
{
  private Dictionary<string, DragonKnightItemInfo> _dataById;
  private Dictionary<int, DragonKnightItemInfo> _dataByInternalId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DragonKnightItemInfo, string>(res as Hashtable, "id", out this._dataById, out this._dataByInternalId);
  }

  public DragonKnightItemInfo Get(int internalId)
  {
    DragonKnightItemInfo dragonKnightItemInfo;
    this._dataByInternalId.TryGetValue(internalId, out dragonKnightItemInfo);
    return dragonKnightItemInfo;
  }

  public DragonKnightItemInfo Get(string id)
  {
    DragonKnightItemInfo dragonKnightItemInfo;
    this._dataById.TryGetValue(id, out dragonKnightItemInfo);
    return dragonKnightItemInfo;
  }

  public void Clear()
  {
    if (this._dataById != null)
    {
      this._dataById.Clear();
      this._dataById = (Dictionary<string, DragonKnightItemInfo>) null;
    }
    if (this._dataByInternalId == null)
      return;
    this._dataByInternalId.Clear();
    this._dataByInternalId = (Dictionary<int, DragonKnightItemInfo>) null;
  }
}
