﻿// Decompiled with JetBrains decompiler
// Type: WorldFlagInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class WorldFlagInfo
{
  public const string IMAGE_ROOT_PATH = "Texture/GvEBossIcon/";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "sort")]
  public int Sort;

  public int ServerStoredId
  {
    get
    {
      int result = 0;
      int.TryParse(this.ID, out result);
      return result;
    }
  }

  public string SpriteName
  {
    get
    {
      return Utils.GetFlagIconPath(this.ServerStoredId);
    }
  }
}
