﻿// Decompiled with JetBrains decompiler
// Type: UITweener
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class UITweener : MonoBehaviour
{
  [HideInInspector]
  public AnimationCurve animationCurve = new AnimationCurve(new Keyframe[2]
  {
    new Keyframe(0.0f, 0.0f, 0.0f, 1f),
    new Keyframe(1f, 1f, 1f, 0.0f)
  });
  [HideInInspector]
  public bool ignoreTimeScale = true;
  [HideInInspector]
  public float duration = 1f;
  [HideInInspector]
  public List<EventDelegate> onFinished = new List<EventDelegate>();
  private float mAmountPerDelta = 1000f;
  public static UITweener current;
  [HideInInspector]
  public UITweener.Method method;
  [HideInInspector]
  public UITweener.Style style;
  [HideInInspector]
  public float delay;
  [HideInInspector]
  public bool steeperCurves;
  [HideInInspector]
  public int tweenGroup;
  [HideInInspector]
  public GameObject eventReceiver;
  [HideInInspector]
  public string callWhenFinished;
  private bool mStarted;
  private float mStartTime;
  private float mDuration;
  private float mFactor;
  private List<EventDelegate> mTemp;

  public float amountPerDelta
  {
    get
    {
      if ((double) this.mDuration != (double) this.duration)
      {
        this.mDuration = this.duration;
        this.mAmountPerDelta = Mathf.Abs((double) this.duration <= 0.0 ? 1000f : 1f / this.duration) * Mathf.Sign(this.mAmountPerDelta);
      }
      return this.mAmountPerDelta;
    }
  }

  public float tweenFactor
  {
    get
    {
      return this.mFactor;
    }
    set
    {
      this.mFactor = Mathf.Clamp01(value);
    }
  }

  public AnimationOrTween.Direction direction
  {
    get
    {
      return (double) this.amountPerDelta < 0.0 ? AnimationOrTween.Direction.Reverse : AnimationOrTween.Direction.Forward;
    }
  }

  private void Reset()
  {
    if (this.mStarted)
      return;
    this.SetStartToCurrentValue();
    this.SetEndToCurrentValue();
  }

  protected virtual void Start()
  {
    this.Update();
  }

  private void Update()
  {
    float num1 = !this.ignoreTimeScale ? Time.deltaTime : RealTime.deltaTime;
    float num2 = !this.ignoreTimeScale ? Time.time : RealTime.time;
    if (!this.mStarted)
    {
      this.mStarted = true;
      this.mStartTime = num2 + this.delay;
    }
    if ((double) num2 < (double) this.mStartTime)
      return;
    this.mFactor += this.amountPerDelta * num1;
    if (this.style == UITweener.Style.Loop)
    {
      if ((double) this.mFactor > 1.0)
        this.mFactor -= Mathf.Floor(this.mFactor);
    }
    else if (this.style == UITweener.Style.PingPong)
    {
      if ((double) this.mFactor > 1.0)
      {
        this.mFactor = (float) (1.0 - ((double) this.mFactor - (double) Mathf.Floor(this.mFactor)));
        this.mAmountPerDelta = -this.mAmountPerDelta;
      }
      else if ((double) this.mFactor < 0.0)
      {
        this.mFactor = -this.mFactor;
        this.mFactor -= Mathf.Floor(this.mFactor);
        this.mAmountPerDelta = -this.mAmountPerDelta;
      }
    }
    if (this.style == UITweener.Style.Once && ((double) this.duration == 0.0 || (double) this.mFactor > 1.0 || (double) this.mFactor < 0.0))
    {
      this.mFactor = Mathf.Clamp01(this.mFactor);
      this.Sample(this.mFactor, true);
      if ((double) this.duration == 0.0 || (double) this.mFactor == 1.0 && (double) this.mAmountPerDelta > 0.0 || (double) this.mFactor == 0.0 && (double) this.mAmountPerDelta < 0.0)
        this.enabled = false;
      if (!((UnityEngine.Object) UITweener.current == (UnityEngine.Object) null))
        return;
      UITweener.current = this;
      if (this.onFinished != null)
      {
        this.mTemp = this.onFinished;
        this.onFinished = new List<EventDelegate>();
        EventDelegate.Execute(this.mTemp);
        for (int index = 0; index < this.mTemp.Count; ++index)
        {
          EventDelegate ev = this.mTemp[index];
          if (ev != null && !ev.oneShot)
            EventDelegate.Add(this.onFinished, ev, ev.oneShot);
        }
        this.mTemp = (List<EventDelegate>) null;
      }
      if ((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.callWhenFinished))
        this.eventReceiver.SendMessage(this.callWhenFinished, (object) this, SendMessageOptions.DontRequireReceiver);
      UITweener.current = (UITweener) null;
    }
    else
      this.Sample(this.mFactor, false);
  }

  public void SetOnFinished(EventDelegate.Callback del)
  {
    EventDelegate.Set(this.onFinished, del);
  }

  public void SetOnFinished(EventDelegate del)
  {
    EventDelegate.Set(this.onFinished, del);
  }

  public void AddOnFinished(EventDelegate.Callback del)
  {
    EventDelegate.Add(this.onFinished, del);
  }

  public void AddOnFinished(EventDelegate del)
  {
    EventDelegate.Add(this.onFinished, del);
  }

  public void RemoveOnFinished(EventDelegate del)
  {
    if (this.onFinished != null)
      this.onFinished.Remove(del);
    if (this.mTemp == null)
      return;
    this.mTemp.Remove(del);
  }

  private void OnDisable()
  {
    this.mStarted = false;
  }

  public void Sample(float factor, bool isFinished)
  {
    float num1 = Mathf.Clamp01(factor);
    if (this.method == UITweener.Method.EaseIn)
    {
      num1 = 1f - Mathf.Sin((float) (1.57079637050629 * (1.0 - (double) num1)));
      if (this.steeperCurves)
        num1 *= num1;
    }
    else if (this.method == UITweener.Method.EaseOut)
    {
      num1 = Mathf.Sin(1.570796f * num1);
      if (this.steeperCurves)
      {
        float num2 = 1f - num1;
        num1 = (float) (1.0 - (double) num2 * (double) num2);
      }
    }
    else if (this.method == UITweener.Method.EaseInOut)
    {
      num1 -= Mathf.Sin(num1 * 6.283185f) / 6.283185f;
      if (this.steeperCurves)
      {
        float f = (float) ((double) num1 * 2.0 - 1.0);
        float num2 = Mathf.Sign(f);
        float num3 = 1f - Mathf.Abs(f);
        float num4 = (float) (1.0 - (double) num3 * (double) num3);
        num1 = (float) ((double) num2 * (double) num4 * 0.5 + 0.5);
      }
    }
    else if (this.method == UITweener.Method.BounceIn)
      num1 = this.BounceLogic(num1);
    else if (this.method == UITweener.Method.BounceOut)
      num1 = 1f - this.BounceLogic(1f - num1);
    this.OnUpdate(this.animationCurve == null ? num1 : this.animationCurve.Evaluate(num1), isFinished);
  }

  private float BounceLogic(float val)
  {
    val = (double) val >= 0.363635987043381 ? ((double) val >= 0.727271974086761 ? ((double) val >= 0.909089982509613 ? (float) (121.0 / 16.0 * (double) (val -= 0.9545454f) * (double) val + 63.0 / 64.0) : (float) (121.0 / 16.0 * (double) (val -= 0.818181f) * (double) val + 15.0 / 16.0)) : (float) (121.0 / 16.0 * (double) (val -= 0.545454f) * (double) val + 0.75)) : 7.5685f * val * val;
    return val;
  }

  [Obsolete("Use PlayForward() instead")]
  public void Play()
  {
    this.Play(true);
  }

  public void PlayForward()
  {
    this.Play(true);
  }

  public void PlayReverse()
  {
    this.Play(false);
  }

  public void Play(bool forward)
  {
    this.mAmountPerDelta = Mathf.Abs(this.amountPerDelta);
    if (!forward)
      this.mAmountPerDelta = -this.mAmountPerDelta;
    this.enabled = true;
    this.Update();
  }

  public void ResetToBeginning()
  {
    this.mStarted = false;
    this.mFactor = (double) this.amountPerDelta >= 0.0 ? 0.0f : 1f;
    this.Sample(this.mFactor, false);
  }

  public void Toggle()
  {
    this.mAmountPerDelta = (double) this.mFactor <= 0.0 ? Mathf.Abs(this.amountPerDelta) : -this.amountPerDelta;
    this.enabled = true;
  }

  protected abstract void OnUpdate(float factor, bool isFinished);

  public static T Begin<T>(GameObject go, float duration) where T : UITweener
  {
    T obj = go.GetComponent<T>();
    if ((UnityEngine.Object) obj != (UnityEngine.Object) null && obj.tweenGroup != 0)
    {
      obj = (T) null;
      T[] components = go.GetComponents<T>();
      int index = 0;
      for (int length = components.Length; index < length; ++index)
      {
        obj = components[index];
        if (!((UnityEngine.Object) obj != (UnityEngine.Object) null) || obj.tweenGroup != 0)
          obj = (T) null;
        else
          break;
      }
    }
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
    {
      obj = go.AddComponent<T>();
      if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      {
        Debug.LogError((object) ("Unable to add " + (object) typeof (T) + " to " + NGUITools.GetHierarchy(go)), (UnityEngine.Object) go);
        return (T) null;
      }
    }
    obj.mStarted = false;
    obj.duration = duration;
    obj.mFactor = 0.0f;
    obj.mAmountPerDelta = Mathf.Abs(obj.amountPerDelta);
    obj.style = UITweener.Style.Once;
    obj.animationCurve = new AnimationCurve(new Keyframe[2]
    {
      new Keyframe(0.0f, 0.0f, 0.0f, 1f),
      new Keyframe(1f, 1f, 1f, 0.0f)
    });
    obj.eventReceiver = (GameObject) null;
    obj.callWhenFinished = (string) null;
    obj.enabled = true;
    return obj;
  }

  public virtual void SetStartToCurrentValue()
  {
  }

  public virtual void SetEndToCurrentValue()
  {
  }

  public enum Method
  {
    Linear,
    EaseIn,
    EaseOut,
    EaseInOut,
    BounceIn,
    BounceOut,
  }

  public enum Style
  {
    Once,
    Loop,
    PingPong,
  }
}
