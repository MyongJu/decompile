﻿// Decompiled with JetBrains decompiler
// Type: DiceTenWinPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DiceTenWinPopup : Popup
{
  private List<float> _rewardNodeTransformsX = new List<float>();
  private Dictionary<int, DiceRewardWinSlot> _itemDict = new Dictionary<int, DiceRewardWinSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private const int TOTAL_REWARD_COUNT = 10;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private Transform[] _iconRewardWinNodes;
  [SerializeField]
  private Animator _diceWinAnimator;
  [SerializeField]
  private UIButton _diceTenBtn;
  [SerializeField]
  private UILabel _diceTenLabel;
  [SerializeField]
  private UITexture _diceTenTexture;
  private List<DiceData.RewardData> _rewardDataList;
  private System.Action _onTenWinFinished;
  private System.Action _onPlayTenDice;
  private bool _isPopupHide;
  private Color _dicePlayCostTextColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DiceTenWinPopup.Parameter parameter = orgParam as DiceTenWinPopup.Parameter;
    if (parameter != null)
    {
      this._rewardDataList = parameter.rewardDataList;
      this._onTenWinFinished = parameter.onTenWinFinished;
      this._onPlayTenDice = parameter.onPlayTenDice;
    }
    this._dicePlayCostTextColor = this._diceTenLabel.color;
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this._isPopupHide = true;
    this.ClearData();
    this.ExecuteTenWinFinished();
  }

  public override string Type
  {
    get
    {
      return "Dice/DiceTenWinPopup";
    }
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOkayBtnPressed()
  {
    this.OnCloseBtnPressed();
  }

  public void OnRewardAnimSlammed()
  {
    this._diceWinAnimator.Play("dice_ten_win_reset");
    this.StopMultiWinAnim();
  }

  public void OnPlayTenPressed()
  {
    if (this._onPlayTenDice == null)
      return;
    this.OnCloseBtnPressed();
    this._onPlayTenDice();
  }

  private void ExecuteTenWinFinished()
  {
    if (this._onTenWinFinished == null)
      return;
    this._onTenWinFinished();
  }

  private void StopMultiWinAnim()
  {
    using (Dictionary<int, DiceRewardWinSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.StopMultiWinAnim();
    }
  }

  [DebuggerHidden]
  private IEnumerator CheckSingleRewardStatus()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DiceTenWinPopup.\u003CCheckSingleRewardStatus\u003Ec__Iterator4F()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateUI()
  {
    if (this._rewardDataList == null || this._rewardDataList.Count != 10)
      return;
    if (this._rewardNodeTransformsX != null)
    {
      this._rewardNodeTransformsX.Clear();
      for (int index = 0; index < this._iconRewardWinNodes.Length; ++index)
        this._rewardNodeTransformsX.Add(this._iconRewardWinNodes[index].localPosition.x);
    }
    MarksmanPayload.Instance.FeedMarksmanTexture(this._diceTenTexture, DicePayload.Instance.TempDiceData.PropsImagePath);
    this._diceTenLabel.text = DicePayload.Instance.DiceTenCost.ToString();
    this._diceTenLabel.color = !DicePayload.Instance.HasEnoughProps2PlayTen ? Color.red : this._dicePlayCostTextColor;
    for (int key = 0; key < this._iconRewardWinNodes.Length; ++key)
    {
      if (key < this._rewardDataList.Count)
      {
        DiceRewardWinSlot itemRenderer = this.CreateItemRenderer(this._rewardDataList[key], this._iconRewardWinNodes[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    this.ShowRewardAnimation();
    Utils.ExecuteInSecs(0.5f, (System.Action) (() =>
    {
      if (this._isPopupHide)
        return;
      this.StartCoroutine(this.CheckSingleRewardStatus());
    }));
  }

  private void ShowRewardAnimation()
  {
    this._diceWinAnimator.Play("dice_ten_win");
  }

  private void ClearData()
  {
    using (Dictionary<int, DiceRewardWinSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private DiceRewardWinSlot CreateItemRenderer(DiceData.RewardData rewardData, Transform parent)
  {
    this._itemPool.Initialize(this._itemPrefab, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    DiceRewardWinSlot component = gameObject.GetComponent<DiceRewardWinSlot>();
    component.SetData(rewardData);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<DiceData.RewardData> rewardDataList;
    public System.Action onTenWinFinished;
    public System.Action onPlayTenDice;
  }
}
