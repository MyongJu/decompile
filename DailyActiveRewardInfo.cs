﻿// Decompiled with JetBrains decompiler
// Type: DailyActiveRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DailyActiveRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "req_score")]
  public int RequrieScore;
  [Config(Name = "wood")]
  public int RewardWood;
  [Config(Name = "food")]
  public int RewardFood;
  [Config(Name = "ore")]
  public int RewardOre;
  [Config(Name = "silver")]
  public int RewardSilver;
  [Config(Name = "lordexp")]
  public int RewardLordExp;
  [Config(Name = "dragonexp")]
  public int RewardDragonExp;
  [Config(Name = "reward_id_1")]
  public int RewardItem1;
  [Config(Name = "reward_value_1")]
  public float RewardItemValue1;
  [Config(Name = "reward_id_2")]
  public int RewardItem2;
  [Config(Name = "reward_value_2")]
  public float RewardItemValue2;
  [Config(Name = "reward_id_3")]
  public int RewardItem3;
  [Config(Name = "reward_value_3")]
  public float RewardItemValue3;
  [Config(Name = "reward_id_4")]
  public int RewardItem4;
  [Config(Name = "reward_value_4")]
  public float RewardItemValue4;
  [Config(Name = "lucky_archer_coin")]
  public int LuckArcherCoin;

  public float Ratio { get; set; }
}
