﻿// Decompiled with JetBrains decompiler
// Type: BoostItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BoostItem : MonoBehaviour
{
  public UILabel titleLb;
  public UILabel descriptionLb;
  public UILabel basicEffectLb;
  public UILabel tempEffectLb;
  public UILabel timeLb;
  private int time;
  private float _startTime;

  public void SetData(BoostItemData data)
  {
    this.titleLb.text = data.title;
    this.descriptionLb.text = string.Empty;
    this.tempEffectLb.text = string.Empty;
    this.time = data.leftTime;
    this._startTime = data.startTime;
    BoostsConst.SetupEffectStr(this.basicEffectLb, data.basicEffect);
    if (this.time >= 0)
      this.timeLb.text = Utils.FormatTime(this.time, false, false, true);
    else
      this.timeLb.text = string.Empty;
  }

  public void Release()
  {
  }

  private void Update()
  {
    if (this.time < 0)
      return;
    this.timeLb.text = Utils.FormatTime(Mathf.Max(0, this.time - (int) ((double) Time.time - (double) this._startTime)), false, false, true);
  }
}
