﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerBenefitItemRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class MerlinTowerBenefitItemRender : MonoBehaviour
{
  private const string USE_CONSUME = "id_use_consume";
  private const string HAVE_BOUGHT = "tower_floor_benefit_bought_description";
  public UILabel benefitName;
  public UILabel cost;
  public UILabel bought;
  public UILabel price;
  public UITexture benefitIcon;
  public System.Action<MerlinTowerBenefitItemRender> OnItemClickedHandler;
  private bool isBought;
  private MerlinTowerBenefitData data;
  private int type;
  private int currentLayer;
  private int boughtLayer;
  private bool boughtInCurrentLayer;

  public bool IsBought
  {
    get
    {
      return this.isBought;
    }
  }

  public bool Bought
  {
    set
    {
      if (!value)
        return;
      NGUITools.SetActive(this.bought.gameObject, value);
      this.GetComponent<BoxCollider>().enabled = false;
      this.bought.text = ScriptLocalization.GetWithPara("tower_floor_benefit_bought_description", new Dictionary<string, string>()
      {
        {
          "0",
          this.currentLayer.ToString()
        }
      }, true);
    }
  }

  public bool Disable
  {
    set
    {
      if (!value)
        return;
      this.bought.text = string.Empty;
      NGUITools.SetActive(this.bought.gameObject, value);
    }
  }

  public MerlinTowerBenefitData Data
  {
    get
    {
      return this.data;
    }
  }

  public void FeedData(MerlinTowerBenefitData data)
  {
    if (data == null)
      return;
    this.data = data;
    this.type = data.type;
    this.currentLayer = data.currentLayer;
    this.boughtLayer = data.boughtLayer;
    this.isBought = data.isBought;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.cost.text = ScriptLocalization.Get("id_use_consume", true);
    this.price.text = this.data.price.ToString();
    this.bought.gameObject.SetActive(false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.benefitIcon, this.data.benefitIcon, (System.Action<bool>) null, true, false, string.Empty);
    if (this.type == 2)
      this.benefitName.text = ScriptLocalization.GetWithPara(this.data.benefitName, new Dictionary<string, string>()
      {
        {
          "0",
          Utils.GetBenefitValueString(this.data.format, (double) this.data.value)
        }
      }, true);
    else
      this.benefitName.text = this.data.benefitName + Utils.GetBenefitValueString(this.data.format, (double) this.data.value);
    this.UpdateBoughtItem();
  }

  private void UpdateBoughtItem()
  {
    if (this.boughtLayer == -1)
      return;
    NGUITools.SetActive(this.bought.gameObject, true);
    this.GetComponent<BoxCollider>().enabled = false;
    this.bought.text = ScriptLocalization.GetWithPara("tower_floor_benefit_bought_description", new Dictionary<string, string>()
    {
      {
        "0",
        this.boughtLayer.ToString()
      }
    }, true);
  }

  public void OnItemClicked()
  {
    if (this.OnItemClickedHandler == null)
      return;
    this.OnItemClickedHandler(this);
  }

  public enum BenefitType
  {
    DirectlyUse = 1,
    MarchCapacity = 2,
  }
}
