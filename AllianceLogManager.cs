﻿// Decompiled with JetBrains decompiler
// Type: AllianceLogManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceLogManager
{
  private Dictionary<int, string> allianceLogCatetory = new Dictionary<int, string>()
  {
    {
      0,
      "member"
    },
    {
      1,
      "event"
    },
    {
      2,
      "battle"
    }
  };
  private Dictionary<string, Color> typeBGMap = new Dictionary<string, Color>();
  private Dictionary<string, string> typeSpriteMap = new Dictionary<string, string>();
  private Dictionary<int, string> categoryMap = new Dictionary<int, string>();
  private Dictionary<int, string> portalMonsterLevelMap = new Dictionary<int, string>();
  private const string ALLIANCE_LOG_MEMBER = "alliance_log_member_";
  private const string ALLIANCE_LOG_EVENT = "alliance_log_event_";
  private const string ALLIANCE_LOG_BATTLE = "alliance_log_battle_";
  private const int PORTAL_MONSTER_LEVEL_COUNT = 5;
  private static AllianceLogManager _instance;
  private ArrayList _dataList;

  public event System.Action OnDataReceived;

  public static AllianceLogManager Instance
  {
    get
    {
      if (AllianceLogManager._instance == null)
        AllianceLogManager._instance = new AllianceLogManager();
      return AllianceLogManager._instance;
    }
  }

  public void InitCommonData()
  {
    this.InitPortalMonsterLevel();
    this.InitLogBGColor();
    this.InitLogLogo();
    this.InitCatetoryMap();
  }

  public Dictionary<string, Color> TypeBGMap
  {
    get
    {
      return this.typeBGMap;
    }
  }

  public Dictionary<string, string> TypeSpriteMap
  {
    get
    {
      return this.typeSpriteMap;
    }
  }

  public Dictionary<int, string> CategoryMap
  {
    get
    {
      return this.categoryMap;
    }
  }

  public Dictionary<int, string> PortalMonsterLevelMap
  {
    get
    {
      return this.portalMonsterLevelMap;
    }
  }

  public ArrayList DataList
  {
    get
    {
      return this._dataList;
    }
  }

  private void Decode(object orgData)
  {
    if (orgData == null)
      return;
    if (this._dataList != null)
    {
      this._dataList.Clear();
      this._dataList = (ArrayList) null;
    }
    this._dataList = orgData as ArrayList;
  }

  public void RequestAllianceCategoryLog(System.Action<bool, object> callback, int index, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    if (this.allianceLogCatetory.ContainsKey(index))
    {
      postData.Add((object) "uid", (object) PlayerData.inst.uid);
      postData.Add((object) "type", (object) this.allianceLogCatetory[index]);
    }
    MessageHub.inst.GetPortByAction("Alliance:loadAllianceDynamicByCategory").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
        this.Decode(data);
      if (callback == null)
        return;
      callback(ret, data);
    }), blockScreen);
  }

  private void InitPortalMonsterLevel()
  {
    string str1 = "alliance_portal_monster_level_";
    string str2 = "_subtitle";
    for (int key = 0; key < 5; ++key)
    {
      string str3 = str1 + (object) (key + 1) + str2;
      this.portalMonsterLevelMap.Add(key, str3);
    }
  }

  private void InitLogBGColor()
  {
    this.typeBGMap.Add("join_in", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("permited_in", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("invited_in", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("leave", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("kicked_out", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("title_up", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("title_down", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("leader_change", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("building_completed", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("level_up", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("destroy_building", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("building_move", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("building_settle", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("research_level_up", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("research_choose", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("research_speed_up", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("research_mark", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("shop_item_online", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("fallen_knight_start", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("BOSS_start", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("BOSS_killed", Utils.GetColorFromHex(6912863U));
    this.typeBGMap.Add("BOSS_failed", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("member_attacked", Utils.GetColorFromHex(8939371U));
    this.typeBGMap.Add("member_attack", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("building_destroyed", Utils.GetColorFromHex(8939371U));
    this.typeBGMap.Add("member_city_attacked", Utils.GetColorFromHex(8939371U));
    this.typeBGMap.Add("member_attack_city", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("building_attack", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("building_destroy", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("wonder_avalon_occupying", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("wonder_tower_occupying", Utils.GetColorFromHex(8553090U));
    this.typeBGMap.Add("wonder_avalon_occupied", Utils.GetColorFromHex(8553090U));
  }

  private void InitLogLogo()
  {
    this.typeSpriteMap.Add("join_in", "icon_alliance_join");
    this.typeSpriteMap.Add("permited_in", "icon_alliance_join");
    this.typeSpriteMap.Add("invited_in", "icon_alliance_join");
    this.typeSpriteMap.Add("leave", "icon_alliance_kick_out");
    this.typeSpriteMap.Add("kicked_out", "icon_alliance_kick_out");
    this.typeSpriteMap.Add("title_up", "icon_alliance");
    this.typeSpriteMap.Add("title_down", "icon_alliance");
    this.typeSpriteMap.Add("leader_change", "icon_alliance");
    this.typeSpriteMap.Add("building_completed", "icon_alliance_fortress");
    this.typeSpriteMap.Add("level_up", "icon_alliance_fortress");
    this.typeSpriteMap.Add("destroy_building", "icon_alliance_fortress");
    this.typeSpriteMap.Add("building_move", "icon_alliance_fortress");
    this.typeSpriteMap.Add("building_settle", "icon_alliance_fortress");
    this.typeSpriteMap.Add("research_level_up", "icon_tech");
    this.typeSpriteMap.Add("research_choose", "icon_tech");
    this.typeSpriteMap.Add("research_speed_up", "icon_tech");
    this.typeSpriteMap.Add("research_mark", "icon_tech");
    this.typeSpriteMap.Add("shop_item_online", "icon_alliance_store");
    this.typeSpriteMap.Add("fallen_knight_start", "icon_intergral_fallen_knight");
    this.typeSpriteMap.Add("BOSS_start", "icon_alliance_boss");
    this.typeSpriteMap.Add("BOSS_failed", "icon_alliance_boss");
    this.typeSpriteMap.Add("BOSS_killed", "icon_alliance_boss");
    this.typeSpriteMap.Add("building_destroyed", "icon_alliance_war");
    this.typeSpriteMap.Add("member_attacked", "icon_alliance_war");
    this.typeSpriteMap.Add("member_attack", "icon_alliance_war");
    this.typeSpriteMap.Add("member_city_attacked", "icon_alliance_war");
    this.typeSpriteMap.Add("member_attack_city", "icon_alliance_war");
    this.typeSpriteMap.Add("building_attack", "icon_alliance_war");
    this.typeSpriteMap.Add("building_destroy", "icon_alliance_war");
    this.typeSpriteMap.Add("wonder_avalon_occupying", "icon_alliance_war");
    this.typeSpriteMap.Add("wonder_tower_occupying", "icon_alliance_war");
    this.typeSpriteMap.Add("wonder_avalon_occupied", "icon_alliance_war");
  }

  private void InitCatetoryMap()
  {
    this.categoryMap.Add(0, "alliance_log_member_");
    this.categoryMap.Add(1, "alliance_log_event_");
    this.categoryMap.Add(2, "alliance_log_battle_");
  }

  public void ClearCommonData()
  {
    if (this.typeBGMap != null)
      this.typeBGMap.Clear();
    if (this.typeSpriteMap != null)
      this.typeSpriteMap.Clear();
    if (this.categoryMap != null)
      this.categoryMap.Clear();
    if (this.portalMonsterLevelMap == null)
      return;
    this.portalMonsterLevelMap.Clear();
  }
}
