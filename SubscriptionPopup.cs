﻿// Decompiled with JetBrains decompiler
// Type: SubscriptionPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class SubscriptionPopup : Popup
{
  private List<int> separator = new List<int>();
  private int curLevel = 4;
  private const string gifticon = "Texture/ItemIcons/item_month_card_gift";
  private const string dockicon = "Texture/GUI_Textures/dock-1";
  private const string headericon = "Texture/GUI_Textures/month_card_top_banner";
  private const string goldicon = "Texture/BuildingConstruction/gold_coin";
  private int curProsperity;
  public UIButton collectBtn;
  public UILabel refreshTime;
  public UILabel remainingDay;
  public IconListComponent ilc;
  public UITexture dock;
  public UITexture header;
  public GameObject activeGroup;
  public GameObject inactiveGroup;
  public GameObject hinteffect;
  public UIButton closeButton;
  public UIButton cancelButton;
  public UIButton subscribeButton;
  public UIButton resumeButton;
  public UIButton collectButton;
  public UIButton webPrivacyPolicy;
  public UIButton dailyPackageButton;
  public HelpBtnComponent helpButton;
  public UITable benefitTable;
  public GameObject benefitTemplate;
  public UITable itemTable;
  public GameObject itemTemplate;
  [Header("localize")]
  public UILabel Title;
  public UILabel daily_delivery;
  public UILabel cancellabel;
  public UILabel subscribelabel;
  public UILabel collectlabel;
  public UILabel resumelabel;
  public UILabel expirelabel;
  public UILabel costlabel;
  public UILabel awardDiscription;
  public UILabel checkDiscription;
  public UILabel _labelDescription;
  private bool OnceInited;
  private Coroutine _checkSubscription;

  public void OnCollectButtonClicked()
  {
    SubscriptionSystem.Instance.collectSubscription();
  }

  private void _OnShow()
  {
    if (!this.OnceInited)
    {
      this._InitLocalize();
      this._InitDelegate();
      this.OnceInited = true;
    }
    bool flag = SubscriptionSystem.Instance.HasStarted();
    this.activeGroup.SetActive(flag);
    this.inactiveGroup.SetActive(!flag);
    if (flag)
    {
      bool state = SubscriptionSystem.Instance.IsActive();
      Debug.Log((object) string.Format("{0} subscription refresh panel called,active {1}", (object) Time.time, (object) state));
      NGUITools.SetActive(this.cancelButton.gameObject, state);
      NGUITools.SetActive(this.resumeButton.gameObject, !state);
      if (state)
      {
        this.ClearCoroutine();
        this._checkSubscription = this.StartCoroutine(this.UpdateResumeButton());
      }
      this.collectBtn.isEnabled = state && SubscriptionSystem.Instance.CanCollect();
    }
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.dailyPackageButton)
      this.dailyPackageButton.isEnabled = !IAPDailyRechargePackagePayload.Instance.IsSwitchOff;
    this.separator.Clear();
    SubscriptionInfo subInfo = SubscriptionSystem.Instance.GetSubInfo();
    List<IconData> iconDataList = new List<IconData>();
    if (this.itemTable.GetChildList().Count <= 0)
    {
      using (Dictionary<long, int>.Enumerator enumerator = subInfo.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, int> current = enumerator.Current;
          int num = current.Value;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem((int) current.Key);
          IconData iconData = new IconData(itemStaticInfo.ImagePath, new string[1]
          {
            num.ToString()
          });
          iconData.Data = (object) itemStaticInfo.internalId;
          GameObject go = NGUITools.AddChild(this.itemTable.gameObject, this.itemTemplate);
          if ((bool) ((UnityEngine.Object) go))
          {
            NGUITools.SetActive(go, true);
            Icon component = go.GetComponent<Icon>();
            component.StopAutoFillName();
            component.FeedData((IComponentData) iconData);
            component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
            component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
            component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
          }
        }
      }
      this.itemTable.Reposition();
    }
    if (this.benefitTable.GetChildList().Count <= 0)
    {
      using (Dictionary<long, float>.Enumerator enumerator = subInfo.benefits.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<long, float> current = enumerator.Current;
          float num = current.Value;
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[(int) current.Key];
          GameObject go = NGUITools.AddChild(this.benefitTable.gameObject, this.benefitTemplate);
          if ((bool) ((UnityEngine.Object) go))
          {
            NGUITools.SetActive(go, true);
            BenefitItemRenderer component = go.GetComponent<BenefitItemRenderer>();
            component.UseTargetImage = false;
            component.SetData((double) num, dbProperty, dbProperty.IsNegative, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR, BenefitItemRenderer.DESC_COLOR);
          }
        }
      }
      this.benefitTable.Reposition();
    }
    this.UpdateTime(0);
  }

  private void _InitLocalize()
  {
    SubscriptionInfo subInfo = SubscriptionSystem.Instance.GetSubInfo();
    this.Title.text = Utils.XLAT("daily_delivery_uppercase_subscription");
    this.daily_delivery.text = Utils.XLAT("daily_delivery_subscription_daily_rewards");
    this.costlabel.text = string.Format(Utils.XLAT("daily_delivery_subscription_cost_num"), (object) PaymentManager.Instance.GetFormattedPrice(SubscriptionSystem.Instance.GetProductID(subInfo.pay_id), subInfo.pay_id));
    this.cancellabel.text = Utils.XLAT("id_uppercase_unsubscribe");
    this.subscribelabel.text = Utils.XLAT("id_uppercase_subscribe");
    this.collectlabel.text = Utils.XLAT("id_uppercase_claim");
    this.resumelabel.text = Utils.XLAT("daily_delivery_subscription_restore_button");
    this.awardDiscription.text = Utils.XLAT("iap_daily_gift_pack_bonus_description");
    this.checkDiscription.text = Utils.XLAT("iap_daily_gift_pack_bonus_name");
    if (SubscriptionSystem.Instance.RemainDays() > 0)
    {
      this.expirelabel.gameObject.SetActive(true);
      this.expirelabel.text = string.Format(Utils.XLAT("daily_delivery_subscription_finish_tip"), (object) SubscriptionSystem.Instance.RemainDays());
    }
    else
      this.expirelabel.gameObject.SetActive(false);
    if ((bool) ((UnityEngine.Object) this._labelDescription))
      this._labelDescription.text = ScriptLocalization.Get("help_subscription_description", true);
    if (!((UnityEngine.Object) this.helpButton != (UnityEngine.Object) null))
      return;
    this.helpButton.ID = this.GetHelpButtonID();
  }

  private string GetHelpButtonID()
  {
    return "help_subscription";
  }

  private void OnCancelSubscribe()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = this.GetHelpButtonID()
    });
  }

  private void _InitDelegate()
  {
    this.closeButton.onClick.Clear();
    EventDelegate.Add(this.closeButton.onClick, (EventDelegate.Callback) (() => UIManager.inst.CloseTopPopup((Popup.PopupParameter) null)));
    this.cancelButton.onClick.Clear();
    EventDelegate.Add(this.cancelButton.onClick, new EventDelegate.Callback(this.OnCancelSubscribe));
    this.collectBtn.onClick.Clear();
    EventDelegate.Add(this.collectBtn.onClick, (EventDelegate.Callback) (() => SubscriptionSystem.Instance.collectSubscription()));
    this.subscribeButton.onClick.Clear();
    EventDelegate.Add(this.subscribeButton.onClick, (EventDelegate.Callback) (() => SubscriptionSystem.Instance.activateSubscription()));
    this.resumeButton.onClick.Clear();
    EventDelegate.Add(this.resumeButton.onClick, (EventDelegate.Callback) (() => SubscriptionSystem.Instance.resumeSubscription()));
    EventDelegate.Add(this.webPrivacyPolicy.onClick, (EventDelegate.Callback) (() => Application.OpenURL("http://www.funplus.com/privacy_policy/")));
  }

  private void OnUpdate()
  {
    this._OnShow();
  }

  private void UpdateTime(int obj)
  {
    NetServerTime.inst.TodayLeftTimeUTC();
  }

  private void OnDBChanged(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    this._OnShow();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    SubscriptionEvents.updateEvent.AddListener(new System.Action(this.OnUpdate));
    this._OnShow();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateTime);
    Utils.RecordPopupId(this.ID);
    BuilderFactory.Instance.Build((UIWidget) this.header, "Texture/GUI_Textures/npc_auction", (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateTime);
    SubscriptionEvents.updateEvent.RemoveListener(new System.Action(this.OnUpdate));
    Utils.SetPopupLevel(this.ID);
  }

  [DebuggerHidden]
  private IEnumerator UpdateResumeButton()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SubscriptionPopup.\u003CUpdateResumeButton\u003Ec__Iterator46()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void OnDailyRewardsBtnPressed()
  {
    List<int> popupIds = IAPDailyRechargePackagePayload.Instance.popupIDs;
    bool flag = false;
    if (popupIds.Count == 2)
    {
      while (popupIds.Count > 0)
        UIManager.inst.ClosePopup(popupIds[0], (Popup.PopupParameter) null);
      flag = true;
    }
    if (flag)
    {
      UIManager.inst.OpenPopup("SubscibePopup", (Popup.PopupParameter) null);
      UIManager.inst.OpenPopup("IAP/IAPDailyRechargePopup", (Popup.PopupParameter) null);
    }
    else
      UIManager.inst.OpenPopup("IAP/IAPDailyRechargePopup", (Popup.PopupParameter) null);
  }

  private void ClearCoroutine()
  {
    if (this._checkSubscription == null)
      return;
    this.StopCoroutine(this._checkSubscription);
    this._checkSubscription = (Coroutine) null;
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }
}
