﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightSkillInfoPopup : Popup
{
  private Benefits _skillBenefits = new Benefits();
  public UITexture skillTexture;
  public UILabel skillName;
  public UILabel skillStateText;
  public EquipmentBenefits benefitContent;
  public UILabel additionDescText;
  public UITable table;
  private DragonKnightSkillInfo _skillInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DragonKnightSkillInfoPopup.Parameter parameter = orgParam as DragonKnightSkillInfoPopup.Parameter;
    if (parameter != null)
      this._skillInfo = parameter.skillInfo;
    this.ShowSkillDetail();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowSkillDetail()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.skillTexture, this._skillInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.skillName.text = this._skillInfo.Name;
    this.skillStateText.text = this._skillInfo.CurrentStateDesc;
    if (!((UnityEngine.Object) this.benefitContent != (UnityEngine.Object) null))
      return;
    this.ResetBenefits();
    this.benefitContent.Init(this._skillBenefits);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
    {
      if (this._skillInfo.CurrentState != DragonKnightSkillInfo.SkillState.LOCKED)
        return;
      NGUITools.SetActive(this.additionDescText.transform.parent.gameObject, true);
      this.table.repositionNow = true;
      this.table.Reposition();
      if ((UnityEngine.Object) this.benefitContent.scrollView != (UnityEngine.Object) null)
        this.benefitContent.scrollView.ResetPosition();
      this.additionDescText.text = ScriptLocalization.GetWithPara("dragon_knight_skill_unlock_level_description", new Dictionary<string, string>()
      {
        {
          "0",
          this._skillInfo.unlockLevel.ToString()
        }
      }, true);
      this.additionDescText.color = Color.red;
      if (this._skillInfo.benefits == null)
        return;
      this.table.transform.localPosition = new Vector3(300f, this.table.transform.localPosition.y, this.table.transform.localPosition.z);
    }));
  }

  private void ResetBenefits()
  {
    DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
    if (this._skillInfo == null || dragonKnightData == null)
      return;
    int num = Mathf.Min(this._skillInfo.CurrentStageMaxLevel, dragonKnightData.Level);
    List<string> stringList = new List<string>((IEnumerable<string>) this._skillInfo.benefits.benefits.Keys);
    for (int index = 0; index < stringList.Count; ++index)
    {
      this._skillBenefits.benefits.Remove(stringList[index]);
      this._skillBenefits.benefits.Add(stringList[index], this._skillInfo.benefits.benefits[stringList[index]] * (float) num);
    }
  }

  public class Parameter : Popup.PopupParameter
  {
    public DragonKnightSkillInfo skillInfo;
  }
}
