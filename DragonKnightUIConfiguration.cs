﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightUIConfiguration
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UnityEngine;

public class DragonKnightUIConfiguration
{
  public const string ASSET_PATH = "Prefab/DragonKnight/Objects/DragonKnightUI";
  private Dictionary<string, float> _configuration;
  private static DragonKnightUIConfiguration _instance;

  public static DragonKnightUIConfiguration Instance
  {
    get
    {
      if (DragonKnightUIConfiguration._instance == null)
        DragonKnightUIConfiguration._instance = new DragonKnightUIConfiguration();
      return DragonKnightUIConfiguration._instance;
    }
  }

  public Dictionary<string, float> GetConfiguration()
  {
    if (this._configuration == null)
    {
      this._configuration = JsonReader.Deserialize<Dictionary<string, float>>((AssetManager.Instance.Load("Prefab/DragonKnight/Objects/DragonKnightUI", (System.Type) null) as TextAsset).text);
      AssetManager.Instance.UnLoadAsset("Prefab/DragonKnight/Objects/DragonKnightUI", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }
    return this._configuration;
  }
}
