﻿// Decompiled with JetBrains decompiler
// Type: SoldierInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SoldierInfo : MonoBehaviour
{
  public UILabel name;
  public UILabel level;
  public UITexture icon;
  public UILabel killed;
  public UILabel wounded;
  public UILabel survived;
  public UILabel kill;

  public void SeedData(SoldierInfo.Data d)
  {
    this.name.text = d.name;
    this.level.text = d.level;
    this.killed.text = d.dead.ToString();
    this.wounded.text = d.wounded.ToString();
    this.survived.text = d.survived.ToString();
    this.kill.text = d.kill.ToString();
    BuilderFactory.Instance.HandyBuild((UIWidget) this.icon, d.icon, (System.Action<bool>) null, true, false, string.Empty);
  }

  public class Data
  {
    public string name;
    public string icon;
    public string level;
    public long survived;
    public long wounded;
    public long dead;
    public long kill;
    public int priority;
  }
}
