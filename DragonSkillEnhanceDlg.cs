﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillEnhanceDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonSkillEnhanceDlg : UI.Dialog
{
  private string _skillType = ConfigDragonSkillGroupInfo.DARK;
  private List<DragonSkillEnhanceItem> _allStarLevelUpItem = new List<DragonSkillEnhanceItem>();
  [SerializeField]
  private DragonSkillEnhanceItem _skillStarLevelUpItemTempalte;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private GameObject _rootCurrentSelectInfo;
  [SerializeField]
  private GameObject _rootCanStarLevelUp;
  [SerializeField]
  private GameObject _rootMaxStarLevel;
  [SerializeField]
  private UILabel _labelCurrentBonus;
  [SerializeField]
  private UILabel _labelNextBounus;
  [SerializeField]
  private UITexture _textureCostItemIcon;
  [SerializeField]
  private UILabel _labelCostItemCount;
  [SerializeField]
  private UIButton _buttonEnhance;
  [SerializeField]
  private UIButton _buttonResetAll;
  [SerializeField]
  private UIToggle _toggleLight;
  [SerializeField]
  private UIToggle _toggleDark;
  [SerializeField]
  private DragonSkillTipItem _tipItem;
  [SerializeField]
  private GameObject _dragonRoot;
  [SerializeField]
  private UITexture _dragonTexture;
  [SerializeField]
  private DragonPowerItem _dragonPowerItem;
  [SerializeField]
  private UILabel _labelCostItemName;
  private DragonView _dragonView;
  private int _currentSelectSkillId;
  private bool _canEnhance;
  private bool _listDirty;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    UICamera.onDrag += new UICamera.VectorDelegate(this.OnDragEvent);
    this._currentSelectSkillId = 0;
    this._skillStarLevelUpItemTempalte.gameObject.SetActive(false);
    this._tipItem.Hide();
    this.UpdateAll();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.CreateDragonView();
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.DestoryDragonView();
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
  }

  protected void OnDragonDataChanged(long dragonId)
  {
    if (PlayerData.inst.uid != dragonId)
      return;
    this.UpdateAll();
  }

  protected void UpdateDragonView()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (this._dragonView == null || !ret)
        return;
      this._dragonView.SetRendererLayer("UI3D");
      this._dragonView.Show();
      this._dragonView.SetAnimationStateTrigger("EnterUI");
    }));
  }

  protected void UpdateAll()
  {
    this._listDirty = true;
    this.UpdateCurrentSelectSkill();
    this.UpdateDragonDetail();
    this.UpdateDragonView();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    UICamera.onDrag -= new UICamera.VectorDelegate(this.OnDragEvent);
  }

  protected void UpdateDragonDetail()
  {
    this._dragonPowerItem.Refresh();
  }

  protected void CreateDragonView()
  {
    if (this._dragonView != null)
      this.DestoryDragonView();
    this._dragonView = new DragonView();
    this._dragonView.Mount(this._dragonRoot.transform);
    this._dragonView.SetPosition(Vector3.zero);
    this._dragonView.SetRotation(Quaternion.identity);
    this._dragonView.SetScale(new Vector3(400f, 400f, 400f));
    this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer("UI3D");
      this._dragonView.Show();
      this._dragonView.SetAnimationStateTrigger("EnterUI");
    }));
  }

  protected void DestoryDragonView()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.Dispose();
    this._dragonView = (DragonView) null;
  }

  public void OnToggleChanged()
  {
    UIToggle activeToggle = UIToggle.GetActiveToggle(2);
    if ((UnityEngine.Object) activeToggle == (UnityEngine.Object) this._toggleLight)
      this.OnButtonLightClicked();
    if (!((UnityEngine.Object) activeToggle == (UnityEngine.Object) this._toggleDark))
      return;
    this.OnButtonDarkClicked();
  }

  public void Update()
  {
    if (!this._listDirty)
      return;
    this._listDirty = false;
    this.UpdateSkillList();
  }

  public void OnButtonLightClicked()
  {
    this._skillType = ConfigDragonSkillGroupInfo.LIGHT;
    this._currentSelectSkillId = 0;
    this._listDirty = true;
  }

  public void OnButtonDarkClicked()
  {
    this._skillType = ConfigDragonSkillGroupInfo.DARK;
    this._currentSelectSkillId = 0;
    this._listDirty = true;
  }

  public void OnSkillIconClicked(DragonSkillEnhanceItem item)
  {
    this._tipItem.SetSkillId(item.SkillId);
    Vector3 position = this._tipItem.transform.position;
    position.y = item.transform.position.y;
    this._tipItem.transform.position = position;
    this._tipItem.Show();
  }

  public void OnSkillReseted(DragonSkillEnhanceItem item)
  {
    this.UpdateCurrentSelectSkill();
    this.UpdateDragonDetail();
    this.UpdateResetAllButtonState();
  }

  public void OnSkillLevelUpItemClicked(DragonSkillEnhanceItem item)
  {
    if (!ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(item.SkillId).group_id))
      return;
    DragonSkillEnhanceItem starLevelUpItemById = this.GetSkillStarLevelUpItemById(this._currentSelectSkillId);
    if ((UnityEngine.Object) starLevelUpItemById != (UnityEngine.Object) null)
      starLevelUpItemById.SetSelected(false);
    item.SetSelected(true);
    this._currentSelectSkillId = item.SkillId;
    this.UpdateCurrentSelectSkill();
  }

  public void UpdateCurrentSelectSkill()
  {
    this._buttonEnhance.isEnabled = false;
    if (this._currentSelectSkillId == 0)
    {
      this._rootCurrentSelectInfo.SetActive(false);
    }
    else
    {
      this._rootCurrentSelectInfo.SetActive(true);
      this._rootCanStarLevelUp.SetActive(false);
      this._rootMaxStarLevel.SetActive(false);
      DragonData dragonData = PlayerData.inst.dragonData;
      if (dragonData == null)
        return;
      int starLevelBySkillId = dragonData.GetSkillGroupStarLevelBySkillId(this._currentSelectSkillId);
      DragonSkillEnhanceInfo byStarLevel1 = ConfigManager.inst.DB_DragonSkillEnhance.GetByStarLevel(starLevelBySkillId);
      DragonSkillEnhanceInfo byStarLevel2 = ConfigManager.inst.DB_DragonSkillEnhance.GetByStarLevel(starLevelBySkillId + 1);
      float num1 = byStarLevel1 == null ? 0.0f : byStarLevel1.EnhanceEffect;
      if (byStarLevel2 == null)
      {
        this._rootMaxStarLevel.SetActive(true);
        this._labelCurrentBonus.text = string.Format("{0}%", (object) (float) ((double) num1 * 100.0));
      }
      else
      {
        float enhanceEffect = byStarLevel2.EnhanceEffect;
        this._labelCurrentBonus.text = string.Format("{0}%", (object) (float) ((double) num1 * 100.0));
        this._labelNextBounus.text = string.Format("{0}%", (object) (float) ((double) enhanceEffect * 100.0));
        this._rootCanStarLevelUp.SetActive(true);
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(byStarLevel2.ItemId);
        if (itemStaticInfo != null)
        {
          this._labelCostItemName.text = ScriptLocalization.GetWithPara("dragon_skill_enhance_items_needed", new Dictionary<string, string>()
          {
            {
              "0",
              itemStaticInfo.LocName
            }
          }, true);
          BuilderFactory.Instance.HandyBuild((UIWidget) this._textureCostItemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
        }
        ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(byStarLevel2.ItemId);
        int num2 = consumableItemData == null ? 0 : consumableItemData.quantity;
        if (num2 < byStarLevel2.ItemCount)
        {
          this._canEnhance = false;
          this._labelCostItemCount.text = string.Format("[ff0000ff]{0}[ffffffff]/{1}", (object) num2, (object) byStarLevel2.ItemCount);
        }
        else
        {
          this._canEnhance = true;
          this._labelCostItemCount.text = string.Format("[ffffffff]{0}/{1}", (object) num2, (object) byStarLevel2.ItemCount);
        }
        this._buttonEnhance.isEnabled = this._canEnhance;
      }
    }
  }

  private int CompareDragon(DragonInfo a, DragonInfo b)
  {
    return a.level.CompareTo(b.level);
  }

  protected void UpdateResetAllButtonState()
  {
    DragonData dragonData = PlayerData.inst.dragonData;
    if (dragonData == null)
      return;
    this._buttonResetAll.isEnabled = dragonData.HaveSkillGroupEnhanced();
  }

  public void UpdateSkillList()
  {
    DragonData dragonData = PlayerData.inst.dragonData;
    if (dragonData == null)
      return;
    this.DestoryAllSkillStarLevelUpItem();
    bool flag = false;
    using (List<ConfigDragonSkillGroupInfo>.Enumerator enumerator = (!(this._skillType == ConfigDragonSkillGroupInfo.LIGHT) ? ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDarkDragonSkillGroupInfo() : ConfigManager.inst.DB_ConfigDragonSkillGroup.GetLightDragonSkillGroupInfo()).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConfigDragonSkillGroupInfo current = enumerator.Current;
        DragonSkillEnhanceItem skillStarLevelUpItem = this.CreateSkillStarLevelUpItem();
        ConfigDragonSkillMainInfo levSkillByGrounpId = ConfigManager.inst.DB_ConfigDragonSkillMain.GetCurrentMinLevSkillByGrounpId(current.internalId);
        if (ConfigManager.inst.DB_ConfigDragonSkillGroup.CheckDragonSkillGroupIsUnLock(current.internalId))
        {
          levSkillByGrounpId = ConfigManager.inst.DB_ConfigDragonSkillMain.GetCurrentMaxLevSkillByGrounpId(current.internalId);
          if (this._currentSelectSkillId == 0)
            this._currentSelectSkillId = levSkillByGrounpId.internalId;
        }
        skillStarLevelUpItem.SetSkillId(levSkillByGrounpId.internalId);
        skillStarLevelUpItem.SetSelected(levSkillByGrounpId.internalId == this._currentSelectSkillId);
        skillStarLevelUpItem.OnClicked += new System.Action<DragonSkillEnhanceItem>(this.OnSkillLevelUpItemClicked);
        skillStarLevelUpItem.OnSkillIconClicked += new System.Action<DragonSkillEnhanceItem>(this.OnSkillIconClicked);
        skillStarLevelUpItem.OnSkillReseted += new System.Action<DragonSkillEnhanceItem>(this.OnSkillReseted);
        if (dragonData.GetSkillGroupStarLevel(current.internalId) > 0)
          flag = true;
      }
    }
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.movement = UIScrollView.Movement.Unrestricted;
    this._scrollView.ResetPosition();
    this._scrollView.currentMomentum = Vector3.zero;
    this._scrollView.movement = UIScrollView.Movement.Vertical;
    this.UpdateCurrentSelectSkill();
    this.UpdateResetAllButtonState();
  }

  protected DragonSkillEnhanceItem GetSkillStarLevelUpItemById(int skillId)
  {
    return this._allStarLevelUpItem.Find((Predicate<DragonSkillEnhanceItem>) (p => p.SkillId == skillId));
  }

  protected void DestoryAllSkillStarLevelUpItem()
  {
    using (List<DragonSkillEnhanceItem>.Enumerator enumerator = this._allStarLevelUpItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this._allStarLevelUpItem.Clear();
  }

  protected DragonSkillEnhanceItem CreateSkillStarLevelUpItem()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._skillStarLevelUpItemTempalte.gameObject);
    gameObject.SetActive(true);
    gameObject.transform.SetParent(this._table.transform);
    gameObject.transform.localScale = Vector3.one;
    DragonSkillEnhanceItem component = gameObject.GetComponent<DragonSkillEnhanceItem>();
    this._allStarLevelUpItem.Add(component);
    return component;
  }

  public void OnButtonEnhanceSkillClicked()
  {
    if (this._currentSelectSkillId == 0 || !this._canEnhance)
      return;
    Hashtable postData = new Hashtable()
    {
      {
        (object) "skill_id",
        (object) this._currentSelectSkillId
      }
    };
    RequestManager.inst.SendRequest("Dragon:upgradeSkillStar", postData, new System.Action<bool, object>(this.OnEnhanceSkillCallback), true);
  }

  protected void OnEnhanceSkillCallback(bool result, object data)
  {
    if (!result)
      return;
    DragonSkillEnhanceItem starLevelUpItemById = this.GetSkillStarLevelUpItemById(this._currentSelectSkillId);
    if ((bool) ((UnityEngine.Object) starLevelUpItemById))
    {
      starLevelUpItemById.SetSkillId(this._currentSelectSkillId);
      starLevelUpItemById.PlayEnhancedEffect();
    }
    this.UpdateCurrentSelectSkill();
    this.UpdateDragonDetail();
    this.UpdateResetAllButtonState();
  }

  public void OnButtonResetAllSkillsClicked()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_skill_enhance_reset_all");
    if (shopData != null)
      UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
      {
        itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopData.Item_InternalId),
        callback = new System.Action<bool, object>(this.OnResetAllSkillsCallback),
        titleText = Utils.XLAT("dragon_skill_enhance_reset_title"),
        btnText = Utils.XLAT("id_uppercase_confirm")
      });
    else
      D.error((object) "can not find shop item: shopitem_dragon_skill_enhance_reset_all");
  }

  protected void OnResetAllSkillsCallback(bool result, object data)
  {
    if (!result)
      return;
    this.UpdateAll();
  }

  private void OnDragEvent(GameObject go, Vector2 delta)
  {
    if ((UnityEngine.Object) go != (UnityEngine.Object) this._dragonTexture.gameObject)
      return;
    float num = 1f;
    this._dragonRoot.transform.Rotate(Vector3.up, -delta.x / num);
  }
}
