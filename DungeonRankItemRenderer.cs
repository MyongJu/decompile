﻿// Decompiled with JetBrains decompiler
// Type: DungeonRankItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungeonRankItemRenderer : MonoBehaviour
{
  public UITexture rankTexture;
  public UITexture playerTexture;
  public UILabel playerName;
  public UILabel playerDungeonLevel;
  public UISprite[] rankFrames;
  public UISprite dividingLine;
  private string _name;
  private string _acronym;
  private string _portrait;
  private string _icon;
  private int _level;
  private int _ranking;
  private int _lordTitleId;

  public void SetData(string name, string acronym, string portrait, string icon, int lordTitleId, int level, int ranking)
  {
    this._name = name;
    this._acronym = acronym;
    this._portrait = portrait;
    this._icon = icon;
    this._level = level;
    this._ranking = ranking;
    this._lordTitleId = lordTitleId;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.rankTexture, "Texture/LeaderboardIcons/icon_no" + (object) this._ranking, (System.Action<bool>) null, true, false, string.Empty);
    CustomIconLoader.Instance.requestCustomIcon(this.playerTexture, "Texture/Hero/Portrait_Icon/player_portrait_icon_" + (!string.IsNullOrEmpty(this._portrait) ? this._portrait : "0"), this._icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.playerTexture, this._lordTitleId, 2);
    this.playerName.text = string.Format(!string.IsNullOrEmpty(this._acronym) ? "({0}){1}" : "{0}{1}", (object) this._acronym, (object) this._name);
    this.playerDungeonLevel.text = Utils.XLAT("dragon_knight_floors_explored") + Utils.FormatThousands(this._level.ToString());
    NGUITools.SetActive(this.rankFrames[this._ranking - 1].gameObject, true);
    NGUITools.SetActive(this.dividingLine.gameObject, this._ranking != this.rankFrames.Length);
  }
}
