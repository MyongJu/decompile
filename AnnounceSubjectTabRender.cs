﻿// Decompiled with JetBrains decompiler
// Type: AnnounceSubjectTabRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnnounceSubjectTabRender : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelSelected;
  [SerializeField]
  private UILabel _labelUnselected;
  [SerializeField]
  private GameObject _objSelected;
  [SerializeField]
  private GameObject _objUnselected;
  private string _catalogName;
  private int _catalogIndex;
  private bool _defaultSelected;
  public System.Action<int> OnCurrentCatalogClicked;

  public void SetData(string name, int catalogIndex = -1, bool selected = false)
  {
    this._catalogName = name;
    this._catalogIndex = catalogIndex;
    this._defaultSelected = selected;
    this.UpdateUI();
  }

  public void OnCatalogClicked()
  {
    AnnounceSubjectTabRender[] componentsInChildren = this.transform.parent.GetComponentsInChildren<AnnounceSubjectTabRender>();
    if (componentsInChildren != null)
    {
      for (int index = 0; index < componentsInChildren.Length; ++index)
        componentsInChildren[index].ResetCatalog();
    }
    this.SetCatalog();
  }

  public void ResetCatalog()
  {
    NGUITools.SetActive(this._objSelected, false);
    NGUITools.SetActive(this._objUnselected, true);
  }

  private void SetCatalog()
  {
    NGUITools.SetActive(this._objSelected, true);
    NGUITools.SetActive(this._objUnselected, false);
    if (this.OnCurrentCatalogClicked == null)
      return;
    this.OnCurrentCatalogClicked(this._catalogIndex);
  }

  private void UpdateUI()
  {
    UILabel labelSelected = this._labelSelected;
    string catalogName = this._catalogName;
    this._labelUnselected.text = catalogName;
    string str = catalogName;
    labelSelected.text = str;
    if (this._defaultSelected)
      this.SetCatalog();
    else
      this.ResetCatalog();
  }
}
