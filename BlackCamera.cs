﻿// Decompiled with JetBrains decompiler
// Type: BlackCamera
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BlackCamera : MonoBehaviour
{
  public static GameObject Instance;

  private void Awake()
  {
    Object.DontDestroyOnLoad((Object) this.gameObject);
    BlackCamera.Instance = this.gameObject;
  }

  private void OnDestory()
  {
    BlackCamera.Instance = (GameObject) null;
  }
}
