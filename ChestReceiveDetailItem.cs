﻿// Decompiled with JetBrains decompiler
// Type: ChestReceiveDetailItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ChestReceiveDetailItem : MonoBehaviour
{
  [SerializeField]
  private UILabel _LabelPlayerName;
  [SerializeField]
  private GameObject _RootLuckiest;
  [SerializeField]
  private UILabel _LabelGetCount;
  [SerializeField]
  private Color _bgColorForOthers;
  [SerializeField]
  private Color _bgColorForSelf;
  [SerializeField]
  private UISprite[] _allSpriteBackground;

  public void SetData(long uid, string playerName, int receivedCount, bool isLuckiest)
  {
    this._LabelPlayerName.text = playerName;
    this._LabelGetCount.text = receivedCount.ToString();
    this._RootLuckiest.SetActive(isLuckiest);
    if (uid == PlayerData.inst.uid)
    {
      foreach (UIWidget uiWidget in this._allSpriteBackground)
        uiWidget.color = this._bgColorForSelf;
    }
    else
    {
      foreach (UIWidget uiWidget in this._allSpriteBackground)
        uiWidget.color = this._bgColorForOthers;
    }
  }
}
