﻿// Decompiled with JetBrains decompiler
// Type: PVPEncounterBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using UnityEngine;

public class PVPEncounterBanner : SpriteBanner
{
  public UISpriteMesh m_Circle;
  private TileType m_TileType;
  private long m_ResourceId;
  private int m_Level;
  private bool m_UpdateAnchor;

  public override void OnFinalize()
  {
    base.OnFinalize();
    this.m_TileType = TileType.None;
    this.m_ResourceId = 0L;
    this.m_Level = 0;
    this.m_UpdateAnchor = false;
  }

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    if (this.m_TileType == tile.TileType && this.m_ResourceId == (long) tile.ResourceId && this.m_Level == tile.Level)
      return;
    this.m_TileType = tile.TileType;
    this.m_ResourceId = (long) tile.ResourceId;
    this.m_Level = tile.Level;
    if (tile.TileType == TileType.Encounter)
    {
      WorldEncounterData data = ConfigManager.inst.DB_WorldEncount.GetData((long) tile.ResourceId);
      if (data != null)
      {
        this.Level = data.level;
        this.Name = ScriptLocalization.Get(data.Name, false);
      }
      else
        D.error((object) ("Bad world encounter data! " + tile.Location.ToString()));
    }
    else if (tile.TileType == TileType.EventVisitNPC)
    {
      FestivalVisitInfo data = ConfigManager.inst.DB_FestivalVisit.GetData(tile.ResourceId);
      if (data != null)
      {
        this.Level = 1.ToString();
        this.Name = data.LOC_Name;
      }
    }
    else if (tile.TileType == TileType.GveCamp)
    {
      this.Level = tile.Level.ToString();
      this.Name = ScriptLocalization.Get("gve_tile_name", true);
    }
    this.m_UpdateAnchor = true;
  }

  private void Update()
  {
    if (!this.m_UpdateAnchor)
      return;
    this.m_UpdateAnchor = false;
    if ((bool) ((Object) this.m_NameRenderer))
    {
      Bounds bounds = this.m_NameRenderer.bounds;
      Vector3 vector3 = this.m_NameLabel.transform.InverseTransformPoint(new Vector3(bounds.min.x, bounds.center.y, bounds.center.z));
      vector3.x -= 0.5f * (float) this.m_Circle.width;
      this.m_Circle.transform.localPosition = vector3;
    }
    if (this.m_TileType != TileType.GveCamp)
      return;
    Vector3 localPosition = this.transform.localPosition;
    localPosition.y -= 80f;
    this.transform.localPosition = localPosition;
  }
}
