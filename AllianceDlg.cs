﻿// Decompiled with JetBrains decompiler
// Type: AllianceDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceDlg : UI.Dialog
{
  public AllianceOverviewPage mOverviewRoot;
  public AllianceMemberPage mMemberRoot;
  public AllianceManagePage mManageRoot;
  public UIToggle[] mTabs;
  public GameObject m_Badge;
  private int m_SelectedIndex;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    AllianceDlg.Parameter parameter = orgParam as AllianceDlg.Parameter;
    this.mOverviewRoot.HidePage();
    this.mMemberRoot.HidePage();
    this.mManageRoot.HidePage();
    this.m_Badge.SetActive(false);
    this.m_SelectedIndex = -1;
    this.mTabs[parameter == null ? 0 : parameter.selectedIndex].Set(true);
    this.OnTabSelected();
    this.RegisterListener();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.mOverviewRoot.HidePage();
    this.mMemberRoot.HidePage();
    this.mManageRoot.HidePage();
    this.UnregisterListener();
  }

  private void RegisterListener()
  {
    AllianceManager.Instance.onPushJoin += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushLeave += new System.Action<object>(this.OnServerPushLeave);
    AllianceManager.Instance.onPushDisban += new System.Action<object>(this.OnServerDisband);
    AllianceManager.Instance.onPushSetRank += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushModify += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushTransferHighlord += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushLevelUpgrade += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpRequest += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpResponse += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpComplete += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushEmpowerTempHighlord += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushRevokeTempHighlord += new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushTempHighlordTimeup += new System.Action<object>(this.OnServerPushUpdate);
    DBManager.inst.DB_March.onDataCreate += new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_March.onDataRemoved += new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_March.onDataUpdate += new System.Action<long>(this.OnWarUpdated);
    DBManager.inst.DB_Rally.onRallyDataCreated += new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_Rally.onRallyDataRemoved += new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_AllianceInviteApply.onDataChanged += new System.Action<long, long>(this.OnInviteApplyChanged);
    DBManager.inst.DB_AllianceInviteApply.onDataRemoved += new System.Action(this.UpdateBadge);
  }

  private void UnregisterListener()
  {
    AllianceManager.Instance.onPushJoin -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushLeave -= new System.Action<object>(this.OnServerPushLeave);
    AllianceManager.Instance.onPushDisban -= new System.Action<object>(this.OnServerDisband);
    AllianceManager.Instance.onPushSetRank -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushModify -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushTransferHighlord -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushLevelUpgrade -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpRequest -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpResponse -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushHelpComplete -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushEmpowerTempHighlord -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushRevokeTempHighlord -= new System.Action<object>(this.OnServerPushUpdate);
    AllianceManager.Instance.onPushTempHighlordTimeup -= new System.Action<object>(this.OnServerPushUpdate);
    DBManager.inst.DB_March.onDataCreate -= new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_March.onDataRemoved -= new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_March.onDataUpdate -= new System.Action<long>(this.OnWarUpdated);
    DBManager.inst.DB_Rally.onRallyDataCreated -= new System.Action<long>(this.OnWarCreate);
    DBManager.inst.DB_Rally.onRallyDataRemoved -= new System.Action(this.OnWarRemoved);
    DBManager.inst.DB_AllianceInviteApply.onDataChanged -= new System.Action<long, long>(this.OnInviteApplyChanged);
    DBManager.inst.DB_AllianceInviteApply.onDataRemoved -= new System.Action(this.UpdateBadge);
  }

  private void OnWarCreate(long id)
  {
    this.RefreshInfo();
  }

  private void OnWarUpdated(long id)
  {
    this.RefreshInfo();
  }

  private void OnWarRemoved()
  {
    this.RefreshInfo();
  }

  private void OnInviteApplyChanged(long uid, long allianceId)
  {
    if (allianceId != PlayerData.inst.allianceId)
      return;
    this.UpdateBadge();
  }

  private void OnServerPushUpdate(object data)
  {
    this.RefreshInfo();
  }

  private void UpdateBadge()
  {
    AllianceMemberData allianceMemberData = PlayerData.inst.allianceData.members.Get(PlayerData.inst.uid);
    if (allianceMemberData != null && allianceMemberData.title >= 4)
      this.m_Badge.SetActive(DBManager.inst.DB_AllianceInviteApply.IsApplied(PlayerData.inst.allianceId));
    else
      this.m_Badge.SetActive(false);
  }

  private void OnServerPushLeave(object data)
  {
    if (!(bool) ((UnityEngine.Object) this.gameObject))
      return;
    if (PlayerData.inst.allianceId == 0L)
      this.OnDlgClosePressed();
    else
      this.RefreshInfo();
  }

  private void OnServerDisband(object data)
  {
    this.OnDlgClosePressed();
  }

  private int GetSelectedIndex()
  {
    for (int index = 0; index < this.mTabs.Length; ++index)
    {
      if (this.mTabs[index].value)
        return index;
    }
    return -1;
  }

  public void OnTabSelected()
  {
    int selectedIndex = this.GetSelectedIndex();
    if (selectedIndex == this.m_SelectedIndex)
      return;
    switch (selectedIndex)
    {
      case 0:
        this.mOverviewRoot.ShowPage(new System.Action(this.UpdateBadge));
        this.mMemberRoot.HidePage();
        this.mManageRoot.HidePage();
        break;
      case 1:
        this.mOverviewRoot.HidePage();
        this.mMemberRoot.ShowPage();
        this.mManageRoot.HidePage();
        break;
      case 2:
        this.mOverviewRoot.HidePage();
        this.mMemberRoot.HidePage();
        this.mManageRoot.ShowPage();
        break;
    }
    this.m_SelectedIndex = selectedIndex;
  }

  public void OnDlgClosePressed()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnDisbandBtnPressed()
  {
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      title = ScriptLocalization.Get("alliance_disband_alliance_title", true),
      content = ScriptLocalization.Get("alliance_disband_alliance_description", true),
      yes = ScriptLocalization.Get("id_uppercase_yes", true),
      no = ScriptLocalization.Get("id_uppercase_no", true),
      yesCallback = new System.Action(this.OnDisbanConfirm),
      noCallback = (System.Action) null
    });
  }

  private void OnDisbanConfirm()
  {
    string allianceName = PlayerData.inst.allianceData.allianceName;
    AllianceManager.Instance.DisbanAlliance(PlayerData.inst.allianceId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["Alliance_Name"] = allianceName;
      ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_disband"), para));
      this.OnDlgClosePressed();
    }));
  }

  public void OnDonationPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceTechDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnCityPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceCityDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnMainShopBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceStore", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnMailPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param0, _param1) =>
    {
      List<string> recipients = new List<string>();
      Dictionary<long, AllianceMemberData>.Enumerator enumerator = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId).members.datas.GetEnumerator();
      while (enumerator.MoveNext())
      {
        UserData userData = DBManager.inst.DB_User.Get(enumerator.Current.Value.uid);
        if (userData.uid != PlayerData.inst.uid && enumerator.Current.Value.status != -1)
          recipients.Add(userData.userName);
      }
      PlayerData.inst.mail.SendAllianceMail(PlayerData.inst.allianceId, recipients);
    }), true);
  }

  public void OnBoardPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceBoardDialog", (UI.Dialog.DialogParameter) new AllianceBoardDialog.Parameter()
    {
      allianceData = PlayerData.inst.allianceData
    }, true, true, true);
  }

  public void OnMainHelpBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceHelpRequestsDlg", (UI.Dialog.DialogParameter) new AllianceHelpRequestDlg.Parameter()
    {
      showBack = true
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnMainReinforcementsBtnPressed()
  {
    UIManager.inst.OpenDlg("EmbassyDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnMainWarRallyBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceWarMainDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnSummaryJoinAllianceBtnPressed()
  {
    this.OnComingSoon();
  }

  public void OnViewAlliancePressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceJoinAndApplyDialog", (UI.Dialog.DialogParameter) new AllianceJoinAndApplyDialog.Parameter()
    {
      allianceData = PlayerData.inst.allianceData,
      justView = true
    }, true, true, true);
  }

  private void RefreshInfo()
  {
    try
    {
      this.mOverviewRoot.UpdatePage();
      this.mManageRoot.UpdatePage();
    }
    catch
    {
    }
  }

  public void OnComingSoon()
  {
    Utils.PopUpCommingSoon();
  }

  public void OnInviteBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceInviteDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnLeaveAllianceBtnPressed()
  {
    if (AllianceWarPayload.Instance.IsAllianceCurrentJoined(0L))
      AllianceWarPayload.Instance.ShowAllianceQuitConfirm(1, (System.Action) null, (System.Action) (() => this.LeaveAlliance()));
    else
      this.LeaveAlliance();
  }

  private void LeaveAlliance()
  {
    AllianceData alliance = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    if (alliance.creatorId == PlayerData.inst.uid)
    {
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_leader_cannot_leave", true), (System.Action) null, 4f, false);
    }
    else
    {
      LeaveAllianceDlg dlg = NGUITools.AddChild(GameObject.Find("/UIManager/UI Root"), AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/Alliance/AllianceLeaveDlg", (System.Type) null) as GameObject).GetComponent<LeaveAllianceDlg>();
      dlg.Show(alliance, (System.Action<bool>) (leftAlliance =>
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) dlg.gameObject);
        if (!leftAlliance)
          return;
        this.OnDlgClosePressed();
      }));
    }
  }

  public void OnBlockListBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceBlockingDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnEditNameBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceCustomizeDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnViewRanksBtnPressed()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceRankPrivilegePopup", (Popup.PopupParameter) null);
  }

  public void OnDiplomacyBtnPressed()
  {
    UIManager.inst.OpenDlg("Alliance/AllianceDiplomacyDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnTransferLeadershipBtnPressed()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "alliance_id"] = (object) PlayerData.inst.allianceId;
    MessageHub.inst.GetPortByAction("Alliance:getAllianceMemberInfo").SendRequest(postData, (System.Action<bool, object>) ((_param0, _param1) => UIManager.inst.OpenDlg("Alliance/AllianceTransferLeadershipDlg", (UI.Dialog.DialogParameter) null, true, true, true)), true);
  }

  public void OnDonateRanking()
  {
    UIManager.inst.OpenPopup("AllianceTech/AllianceTechDonateRankPop", (Popup.PopupParameter) null);
  }

  public void OnUsurpBtnPressed()
  {
    Utils.SetPriceToLabel(((AllianceUsurpPopup) UIManager.inst.OpenPopup("Alliance/AllianceUsurpPopup", (Popup.PopupParameter) new MessageBoxWith2Buttons.Parameter()
    {
      yesCallback = new System.Action(this.OnUsurpConfirm),
      noCallback = (System.Action) null
    })).goldLabel, 1000);
  }

  private void OnUsurpConfirm()
  {
    if (PlayerData.inst.hostPlayer.Currency < 1000)
      Utils.ShowNotEnoughGoldTip();
    else
      MessageHub.inst.GetPortByAction("Alliance:applyForHighlord").SendRequest(new Hashtable(), (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        ChatMessageManager.SendUsurpAllianceMessage();
        this.OnTabSelected();
      }), true);
  }

  public void OnButtonAllianceChestClicked()
  {
    UIManager.inst.OpenDlg("AllianceChest/AllianceChestDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  public void OnViewLogBtnPressed()
  {
    UIManager.inst.OpenPopup("Alliance/AllianceLogPopup", (Popup.PopupParameter) null);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int selectedIndex;
  }
}
