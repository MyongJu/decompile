﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentSkillSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DragonKnightTalentSkillSlot : MonoBehaviour
{
  private bool m_LastCanUse = true;
  private bool m_Enable = true;
  private const string VFX_PATH = "Prefab/DragonKnight/Objects/VFX/fx_cool_down";
  public UITexture m_ItemIcon;
  public UISprite m_Lock;
  public UISprite m_Highlight;
  public UIToggle m_Toggle;
  public UILabel m_CoolDown;
  public SpriteRenderer m_Mask;
  public System.Action OnChanged;
  private DragonKnightTalentInfo m_TalentInfo;
  private int m_CDTime;
  private bool m_Unlock;
  private bool m_Active;
  private TweenAlpha m_TweenAlpha;

  private void Start()
  {
    this.Unlock(this.m_Unlock);
    this.m_Toggle.Set(false);
    this.RemoveTweenAlpha();
  }

  public DragonKnightTalentInfo TalentInfo
  {
    get
    {
      return this.m_TalentInfo;
    }
  }

  public void SetEnable(bool enable)
  {
    this.m_Enable = enable;
  }

  public void SetData(int talentSkillId)
  {
    this.m_TalentInfo = ConfigManager.inst.DB_DragonKnightTalent.GetDragonKnightTalentInfo(talentSkillId);
    if (this.m_TalentInfo != null)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_TalentInfo.imagePath, (System.Action<bool>) null, false, true, string.Empty);
    else
      BuilderFactory.Instance.Release((UIWidget) this.m_ItemIcon);
    this.m_Toggle.enabled = !this.IsEmpty;
    this.m_Mask.material.SetFloat("_Percentage", 0.0f);
    this.m_CDTime = 0;
    this.m_CoolDown.text = string.Empty;
    this.m_LastCanUse = true;
  }

  public void Blink()
  {
    this.m_TweenAlpha = UITweener.Begin<TweenAlpha>(this.m_Highlight.gameObject, 0.5f);
    this.m_TweenAlpha.from = 0.0f;
    this.m_TweenAlpha.to = 1f;
    this.m_TweenAlpha.duration = 0.5f;
    this.m_TweenAlpha.style = UITweener.Style.PingPong;
  }

  public void Unblink()
  {
    this.RemoveTweenAlpha();
  }

  public bool IsEmpty
  {
    get
    {
      return this.m_TalentInfo == null;
    }
  }

  public bool IsActive
  {
    get
    {
      if (!this.IsEmpty)
        return this.m_Toggle.value;
      return false;
    }
  }

  public void Unlock(bool unlock)
  {
    this.m_Unlock = unlock;
    this.m_Toggle.enabled = !this.IsEmpty && unlock;
    this.m_Lock.gameObject.SetActive(!unlock);
  }

  public void Trigger()
  {
    this.m_CDTime = this.m_TalentInfo.cdTime;
    this.m_Toggle.Set(false);
    this.RemoveTweenAlpha();
  }

  public bool IsNotCooling
  {
    get
    {
      bool flag1 = this.m_CDTime <= 0;
      bool flag2 = this.m_TalentInfo.cdTime == 0;
      if (!flag1)
        return flag2;
      return true;
    }
  }

  public void OnValueChanged()
  {
    if (this.OnChanged == null)
      return;
    this.OnChanged();
  }

  public void OnClick()
  {
    Room currentRoom = DragonKnightSystem.Instance.RoomManager.CurrentRoom;
    if (currentRoom != null && (currentRoom.ContainsBoss() || currentRoom.ContainsMonsters() || (currentRoom.ContainsPlayer() || this.m_TalentInfo == null)))
      return;
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightTalentSkillDetailPopup", (Popup.PopupParameter) new DragonKnightTalentSkillDetailPopup.Parameter()
    {
      talentInfo = this.m_TalentInfo
    });
  }

  public void DecreaseCDTime()
  {
    --this.m_CDTime;
  }

  private void Update()
  {
    if (!this.m_Enable)
    {
      this.m_Toggle.enabled = false;
      this.m_Toggle.Set(false);
      this.RemoveTweenAlpha();
    }
    else
    {
      RoundPlayer myPlayer = BattleManager.Instance.GetMyPlayer();
      if (myPlayer == null || this.m_TalentInfo == null)
        return;
      bool flag1 = this.m_CDTime <= 0;
      bool flag2 = this.m_TalentInfo.cdTime == 0;
      bool flag3 = myPlayer.Mana >= this.m_TalentInfo.manaCost;
      bool flag4 = flag1 && flag3;
      this.m_Toggle.enabled = flag4 && this.m_Unlock;
      this.m_CoolDown.gameObject.SetActive(flag3);
      this.m_CoolDown.text = flag1 || flag2 ? string.Empty : this.m_CDTime.ToString();
      float num = (float) this.m_CDTime / (float) this.m_TalentInfo.cdTime;
      this.m_Mask.gameObject.SetActive(!flag2);
      this.m_Mask.material.SetFloat("_Percentage", Mathf.Clamp01(num));
      if (!this.m_LastCanUse && flag4 && this.m_Toggle.enabled)
        VfxManager.Instance.CreateAndPlay("Prefab/DragonKnight/Objects/VFX/fx_cool_down", this.m_ItemIcon.transform);
      this.m_LastCanUse = flag4;
    }
  }

  private void RemoveTweenAlpha()
  {
    if (!(bool) ((UnityEngine.Object) this.m_TweenAlpha))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_TweenAlpha);
    this.m_TweenAlpha = (TweenAlpha) null;
    this.m_Highlight.alpha = 0.0f;
  }
}
