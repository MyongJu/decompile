﻿// Decompiled with JetBrains decompiler
// Type: TreasuryInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TreasuryInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "duration")]
  public float duration;
  [Config(Name = "gold_min")]
  public long minGoldCount;
  [Config(Name = "gold_max")]
  public long maxGoldCount;
  [Config(Name = "interests")]
  public float interests;
  [Config(Name = "unlock_type")]
  public int unlockType;
}
