﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryIapSlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelBtnPropsCount;
  [SerializeField]
  private UILabel _labelBtnCash;
  [SerializeField]
  private UITexture _textureBtnProps;
  [SerializeField]
  private UITexture _textureIapIcon;
  [SerializeField]
  private UILabel _labelIapPackageName;
  [SerializeField]
  private UILabel _labelIapPackageRemained;
  [SerializeField]
  private UILabel _labelIapDiscount;
  private AnniversaryIapMainInfo _iapMainInfo;
  private Color _propsCountOriginColor;

  public int LeftCount
  {
    get
    {
      if (this._iapMainInfo != null)
        return AnniversaryIapPayload.Instance.GetIapPackageRemained(this._iapMainInfo.internalId);
      return 0;
    }
  }

  public void SetData(AnniversaryIapMainInfo iapMainInfo)
  {
    this._iapMainInfo = iapMainInfo;
    this._propsCountOriginColor = this._labelBtnPropsCount.color;
    AnniversaryIapPayload.Instance.OnAnnvIapBuySuccess -= new System.Action<bool>(this.OnAnnvIapBuySuccess);
    AnniversaryIapPayload.Instance.OnAnnvIapBuySuccess += new System.Action<bool>(this.OnAnnvIapBuySuccess);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdated);
    this.UpdateUI();
  }

  public void ClearData()
  {
    this._iapMainInfo = (AnniversaryIapMainInfo) null;
    AnniversaryIapPayload.Instance.OnAnnvIapBuySuccess -= new System.Action<bool>(this.OnAnnvIapBuySuccess);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
  }

  public void OnPropsBuyClicked()
  {
    if (this._iapMainInfo == null)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    if (AnniversaryIapPayload.Instance.PropsAmount < this._iapMainInfo.needPropsCount)
    {
      IAPPackageInfo iapPackageInfo = ConfigManager.inst.DB_IAPPackage.Get(this._iapMainInfo.iapPackageId);
      if (iapPackageInfo == null)
        return;
      para.Add("0", PaymentManager.Instance.GetFormattedPrice(iapPackageInfo.productId, iapPackageInfo.pay_id));
      UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
      {
        title = Utils.XLAT("id_uppercase_insufficient_items"),
        content = ScriptLocalization.GetWithPara("iap_package_1_year_celebration_not_enough_tokens_notice", para, true),
        confirmButtonText = Utils.XLAT("id_uppercase_okay"),
        onOkayCallback = (System.Action) (() => this.BuyIapByCash())
      });
    }
    else
    {
      para.Add("0", this._iapMainInfo.needPropsCount.ToString());
      UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
      {
        title = Utils.XLAT("iap_anniversary_trade_in_button"),
        content = ScriptLocalization.GetWithPara("iap_package_1_year_celebration_spend_tokens_notice", para, true),
        confirmButtonText = Utils.XLAT("id_uppercase_okay"),
        onOkayCallback = (System.Action) (() => AnniversaryIapPayload.Instance.AnnvExchangePackage(this._iapMainInfo.internalId, (System.Action<bool, object>) ((ret, data) =>
        {
          if (!ret)
            return;
          UIManager.inst.toast.Show(Utils.XLAT("mail_title_anniversary_iap_exchange"), (System.Action) null, 4f, true);
          this.OnAnnvIapBuySuccess(true);
        })))
      });
    }
  }

  public void OnDetailCashBuyClicked()
  {
    UIManager.inst.OpenPopup("Anniversary/AnniversaryIapBuyPopup", (Popup.PopupParameter) new AnniversaryIapPopup.Parameter()
    {
      annvIapMainInfo = this._iapMainInfo,
      OnPurchaseEnd = (System.Action<int>) (iapId => this.RefreshUI())
    });
  }

  public void OnCashBuyClicked()
  {
    this.BuyIapByCash();
  }

  private void BuyIapByCash()
  {
    if (this._iapMainInfo == null)
      return;
    IAPStorePackagePayload.Instance.BuySpecialProduct(this._iapMainInfo.internalId, this._iapMainInfo.IapPackageInfo.productId, this._iapMainInfo.name, this._iapMainInfo.IapGroupName, (System.Action) (() => this.RefreshUI()));
  }

  private void RefreshUI()
  {
    AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.OnAnnvIapBuySuccess(true);
    }));
  }

  private void OnAnnvIapBuySuccess(bool success)
  {
    if (this._iapMainInfo == null)
      return;
    if (AnniversaryIapPayload.Instance.GetIapPackageRemained(this._iapMainInfo.internalId) == 0)
    {
      NGUITools.SetActive(this.gameObject, false);
      UITable componentInParent = this.transform.GetComponentInParent<UITable>();
      if (!(bool) ((UnityEngine.Object) componentInParent))
        return;
      componentInParent.repositionNow = true;
      componentInParent.Reposition();
    }
    else
      this.UpdateUI();
  }

  private void OnItemDataUpdated(int itemId)
  {
    if (itemId != AnniversaryIapPayload.Instance.NeedPropsId)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this._iapMainInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureBtnProps, AnniversaryIapPayload.Instance.PropsImagePath, (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureIapIcon, this._iapMainInfo.IconPath, (System.Action<bool>) null, true, true, string.Empty);
    this._labelBtnPropsCount.color = AnniversaryIapPayload.Instance.PropsAmount >= this._iapMainInfo.needPropsCount ? this._propsCountOriginColor : Color.red;
    this._labelBtnPropsCount.text = this._iapMainInfo.needPropsCount.ToString();
    this._labelIapPackageName.text = this._iapMainInfo.Name;
    int iapPackageRemained = AnniversaryIapPayload.Instance.GetIapPackageRemained(this._iapMainInfo.internalId);
    if (iapPackageRemained > 0)
      this._labelIapPackageRemained.text = Utils.XLAT("iap_package_1_year_celebration_packages_left_name") + iapPackageRemained.ToString();
    else if (iapPackageRemained == -1)
      this._labelIapPackageRemained.text = Utils.XLAT("iap_package_1_year_celebration_packages_left_name") + Utils.XLAT("id_infinity_symbol");
    IAPPackageInfo iapPackageInfo = ConfigManager.inst.DB_IAPPackage.Get(this._iapMainInfo.iapPackageId);
    if (iapPackageInfo != null)
      this._labelBtnCash.text = PaymentManager.Instance.GetFormattedPrice(iapPackageInfo.productId, iapPackageInfo.pay_id);
    this._labelIapDiscount.text = Utils.XLAT("id_uppercase_hot");
    NGUITools.SetActive(this._labelIapDiscount.transform.parent.gameObject, this._iapMainInfo.IsHot);
  }
}
