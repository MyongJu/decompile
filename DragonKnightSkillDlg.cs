﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class DragonKnightSkillDlg : UI.Dialog
{
  private Dictionary<int, DragonKnightSkillStageRenderer> itemDict = new Dictionary<int, DragonKnightSkillStageRenderer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UIScrollView scrollView;
  public UIGrid grid;
  public GameObject itemPrefab;
  public GameObject itemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowDragonKnightSkill();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void ShowDragonKnightSkill()
  {
    this.ClearData();
    Dictionary<int, List<DragonKnightSkillInfo>> knightAllStageInfo = ConfigManager.inst.DB_DragonKnightSkill.GetDragonKnightAllStageInfo();
    if (knightAllStageInfo != null && knightAllStageInfo.Count > 0)
    {
      bool flag = false;
      using (Dictionary<int, List<DragonKnightSkillInfo>>.Enumerator enumerator = knightAllStageInfo.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, List<DragonKnightSkillInfo>> current = enumerator.Current;
          if (!flag)
          {
            DragonKnightSkillStageRenderer itemRenderer = this.CreateItemRenderer(current.Value);
            this.itemDict.Add(current.Key, itemRenderer);
            flag = ConfigManager.inst.DB_DragonKnightSkill.IsStageSkillsLocked(current.Key);
          }
          else
            break;
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, DragonKnightSkillStageRenderer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, DragonKnightSkillStageRenderer> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.grid.repositionNow = true;
    this.grid.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private DragonKnightSkillStageRenderer CreateItemRenderer(List<DragonKnightSkillInfo> stageInfo)
  {
    GameObject gameObject = this.itemPool.AddChild(this.grid.gameObject);
    gameObject.SetActive(true);
    DragonKnightSkillStageRenderer component = gameObject.GetComponent<DragonKnightSkillStageRenderer>();
    component.SetData(stageInfo);
    return component;
  }
}
