﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryChampionMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AnniversaryChampionMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(CustomParse = true, CustomType = typeof (UnitGroup), Name = "Units")]
  public UnitGroup Units;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits Benefit;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "level")]
  public int Level;
  [Config(Name = "champion_reward_id")]
  public int RewardId;
  [Config(Name = "anniversary_worshiped_reward_id")]
  public int AnnvWorshipedRewardId;
  [Config(Name = "anniversary_worship_reward_id")]
  public int AnnvWorshipRewardId;
  [Config(Name = "worship_reward_id")]
  public int WorshipRewardId;

  public string LocalName
  {
    get
    {
      return ScriptLocalization.Get(this.Name, true);
    }
  }
}
