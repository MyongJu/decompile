﻿// Decompiled with JetBrains decompiler
// Type: PresureTest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class PresureTest : MonoBehaviour
{
  private const string boardpath = "Prefab/OpeningCinematic/PresureBoard";
  private const string citypath = "Prefab/OpeningCinematic/PresureCity";
  private static PresureTest _instance;
  public PresureController board;

  public static PresureTest Instance
  {
    get
    {
      if ((UnityEngine.Object) PresureTest._instance == (UnityEngine.Object) null)
      {
        PresureTest._instance = UnityEngine.Object.FindObjectOfType<PresureTest>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) PresureTest._instance)
          throw new ArgumentException("PresureTest hasn't been created yet.");
      }
      return PresureTest._instance;
    }
  }

  public void Startup()
  {
    this.PlayOpeningCinematic();
  }

  public void PlayOpeningCinematic()
  {
    AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/PresureBoard", (System.Action<UnityEngine.Object, bool>) ((obj, ret) =>
    {
      GameObject go = UnityEngine.Object.Instantiate(obj) as GameObject;
      AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/PresureBoard", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      this.board = go.GetComponent<PresureController>();
      AssetManager.Instance.LoadAsync("Prefab/OpeningCinematic/PresureCity", (System.Action<UnityEngine.Object, bool>) ((objcity, retcity) =>
      {
        Utils.DuplicateGOB(objcity as GameObject, go.transform).transform.localScale = Vector3.one;
        UIManager.inst.splashScreen.gameObject.SetActive(false);
        this.board.PlayAction(1);
        AssetManager.Instance.UnLoadAsset("Prefab/OpeningCinematic/PresureCity", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }), (System.Type) null);
    }), (System.Type) null);
  }

  public void AddTroops1()
  {
    this.board.GenerateOneTroop(true, true);
  }

  public void AddTroops2()
  {
    for (int index = 0; index < 10; ++index)
      this.board.GenerateOneTroop(true, true);
  }

  public void AddTroops3()
  {
    this.board.GenerateOneTroop(false, true);
  }

  public void AddTroops4()
  {
    for (int index = 0; index < 10; ++index)
      this.board.GenerateOneTroop(false, true);
  }

  public void AddTroops5()
  {
    this.board.GenerateOneTroop(true, false);
  }

  public void AddTroops6()
  {
    for (int index = 0; index < 10; ++index)
      this.board.GenerateOneTroop(true, false);
  }
}
