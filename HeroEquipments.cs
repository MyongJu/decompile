﻿// Decompiled with JetBrains decompiler
// Type: HeroEquipments
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HeroEquipments : MonoBehaviour
{
  private List<Renderer> _baseSkinRenderer = new List<Renderer>();
  private Color _currentSkin = Color.black;
  private Dictionary<int, EquipmentModel> _equipmentInst = new Dictionary<int, EquipmentModel>();
  public const string SkinProperty = "_SkinDye";
  public const string PrimaryDyeProperty = "_PrimaryDye";
  public bool female;
  public Transform rootBone;
  public Transform leftHand;
  public Transform rightHand;
  public GameObject[] baseChest;
  public GameObject[] baseLeg;
  public GameObject[] baseHelm;
  public GameObject[] baseGloves;
  public GameObject[] baseHair;
  public GameObject[] baseHead;
  public GameObject[] baseRightWeaponPrefab;
  public GameObject[] baseLeftWeaponPrefab;
  private GameObject[] _baseWeapons;
  private bool _customizeMode;
  private Animator _animator;
  private HeroData _heroData;

  public Animator Animator
  {
    get
    {
      return this._animator;
    }
  }

  public bool CustomizeMode
  {
    get
    {
      return this._customizeMode;
    }
    set
    {
    }
  }

  private void Awake()
  {
    if (GameEngine.IsAvailable)
    {
      GameEngine instance = GameEngine.Instance;
      if ((Object) instance != (Object) null && instance.PlayerData != null)
        this._heroData = instance.PlayerData.CurrentHero;
    }
    this._animator = this.GetComponent<Animator>();
    this.InitBaseEquipmentModel();
  }

  private void OnEnable()
  {
    if (this._heroData == null)
      return;
    this.SyncData();
  }

  private void OnDisable()
  {
    if (this._heroData != null)
      ;
  }

  private void SyncData()
  {
    if (this._heroData == null)
      ;
  }

  public void InitBaseEquipmentModel()
  {
    int length = 0;
    for (int index = 0; index < this.baseRightWeaponPrefab.Length; ++index)
    {
      if ((Object) this.baseRightWeaponPrefab[index] != (Object) null)
        ++length;
    }
    for (int index = 0; index < this.baseLeftWeaponPrefab.Length; ++index)
    {
      if ((Object) this.baseLeftWeaponPrefab[index] != (Object) null)
        ++length;
    }
    if (length <= 0)
      return;
    this._baseWeapons = new GameObject[length];
    int index1 = 0;
    Transform transform1 = !(bool) ((Object) this.rightHand) ? this.transform : this.rightHand;
    for (int index2 = 0; index2 < this.baseRightWeaponPrefab.Length; ++index2)
    {
      if (!((Object) this.baseRightWeaponPrefab[index2] == (Object) null))
      {
        GameObject gameObject = Object.Instantiate<GameObject>(this.baseRightWeaponPrefab[index2]);
        if ((bool) ((Object) gameObject))
        {
          gameObject.transform.parent = transform1;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.transform.localRotation = Quaternion.identity;
          gameObject.transform.localScale = Vector3.one;
          this._baseWeapons[index1] = gameObject;
        }
        ++index1;
      }
    }
    Transform transform2 = !(bool) ((Object) this.leftHand) ? this.transform : this.leftHand;
    for (int index2 = 0; index2 < this.baseLeftWeaponPrefab.Length; ++index2)
    {
      if (!((Object) this.baseLeftWeaponPrefab[index2] == (Object) null))
      {
        GameObject gameObject = Object.Instantiate<GameObject>(this.baseLeftWeaponPrefab[index2]);
        if ((bool) ((Object) gameObject))
        {
          gameObject.transform.parent = transform2;
          gameObject.transform.localPosition = Vector3.zero;
          gameObject.transform.localRotation = Quaternion.identity;
          gameObject.transform.localScale = Vector3.one;
          this._baseWeapons[index1] = gameObject;
        }
        ++index1;
      }
    }
  }

  public Color GetSkinDye()
  {
    return this._currentSkin;
  }

  public void SetSkin(Color skin)
  {
    if (skin == this._currentSkin)
      return;
    this._currentSkin = skin;
    int count = this._baseSkinRenderer.Count;
    for (int index = 0; index < count; ++index)
    {
      Renderer renderer = this._baseSkinRenderer[index];
      if ((bool) ((Object) renderer))
        renderer.material.SetColor("_SkinDye", skin);
    }
  }

  private void VarifyRendererProperty(GameObject[] inBase, List<Renderer> ioList, string inProperty)
  {
    if (inBase == null)
      return;
    for (int index = 0; index < inBase.Length; ++index)
    {
      Renderer component = inBase[index].GetComponent<Renderer>();
      if ((bool) ((Object) component) && (bool) ((Object) component.sharedMaterial) && component.sharedMaterial.HasProperty(inProperty))
        ioList.Add(component);
    }
  }

  private void OnDestroy()
  {
  }

  private EquipmentModel GetEquipmentInst(int inID)
  {
    if (inID == Hero_EquipmentData.InvalidID)
      return (EquipmentModel) null;
    return (EquipmentModel) null;
  }
}
