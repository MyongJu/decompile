﻿// Decompiled with JetBrains decompiler
// Type: AllianceCandidateRankHeader
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class AllianceCandidateRankHeader : MonoBehaviour, IAllianceRankHeader, IOnlineUpdateable
{
  private Dictionary<long, IMemberItemRenderer> m_ItemDict = new Dictionary<long, IMemberItemRenderer>();
  public UILabel m_RankName;
  public UILabel m_RankLevel;
  public UILabel m_Members;
  public UISprite m_Collapsed;
  public UISprite m_Expanded;
  public UIGrid m_Grid;
  public GameObject m_ItemPrefab;
  private AllianceData m_AllianceData;
  private bool m_IsExpanded;
  private System.Action m_OnChanged;
  private bool m_Initialized;

  public void SetData(AllianceData allianceData, int title, bool expand, System.Action onChanged)
  {
    this.m_AllianceData = allianceData;
    this.m_IsExpanded = expand;
    this.m_OnChanged = onChanged;
    this.RegisterListeners();
    this.UpdateUI();
  }

  public void SetNominate(bool nominate)
  {
  }

  public void OnHeaderClick()
  {
    this.m_IsExpanded = !this.m_IsExpanded;
    this.UpdateUI();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  public void UpdateOnlineStatus(List<long> status)
  {
    Dictionary<long, IMemberItemRenderer>.Enumerator enumerator = this.m_ItemDict.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.UpdateOnlineStatus(status);
  }

  private void UpdateUI()
  {
    this.InitOnce();
    this.m_Grid.gameObject.SetActive(this.m_IsExpanded);
    this.m_Grid.Reposition();
    this.m_RankName.text = this.m_AllianceData.GetTitleName(0);
    this.m_RankLevel.text = AllianceTitle.TitleToRank(0);
    this.m_Collapsed.gameObject.SetActive(!this.m_IsExpanded);
    this.m_Expanded.gameObject.SetActive(this.m_IsExpanded);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void ClearList()
  {
    using (Dictionary<long, IMemberItemRenderer>.ValueCollection.Enumerator enumerator = this.m_ItemDict.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IMemberItemRenderer current = enumerator.Current;
        current.gameObject.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemDict.Clear();
  }

  private void InitOnce()
  {
    if (this.m_Initialized)
      return;
    this.m_Initialized = true;
    this.ClearList();
    List<AllianceInvitedApplyData> dataByAllianceId = DBManager.inst.DB_AllianceInviteApply.GetDataByAllianceId(this.m_AllianceData.allianceId);
    for (int index = 0; index < dataByAllianceId.Count; ++index)
    {
      AllianceInvitedApplyData invitedApplyData = dataByAllianceId[index];
      if (invitedApplyData.status == AllianceInvitedApplyData.Status.asking)
        this.AddItem(DBManager.inst.DB_User.Get(invitedApplyData.uid));
    }
  }

  private void AddItem(UserData user)
  {
    GameObject gameObject = Utils.DuplicateGOB(this.m_ItemPrefab);
    gameObject.SetActive(true);
    IMemberItemRenderer component = gameObject.GetComponent<IMemberItemRenderer>();
    component.SetData(this.m_AllianceData, user, 0);
    this.m_ItemDict.Add(user.uid, component);
  }

  private void OnEnable()
  {
    this.RegisterListeners();
  }

  private void OnDisable()
  {
    this.UnregisterListeners();
  }

  private void RegisterListeners()
  {
    if (this.m_AllianceData == null)
      return;
    DBManager.inst.DB_AllianceInviteApply.onDataCreate += new System.Action<long, long>(this.OnCandidateCreate);
    DBManager.inst.DB_AllianceInviteApply.onDataRemove += new System.Action<long, long>(this.OnCandidateRemove);
    this.m_AllianceData.members.onDataCreate += new System.Action<AllianceMemberData>(this.OnMemberCreate);
  }

  private void UnregisterListeners()
  {
    if (this.m_AllianceData == null || !GameEngine.IsAvailable || GameEngine.IsShuttingDown)
      return;
    DBManager.inst.DB_AllianceInviteApply.onDataCreate -= new System.Action<long, long>(this.OnCandidateCreate);
    DBManager.inst.DB_AllianceInviteApply.onDataRemove -= new System.Action<long, long>(this.OnCandidateRemove);
    this.m_AllianceData.members.onDataCreate -= new System.Action<AllianceMemberData>(this.OnMemberCreate);
  }

  private void OnMemberCreate(AllianceMemberData member)
  {
    if (this.m_AllianceData.allianceId == member.allianceId)
      this.RemoveMember(member.uid);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void OnCandidateCreate(long uid, long allianceId)
  {
    if (this.m_AllianceData.allianceId == allianceId && DBManager.inst.DB_AllianceInviteApply.Get(allianceId, uid).status == AllianceInvitedApplyData.Status.asking)
      this.CreateMember(DBManager.inst.DB_User.Get(uid));
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void OnCandidateRemove(long uid, long allianceId)
  {
    if (this.m_AllianceData.allianceId == allianceId && DBManager.inst.DB_AllianceInviteApply.Get(allianceId, uid).status == AllianceInvitedApplyData.Status.asking)
      this.RemoveMember(uid);
    this.m_Members.text = this.m_ItemDict.Count.ToString();
  }

  private void CreateMember(UserData user)
  {
    if (this.m_ItemDict.ContainsKey(user.uid))
      return;
    this.AddItem(user);
    this.m_Grid.Reposition();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  private void RemoveMember(long uid)
  {
    if (!this.m_ItemDict.ContainsKey(uid))
      return;
    IMemberItemRenderer memberItemRenderer = this.m_ItemDict[uid];
    memberItemRenderer.gameObject.SetActive(false);
    memberItemRenderer.gameObject.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) memberItemRenderer.gameObject);
    this.m_ItemDict.Remove(uid);
    this.m_Grid.Reposition();
    if (this.m_OnChanged == null)
      return;
    this.m_OnChanged();
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
