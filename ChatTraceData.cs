﻿// Decompiled with JetBrains decompiler
// Type: ChatTraceData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class ChatTraceData
{
  public ArrayList datas = new ArrayList(2);
  public const int TRACE_TYPE_INDEX = 0;
  public const int TRACE_SPEAKER_FLAG_INDEX = 1;
  public const int TRACE_PRE_INDEX = 2;
  private bool _isSpeaker;
  private ChatTraceData.TraceType _type;

  public ChatTraceData()
  {
    this.datas.Clear();
    this.datas.Add((object) ChatTraceData.TraceType.Empty);
    this.datas.Add((object) false);
  }

  public bool isSpeaker
  {
    set
    {
      if (this.datas.Count <= 0)
        return;
      this.datas[1] = (object) value;
      this._isSpeaker = value;
    }
    get
    {
      return this._isSpeaker;
    }
  }

  public ChatTraceData.TraceType type
  {
    set
    {
      this._type = value;
      if (this.datas.Count > 0)
        this.datas[0] = (object) this._type;
      else
        this.datas.Add((object) this._type);
    }
    get
    {
      return this._type;
    }
  }

  public bool containeData
  {
    get
    {
      return this.type != ChatTraceData.TraceType.Empty;
    }
  }

  public void Decode(object orgData)
  {
    if (orgData == null)
      return;
    this.datas = orgData as ArrayList;
    this._type = ChatTraceData.TraceType.Empty;
    if (this.datas == null)
      return;
    this._type = this.datas.Count <= 0 || this.datas[0] == null || !Enum.IsDefined(typeof (ChatTraceData.TraceType), (object) this.datas[0].ToString()) ? ChatTraceData.TraceType.NoDefined : (ChatTraceData.TraceType) Enum.Parse(typeof (ChatTraceData.TraceType), this.datas[0].ToString());
    if (this.datas.Count > 1 && this.datas[1] != null)
      bool.TryParse(this.datas[1].ToString(), out this._isSpeaker);
    else
      this._isSpeaker = false;
  }

  public ArrayList Encode()
  {
    return this.datas;
  }

  public void InsertData(string[] newDatas)
  {
    this.datas.Clear();
    this.datas.Add((object) this.type);
    this.datas.Add((object) this.isSpeaker);
    for (int index = 0; newDatas != null && index < newDatas.Length; ++index)
      this.datas.Add((object) newDatas[index]);
  }

  public object GetData(int index)
  {
    if (index + 2 < this.datas.Count && this.datas != null)
      return this.datas[index + 2];
    return (object) null;
  }

  public int GetInt(int index)
  {
    int result = 0;
    if (index + 2 < this.datas.Count && this.datas != null && (this.datas[index + 2] != null && int.TryParse(this.datas[index + 2].ToString(), out result)))
      return result;
    return result;
  }

  public long GetLong(int index)
  {
    long result = 0;
    if (index + 2 < this.datas.Count && this.datas != null && (this.datas[index + 2] != null && long.TryParse(this.datas[index + 2].ToString(), out result)))
      return result;
    return result;
  }

  public string GetString(int index)
  {
    string empty = string.Empty;
    if (index + 2 < this.datas.Count && this.datas != null && this.datas[index + 2] != null)
      return this.datas[index + 2].ToString();
    return empty;
  }

  public string ShowInfo()
  {
    string str = "Type : " + this._type.ToString() + " -- ";
    if (this.datas != null && this.datas.Count > 0)
    {
      for (int index = 1; index < this.datas.Count; ++index)
        str = str + string.Empty + (object) index + ":" + this.datas[index] + ", ";
    }
    return str;
  }

  public enum TraceType
  {
    Empty,
    NoDefined,
    WarReport_Attack,
    WarReport_BeAttack,
    WarReport_Rally,
    WarReport_BeRally,
    WarReport_Scout,
    Item,
    Forge,
    AllianceAdimMessage,
    JoinAlliance,
    AcceptJoinAlliance,
    Knight_Start,
    CasinoCard,
    Rally,
    Trade,
    Bookmark,
    EquimentEnhance,
    MagicReport_Attack,
    MagicReport_BeAttack,
    Chat_Member,
    ForgeDK,
    EquimentEnhanceDK,
  }
}
