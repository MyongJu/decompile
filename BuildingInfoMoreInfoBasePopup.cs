﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfoMoreInfoBasePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingInfoMoreInfoBasePopup : Popup
{
  protected BuildingInfo buildingInfo;
  protected string[] headerData;
  protected List<string[]> elementsData;
  public UILabel titelLabel;
  public UILabel descriptionLabel;
  public GridElementComponent header;
  public UIGrid elementGrid;
  public int totalWidth;
  public UIScrollView scrollView;
  public GameObject elementStyle0;
  public GameObject elementStyle1;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.buildingInfo = (orgParam as BuildingInfoMoreInfoBasePopup.Parameter).info;
    this.titelLabel.text = Utils.XLAT(this.buildingInfo.Type + "_name");
    this.descriptionLabel.text = Utils.XLAT(this.buildingInfo.Type + "_description");
    this.PrepareData();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  protected virtual void PrepareData()
  {
    BuildingInfoUtils.PrepareDataMap[this.buildingInfo.Type](ref this.headerData, ref this.elementsData, this.buildingInfo);
  }

  protected virtual void UpdateUI()
  {
    this.header.FeedData((IComponentData) new GridElementComponentData(this.headerData, this.totalWidth));
    float b = 100f;
    for (int index = 0; index < this.elementsData.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.elementGrid.gameObject, index % 2 != 0 ? this.elementStyle1 : this.elementStyle0);
      gameObject.GetComponentInChildren<GridElementComponent>().FeedData((IComponentData) new GridElementComponentData(this.elementsData[index], this.totalWidth));
      if (index == this.buildingInfo.Building_Lvl - 1)
        gameObject.GetComponentInChildren<UISprite>().color = Utils.GetColorFromHex(8738840U);
      b = Mathf.Max(gameObject.GetComponentInChildren<UIGrid>().cellHeight, b);
    }
    foreach (UIWidget componentsInChild in this.elementGrid.GetComponentsInChildren<UISprite>())
      componentsInChild.height = (int) b;
    this.elementGrid.cellHeight = b;
    this.elementGrid.Reposition();
    this.scrollView.ResetPosition();
  }

  public class Parameter : Popup.PopupParameter
  {
    public BuildingInfo info;
  }
}
