﻿// Decompiled with JetBrains decompiler
// Type: SubscriptionHelpBtn
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class SubscriptionHelpBtn : MonoBehaviour
{
  public string iosID = "Default";
  public string androidID = "Default";
  public bool useDefaultClickHandle = true;
  private string ID = string.Empty;

  public void OnClick()
  {
    if (!this.useDefaultClickHandle)
      return;
    switch (Application.platform)
    {
      case RuntimePlatform.IPhonePlayer:
        this.ID = this.iosID;
        break;
      case RuntimePlatform.Android:
        this.ID = this.androidID;
        break;
      default:
        this.ID = this.androidID;
        break;
    }
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = this.ID
    });
  }
}
