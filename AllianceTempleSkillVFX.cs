﻿// Decompiled with JetBrains decompiler
// Type: AllianceTempleSkillVFX
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AllianceTempleSkillVFX : MonoBehaviour, IConstructionController
{
  private TileData m_TileData;
  private bool m_dirty;

  public void Reset()
  {
    this.m_dirty = false;
  }

  public void UpdateUI(TileData tile)
  {
    this.m_TileData = tile;
    this.m_dirty = true;
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    this.m_TileData.Magic.SetSortingLayerID(sortingLayerID);
  }

  public void Update()
  {
    if (!this.m_dirty || this.m_TileData == null)
      return;
    this.m_dirty = false;
    this.m_TileData.Magic.UpdateVFX(this.transform);
  }
}
