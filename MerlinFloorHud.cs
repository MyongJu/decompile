﻿// Decompiled with JetBrains decompiler
// Type: MerlinFloorHud
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinFloorHud : Dialog
{
  [SerializeField]
  private float _monsterDistance = 10f;
  [SerializeField]
  private MerlinTowerSystem _merlinTowerSystem;
  [SerializeField]
  private UILabel _labelMineLevel;
  [SerializeField]
  private UILabel _labelFloorNumber;
  [SerializeField]
  private UIButton _buttonViewBenefit;
  [SerializeField]
  private UIButton _buttonBuyBenefit;
  [SerializeField]
  private GameObject _rootGoUpstair;
  [SerializeField]
  private GameObject _rootEnterKingdom;
  [SerializeField]
  private UIButton _buttonAttack;
  [SerializeField]
  private GameObject _rootMonster;
  [SerializeField]
  private Transform _transformMonsterInUI;
  [SerializeField]
  private Transform _transformMonster;
  [SerializeField]
  private Camera _monsterCamera;
  [SerializeField]
  private Camera _uiCamera;
  [SerializeField]
  private UILabel _labelCurrencyCount;
  [SerializeField]
  private GameObject _redTip;
  [SerializeField]
  private MerlinFloorAnimation _floorAnimation;
  private bool _uiDirty;
  private bool _needShowBattleReuslt;
  private Hashtable troops;
  private long monsterId;
  private string monsterName;
  private MagicTowerMonsterInfo monsterInfo;
  private Hashtable _warReportDetail;
  private bool _isWin;

  private UserMerlinTowerData TowerData
  {
    get
    {
      return MerlinTowerPayload.Instance.UserData;
    }
  }

  protected void Update()
  {
    if (this._uiDirty)
    {
      this._uiDirty = false;
      this.UpdateUI();
    }
    if (!this._needShowBattleReuslt)
      return;
    this._needShowBattleReuslt = false;
    this.ShowBattleResult();
  }

  protected void ShowBattleResult()
  {
    Hashtable battleResult = MerlinTowerPayload.Instance.BattleResult;
    if (battleResult == null)
      return;
    if (battleResult.ContainsKey((object) "start_troops") && battleResult[(object) "start_troops"] != null)
      this.troops = battleResult[(object) "start_troops"] as Hashtable;
    if (battleResult.ContainsKey((object) "defender_id") && battleResult[(object) "defender_id"] != null)
    {
      long num = long.Parse(battleResult[(object) "defender_id"].ToString());
      this.monsterId = num;
      this.monsterInfo = ConfigManager.inst.DB_MagicTowerMonster.GetData((int) num);
    }
    if (battleResult.ContainsKey((object) "defender_name") && battleResult[(object) "defender_name"] != null)
      this.monsterName = battleResult[(object) "defender_name"].ToString();
    if (battleResult.ContainsKey((object) "win") && battleResult[(object) "win"] != null)
      bool.TryParse(battleResult[(object) "win"].ToString(), out this._isWin);
    if (battleResult.ContainsKey((object) "detail") && battleResult[(object) "detail"] != null)
      this._warReportDetail = battleResult[(object) "detail"] as Hashtable;
    this.ShowAnimation();
  }

  private void ShowAnimation()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
    DragonData dragonData = PlayerData.inst.dragonData;
    int num = 0;
    if (dragonData != null)
      num = dragonData.Level;
    MerlinTowerAttackShowPara towerAttackShowPara1 = new MerlinTowerAttackShowPara();
    MerlinTowerAttackShowPara towerAttackShowPara2 = towerAttackShowPara1;
    DB.DragonInfo dragonInfo;
    if (dragonData == null)
      dragonInfo = (DB.DragonInfo) null;
    else
      dragonInfo = new DB.DragonInfo()
      {
        level = num,
        tendency = dragonData.tendency
      };
    towerAttackShowPara2.dragon = dragonInfo;
    towerAttackShowPara1.targetName = this.monsterName;
    towerAttackShowPara1.troops = this.troops;
    MerlinTowerAttackShowPara towerAttackShowPara3 = towerAttackShowPara1;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerAttackShowPopup", (Popup.PopupParameter) new MerlinTowerAttackShowPopup.Parameter()
    {
      para = towerAttackShowPara3,
      towerData = this.TowerData,
      isWin = this._isWin,
      warReportData = this._warReportDetail
    });
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    Logger.Log("MerlinFloorHud::OnShow");
    base.OnShow(orgParam);
    this._uiDirty = false;
    this._needShowBattleReuslt = false;
    this.UpdateUI();
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    RequestManager.inst.AddObserver("magicTower:challengeTower", new System.Action<string, bool>(this.OnChallengeResponse));
    RequestManager.inst.AddObserver("magicTower:climbTower", new System.Action<string, bool>(this.OnClimbTowerResponse));
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    Logger.Log("MerlinFloorHud::OnHide");
    base.OnHide(orgParam);
    this.DestoryCurrentMonster();
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    RequestManager.inst.RemoveObserver("magicTower:challengeTower", new System.Action<string, bool>(this.OnChallengeResponse));
    RequestManager.inst.RemoveObserver("magicTower:climbTower", new System.Action<string, bool>(this.OnClimbTowerResponse));
  }

  private void OnSecondEvent(int serverTimestamp)
  {
    if (this.TowerData.CurrentTowerMainInfo == null)
      return;
    int level = this.TowerData.CurrentTowerMainInfo.Level;
    if (this.TowerData.HasMonster && level > 1)
      this.SetRedTipDisplay(level - 1);
    else
      this.SetRedTipDisplay(level);
  }

  private void SetRedTipDisplay(int level)
  {
    if (MerlinTowerPayload.Instance.CanShowTipInCurrentLevel(level))
      this._redTip.SetActive(true);
    else
      this._redTip.SetActive(false);
  }

  protected void OnChallengeResponse(string portType, bool result)
  {
    if (!result)
      return;
    this._uiDirty = true;
    this._needShowBattleReuslt = true;
  }

  protected void OnItemDataChanged(int id)
  {
    this.UpdateCurrencyCount();
  }

  protected void UpdateUI()
  {
    this._floorAnimation.SetCurrentRoomData(this.TowerData);
    this.UpdateCurrencyCount();
    this.UpdateMonster();
    if (this.TowerData == null)
    {
      Logger.Error((object) "No Tower Data In Merlin Tower Info");
    }
    else
    {
      MagicTowerMainInfo currentTowerMainInfo = this.TowerData.CurrentTowerMainInfo;
      if (currentTowerMainInfo == null)
        return;
      this._labelFloorNumber.text = ScriptLocalization.GetWithPara("tower_floor_number_name", new Dictionary<string, string>()
      {
        {
          "0",
          currentTowerMainInfo.Level.ToString()
        }
      }, 1 != 0);
      this._rootEnterKingdom.SetActive(currentTowerMainInfo.Level > 1);
      this._buttonBuyBenefit.gameObject.SetActive(currentTowerMainInfo.Level >= 1);
      this._buttonViewBenefit.gameObject.SetActive(currentTowerMainInfo.Level >= 1);
      this._labelMineLevel.text = ScriptLocalization.GetWithPara("tower_enter_mines_button", new Dictionary<string, string>()
      {
        {
          "0",
          currentTowerMainInfo.MineLevel.ToString()
        }
      }, 1 != 0);
      this._labelMineLevel.transform.position = this._floorAnimation.GetDoorPosition();
    }
  }

  protected void UpdateCurrencyCount()
  {
    this._labelCurrencyCount.text = Utils.FormatThousands(ItemBag.Instance.GetItemCount("item_magic_tower_coin_1").ToString());
  }

  protected void DestoryCurrentMonster()
  {
    for (int index = 0; index < this._transformMonster.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._transformMonster.GetChild(index).gameObject);
    this._rootMonster.SetActive(false);
  }

  protected void UpdateMonster()
  {
    if (this.TowerData.CurrentTowerMainInfo == null)
      return;
    this.DestoryCurrentMonster();
    string modelPath = this.TowerData.CurrentTowerMainInfo.ModelPath;
    bool flag = this.TowerData.HasMonster && !string.IsNullOrEmpty(modelPath);
    if (flag)
    {
      this._rootMonster.SetActive(true);
      UnityEngine.Object original = AssetManager.Instance.Load(modelPath, (System.Type) null);
      if ((bool) original)
      {
        GameObject go = UnityEngine.Object.Instantiate(original) as GameObject;
        go.transform.SetParent(this._transformMonster);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = Vector3.one;
        go.transform.localRotation = Quaternion.identity;
        Vector3 vector3 = this._monsterCamera.transform.position + this._monsterCamera.ScreenPointToRay(this._uiCamera.WorldToScreenPoint(this._transformMonsterInUI.position)).direction.normalized * this._monsterDistance;
        this._transformMonster.position = vector3;
        this._transformMonster.LookAt(new Vector3(this._monsterCamera.transform.position.x, vector3.y, this._monsterCamera.transform.position.z));
        UnityEngine.Animation componentInChildren = go.GetComponentInChildren<UnityEngine.Animation>();
        if ((bool) ((UnityEngine.Object) componentInChildren))
          componentInChildren.Play("Idle");
        NGUITools.SetLayer(go, this._transformMonster.gameObject.layer);
      }
      else
        Logger.Error((object) "Monster prefab path error: ", (object) original);
    }
    else
      this._rootMonster.SetActive(false);
    this._buttonAttack.gameObject.SetActive(flag);
    MagicTowerMainInfo dataWithLevel = ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(this.TowerData.CurrentTowerMainInfo.Level + 1);
    this._rootGoUpstair.SetActive(!flag && dataWithLevel != null);
  }

  public void OnButtonGoUpstairClicked()
  {
    Logger.Log(nameof (OnButtonGoUpstairClicked));
    MagicTowerMainInfo dataWithLevel = ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(this.TowerData.CurrentTowerMainInfo.Level + 1);
    if (dataWithLevel != null)
    {
      MerlinTowerPayload.Instance.SendClaimbTowerRequest(dataWithLevel.internalId);
    }
    else
    {
      Logger.Log("already max level");
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_already_scaled", true), (System.Action) null, 4f, true);
    }
  }

  public void OnClimbTowerResponse(string portType, bool result)
  {
    if (!result)
      return;
    this._rootGoUpstair.SetActive(false);
    this._rootEnterKingdom.SetActive(false);
    this._floorAnimation.PlayAnimation((System.Action) (() =>
    {
      if (ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(this.TowerData.CurrentTowerMainInfo.Level + 1) == null)
        UIManager.inst.toast.Show(ScriptLocalization.Get("toast_tower_already_scaled", true), (System.Action) null, 4f, true);
      this.UpdateUI();
    }));
  }

  public void OnButtonEnterKingdomClicked()
  {
    Logger.Log(nameof (OnButtonEnterKingdomClicked));
    MagicTowerMainInfo currentTowerMainInfo = MerlinTowerPayload.Instance.UserData.CurrentTowerMainInfo;
    if (currentTowerMainInfo == null)
      return;
    MessageBoxWith2Buttons.Parameter parameter = new MessageBoxWith2Buttons.Parameter();
    string withPara1 = ScriptLocalization.GetWithPara("tower_enter_mines_button", new Dictionary<string, string>()
    {
      {
        "0",
        currentTowerMainInfo.MineLevel.ToString()
      }
    }, 1 != 0);
    string withPara2 = ScriptLocalization.GetWithPara("tower_enter_mine_description", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatShortThousands(currentTowerMainInfo.Speed * 3600)
      }
    }, 1 != 0);
    parameter.title = withPara1;
    parameter.content = withPara2;
    parameter.yes = ScriptLocalization.Get("id_uppercase_yes", true);
    parameter.no = ScriptLocalization.Get("id_uppercase_no", true);
    parameter.yesCallback = new System.Action(this.OnConfirmEnterKingdom);
    parameter.noCallback = (System.Action) null;
    UIManager.inst.OpenPopup("MessageBoxWith2Buttons", (Popup.PopupParameter) parameter);
  }

  protected void OnConfirmEnterKingdom()
  {
    Logger.Log(nameof (OnConfirmEnterKingdom));
    UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
    {
      this._merlinTowerSystem.EnterKingdom();
      UIManager.inst.Cloud.PokeCloud((System.Action) null);
    }));
  }

  public void OnButtonAttackClicked()
  {
    AudioManager.Instance.StopAndPlaySound("sfx_dragon_knight_battle_room");
    Logger.Log(nameof (OnButtonAttackClicked));
    if (this.TowerData.towerId <= 0)
      return;
    UIManager.inst.OpenDlg("MerlinTower/MerlinTowerMarchAllocDlg", (UI.Dialog.DialogParameter) new MerlinTowerMarchAllocDlg.Parameter()
    {
      towerId = this.TowerData.towerId,
      desTroopCount = (int) MerlinTowerPayload.Instance.UserData.capacity,
      confirmCallback = new System.Action<Hashtable, bool>(this.OnMarchClick),
      marchType = MarchAllocDlg.MarchType.monsterAttack,
      isRally = false
    }, true, true, true);
  }

  private void OnMarchClick(Hashtable troops, bool isWidthDragon)
  {
    if (this.TowerData == null)
      return;
    MerlinTowerPayload.Instance.SendChallengeTowerRequest(this.TowerData.towerId);
  }

  public void OnButtonSettingClicked()
  {
    Logger.Log(nameof (OnButtonSettingClicked));
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerSettingPopup", (Popup.PopupParameter) null);
  }

  public void OnButtonBuyBenefitClicked()
  {
    Logger.Log(nameof (OnButtonBuyBenefitClicked));
    MagicTowerMainInfo currentTowerMainInfo = this.TowerData.CurrentTowerMainInfo;
    if (currentTowerMainInfo == null)
      return;
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerBuffStorePopup", (Popup.PopupParameter) new MerlinTowerBuffStorePopup.Parameter()
    {
      layer = currentTowerMainInfo.Level
    });
  }

  public void OnButtonViewBenefitClicked()
  {
    Logger.Log(nameof (OnButtonViewBenefitClicked));
    UIManager.inst.OpenPopup("MerlinTower/MerlinTowerViewBenefitsPopup", (Popup.PopupParameter) null);
  }

  public void OnButtonExchangeShopClick()
  {
    UIManager.inst.OpenDlg("DragonKnight/DungeonAncientStoreDialog", (UI.Dialog.DialogParameter) new DungeonAncientStoreDialog.Parameter()
    {
      group = "magic_tower_shop",
      currency = "item_magic_tower_coin_1",
      titleKey = "tower_shop_name"
    }, true, true, true);
  }
}
