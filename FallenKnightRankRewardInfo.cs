﻿// Decompiled with JetBrains decompiler
// Type: FallenKnightRankRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class FallenKnightRankRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public int Type;
  [Config(Name = "rank_min")]
  public long RankMin;
  [Config(Name = "rank_max")]
  public long RankMax;
  [Config(Name = "fund")]
  public long Fund;
  [Config(Name = "honor")]
  public long Honor;
  [Config(Name = "reward_id_1")]
  public string RankReward_1;
  [Config(Name = "reward_value_1")]
  public long RankRewardValue_1;
  [Config(Name = "reward_id_2")]
  public string RankReward_2;
  [Config(Name = "reward_value_2")]
  public long RankRewardValue_2;
  [Config(Name = "reward_id_3")]
  public string RankReward_3;
  [Config(Name = "reward_value_3")]
  public long RankRewardValue_3;
  [Config(Name = "reward_id_4")]
  public string RankReward_4;
  [Config(Name = "reward_value_4")]
  public long RankRewardValue_4;
  [Config(Name = "reward_id_5")]
  public string RankReward_5;
  [Config(Name = "reward_value_5")]
  public long RankRewardValue_5;
  [Config(Name = "reward_id_6")]
  public string RankReward_6;
  [Config(Name = "reward_value_6")]
  public long RankRewardValue_6;
}
