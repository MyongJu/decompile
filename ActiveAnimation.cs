﻿// Decompiled with JetBrains decompiler
// Type: ActiveAnimation
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using AnimationOrTween;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Internal/Active Animation")]
public class ActiveAnimation : MonoBehaviour
{
  public List<EventDelegate> onFinished = new List<EventDelegate>();
  private string mClip = string.Empty;
  public static ActiveAnimation current;
  [HideInInspector]
  public GameObject eventReceiver;
  [HideInInspector]
  public string callWhenFinished;
  private UnityEngine.Animation mAnim;
  private AnimationOrTween.Direction mLastDirection;
  private AnimationOrTween.Direction mDisableDirection;
  private bool mNotify;
  private Animator mAnimator;

  private float playbackTime
  {
    get
    {
      return Mathf.Clamp01(this.mAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);
    }
  }

  public bool isPlaying
  {
    get
    {
      if ((UnityEngine.Object) this.mAnim == (UnityEngine.Object) null)
      {
        if (!((UnityEngine.Object) this.mAnimator != (UnityEngine.Object) null))
          return false;
        if (this.mLastDirection == AnimationOrTween.Direction.Reverse)
        {
          if ((double) this.playbackTime == 0.0)
            return false;
        }
        else if ((double) this.playbackTime == 1.0)
          return false;
        return true;
      }
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (this.mAnim.IsPlaying(current.name))
          {
            if (this.mLastDirection == AnimationOrTween.Direction.Forward)
            {
              if ((double) current.time < (double) current.length)
                return true;
            }
            else if (this.mLastDirection != AnimationOrTween.Direction.Reverse || (double) current.time > 0.0)
              return true;
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      return false;
    }
  }

  public void Finish()
  {
    if ((UnityEngine.Object) this.mAnim != (UnityEngine.Object) null)
    {
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (this.mLastDirection == AnimationOrTween.Direction.Forward)
            current.time = current.length;
          else if (this.mLastDirection == AnimationOrTween.Direction.Reverse)
            current.time = 0.0f;
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      this.mAnim.Sample();
    }
    else
    {
      if (!((UnityEngine.Object) this.mAnimator != (UnityEngine.Object) null))
        return;
      this.mAnimator.Play(this.mClip, 0, this.mLastDirection != AnimationOrTween.Direction.Forward ? 0.0f : 1f);
    }
  }

  public void Reset()
  {
    if ((UnityEngine.Object) this.mAnim != (UnityEngine.Object) null)
    {
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (this.mLastDirection == AnimationOrTween.Direction.Reverse)
            current.time = current.length;
          else if (this.mLastDirection == AnimationOrTween.Direction.Forward)
            current.time = 0.0f;
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
    }
    else
    {
      if (!((UnityEngine.Object) this.mAnimator != (UnityEngine.Object) null))
        return;
      this.mAnimator.Play(this.mClip, 0, this.mLastDirection != AnimationOrTween.Direction.Reverse ? 0.0f : 1f);
    }
  }

  private void Start()
  {
    if (!((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null) || !EventDelegate.IsValid(this.onFinished))
      return;
    this.eventReceiver = (GameObject) null;
    this.callWhenFinished = (string) null;
  }

  private void Update()
  {
    float deltaTime = RealTime.deltaTime;
    if ((double) deltaTime == 0.0)
      return;
    if ((UnityEngine.Object) this.mAnimator != (UnityEngine.Object) null)
    {
      this.mAnimator.Update(this.mLastDirection != AnimationOrTween.Direction.Reverse ? deltaTime : -deltaTime);
      if (this.isPlaying)
        return;
      this.mAnimator.enabled = false;
      this.enabled = false;
    }
    else if ((UnityEngine.Object) this.mAnim != (UnityEngine.Object) null)
    {
      bool flag = false;
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (this.mAnim.IsPlaying(current.name))
          {
            float num = current.speed * deltaTime;
            current.time += num;
            if ((double) num < 0.0)
            {
              if ((double) current.time > 0.0)
                flag = true;
              else
                current.time = 0.0f;
            }
            else if ((double) current.time < (double) current.length)
              flag = true;
            else
              current.time = current.length;
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      this.mAnim.Sample();
      if (flag)
        return;
      this.enabled = false;
    }
    else
    {
      this.enabled = false;
      return;
    }
    if (!this.mNotify)
      return;
    this.mNotify = false;
    if ((UnityEngine.Object) ActiveAnimation.current == (UnityEngine.Object) null)
    {
      ActiveAnimation.current = this;
      EventDelegate.Execute(this.onFinished);
      if ((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.callWhenFinished))
        this.eventReceiver.SendMessage(this.callWhenFinished, SendMessageOptions.DontRequireReceiver);
      ActiveAnimation.current = (ActiveAnimation) null;
    }
    if (this.mDisableDirection == AnimationOrTween.Direction.Toggle || this.mLastDirection != this.mDisableDirection)
      return;
    NGUITools.SetActive(this.gameObject, false);
  }

  private void Play(string clipName, AnimationOrTween.Direction playDirection)
  {
    if (playDirection == AnimationOrTween.Direction.Toggle)
      playDirection = this.mLastDirection == AnimationOrTween.Direction.Forward ? AnimationOrTween.Direction.Reverse : AnimationOrTween.Direction.Forward;
    if ((UnityEngine.Object) this.mAnim != (UnityEngine.Object) null)
    {
      this.enabled = true;
      this.mAnim.enabled = false;
      if (string.IsNullOrEmpty(clipName))
      {
        if (!this.mAnim.isPlaying)
          this.mAnim.Play();
      }
      else if (!this.mAnim.IsPlaying(clipName))
        this.mAnim.Play(clipName);
      IEnumerator enumerator = this.mAnim.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          AnimationState current = (AnimationState) enumerator.Current;
          if (string.IsNullOrEmpty(clipName) || current.name == clipName)
          {
            float num = Mathf.Abs(current.speed);
            current.speed = num * (float) playDirection;
            if (playDirection == AnimationOrTween.Direction.Reverse && (double) current.time == 0.0)
              current.time = current.length;
            else if (playDirection == AnimationOrTween.Direction.Forward && (double) current.time == (double) current.length)
              current.time = 0.0f;
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator as IDisposable;
        if (disposable != null)
          disposable.Dispose();
      }
      this.mLastDirection = playDirection;
      this.mNotify = true;
      this.mAnim.Sample();
    }
    else
    {
      if (!((UnityEngine.Object) this.mAnimator != (UnityEngine.Object) null))
        return;
      if (this.enabled && this.isPlaying && this.mClip == clipName)
      {
        this.mLastDirection = playDirection;
      }
      else
      {
        this.enabled = true;
        this.mNotify = true;
        this.mLastDirection = playDirection;
        this.mClip = clipName;
        this.mAnimator.Play(this.mClip, 0, playDirection != AnimationOrTween.Direction.Forward ? 1f : 0.0f);
      }
    }
  }

  public static ActiveAnimation Play(UnityEngine.Animation anim, string clipName, AnimationOrTween.Direction playDirection, EnableCondition enableBeforePlay, DisableCondition disableCondition)
  {
    if (!NGUITools.GetActive(anim.gameObject))
    {
      if (enableBeforePlay != EnableCondition.EnableThenPlay)
        return (ActiveAnimation) null;
      NGUITools.SetActive(anim.gameObject, true);
      UIPanel[] componentsInChildren = anim.gameObject.GetComponentsInChildren<UIPanel>();
      int index = 0;
      for (int length = componentsInChildren.Length; index < length; ++index)
        componentsInChildren[index].Refresh();
    }
    ActiveAnimation activeAnimation = anim.GetComponent<ActiveAnimation>();
    if ((UnityEngine.Object) activeAnimation == (UnityEngine.Object) null)
      activeAnimation = anim.gameObject.AddComponent<ActiveAnimation>();
    activeAnimation.mAnim = anim;
    activeAnimation.mDisableDirection = (AnimationOrTween.Direction) disableCondition;
    activeAnimation.onFinished.Clear();
    activeAnimation.Play(clipName, playDirection);
    if ((UnityEngine.Object) activeAnimation.mAnim != (UnityEngine.Object) null)
      activeAnimation.mAnim.Sample();
    else if ((UnityEngine.Object) activeAnimation.mAnimator != (UnityEngine.Object) null)
      activeAnimation.mAnimator.Update(0.0f);
    return activeAnimation;
  }

  public static ActiveAnimation Play(UnityEngine.Animation anim, string clipName, AnimationOrTween.Direction playDirection)
  {
    return ActiveAnimation.Play(anim, clipName, playDirection, EnableCondition.DoNothing, DisableCondition.DoNotDisable);
  }

  public static ActiveAnimation Play(UnityEngine.Animation anim, AnimationOrTween.Direction playDirection)
  {
    return ActiveAnimation.Play(anim, (string) null, playDirection, EnableCondition.DoNothing, DisableCondition.DoNotDisable);
  }

  public static ActiveAnimation Play(Animator anim, string clipName, AnimationOrTween.Direction playDirection, EnableCondition enableBeforePlay, DisableCondition disableCondition)
  {
    if (enableBeforePlay != EnableCondition.IgnoreDisabledState && !NGUITools.GetActive(anim.gameObject))
    {
      if (enableBeforePlay != EnableCondition.EnableThenPlay)
        return (ActiveAnimation) null;
      NGUITools.SetActive(anim.gameObject, true);
      UIPanel[] componentsInChildren = anim.gameObject.GetComponentsInChildren<UIPanel>();
      int index = 0;
      for (int length = componentsInChildren.Length; index < length; ++index)
        componentsInChildren[index].Refresh();
    }
    ActiveAnimation activeAnimation = anim.GetComponent<ActiveAnimation>();
    if ((UnityEngine.Object) activeAnimation == (UnityEngine.Object) null)
      activeAnimation = anim.gameObject.AddComponent<ActiveAnimation>();
    activeAnimation.mAnimator = anim;
    activeAnimation.mDisableDirection = (AnimationOrTween.Direction) disableCondition;
    activeAnimation.onFinished.Clear();
    activeAnimation.Play(clipName, playDirection);
    if ((UnityEngine.Object) activeAnimation.mAnim != (UnityEngine.Object) null)
      activeAnimation.mAnim.Sample();
    else if ((UnityEngine.Object) activeAnimation.mAnimator != (UnityEngine.Object) null)
      activeAnimation.mAnimator.Update(0.0f);
    return activeAnimation;
  }
}
