﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceActivityMainSub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceActivityMainSub
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, AllianceActivitySubInfo> datas;
  private Dictionary<int, AllianceActivitySubInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<AllianceActivitySubInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public AllianceActivitySubInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AllianceActivitySubInfo) null;
  }

  public AllianceActivitySubInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceActivitySubInfo) null;
  }
}
