﻿// Decompiled with JetBrains decompiler
// Type: MonsterStateIdle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class MonsterStateIdle : IState
{
  private MonsterAnimEvent _controller;

  public MonsterStateIdle(MonsterAnimEvent controller)
  {
    this._controller = controller;
  }

  public string Key
  {
    get
    {
      return typeof (MonsterStateIdle).ToString();
    }
  }

  public Hashtable Data { get; set; }

  public void OnEnter()
  {
    if (this._controller.Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
      return;
    this._controller.Animator.CrossFadeInFixedTime("Idle", 0.01f);
  }

  public void OnProcess()
  {
    if (!this._controller.HasAttacker)
      return;
    this._controller.StateMachine.SetState(typeof (MonsterStateAttack).ToString(), (Hashtable) null);
  }

  public void OnExit()
  {
  }

  public void Dispose()
  {
  }
}
