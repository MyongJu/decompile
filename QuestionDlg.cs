﻿// Decompiled with JetBrains decompiler
// Type: QuestionDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class QuestionDlg : Popup
{
  public UIButton leftButton;
  public UIButton rightButton;
  public UILabel leftLabel;
  public UILabel rightLable;
  public UIButton middleButton;
  public UILabel middleButtonLabel;
  public UILabel description;
  private EventDelegate.Callback _leftClick;
  private EventDelegate.Callback _rightClick;
  private EventDelegate.Callback _middleClick;

  private void Awake()
  {
    this.gameObject.SetActive(false);
  }

  public void ShowQuestion(string inDescription, string inLeftButtonName, EventDelegate.Callback inClickLeft, string inRightButtonName, EventDelegate.Callback inClickRight)
  {
    if (inDescription != null && (Object) this.description != (Object) null)
      this.description.text = inDescription;
    if (string.IsNullOrEmpty(inLeftButtonName) && string.IsNullOrEmpty(inRightButtonName))
    {
      D.error((object) "At leaset one of button need to active");
    }
    else
    {
      bool flag = string.IsNullOrEmpty(inLeftButtonName) || string.IsNullOrEmpty(inRightButtonName);
      this.leftButton.gameObject.SetActive(!flag);
      this.rightButton.gameObject.SetActive(!flag);
      this.middleButton.gameObject.SetActive(flag);
      if (!string.IsNullOrEmpty(inLeftButtonName))
      {
        UILabel uiLabel = !flag ? this.leftLabel : this.middleButtonLabel;
        if (flag)
          this._middleClick = inClickLeft;
        else
          this._leftClick = inClickLeft;
        uiLabel.text = inLeftButtonName;
      }
      if (!string.IsNullOrEmpty(inRightButtonName))
      {
        UILabel uiLabel = !flag ? this.rightLable : this.middleButtonLabel;
        if (flag)
          this._middleClick = inClickRight;
        else
          this._rightClick = inClickRight;
        uiLabel.text = inRightButtonName;
      }
      this.gameObject.SetActive(true);
    }
  }

  public void CancelQuestion()
  {
    this.CleanupAndClose();
  }

  public void ClickLeft()
  {
    if (this._leftClick != null)
      this._leftClick();
    this.CleanupAndClose();
  }

  public void ClickRight()
  {
    if (this._rightClick != null)
      this._rightClick();
    this.CleanupAndClose();
  }

  public void ClickMiddle()
  {
    if (this._middleClick != null)
      this._middleClick();
    this.CleanupAndClose();
  }

  private void CleanupAndClose()
  {
    this._leftClick = (EventDelegate.Callback) null;
    this._rightClick = (EventDelegate.Callback) null;
    this._middleClick = (EventDelegate.Callback) null;
    this.gameObject.SetActive(false);
  }
}
