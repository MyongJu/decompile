﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryBattleResultPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryBattleResultPopup : Popup
{
  private string vfxName = string.Empty;
  private const string VFX_PATH = "Prefab/Anniversary/";
  private const string IMAGE_PATH = "Texture/DragonKnight/DungeonTexture/";
  private const string BG_PATH = "Texture/GUI_Textures/";
  private const string PVP_RESULT_WIN_IMAGE = "dragon_knight_pvp_result_win";
  private const string PVP_RESULT_LOSE_IMAGE = "dragon_knight_pvp_result_lose";
  private const string PVP_RESULT_WIN_VFX = "fx_AnniversaryKingWarPopup_win";
  private const string ANN_WIN = "annv_winner";
  private const string ANN_LOSE = "annv_lost";
  public UILabel title;
  public UILabel announcement;
  public UILabel casualty_description;
  public UILabel self_num;
  public UILabel self_power;
  public UILabel self_name;
  public UILabel self_killCount;
  public UILabel self_troopNum;
  public UILabel self_troopPower;
  public UITexture self_image;
  public UILabel self_killDes;
  public UILabel other_num;
  public UILabel other_power;
  public UILabel other_name;
  public UILabel other_killCount;
  public UILabel other_troopNum;
  public UILabel other_troopPower;
  public UITexture other_image;
  public UITexture winner;
  public UILabel battle_detail;
  public UILabel other_killDes;
  public UILabel subTitle;
  public GameObject vfxRoot;
  public UITexture resultPVP;
  private AnniversaryBattleRole selfbattleRoleInfo;
  private AnniversaryBattleRole otherbattleRoleInfo;
  private bool isVictory;
  private string slotName;
  private GameObject vfxObj;

  private void UpdateUI()
  {
    this.UpdateTitles();
    this.UpdateBackgroud();
    this.UpdateSelfInfo();
    this.UpdateOtherInfo();
  }

  private void UpdateBackgroud()
  {
    if (this.isVictory)
    {
      this.vfxName = "fx_AnniversaryKingWarPopup_win";
      this.subTitle.text = Utils.XLAT("dragon_knight_pvp_success");
      BuilderFactory.Instance.HandyBuild((UIWidget) this.winner, "Texture/GUI_Textures/annv_winner", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.resultPVP, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_win", (System.Action<bool>) null, true, false, string.Empty);
      this.SetVFX(this.vfxName);
      AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_win", false);
    }
    else
    {
      this.subTitle.text = Utils.XLAT("dragon_knight_pvp_fail");
      BuilderFactory.Instance.HandyBuild((UIWidget) this.winner, "Texture/GUI_Textures/annv_lost", (System.Action<bool>) null, true, true, string.Empty);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.resultPVP, "Texture/DragonKnight/DungeonTexture/dragon_knight_pvp_result_lose", (System.Action<bool>) null, true, false, string.Empty);
      AudioManager.Instance.PlaySound("sfx_dragon_knight_pvp_lose", false);
    }
  }

  private void SetVFX(string vfxUsedName)
  {
    if (string.IsNullOrEmpty(vfxUsedName))
      return;
    this.vfxObj = AssetManager.Instance.HandyLoad("Prefab/Anniversary/" + vfxUsedName, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) this.vfxObj != (UnityEngine.Object) null))
      return;
    this.vfxObj = UnityEngine.Object.Instantiate<GameObject>(this.vfxObj);
    this.vfxObj.transform.parent = this.vfxRoot.transform;
    this.vfxObj.transform.localPosition = Vector3.zero;
    this.vfxObj.transform.localScale = Vector3.one;
  }

  private void UpdateTitles()
  {
    this.title.text = Utils.XLAT("event_1_year_tournament_results_name");
    if (this.isVictory)
      this.announcement.text = ScriptLocalization.GetWithPara("event_1_year_tournament_victory_announcement", new Dictionary<string, string>()
      {
        {
          "1",
          ScriptLocalization.Get(this.otherbattleRoleInfo.Name, true)
        },
        {
          "2",
          this.slotName
        }
      }, true);
    else
      this.announcement.text = Utils.XLAT("event_1_year_tournament_defeat_announcement");
    this.casualty_description.text = Utils.XLAT("event_1_year_tournament_casualty_description");
    this.battle_detail.text = Utils.XLAT("battle_report_button_uppercase_loaddamagedetails");
  }

  private void UpdateSelfInfo()
  {
    this.self_name.text = ScriptLocalization.Get(this.selfbattleRoleInfo.Name, true);
    this.self_num.text = this.selfbattleRoleInfo.troopCount.ToString();
    this.self_power.text = this.selfbattleRoleInfo.troopPower.ToString();
    this.self_killCount.text = this.selfbattleRoleInfo.killTroopCount.ToString();
    this.self_troopNum.text = Utils.XLAT("id_total_troops");
    this.self_troopPower.text = Utils.XLAT("id_troop_power");
    this.self_killDes.text = Utils.XLAT("battle_report_troopoverview_kills");
    CustomIconLoader.Instance.requestCustomIcon(this.self_image, UserData.GetPortraitIconPath(this.selfbattleRoleInfo.portrait), this.selfbattleRoleInfo.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.self_image, this.selfbattleRoleInfo.lordTitleId, 1);
  }

  private void UpdateOtherInfo()
  {
    this.other_name.text = ScriptLocalization.Get(this.otherbattleRoleInfo.Name, true);
    this.other_num.text = this.otherbattleRoleInfo.troopCount.ToString();
    this.other_power.text = this.otherbattleRoleInfo.troopPower.ToString();
    this.other_killCount.text = this.otherbattleRoleInfo.killTroopCount.ToString();
    this.other_troopNum.text = Utils.XLAT("id_total_troops");
    this.other_troopPower.text = Utils.XLAT("id_troop_power");
    this.other_killDes.text = Utils.XLAT("battle_report_troopoverview_kills");
    CustomIconLoader.Instance.requestCustomIcon(this.other_image, UserData.GetPortraitIconPath(this.otherbattleRoleInfo.portrait), this.otherbattleRoleInfo.icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.other_image, this.otherbattleRoleInfo.lordTitleId, 1);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AnniversaryBattleResultPopup.Parameter parameter = orgParam as AnniversaryBattleResultPopup.Parameter;
    if (parameter != null)
    {
      this.selfbattleRoleInfo = parameter.selfBattleRole;
      this.otherbattleRoleInfo = parameter.otherBattleRole;
      this.slotName = parameter.slotName;
      this.isVictory = parameter.isVictory;
    }
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.CloseTopPopup((Popup.PopupParameter) null);
  }

  public void OnBattleReportBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("Mail/MailMainDlg", (UI.Dialog.DialogParameter) new MailMainDlg.Parameter()
    {
      category = MailCategory.BattleReport
    }, true, true, true);
  }

  public class Parameter : Popup.PopupParameter
  {
    public AnniversaryBattleRole selfBattleRole;
    public AnniversaryBattleRole otherBattleRole;
    public string slotName;
    public bool isVictory;
  }
}
