﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightTalentActiveInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightTalentActiveInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "skill_id")]
  public int talentId;
  [Config(Name = "active_slot_id")]
  public int activeSlotId;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "skill_priority")]
  public int skillPriority;
}
