﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AnniversaryIapMainInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "iap")]
  public int iapPackageId;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "order")]
  public int order;
  [Config(Name = "buy_max")]
  public string buyMax;
  [Config(Name = "spend_number")]
  public int needPropsCount;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "is_hot")]
  public int isHot;

  public bool IsHot
  {
    get
    {
      return this.isHot == 1;
    }
  }

  public int BuyMax
  {
    get
    {
      int result = -1;
      if (int.TryParse(this.buyMax, out result))
        return result;
      return -1;
    }
  }

  public string Name
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }

  public string IconPath
  {
    get
    {
      return "Texture/IAP/" + this.icon;
    }
  }

  public string CatalogName
  {
    get
    {
      IAPPackageInfo iapPackageInfo = ConfigManager.inst.DB_IAPPackage.Get(this.iapPackageId);
      if (iapPackageInfo != null)
        return PaymentManager.Instance.GetFormattedPrice(iapPackageInfo.productId, iapPackageInfo.pay_id);
      return string.Empty;
    }
  }

  public IAPPackageInfo IapPackageInfo
  {
    get
    {
      return ConfigManager.inst.DB_IAPPackage.Get(this.iapPackageId);
    }
  }

  public string IapGroupName
  {
    get
    {
      return "anniversary_package";
    }
  }
}
