﻿// Decompiled with JetBrains decompiler
// Type: FirstAnniversaryRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class FirstAnniversaryRewardPopup : Popup
{
  private Dictionary<int, ItemIconRenderer> _itemDict = new Dictionary<int, ItemIconRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIButton _receiveGiftButton;
  [SerializeField]
  private ItemIconRenderer _giftItemIcon;
  [SerializeField]
  private UILabel _giftGreetings;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private GameObject _itemRoot;
  private SuperLoginMainInfo _mainInfo;
  private System.Action<int> _giftReceived;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    FirstAnniversaryRewardPopup.Parameter parameter = orgParam as FirstAnniversaryRewardPopup.Parameter;
    if (parameter != null)
    {
      this._mainInfo = parameter.mainInfo;
      this._giftReceived = parameter.onGiftReceived;
    }
    this._itemPool.Initialize(this._itemPrefab, this._itemRoot);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnClaimBtnPressed()
  {
    if (this._mainInfo == null)
      return;
    SuperLoginPayload.Instance.ReceiveDailyReward(this._mainInfo.day, "super_log_in_1_year_celebration", (System.Action<bool, object>) ((ret, datd) =>
    {
      if (!ret)
        return;
      if (this._giftReceived != null)
        this._giftReceived(this._mainInfo.day);
      this.ShowGiftReceivedEffect();
      this.OnCloseBtnPressed();
    }));
  }

  private void ShowGiftReceivedEffect()
  {
    if (this._mainInfo == null || this._mainInfo.Rewards == null)
      return;
    RewardsCollectionAnimator.Instance.Clear();
    if (this._mainInfo.Rewards != null && this._mainInfo.Rewards.rewards != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = this._mainInfo.Rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int.TryParse(current.Key, out result);
          if (result > 0)
            RewardsCollectionAnimator.Instance.items2.Add(new Item2RewardInfo.Data()
            {
              itemid = result,
              count = (float) current.Value
            });
        }
      }
    }
    RewardsCollectionAnimator.Instance.CollectItems2(false);
  }

  private void UpdateUI()
  {
    if (this._mainInfo == null)
      return;
    this._receiveGiftButton.isEnabled = this._mainInfo.RewardState == SuperLoginPayload.DailyRewardState.CAN_GET;
    this._giftGreetings.text = this._mainInfo.Description;
    this.ClearData();
    if (this._mainInfo.Rewards != null && this._mainInfo.Rewards.rewards != null)
    {
      int num = 0;
      using (Dictionary<string, int>.Enumerator enumerator = this._mainInfo.Rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int.TryParse(current.Key, out result);
          if (result > 0)
          {
            ItemIconRenderer itemRenderer = this.CreateItemRenderer(result, current.Value);
            this._itemDict.Add(num++, itemRenderer);
          }
        }
      }
    }
    this.Reposition();
  }

  private void ClearData()
  {
    using (Dictionary<int, ItemIconRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
  }

  private ItemIconRenderer CreateItemRenderer(int itemId, int itemCount)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    ItemIconRenderer component1 = gameObject.GetComponent<ItemIconRenderer>();
    component1.SetData(itemId, string.Empty, true);
    Transform child = gameObject.transform.FindChild("AmountValue");
    if ((bool) ((UnityEngine.Object) child))
    {
      UILabel component2 = child.GetComponent<UILabel>();
      if ((bool) ((UnityEngine.Object) component2))
        component2.text = itemCount.ToString();
    }
    return component1;
  }

  public class Parameter : Popup.PopupParameter
  {
    public SuperLoginMainInfo mainInfo;
    public System.Action<int> onGiftReceived;
  }
}
