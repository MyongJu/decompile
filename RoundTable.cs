﻿// Decompiled with JetBrains decompiler
// Type: RoundTable
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoundTable : MonoBehaviour
{
  public float m_Radius = 100f;
  public float m_StepAngle = 30f;
  public float m_AngleOffset;
  public bool m_RepositionNow;

  private void Start()
  {
    this.Reposition();
  }

  public void Reposition()
  {
    List<Transform> childList = this.GetChildList();
    int count = childList.Count;
    float num1 = (float) (Math.PI / 180.0 * (double) this.m_StepAngle * (double) (count - 1) * 0.5);
    float num2 = (float) Math.PI / 180f * this.m_StepAngle;
    float num3 = (float) Math.PI / 180f * this.m_AngleOffset - num1;
    for (int index = 0; index < count; ++index)
    {
      float f = num3 + num2 * (float) index;
      float x = this.m_Radius * Mathf.Cos(f);
      float y = this.m_Radius * Mathf.Sin(f);
      childList[index].localPosition = new Vector3(x, y, 0.0f);
    }
  }

  private List<Transform> GetChildList()
  {
    List<Transform> transformList = new List<Transform>();
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      Transform child = this.transform.GetChild(index);
      if ((bool) ((UnityEngine.Object) child) && NGUITools.GetActive(child.gameObject))
        transformList.Add(child);
    }
    return transformList;
  }

  private void Update()
  {
    if (!this.m_RepositionNow)
      return;
    this.m_RepositionNow = false;
    this.Reposition();
  }
}
