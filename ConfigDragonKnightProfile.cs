﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonKnightProfile
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDragonKnightProfile
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigDragonKnightProfileInfo> datas;
  private Dictionary<int, ConfigDragonKnightProfileInfo> dicByUniqueId;
  private Dictionary<string, List<ConfigDragonKnightProfileInfo>> categoryMap;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigDragonKnightProfileInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.categoryMap = new Dictionary<string, List<ConfigDragonKnightProfileInfo>>();
    if (this.dicByUniqueId == null)
      return;
    Dictionary<int, ConfigDragonKnightProfileInfo>.ValueCollection.Enumerator enumerator = this.dicByUniqueId.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this.categoryMap.ContainsKey(enumerator.Current.category))
        this.categoryMap.Add(enumerator.Current.category, new List<ConfigDragonKnightProfileInfo>());
      this.categoryMap[enumerator.Current.category].Add(enumerator.Current);
    }
  }

  public List<ConfigDragonKnightProfileInfo> GetDragonKnightProfileInfoByCategory(string category)
  {
    if (this.categoryMap.ContainsKey(category))
      return this.categoryMap[category];
    return new List<ConfigDragonKnightProfileInfo>();
  }

  public void Clear()
  {
    this.datas.Clear();
    this.dicByUniqueId.Clear();
    this.categoryMap.Clear();
  }
}
