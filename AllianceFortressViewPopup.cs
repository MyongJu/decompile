﻿// Decompiled with JetBrains decompiler
// Type: AllianceFortressViewPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceFortressViewPopup : Popup
{
  private List<AllianceFortressBenefitRenderer> m_ItemList = new List<AllianceFortressBenefitRenderer>();
  public UIScrollView m_ScrollView;
  public UIGrid m_Grid;
  public GameObject m_BenefitItemPrefab;
  private AllianceFortressViewPopup.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as AllianceFortressViewPopup.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    this.ClearData();
  }

  public void OnCloseButtonPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.AddWallFireDebuf();
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.m_Parameter.allianceId);
    if (allianceData != null)
    {
      Alliance_LevelInfo dataByLevel = ConfigManager.inst.DB_Alliance_Level.GetDataByLevel(allianceData.allianceLevel);
      this.AddBenefit(dataByLevel.benefit_name_1, dataByLevel.benefit_des_1, dataByLevel.benefit_value_des_1, "prop_resource_generation_percent");
      this.AddBenefit(dataByLevel.benefit_name_2, dataByLevel.benefit_des_2, dataByLevel.benefit_value_des_2, "prop_resource_storage_percent");
      this.AddBenefit(dataByLevel.benefit_name_3, dataByLevel.benefit_des_3, dataByLevel.benefit_value_des_3, "prop_resource_gather_percent");
    }
    this.m_Grid.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void ClearData()
  {
    int count = this.m_ItemList.Count;
    for (int index = 0; index < count; ++index)
    {
      AllianceFortressBenefitRenderer fortressBenefitRenderer = this.m_ItemList[index];
      fortressBenefitRenderer.transform.parent = (Transform) null;
      Object.Destroy((Object) fortressBenefitRenderer.gameObject);
    }
    this.m_ItemList.Clear();
  }

  private void AddBenefit(string name, string desc, string value, string benefitID)
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitID];
    GameObject gameObject = Utils.DuplicateGOB(this.m_BenefitItemPrefab, this.m_Grid.transform);
    gameObject.SetActive(true);
    gameObject.transform.localScale = Vector3.one;
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["0"] = value;
    string withPara = ScriptLocalization.GetWithPara(desc, para, true);
    AllianceFortressBenefitRenderer component = gameObject.GetComponent<AllianceFortressBenefitRenderer>();
    component.SetData(Utils.XLAT(name), withPara, dbProperty.ImagePath);
    this.m_ItemList.Add(component);
  }

  private void AddWallFireDebuf()
  {
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("burning_speed_percent");
    string title = ScriptLocalization.Get("alliance_fort_features_alliance_burning_name", true);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["0"] = (data.Value * 100.0).ToString("f2") + "%";
    string withPara = ScriptLocalization.GetWithPara("alliance_fort_features_alliance_burning_description", para, true);
    GameObject gameObject = Utils.DuplicateGOB(this.m_BenefitItemPrefab, this.m_Grid.transform);
    gameObject.SetActive(true);
    gameObject.transform.localScale = Vector3.one;
    AllianceFortressBenefitRenderer component = gameObject.GetComponent<AllianceFortressBenefitRenderer>();
    component.SetData(title, withPara, "Texture/Benefits/alliance_burn");
    this.m_ItemList.Add(component);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long allianceId;
  }
}
