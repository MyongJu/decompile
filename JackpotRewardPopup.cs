﻿// Decompiled with JetBrains decompiler
// Type: JackpotRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class JackpotRewardPopup : Popup
{
  [SerializeField]
  private ItemIconRenderer _itemIconExtra;
  [SerializeField]
  private UILabel _itemNameExtra;
  [SerializeField]
  private UILabel _itemCountExtra;
  [SerializeField]
  private UILabel _labelTitle;
  [SerializeField]
  private UILabel _labelNoWinTitle;
  [SerializeField]
  private UILabel _labelDescription;
  [SerializeField]
  private UILabel _labelNoWinDescription;
  [SerializeField]
  private GameObject _objWinRewardFrame;
  [SerializeField]
  private GameObject _objNoRewardFrame;
  [SerializeField]
  private UIButton _buttonCollect;
  [SerializeField]
  private UIButton _buttonOkay;
  [SerializeField]
  private GameObject _root;
  private System.Action _onPopupClosed;
  private int _extraItemId;
  private int _winType;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    JackpotRewardPopup.Parameter parameter = orgParam as JackpotRewardPopup.Parameter;
    if (parameter != null)
    {
      this._onPopupClosed = parameter.onPopupClosed;
      this._extraItemId = parameter.extraItemId;
      this._winType = parameter.winType;
    }
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public void OnCloseClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this._onPopupClosed == null)
      return;
    this._onPopupClosed();
  }

  private void UpdateUI()
  {
    NGUITools.SetActive(this._itemIconExtra.transform.parent.gameObject, false);
    NGUITools.SetActive(this._objWinRewardFrame, false);
    NGUITools.SetActive(this._objNoRewardFrame, false);
    NGUITools.SetActive(this._buttonCollect.gameObject, false);
    NGUITools.SetActive(this._buttonOkay.gameObject, false);
    string empty1 = string.Empty;
    Vector3 localPosition = this._root.transform.localPosition;
    string Term;
    if (this._winType == 1 || this._winType == 2)
    {
      NGUITools.SetActive(this._itemIconExtra.transform.parent.gameObject, true);
      NGUITools.SetActive(this._buttonCollect.gameObject, true);
      NGUITools.SetActive(this._objWinRewardFrame, true);
      Term = "event_jackpot_with_reward";
      this._itemIconExtra.SetData(this._extraItemId, string.Empty, true);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._extraItemId);
      if (itemStaticInfo != null)
        this._itemNameExtra.text = itemStaticInfo.LocName;
      int num;
      switch (this._winType)
      {
        case 1:
          num = 1;
          break;
        case 2:
          num = 5;
          break;
        default:
          return;
      }
      this._itemCountExtra.text = string.Format("x{0}", (object) num);
    }
    else if (this._winType == 3)
    {
      NGUITools.SetActive(this._buttonCollect.gameObject, true);
      NGUITools.SetActive(this._objWinRewardFrame, true);
      Term = "Jackpot win blablabla";
    }
    else
    {
      NGUITools.SetActive(this._buttonOkay.gameObject, true);
      NGUITools.SetActive(this._objNoRewardFrame, true);
      Term = "event_jackpot_with_no_reward";
      localPosition.y = -150f;
    }
    this._root.transform.localPosition = localPosition;
    Dictionary<string, string> para = new Dictionary<string, string>();
    ItemStaticInfo baseRewardInfo = JackpotPayload.Instance.BaseRewardInfo;
    if (baseRewardInfo != null && JackpotPayload.Instance.BaseRewardCount > 0)
    {
      para.Add("0", baseRewardInfo.LocName);
      para.Add("1", JackpotPayload.Instance.BaseRewardCount.ToString());
      UILabel labelDescription = this._labelDescription;
      string withPara = ScriptLocalization.GetWithPara(Term, para, true);
      this._labelNoWinDescription.text = withPara;
      string str = withPara;
      labelDescription.text = str;
    }
    else
    {
      UILabel labelDescription = this._labelDescription;
      string empty2 = string.Empty;
      this._labelNoWinDescription.text = empty2;
      string str = empty2;
      labelDescription.text = str;
    }
    UILabel labelTitle = this._labelTitle;
    string str1 = Utils.XLAT("event_jackpot_title");
    this._labelNoWinTitle.text = str1;
    string str2 = str1;
    labelTitle.text = str2;
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action onPopupClosed;
    public int extraItemId;
    public int winType;
  }
}
