﻿// Decompiled with JetBrains decompiler
// Type: ConfigAttribute
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Field)]
public class ConfigAttribute : Attribute
{
  public string Name { get; set; }

  public bool IsArray { get; set; }

  public int ArraySize { get; set; }

  public bool IsTimeSpan { get; set; }

  public bool CustomParse { get; set; }

  public System.Type CustomType { get; set; }

  public bool IsArrayList { get; set; }

  public bool NeedValueSort { get; set; }

  public string ParseFuncKey { get; set; }
}
