﻿// Decompiled with JetBrains decompiler
// Type: SevenDayRewardInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SevenDayRewardInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "day")]
  public int Day;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;
  [Config(Name = "shop_item_1")]
  public int SaleItem1;
  [Config(Name = "shop_item_max_1")]
  public int SaleItem1MaxCount;
  [Config(Name = "shop_item_price_1")]
  public int SaleItem1Price;
  [Config(Name = "shop_item_discount_1")]
  public float SaleItem1Discount;
  [Config(Name = "shop_item_2")]
  public int SaleItem2;
  [Config(Name = "shop_item_max_2")]
  public int SaleItem2MaxCount;
  [Config(Name = "shop_item_price_2")]
  public int SaleItem2Price;
  [Config(Name = "shop_item_discount_2")]
  public float SaleItem2Discount;
  [Config(Name = "shop_item_3")]
  public int SaleItem3;
  [Config(Name = "shop_item_max_3")]
  public int SaleItem3MaxCount;
  [Config(Name = "shop_item_price_3")]
  public int SaleItem3Price;
  [Config(Name = "shop_item_discount_3")]
  public float SaleItem3Discount;
  [Config(Name = "shop_item_4")]
  public int SaleItem4;
  [Config(Name = "shop_item_max_4")]
  public int SaleItem4MaxCount;
  [Config(Name = "shop_item_price_4")]
  public int SaleItem4Price;
  [Config(Name = "shop_item_discount_4")]
  public float SaleItem4Discount;
  private Dictionary<int, int> _saleItems;
  private Dictionary<int, int> _prices;
  private List<int> _salelist;
  private Dictionary<int, float> _discounts;

  public Dictionary<int, int> SaleItems
  {
    get
    {
      if (this._saleItems == null)
      {
        this._saleItems = new Dictionary<int, int>();
        this.AddItem(this._saleItems, this.SaleItem1, this.SaleItem1MaxCount);
        this.AddItem(this._saleItems, this.SaleItem2, this.SaleItem2MaxCount);
        this.AddItem(this._saleItems, this.SaleItem3, this.SaleItem3MaxCount);
        this.AddItem(this._saleItems, this.SaleItem4, this.SaleItem4MaxCount);
      }
      return this._saleItems;
    }
  }

  public Dictionary<int, int> Prices
  {
    get
    {
      if (this._prices == null)
      {
        this._prices = new Dictionary<int, int>();
        this.AddItem(this._prices, this.SaleItem1, this.SaleItem1Price);
        this.AddItem(this._prices, this.SaleItem2, this.SaleItem2Price);
        this.AddItem(this._prices, this.SaleItem3, this.SaleItem3Price);
        this.AddItem(this._prices, this.SaleItem4, this.SaleItem4Price);
      }
      return this._prices;
    }
  }

  public List<int> SaleItemList
  {
    get
    {
      if (this._salelist == null)
      {
        this._salelist = new List<int>();
        this.AddItem(this._salelist, this.SaleItem1, this.SaleItem1Price);
        this.AddItem(this._salelist, this.SaleItem2, this.SaleItem2Price);
        this.AddItem(this._salelist, this.SaleItem3, this.SaleItem3Price);
        this.AddItem(this._salelist, this.SaleItem4, this.SaleItem4Price);
      }
      return this._salelist;
    }
  }

  public Dictionary<int, float> Discounts
  {
    get
    {
      if (this._discounts == null)
      {
        this._discounts = new Dictionary<int, float>();
        this.AddDiscount(this._discounts, this.SaleItem1, this.SaleItem1Discount);
        this.AddDiscount(this._discounts, this.SaleItem2, this.SaleItem2Discount);
        this.AddDiscount(this._discounts, this.SaleItem3, this.SaleItem3Discount);
        this.AddDiscount(this._discounts, this.SaleItem4, this.SaleItem4Discount);
      }
      return this._discounts;
    }
  }

  private void AddDiscount(Dictionary<int, float> saleItems, int itemId, float value)
  {
    if (itemId <= 0 || (double) value <= 0.0 || saleItems.ContainsKey(itemId))
      return;
    saleItems.Add(itemId, value);
  }

  private void AddItem(Dictionary<int, int> saleItems, int itemId, int value)
  {
    if (itemId <= 0 || value <= 0 || saleItems.ContainsKey(itemId))
      return;
    saleItems.Add(itemId, value);
  }

  private void AddItem(List<int> saleItems, int itemId, int value)
  {
    if (itemId <= 0 || value <= 0)
      return;
    saleItems.Add(itemId);
  }
}
