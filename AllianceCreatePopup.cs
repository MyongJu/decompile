﻿// Decompiled with JetBrains decompiler
// Type: AllianceCreatePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UI;
using UnityEngine;

public class AllianceCreatePopup : Popup
{
  public UILabel m_NameCount;
  public UILabel m_NameTip;
  public UIInput m_NameInput;
  public UISprite m_NameStatus;
  public SpriteAnimation m_NameBusyIcon;
  public UIInput m_MessageInput;
  public UIButton m_CreateButton;
  public UIButton m_CreateGoldButton;
  public UILabel m_GoldTip;
  private bool m_NameOkay;
  private IEnumerator m_NameCoroutine;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.ResetNameTip();
    this.m_NameStatus.spriteName = string.Empty;
    this.m_NameBusyIcon.gameObject.SetActive(false);
    this.m_NameOkay = false;
    this.m_CreateButton.isEnabled = false;
    this.m_CreateGoldButton.isEnabled = false;
    this.m_NameInput.onValidate = new UIInput.OnValidate(Utils.AllianceNameValidator);
    this.m_NameInput.defaultText = ScriptLocalization.Get("alliance_name_placeholder", true);
    this.m_MessageInput.defaultText = ScriptLocalization.Get("alliance_no_motto_description", true);
    this.m_MessageInput.onValidate = new UIInput.OnValidate(this.InputValidator);
    this.UpdateCreateButton();
  }

  public void OnClosePopupClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnCreateClick()
  {
    if (IllegalWordsUtils.WarningIllegalWords(this.m_NameInput.value))
      return;
    AllianceManager.Instance.CreateAlliance(this.m_NameInput.value, string.Empty, !string.IsNullOrEmpty(this.m_MessageInput.value) ? IllegalWordsUtils.Filter(this.m_MessageInput.value) : string.Empty, Language.Instance.GetAllianceLanguage(), 0, false, "0", new System.Action<bool, object>(this.CreateAllianceCallback));
  }

  private void CreateAllianceCallback(bool ret, object data)
  {
    this.OnClosePopupClick();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    if (!ret)
      return;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    Dictionary<string, string> para = new Dictionary<string, string>();
    para["Alliance_Name"] = allianceData.allianceName;
    ToastManager.Instance.AddToast("BASIC_TOAST", (object) new ToastArgs(ConfigManager.inst.DB_Toast.GetData("toast_alliance_create"), para));
  }

  public void OnNameInputChanged()
  {
    this.m_NameBusyIcon.gameObject.SetActive(false);
    this.m_NameStatus.spriteName = string.Empty;
    this.m_NameOkay = false;
    if (this.m_NameCoroutine != null)
    {
      this.StopCoroutine(this.m_NameCoroutine);
      this.m_NameCoroutine = (IEnumerator) null;
    }
    string str = this.m_NameInput.value.Trim().Replace("[", "【").Replace("]", "】").Replace("\n", string.Empty);
    byte[] bytes = Encoding.UTF8.GetBytes(str);
    this.m_NameCount.text = string.Format("{0}/20", (object) bytes.Length);
    if (bytes.Length < 3 || bytes.Length > 20)
    {
      this.m_NameStatus.spriteName = "red_cross";
      this.SetNameError(Utils.XLAT("alliance_settings_change_name_conditions"));
    }
    else
      this.StartCoroutine(this.m_NameCoroutine = this.WaitAndCheckName(str));
  }

  [DebuggerHidden]
  private IEnumerator WaitAndCheckName(string newName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceCreatePopup.\u003CWaitAndCheckName\u003Ec__Iterator25()
    {
      newName = newName,
      \u003C\u0024\u003EnewName = newName,
      \u003C\u003Ef__this = this
    };
  }

  private void CheckNameCallback(bool ret, object data)
  {
    if (this._state.isStatesInvalid)
      return;
    this.m_NameBusyIcon.gameObject.SetActive(false);
    if (ret)
    {
      this.m_NameOkay = true;
      this.m_NameStatus.spriteName = "green_tick";
      this.ResetNameTip();
    }
    else
    {
      this.m_NameOkay = false;
      this.m_NameStatus.spriteName = "red_cross";
      this.SetNameError(Utils.XLAT("alliance_create_name_reminder"));
    }
  }

  private void ResetNameTip()
  {
    this.m_NameTip.text = ScriptLocalization.Get("alliance_name_conditions", true);
    this.m_NameTip.color = new Color(0.6941177f, 0.6941177f, 0.6941177f, 1f);
  }

  private void SetNameError(string errmsg)
  {
    this.m_NameTip.text = errmsg;
    this.m_NameTip.color = Color.red;
  }

  private void Update()
  {
    this.m_CreateButton.isEnabled = this.m_NameOkay;
    this.m_CreateGoldButton.isEnabled = this.m_NameOkay;
  }

  private void UpdateCreateButton()
  {
    bool flag = PlayerData.inst.playerCityData.level >= 6;
    this.m_CreateButton.gameObject.SetActive(flag);
    this.m_CreateGoldButton.gameObject.SetActive(!flag);
    this.m_GoldTip.gameObject.SetActive(!flag);
  }

  private char InputValidator(string text, int charIndex, char addedChar)
  {
    if (Encoding.UTF8.GetBytes(text).Length > 300 || (int) addedChar == 10)
      return char.MinValue;
    return addedChar;
  }
}
