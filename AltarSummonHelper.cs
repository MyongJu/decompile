﻿// Decompiled with JetBrains decompiler
// Type: AltarSummonHelper
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public static class AltarSummonHelper
{
  public static int GetRewardPoolSize(int itemId)
  {
    return AltarSummonHelper.GetRewardPoolSize(AltarSummonHelper.GetGroupId(itemId));
  }

  public static int GetRewardPoolSize(string groupId)
  {
    List<ConfigAltarSummonRewardInfo> rewardPoolByGroupId = ConfigManager.inst.DB_ConfigAltarSummonRewardMain.GetRewardPoolByGroupId(groupId);
    if (rewardPoolByGroupId == null)
      return 0;
    return rewardPoolByGroupId.Count;
  }

  public static string GetGroupId(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo != null)
      return itemStaticInfo.Param1;
    return string.Empty;
  }

  public static int GetRequiredSize(int itemId)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    int result = 0;
    if (itemStaticInfo != null)
      int.TryParse(itemStaticInfo.Param2, out result);
    return result;
  }

  public static ConfigAltarSummonRewardInfo GetTestData(int itemId)
  {
    return ConfigManager.inst.DB_ConfigAltarSummonRewardMain.GetRewardPoolByGroupId(AltarSummonHelper.GetGroupId(itemId))[0];
  }
}
