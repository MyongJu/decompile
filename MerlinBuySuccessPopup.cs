﻿// Decompiled with JetBrains decompiler
// Type: MerlinBuySuccessPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinBuySuccessPopup : Popup
{
  private Dictionary<int, MerlinTrialsRewardSlot> _rewardItemDict = new Dictionary<int, MerlinTrialsRewardSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UIScrollView _rewardScrollView;
  [SerializeField]
  private UITable _rewardTable;
  [SerializeField]
  private GameObject _rewardItemPrefab;
  private MerlinTrialsShopSpecificInfo _shopInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    MerlinBuySuccessPopup.Parameter parameter = orgParam as MerlinBuySuccessPopup.Parameter;
    if (parameter != null)
      this._shopInfo = parameter.shopInfo;
    this._itemPool.Initialize(this._rewardItemPrefab, this._rewardTable.gameObject);
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearReward();
  }

  public void OnClosePressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ClearReward();
    if (this._shopInfo != null && this._shopInfo.rewards != null && this._shopInfo.rewards.rewards != null)
    {
      using (Dictionary<string, int>.Enumerator enumerator = this._shopInfo.rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int result = 0;
          int count = current.Value;
          int.TryParse(current.Key, out result);
          MerlinTrialsRewardSlot rewardSlot = this.GenerateRewardSlot(result, count);
          if (!this._rewardItemDict.ContainsKey(result))
            this._rewardItemDict.Add(result, rewardSlot);
        }
      }
    }
    this.RepositionReward();
  }

  private void ClearReward()
  {
    using (Dictionary<int, MerlinTrialsRewardSlot>.Enumerator enumerator = this._rewardItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._rewardItemDict.Clear();
    this._itemPool.Clear();
  }

  private void RepositionReward()
  {
    this._rewardTable.repositionNow = true;
    this._rewardTable.Reposition();
    this._rewardScrollView.ResetPosition();
  }

  private MerlinTrialsRewardSlot GenerateRewardSlot(int itemId, int count)
  {
    GameObject gameObject = this._itemPool.AddChild(this._rewardTable.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsRewardSlot component = gameObject.GetComponent<MerlinTrialsRewardSlot>();
    component.SetData(itemId, count, false);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public MerlinTrialsShopSpecificInfo shopInfo;
  }
}
