﻿// Decompiled with JetBrains decompiler
// Type: RTMDispatchTask
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Rpc;
using System;
using System.Collections;

public class RTMDispatchTask : AsynTask
{
  public override void Execute()
  {
    ArrayList pushSystemHosts = NetApi.inst.PushSystemHosts;
    string empty = string.Empty;
    if (pushSystemHosts == null)
      return;
    if (pushSystemHosts.Count <= 0)
      return;
    try
    {
      string str = pushSystemHosts[0] as string;
      if (string.IsNullOrEmpty(str))
        return;
      NetApi.inst.RTM_URL = new TDispatchClient(str.Split(':')[0], int.Parse(str.Split(':')[1]), NetApi.inst.ConnectTimout).Get("rtmGated");
      this.IsFinish = true;
    }
    catch (Exception ex)
    {
      NetApi.inst.RTM_Status = NetApi.RTMSTATE.DispatchError;
      NetApi.inst.RTMError = ex.Message;
      this.IsFinish = true;
    }
  }
}
