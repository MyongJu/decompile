﻿// Decompiled with JetBrains decompiler
// Type: ActivityFestivalDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class ActivityFestivalDlg : UI.Dialog
{
  private ActivityFestivalDlg.Data data = new ActivityFestivalDlg.Data();
  [SerializeField]
  private ActivityFestivalDlg.Panel panel;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.data.id = (orgParam as ActivityFestivalDlg.Params).festivalActivetyInternalId;
    FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.data.id);
    if (data == null)
      return;
    this.panel.dlgTitle.text = data.Loc_outTitle;
    this.panel.title.text = Utils.XLAT(data.innerTitleId);
    this.panel.description.text = Utils.XLAT(data.innerDescriptionId);
    for (int index = 0; index < 4; ++index)
    {
      if (data.innerItems[index] != 0)
      {
        this.panel.slots[index].gameObject.SetActive(true);
        this.panel.slots[index].Setup(data.innerItems[index]);
      }
      else
        this.panel.slots[index].gameObject.SetActive(false);
    }
    this.panel.itemTable.Reposition();
    this.panel.table.repositionNow = true;
    for (int index = 0; index < 5; ++index)
      this.panel.items[index].localPosition = new Vector3(0.0f, this.panel.items[index].localPosition.y, this.panel.items[index].localPosition.z);
    this.panel.title.transform.localPosition = new Vector3(-1150f, this.panel.title.transform.localPosition.y, this.panel.title.transform.localPosition.z);
    this.panel.description.transform.localPosition = new Vector3(-1150f, this.panel.title.transform.localPosition.y, this.panel.title.transform.localPosition.z);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    for (int index = 0; index < this.panel.slots.Length; ++index)
    {
      if (this.panel.slots[index].gameObject.activeSelf)
        this.panel.slots[index].Release();
    }
  }

  public void Reset()
  {
    this.uiManagerPanel.BT_Back = this.transform.Find("Content/Btn_Full_Screen_Back").gameObject.GetComponent<UIButton>();
    this.uiManagerPanel.BT_Close = this.transform.Find("Content/Btn_Full_Screen_Close").gameObject.GetComponent<UIButton>();
    this.panel.dlgTitle = this.transform.Find("Content/Full_Screen_Panel_Titled/Label").gameObject.GetComponent<UILabel>();
    this.panel.container = this.transform.Find("Content/Panel").gameObject.transform;
    this.panel.table = this.transform.Find("Content/Panel/Table").gameObject.GetComponent<UITable>();
    this.panel.title = this.transform.Find("Content/Panel/Table/0_event_title").gameObject.GetComponent<UILabel>();
    this.panel.slots = new ActivityFestivalItemSlot[4];
    for (int index = 0; index < 4; ++index)
    {
      GameObject gameObject = this.transform.Find("Content/Panel/Table/2_items/itemSlot" + (object) index).gameObject;
      this.panel.slots[index] = gameObject.GetComponent<ActivityFestivalItemSlot>();
    }
    this.panel.description = this.transform.Find("Content/Panel/Table/4_event_description").gameObject.GetComponent<UILabel>();
    this.panel.items = new Transform[5];
    this.panel.items[0] = this.transform.Find("Content/Panel/Table/0_event_title");
    this.panel.items[1] = this.transform.Find("Content/Panel/Table/1_item_title");
    this.panel.items[2] = this.transform.Find("Content/Panel/Table/2_items");
    this.panel.items[3] = this.transform.Find("Content/Panel/Table/3_event_description_title");
    this.panel.items[4] = this.transform.Find("Content/Panel/Table/4_event_description");
  }

  protected class Data
  {
    public int id;
  }

  public class Params : UI.Dialog.DialogParameter
  {
    public int festivalActivetyInternalId;
  }

  [Serializable]
  protected class Panel
  {
    public ActivityFestivalItemSlot[] slots = new ActivityFestivalItemSlot[4];
    public const int MAX_SLOT_COUNT = 4;
    public const int ITEM_COUNT = 5;
    public UILabel dlgTitle;
    public Transform container;
    public UITable table;
    public UITable itemTable;
    public UILabel title;
    public UILabel description;
    public Transform[] items;
  }
}
