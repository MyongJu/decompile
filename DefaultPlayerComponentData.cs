﻿// Decompiled with JetBrains decompiler
// Type: DefaultPlayerComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DefaultPlayerComponentData : IComponentData
{
  public string k;
  public string x;
  public string y;
  public string user_name;
  public string acronym;
  public string portrait;
  public string icon;
  public int lordTitleId;
  public string power;
  public string city_defence;
  public string troopAmount;
  public string level;
}
