﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxWith2Buttons
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class MessageBoxWith2Buttons : Popup
{
  [SerializeField]
  private UILabel m_TitleLabel;
  [SerializeField]
  private UILabel m_ContentLabel;
  [SerializeField]
  private UILabel m_YesLabel;
  [SerializeField]
  private UILabel m_NoLabel;
  [SerializeField]
  private UIButton m_YesButton;
  [SerializeField]
  private UIButton m_NopButton;
  [SerializeField]
  private UIButton m_CloseButton;
  private MessageBoxWith2Buttons.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as MessageBoxWith2Buttons.Parameter;
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private void UpdateUI()
  {
    this.m_TitleLabel.text = this.m_Parameter.title;
    this.m_ContentLabel.text = this.m_Parameter.content;
    this.m_YesLabel.text = this.m_Parameter.yes;
    this.m_NoLabel.text = this.m_Parameter.no;
  }

  public void OnYes()
  {
    if (this.m_Parameter.yesCallback != null)
      this.m_Parameter.yesCallback();
    this.OnClose();
  }

  public void OnNo()
  {
    if (this.m_Parameter.noCallback != null)
      this.m_Parameter.noCallback();
    this.OnClose();
  }

  public void OnClose()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string title;
    public string content;
    public string yes;
    public string no;
    public System.Action yesCallback;
    public System.Action noCallback;
  }
}
