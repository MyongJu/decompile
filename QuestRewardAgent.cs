﻿// Decompiled with JetBrains decompiler
// Type: QuestRewardAgent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;

public class QuestRewardAgent
{
  private string _maxResouceIcon = string.Empty;
  private string _silverIcon = string.Empty;
  private List<QuestReward> _rewards;
  private bool foodRewardMax;
  private bool woodRewardMax;
  private bool oreRewardMax;
  private bool sliverRewardMax;
  private bool hadFoodReward;
  private bool hadWoodReward;
  private bool hadOreReward;
  private bool hadSilverReward;
  private CityManager.ResourceTypes _maxResourceType;
  private int _maxRewardCount;
  private int _silverRewardCount;
  private long questId;

  public QuestRewardAgent(long questId)
  {
    this.questId = questId;
    this.InitMaxReward();
  }

  public int SilverRewardCount
  {
    get
    {
      return this._silverRewardCount;
    }
  }

  public string SilverIcon
  {
    get
    {
      return this._silverIcon;
    }
  }

  public int MaxRewardCount
  {
    get
    {
      return this._maxRewardCount;
    }
  }

  public string MaxResouceIcon
  {
    get
    {
      return this._maxResouceIcon;
    }
  }

  private QuestData2 Quest
  {
    get
    {
      return DBManager.inst.DB_Quest.Get(this.questId);
    }
  }

  private TimerQuestData TimeQuest
  {
    get
    {
      return DBManager.inst.DB_Local_TimerQuest.Get(this.questId);
    }
  }

  public CityManager.ResourceTypes MaxResourceType
  {
    get
    {
      return this._maxResourceType;
    }
  }

  public List<QuestReward> Rewards
  {
    get
    {
      if (this._rewards == null)
      {
        int questId = (int) this.questId;
        if (this.Quest == null && this.TimeQuest != null)
          questId = this.TimeQuest.internalId;
        this._rewards = QuestReward.CreateReward(questId);
      }
      return this._rewards;
    }
  }

  private void InitMaxReward()
  {
    List<QuestReward>.Enumerator enumerator = this.Rewards.GetEnumerator();
    while (enumerator.MoveNext())
    {
      QuestReward current = enumerator.Current;
      if (current is ResourceReward)
      {
        ResourceReward resourceReward = current as ResourceReward;
        if (this._maxRewardCount < resourceReward.value)
        {
          this._maxRewardCount = resourceReward.value;
          this._maxResourceType = resourceReward.type;
        }
        switch (resourceReward.type)
        {
          case CityManager.ResourceTypes.FOOD:
            this.hadFoodReward = true;
            continue;
          case CityManager.ResourceTypes.WOOD:
            this.hadWoodReward = true;
            continue;
          case CityManager.ResourceTypes.ORE:
            this.hadOreReward = true;
            continue;
          default:
            continue;
        }
      }
      else if (current is SilverReward)
      {
        this.hadSilverReward = true;
        this._silverRewardCount = current.GetValue();
        this._silverIcon = "silverboost";
      }
    }
    this.foodRewardMax = false;
    this.woodRewardMax = false;
    this.oreRewardMax = false;
    this.sliverRewardMax = false;
    switch (this._maxResourceType)
    {
      case CityManager.ResourceTypes.FOOD:
        this.foodRewardMax = true;
        this._maxResouceIcon = "foodboost";
        break;
      case CityManager.ResourceTypes.SILVER:
        this._maxResouceIcon = "silverboost";
        this.sliverRewardMax = true;
        break;
      case CityManager.ResourceTypes.WOOD:
        this._maxResouceIcon = "woodboost";
        this.woodRewardMax = true;
        break;
      case CityManager.ResourceTypes.ORE:
        this._maxResouceIcon = "oreboost";
        this.oreRewardMax = true;
        break;
    }
  }

  public bool HadFoodReward()
  {
    return this.hadFoodReward;
  }

  public bool HadWoodReward()
  {
    return this.hadWoodReward;
  }

  public bool HadOreReward()
  {
    return this.hadOreReward;
  }

  public bool HadSilverReward()
  {
    return this.hadSilverReward;
  }

  public bool FoodRewardMax()
  {
    return this.foodRewardMax;
  }

  public bool WoodRewardMax()
  {
    return this.woodRewardMax;
  }

  public bool OreRewardMax()
  {
    return this.oreRewardMax;
  }

  public bool SilverRewardMax()
  {
    return this.sliverRewardMax;
  }
}
