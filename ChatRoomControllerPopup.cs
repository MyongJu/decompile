﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomControllerPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class ChatRoomControllerPopup : Popup
{
  public const string POPUP_TYPE = "Chat/ChatRoomControllerPopUp";
  public ChatRoomControler chatRoomController;
  private long roomID;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.roomID = (orgParam as ChatRoomControllerPopup.Parameter).roomId;
    this.chatRoomController.Setup(this.roomID);
    this.AddEventListener();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventListener();
  }

  public void AddEventListener()
  {
    DBManager.inst.DB_room.onDataChanged += new System.Action<long>(this.OnRoomDataChanged);
    DBManager.inst.DB_room.onDataRemove += new System.Action<long>(this.OnRoomRemoved);
    DBManager.inst.DB_room.onMemberChanged += new System.Action<long, long>(this.OnRoomMemberDataChanged);
    DBManager.inst.DB_room.onMemberRemoved += new System.Action<long>(this.OnRoomDataChanged);
  }

  public void RemoveEventListener()
  {
    DBManager.inst.DB_room.onDataChanged -= new System.Action<long>(this.OnRoomDataChanged);
    DBManager.inst.DB_room.onDataRemove -= new System.Action<long>(this.OnRoomRemoved);
    DBManager.inst.DB_room.onMemberChanged -= new System.Action<long, long>(this.OnRoomMemberDataChanged);
    DBManager.inst.DB_room.onMemberRemoved -= new System.Action<long>(this.OnRoomDataChanged);
  }

  public void OnRoomDataChanged(long roomId)
  {
    if (roomId != this.roomID)
      return;
    this.chatRoomController.Setup(roomId);
  }

  public void OnRoomMemberDataChanged(long roomId, long uid)
  {
    if (roomId != this.roomID)
      return;
    this.chatRoomController.Setup(roomId);
  }

  private void OnRoomRemoved(long roomId)
  {
    if (roomId != this.roomID)
      return;
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long roomId;
  }
}
