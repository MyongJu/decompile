﻿// Decompiled with JetBrains decompiler
// Type: ContactBlockBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using UI;
using UnityEngine;

public class ContactBlockBar : MonoBehaviour
{
  private long _uid;
  [SerializeField]
  private ContactBlockBar.Panel panel;

  public void Setup(long uid)
  {
    this._uid = uid;
    UserData userData = DBManager.inst.DB_User.Get(uid);
    if (userData != null)
    {
      CustomIconLoader.Instance.requestCustomIcon(this.panel.userIcon, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
      LordTitlePayload.Instance.ApplyUserAvator(this.panel.userIcon, userData.LordTitle, 1);
      this.panel.userName.text = userData.userName_Kingdom_Alliance_Name;
    }
    this.panel.BT_Remove.enabled = true;
  }

  public void OnRemoveButtonClick()
  {
    ContactManager.UnblockUser(this._uid, (System.Action<bool, object>) null);
    this.panel.BT_Remove.enabled = false;
  }

  public void OnPlayerInfoButtonClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlgDetail", (UI.Dialog.DialogParameter) new PlayerProfileDlgDetail.Parameter()
    {
      uid = this._uid
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  private void RemoveBlockCallback(bool result, object orgData)
  {
    if (!result)
      return;
    this.panel.BT_Remove.enabled = false;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture userIcon;
    public UILabel userName;
    public UISprite alliaceIcon;
    public UIButton BT_Remove;
  }
}
