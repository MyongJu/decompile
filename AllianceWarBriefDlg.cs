﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarBriefDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarBriefDlg : Dialog
{
  private AllianceWarRankData _allianceWarRankData = new AllianceWarRankData();
  private List<AllianceWarRankItem> _allRankItem = new List<AllianceWarRankItem>();
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _contentContainer;
  [SerializeField]
  private AllianceWarRankItem _allianceWarRankItemTemplate;
  [SerializeField]
  private UILabel _labelKingdom;
  [SerializeField]
  private UILabel _labelStatus;
  [SerializeField]
  private UILabel _labelMyScore;
  [SerializeField]
  private UIButton _buttonBackHome;
  [SerializeField]
  private Camera _camera;
  [SerializeField]
  private UIViewport _viewport;
  [SerializeField]
  private GameObject _rewardTipGo;
  [SerializeField]
  private UILabel _rewardTipCount;

  public override void OnOpen(UIControler.UIParameter orgParam = null)
  {
    base.OnOpen(orgParam);
    this._viewport.sourceCamera = UIManager.inst.ui2DCamera;
    this._camera.gameObject.SetActive(true);
    this._allianceWarRankItemTemplate.gameObject.SetActive(false);
    Hashtable postData = new Hashtable();
    RequestManager.inst.SendRequest("AC:getScoreRank", postData, new System.Action<bool, object>(this.OnAllianceRankLoaded), true);
    AllianceWarPayload.Instance.LoadUserScoreRewardInfo(new System.Action(this.OnUserScoreUpdate));
    this.UpdateUI();
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondTick);
  }

  public override void OnClose(UIControler.UIParameter orgParam = null)
  {
    base.OnClose(orgParam);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondTick);
  }

  protected void OnAllianceRankLoaded(bool result, object data)
  {
    if (result)
      this._allianceWarRankData.Decode(data);
    this.UpdateUI();
  }

  protected AllianceWarRankItem CreateRankItem()
  {
    GameObject gameObject = NGUITools.AddChild(this._contentContainer.gameObject, this._allianceWarRankItemTemplate.gameObject);
    gameObject.SetActive(true);
    AllianceWarRankItem component = gameObject.GetComponent<AllianceWarRankItem>();
    this._allRankItem.Add(component);
    return component;
  }

  protected void DestroyAllRankItem()
  {
    using (List<AllianceWarRankItem>.Enumerator enumerator = this._allRankItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceWarRankItem current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current) && (bool) ((UnityEngine.Object) current.gameObject))
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) current.gameObject);
      }
    }
    this._allRankItem.Clear();
  }

  protected void UpdateUI()
  {
    this.DestroyAllRankItem();
    string str1 = string.Empty;
    using (List<AllianceWarRankItemData>.Enumerator enumerator = this._allianceWarRankData.AllRankItemData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceWarRankItemData current = enumerator.Current;
        AllianceWarRankItem rankItem = this.CreateRankItem();
        rankItem.SetData(this._allianceWarRankData, current, this._contentContainer);
        if (current.Kingdom != PlayerData.inst.userData.world_id)
          rankItem.SetGotoButtonEnabled(true);
        else
          rankItem.SetGotoButtonEnabled(false);
        if (PlayerData.inst.userData.IsForeigner && current.Kingdom == PlayerData.inst.userData.current_world_id)
          str1 = current.AllianceName;
      }
    }
    if (!string.IsNullOrEmpty(str1))
      str1 = ScriptLocalization.GetWithPara("toast_alliance_warfare_target_alliance", new Dictionary<string, string>()
      {
        {
          "0",
          str1
        }
      }, true);
    this._labelMyScore.text = ScriptLocalization.GetWithPara("toast_alliance_warfare_personal_score", new Dictionary<string, string>()
    {
      {
        "0",
        this._allianceWarRankData.Score.ToString()
      }
    }, true);
    string str2 = ScriptLocalization.GetWithPara("alliance_warfare_current_kingdom_name", new Dictionary<string, string>()
    {
      {
        "0",
        "#" + (object) PlayerData.inst.userData.current_world_id
      }
    }, true);
    if (!string.IsNullOrEmpty(str1))
      str2 = string.Format("{0}[ff0000ff]({1})[-]", (object) str2, (object) str1);
    this._labelKingdom.text = str2;
    this._contentContainer.Reposition();
    this._scrollView.ResetPosition();
    this.UpdateStatus();
  }

  public void OnButtonGoBackHomeClicked()
  {
    if (PlayerData.inst.userData.IsForeigner)
    {
      AllianceWarPayload.Instance.GotoBack();
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }
    else
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_alliance_warfare_return_inmotherland", true), (System.Action) null, 4f, true);
  }

  public void OnButtonCloseClicked()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnSecondTick(int delta)
  {
    this.UpdateStatus();
  }

  protected void UpdateStatus()
  {
    if ((long) NetServerTime.inst.ServerTimestamp < (long) this._allianceWarRankData.StartTime)
      this._labelStatus.text = ScriptLocalization.GetWithPara("alliance_warfare_preparing_status", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatTime(this._allianceWarRankData.StartTime - NetServerTime.inst.ServerTimestamp, true, true, true)
        }
      }, true);
    else
      this._labelStatus.text = ScriptLocalization.GetWithPara("alliance_warfare_opened_status", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatTime(PlayerData.inst.userData.AcEndTime - NetServerTime.inst.ServerTimestamp, true, true, true)
        }
      }, true);
  }

  public void OnScoreRewardClicked()
  {
    UIManager.inst.OpenPopup("AllianceWar/AllianceWarScoreRewardPopup", (Popup.PopupParameter) null);
  }

  private void OnUserScoreUpdate()
  {
    long userScore = AllianceWarPayload.Instance.UserScore;
    List<AllianceWarScoreRewardInfo> rewardInfoList = ConfigManager.inst.DB_AllianceWarScoreReward.RewardInfoList;
    int num = 0;
    List<string> cliamedRewardList = AllianceWarPayload.Instance.CliamedRewardList;
    using (List<AllianceWarScoreRewardInfo>.Enumerator enumerator = rewardInfoList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceWarScoreRewardInfo current = enumerator.Current;
        if (userScore > current.ScoreReq && !cliamedRewardList.Contains(current.ID))
          ++num;
      }
    }
    this._rewardTipGo.SetActive(num > 0);
    this._rewardTipCount.text = num.ToString();
  }
}
