﻿// Decompiled with JetBrains decompiler
// Type: CasinoPickCardDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class CasinoPickCardDlg : UI.Dialog
{
  private int currentChestId = -1;
  private int totalMultiplier = 1;
  private float tipShowOffsetX = 0.65f;
  private bool isCardPicked = true;
  private ArrayList openedCellList = new ArrayList();
  public Animator shuffleAnim;
  public UILabel runestone;
  public UIButton shuffleButton;
  public UIButton shareButton;
  public UIButton giveupButton;
  public UILabel giveupBtnText;
  public GameObject tipItem;
  public UILabel tipName;
  public UILabel tipDetails;
  public CasinoPickCardDlg.CardItemInfo[] cardItemInfo;
  private int clickedCount;
  private int currentOpenedCell;
  private long needRunestoneCount;
  private long currentRunestoneCount;
  private bool canShuffle;
  private CasinoChestInfo chestItemInfo;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.currentChestId = -1;
    this.runestone.text = string.Empty;
    this.openedCellList.Clear();
    this.clickedCount = 0;
    this.totalMultiplier = 1;
    this.isCardPicked = true;
    this.currentRunestoneCount = PlayerData.inst.userData.dragon_coin;
    this.AddEventHandler();
    CasinoPickCardDlg.Parameter parameter = orgParam as CasinoPickCardDlg.Parameter;
    if (parameter != null)
    {
      this.currentChestId = parameter.hitChestId;
      if (parameter.openedCells != null)
        this.openedCellList = parameter.openedCells;
      this.canShuffle = parameter.canShuffle;
    }
    else
      this.CheckCardShuffledStatus();
    if (this.canShuffle && (UnityEngine.Object) this.shuffleAnim != (UnityEngine.Object) null)
    {
      this.shuffleAnim.Play("PreShuffle", -1, 0.0f);
      this.shuffleAnim.Update(0.0f);
    }
    if (parameter == null)
      return;
    this.ShowCardItemDataDetails();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.clickedCount = 0;
    this.totalMultiplier = 1;
    this.canShuffle = false;
    if ((UnityEngine.Object) this.shuffleAnim != (UnityEngine.Object) null)
    {
      this.shuffleAnim.Play("Empty", -1, 0.0f);
      this.shuffleAnim.Update(0.0f);
    }
    this.UpdateFrameInitStatus();
    this.RemoveEventHandler();
  }

  public void OnCardRewardItemPressed(int clickedIndex)
  {
    if (!this.CheckRunestoneCount())
      return;
    this.isCardPicked = false;
    this.shuffleAnim.Play("Empty", -1, 0.0f);
    this.shuffleAnim.Update(0.0f);
    AudioManager.Instance.StopAndPlaySound("sfx_city_casino_turn");
    this.ShowClickedRewardItem(clickedIndex);
  }

  public void OnShareBtnPressed()
  {
    UIManager.inst.OpenPopup("Casino/ConfirmationPopup", (Popup.PopupParameter) new CasinoConfirmationPopup.Parameter()
    {
      type = 0
    });
  }

  public void OnBackBtnPressed()
  {
    this.OnGiveupBtnPressed();
  }

  public void OnGiveupBtnPressed()
  {
    if (this.clickedCount == this.cardItemInfo.Length)
    {
      MessageHub.inst.GetPortByAction("casino:closeChest").SendRequest((Hashtable) null, (System.Action<bool, object>) null, true);
      UIManager.inst.OpenDlg("Casino/CasinoDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    }
    else
      UIManager.inst.OpenPopup("Casino/ConfirmationPopup", (Popup.PopupParameter) new CasinoConfirmationPopup.Parameter()
      {
        type = 1
      });
  }

  public void OnShuffleBtnPressed()
  {
    this.DisableHighlightEffect();
    if ((UnityEngine.Object) this.shuffleAnim != (UnityEngine.Object) null)
    {
      this.shuffleAnim.Play("Shuffle", -1, 0.0f);
      this.shuffleAnim.Update(0.0f);
    }
    this.tipItem.SetActive(false);
    this.shuffleButton.gameObject.SetActive(false);
    this.shareButton.gameObject.SetActive(true);
    AudioManager.Instance.StopAndPlaySound("sfx_city_casino_shuffle");
    this.UpdateCardShuffleStatus();
  }

  public void OnItemPressHandler(int clickId)
  {
    if (clickId > this.cardItemInfo.Length - 1)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.cardItemInfo[clickId].itemId);
    if (itemStaticInfo == null)
      return;
    Utils.DelayShowTip(itemStaticInfo.internalId, this.cardItemInfo[clickId].texture.transform, 0L, 0L, 0);
  }

  public void OnItemReleaseHandler()
  {
    Utils.StopShowItemTip();
  }

  public void OnDisplayedIconPressed(int clickedId)
  {
    if (this.clickedCount > 0)
      return;
    this.ShowClickedItemTipInfo(clickedId);
    if (clickedId >= this.cardItemInfo.Length)
      return;
    Vector3 position1 = this.cardItemInfo[clickedId].texture.transform.position;
    Vector3 position2 = this.tipItem.transform.position;
    this.tipItem.transform.position = new Vector3(position1.x - this.tipShowOffsetX, position1.y, position2.z);
  }

  public void ShowClickedItemTipInfo(int clickedIndex)
  {
    if (clickedIndex > this.cardItemInfo.Length - 1)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.cardItemInfo[clickedIndex].itemId);
    this.tipName.text = "~";
    this.tipDetails.text = "~";
    if (itemStaticInfo != null)
    {
      Utils.ShowItemTip(itemStaticInfo.internalId, this.cardItemInfo[clickedIndex].texture.transform, 0L, 0L, 0);
    }
    else
    {
      if (this.cardItemInfo[clickedIndex].type.Contains("item"))
        return;
      this.tipName.text = this.cardItemInfo[clickedIndex].type;
      this.tipDetails.text = Utils.XLAT("tavern_cards_rewards_multiply_description");
    }
  }

  public void ShowCardItemDataDetails()
  {
    this.ResetCardsDepthByRandom();
    if (this.currentChestId == -1)
      MessageHub.inst.GetPortByAction("casino:getChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        Hashtable inData = data as Hashtable;
        if (inData == null || inData[(object) "chest_id"] == null)
          return;
        DatabaseTools.UpdateData(inData, "chest_id", ref this.currentChestId);
        DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out this.openedCellList);
        this.ShowAllCardItemData();
      }), true);
    else
      this.ShowAllCardItemData();
    this.ShowMainContent();
    this.UpdataNPC();
    this.UpdateAllCostText();
  }

  public void ShowAllCardItemData()
  {
    if (this.canShuffle)
    {
      this.shuffleButton.gameObject.SetActive(true);
      this.shareButton.gameObject.SetActive(false);
    }
    else
    {
      this.shuffleButton.gameObject.SetActive(false);
      this.shareButton.gameObject.SetActive(true);
    }
    this.chestItemInfo = ConfigManager.inst.DB_CasinoChest.GetItem(this.currentChestId);
    if (this.chestItemInfo == null)
      return;
    ConfigManager.inst.DB_CasinoChest.GetCasinoChestInfoList();
    Dictionary<int, CasinoChestInfo.RewardItemInfo> rewardItemInfo = this.chestItemInfo.GetRewardItemInfo();
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      CasinoPickCardDlg.CardItemInfo cardItemInfo = this.cardItemInfo[index];
      string path = string.Empty;
      if (cardItemInfo != null && rewardItemInfo[index] != null)
      {
        cardItemInfo.itemId = -1;
        cardItemInfo.type = rewardItemInfo[index].type;
        if (rewardItemInfo[index].type == "item")
        {
          int internalId = !string.IsNullOrEmpty(rewardItemInfo[index].reward) ? int.Parse(rewardItemInfo[index].reward) : -1;
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          cardItemInfo.itemId = internalId;
          if (itemStaticInfo != null)
            path = itemStaticInfo.ImagePath;
        }
        else
          path = "Texture/ItemIcons/icon_casino_" + rewardItemInfo[index].type.ToLower();
        if (!string.IsNullOrEmpty(path))
        {
          BuilderFactory.Instance.HandyBuild((UIWidget) cardItemInfo.texture, path, (System.Action<bool>) null, true, false, string.Empty);
          if (rewardItemInfo[index].type != "item")
            this.ResetTexture(cardItemInfo.texture);
        }
        if (this.chestItemInfo.highlightIndex == index + 1 && this.canShuffle)
          CasinoFunHelper.Instance.PlayParticles(cardItemInfo.highlight);
      }
    }
    this.UpdateCellFrameStatus();
  }

  public void UpdateCellFrameStatus()
  {
    if (this.openedCellList != null && this.openedCellList.Count <= 0 || this.openedCellList == null)
      return;
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      CasinoPickCardDlg.CardItemInfo cardItemInfo = this.cardItemInfo[index];
      cardItemInfo.sprite.enabled = true;
      cardItemInfo.label.enabled = true;
      CasinoFunHelper.Instance.StopParticles(cardItemInfo.effects);
      CasinoFunHelper.Instance.StopParticles(cardItemInfo.highlight);
    }
    for (int index = 0; index < this.openedCellList.Count; ++index)
    {
      Hashtable openedCell = this.openedCellList[index] as Hashtable;
      int outData1 = -1;
      int outData2 = -1;
      DatabaseTools.UpdateData(openedCell, "open_index", ref outData1);
      DatabaseTools.UpdateData(openedCell, "config_index", ref outData2);
      if (outData1 != -1 && outData2 != -1)
        this.ShowClickedRewardItemDetails(outData1 - 1, outData2 - 1);
    }
  }

  public void OnCurrencyBtnPressed()
  {
    UIManager.inst.OpenDlg("Casino/CasinoStoreDlg", (UI.Dialog.DialogParameter) new CasinoStoreDlg.Parameter()
    {
      displayType = 1
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnRewardRecordBtnPressed()
  {
    this.tipItem.SetActive(false);
    MessageHub.inst.GetPortByAction("casino:getChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      int outData = -1;
      ArrayList data1 = new ArrayList();
      if (inData == null || inData[(object) "chest_id"] == null)
        return;
      DatabaseTools.UpdateData(inData, "chest_id", ref outData);
      DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out data1);
      UIManager.inst.OpenPopup("Casino/CardHistoryPopup", (Popup.PopupParameter) new CasinoCardHistoryPopup.Parameter()
      {
        uid = PlayerData.inst.uid,
        chestId = outData,
        openedCellList = data1
      });
    }), true);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged += new System.Action<long>(this.OnUerDataChange);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataChanged -= new System.Action<long>(this.OnUerDataChange);
  }

  private void OnUerDataChange(long user_id)
  {
    if (user_id != PlayerData.inst.uid)
      return;
    this.runestone.text = PlayerData.inst.userData.dragon_coin.ToString();
  }

  private bool CheckRunestoneCount()
  {
    if (PlayerData.inst.userData.dragon_coin >= this.needRunestoneCount)
      return true;
    UIManager.inst.OpenPopup("Casino/GetMorePopup", (Popup.PopupParameter) new CasinoGetMorePopup.Parameter()
    {
      type = "casino2"
    });
    return false;
  }

  private void ShowClickedRewardItem(int clickedIndex)
  {
    MessageHub.inst.GetPortByAction("casino:openCell").SendRequest(new Hashtable()
    {
      {
        (object) "cellIndex",
        (object) (clickedIndex + 1)
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      if (inData == null || inData[(object) "config_index"] == null)
        return;
      DatabaseTools.UpdateData(inData, "config_index", ref this.currentOpenedCell);
      Hashtable hashtable = new Hashtable()
      {
        {
          (object) "open_index",
          (object) (clickedIndex + 1)
        },
        {
          (object) "config_index",
          (object) this.currentOpenedCell
        }
      };
      this.ShowClickedRewardItemDetails(clickedIndex, this.currentOpenedCell - 1);
    }), true);
  }

  private void ShowClickedRewardItemDetails(int clickedIndex, int hitIndex)
  {
    if (hitIndex < 0 || hitIndex > this.cardItemInfo.Length - 1)
      return;
    int index = hitIndex;
    string str = string.Empty;
    CasinoChestInfo.RewardItemInfo rewardItemInfo = this.chestItemInfo.GetRewardItemInfo()[index];
    CasinoPickCardDlg.CardItemInfo cardItemInfo = this.cardItemInfo[clickedIndex];
    if (cardItemInfo != null)
    {
      cardItemInfo.sprite.enabled = false;
      cardItemInfo.label.enabled = false;
      this.shareButton.gameObject.SetActive(true);
      this.shuffleButton.gameObject.SetActive(false);
      cardItemInfo.sprite.gameObject.GetComponent<UIButton>().enabled = false;
      if (!this.isCardPicked)
        CasinoFunHelper.Instance.PlayParticles(cardItemInfo.effects);
      cardItemInfo.clickedEffectAnim.Stop();
      cardItemInfo.clickedEffectAnim.Play();
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(!string.IsNullOrEmpty(rewardItemInfo.reward) ? int.Parse(rewardItemInfo.reward) : -1);
      if (itemStaticInfo != null)
      {
        str = itemStaticInfo.ImagePath;
        cardItemInfo.itemId = itemStaticInfo.internalId;
      }
      if (!rewardItemInfo.type.Contains("item"))
      {
        CasinoFunHelper.Instance.PlayParticles(cardItemInfo.multiplier);
        this.totalMultiplier *= int.Parse(rewardItemInfo.type.ToLower().Replace("x", string.Empty));
        str = "Texture/ItemIcons/icon_casino_" + rewardItemInfo.type.ToLower();
      }
      else
      {
        if (this.isCardPicked)
          this.totalMultiplier = 1;
        this.DisableAllMultiplierEffect();
      }
      if (!string.IsNullOrEmpty(str))
      {
        BuilderFactory.Instance.HandyBuild((UIWidget) cardItemInfo.texture, str, (System.Action<bool>) null, true, false, string.Empty);
        if (!rewardItemInfo.type.Contains("item"))
          this.ResetTexture(cardItemInfo.texture);
        if (rewardItemInfo.type.Contains("item") && !this.isCardPicked)
          this.ShowItemCollectedEffect(str);
      }
      if (this.chestItemInfo.highlightIndex == index + 1)
        CasinoFunHelper.Instance.PlayParticles(cardItemInfo.highlight);
    }
    ++this.clickedCount;
    this.UpdateAllCostText();
  }

  private void ShowItemCollectedEffect(string texturePath)
  {
    RewardsCollectionAnimator.Instance.Clear();
    ItemRewardInfo.Data data = new ItemRewardInfo.Data();
    data.icon = texturePath;
    data.count = (float) this.totalMultiplier;
    this.totalMultiplier = 1;
    RewardsCollectionAnimator.Instance.items.Add(data);
    RewardsCollectionAnimator.Instance.CollectItems(false);
  }

  private void ResetTexture(UITexture texture)
  {
    AdvancedItemIcon componentInChildren = texture.gameObject.GetComponentInChildren<AdvancedItemIcon>();
    if (!(bool) ((UnityEngine.Object) componentInChildren))
      return;
    NGUITools.Destroy((UnityEngine.Object) componentInChildren.gameObject);
  }

  private void DisableAllClickedEffect()
  {
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
      CasinoFunHelper.Instance.StopParticles(this.cardItemInfo[index].effects);
  }

  private void DisableAllMultiplierEffect()
  {
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
      CasinoFunHelper.Instance.StopParticles(this.cardItemInfo[index].multiplier);
  }

  private void DisableHighlightEffect()
  {
    if (this.chestItemInfo == null || this.chestItemInfo.highlightIndex <= 0 || this.chestItemInfo.highlightIndex > this.cardItemInfo.Length)
      return;
    CasinoFunHelper.Instance.StopParticles(this.cardItemInfo[this.chestItemInfo.highlightIndex - 1].highlight);
  }

  private void UpdateFrameInitStatus()
  {
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      CasinoPickCardDlg.CardItemInfo cardItemInfo = this.cardItemInfo[index];
      cardItemInfo.sprite.enabled = true;
      cardItemInfo.label.enabled = true;
      cardItemInfo.sprite.gameObject.SetActive(true);
      cardItemInfo.label.gameObject.SetActive(true);
      cardItemInfo.sprite.transform.localRotation = Quaternion.identity;
      cardItemInfo.sprite.gameObject.GetComponent<UIButton>().enabled = true;
    }
  }

  private void UpdateAllCostText()
  {
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      if (this.clickedCount == 0)
      {
        this.needRunestoneCount = 0L;
        this.cardItemInfo[index].label.color = Color.white;
        this.cardItemInfo[index].label.text = Utils.XLAT("id_uppercase_free");
      }
      else if (this.clickedCount > 0 && this.clickedCount < this.cardItemInfo.Length)
      {
        this.needRunestoneCount = (long) Mathf.Pow(2f, (float) (this.clickedCount - 1));
        this.cardItemInfo[index].label.text = this.needRunestoneCount.ToString();
        if (PlayerData.inst.userData.dragon_coin < this.needRunestoneCount)
          this.cardItemInfo[index].label.color = Color.red;
        else
          this.cardItemInfo[index].label.color = Color.white;
      }
    }
    this.giveupBtnText.text = this.clickedCount != this.cardItemInfo.Length ? Utils.XLAT("tavern_uppercase_give_up") : Utils.XLAT("tavern_uppercase_go_back");
    this.runestone.text = PlayerData.inst.userData.dragon_coin.ToString();
  }

  private void UpdateShareBtnStatus(bool enable)
  {
    this.shareButton.isEnabled = enable;
    this.shareButton.UpdateColor(true);
  }

  private void ShowMainContent()
  {
    Transform transform = this.transform.Find("CasinoPickCard/FrontPanel/PremiumCurrencyBtn/icon");
    if (!((UnityEngine.Object) transform != (UnityEngine.Object) null))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!(bool) ((UnityEngine.Object) component))
      return;
    MarksmanPayload.Instance.FeedMarksmanTexture(component, "Texture/ItemIcons/item_casino_rune_stone_tiny");
  }

  private void UpdataNPC()
  {
    Transform transform = this.transform.Find("CasinoPickCard/npc");
    if (!((UnityEngine.Object) transform != (UnityEngine.Object) null))
      return;
    UITexture component = transform.GetComponent<UITexture>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    Utils.SetPortrait(component, "Texture/STATIC_TEXTURE/tutorial_npc_03");
  }

  private int SortByRandom(int a, int b)
  {
    int[] numArray = new int[3]{ -1, 0, 1 };
    int index = UnityEngine.Random.Range(0, numArray.Length);
    return numArray[index];
  }

  private void ResetCardsDepthByRandom()
  {
    List<int> intList = new List<int>()
    {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8
    };
    intList.Sort(new Comparison<int>(this.SortByRandom));
    int num1 = 0;
    for (int index = 0; index < this.cardItemInfo.Length; ++index)
    {
      CasinoPickCardDlg.CardItemInfo cardItemInfo = this.cardItemInfo[index];
      if (cardItemInfo != null && index < intList.Count)
      {
        int num2 = intList[index] * 4;
        cardItemInfo.bkgSprite.depth = num2;
        int num3;
        cardItemInfo.texture.depth = num3 = num2 + 1;
        int num4;
        cardItemInfo.sprite.depth = (num4 = num3 + 1) + 4;
        cardItemInfo.label.depth = (num1 = num4 + 1) + 4;
      }
    }
  }

  private void UpdateCardShuffleStatus()
  {
    MessageHub.inst.GetPortByAction("casino:shuffle").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.canShuffle = false;
    }), true);
  }

  private void CheckCardShuffledStatus()
  {
    MessageHub.inst.GetPortByAction("casino:getChest").SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable inData = data as Hashtable;
      int outData = -1;
      if (inData == null || inData[(object) "chest_id"] == null)
        return;
      DatabaseTools.UpdateData(inData, "chest_id", ref this.currentChestId);
      DatabaseTools.UpdateData(inData, "shuffle", ref outData);
      DatabaseTools.CheckAndParseOrgData(inData[(object) "opened"], out this.openedCellList);
      this.canShuffle = outData != 1;
      if (this.canShuffle && (UnityEngine.Object) this.shuffleAnim != (UnityEngine.Object) null)
      {
        this.shuffleAnim.Play("PreShuffle", -1, 0.0f);
        this.shuffleAnim.Update(0.0f);
        this.shuffleButton.gameObject.SetActive(true);
        this.shareButton.gameObject.SetActive(false);
      }
      else
      {
        this.shuffleButton.gameObject.SetActive(false);
        this.shareButton.gameObject.SetActive(true);
      }
      this.ShowCardItemDataDetails();
    }), true);
  }

  [Serializable]
  public class CardItemInfo
  {
    [HideInInspector]
    public int itemId;
    [HideInInspector]
    public string type;
    public UITexture texture;
    public UISprite sprite;
    public UISprite bkgSprite;
    public UILabel label;
    public GameObject effects;
    public GameObject highlight;
    public GameObject multiplier;
    public UnityEngine.Animation clickedEffectAnim;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int hitChestId;
    public ArrayList openedCells;
    public bool canShuffle;
  }
}
