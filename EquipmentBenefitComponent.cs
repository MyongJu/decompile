﻿// Decompiled with JetBrains decompiler
// Type: EquipmentBenefitComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentBenefitComponent : ComponentRenderBase
{
  public UITexture icon;
  public UILabel descriptionLabel;
  public UILabel valueLabel;

  public override void Init()
  {
    EquipmentBenefitComponentData data = this.data as EquipmentBenefitComponentData;
    this.descriptionLabel.text = data.element.propertyDefinition.Name;
    this.valueLabel.text = data.element.propertyDefinition.ConvertToDisplayString((double) data.element.value, true, true);
    BuilderFactory.Instance.Build((UIWidget) this.icon, data.element.propertyDefinition.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.icon);
  }
}
