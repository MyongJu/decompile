﻿// Decompiled with JetBrains decompiler
// Type: LoaderInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class LoaderInfo
{
  private int _maxResendCount = 1;
  private int _fristSendTime = -1;
  private string _responStatus = string.Empty;
  private string _url;
  private string _action;
  private string _errorMsg;
  private int _type;
  private int _timeout;
  private int _sendCount;
  private int _reSendTime;
  private bool _autoRetry;
  private bool _isWorker;
  private bool _isSending;
  private bool _forceEncrypt;
  private Hashtable _postData;
  private byte[] _resData;
  private System.Action<LoaderHttp> _onResult;
  private System.Action<LoaderHttp> _onFailure;
  private System.Action<LoaderHttp> _onProgress;
  private System.Action<bool, object> _onCallback;
  private bool _isTimeOut;
  private int _retryMaxTime;
  private bool _needResend;
  private int _totalBytes;
  private int _currentBytes;
  private bool _blockScreen;
  private int _requestSize;
  private int _responseSize;
  private int _durationTime;

  public string URL
  {
    get
    {
      return this._url;
    }
    set
    {
      this._url = value;
    }
  }

  public string Action
  {
    get
    {
      return this._action;
    }
    set
    {
      this._action = value;
    }
  }

  public bool IsTimeOut
  {
    get
    {
      return this._isTimeOut;
    }
    set
    {
      this._isTimeOut = value;
    }
  }

  public string ErrorMsg
  {
    get
    {
      return this._errorMsg;
    }
    set
    {
      this._errorMsg = value;
    }
  }

  public int Type
  {
    get
    {
      return this._type;
    }
    set
    {
      this._type = value;
    }
  }

  public int TimeOut
  {
    get
    {
      return this._timeout;
    }
    set
    {
      this._timeout = value;
    }
  }

  public int SendCount
  {
    get
    {
      return this._sendCount;
    }
    set
    {
      this._sendCount = value;
    }
  }

  public int MaxResendCount
  {
    get
    {
      return this._maxResendCount;
    }
    set
    {
      this._maxResendCount = value;
    }
  }

  public bool NeedResend()
  {
    return this._needResend;
  }

  public bool CanResend()
  {
    this._needResend = false;
    if (this.SendCount < this.MaxResendCount)
    {
      this._needResend = true;
      return this._needResend;
    }
    if (this.FirstSendTime <= -1)
      return this._needResend;
    int num = LoaderManager.inst.Now - this.FirstSendTime;
    if (this.RetryMaxTime <= 0 && this.RetryMaxTime <= 0)
      this.RetryMaxTime = 30;
    this._needResend = num < this.RetryMaxTime;
    return this._needResend;
  }

  public int RetryMaxTime
  {
    get
    {
      return this._retryMaxTime;
    }
    set
    {
      this._retryMaxTime = value;
    }
  }

  public int FirstSendTime
  {
    get
    {
      return this._fristSendTime;
    }
    set
    {
      this._fristSendTime = value;
    }
  }

  public int ReSendTime
  {
    get
    {
      return this._reSendTime;
    }
    set
    {
      this._reSendTime = value;
    }
  }

  public bool AutoRetry
  {
    get
    {
      return this._autoRetry;
    }
    set
    {
      this._autoRetry = value;
    }
  }

  public bool IsWroker
  {
    get
    {
      return this._isWorker;
    }
    set
    {
      this._isWorker = value;
    }
  }

  public bool IsSending
  {
    get
    {
      return this._isSending;
    }
    set
    {
      this._isSending = value;
    }
  }

  public bool ForceEncrypt
  {
    get
    {
      return this._forceEncrypt;
    }
    set
    {
      this._forceEncrypt = value;
    }
  }

  public Hashtable PostData
  {
    get
    {
      return this._postData;
    }
    set
    {
      this._postData = value;
    }
  }

  public byte[] ResData
  {
    get
    {
      return this._resData;
    }
    set
    {
      this._resData = value;
    }
  }

  public System.Action<LoaderHttp> OnResult
  {
    get
    {
      return this._onResult;
    }
    set
    {
      this._onResult = value;
    }
  }

  public System.Action<LoaderHttp> OnFailure
  {
    get
    {
      return this._onFailure;
    }
    set
    {
      this._onFailure = value;
    }
  }

  public System.Action<LoaderHttp> OnProgress
  {
    get
    {
      return this._onProgress;
    }
    set
    {
      this._onProgress = value;
    }
  }

  public System.Action<bool, object> OnCallback
  {
    get
    {
      return this._onCallback;
    }
    set
    {
      this._onCallback = value;
    }
  }

  public int TotalBytes
  {
    set
    {
      this._totalBytes = value;
    }
    get
    {
      return this._totalBytes;
    }
  }

  public int CurrentBytes
  {
    set
    {
      this._currentBytes = value;
    }
    get
    {
      return this._currentBytes;
    }
  }

  public string ResponStatus
  {
    get
    {
      return this._responStatus;
    }
    set
    {
      this._responStatus = value;
    }
  }

  public bool BlockScreen
  {
    get
    {
      return this._blockScreen;
    }
    set
    {
      this._blockScreen = value;
    }
  }

  public int RequestSize
  {
    get
    {
      return this._requestSize;
    }
    set
    {
      this._requestSize = value;
    }
  }

  public int ResponseSize
  {
    get
    {
      return this._responseSize;
    }
    set
    {
      this._responseSize = value;
    }
  }

  public int DurationTime
  {
    get
    {
      return this._durationTime;
    }
    set
    {
      this._durationTime = value;
    }
  }

  public Dictionary<string, string> Headers { get; set; }

  public byte[] RawPostData { get; set; }

  public override string ToString()
  {
    return string.Format("[LoaderInfo]: Action={1}, PostData={2}, ResData={3}, URL={0}", (object) this.URL, (object) this.Action, (object) Utils.Object2Json((object) this.PostData), (object) Utils.ByteArray2String(this.ResData));
  }

  public LoaderInfo Clone()
  {
    LoaderInfo loaderInfo = new LoaderInfo();
    loaderInfo.Action = this.Action;
    Hashtable hashtable1 = (Hashtable) null;
    if (this.PostData != null && this.PostData.ContainsKey((object) "params"))
    {
      Hashtable hashtable2 = this.PostData[(object) "params"] as Hashtable;
      if (hashtable2.ContainsKey((object) "args") && hashtable2[(object) "args"] != null)
        hashtable1 = hashtable2[(object) "args"] as Hashtable;
    }
    loaderInfo.PostData = hashtable1;
    loaderInfo.OnCallback = this.OnCallback;
    return loaderInfo;
  }

  public void Dispose()
  {
    this._fristSendTime = -1;
    this.DurationTime = 0;
    this.ResponStatus = (string) null;
    this.RequestSize = 0;
    this.ResponseSize = 0;
    this._url = (string) null;
    this._type = 0;
    this._timeout = 0;
    this._sendCount = 0;
    this._reSendTime = 0;
    this._action = (string) null;
    this._isWorker = false;
    this._autoRetry = false;
    this._isSending = false;
    this._postData = (Hashtable) null;
    this._errorMsg = (string) null;
    this._resData = (byte[]) null;
    this._onResult = (System.Action<LoaderHttp>) null;
    this._onFailure = (System.Action<LoaderHttp>) null;
    this._onCallback = (System.Action<bool, object>) null;
    this._onProgress = (System.Action<LoaderHttp>) null;
    this._forceEncrypt = false;
  }
}
