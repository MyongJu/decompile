﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarMiniMapTrackSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceWarMiniMapTrackSlot : MonoBehaviour
{
  private Color _clickColor = new Color(0.9215686f, 0.5372549f, 0.0f);
  [SerializeField]
  private UILabel _labelTrackedName;
  [SerializeField]
  private UISprite _spriteTracked;
  public System.Action<AllianceData> OnAllianceTracked;
  private Color _originColor;
  private AllianceData _oppAllianceData;
  private bool _isTracked;

  public long OppAllianceId
  {
    get
    {
      if (this._oppAllianceData != null)
        return this._oppAllianceData.allianceId;
      return 0;
    }
  }

  public void SetData(AllianceData oppAllianceData, bool isTracked = false)
  {
    this._oppAllianceData = oppAllianceData;
    this._originColor = this._labelTrackedName.color;
    this._isTracked = isTracked;
    this.UpdateUI();
  }

  public void OnTrackSlotClicked(bool defaultTracked = false)
  {
    if (this._oppAllianceData == null)
      return;
    this.ResetAllColor();
    this.SetColor();
    this.ResetAllTrackedStatue();
    this.SetTrackedStatus(true);
    AllianceWarPayload.Instance.LoadTrack(this._oppAllianceData.allianceId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      MiniMapSystem.Instance.ShowAllianceWarEnemy();
      AllianceWarPayload.Instance.SetTrackedAlliance(this._oppAllianceData.allianceId);
      if (this.OnAllianceTracked == null || defaultTracked)
        return;
      this.OnAllianceTracked(this._oppAllianceData);
    }));
  }

  public void SetColor()
  {
    this._labelTrackedName.color = this._clickColor;
  }

  public void ResetColor()
  {
    this._labelTrackedName.color = this._originColor;
  }

  public void ResetAllColor()
  {
    AllianceWarMiniMapTrackSlot[] componentsInChildren = this.transform.parent.GetComponentsInChildren<AllianceWarMiniMapTrackSlot>();
    if (componentsInChildren == null)
      return;
    for (int index = 0; index < componentsInChildren.Length; ++index)
      componentsInChildren[index].ResetColor();
  }

  public void ResetAllTrackedStatue()
  {
    AllianceWarMiniMapTrackSlot[] componentsInChildren = this.transform.parent.GetComponentsInChildren<AllianceWarMiniMapTrackSlot>();
    if (componentsInChildren == null)
      return;
    for (int index = 0; index < componentsInChildren.Length; ++index)
      componentsInChildren[index].SetTrackedStatus(false);
  }

  public void SetTrackedStatus(bool isTracked)
  {
    NGUITools.SetActive(this._spriteTracked.gameObject, isTracked);
  }

  private void UpdateUI()
  {
    if (this._oppAllianceData == null)
      return;
    this._labelTrackedName.text = string.Format("K{0}.({1}){2}", (object) this._oppAllianceData.worldId, (object) this._oppAllianceData.allianceAcronym, (object) this._oppAllianceData.allianceName);
    this.SetTrackedStatus(this._isTracked);
  }
}
