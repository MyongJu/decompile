﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsShopInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class MerlinTrialsShopInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "itemid")]
  public int itemId;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "price")]
  public int price;
  [Config(Name = "category")]
  public string category;
}
