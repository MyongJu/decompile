﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryIapMainDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AnniversaryIapMainDlg : UI.Dialog
{
  private Dictionary<int, AnniversaryCatalog> _catalogItemDict = new Dictionary<int, AnniversaryCatalog>();
  private Dictionary<int, AnniversaryIapSlot> _iapPackageItemDict = new Dictionary<int, AnniversaryIapSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UITexture _textureNoPackage;
  [SerializeField]
  private UITexture _textureProps;
  [SerializeField]
  private UILabel _labelPropsCount;
  [SerializeField]
  private GameObject _objHasFreeProps;
  [SerializeField]
  private UIScrollView _catalogScrollView;
  [SerializeField]
  private UITable _catalogTable;
  [SerializeField]
  private GameObject _catalogItemPrefab;
  [SerializeField]
  private GameObject _catalogItemRoot;
  [SerializeField]
  private UIScrollView _iapPackageScrollView;
  [SerializeField]
  private UITable _iapPackageTable;
  [SerializeField]
  private GameObject _iapPackageItemPrefab;
  [SerializeField]
  private GameObject _iapPackageItemRoot;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    AnniversaryIapMainDlg.Parameter parameter = orgParam as AnniversaryIapMainDlg.Parameter;
    if (parameter != null && !parameter.showCloseButton && (bool) ((UnityEngine.Object) this.uiManagerPanel.BT_Close))
      NGUITools.SetActive(this.uiManagerPanel.BT_Close.gameObject, false);
    AnniversaryIapPayload.Instance.OnAnnvIapDataUpdated += new System.Action(this.OnAnnvIapDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdated);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    this.UpdateUI();
    this.UpdateCatalog();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    AnniversaryIapPayload.Instance.OnAnnvIapDataUpdated -= new System.Action(this.OnAnnvIapDataUpdated);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdated);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    AnnouncementPopupController.Instance.isFirstAnnPopupClosed = true;
    this.ClearCatalogData();
    this.ClearRightData();
  }

  public void OnMorePropsClicked()
  {
    AnniversaryIapPayload.Instance.RequestServerData((System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      UIManager.inst.OpenPopup("Anniversary/AnniversaryIapPropsPopup", (Popup.PopupParameter) null);
    }));
  }

  private void OnAnnvIapDataUpdated()
  {
    this.UpdateUI();
  }

  private void OnItemDataUpdated(int itemId)
  {
    if (itemId != AnniversaryIapPayload.Instance.NeedPropsId)
      return;
    this.UpdateUI();
  }

  private void OnSecondEvent(int time)
  {
    if ((long) NetServerTime.inst.ServerTimestamp < AnniversaryIapPayload.Instance.EndTime || !UIManager.inst.IsDlgExist("Anniversary/AnniversaryIapMainDlg"))
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureNoPackage, "Texture/STATIC_TEXTURE/tutorial_npc", (System.Action<bool>) null, true, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this._textureProps, AnniversaryIapPayload.Instance.PropsImagePath, (System.Action<bool>) null, true, true, string.Empty);
    this._labelPropsCount.text = string.Format("{0}{1}", (object) Utils.XLAT("iap_package_1_year_celebration_tokens_description"), (object) AnniversaryIapPayload.Instance.PropsAmount);
    NGUITools.SetActive(this._objHasFreeProps, AnniversaryIapPayload.Instance.AnnvIapData.CanReceivePropsNum > 0L);
    this.RefreshNoPackage();
  }

  private void RefreshNoPackage()
  {
    bool state = true;
    using (Dictionary<int, AnniversaryIapSlot>.Enumerator enumerator = this._iapPackageItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.LeftCount != 0)
          state = false;
      }
    }
    NGUITools.SetActive(this._textureNoPackage.transform.parent.gameObject, state);
  }

  private void UpdateCatalog()
  {
    this.ClearCatalogData();
    List<AnniversaryIapMainInfo> anniversaryIapMainInfoList = ConfigManager.inst.DB_AnniversaryIapMain.GetAnniversaryIapMainInfoList();
    if (anniversaryIapMainInfoList != null && anniversaryIapMainInfoList.Count > 0)
    {
      int type = anniversaryIapMainInfoList[anniversaryIapMainInfoList.Count - 1].type;
      for (int i = 1; i <= type; ++i)
      {
        AnniversaryIapMainInfo anniversaryIapMainInfo = anniversaryIapMainInfoList.Find((Predicate<AnniversaryIapMainInfo>) (x => x.type == i));
        if (anniversaryIapMainInfo != null && !string.IsNullOrEmpty(anniversaryIapMainInfo.CatalogName))
        {
          AnniversaryCatalog catalogSlot = this.GenerateCatalogSlot(anniversaryIapMainInfo.CatalogName, AnniversaryCatalog.CatalogType.OTHER, i);
          this._catalogItemDict.Add(i, catalogSlot);
        }
      }
    }
    this.RepositionCatalog();
  }

  private AnniversaryCatalog GenerateCatalogSlot(string catalogName, AnniversaryCatalog.CatalogType catalogType, int catalogIndex)
  {
    this._itemPool.Initialize(this._catalogItemPrefab, this._catalogItemRoot);
    GameObject go = this._itemPool.AddChild(this._catalogTable.gameObject);
    NGUITools.SetActive(go, true);
    AnniversaryCatalog component = go.GetComponent<AnniversaryCatalog>();
    component.OnCurrentCatalogClicked += new System.Action<AnniversaryCatalog.CatalogType, int>(this.OnCatalogClicked);
    component.SetData(catalogName, catalogType, catalogIndex, catalogIndex == 1, false);
    return component;
  }

  private void RepositionCatalog()
  {
    this._catalogTable.repositionNow = true;
    this._catalogTable.Reposition();
    this._catalogScrollView.ResetPosition();
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, AnniversaryCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, AnniversaryCatalog> current = enumerator.Current;
        current.Value.OnCurrentCatalogClicked -= new System.Action<AnniversaryCatalog.CatalogType, int>(this.OnCatalogClicked);
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  private void OnCatalogClicked(AnniversaryCatalog.CatalogType catalogType, int catalogIndex)
  {
    this.ClearRightData();
    List<AnniversaryIapMainInfo> all = ConfigManager.inst.DB_AnniversaryIapMain.GetAnniversaryIapMainInfoList().FindAll((Predicate<AnniversaryIapMainInfo>) (x => x.type == catalogIndex));
    if (all != null && all.Count > 0)
    {
      all.Sort((Comparison<AnniversaryIapMainInfo>) ((a, b) => a.order.CompareTo(b.order)));
      for (int key = 0; key < all.Count; ++key)
      {
        if (AnniversaryIapPayload.Instance.GetIapPackageRemained(all[key].internalId) != 0)
        {
          AnniversaryIapSlot rightSlot = this.GenerateRightSlot(all[key]);
          this._iapPackageItemDict.Add(key, rightSlot);
        }
      }
    }
    this.RefreshNoPackage();
    this.RepositionRight();
  }

  private AnniversaryIapSlot GenerateRightSlot(AnniversaryIapMainInfo iapMainInfo)
  {
    this._itemPool.Initialize(this._iapPackageItemPrefab, this._iapPackageItemRoot);
    GameObject go = this._itemPool.AddChild(this._iapPackageTable.gameObject);
    NGUITools.SetActive(go, true);
    AnniversaryIapSlot component = go.GetComponent<AnniversaryIapSlot>();
    component.SetData(iapMainInfo);
    return component;
  }

  private void RepositionRight()
  {
    this._iapPackageTable.repositionNow = true;
    this._iapPackageTable.Reposition();
    this._iapPackageScrollView.ResetPosition();
  }

  private void ClearRightData()
  {
    using (Dictionary<int, AnniversaryIapSlot>.Enumerator enumerator = this._iapPackageItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, AnniversaryIapSlot> current = enumerator.Current;
        current.Value.ClearData();
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._iapPackageItemDict.Clear();
    this._itemPool.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public bool showCloseButton = true;
  }
}
