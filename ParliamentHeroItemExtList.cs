﻿// Decompiled with JetBrains decompiler
// Type: ParliamentHeroItemExtList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class ParliamentHeroItemExtList : MonoBehaviour
{
  [SerializeField]
  private ParliamentHeroItemExt itemPrefab;
  [SerializeField]
  private UITable container;

  public void SetData(List<LegendCardData> datas)
  {
    for (int index = 0; index < datas.Count; ++index)
      this.GetItem().SetData(datas[index]);
    this.container.Reposition();
  }

  private ParliamentHeroItemExt GetItem()
  {
    GameObject go = NGUITools.AddChild(this.container.gameObject, this.itemPrefab.gameObject);
    NGUITools.SetActive(go, true);
    return go.GetComponent<ParliamentHeroItemExt>();
  }
}
