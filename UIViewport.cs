﻿// Decompiled with JetBrains decompiler
// Type: UIViewport
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/Viewport Camera")]
[RequireComponent(typeof (Camera))]
public class UIViewport : MonoBehaviour
{
  public float fullSize = 1f;
  public Camera sourceCamera;
  public Transform topLeft;
  public Transform bottomRight;
  private Camera mCam;

  private void Start()
  {
    this.mCam = this.GetComponent<Camera>();
    if (!((Object) this.sourceCamera == (Object) null))
      return;
    this.sourceCamera = Camera.main;
  }

  private void LateUpdate()
  {
    if (!((Object) this.topLeft != (Object) null) || !((Object) this.bottomRight != (Object) null))
      return;
    Vector3 screenPoint1 = this.sourceCamera.WorldToScreenPoint(this.topLeft.position);
    Vector3 screenPoint2 = this.sourceCamera.WorldToScreenPoint(this.bottomRight.position);
    Rect rect = new Rect(screenPoint1.x / (float) Screen.width, screenPoint2.y / (float) Screen.height, (screenPoint2.x - screenPoint1.x) / (float) Screen.width, (screenPoint1.y - screenPoint2.y) / (float) Screen.height);
    float num = this.fullSize * rect.height;
    if (rect != this.mCam.rect)
      this.mCam.rect = rect;
    if ((double) this.mCam.orthographicSize == (double) num)
      return;
    this.mCam.orthographicSize = num;
  }
}
