﻿// Decompiled with JetBrains decompiler
// Type: HeroLevelUpInfoComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UnityEngine;

public class HeroLevelUpInfoComponent : ComponentRenderBase
{
  private readonly Color ColorNextLevel = new Color(0.25f, 0.82f, 0.13f, 1f);
  private readonly Color ColorCurLevel = new Color(0.81f, 0.81f, 0.81f, 1f);
  [SerializeField]
  private ParliamentHeroItem _HeroItem;
  [SerializeField]
  private LevelUpSliderComponent _Slider;
  [SerializeField]
  private UILabel _HeroNameLabel;
  [SerializeField]
  private GameObject _Arrow1;
  [SerializeField]
  private GameObject _Arrow2;
  [SerializeField]
  private UILabel _LevelLabel;
  [SerializeField]
  private UILabel _CurLevelLabel;
  [SerializeField]
  private UILabel _NewLevelLabel;
  [SerializeField]
  private UILabel _BenefitNameLabel;
  [SerializeField]
  private UILabel _CurBenefitValueLabel;
  [SerializeField]
  private UILabel _NewBenefitValueLabel;
  protected LegendCardData legendData;

  public override void Init()
  {
    base.Init();
    HeroLevelUpInfoComponentData data = this.data as HeroLevelUpInfoComponentData;
    if (data == null)
      return;
    this.legendData = data.legendData;
    this._HeroItem.SetData(this.legendData, true, false);
    this.InitBenefitsUI();
    this.InitSlider();
  }

  private void InitBenefitsUI()
  {
    NGUITools.SetActive(this._Arrow1, false);
    NGUITools.SetActive(this._Arrow2, false);
    NGUITools.SetActive(this._NewLevelLabel.gameObject, false);
    NGUITools.SetActive(this._NewBenefitValueLabel.gameObject, false);
    this._LevelLabel.text = ScriptLocalization.GetWithPara("id_level_num", new Dictionary<string, string>()
    {
      {
        "num",
        string.Empty
      }
    }, true);
    this._CurLevelLabel.text = this.legendData.Level.ToString();
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.legendData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    this._HeroNameLabel.text = parliamentHeroInfo.Name;
    this._HeroNameLabel.color = parliamentHeroInfo.QualityColor;
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[parliamentHeroInfo.levelBenefit];
    this._BenefitNameLabel.text = dbProperty.Name;
    this._CurBenefitValueLabel.text = dbProperty.ConvertToDisplayString((double) parliamentHeroInfo.GetLevelBenefitByLevel(this.legendData.Level), false, true);
  }

  private void InitSlider()
  {
    this._Slider.FeedData((IComponentData) new LevelUpSliderComponentData()
    {
      CurExp = this.legendData.CurrentLevelXP,
      NewExp = this.legendData.CurrentLevelXP,
      CurLevel = this.legendData.Level,
      NewLevel = this.legendData.Level,
      CurLevelExpMax = HeroCardUtils.GetRequiredExpToNextLevel(this.legendData.LegendId, this.legendData.Level),
      NewLevelExpMax = HeroCardUtils.GetRequiredExpToNextLevel(this.legendData.LegendId, this.legendData.Level),
      IsMaxLevel = this.legendData.IsMaxLevel
    });
  }

  public void AddExp(long exp, ref bool isMax, ref long overExp)
  {
    int newLevel = 0;
    long newExp = 0;
    HeroCardUtils.HeroCardAddExp(this.legendData, exp, ref newLevel, ref newExp);
    this.UpdateBenefitsUI(newLevel, exp);
    this.UpdateSlider(newExp, newLevel);
    if (newLevel != HeroCardUtils.GetHeroMaxLevel(this.legendData.LegendId))
      return;
    isMax = true;
    overExp = newExp;
  }

  private void UpdateBenefitsUI(int newLevel, long addExp)
  {
    NGUITools.SetActive(this._Arrow1, addExp > 0L);
    NGUITools.SetActive(this._Arrow2, addExp > 0L);
    NGUITools.SetActive(this._NewLevelLabel.gameObject, addExp > 0L);
    NGUITools.SetActive(this._NewBenefitValueLabel.gameObject, addExp > 0L);
    this._NewLevelLabel.text = newLevel.ToString();
    ParliamentHeroInfo parliamentHeroInfo = ConfigManager.inst.DB_ParliamentHero.Get(this.legendData.LegendId);
    if (parliamentHeroInfo == null)
      return;
    this._NewBenefitValueLabel.text = ConfigManager.inst.DB_Properties[parliamentHeroInfo.levelBenefit].ConvertToDisplayString((double) parliamentHeroInfo.GetLevelBenefitByLevel(newLevel), false, true);
    this._NewBenefitValueLabel.color = newLevel <= this.legendData.Level ? this.ColorCurLevel : this.ColorNextLevel;
    this._NewLevelLabel.color = newLevel <= this.legendData.Level ? this.ColorCurLevel : this.ColorNextLevel;
  }

  private void UpdateSlider(long newExp, int newLevel)
  {
    this._Slider.FeedData((IComponentData) new LevelUpSliderComponentData()
    {
      CurExp = this.legendData.CurrentLevelXP,
      NewExp = newExp,
      CurLevel = this.legendData.Level,
      NewLevel = newLevel,
      CurLevelExpMax = HeroCardUtils.GetRequiredExpToNextLevel(this.legendData.LegendId, this.legendData.Level),
      NewLevelExpMax = HeroCardUtils.GetRequiredExpToNextLevel(this.legendData.LegendId, newLevel),
      IsMaxLevel = (HeroCardUtils.GetHeroMaxLevel(this.legendData.LegendId) <= newLevel)
    });
  }

  public override void Dispose()
  {
    base.Dispose();
  }
}
