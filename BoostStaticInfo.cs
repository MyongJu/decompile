﻿// Decompiled with JetBrains decompiler
// Type: BoostStaticInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class BoostStaticInfo
{
  [Config(Name = "type_desc_loc")]
  public string Loc_Description = string.Empty;
  public int internalId;
  [Config(Name = "id1")]
  public int Item_InternalID;
  [Config(Name = "category")]
  public BoostCategory Category;
  [Config(Name = "type")]
  public BoostType Type;
  [Config(Name = "onsale")]
  public bool OnSale;
  [Config(Name = "id2")]
  public int ShopItemID;
  [Config(Name = "type_loc")]
  public string Loc_Name;
  [Config(Name = "type_image")]
  public string Image;
}
