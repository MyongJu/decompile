﻿// Decompiled with JetBrains decompiler
// Type: BuildingHintPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class BuildingHintPopup : Popup
{
  private HubPort mPortIdleBuildingRewards = MessageHub.inst.GetPortByAction("City:idleBuildingRewards");
  private const string hasIdle = "hasshowidle";
  private string buildingType;
  private BuildingIdleInfo bii;
  private int step;
  private bool isRandomStr;
  public UILabel LB_description;

  private void CheckRewards()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    PlayerPrefsEx.SetInt("idleBuildingShowCount", PlayerPrefsEx.GetInt("idleBuildingShowCount") + 1);
    this.mPortIdleBuildingRewards.SendRequest((Hashtable) null, (System.Action<bool, object>) ((ret, obj) =>
    {
      if (obj == null)
        return;
      RewardsCollectionAnimator.Instance.Clear();
      foreach (DictionaryEntry dictionaryEntry in obj as Hashtable)
      {
        if (!(dictionaryEntry.Key.ToString() == nameof (ret)))
        {
          ResRewardsInfo.Data data = new ResRewardsInfo.Data();
          if (int.TryParse(dictionaryEntry.Value.ToString(), out data.count))
          {
            string key = dictionaryEntry.Key.ToString();
            if (key != null)
            {
              // ISSUE: reference to a compiler-generated field
              if (BuildingHintPopup.\u003C\u003Ef__switch\u0024map32 == null)
              {
                // ISSUE: reference to a compiler-generated field
                BuildingHintPopup.\u003C\u003Ef__switch\u0024map32 = new Dictionary<string, int>(6)
                {
                  {
                    "food",
                    0
                  },
                  {
                    "wood",
                    1
                  },
                  {
                    "ore",
                    2
                  },
                  {
                    "silver",
                    3
                  },
                  {
                    "steel",
                    4
                  },
                  {
                    "coin",
                    5
                  }
                };
              }
              int num;
              // ISSUE: reference to a compiler-generated field
              if (BuildingHintPopup.\u003C\u003Ef__switch\u0024map32.TryGetValue(key, out num))
              {
                switch (num)
                {
                  case 0:
                    data.rt = ResRewardsInfo.ResType.Food;
                    goto label_17;
                  case 1:
                    data.rt = ResRewardsInfo.ResType.Wood;
                    goto label_17;
                  case 2:
                    data.rt = ResRewardsInfo.ResType.Ore;
                    goto label_17;
                  case 3:
                    data.rt = ResRewardsInfo.ResType.Silver;
                    goto label_17;
                  case 4:
                    data.rt = ResRewardsInfo.ResType.Steel;
                    goto label_17;
                  case 5:
                    data.rt = ResRewardsInfo.ResType.Coin;
                    goto label_17;
                }
              }
            }
            data.rt = ResRewardsInfo.ResType.Food;
label_17:
            RewardsCollectionAnimator.Instance.Ress.Add(data);
          }
        }
      }
      RewardsCollectionAnimator.Instance.CollectResource(false);
    }), true);
  }

  public void OnClickPanel()
  {
    if (this.step == 1 && !this.isRandomStr)
    {
      if (this.bii.intro_2 != null)
      {
        this.LB_description.text = ScriptLocalization.Get(this.bii.intro_2, true);
        this.step = 2;
      }
      else
        this.CheckRewards();
    }
    else
      this.CheckRewards();
  }

  public string GetRandomString()
  {
    return ConfigManager.inst.DB_BuildingIdleCommon.GetBuildingIdleCommonInfo(Utils.RndRange(1, ConfigManager.inst.DB_BuildingIdleCommon.GetBuildingIdleCommonCount()).ToString()).content;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.buildingType = (orgParam as BuildingHintPopup.Parameter).buildingType;
    this.bii = ConfigManager.inst.DB_BuildingIdle.GetBuildingIdleInfoByBuildingType(this.buildingType);
    string randomString;
    if (!PlayerPrefsEx.HasKey(this.buildingType + "hasshowidle"))
    {
      PlayerPrefsEx.SetString(this.buildingType + "hasshowidle", NetServerTime.inst.UpdateTime.ToString());
      randomString = ScriptLocalization.Get(this.bii.intro_1, true);
      this.isRandomStr = false;
    }
    else if (NetServerTime.inst.IsToday(double.Parse(PlayerPrefsEx.GetString(this.buildingType + "hasshowidle"))))
    {
      randomString = this.GetRandomString();
      this.isRandomStr = true;
    }
    else
    {
      PlayerPrefsEx.SetString(this.buildingType + "hasshowidle", NetServerTime.inst.UpdateTime.ToString());
      randomString = ScriptLocalization.Get(this.bii.intro_1, true);
      this.isRandomStr = false;
    }
    this.LB_description.text = !this.isRandomStr ? randomString : ScriptLocalization.Get(randomString, true);
    this.step = 1;
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string buildingType;
  }
}
