﻿// Decompiled with JetBrains decompiler
// Type: LeaderBoardAllianceInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class LeaderBoardAllianceInfo : Popup
{
  private const string ALLIANCE_FLAG = null;
  private const string ALLIANCE_SHIELD = null;
  private const string ALLIANCE_PATTERN = null;
  private const string CROWN_PATH = null;
  public UITexture allianceTexture_Flag;
  public UITexture allianceTexture_Shield;
  public UITexture allianceTexture_Pattern;
  public UITexture crown;
  public UILabel allianceName;
  public UILabel alliancePower;
  public UILabel allianceMember;
  public UILabel allianceRank;
  public UIButton messageBtn;
  public UIButton highloardBtn;
  public UIButton membersBtn;
  public UILabel announcement;
  private long id;

  private void Clear()
  {
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    this.Clear();
    base.OnOpen(orgParam);
    this.id = (orgParam as LeaderBoardAllianceInfo.LeaderBoardAllianceInfoParameter).allianceID;
    this.allianceName.text = string.Format("LOL ALLIANCE_{0}", (object) this.id);
    this.alliancePower.text = 2223456.ToString();
    this.allianceMember.text = string.Format("Happy Member {0}", (object) this.id);
    this.allianceRank.text = string.Format("{0}/{1}", (object) 18.ToString(), (object) 50.ToString());
    this.announcement.text = string.Format("Annoucement{0}", (object) this.id);
  }

  public void OnMessageBtnClicked()
  {
  }

  public void OnHighLordBtnClicked()
  {
  }

  public void OnMembersBtnClicked()
  {
  }

  public void OnCloseBtnClicked()
  {
    this.Clear();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    Object.Destroy((Object) this.gameObject);
  }

  public class LeaderBoardAllianceInfoParameter : Popup.PopupParameter
  {
    public long allianceID;
  }
}
