﻿// Decompiled with JetBrains decompiler
// Type: ActivityStageRank
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class ActivityStageRank : MonoBehaviour
{
  public Icon template1;
  public Icon template2;
  public UIGrid grid;
  public UIScrollView scrollView;
  public UILabel rankLabel;
  private bool inited;
  private ActivityBaseData data;

  public void UpdateUI(ActivityBaseData data)
  {
    if (this.inited)
      return;
    this.data = data;
    IRankUIRewardData rankUiRewardData = data as IRankUIRewardData;
    this.ClearGrid();
    List<int> rankTopOneItems = rankUiRewardData.GetRankTopOneItems();
    Dictionary<int, int> topOneRewardItems = rankUiRewardData.GetRankTopOneRewardItems();
    this.rankLabel.text = string.Format(Utils.XLAT("event_rank_num"), (object) 1);
    for (int index = 0; index < rankTopOneItems.Count; ++index)
    {
      int internalId = rankTopOneItems[index];
      int num = topOneRewardItems[internalId];
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template1.gameObject);
      gameObject.SetActive(true);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      Icon component = gameObject.GetComponent<Icon>();
      component.FeedData((IComponentData) new IconData(itemStaticInfo.ImagePath, new string[2]
      {
        itemStaticInfo.LocName,
        "X" + num.ToString()
      })
      {
        Data = (object) internalId
      });
      component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
    }
    this.grid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.inited = true;
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void OnViewAllRewardBtnClick()
  {
    TimeLimitAllRewardPopup.Parameter parameter = new TimeLimitAllRewardPopup.Parameter()
    {
      isTotalReward = false,
      subTitleString = Utils.XLAT("event_stage_ranking_reward")
    };
    parameter.data = this.data;
    UIManager.inst.OpenPopup("Activity/TimeLimitAllRewardPopup", (Popup.PopupParameter) parameter);
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
