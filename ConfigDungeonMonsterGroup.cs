﻿// Decompiled with JetBrains decompiler
// Type: ConfigDungeonMonsterGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDungeonMonsterGroup
{
  private Dictionary<string, DungeonMonsterGroupStaticInfo> datas;
  private Dictionary<int, DungeonMonsterGroupStaticInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<DungeonMonsterGroupStaticInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  public DungeonMonsterGroupStaticInfo GetData(int internalId)
  {
    if (this.dicByUniqueId != null && this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (DungeonMonsterGroupStaticInfo) null;
  }

  public DungeonMonsterGroupStaticInfo GetData(string id)
  {
    if (this.datas != null && this.datas.ContainsKey(id))
      return this.datas[id];
    return (DungeonMonsterGroupStaticInfo) null;
  }
}
