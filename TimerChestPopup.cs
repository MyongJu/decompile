﻿// Decompiled with JetBrains decompiler
// Type: TimerChestPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Funplus;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimerChestPopup : Popup
{
  private const string promotionIconTemplate = "Texture/TimerChest/timer_chest_promotion_{0}";
  private const string availableTime = "id_available_time";
  public UITexture giftIcon;
  public UILabel giftCount;
  public UILabel giftName;
  public UILabel giftDescription;
  public UILabel promotionName;
  public UILabel promotionDescription;
  public UITexture promotionIcon;
  public Transform border;
  public UITexture backgroud;
  public UILabel coldTime;
  public UIButton claimBtn;
  private TimerChestManager manager;
  private int giftId;
  private IAPStorePackage iapPackage;
  private ItemStaticInfo givenGiftInfo;
  private int givenGiftCount;
  private Dictionary<string, TimerChestPopup.PromotionInfo> promotionInfoTable;
  private Dictionary<string, System.Action> grabMethodMap;
  private System.Action grabMethod;
  private string grabType;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
    this.promotionInfoTable = new Dictionary<string, TimerChestPopup.PromotionInfo>();
    this.promotionInfoTable.Add("month_card", new TimerChestPopup.PromotionInfo()
    {
      name = "timed_reward_daily_delivery_title",
      despriction = "timed_reward_daily_delivery_description"
    });
    this.promotionInfoTable.Add("iap_package", new TimerChestPopup.PromotionInfo()
    {
      name = string.Empty,
      despriction = string.Empty
    });
    this.promotionInfoTable.Add("artifact_limit", new TimerChestPopup.PromotionInfo()
    {
      name = "timed_reward_temp_artifact_title",
      despriction = "timed_reward_temp_artifact_description"
    });
    this.grabMethodMap = new Dictionary<string, System.Action>();
    this.grabMethodMap.Add("month_card", new System.Action(this.GoToMonthCard));
    this.grabMethodMap.Add("iap_package", new System.Action(this.GoToIAPStore));
    this.grabMethodMap.Add("artifact_limit", new System.Action(this.GoToArtifactLimit));
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TimerChestPopup.Parameter grabData = orgParam as TimerChestPopup.Parameter;
    if (grabData == null)
      return;
    this.manager = grabData.manager;
    this.givenGiftInfo = ConfigManager.inst.DB_Items.GetItem(grabData.giftId);
    this.givenGiftCount = grabData.giftCount;
    this.giftId = grabData.giftId;
    if (this.givenGiftInfo != null)
    {
      this.giftDescription.text = this.givenGiftInfo.LocDescription;
      this.giftCount.text = string.Format("X {0}", (object) this.givenGiftCount);
      Utils.SetItemName(this.giftName, grabData.giftId);
      Utils.SetItemBackground(this.backgroud, grabData.giftId);
      BuilderFactory.Instance.HandyBuild((UIWidget) this.giftIcon, this.givenGiftInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    }
    this.RefreshCDTime();
    this.refreshGrab(grabData);
    DBManager.inst.DB_MonthCard.onDataUpdated += new System.Action<long>(this.OnUpdate);
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdate);
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.giftId, this.border, 0L, 0L, 0);
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.giftId, this.border, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_MonthCard.onDataUpdated -= new System.Action<long>(this.OnUpdate);
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdate);
  }

  private void OnGrabDataChange()
  {
    this.refreshGrab(this.manager.UpdateGrab());
  }

  public void OnUpdate(long uid)
  {
    if (!this.shouldRefreshGrab || uid != PlayerData.inst.uid)
      return;
    this.OnGrabDataChange();
  }

  public void OnUpdate(double delta)
  {
    if (this.shouldRefreshGrab)
      this.OnGrabDataChange();
    this.RefreshCDTime();
  }

  public void OnUpdate()
  {
    if (!this.shouldRefreshGrab)
      return;
    this.OnGrabDataChange();
  }

  private bool shouldRefreshGrab
  {
    get
    {
      return ((((false ? 1 : 0) | (!(this.grabType == "month_card") ? 0 : (NetServerTime.inst.UpdateTime <= DBManager.inst.DB_MonthCard.Get(PlayerData.inst.uid).expiration_date ? 1 : 0))) != 0 ? 1 : 0) | (this.iapPackage == null ? 0 : (this.GetPackageByGroup(this.iapPackage.group.id) == null ? 1 : 0))) != 0;
    }
  }

  private void RefreshCDTime()
  {
    if (this.manager.leftTime != 0)
    {
      this.claimBtn.isEnabled = false;
      this.coldTime.text = ScriptLocalization.GetWithPara("id_available_time", new Dictionary<string, string>()
      {
        {
          "0",
          Utils.FormatTime(this.manager.leftTime, false, false, true)
        }
      }, true);
      this.coldTime.gameObject.SetActive(true);
    }
    else
    {
      this.coldTime.gameObject.SetActive(false);
      this.claimBtn.isEnabled = true;
    }
  }

  private void refreshGrab(TimerChestPopup.Parameter grabData)
  {
    this.iapPackage = (IAPStorePackage) null;
    this.grabType = grabData.grabCode;
    this.grabMethod = this.grabMethodMap[this.grabType];
    if (grabData.packageGroup != null)
    {
      this.promotionInfoTable[this.grabType].name = grabData.packageGroup.name;
      this.iapPackage = this.GetPackageByGroup(grabData.packageGroup.id);
      this.promotionInfoTable[this.grabType].despriction = this.iapPackage == null ? string.Empty : this.iapPackage.package.description;
    }
    this.promotionName.text = ScriptLocalization.Get(this.promotionInfoTable[this.grabType].name, true);
    this.promotionDescription.text = ScriptLocalization.Get(this.promotionInfoTable[this.grabType].despriction, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.promotionIcon, string.Format("Texture/TimerChest/timer_chest_promotion_{0}", (object) this.grabType), (System.Action<bool>) null, true, false, string.Empty);
  }

  private IAPStorePackage GetPackageByGroup(string groupId)
  {
    IAPStorePackage iapStorePackage = (IAPStorePackage) null;
    List<IAPStorePackage>.Enumerator enumerator = IAPStorePackagePayload.Instance.GetAvailablePackages().GetEnumerator();
    while (enumerator.MoveNext())
    {
      iapStorePackage = enumerator.Current;
      if (string.Compare(iapStorePackage.group.id, groupId) == 0)
        return iapStorePackage;
    }
    return iapStorePackage;
  }

  private void OnOpenTimeChest(bool result, object originParam)
  {
    Hashtable hashtable = originParam as Hashtable;
    if (!result || hashtable == null)
      return;
    IEnumerator enumerator = (IEnumerator) hashtable.GetEnumerator();
    if (!enumerator.MoveNext())
      return;
    int result1;
    int result2;
    if (!int.TryParse(enumerator.Current.ToString(), out result1) || !int.TryParse(hashtable[enumerator.Current].ToString(), out result2))
    {
      result1 = this.givenGiftInfo.internalId;
      result2 = this.givenGiftCount;
    }
    ItemRewardInfo.Data data = new ItemRewardInfo.Data();
    data.count = (float) result2;
    data.icon = ConfigManager.inst.DB_Items.GetItem(result1).ImagePath;
    RewardsCollectionAnimator.Instance.Clear();
    RewardsCollectionAnimator.Instance.items.Add(data);
    RewardsCollectionAnimator.Instance.CollectItems(true);
    AudioManager.Instance.PlaySound("sfx_harvest_timed_chest", false);
    this.manager.resetRefreshTime();
  }

  private void PushGrabTypeBI()
  {
    TimerChestPopup.GrabButtonClickBIItem buttonClickBiItem = new TimerChestPopup.GrabButtonClickBIItem();
    buttonClickBiItem.d_c1 = new TimerChestPopup.GrabButtonClickBICell();
    buttonClickBiItem.d_c1.key = "package_id";
    buttonClickBiItem.d_c1.value = this.iapPackage == null ? string.Empty : this.iapPackage.id;
    buttonClickBiItem.d_c2 = new TimerChestPopup.GrabButtonClickBICell();
    buttonClickBiItem.d_c2.key = "grab_type";
    buttonClickBiItem.d_c2.value = this.grabType;
    string properties = Utils.Object2Json((object) buttonClickBiItem);
    if (Application.isEditor || !FunplusSdk.Instance.IsSdkInstalled())
      return;
    FunplusBi.Instance.TraceEvent("grab_click", properties);
  }

  public void OnRewardCollectButtonClick()
  {
    MessageHub.inst.GetPortByAction("Gift:openTimerChest").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnOpenTimeChest), true);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnGrabButtonClick()
  {
    this.grabMethod();
    this.PushGrabTypeBI();
    OperationTrace.TraceClick(ClickArea.MonthCardTimerCheast);
  }

  private void registerDialogToManager(string openingDialog)
  {
    this.manager.openingDialog = openingDialog;
    this.manager.popupHiding = true;
  }

  private void GoToMonthCard()
  {
    Utils.ShowMonthCardPopup();
  }

  private void GoToIAPStore()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) new IAPStoreDialog.Parameter()
    {
      groupId = this.iapPackage.group.internalId.ToString(),
      enterSource = nameof (TimerChestPopup)
    });
    if ((UnityEngine.Object) UIManager.inst.GetDlg<UI.Dialog>(UIManager.DialogType.IAPStoreDialog) != (UnityEngine.Object) null)
      this.registerDialogToManager(UIManager.DialogType.IAPStoreDialog);
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void GoToArtifactLimit()
  {
    ArtifactSalePayload.Instance.ShowArtifactSaleStore((System.Action<bool, object>) ((ret, data) =>
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) UIManager.inst.GetDlg<ArtifactSaleStoreDlg>("Artifact/ArtifactSaleStoreDlg"))
        this.registerDialogToManager("Artifact/ArtifactSaleStoreDlg");
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }));
  }

  private class GrabButtonClickBICell
  {
    public string key;
    public string value;
  }

  private class GrabButtonClickBIItem
  {
    public TimerChestPopup.GrabButtonClickBICell d_c1;
    public TimerChestPopup.GrabButtonClickBICell d_c2;
  }

  private class PromotionInfo
  {
    public string name;
    public string despriction;
  }

  public class Parameter : Popup.PopupParameter
  {
    public TimerChestManager manager;
    public int giftId;
    public int giftCount;
    public string grabCode;
    public IAPPackageGroupInfo packageGroup;
  }
}
