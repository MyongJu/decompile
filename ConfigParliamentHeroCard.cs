﻿// Decompiled with JetBrains decompiler
// Type: ConfigParliamentHeroCard
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigParliamentHeroCard
{
  private List<ParliamentHeroCardInfo> _parliamentHeroCardInfoList = new List<ParliamentHeroCardInfo>();
  private Dictionary<string, ParliamentHeroCardInfo> _datas;
  private Dictionary<int, ParliamentHeroCardInfo> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ParliamentHeroCardInfo, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ParliamentHeroCardInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._parliamentHeroCardInfoList.Add(enumerator.Current);
    }
    this._parliamentHeroCardInfoList.Sort((Comparison<ParliamentHeroCardInfo>) ((a, b) => int.Parse(a.id).CompareTo(int.Parse(b.id))));
  }

  public ParliamentHeroCardInfo GetParliamentHeroCardInfoByItemId(int itemId)
  {
    List<ParliamentHeroCardInfo> parliamentHeroCardInfoList = new List<ParliamentHeroCardInfo>();
    Dictionary<string, ParliamentHeroCardInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && enumerator.Current.heroCard == itemId)
        parliamentHeroCardInfoList.Add(enumerator.Current);
    }
    if (parliamentHeroCardInfoList.Count == 1)
      return parliamentHeroCardInfoList[0];
    return (ParliamentHeroCardInfo) null;
  }

  public List<ParliamentHeroCardInfo> GetParliamentHeroCardInfoList()
  {
    return this._parliamentHeroCardInfoList;
  }

  public ParliamentHeroCardInfo Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ParliamentHeroCardInfo) null;
  }

  public ParliamentHeroCardInfo Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ParliamentHeroCardInfo) null;
  }
}
