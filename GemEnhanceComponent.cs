﻿// Decompiled with JetBrains decompiler
// Type: GemEnhanceComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UnityEngine;

public class GemEnhanceComponent : ComponentRenderBase
{
  public System.Action<GemEnhanceComponent> OnToggleChangedHandler;
  public UITexture texture;
  public UILabel lblGemLevel;
  public UILabel lblGemName;
  public UILabel lblExp;
  public UIToggle toggle;
  public UITexture backgroud;
  public Transform border;
  public GemData gemData;

  public override void Init()
  {
    GemEnhanceComponentData data1 = this.data as GemEnhanceComponentData;
    if (data1 == null)
      return;
    this.gemData = data1.gemData;
    if (this.gemData == null)
      return;
    ConfigEquipmentGemInfo data2 = ConfigManager.inst.DB_EquipmentGem.GetData(this.gemData.ConfigId);
    if (data2 == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(data2.itemID);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.Build((UIWidget) this.texture, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemBackground(this.backgroud, itemStaticInfo.internalId);
    this.lblGemLevel.text = string.Format("Lv.{0}", (object) data2.level);
    this.lblGemName.text = itemStaticInfo.LocName;
    Utils.SetItemName(this.lblGemName, itemStaticInfo.internalId);
    this.lblExp.text = string.Format("{0} +{1}", (object) ScriptLocalization.Get("forge_armory_gemstones_exp_name", true), (object) GemManager.Instance.GetGemTotalExp(this.gemData));
  }

  public override void Dispose()
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture);
  }

  public void OnToggleChanged()
  {
    if (this.OnToggleChangedHandler == null)
      return;
    this.OnToggleChangedHandler(this);
  }

  public bool Select
  {
    set
    {
      this.toggle.value = value;
    }
    get
    {
      return this.toggle.value;
    }
  }
}
