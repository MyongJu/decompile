﻿// Decompiled with JetBrains decompiler
// Type: PlayerMessageEntry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class PlayerMessageEntry
{
  public string blockedKey = "mail_blocked_send_mail_fail_notice";
  public long time;
  public string content;
  public long fromUID;
  public string fromPortrait;
  public string fromIcon;
  public int fromLordTitleId;
  public bool sendToAll;
  public bool blocked;

  public string Content
  {
    get
    {
      if (this.blocked)
        return Utils.XLAT(this.blockedKey);
      return this.content;
    }
  }
}
