﻿// Decompiled with JetBrains decompiler
// Type: GemContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using GemConstant;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class GemContent : MonoBehaviour
{
  public GameObject empty;
  public UIGrid grid;
  public EquipmentGemComponent template;
  public UIScrollView scrollView;
  public GemInfoContent gemInfo;
  public UILabel lblAmount;
  private List<EquipmentGemComponent> widgetList;
  private List<GemData> dataList;
  private EquipmentGemComponent current;
  private long currentSelectInfoID;
  private SlotType slotType;
  private bool isDirty;

  private SlotType ToSlotType(BagType bagType)
  {
    SlotType slotType = SlotType.lord;
    switch (bagType)
    {
      case BagType.Hero:
        slotType = SlotType.lord;
        break;
      case BagType.DragonKnight:
        slotType = SlotType.dragon_knight;
        break;
    }
    return slotType;
  }

  private void OnEnable()
  {
    DBManager.inst.DB_Gems.onDataChanged += new System.Action<long, long>(this.OnGemDataChanged);
    DBManager.inst.DB_Gems.onDataRemove += new System.Action<long, long>(this.OnGemDataChanged);
  }

  private void OnDisable()
  {
    if (!GameEngine.IsAvailable)
      return;
    DBManager.inst.DB_Gems.onDataChanged -= new System.Action<long, long>(this.OnGemDataChanged);
    DBManager.inst.DB_Gems.onDataRemove -= new System.Action<long, long>(this.OnGemDataChanged);
  }

  private void OnGemDataChanged(long uid, long gemId)
  {
    this.isDirty = true;
  }

  private void Update()
  {
    if (!this.isDirty)
      return;
    this.UpdateUI();
    this.isDirty = false;
  }

  public void Init(BagType bagType, long selectedGemId)
  {
    this.slotType = this.ToSlotType(bagType);
    this.UpdateUI();
  }

  private int SortMethod(GemData x, GemData y)
  {
    if (x.GemType != y.GemType)
      return (x.GemType != GemType.EXP ? (int) x.GemType : 99).CompareTo(y.GemType != GemType.EXP ? (int) y.GemType : 99);
    if (x.Level != y.Level)
      return -x.Level.CompareTo(y.Level);
    if (x.ConfigId == y.ConfigId)
      return x.ID.CompareTo(y.ID);
    return x.ConfigId.CompareTo(y.ConfigId);
  }

  private void UpdateUI()
  {
    this.current = (EquipmentGemComponent) null;
    this.dataList = DBManager.inst.DB_Gems.GetGemsBySlotType(this.slotType);
    this.dataList.Sort(new Comparison<GemData>(this.SortMethod));
    this.empty.SetActive(this.dataList.Count == 0);
    List<GemDataWrapper> gemDataWrapper = GemManager.Instance.GetGemDataWrapper(this.dataList);
    if (this.widgetList == null)
    {
      this.widgetList = new List<EquipmentGemComponent>();
    }
    else
    {
      for (int index = 0; index < this.widgetList.Count; ++index)
        NGUITools.SetActive(this.widgetList[index].gameObject, false);
    }
    int index1 = 0;
    for (int count = gemDataWrapper.Count; index1 < count; ++index1)
    {
      EquipmentGemComponent equipmentGemComponent;
      if (index1 < this.widgetList.Count)
      {
        equipmentGemComponent = this.widgetList[index1];
      }
      else
      {
        equipmentGemComponent = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject).GetComponent<EquipmentGemComponent>();
        equipmentGemComponent.OnSelectedHandler = new System.Action<EquipmentGemComponent>(this.OnSelectedHandler);
        this.widgetList.Add(equipmentGemComponent);
      }
      NGUITools.SetActive(equipmentGemComponent.gameObject, true);
      equipmentGemComponent.FeedData((IComponentData) new EquipmentGemComponentData(gemDataWrapper[index1]));
      if (equipmentGemComponent.gemDataWrapper.GemData.ID == this.currentSelectInfoID)
      {
        this.current = equipmentGemComponent;
        this.current.Select = true;
      }
    }
    this.lblAmount.text = string.Format("{0}/{1}", (object) DBManager.inst.DB_Gems.GetSocketableGemCount(), (object) GemManager.Instance.GetGemBagCapacity());
    if ((UnityEngine.Object) this.current == (UnityEngine.Object) null && this.widgetList.Count > 0 && gemDataWrapper.Count > 0)
    {
      this.current = this.widgetList[0];
      this.current.Select = true;
      this.currentSelectInfoID = this.current.gemDataWrapper.GemData.ID;
    }
    if ((UnityEngine.Object) this.current == (UnityEngine.Object) null)
    {
      this.gemInfo.gameObject.SetActive(false);
    }
    else
    {
      this.gemInfo.gameObject.SetActive(true);
      this.gemInfo.showInfo(this.current);
    }
    this.scrollView.ResetPosition();
    this.grid.repositionNow = true;
  }

  public void Dispose()
  {
  }

  private void OnSelectedHandler(EquipmentGemComponent selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.gemDataWrapper.GemData.ID == selected.gemDataWrapper.GemData.ID)
      return;
    this.currentSelectInfoID = selected.gemDataWrapper.GemData.ID;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.gemInfo.gameObject.SetActive(true);
    this.gemInfo.showInfo(this.current);
  }

  public void OnEatGemsClicked()
  {
    UIManager.inst.OpenDlg("Gem/EquipmentGemRefineDlg", (UI.Dialog.DialogParameter) new EquipmentGemRefineDlg.Parameter()
    {
      gemId = this.currentSelectInfoID,
      slotType = this.slotType
    }, 1 != 0, 1 != 0, 1 != 0);
  }
}
