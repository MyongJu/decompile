﻿// Decompiled with JetBrains decompiler
// Type: EmojiRender
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Runtime.InteropServices;
using UnityEngine;

public abstract class EmojiRender : MonoBehaviour
{
  public abstract void Show(Emoji element, [Optional] Vector3 position, [Optional] Vector2 size);

  public abstract void Hide();
}
