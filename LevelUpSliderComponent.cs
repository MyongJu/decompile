﻿// Decompiled with JetBrains decompiler
// Type: LevelUpSliderComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LevelUpSliderComponent : ComponentRenderBase
{
  [SerializeField]
  private UISlider _Forground1;
  [SerializeField]
  private UISlider _Forground2;
  [SerializeField]
  private UISlider _Forground3;
  [SerializeField]
  private UISlider _Forground4;
  [SerializeField]
  private UILabel _ExpLabel;

  public override void Init()
  {
    base.Init();
    LevelUpSliderComponentData data = this.data as LevelUpSliderComponentData;
    if (data == null)
      return;
    if (data.IsMaxLevel)
    {
      this._ExpLabel.text = Utils.XLAT("id_uppercase_maxed");
      NGUITools.SetActive(this._Forground1.gameObject, true);
      NGUITools.SetActive(this._Forground2.gameObject, false);
      NGUITools.SetActive(this._Forground3.gameObject, false);
      NGUITools.SetActive(this._Forground4.gameObject, false);
      this._Forground1.value = 1f;
    }
    else if (data.CurLevelExpMax <= 0L || data.NewLevelExpMax <= 0L)
    {
      D.error((object) "the exp denominator is equal or little than 0");
    }
    else
    {
      this._ExpLabel.text = string.Format("{0}/{1}", (object) data.NewExp, (object) data.NewLevelExpMax);
      int curLevel = data.CurLevel;
      int newLevel = data.NewLevel;
      float num1 = Mathf.Clamp01((float) data.CurExp / (float) data.CurLevelExpMax);
      float num2 = Mathf.Clamp01((float) data.NewExp / (float) data.NewLevelExpMax);
      if (curLevel == newLevel)
      {
        NGUITools.SetActive(this._Forground1.gameObject, true);
        NGUITools.SetActive(this._Forground2.gameObject, true);
        NGUITools.SetActive(this._Forground3.gameObject, false);
        NGUITools.SetActive(this._Forground4.gameObject, false);
        this._Forground1.value = num1;
        this._Forground2.value = num2;
      }
      else if (curLevel + 1 == newLevel && (double) num2 < (double) num1)
      {
        NGUITools.SetActive(this._Forground1.gameObject, false);
        NGUITools.SetActive(this._Forground2.gameObject, true);
        NGUITools.SetActive(this._Forground3.gameObject, true);
        NGUITools.SetActive(this._Forground4.gameObject, true);
        this._Forground2.value = num2;
        this._Forground3.value = num1;
        this._Forground4.value = 1f;
      }
      else
      {
        NGUITools.SetActive(this._Forground1.gameObject, false);
        NGUITools.SetActive(this._Forground2.gameObject, true);
        NGUITools.SetActive(this._Forground3.gameObject, false);
        NGUITools.SetActive(this._Forground4.gameObject, true);
        this._Forground2.value = num2;
        this._Forground4.value = 1f;
      }
    }
  }

  public override void Dispose()
  {
    base.Dispose();
  }
}
