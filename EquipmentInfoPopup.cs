﻿// Decompiled with JetBrains decompiler
// Type: EquipmentInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class EquipmentInfoPopup : Popup
{
  public EquipmentBenefits benefitContent;
  public EquipmentComponent equipComponent;
  public UIButton euipBtn;
  private int equipmentID;
  private int enhanceLevel;
  private ConfigEquipmentMainInfo mainInfo;
  private BagType bagType;

  private void Init()
  {
    ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentID, this.enhanceLevel);
    if (idAndEnhanceLevel == null)
    {
      this.bagType = BagType.DragonKnight;
      idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentID, this.enhanceLevel);
    }
    this.benefitContent.Init(idAndEnhanceLevel.benefits);
    this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(this.equipmentID, this.enhanceLevel, this.bagType, 0L));
    this.mainInfo = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(this.equipmentID);
    this.euipBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnOkBtnClick)));
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnOkBtnClick()
  {
    this.OnCloseBtnClick();
  }

  private void Dispose()
  {
    this.benefitContent.Dispose();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.equipmentID = (orgParam as EquipmentInfoPopup.Parameter).equipmenID;
    this.enhanceLevel = (orgParam as EquipmentInfoPopup.Parameter).enhanceLevel;
    this.bagType = (orgParam as EquipmentInfoPopup.Parameter).bagType;
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int enhanceLevel = -1;
    public int equipmenID;
    public BagType bagType;
  }
}
