﻿// Decompiled with JetBrains decompiler
// Type: GemEnhanceConfirmItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GemEnhanceConfirmItem : MonoBehaviour
{
  public UILabel lblGemDesc;

  public void setGemDesc(string name)
  {
    if (!((Object) this.lblGemDesc != (Object) null))
      return;
    this.lblGemDesc.text = name;
  }
}
