﻿// Decompiled with JetBrains decompiler
// Type: PVPHospitalBanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PVPHospitalBanner : SpriteBanner
{
  public TextMesh m_Status;
  public TextMesh m_Timer;
  public TextMesh m_Healing;
  public UISpriteMeshProgressBar m_Progress;
  private long m_JobID;

  public override void UpdateUI(TileData tile)
  {
    base.UpdateUI(tile);
    string empty = string.Empty;
    string str = string.Empty;
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(tile.AllianceID);
    if (allianceData != null)
      empty += string.Format("({0})", (object) allianceData.allianceAcronym);
    AllianceHospitalData hospitalData = tile.HospitalData;
    if (hospitalData != null)
    {
      AllianceBuildingStaticInfo buildingStaticInfo = ConfigManager.inst.DB_AllianceBuildings.Get(hospitalData.ConfigId);
      empty += string.IsNullOrEmpty(hospitalData.Name) ? buildingStaticInfo.LocalName : hospitalData.Name;
      str = string.Format("({0})", (object) PVPHospitalBanner.GetStateString(hospitalData));
      this.m_JobID = hospitalData.MyJobId;
      this.m_Healing.text = string.Format("{0} {1}", (object) Utils.XLAT("alliance_hospital_status_healing"), (object) hospitalData.GetMyHealingTroopCount());
    }
    this.m_Progress.gameObject.SetActive(this.m_JobID != 0L);
    this.Name = empty;
    this.m_Status.text = str;
  }

  private void Update()
  {
    if (this.m_JobID == 0L)
    {
      this.m_Progress.gameObject.SetActive(false);
    }
    else
    {
      JobHandle job = JobManager.Instance.GetJob(this.m_JobID);
      if (!(bool) job)
        this.m_Progress.gameObject.SetActive(false);
      else if (!job.IsFinished())
      {
        int num = job.Duration();
        int time = job.LeftTime();
        this.m_Progress.SetProgress((float) (num - time) / (float) num);
        this.m_Timer.text = Utils.FormatTime(time, false, false, true);
      }
      else
      {
        this.m_Progress.SetProgress(1f);
        this.m_Timer.text = Utils.FormatTime(0, false, false, true);
      }
    }
  }

  private static string GetStateString(AllianceHospitalData hospitalData)
  {
    switch (hospitalData.CurrentState)
    {
      case AllianceHospitalData.State.UNCOMPLETE:
        return Utils.XLAT("alliance_fort_status_unfinished");
      case AllianceHospitalData.State.BUILDING:
        return Utils.XLAT("alliance_fort_status_under_construction");
      case AllianceHospitalData.State.COMPLETE:
        if (hospitalData.GetInjuredMemberCount() <= 0L)
          return Utils.XLAT("alliance_hospital_inactive_status");
        return Utils.XLAT("alliance_hospital_active_status");
      case AllianceHospitalData.State.FREEZE:
        return Utils.XLAT("alliance_hospital_inactive_status");
      default:
        return string.Empty;
    }
  }
}
