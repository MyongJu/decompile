﻿// Decompiled with JetBrains decompiler
// Type: KingdomTitleBenefitSlotRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class KingdomTitleBenefitSlotRenderer : MonoBehaviour
{
  public UILabel benefitDescription;
  public UILabel benefitPercentage;
  private int titleType;
  private string benefitKey;
  private float benefitValue;

  public void SetData(int titleType, string benefitKey, float benefitValue)
  {
    this.titleType = titleType;
    this.benefitKey = benefitKey;
    this.benefitValue = benefitValue;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int result = 0;
    int.TryParse(this.benefitKey, out result);
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[result];
    if (dbProperty != null)
      this.benefitDescription.text = dbProperty.Name;
    if (this.titleType <= 0)
    {
      this.benefitPercentage.color = Color.green;
      this.benefitDescription.color = Color.green;
      this.benefitPercentage.text = string.Format("{0}{1}{2}", (double) this.benefitValue <= 0.0 ? (object) string.Empty : (object) "+", (object) (float) ((double) this.benefitValue * 100.0), (object) "%");
    }
    else
    {
      this.benefitPercentage.color = Color.red;
      this.benefitDescription.color = Color.red;
      this.benefitPercentage.text = string.Format("{0}{1}{2}", (double) this.benefitValue <= 0.0 ? (object) string.Empty : (object) "+", (object) (float) ((double) this.benefitValue * 100.0), (object) "%");
    }
  }
}
