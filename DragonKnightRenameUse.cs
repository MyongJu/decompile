﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightRenameUse
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class DragonKnightRenameUse : ItemBaseUse
{
  public DragonKnightRenameUse(int id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  public DragonKnightRenameUse(string id, bool isShopItem = false)
    : base(id, isShopItem)
  {
  }

  protected override void CustomUse(System.Action<bool, object> resultHandler)
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightProfileRenamePopup", (Popup.PopupParameter) null);
  }
}
