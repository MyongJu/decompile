﻿// Decompiled with JetBrains decompiler
// Type: LotteryDiyBaseData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class LotteryDiyBaseData
{
  protected Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> _lotteryRewards = new Dictionary<int, LotteryDiyBaseData.LotteryDiyReward>();
  protected Dictionary<int, int> _lotterySpendNumber = new Dictionary<int, int>();
  protected Dictionary<int, LotteryDiyBaseData.RewardData> _lotteryPrevItemDict = new Dictionary<int, LotteryDiyBaseData.RewardData>();
  public const int LOTTERY_RANK_COUNT = 3;
  protected int _startTime;
  protected int _endTime;
  protected string _title;
  protected int _theme;
  protected int _spendItem;
  protected long _getItemGold;
  protected long _getItemGoldNumber;
  protected long _currentGold;
  protected long _goldClaimNumber;
  protected long _getItemDailyScore;
  protected long _getItemDailyNumber;
  protected long _currentDailyScore;
  protected bool _dailyIsClaim;

  public virtual int StartTime
  {
    get
    {
      return this._startTime;
    }
  }

  public virtual int EndTime
  {
    get
    {
      return this._endTime;
    }
  }

  public virtual string Title
  {
    get
    {
      return this._title;
    }
  }

  public virtual int Theme
  {
    get
    {
      return this._theme;
    }
  }

  public virtual string ThemeSprite
  {
    get
    {
      return "icon_diy_lottery_" + this._theme.ToString();
    }
  }

  public virtual string ThemeEffect
  {
    get
    {
      return "diy_lottery_theme_" + this._theme.ToString();
    }
  }

  public virtual int SpendItem
  {
    get
    {
      return this._spendItem;
    }
  }

  public string SpendItemName
  {
    get
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._spendItem);
      if (itemStaticInfo != null)
        return itemStaticInfo.LocName;
      return string.Empty;
    }
  }

  public virtual int SpendItemCount
  {
    get
    {
      ConsumableItemData consumableItemData = DBManager.inst.DB_Item.Get(this._spendItem);
      if (consumableItemData != null)
        return consumableItemData.quantity;
      return 0;
    }
  }

  public virtual long GetItemGold
  {
    get
    {
      return this._getItemGold;
    }
  }

  public virtual long GetItemGoldNumber
  {
    get
    {
      return this._getItemGoldNumber;
    }
  }

  public virtual long CurrentGold
  {
    get
    {
      return this._currentGold;
    }
  }

  public virtual long GoldClaimNumber
  {
    get
    {
      return this._goldClaimNumber;
    }
  }

  public virtual long GetItemDailyScore
  {
    get
    {
      return this._getItemDailyScore;
    }
  }

  public virtual long GetItemDailyNumber
  {
    get
    {
      return this._getItemDailyNumber;
    }
  }

  public virtual long CurrentDailyScore
  {
    get
    {
      return this._currentDailyScore;
    }
  }

  public virtual bool DailyIsClaim
  {
    get
    {
      return this._dailyIsClaim;
    }
  }

  public virtual Dictionary<int, LotteryDiyBaseData.LotteryDiyReward> LotteryRewards
  {
    get
    {
      return this._lotteryRewards;
    }
  }

  public virtual Dictionary<int, int> LotterySpendNumber
  {
    get
    {
      return this._lotterySpendNumber;
    }
  }

  public virtual Dictionary<int, LotteryDiyBaseData.RewardData> LotteryPrevItemDict
  {
    get
    {
      return this._lotteryPrevItemDict;
    }
  }

  public bool IsPrevLotteryFinished
  {
    get
    {
      if (this._lotteryPrevItemDict.Count == 0)
        return true;
      bool flag = true;
      Dictionary<int, LotteryDiyBaseData.RewardData>.ValueCollection.Enumerator enumerator = this._lotteryPrevItemDict.Values.GetEnumerator();
      while (enumerator.MoveNext())
        flag &= enumerator.Current.isOpen;
      return flag;
    }
  }

  public long RemainedGoldClaim
  {
    get
    {
      long num = 0;
      if (this._getItemGold != 0L)
        num = this._currentGold / this._getItemGold * this._getItemGoldNumber - this._goldClaimNumber;
      return num;
    }
  }

  public bool HasFreeReward
  {
    get
    {
      return ((true ? 1 : 0) & (this._dailyIsClaim ? 0 : (this._currentDailyScore >= this._getItemDailyScore ? 1 : 0))) != 0 | this.RemainedGoldClaim > 0L;
    }
  }

  public LotteryDiyBaseData.RewardData GetRewardDataByIndex(int index)
  {
    if (this._lotteryPrevItemDict != null && this._lotteryPrevItemDict.ContainsKey(index))
      return this._lotteryPrevItemDict[index];
    return (LotteryDiyBaseData.RewardData) null;
  }

  public int GetSpendNumberByIndex(int index)
  {
    if (this._lotterySpendNumber != null && this._lotterySpendNumber.ContainsKey(index))
      return this._lotterySpendNumber[index];
    return 0;
  }

  public int GetRewardNumByRank(int rank)
  {
    if (this._lotteryRewards != null && this._lotteryRewards.ContainsKey(rank))
      return this._lotteryRewards[rank].RewardData.Count;
    return 0;
  }

  public int GetSelectableNumByRank(int rank)
  {
    if (this._lotteryRewards != null && this._lotteryRewards[rank] != null)
      return this._lotteryRewards[rank].LimitCount;
    return 0;
  }

  public bool Decode(Hashtable data)
  {
    bool flag1 = false;
    if (data == null)
      return false;
    bool flag2 = flag1 | DatabaseTools.UpdateData(data, "start_time", ref this._startTime) | DatabaseTools.UpdateData(data, "end_time", ref this._endTime) | DatabaseTools.UpdateData(data, "title", ref this._title) | DatabaseTools.UpdateData(data, "theme", ref this._theme) | DatabaseTools.UpdateData(data, "spend_item", ref this._spendItem) | DatabaseTools.UpdateData(data, "get_item_gold", ref this._getItemGold) | DatabaseTools.UpdateData(data, "get_item_gold_number", ref this._getItemGoldNumber) | DatabaseTools.UpdateData(data, "current_gold", ref this._currentGold) | DatabaseTools.UpdateData(data, "gold_claim_number", ref this._goldClaimNumber) | DatabaseTools.UpdateData(data, "get_item_daily_score", ref this._getItemDailyScore) | DatabaseTools.UpdateData(data, "get_item_daily_number", ref this._getItemDailyNumber) | DatabaseTools.UpdateData(data, "current_daily_score", ref this._currentDailyScore) | DatabaseTools.UpdateData(data, "daily_is_claim", ref this._dailyIsClaim);
    for (int key = 1; key <= 3; ++key)
    {
      if (data[(object) ("rank" + key.ToString())] != null)
      {
        LotteryDiyBaseData.LotteryDiyReward lotteryDiyReward = new LotteryDiyBaseData.LotteryDiyReward();
        lotteryDiyReward.Decode(data[(object) ("rank" + key.ToString())] as Hashtable);
        this._lotteryRewards.Remove(key);
        this._lotteryRewards.Add(key, lotteryDiyReward);
        flag2 = ((flag2 ? 1 : 0) | 1) != 0;
      }
    }
    if (data[(object) "spend_number"] != null)
    {
      Hashtable hashtable = data[(object) "spend_number"] as Hashtable;
      if (hashtable != null)
      {
        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result1 = 0;
          int result2 = 0;
          int.TryParse(enumerator.Current.ToString(), out result1);
          int.TryParse(hashtable[(object) enumerator.Current.ToString()].ToString(), out result2);
          if (this._lotterySpendNumber.ContainsKey(result1))
            this._lotterySpendNumber.Remove(result1);
          this._lotterySpendNumber.Add(result1, result2);
        }
      }
      flag2 = ((flag2 ? 1 : 0) | 1) != 0;
    }
    if (data[(object) "item_list"] != null)
    {
      Hashtable hashtable = data[(object) "item_list"] as Hashtable;
      if (hashtable != null)
      {
        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
        while (enumerator.MoveNext())
        {
          int result = 0;
          int.TryParse(enumerator.Current.ToString(), out result);
          int outData1 = 0;
          int outData2 = 0;
          int outData3 = 0;
          int outData4 = 0;
          int outData5 = 0;
          Hashtable inData = hashtable[(object) enumerator.Current.ToString()] as Hashtable;
          if (inData != null)
          {
            DatabaseTools.UpdateData(inData, "id", ref outData1);
            DatabaseTools.UpdateData(inData, "item_id", ref outData2);
            DatabaseTools.UpdateData(inData, "number", ref outData3);
            DatabaseTools.UpdateData(inData, "is_open", ref outData4);
            DatabaseTools.UpdateData(inData, "step", ref outData5);
          }
          if (this._lotteryPrevItemDict.ContainsKey(result))
            this._lotteryPrevItemDict.Remove(result);
          this._lotteryPrevItemDict.Add(result, new LotteryDiyBaseData.RewardData(outData1, outData2, outData3, outData5, outData4 == 1));
        }
      }
      else
        this._lotteryPrevItemDict.Clear();
      flag2 = ((flag2 ? 1 : 0) | 1) != 0;
    }
    return flag2;
  }

  public struct Params
  {
    public const string START_TIME = "start_time";
    public const string END_TIME = "end_time";
    public const string TITLE = "title";
    public const string THEME = "theme";
    public const string REWARD_RANK = "rank";
    public const string SPEND_ITEM = "spend_item";
    public const string GET_ITEM_GOLD = "get_item_gold";
    public const string GET_ITEM_GOLD_NUMBER = "get_item_gold_number";
    public const string CURRENT_GOLD = "current_gold";
    public const string GOLD_CLAIM_NUMBER = "gold_claim_number";
    public const string GET_ITEM_DAILY_SCORE = "get_item_daily_score";
    public const string GET_ITEM_DAILY_NUMBER = "get_item_daily_number";
    public const string CURRENT_DAILY_SCORE = "current_daily_score";
    public const string DAILY_IS_CLAIM = "daily_is_claim";
    public const string SPEND_NUMBER = "spend_number";
    public const string ITEM_LIST = "item_list";
  }

  public class LotteryDiyReward
  {
    private ArrayList _itemList = new ArrayList();
    private List<LotteryDiyBaseData.RewardData> _rewardData = new List<LotteryDiyBaseData.RewardData>();
    private int _limitCount;

    public int LimitCount
    {
      get
      {
        return this._limitCount;
      }
    }

    public List<LotteryDiyBaseData.RewardData> RewardData
    {
      get
      {
        this._rewardData.Sort((Comparison<LotteryDiyBaseData.RewardData>) ((a, b) => a.id.CompareTo(b.id)));
        return this._rewardData;
      }
    }

    public bool Decode(Hashtable data)
    {
      bool flag1 = false;
      if (data == null)
        return false;
      bool flag2 = flag1 | DatabaseTools.UpdateData(data, "limit", ref this._limitCount);
      if (data[(object) "item_list"] != null)
      {
        flag2 |= DatabaseTools.CheckAndParseOrgData((object) (data[(object) "item_list"] as ArrayList), out this._itemList);
        if (this._itemList != null)
        {
          for (int index = 0; index < this._itemList.Count; ++index)
          {
            Hashtable inData = this._itemList[index] as Hashtable;
            if (inData != null)
            {
              int outData1 = 0;
              int outData2 = 0;
              int outData3 = 0;
              DatabaseTools.UpdateData(inData, "id", ref outData1);
              DatabaseTools.UpdateData(inData, "item_id", ref outData2);
              DatabaseTools.UpdateData(inData, "number", ref outData3);
              this._rewardData.Add(new LotteryDiyBaseData.RewardData(outData1, outData2, outData3, -1, false));
            }
          }
        }
      }
      return flag2;
    }

    public struct Params
    {
      public const string LIMIT = "limit";
      public const string ITEM_LIST = "item_list";
    }
  }

  public class RewardData
  {
    public int id;
    public int itemId;
    public int number;
    public bool isOpen;
    public int step;

    public RewardData(int id, int itemId, int number, int step = -1, bool isOpen = false)
    {
      this.id = id;
      this.itemId = itemId;
      this.number = number;
      this.step = step;
      this.isOpen = isOpen;
    }
  }
}
