﻿// Decompiled with JetBrains decompiler
// Type: DragonEgg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DragonEgg : MonoBehaviour
{
  public int stepnum = 3;
  private const string vfxpath = "Prefab/VFX/DragonEgg/";
  private const string vfxprefix = "fx_dragon_light_";
  private long vfxhandle;

  public void UpdateStates(int lefttime, int duration)
  {
    this.StartCoroutine(this.PlayState((float) lefttime, this.stepnum));
    float num1 = (float) lefttime / (float) duration;
    int index1 = 0;
    for (int index2 = this.stepnum - 1; index2 > 0; --index2)
    {
      float num2 = (float) index2 / (float) this.stepnum;
      if ((double) num1 < (double) num2)
        index1 = this.stepnum - index2;
      float time = (float) lefttime - (1f - num2) * (float) duration;
      if ((double) time > 0.0)
        this.StartCoroutine(this.PlayState(time, index2));
    }
    if (index1 == 0)
      return;
    this.GetComponent<Animator>().Play("step" + index1.ToString(), -1, 0.99f);
    this.PlayEffect(index1);
  }

  [DebuggerHidden]
  public IEnumerator PlayState(float time, int index)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonEgg.\u003CPlayState\u003Ec__Iterator50()
    {
      time = time,
      index = index,
      \u003C\u0024\u003Etime = time,
      \u003C\u0024\u003Eindex = index,
      \u003C\u003Ef__this = this
    };
  }

  private void PlayEffect(int index)
  {
    if (this.vfxhandle != 0L)
      VfxManager.Instance.Delete(this.vfxhandle);
    this.vfxhandle = VfxManager.Instance.CreateAndPlay("Prefab/VFX/DragonEgg/fx_dragon_light_" + index.ToString(), this.transform.parent);
  }

  public void DisposeEffect()
  {
    if (this.vfxhandle == 0L)
      return;
    VfxManager.Instance.Delete(this.vfxhandle);
  }
}
