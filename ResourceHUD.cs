﻿// Decompiled with JetBrains decompiler
// Type: ResourceHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResourceHUD : MonoBehaviour
{
  public UISprite mMeter;
  public UILabel mLabel;
  private long _amount;
  private long _totalAmount;

  public void SetAmounts(long amount, long totalAmount)
  {
    this._amount = amount;
    this._totalAmount = totalAmount;
    this.mLabel.text = Utils.FormatShortThousandsLong(amount);
  }

  public long amount
  {
    get
    {
      return this._amount;
    }
    set
    {
      this._amount = value;
      this.SetAmounts(this._amount, this._totalAmount);
    }
  }

  public long totalAmount
  {
    get
    {
      return this._totalAmount;
    }
    set
    {
      this._totalAmount = value;
      this.SetAmounts(this._amount, this._totalAmount);
    }
  }
}
