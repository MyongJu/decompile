﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceBoss
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceBoss
{
  private List<AllianceBossInfo> allianceBossInfoList = new List<AllianceBossInfo>();
  private Dictionary<string, AllianceBossInfo> datas;
  private Dictionary<int, AllianceBossInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AllianceBossInfo, string>(res as Hashtable, "id", out this.datas, out this.dicByUniqueId);
    Dictionary<string, AllianceBossInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this.allianceBossInfoList.Add(enumerator.Current);
    }
    this.allianceBossInfoList.Sort(new Comparison<AllianceBossInfo>(this.SortByID));
  }

  public List<AllianceBossInfo> GetAllianceBossInfoList()
  {
    return this.allianceBossInfoList;
  }

  public AllianceBossInfo Get(int interalId)
  {
    if (this.dicByUniqueId.ContainsKey(interalId))
      return this.dicByUniqueId[interalId];
    return (AllianceBossInfo) null;
  }

  public AllianceBossInfo Get(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceBossInfo) null;
  }

  public int SortByID(AllianceBossInfo a, AllianceBossInfo b)
  {
    return a.internalId.CompareTo(b.internalId);
  }
}
