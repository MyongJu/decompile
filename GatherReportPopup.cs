﻿// Decompiled with JetBrains decompiler
// Type: GatherReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UI;
using UnityEngine;

public class GatherReportPopup : BaseReportPopup
{
  private WarMessage message;
  public UILabel tileNameLabel;
  public ResourceComponent resourceComponent;
  public GatherItemComponent gatherItemComponent;
  public GameObject items;
  public TableContainer tableContainer;

  private void Init()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/gather_report_header", (System.Action<bool>) null, true, false, string.Empty);
    this.ConfigInfo();
    this.ConfigResources();
    this.ConfigReward();
    this.tableContainer.ResetPosition();
  }

  private void ConfigInfo()
  {
    if (this.param.hashtable.ContainsKey((object) "resource_id"))
    {
      Tile_StatisticsInfo data = ConfigManager.inst.DB_Tile_Statistics.GetData(int.Parse(this.param.hashtable[(object) "resource_id"].ToString()));
      if (data != null)
        this.tileNameLabel.text = "Lv." + (object) data.Level + " " + Utils.XLAT(data.Tile_Name_LOC_ID);
      else
        this.tileNameLabel.text = string.Empty;
    }
    else
      this.tileNameLabel.text = string.Empty;
  }

  private void ConfigResources()
  {
    Hashtable source = this.param.hashtable[(object) "resource"] as Hashtable;
    if (source != null)
    {
      this.resourceComponent.FeedData<ResourceComponentData>(source);
      this.resourceComponent.gameObject.SetActive(true);
    }
    else
      this.resourceComponent.gameObject.SetActive(false);
  }

  private void ConfigReward()
  {
    if (this.param.hashtable.ContainsKey((object) "dragon_attr") || this.param.hashtable.ContainsKey((object) "rewards") || this.param.hashtable.ContainsKey((object) "kingdom_coin"))
    {
      this.items.SetActive(true);
      this.gatherItemComponent.FeedData((IComponentData) new GatherItemComponentData(this.param.hashtable[(object) "dragon_attr"] as Hashtable, this.param.hashtable[(object) "rewards"] as Hashtable, this.param.hashtable));
    }
    else
      this.items.SetActive(false);
  }

  public void OnLocationClicked()
  {
    WarMessage mail = this.param.mail as WarMessage;
    this.GoToTargetPlace(mail.k, mail.x, mail.y);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
