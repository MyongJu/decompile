﻿// Decompiled with JetBrains decompiler
// Type: AllianceTradePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;

public class AllianceTradePopup : BaseReportPopup
{
  public const string iconpath = "Texture/BuildingConstruction/";
  private AllianceTradePopup.ATData atd;
  public Icon ai;
  public UILabel content;
  public RewardItemComponent ric;

  private void Init()
  {
    this.ai.FeedData((IComponentData) new IconData(UserData.GetPortraitIconPath(this.atd.portrait), this.atd.icon, this.atd.lord_title, new string[2]
    {
      this.atd.name,
      " X:" + this.atd.x.ToString() + " Y:" + this.atd.y.ToString()
    }));
    this.content.text = this.param.mail.GetBodyString();
    this.AnalyzeRewards();
  }

  private void AnalyzeRewards()
  {
    List<IconData> data = new List<IconData>();
    if (this.atd.resource != null)
    {
      long src;
      this.atd.resource.TryGetValue("food", out src);
      IconData iconData1 = new IconData("Texture/BuildingConstruction/food_icon", new string[2]
      {
        ScriptLocalization.Get("id_food", true),
        "+" + Utils.FormatShortThousandsLong(src)
      });
      data.Add(iconData1);
      this.atd.resource.TryGetValue("wood", out src);
      IconData iconData2 = new IconData("Texture/BuildingConstruction/wood_icon", new string[2]
      {
        ScriptLocalization.Get("id_wood", true),
        "+" + Utils.FormatShortThousandsLong(src)
      });
      data.Add(iconData2);
      this.atd.resource.TryGetValue("ore", out src);
      IconData iconData3 = new IconData("Texture/BuildingConstruction/ore_icon", new string[2]
      {
        ScriptLocalization.Get("id_ore", true),
        "+" + Utils.FormatShortThousandsLong(src)
      });
      data.Add(iconData3);
      this.atd.resource.TryGetValue("silver", out src);
      IconData iconData4 = new IconData("Texture/BuildingConstruction/silver_icon", new string[2]
      {
        ScriptLocalization.Get("id_silver", true),
        "+" + Utils.FormatShortThousandsLong(src)
      });
      data.Add(iconData4);
    }
    this.ric.FeedData((IComponentData) new IconListComponentData(data));
  }

  public void OnLocationCliked()
  {
    this.GoToTargetPlace(this.atd.k, this.atd.x, this.atd.y);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.atd = JsonReader.Deserialize<AllianceTradePopup.ATData>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class ATData
  {
    public int k;
    public int x;
    public int y;
    public int portrait;
    public string icon;
    public int lord_title;
    public string name;
    public Dictionary<string, long> resource;
  }
}
