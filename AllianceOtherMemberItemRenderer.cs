﻿// Decompiled with JetBrains decompiler
// Type: AllianceOtherMemberItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceOtherMemberItemRenderer : MonoBehaviour, IMemberItemRenderer, IOnlineUpdateable
{
  public UITexture m_MemberPortrait;
  public UITexture m_MemberCrown;
  public UILabel m_MemberName;
  public UILabel m_MemberPower;
  private AllianceData m_AllianceData;
  private UserData m_UserData;
  private int m_Title;
  private bool m_Nominate;

  public void SetData(AllianceData allianceData, UserData userData, int title)
  {
    this.m_AllianceData = allianceData;
    this.m_UserData = userData;
    this.m_Title = title;
    this.UpdateUI();
  }

  public void SetNominate(bool nominate)
  {
    this.m_Nominate = nominate;
  }

  public void OnItemClick()
  {
    if (this.m_Nominate)
    {
      UIManager.inst.OpenPopup("DoubleButtonPopup", (Popup.PopupParameter) new DoubleButtonPopup.Parameter()
      {
        title = ScriptLocalization.Get("id_uppercase_confirm", true),
        description = ScriptLocalization.Get("throne_king_appoint_confirm", true),
        leftButtonClickEvent = (System.Action) null,
        rightButtonClickEvent = (System.Action) (() => RequestManager.inst.SendRequest("wonder:appointKing", new Hashtable()
        {
          {
            (object) "target_uid",
            (object) this.m_UserData.uid
          }
        }, (System.Action<bool, object>) null, true))
      });
    }
    else
    {
      AllianceMemberOptionPopup.Parameter parameter = new AllianceMemberOptionPopup.Parameter();
      parameter.targetUserData = this.m_UserData;
      parameter.targetTitle = this.m_Title;
      AllianceMemberData allianceMemberData = this.m_AllianceData.members.Get(PlayerData.inst.uid);
      parameter.yourTitle = allianceMemberData == null ? 0 : allianceMemberData.title;
      parameter.yourUserData = PlayerData.inst.userData;
      UIManager.inst.OpenPopup("Alliance/AllianceMemberOptionPopup", (Popup.PopupParameter) parameter);
    }
  }

  public void UpdateOnlineStatus(List<long> onlineStatus)
  {
  }

  private void UpdateUI()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_MemberCrown, AllianceUtilities.GetCrownIconPath(this.m_Title), (System.Action<bool>) null, true, false, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_MemberPortrait, this.m_UserData.PortraitPath, (System.Action<bool>) null, true, false, string.Empty);
    this.m_MemberName.text = this.m_UserData.userName;
    this.m_MemberPower.text = Utils.FormatThousands(this.m_UserData.power.ToString());
  }

  GameObject IOnlineUpdateable.get_gameObject()
  {
    return this.gameObject;
  }
}
