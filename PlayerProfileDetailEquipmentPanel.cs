﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfileDetailEquipmentPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class PlayerProfileDetailEquipmentPanel : MonoBehaviour
{
  public PlayerProfileDetailEquipmentSlot[] m_Slots;
  public GameObject m_RootNode;
  public GameObject m_ClosedTip;

  private void Start()
  {
    this.m_RootNode.SetActive(false);
    this.m_ClosedTip.SetActive(false);
  }

  public void SetData(UserData userData)
  {
    bool flag = userData.IsSettingOn(1);
    this.m_RootNode.SetActive(flag);
    this.m_ClosedTip.SetActive(!flag);
    if (this.m_Slots == null)
      return;
    for (int index = 0; index < this.m_Slots.Length; ++index)
      this.m_Slots[index].SetData(userData);
  }
}
