﻿// Decompiled with JetBrains decompiler
// Type: TradingHallSellGoodSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class TradingHallSellGoodSlot : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelItemUnitPrice;
  [SerializeField]
  private UILabel _labelItemCount;
  [SerializeField]
  private UILabel _labelEquipmentEnhanced;
  [SerializeField]
  private GameObject _itemInvalidNode;
  [SerializeField]
  private ItemIconRenderer _itemIconRenderer;
  [SerializeField]
  public UIButton _buttonValidRetrieve;
  [SerializeField]
  private UIButton _buttonInvalidRetrieve;
  [SerializeField]
  private UIButton _buttonAgainSell;
  private TradingHallData.SellGoodData _sellGoodData;
  private int _sellIndex;
  public System.Action OnShelfGoodSuccess;

  public void SetData(TradingHallData.SellGoodData sellGoodData, int sellIndex = 0)
  {
    this._sellGoodData = sellGoodData;
    this._sellIndex = sellIndex;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
    this.UpdateUI();
  }

  public void ClearData()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  public void OnRetrieveBtnPressed()
  {
    if (this._sellGoodData == null)
      return;
    TradingHallPayload.Instance.RetrieveGood(this._sellGoodData.ExchangeId, this._sellGoodData.EquipmentId, this._sellGoodData.Price, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      TradingHallPayload.Instance.ShowBasicToast("toast_exchange_hall_expired_item_returned", this._sellGoodData.ExchangeId, this._sellGoodData.Amount);
    }));
  }

  public void OnAgainSellBtnPressed()
  {
    if (TradingHallPayload.Instance.CurrentShelfCount >= TradingHallPayload.Instance.MaxShelfCount)
      UIManager.inst.toast.Show(Utils.XLAT("toast_exchange_hall_listed_items_limit"), (System.Action) null, 4f, true);
    else
      UIManager.inst.OpenPopup("TradingHall/TradingHallSellPopup", (Popup.PopupParameter) new TradingHallSellPopup.Parameter()
      {
        isResell = true,
        sellGoodData = this._sellGoodData,
        onSellSuccess = (System.Action) (() =>
        {
          if (this.OnShelfGoodSuccess == null)
            return;
          this.OnShelfGoodSuccess();
        })
      });
  }

  public void OnItemPress()
  {
    if (this._sellGoodData == null)
      return;
    Utils.DelayShowTip(this._sellGoodData.ItemId, this._itemIconRenderer.transform, (long) this._sellGoodData.EquipmentId, 0L, this._sellGoodData.EquipmentEnhanced);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    if (this._sellGoodData == null)
      return;
    Utils.ShowItemTip(this._sellGoodData.ItemId, this._itemIconRenderer.transform, (long) this._sellGoodData.EquipmentId, 0L, this._sellGoodData.EquipmentEnhanced);
  }

  private void UpdateUI()
  {
    if (this._sellGoodData == null)
      return;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this._sellGoodData.ItemId);
    if (itemStaticInfo == null)
      return;
    this._labelItemName.text = itemStaticInfo.LocName;
    this._labelEquipmentEnhanced.text = this._sellGoodData.EquipmentEnhanced <= 0 ? string.Empty : "+" + this._sellGoodData.EquipmentEnhanced.ToString();
    this._labelItemName.text += this._labelEquipmentEnhanced.text;
    if (!this._sellGoodData.IsValid)
      this._labelItemName.text += string.Format("[FF0000]({0})[-]", (object) Utils.XLAT("id_expired"));
    EquipmentManager.Instance.ConfigQualityLabelWithColor(this._labelItemName, itemStaticInfo.Quality);
    this._labelItemUnitPrice.text = Utils.FormatThousands(this._sellGoodData.Price.ToString());
    this._labelItemCount.text = this._sellGoodData.Amount.ToString();
    if (this._itemIconRenderer.ItemId != this._sellGoodData.ItemId)
      this._itemIconRenderer.SetData(this._sellGoodData.ItemId, string.Empty, true);
    this.UpdateValidStatus();
  }

  private void UpdateValidStatus()
  {
    if (this._sellGoodData == null)
      return;
    NGUITools.SetActive(this._buttonInvalidRetrieve.gameObject, !this._sellGoodData.IsValid);
    NGUITools.SetActive(this._buttonAgainSell.gameObject, !this._sellGoodData.IsValid);
    NGUITools.SetActive(this._buttonValidRetrieve.gameObject, this._sellGoodData.IsValid);
  }

  private void OnSecondEvent(int time)
  {
    if (!((UnityEngine.Object) this.gameObject != (UnityEngine.Object) null))
      return;
    this.UpdateUI();
  }
}
