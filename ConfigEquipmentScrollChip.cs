﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentScrollChip
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigEquipmentScrollChip
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, ConfigEquipmentScrollChipInfo> datas;
  private Dictionary<int, ConfigEquipmentScrollChipInfo> dicByUniqueId;
  private Dictionary<int, ConfigEquipmentScrollChipInfo> dicByItemId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigEquipmentScrollChipInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.dicByItemId = new Dictionary<int, ConfigEquipmentScrollChipInfo>();
    Dictionary<int, ConfigEquipmentScrollChipInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
      this.dicByItemId.Add(enumerator.Current.Value.itemID, enumerator.Current.Value);
  }

  public ConfigEquipmentScrollChipInfo GetDataByItemID(int itemID)
  {
    return this.dicByItemId[itemID];
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigEquipmentScrollChipInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, ConfigEquipmentScrollChipInfo>) null;
    }
    if (this.dicByItemId == null)
      return;
    this.dicByItemId.Clear();
    this.dicByItemId = (Dictionary<int, ConfigEquipmentScrollChipInfo>) null;
  }
}
