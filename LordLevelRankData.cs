﻿// Decompiled with JetBrains decompiler
// Type: LordLevelRankData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class LordLevelRankData : PlayerRankData
{
  public int LordLevel { get; set; }

  public long LordExp { get; set; }

  public override string Image
  {
    get
    {
      return UserData.GetPortraitIconPath(this.Portrait);
    }
  }
}
