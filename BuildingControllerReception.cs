﻿// Decompiled with JetBrains decompiler
// Type: BuildingControllerReception
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class BuildingControllerReception : BuildingControllerNew
{
  public Transform bigtimerchestroot;
  private bool _dispose;
  private CollectionUI cui;
  private BigTimerChestInfo cbtc;
  private int remiantime;
  private int endtime;

  public override void InitBuilding()
  {
    base.InitBuilding();
    this.onBuildingSelected += new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    MessageHub.inst.GetPortByAction("big_timer_chest").AddEvent(new System.Action<object>(this.OnBigTimerChestArrive));
    MessageHub.inst.GetPortByAction("gift:getBigTimeChest").SendLoader((Hashtable) null, (System.Action<bool, object>) ((success, obj) =>
    {
      if (this._dispose || !success || (obj == null || (obj as Hashtable).Contains((object) "ret")))
        return;
      this.OpenCollectionUI(obj as Hashtable);
    }), true, false);
  }

  public override void Dispose()
  {
    base.Dispose();
    this.onBuildingSelected -= new BuildingController.OnBuildingSelected(this.OnBuildingSelected);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    MessageHub.inst.GetPortByAction("big_timer_chest").RemoveEvent(new System.Action<object>(this.OnBigTimerChestArrive));
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.cui))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.cui.gameObject);
    this.cui = (CollectionUI) null;
  }

  private void OnDestroy()
  {
    this._dispose = true;
  }

  private void OnBigTimerChestArrive(object obj)
  {
    Hashtable ht = obj as Hashtable;
    if (ht == null)
      return;
    this.OpenCollectionUI(ht);
  }

  private void OpenCollectionUI(Hashtable ht)
  {
    GameObject attachment = AssetManager.Instance.HandyLoad("Prefab/City/TimeRemainAttachment", (System.Type) null) as GameObject;
    TimeRemainAttachment.TRAData traData = new TimeRemainAttachment.TRAData();
    traData.remainTime = int.Parse(ht[(object) "expiration"].ToString());
    this.remiantime = traData.remainTime;
    traData.endTime = NetServerTime.inst.ServerTimestamp + traData.remainTime;
    this.endtime = traData.endTime;
    this.cbtc = ConfigManager.inst.DB_BigTimerChest.GetData(ht[(object) "drop_id"].ToString());
    CollectionUI collectionUi = CollectionUIManager.Instance.OpenCollectionUI(this.bigtimerchestroot, new CityCircleBtnPara(AtlasType.Gui_2, "big_timer_icon", "id_gold_speedup", new EventDelegate(new EventDelegate.Callback(this.OpenCollectionPopup)), true, (string) null, attachment, (CityTouchCircleBtnAttachment.AttachmentData) traData, false)
    {
      closeAfterClick = false
    }, CollectionUI.CollectionType.TrainSoldier);
    if ((UnityEngine.Object) null != (UnityEngine.Object) collectionUi)
    {
      this.cui = collectionUi;
      this.cui.SetSmallCollider();
    }
    this.AddEventHandler();
  }

  private void OpenCollectionPopup()
  {
    if (this.cbtc == null)
      return;
    int rewardId = this.cbtc.reward_id;
    int rewardValue = this.cbtc.reward_value;
    BigTimerChestCollectPopup chestCollectPopup = UIManager.inst.OpenPopup("BigTimerChestCollectPopup", (Popup.PopupParameter) new BigTimerChestCollectPopup.Parameter()
    {
      itemId = rewardId,
      itemCount = rewardValue,
      time = (this.endtime - NetServerTime.inst.ServerTimestamp),
      endTime = this.endtime
    }) as BigTimerChestCollectPopup;
    if (!((UnityEngine.Object) null != (UnityEngine.Object) chestCollectPopup))
      return;
    chestCollectPopup.onChestOpen = new System.Action(this.CloseCollectionUI);
  }

  private void CloseCollectionUI()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.cui)
      this.cui.Close();
    this.RemoveEventHandler();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int time)
  {
    this.remiantime = this.endtime - NetServerTime.inst.ServerTimestamp;
    if (this.remiantime != 0)
      return;
    this.CloseCollectionUI();
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  public override void AddActionButton(ref List<CityCircleBtnPara> ftl)
  {
    base.AddActionButton(ref ftl);
    ftl.Add(this.CCBPair["sign"]);
  }

  private void OnBuildingSelected()
  {
    if (!Application.isEditor)
      FunplusBi.Instance.TraceEvent("clickbuilding", JsonWriter.Serialize((object) new BuildingControllerNew.BIFormat()
      {
        d_c1 = new BuildingControllerNew.BIClick()
        {
          key = "Reception",
          value = "click"
        }
      }));
    AudioManager.Instance.StopAndPlaySound("sfx_city_click_signin_rewards");
    UIManager.inst.OpenPopup("Sign/SignPopup", (Popup.PopupParameter) null);
  }
}
