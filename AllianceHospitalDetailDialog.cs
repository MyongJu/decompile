﻿// Decompiled with JetBrains decompiler
// Type: AllianceHospitalDetailDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceHospitalDetailDialog : UI.Dialog
{
  private List<AllianceHospitalDetailRenderer> m_ItemList = new List<AllianceHospitalDetailRenderer>();
  public GameObject m_PlayerPrefab;
  public UILabel m_Title;
  public UILabel m_InjuredTroops;
  public UILabel m_InjuredMembers;
  public UIWidget m_Focus;
  public UIButton m_Demolish;
  public UIScrollView m_ScrollView;
  public UITable m_Table;
  private AllianceHospitalDetailDialog.Parameter m_Parameter;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as AllianceHospitalDetailDialog.Parameter;
    PVPSystem.Instance.Map.FocusTile(PVPMapData.MapData.ConvertTileKXYToWorldPosition(this.m_Parameter.tileData.Location), this.m_Focus, 0.0f);
    UIManager.inst.CloseKingdomTouchCircle();
    this.UpdateUI();
    this.UpdateList();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ClearList();
  }

  public void OnDemolish()
  {
    string content = string.Format(ScriptLocalization.Get("alliance_hospital_demolish_tip", true), (object) ScriptLocalization.Get(AllianceBuildUtils.GetAllianceBuildingName(TileType.AllianceHospital, this.m_Parameter.tileData.Location), true));
    string title = ScriptLocalization.Get("id_uppercase_warning", true);
    string left = ScriptLocalization.Get("id_uppercase_confirm", true);
    string right = ScriptLocalization.Get("id_uppercase_cancel", true);
    UIManager.inst.ShowConfirmationBox(title, content, left, right, ChooseConfirmationBox.ButtonState.YES_LEFT | ChooseConfirmationBox.ButtonState.NO_RIGHT, new System.Action(this.ConfirmDemolish), (System.Action) null, (System.Action) null);
  }

  private void ConfirmDemolish()
  {
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.m_Parameter.tileData.Location.K;
    postData[(object) "x"] = (object) this.m_Parameter.tileData.Location.X;
    postData[(object) "y"] = (object) this.m_Parameter.tileData.Location.Y;
    MessageHub.inst.GetPortByAction("Map:takeBackAllianceBuilding").SendRequest(postData, (System.Action<bool, object>) null, true);
    this.OnCloseButtonPress();
  }

  private void UpdateUI()
  {
    AllianceHospitalData hospitalData = this.m_Parameter.hospitalData;
    DBManager.inst.DB_Alliance.Get(hospitalData.AllianceID);
    this.m_Title.text = Utils.XLAT("alliance_hospital_uppercase_name");
    this.m_InjuredTroops.text = hospitalData.GetInjuredTroopCount().ToString();
    this.m_InjuredMembers.text = hospitalData.GetInjuredMemberCount().ToString();
    this.m_Demolish.isEnabled = Utils.IsR5Member();
  }

  private void ClearList()
  {
    using (List<AllianceHospitalDetailRenderer>.Enumerator enumerator = this.m_ItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AllianceHospitalDetailRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_ItemList.Clear();
  }

  private void UpdateList()
  {
    this.ClearList();
    int num = 0;
    Dictionary<long, Dictionary<string, AllianceHospitalHealingData>>.Enumerator enumerator = this.m_Parameter.hospitalData.InjuredTroops.GetEnumerator();
    while (enumerator.MoveNext())
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_PlayerPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.m_Table.transform;
      gameObject.transform.localScale = Vector3.one;
      AllianceHospitalDetailRenderer component = gameObject.GetComponent<AllianceHospitalDetailRenderer>();
      component.SetData(DBManager.inst.DB_User.Get(enumerator.Current.Key), enumerator.Current.Value, ++num, new System.Action(this.OnChanged));
      this.m_ItemList.Add(component);
    }
    this.m_Table.Reposition();
    this.m_ScrollView.ResetPosition();
  }

  private void OnChanged()
  {
    this.m_Table.Reposition();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public TileData tileData;
    public AllianceHospitalData hospitalData;
  }
}
