﻿// Decompiled with JetBrains decompiler
// Type: ConfigExchangeHallSort3
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigExchangeHallSort3
{
  private List<ExchangeHallSort3Info> _exchangeHallSort3InfoList = new List<ExchangeHallSort3Info>();
  private Dictionary<string, ExchangeHallSort3Info> _datas;
  private Dictionary<int, ExchangeHallSort3Info> _dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<ExchangeHallSort3Info, string>(res as Hashtable, "id", out this._datas, out this._dicByUniqueId);
    Dictionary<string, ExchangeHallSort3Info>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.id))
        this._exchangeHallSort3InfoList.Add(enumerator.Current);
    }
    this._exchangeHallSort3InfoList.Sort((Comparison<ExchangeHallSort3Info>) ((a, b) => a.priority.CompareTo(b.priority)));
  }

  public ExchangeHallSort3Info Get(int interalId)
  {
    if (this._dicByUniqueId.ContainsKey(interalId))
      return this._dicByUniqueId[interalId];
    return (ExchangeHallSort3Info) null;
  }

  public ExchangeHallSort3Info Get(string id)
  {
    if (this._datas.ContainsKey(id))
      return this._datas[id];
    return (ExchangeHallSort3Info) null;
  }

  public List<ExchangeHallSort3Info> GetExchangeHallSort3InfoList()
  {
    return this._exchangeHallSort3InfoList;
  }
}
