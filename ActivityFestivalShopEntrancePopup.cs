﻿// Decompiled with JetBrains decompiler
// Type: ActivityFestivalShopEntrancePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UI;
using UnityEngine;

public class ActivityFestivalShopEntrancePopup : MonoBehaviour
{
  public int activityId;
  [SerializeField]
  private ActivityFestivalShopEntrancePopup.Panel panel;

  public void Show(int inActivityId)
  {
    this.gameObject.SetActive(true);
    this.activityId = inActivityId;
    FestivalInfo data = ConfigManager.inst.DB_FestivalActives.GetData(this.activityId);
    if (data == null)
      return;
    this.panel.title.text = data.Loc_outTitle;
    this.panel.description.text = Utils.XLAT(data.innerDescriptionId);
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  public void OpenShop()
  {
    ActivityMainDlg dlg = UIManager.inst.GetDlg<ActivityMainDlg>("Activity/ActivityMainDlg");
    if (!((UnityEngine.Object) dlg != (UnityEngine.Object) null))
      return;
    dlg.ShowFestvialShop(this.activityId);
    this.Hide();
  }

  public void OnBackClick()
  {
    this.Hide();
    ActivityMainDlg dlg = UIManager.inst.GetDlg<ActivityMainDlg>("Activity/ActivityMainDlg");
    if (!((UnityEngine.Object) dlg != (UnityEngine.Object) null))
      return;
    dlg.group.gameObject.SetActive(true);
    dlg.loop.Resume();
    this.Hide();
  }

  public void Reset()
  {
    this.panel.scrollView = this.transform.Find("Panel").gameObject.GetComponent<UIScrollView>();
    this.panel.title = this.transform.Find("Panel/title_bg/title").gameObject.GetComponent<UILabel>();
    this.panel.description = this.transform.Find("Panel/description").gameObject.GetComponent<UILabel>();
    this.panel.BT_Exchange = this.transform.Find("bottom/btn_exchange").gameObject.GetComponent<UIButton>();
    this.panel.BT_Back = this.transform.Find("Btn_Full_Screen_Back").gameObject.GetComponent<UIButton>();
  }

  [Serializable]
  protected class Panel
  {
    public UIScrollView scrollView;
    public UILabel title;
    public UILabel description;
    public UIButton BT_Exchange;
    public UIButton BT_Back;
  }
}
