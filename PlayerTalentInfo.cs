﻿// Decompiled with JetBrains decompiler
// Type: PlayerTalentInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class PlayerTalentInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "tree_id")]
  public int talentTreeInternalId;
  [Config(Name = "level")]
  public int level;
  public int maxLevel;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "tier")]
  public int tier;
  [Config(CustomParse = true, CustomType = typeof (Requirements), Name = "Requirements")]
  public Requirements require;
  [Config(CustomParse = true, CustomType = typeof (Benefits), Name = "Benefits")]
  public Benefits benefit;
  [Config(Name = "active_time")]
  public int activeTime;
  [Config(Name = "active_value")]
  public string activeValue;
  [Config(Name = "cd_time")]
  public int cdTime;
  [Config(Name = "type")]
  public int type;

  public string imagePath
  {
    get
    {
      return string.Format("Texture/Talent/Icon/{0}", (object) this.image);
    }
  }

  public int ActiveTimeInMinutes
  {
    get
    {
      return (int) ((double) this.activeTime / 60.0);
    }
  }

  public int CdTimeInHour
  {
    get
    {
      return (int) ((double) this.cdTime / 3600.0);
    }
  }

  public string Loc_name
  {
    get
    {
      return ScriptLocalization.Get(string.Format("{0}_name", (object) ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(this.talentTreeInternalId).ID), true);
    }
  }

  public string description
  {
    get
    {
      return string.Format("{0}_description", (object) ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(this.talentTreeInternalId).ID);
    }
  }

  public string Loc_description
  {
    get
    {
      return ScriptLocalization.Get(string.Format("{0}_description", (object) ConfigManager.inst.DB_TalentTree.GetTalentTreeInfo(this.talentTreeInternalId).ID), true);
    }
  }

  public string Title_name
  {
    get
    {
      return ScriptLocalization.Get(string.Format("{0}_title", (object) this.ID), true);
    }
  }
}
