﻿// Decompiled with JetBrains decompiler
// Type: RequirementAndRewardTab
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class RequirementAndRewardTab : MonoBehaviour
{
  private PageTabsMonitor tab;
  private GameObject requirementContent;
  private GameObject rewardContent;
  private TechLevel techLevel;
  private bool requireInited;
  private bool rewardInited;

  public void Init(TechLevel techLevel)
  {
    this.techLevel = techLevel;
    this.requirementContent = GameObject.Find("TabContent1/Table");
    this.rewardContent = GameObject.Find("TabContent2/Table");
    if (!((UnityEngine.Object) this.tab == (UnityEngine.Object) null))
      return;
    this.tab = this.GetComponentInChildren<PageTabsMonitor>();
    this.tab.onTabSelected = new System.Action<int>(this.OnTabSelected);
    this.tab.SetCurrentTab(0, true);
  }

  private void UpdateRequirementUI()
  {
    if (this.requireInited)
      return;
    DWRequirement dwRequirement = new DWRequirement((Dictionary<DWRequirement.RequirementType, int>) null);
    using (List<KeyValuePair<CityResourceInfo.ResourceType, int>>.Enumerator enumerator = this.techLevel.Costs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<CityResourceInfo.ResourceType, int> current = enumerator.Current;
        dwRequirement.AddRequire(DWRequirementUtil.ConvertCityDataResourceType(current.Key), current.Value);
      }
    }
    this.requirementContent.GetComponent<UITable>().repositionNow = true;
    this.requireInited = true;
  }

  private void UpdateRewardUI()
  {
  }

  private void OnTabSelected(int tabIndex)
  {
    if (tabIndex == 0)
    {
      this.requirementContent.SetActive(true);
      this.rewardContent.SetActive(false);
      this.UpdateRequirementUI();
    }
    else
    {
      this.requirementContent.SetActive(false);
      this.rewardContent.SetActive(true);
      this.UpdateRewardUI();
    }
  }
}
