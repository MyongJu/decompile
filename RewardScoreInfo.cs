﻿// Decompiled with JetBrains decompiler
// Type: RewardScoreInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RewardScoreInfo : MonoBehaviour
{
  public UITexture scoreFrameTexture;
  public UILabel scoreLabel;

  public void SeedData(RewardScoreInfo.Data d)
  {
    if ((UnityEngine.Object) this.scoreLabel != (UnityEngine.Object) null)
    {
      this.scoreLabel.text = d.score.ToString();
      this.scoreLabel.gameObject.SetActive(d.score != 0);
    }
    if (!((UnityEngine.Object) this.scoreFrameTexture != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.scoreFrameTexture, d.framePath, (System.Action<bool>) null, false, true, string.Empty);
  }

  public class Data
  {
    public string framePath;
    public int score;
  }
}
