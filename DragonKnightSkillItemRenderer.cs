﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightSkillItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class DragonKnightSkillItemRenderer : MonoBehaviour
{
  public GameObject skillItemNode;
  public UITexture skillTexture;
  public UILabel skillStateText;
  private DragonKnightSkillInfo _skillInfo;

  public void SetData(DragonKnightSkillInfo skillInfo)
  {
    this._skillInfo = skillInfo;
    this.UpdateUI();
  }

  public void OnSkillItemClicked()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightSkillInfoPopup", (Popup.PopupParameter) new DragonKnightSkillInfoPopup.Parameter()
    {
      skillInfo = this._skillInfo
    });
  }

  private void UpdateUI()
  {
    if (this._skillInfo == null)
      return;
    this.skillStateText.text = this._skillInfo.CurrentStateDesc;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.skillTexture, this._skillInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    if (this._skillInfo.CurrentState == DragonKnightSkillInfo.SkillState.LOCKED)
      GreyUtility.Grey(this.skillItemNode);
    else
      GreyUtility.Normal(this.skillItemNode);
  }
}
