﻿// Decompiled with JetBrains decompiler
// Type: DiceGroupInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DiceGroupInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "spend_item")]
  public int propsId;
  [Config(Name = "spend_number")]
  public int playOneCost;
  [Config(Name = "title")]
  public string title;
  [Config(Name = "get_item_gold")]
  public int getPropsNeedGold;
  [Config(Name = "get_item_number")]
  public int getPropsCount;
  [Config(Name = "theme")]
  public int themeId;

  public int PlayOneCost
  {
    get
    {
      return this.playOneCost;
    }
  }

  public int PlayTenCost
  {
    get
    {
      return this.playOneCost * 10;
    }
  }

  public string Title
  {
    get
    {
      return Utils.XLAT(this.title);
    }
  }
}
