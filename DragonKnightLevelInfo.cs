﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightLevelInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonKnightLevelInfo
{
  public int internalId;
  [Config(Name = "dragon_knight_level")]
  public int dragonKnightLevel;
  [Config(Name = "skill_points")]
  public int skillPoints;
  [Config(Name = "total_experience")]
  public long totalExperience;
  [Config(Name = "power")]
  public long power;
  [Config(Name = "strength")]
  public int strength;
  [Config(Name = "intelligent")]
  public int intelligent;
  [Config(Name = "armor")]
  public int armor;
  [Config(Name = "constitution")]
  public int constitution;
  [Config(Name = "attack")]
  public int attack;
  [Config(Name = "defence")]
  public int defence;
  [Config(Name = "health")]
  public int health;
  [Config(Name = "mana")]
  public int mana;
  [Config(Name = "magic")]
  public int magic;
  [Config(Name = "health_recovery")]
  public int healthRecovery;
  [Config(Name = "mana_recovery")]
  public int manaRecovery;
  [Config(Name = "dungeon_stamina")]
  public int dungeonStamina;
}
