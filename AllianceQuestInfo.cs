﻿// Decompiled with JetBrains decompiler
// Type: AllianceQuestInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;

public class AllianceQuestInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "stronghold_level")]
  public int Stronghold_Level;
  [Config(Name = "chance")]
  public float Chance;
  [Config(Name = "description")]
  public string Description;
  [Config(Name = "image")]
  public string Image;
  [Config(Name = "food_payout")]
  public int Food_Payout;
  [Config(Name = "wood_payout")]
  public int Wood_Payout;
  [Config(Name = "ore_payout")]
  public int Ore_Payout;
  [Config(Name = "silver_payout")]
  public int Silver_Payout;
  [Config(Name = "reward_id_1")]
  public int Reward_ID_1;
  [Config(Name = "reward_value_1")]
  public int Reward_Value_1;
  [Config(Name = "alliance_xp")]
  public int Alliance_XP;
  [Config(Name = "player_xp")]
  public int Hero_XP;
  [Config(Name = "honor")]
  public int Alliance_Honor;
  [Config(Name = "fund")]
  public int Alliance_Fund;
  [Config(Name = "duration_time")]
  public int Duration_Time;

  public string Name
  {
    get
    {
      return ScriptLocalization.Get(this.ID + "_name", true);
    }
  }

  public string Desc
  {
    get
    {
      return ScriptLocalization.Get(this.Description, true);
    }
  }
}
