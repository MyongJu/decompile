﻿// Decompiled with JetBrains decompiler
// Type: LoadingScreenDivide
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class LoadingScreenDivide
{
  private float _duration = 2f;
  public const string ASSET_PATH = "Prefab/UI/DragonKnight/LoadingScreen";
  private LoadingController _controller;
  private int _divisor;
  private int _dividend;
  private float _start;
  private float _source;
  private float _target;
  private bool _tween;
  private static LoadingScreenDivide _instance;

  public static LoadingScreenDivide Instance
  {
    get
    {
      if (LoadingScreenDivide._instance == null)
        LoadingScreenDivide._instance = new LoadingScreenDivide();
      return LoadingScreenDivide._instance;
    }
  }

  public float Duration
  {
    get
    {
      return this._duration;
    }
    set
    {
      this._duration = value;
    }
  }

  public void Show(int divisor)
  {
    this.Hide();
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AssetManager.Instance.Load("Prefab/UI/DragonKnight/LoadingScreen", (System.Type) null) as GameObject);
    AssetManager.Instance.UnLoadAsset("Prefab/UI/DragonKnight/LoadingScreen", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.parent = UIManager.inst.ui2DCamera.transform;
    gameObject.transform.localScale = Vector3.one;
    this._controller = gameObject.GetComponent<LoadingController>();
    this._divisor = divisor;
    this._dividend = 0;
    this._controller.SetProgress(0.0f);
    this._start = 0.0f;
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdateEvent);
  }

  private void Hide()
  {
    if ((bool) ((UnityEngine.Object) this._controller))
    {
      this._controller.gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this._controller.gameObject);
      this._controller = (LoadingController) null;
    }
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdateEvent);
  }

  public void Add()
  {
    ++this._dividend;
    this._tween = true;
    this._start = Time.realtimeSinceStartup;
    this._source = this._controller.GetProgress();
    this._target = (float) this._dividend / (float) this._divisor;
  }

  public void ShowTip(string tip)
  {
    if (!(bool) ((UnityEngine.Object) this._controller))
      return;
    this._controller.SetTip(tip);
  }

  private void OnUpdateEvent(double t)
  {
    if (!(bool) ((UnityEngine.Object) this._controller))
      return;
    if ((double) this._controller.GetProgress() >= 1.0)
    {
      this.Hide();
    }
    else
    {
      float realtimeSinceStartup = Time.realtimeSinceStartup;
      if (!this._tween)
        return;
      float num = (realtimeSinceStartup - this._start) / this._duration;
      if ((double) num >= 1.0)
      {
        this._tween = false;
        this._controller.SetProgress(this._target);
      }
      else
        this._controller.SetProgress(this._source + num * (this._target - this._source));
    }
  }
}
