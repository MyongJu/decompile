﻿// Decompiled with JetBrains decompiler
// Type: FurnaceItemsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class FurnaceItemsDlg : UI.Dialog
{
  private Dictionary<int, FurnaceItem> totals = new Dictionary<int, FurnaceItem>();
  public UILabel title;
  public UIGrid[] contents;
  public UIScrollView[] scrolls;
  public UIButtonBar bar;
  public FurnaceItem itemPrefab;
  public FurnaceRewards rewards;
  public GameObject empty;

  private UIGrid Content
  {
    get
    {
      return this.contents[0];
    }
  }

  private UIScrollView Scroll
  {
    get
    {
      return this.scrolls[0];
    }
  }

  private void OnViewChange(int index)
  {
    NGUITools.SetActive(this.Scroll.gameObject, true);
    NGUITools.SetActive(this.empty, this.GetData().Count == 0);
    this.UpdateUI();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.AddEventHandler();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.bar.SelectedIndex = 0;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.RemoveEventHandler();
    Dictionary<int, FurnaceItem>.ValueCollection.Enumerator enumerator = this.totals.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((UnityEngine.Object) enumerator.Current != (UnityEngine.Object) null)
        enumerator.Current.Dispose();
    }
    this.rewards.Dispose();
    this.totals.Clear();
  }

  private void RefreshLeftArea()
  {
  }

  private List<ItemStaticInfo> FilterData(List<ItemStaticInfo> sources, ItemBag.ItemType type)
  {
    List<ItemStaticInfo> itemStaticInfoList = new List<ItemStaticInfo>();
    for (int index = 0; index < sources.Count; ++index)
    {
      if (sources[index].Type == type && ItemBag.Instance.GetItemCount(sources[index].internalId) > 0)
        itemStaticInfoList.Add(sources[index]);
    }
    return itemStaticInfoList;
  }

  private List<ItemStaticInfo> GetData()
  {
    return ConfigManager.inst.DB_MagicSelter.GetAllDisbandItems(this.bar.SelectedIndex);
  }

  private void UpdateUI()
  {
    List<ItemStaticInfo> data = this.GetData();
    NGUITools.SetActive(this.empty, data.Count == 0);
    Dictionary<int, bool> dictionary = new Dictionary<int, bool>();
    for (int index = 0; index < data.Count; ++index)
    {
      FurnaceItem furnaceItem = this.GetItem(data[index].internalId);
      furnaceItem.OnItemSelected = new System.Action<int, int>(this.OnItemSelectedHandler);
      dictionary.Add(data[index].internalId, true);
      furnaceItem.SetData(data[index].internalId);
    }
    Dictionary<int, FurnaceItem>.KeyCollection.Enumerator enumerator = this.totals.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int current = enumerator.Current;
      if (!dictionary.ContainsKey(current))
        NGUITools.SetActive(this.totals[current].gameObject, false);
    }
    this.Content.Reposition();
    this.Scroll.ResetPosition();
  }

  private void OnItemSelectedHandler(int itemId, int count)
  {
    ItemStaticInfo rewardItem = ConfigManager.inst.DB_MagicSelter.GetRewardItem(itemId);
    if (rewardItem == null)
      return;
    this.rewards.Add(rewardItem.internalId, itemId, count);
  }

  private FurnaceItem GetItem(int itemId)
  {
    if (this.totals.ContainsKey(itemId))
    {
      NGUITools.SetActive(this.totals[itemId].gameObject, true);
      return this.totals[itemId];
    }
    GameObject go = NGUITools.AddChild(this.Content.gameObject, this.itemPrefab.gameObject);
    FurnaceItem component = go.GetComponent<FurnaceItem>();
    component.containIndex = this.bar.SelectedIndex;
    NGUITools.SetActive(go, true);
    this.totals.Add(itemId, component);
    return component;
  }

  private void AddEventHandler()
  {
    this.bar.OnSelectedHandler += new System.Action<int>(this.OnViewChange);
    this.rewards.OnSelterDelegate += new System.Action(this.OnSelterHandler);
    this.rewards.OnItemUnSelected += new System.Action<int>(this.OnUnSelectedItemFromReward);
    DBManager.inst.DB_Item.onDataRemove += new System.Action<int>(this.OnItemDataRemove);
    DBManager.inst.DB_Item.onDataUpdated += new System.Action<int>(this.OnItemDataUpdate);
  }

  private void OnUnSelectedItemFromReward(int rewardId)
  {
    List<int> itemFromRewardItem = ConfigManager.inst.DB_MagicSelter.GetDisbandItemFromRewardItem(rewardId);
    for (int index = 0; index < itemFromRewardItem.Count; ++index)
    {
      if (this.totals.ContainsKey(itemFromRewardItem[index]))
      {
        this.totals[itemFromRewardItem[index]].Clear();
        this.rewards.Add(rewardId, itemFromRewardItem[index], 0);
      }
    }
  }

  private void OnSelterHandler()
  {
    bool flag = false;
    Dictionary<int, FurnaceItem>.ValueCollection.Enumerator enumerator = this.totals.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if ((UnityEngine.Object) enumerator.Current != (UnityEngine.Object) null && !enumerator.Current.Clear())
        flag = true;
    }
    if (!flag)
      return;
    this.Content.Reposition();
  }

  private void RemoveEventHandler()
  {
    this.rewards.OnSelterDelegate -= new System.Action(this.OnSelterHandler);
    this.rewards.OnItemUnSelected -= new System.Action<int>(this.OnUnSelectedItemFromReward);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.OnViewChange);
    this.rewards.OnItemUnSelected = (System.Action<int>) null;
    DBManager.inst.DB_Item.onDataRemove -= new System.Action<int>(this.OnItemDataRemove);
    DBManager.inst.DB_Item.onDataUpdated -= new System.Action<int>(this.OnItemDataUpdate);
  }

  private void OnInventoryDataRemove(long uid, long itemId)
  {
    if (uid == PlayerData.inst.uid)
      ;
  }

  private void OnInventoryDataUpdate(long uid, long itemId)
  {
    if (uid == PlayerData.inst.uid)
      ;
  }

  private void OnItemDataRemove(int itemId)
  {
    this.RemoveItem(itemId);
  }

  private void OnItemDataUpdate(int itemId)
  {
    this.RefreshItem(itemId);
  }

  private void RefreshItem(int itemId)
  {
    if (!this.totals.ContainsKey(itemId))
      return;
    this.totals[itemId].Refresh();
  }

  private void RemoveItem(int itemId)
  {
    if (!this.totals.ContainsKey(itemId))
      return;
    NGUITools.SetActive(this.totals[itemId].gameObject, false);
    NGUITools.SetActive(this.empty, this.GetData().Count == 0);
    this.Content.Reposition();
  }
}
