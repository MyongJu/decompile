﻿// Decompiled with JetBrains decompiler
// Type: KingdomWarMarchPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomWarMarchPopup : Popup
{
  private Dictionary<long, MarchTroopContainer> itemDict = new Dictionary<long, MarchTroopContainer>();
  private GameObjectPool itemPool = new GameObjectPool();
  public UILabel panelTitle;
  public UILabel totalTroopsCount;
  public MarchTroopContainer troopPrefab;
  public UIScrollView scrollView;
  public UITable table;
  private string titleKey;
  private TileData tileData;
  private long troopCapacity;
  private long troopCount;
  private Dictionary<long, long> _troops;
  private long _marchCaptainId;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomWarMarchPopup.Parameter parameter = orgParam as KingdomWarMarchPopup.Parameter;
    if (parameter != null)
    {
      this.titleKey = parameter.titleKey;
      this.tileData = parameter.tileData;
      this.troopCapacity = parameter.troopCapacity;
      this.troopCount = parameter.troopCount;
      this._troops = parameter.troops;
      this._marchCaptainId = parameter.marchCaptainId;
    }
    this.itemPool.Initialize(this.troopPrefab.gameObject, this.table.gameObject);
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.ShowTitleInfo();
    this.ShowTroopsDetail();
  }

  private void ShowTitleInfo()
  {
    this.panelTitle.text = Utils.XLAT(string.IsNullOrEmpty(this.titleKey) ? "id_uppercase_troop_details" : this.titleKey);
    long num = 0;
    if (this.tileData != null)
    {
      switch (this.tileData.TileType)
      {
        case TileType.City:
          num = this.troopCount;
          break;
        case TileType.Wonder:
          DB.WonderData wonderData = this.tileData.WonderData;
          num = wonderData == null ? 0L : wonderData.CurrentTroopsCount;
          break;
        case TileType.WonderTower:
          WonderTowerData wonderTowerData = this.tileData.WonderTowerData;
          num = wonderTowerData == null ? 0L : wonderTowerData.CurrentTroopsCount;
          break;
      }
    }
    else if (this._troops != null && this._troops.Count > 0)
    {
      Dictionary<long, long>.ValueCollection.Enumerator enumerator = this._troops.Values.GetEnumerator();
      while (enumerator.MoveNext())
        num += enumerator.Current;
    }
    this.totalTroopsCount.text = Utils.FormatThousands(((long) Mathf.Min((float) num, (float) this.troopCapacity)).ToString()) + "/" + Utils.FormatThousands(this.troopCapacity.ToString());
  }

  private void ShowTroopsDetail()
  {
    this.ClearData();
    Dictionary<long, long> dictionary = new Dictionary<long, long>();
    long key = 0;
    if (this.tileData != null)
    {
      switch (this.tileData.TileType)
      {
        case TileType.City:
          List<MarchData> reinforceByUid = DBManager.inst.DB_March.GetReinforceByUid(this.tileData.OwnerID);
          if (reinforceByUid != null)
          {
            for (int index = 0; index < reinforceByUid.Count; ++index)
            {
              if (!dictionary.ContainsKey(reinforceByUid[index].ownerUid))
                dictionary.Add(reinforceByUid[index].ownerUid, reinforceByUid[index].marchId);
            }
            break;
          }
          break;
        case TileType.Wonder:
          DB.WonderData wonderData = this.tileData.WonderData;
          dictionary = wonderData == null ? (Dictionary<long, long>) null : wonderData.Troops;
          key = wonderData == null ? 0L : wonderData.OWNER_UID;
          break;
        case TileType.WonderTower:
          WonderTowerData wonderTowerData = this.tileData.WonderTowerData;
          dictionary = wonderTowerData == null ? (Dictionary<long, long>) null : wonderTowerData.Troops;
          key = wonderTowerData == null ? 0L : wonderTowerData.OWNER_UID;
          break;
      }
    }
    else
    {
      dictionary = this._troops;
      key = this._marchCaptainId;
    }
    if (dictionary != null)
    {
      if (dictionary.ContainsKey(key))
      {
        MarchData marchData = DBManager.inst.DB_March.Get(dictionary[key]);
        if (marchData != null)
        {
          MarchTroopContainer itemRenderer = this.CreateItemRenderer(marchData);
          this.itemDict.Add(key, itemRenderer);
        }
      }
      Dictionary<long, long>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (key != enumerator.Current)
        {
          MarchData marchData = DBManager.inst.DB_March.Get(dictionary[enumerator.Current]);
          if (marchData != null)
          {
            MarchTroopContainer itemRenderer = this.CreateItemRenderer(marchData);
            this.itemDict.Add(enumerator.Current, itemRenderer);
          }
        }
      }
    }
    Utils.ExecuteInSecs(0.05f, (System.Action) (() => this.Reposition()));
  }

  private MarchTroopContainer CreateItemRenderer(MarchData marchData)
  {
    GameObject gameObject = this.itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    MarchTroopContainer component = gameObject.GetComponent<MarchTroopContainer>();
    DragonUIInfo componentInChildren = gameObject.GetComponentInChildren<DragonUIInfo>();
    Transform transform = gameObject.transform.Find("Table/NoDragon");
    if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
      NGUITools.SetActive(transform.gameObject, !marchData.withDragon);
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      NGUITools.SetActive(componentInChildren.gameObject, marchData.withDragon);
    if (marchData.withDragon)
    {
      string empty = string.Empty;
      MarchData.MarchType type = marchData.type;
      string skillType;
      switch (type)
      {
        case MarchData.MarchType.encamp:
        case MarchData.MarchType.city_attack:
        case MarchData.MarchType.encamp_attack:
        case MarchData.MarchType.gather_attack:
        case MarchData.MarchType.wonder_attack:
        case MarchData.MarchType.rally_attack:
          skillType = "attack";
          break;
        case MarchData.MarchType.gather:
          skillType = "gather";
          break;
        case MarchData.MarchType.reinforce:
          skillType = "defend";
          break;
        case MarchData.MarchType.monster_attack:
          skillType = "attack_monster";
          break;
        default:
          if (type != MarchData.MarchType.rally)
          {
            if (type != MarchData.MarchType.world_boss_attack)
            {
              skillType = "attack";
              break;
            }
            goto case MarchData.MarchType.monster_attack;
          }
          else
            goto case MarchData.MarchType.encamp;
      }
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      {
        componentInChildren.SetDragonOwnerData(marchData.marchId);
        componentInChildren.SetDragon(marchData.dragonId, skillType, (int) marchData.dragon.tendency, marchData.dragon.level);
      }
    }
    component.useTileData = this.tileData != null;
    component.SetData(TroopType.invalid, marchData.marchId, !marchData.withDragon, true);
    component.onMarchRecalled += new System.Action<MarchData.MarchType>(this.OnMarchRecalled);
    return component;
  }

  private void ClearData()
  {
    using (Dictionary<long, MarchTroopContainer>.Enumerator enumerator = this.itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, MarchTroopContainer> current = enumerator.Current;
        current.Value.Clear();
        current.Value.onMarchRecalled -= new System.Action<MarchData.MarchType>(this.OnMarchRecalled);
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this.itemPool.Release(gameObject);
      }
    }
    this.itemDict.Clear();
    this.itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private void OnMarchRecalled(MarchData.MarchType marchType)
  {
    if (marchType == MarchData.MarchType.wonder_reinforce)
      return;
    this.OnCloseBtnPressed();
  }

  private void OnMarchDataRemoved()
  {
    this.UpdateUI();
  }

  private void OnWonderDataUpdated(DB.WonderData wonderData)
  {
    if (this.tileData == null || wonderData == null || this.tileData.TileType != TileType.Wonder)
      return;
    if (wonderData.CurrentTroopsCount <= 0L)
      this.OnCloseBtnPressed();
    else
      this.UpdateUI();
  }

  private void OnWonderTowerDataUpdated(WonderTowerData wonderTowerData)
  {
    if (this.tileData == null || wonderTowerData == null || this.tileData.TileType != TileType.WonderTower)
      return;
    if (wonderTowerData.CurrentTroopsCount <= 0L)
      this.OnCloseBtnPressed();
    else
      this.UpdateUI();
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_March.onDataRemoved += new System.Action(this.OnMarchDataRemoved);
    DBManager.inst.DB_Wonder.onDataUpdate += new System.Action<DB.WonderData>(this.OnWonderDataUpdated);
    DBManager.inst.DB_WonderTower.onDataUpdate += new System.Action<WonderTowerData>(this.OnWonderTowerDataUpdated);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_March.onDataRemoved -= new System.Action(this.OnMarchDataRemoved);
    DBManager.inst.DB_Wonder.onDataUpdate -= new System.Action<DB.WonderData>(this.OnWonderDataUpdated);
    DBManager.inst.DB_WonderTower.onDataUpdate -= new System.Action<WonderTowerData>(this.OnWonderTowerDataUpdated);
  }

  public class Parameter : Popup.PopupParameter
  {
    public string titleKey = string.Empty;
    public TileData tileData;
    public long troopCapacity;
    public long troopCount;
    public long marchCaptainId;
    public Dictionary<long, long> troops;
  }
}
