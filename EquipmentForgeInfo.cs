﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentForgeInfo : MonoBehaviour
{
  public bool showOwn = true;
  private List<EquipmentSuitItem> allEquipmentSuitItem = new List<EquipmentSuitItem>();
  private List<EquipmentGemItem> _allEquipmentGemItem = new List<EquipmentGemItem>();
  public const string SUIT_ITEM_PATH = "Prefab/UI/Common/EquipmentSuitItem";
  public const string SUIT_GEM_PATH = "Prefab/UI/Common/EquipmentGemItem";
  public EquipmentComponent equipComponent;
  public EquipmentScrollComponent scrollEquipment;
  public EquipmentBenefits benefitContent;
  public UICompnent.RequireComponent require;
  public EquipmentForgeBottom bottom;
  public UITable table;
  public UIScrollView scrollView;
  public Transform equipmentSuitItemContainer;
  public Transform equipmentGemItemContainer;
  private ConfigEquipmentScrollInfo scrollInfo;
  private bool showOwnMessage;
  private BagType bagType;

  public void Init(BagType bagType, int scrollID = 0)
  {
    this.bagType = bagType;
    if (scrollID != 0)
    {
      this.gameObject.SetActive(true);
      this.scrollInfo = ConfigManager.inst.GetEquipmentScroll(bagType).GetData(scrollID);
      this.showOwnMessage = ItemBag.Instance.GetItemCount(this.scrollInfo.itemID) > 0 && this.showOwn;
      this.UpdateEquipmentGem(bagType, this.scrollInfo);
      this.UpdateEquipmentSuitBenefit(bagType, this.scrollInfo.equipmenID);
      this.DrawEquipment();
      this.DrawScroll();
      this.DrawBenefit();
      this.DrawRequire(this.showOwnMessage);
      Utils.ExecuteInSecs(0.01f, (System.Action) (() =>
      {
        if (!((UnityEngine.Object) this.table != (UnityEngine.Object) null))
          return;
        this.table.Reposition();
      }));
      this.scrollView = this.gameObject.GetComponentInParent<UIScrollView>();
      if ((UnityEngine.Object) this.scrollView != (UnityEngine.Object) null)
        this.scrollView.ResetPosition();
    }
    else
      this.gameObject.SetActive(false);
    if (!((UnityEngine.Object) this.bottom != (UnityEngine.Object) null))
      return;
    this.bottom.Init(this.bagType, scrollID);
  }

  public void Dispose()
  {
    this.equipComponent.Dispose();
    this.scrollEquipment.Dispose();
    this.benefitContent.Dispose();
    if (!((UnityEngine.Object) this.bottom != (UnityEngine.Object) null))
      return;
    this.bottom.Dispose();
  }

  private void DrawEquipment()
  {
    InventoryItemData itemByInternalId = DBManager.inst.GetInventory(this.bagType).GetMyItemByInternalID((long) this.scrollInfo.equipmenID);
    this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(this.scrollInfo.equipmenID, itemByInternalId != null ? itemByInternalId.enhanced : 0, this.bagType, itemByInternalId != null ? itemByInternalId.itemId : 0L));
  }

  private void DrawScroll()
  {
    this.scrollEquipment.FeedData((IComponentData) new EquipmentScrollComponetData(this.scrollInfo, this.bagType));
  }

  private void DrawBenefit()
  {
    this.benefitContent.Init(ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.scrollInfo.equipmenID, 0).benefits);
  }

  private void DrawRequire(bool showOwnMessage)
  {
    List<RequireComponentData.RequireElement> requires = new List<RequireComponentData.RequireElement>();
    if (EquipmentManager.Instance.GetOwnForgeQueue(this.bagType) == 0)
      requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.QUEUE, 1L, 0, showOwnMessage));
    ConfigEquipmentPropertyInfo idAndEnhanceLevel = ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.scrollInfo.equipmenID, 0);
    if (idAndEnhanceLevel.steelValue > 0L)
    {
      long finalData = (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) idAndEnhanceLevel.steelValue, "calc_crafting_steel_cost");
      requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.STEEL, finalData, 0, showOwnMessage));
    }
    List<Materials.MaterialElement> materials = this.scrollInfo.Materials.GetMaterials();
    for (int index = 0; index < materials.Count; ++index)
      requires.Add(new RequireComponentData.RequireElement(RequireComponentData.Requirement.ITEM, materials[index].amount, materials[index].internalID, showOwnMessage));
    this.require.FeedData((IComponentData) new RequireComponentData(this.bagType, requires));
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.allEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.allEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(BagType bagType, int equipmentId)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(equipmentId);
    if (data == null || data.suitGroup == 0)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentSuitItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
    component.SetData(PlayerData.inst.uid, bagType, data.suitGroup, (InventoryItemData) null);
    this.allEquipmentSuitItem.Add(component);
  }

  private void ClearEquipmentGemItem()
  {
    using (List<EquipmentGemItem>.Enumerator enumerator = this._allEquipmentGemItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemItem.Clear();
  }

  private void UpdateEquipmentGem(BagType bagType, ConfigEquipmentScrollInfo scrollInfo)
  {
    this.ClearEquipmentGemItem();
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentGemItem", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentGemItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentGemItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentGemItem component = gameObject.GetComponent<EquipmentGemItem>();
    component.SetData(bagType, scrollInfo, this.scrollView);
    this._allEquipmentGemItem.Add(component);
  }
}
