﻿// Decompiled with JetBrains decompiler
// Type: RequestAssistant
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class RequestAssistant
{
  private int _count;

  protected int BlockerNum
  {
    get
    {
      return this._count;
    }
    set
    {
      if (this._count != value)
        this._count = value;
      if (this._count >= 0)
        return;
      this._count = 0;
    }
  }

  public void Block()
  {
    ++this._count;
    this.Check();
  }

  private void Check()
  {
    if (this._count > 0)
    {
      if (!GameEngine.IsReady())
        return;
      UIManager.inst.ShowSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    }
    else
    {
      if (!GameEngine.IsReady())
        return;
      UIManager.inst.HideSystemBlocker("FullScreenWait", (SystemBlocker.SystemBlockerParameter) null);
    }
  }

  public void ShowRetry()
  {
    GameObject blocker = UIManager.inst.GetBlocker("FullScreenWait");
    if (!((Object) blocker != (Object) null))
      return;
    blocker.GetComponent<FullScreenWait>().ShowRetry();
  }

  public void CancelBlock()
  {
    --this._count;
    this.Check();
  }

  public void ShowSystemBlocker(LoaderBatch info)
  {
    if (!info.BlockScreen)
      return;
    this.Block();
  }

  public void HideSystemBlocker(LoaderBatch info)
  {
    if (!info.BlockScreen)
      return;
    this.CancelBlock();
  }
}
