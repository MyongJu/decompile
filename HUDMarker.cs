﻿// Decompiled with JetBrains decompiler
// Type: HUDMarker
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HUDMarker : MonoBehaviour
{
  public UISprite arrow;
  public System.Action onHUDMarkerPressed;
  private Vector3 _position;
  private Coordinate _targetLocation;
  private long _targetID;

  public Coordinate TargetLocation
  {
    get
    {
      return this._targetLocation;
    }
    set
    {
      this._targetLocation = value;
    }
  }

  public long TargetID
  {
    get
    {
      return this._targetID;
    }
    set
    {
      this._targetID = value;
    }
  }

  public void UpdateArrow()
  {
    this.UpdatePosition();
    this.UpdateDirection();
  }

  private void UpdatePosition()
  {
  }

  private void UpdateDirection()
  {
  }

  public void OnButtonPressed()
  {
    if (this.onHUDMarkerPressed == null)
      return;
    this.onHUDMarkerPressed();
  }
}
