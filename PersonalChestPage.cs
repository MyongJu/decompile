﻿// Decompiled with JetBrains decompiler
// Type: PersonalChestPage
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class PersonalChestPage : MonoBehaviour
{
  protected List<PersonalChestItem> _AllPersonalChestItem = new List<PersonalChestItem>();
  private const string KEY_SEND_PERSONAL_CHEST_MODE = "SendPersonalAllianceChestMode";
  [SerializeField]
  private UIToggle _ToggleAnonymous;
  [SerializeField]
  private UIScrollView _ScrollView;
  [SerializeField]
  private UITable _Container;
  [SerializeField]
  private GameObject _RootHaveNoRecord;
  [SerializeField]
  private PersonalChestItem _PersonalChestItemTemplate;
  private bool _needUpdate;
  private List<UserTreasureChestData> _allUserTreasureChestData;

  public void NotifyUpdate()
  {
    this._needUpdate = true;
  }

  protected void Update()
  {
    this.CheckExpiredAndUpdate();
    if (!this._needUpdate)
      return;
    this._needUpdate = false;
    this.UpdateUI();
  }

  protected void UpdateUI()
  {
    this._PersonalChestItemTemplate.gameObject.SetActive(false);
    this._ToggleAnonymous.value = PlayerPrefsEx.GetIntByUid("SendPersonalAllianceChestMode", 1) == 1;
    this.UpdatePersonChestItemList();
    this._ToggleAnonymous.onChange.Clear();
    this._ToggleAnonymous.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.OnToggleAnonymousValueChanged)));
  }

  private void OnToggleAnonymousValueChanged()
  {
    PlayerPrefsEx.SetIntByUid("SendPersonalAllianceChestMode", !this._ToggleAnonymous.value ? 0 : 1);
    PlayerPrefsEx.Save();
  }

  private void CheckExpiredAndUpdate()
  {
    if (this._allUserTreasureChestData == null)
      return;
    using (List<UserTreasureChestData>.Enumerator enumerator = this._allUserTreasureChestData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Expired)
        {
          this._needUpdate = true;
          break;
        }
      }
    }
  }

  private void UpdatePersonChestItemList()
  {
    this.DestroyAllPersonalChestItem();
    this._allUserTreasureChestData = DBManager.inst.DB_UserTreasureChest.GetAllUserChest(PlayerData.inst.uid);
    using (List<UserTreasureChestData>.Enumerator enumerator = this._allUserTreasureChestData.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreatePersonalChestItem().SetData(enumerator.Current, this._ToggleAnonymous);
    }
    this._Container.Reposition();
    this._ScrollView.ResetPosition();
    this._RootHaveNoRecord.SetActive(this._AllPersonalChestItem.Count <= 0);
  }

  private void DestroyAllPersonalChestItem()
  {
    using (List<PersonalChestItem>.Enumerator enumerator = this._AllPersonalChestItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PersonalChestItem current = enumerator.Current;
        current.gameObject.transform.SetParent((Transform) null);
        Object.Destroy((Object) current.gameObject);
      }
    }
    this._AllPersonalChestItem.Clear();
  }

  private PersonalChestItem CreatePersonalChestItem()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this._PersonalChestItemTemplate.gameObject);
    gameObject.transform.SetParent(this._Container.transform);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localScale = Vector3.one;
    gameObject.SetActive(true);
    PersonalChestItem component = gameObject.GetComponent<PersonalChestItem>();
    this._AllPersonalChestItem.Add(component);
    return component;
  }
}
