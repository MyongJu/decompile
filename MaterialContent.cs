﻿// Decompiled with JetBrains decompiler
// Type: MaterialContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MaterialContent : MonoBehaviour
{
  public GameObject empty;
  public UIGrid grid;
  public EquipmentMaterialComponent template;
  public UIScrollView scrollView;
  public Icon material;
  private List<EquipmentMaterialComponent> contentList;
  private List<ItemStaticInfo> currentList;
  private EquipmentMaterialComponent current;
  private int currentSelectInfoID;
  private BagType bagType;

  public void Init(BagType bagType)
  {
    this.bagType = bagType;
    this.ClearGrid();
    List<ConsumableItemData> equipmentMaterialItems = ItemBag.Instance.GetConsumableEquipmentMaterialItems(bagType);
    this.currentList = new List<ItemStaticInfo>();
    for (int index = 0; index < equipmentMaterialItems.Count; ++index)
      this.currentList.Add(ConfigManager.inst.DB_Items.GetItem(equipmentMaterialItems[index].internalId));
    this.currentList.Sort((Comparison<ItemStaticInfo>) ((x, y) => x.Priority.CompareTo(y.Priority)));
    this.empty.SetActive(this.currentList.Count == 0);
    this.contentList = new List<EquipmentMaterialComponent>();
    for (int index = 0; index < this.currentList.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, this.template.gameObject);
      gameObject.SetActive(true);
      EquipmentMaterialComponent component = gameObject.GetComponent<EquipmentMaterialComponent>();
      component.OnSelectedHandler = new System.Action<EquipmentMaterialComponent>(this.OnSelectedHandler);
      this.contentList.Add(component);
      component.FeedData((IComponentData) new EquipmentMaterialComponentData(this.currentList[index]));
      if (component.itemInfo.internalId == this.currentSelectInfoID)
      {
        this.current = component;
        this.current.Select = true;
      }
    }
    if ((UnityEngine.Object) this.current == (UnityEngine.Object) null && this.contentList.Count > 0)
    {
      this.current = this.contentList[0];
      this.current.Select = true;
      this.currentSelectInfoID = this.current.itemInfo.internalId;
    }
    this.material.gameObject.SetActive((UnityEngine.Object) this.current != (UnityEngine.Object) null);
    if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
      this.material.FeedData((IComponentData) new IconData(this.current.itemInfo.ImagePath, new string[2]
      {
        this.current.itemInfo.LocName,
        this.current.itemInfo.LocDescription
      })
      {
        Data = (object) this.current.itemInfo.internalId
      });
    this.scrollView.ResetPosition();
    this.grid.repositionNow = true;
  }

  public void Dispose()
  {
  }

  private void OnSelectedHandler(EquipmentMaterialComponent selected)
  {
    if (!((UnityEngine.Object) this.current != (UnityEngine.Object) null) || this.current.itemInfo.internalId == selected.itemInfo.internalId)
      return;
    this.currentSelectInfoID = selected.itemInfo.internalId;
    this.current.Select = false;
    this.current = selected;
    this.current.Select = true;
    this.material.FeedData((IComponentData) new IconData(this.current.itemInfo.ImagePath, new string[2]
    {
      this.current.itemInfo.LocName,
      this.current.itemInfo.LocDescription
    }));
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }
}
