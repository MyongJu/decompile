﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicTowerPlayerUnit
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigMagicTowerPlayerUnit
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MagicTowerPlayerUnitInfo> datas;
  private Dictionary<int, MagicTowerPlayerUnitInfo> dicByUniqueId;
  private List<int> _costItems;

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicTowerPlayerUnitInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public MagicTowerPlayerUnitInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicTowerPlayerUnitInfo) null;
  }

  public MagicTowerPlayerUnitInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicTowerPlayerUnitInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicTowerPlayerUnitInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicTowerPlayerUnitInfo>) null;
  }
}
