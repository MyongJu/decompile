﻿// Decompiled with JetBrains decompiler
// Type: ResourceComponent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ResourceComponent : ComponentRenderBase
{
  public GameObject food;
  public GameObject wood;
  public GameObject ore;
  public GameObject silver;
  public GameObject dragonFossil;
  public UIGrid grid;

  public override void Init()
  {
    base.Init();
    ResourceComponentData data = this.data as ResourceComponentData;
    if (!string.IsNullOrEmpty(data.food))
    {
      this.food.SetActive(true);
      this.food.GetComponentInChildren<UILabel>().text = Utils.FormatThousands(data.food);
    }
    else
      this.food.SetActive(false);
    if (!string.IsNullOrEmpty(data.wood))
    {
      this.wood.SetActive(true);
      this.wood.GetComponentInChildren<UILabel>().text = Utils.FormatThousands(data.wood);
    }
    else
      this.wood.SetActive(false);
    if (!string.IsNullOrEmpty(data.ore))
    {
      this.ore.SetActive(true);
      this.ore.GetComponentInChildren<UILabel>().text = Utils.FormatThousands(data.ore);
    }
    else
      this.ore.SetActive(false);
    if (!string.IsNullOrEmpty(data.silver))
    {
      this.silver.SetActive(true);
      this.silver.GetComponentInChildren<UILabel>().text = Utils.FormatThousands(data.silver);
    }
    else
      this.silver.SetActive(false);
    if ((bool) ((Object) this.dragonFossil))
    {
      if (!string.IsNullOrEmpty(data.abyss))
      {
        this.dragonFossil.SetActive(true);
        this.dragonFossil.GetComponentInChildren<UILabel>().text = Utils.FormatThousands(data.abyss);
      }
      else
        this.dragonFossil.SetActive(false);
    }
    if (!((Object) this.grid != (Object) null))
      return;
    this.grid.Reposition();
  }
}
