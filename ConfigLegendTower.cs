﻿// Decompiled with JetBrains decompiler
// Type: ConfigLegendTower
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigLegendTower
{
  private Dictionary<string, LegendTowerInfo> datas;
  private Dictionary<int, LegendTowerInfo> dicByUniqueId;
  private Dictionary<string, List<LegendTowerInfo>> towerList;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<LegendTowerInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.towerList = new Dictionary<string, List<LegendTowerInfo>>();
  }

  public LegendTowerInfo GetData(string id)
  {
    return this.datas[id];
  }

  public LegendTowerInfo GetData(int internalId)
  {
    return this.dicByUniqueId[internalId];
  }

  public LegendTowerInfo GetDataByPowerAndType(string towerType, int power)
  {
    if (this.towerList.ContainsKey(towerType))
    {
      List<LegendTowerInfo> tower = this.towerList[towerType];
      for (int index = tower.Count - 1; index >= 0; --index)
      {
        if (tower[index].reqHeroPower < power)
          return tower[index];
      }
    }
    return (LegendTowerInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId != null)
      this.dicByUniqueId.Clear();
    if (this.towerList == null)
      return;
    this.towerList.Clear();
  }
}
