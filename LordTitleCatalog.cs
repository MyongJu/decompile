﻿// Decompiled with JetBrains decompiler
// Type: LordTitleCatalog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LordTitleCatalog : MonoBehaviour
{
  private Color _catalogClickColor = new Color(0.9215686f, 0.5372549f, 0.0f);
  private Dictionary<int, LordTitleCatalog> _catalogItemDict = new Dictionary<int, LordTitleCatalog>();
  private GameObjectPool _itemPool = new GameObjectPool();
  [SerializeField]
  private UILabel _catalogName;
  [SerializeField]
  private UITexture _catalogTexture;
  [SerializeField]
  private UISprite _catalogArrowSprite;
  [SerializeField]
  private UISprite _catalogPickedSprite;
  [SerializeField]
  private GameObject _catalogLockStatus;
  [SerializeField]
  private GameObject _catalogCanUnlockStatus;
  [SerializeField]
  private GameObject _catalogActivatedStatus;
  [SerializeField]
  private GameObject _catalogNewStatus;
  [SerializeField]
  private LordTitleCatalog _parentCatalog;
  [SerializeField]
  private UITable _subcatalogTable;
  [SerializeField]
  private GameObject _subcatalogItemPrefab;
  [SerializeField]
  private GameObject _subcatalogItemRoot;
  private int _catalogLevel;
  private int _catalogId;
  private bool _catalogClicked;
  private Color _catalogOriginColor;
  public System.Action<LordTitleCatalog> OnCurrentCatalogClicked;

  public int CatalogLevel
  {
    get
    {
      return this._catalogLevel;
    }
  }

  public int CatalogId
  {
    get
    {
      return this._catalogId;
    }
  }

  public LordTitleCatalog ParentCatalog
  {
    get
    {
      return this._parentCatalog;
    }
  }

  public bool Folded
  {
    get
    {
      if (this.CatalogLevel == 2)
        return false;
      return !this._catalogClicked;
    }
  }

  public string CatalogName
  {
    get
    {
      return this._catalogName.text;
    }
  }

  public void SetData(int catalogLevel, int catalogId)
  {
    this._catalogLevel = catalogLevel;
    this._catalogId = catalogId;
    this._catalogOriginColor = this._catalogName.color;
    if (this._catalogOriginColor == Color.black)
      this._catalogOriginColor = Color.white;
    this.AddEventHandler();
    this.UpdateUI();
  }

  public void ClearData()
  {
    this.RemoveEventHandler();
    this.ClearCatalogData();
  }

  public void RefreshStatus()
  {
    NGUITools.SetActive(this._catalogLockStatus, LordTitlePayload.Instance.IsAvatorLocked(this._catalogId));
    NGUITools.SetActive(this._catalogCanUnlockStatus, LordTitlePayload.Instance.GetItemCountByAvator(this._catalogId) > 0);
    NGUITools.SetActive(this._catalogActivatedStatus, !LordTitlePayload.Instance.IsAvatorLocked(this._catalogId) && PlayerData.inst.userData.LordTitle == this._catalogId);
    NGUITools.SetActive(this._catalogNewStatus, LordTitlePayload.Instance.IsAvatorNew(this._catalogId));
  }

  public void SetNameColor()
  {
    if ((bool) ((UnityEngine.Object) this._catalogPickedSprite))
      NGUITools.SetActive(this._catalogPickedSprite.gameObject, true);
    if (!((UnityEngine.Object) this.ParentCatalog != (UnityEngine.Object) null))
      return;
    this.ParentCatalog.SetNameColor();
  }

  public void OnCatalogClicked()
  {
    this._catalogClicked = !this._catalogClicked;
    if ((UnityEngine.Object) this._catalogArrowSprite != (UnityEngine.Object) null)
      this._catalogArrowSprite.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, !this._catalogClicked ? 0.0f : -90f));
    if (this._catalogClicked && (UnityEngine.Object) this._subcatalogTable != (UnityEngine.Object) null)
      this.GenerateSubCatalog();
    else
      this.ClearCatalogData();
    this.RepositionCatalog();
    switch (this._catalogLevel)
    {
      case 1:
        if (this.OnCurrentCatalogClicked != null)
        {
          this.OnCurrentCatalogClicked(this);
          break;
        }
        break;
      case 2:
        if ((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null && this._parentCatalog.OnCurrentCatalogClicked != null)
        {
          this._parentCatalog.OnCurrentCatalogClicked(this);
          break;
        }
        break;
    }
    if (this._catalogLevel != 2 || this._catalogId <= 0)
      return;
    LordTitlePayload.Instance.SetUnlockedStatus(this._catalogId, 0);
    this.RefreshStatus();
  }

  public void ResetNameColor()
  {
    if ((double) this._catalogOriginColor.a <= 0.0 || !(bool) ((UnityEngine.Object) this._catalogPickedSprite))
      return;
    NGUITools.SetActive(this._catalogPickedSprite.gameObject, false);
  }

  public void SetDefaultSelected(int index)
  {
    if (!this._catalogItemDict.ContainsKey(index))
      return;
    this._catalogItemDict[index].OnCatalogClicked();
  }

  public void SetSelected(int lordTitleId)
  {
    if (this._catalogItemDict == null)
      return;
    using (Dictionary<int, LordTitleCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LordTitleCatalog> current = enumerator.Current;
        if (current.Value.CatalogId == lordTitleId)
        {
          current.Value.OnCatalogClicked();
          break;
        }
      }
    }
  }

  public void OnLockMarkClick()
  {
    if (LordTitlePayload.Instance.GetItemCountByAvator(this._catalogId) <= 0)
      return;
    LordTitlePayload.Instance.UnlockAvatorTitle(this._catalogId, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.UpdateUI();
    }));
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    DBManager.inst.DB_User.onDataUpdate += new System.Action<long>(this.OnUserDataUpdate);
    LordTitlePayload.Instance.OnLordTitleDataUpdate -= new System.Action(this.OnLordTitleDataUpdate);
    LordTitlePayload.Instance.OnLordTitleDataUpdate += new System.Action(this.OnLordTitleDataUpdate);
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecond);
  }

  private void RemoveEventHandler()
  {
    DBManager.inst.DB_User.onDataUpdate -= new System.Action<long>(this.OnUserDataUpdate);
    LordTitlePayload.Instance.OnLordTitleDataUpdate -= new System.Action(this.OnLordTitleDataUpdate);
    this.OnCurrentCatalogClicked = (System.Action<LordTitleCatalog>) null;
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecond);
  }

  private void OnSecond(int time)
  {
    this.UpdateUI();
  }

  private void OnUserDataUpdate(long uid)
  {
    if (uid != PlayerData.inst.uid)
      return;
    using (Dictionary<int, LordTitleCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.RefreshStatus();
    }
  }

  private void OnLordTitleDataUpdate()
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if ((UnityEngine.Object) this == (UnityEngine.Object) null)
      return;
    switch (this._catalogLevel)
    {
      case 1:
        LordTitleSortInfo lordTitleSortInfo = ConfigManager.inst.DB_LordTitleSort.Get(this._catalogId);
        if (lordTitleSortInfo != null)
        {
          this._catalogName.text = lordTitleSortInfo.LocName;
          if ((UnityEngine.Object) this._catalogTexture != (UnityEngine.Object) null)
          {
            this._catalogTexture.mainTexture = (Texture) null;
            break;
          }
          break;
        }
        break;
      case 2:
        LordTitleMainInfo lordTitleMainInfo = ConfigManager.inst.DB_LordTitleMain.Get(this._catalogId);
        if (lordTitleMainInfo != null)
        {
          this._catalogName.text = LordTitlePayload.Instance.IsAvatorLocked(this._catalogId) ? lordTitleMainInfo.LocName + LordTitlePayload.Instance.GetAvatorNameFixedTimeSuffix(this._catalogId) : lordTitleMainInfo.LocName + LordTitlePayload.Instance.GetAvatorNameTimeSuffix(this._catalogId, false);
          EquipmentManager.Instance.ConfigQualityLabelWithColor(this._catalogName, lordTitleMainInfo.quality);
          if ((UnityEngine.Object) this._catalogTexture != (UnityEngine.Object) null)
          {
            MarksmanPayload.Instance.FeedMarksmanTexture(this._catalogTexture, lordTitleMainInfo.AvatorIconPath);
            break;
          }
          break;
        }
        break;
    }
    this.RefreshStatus();
  }

  private void ClearCatalogData()
  {
    using (Dictionary<int, LordTitleCatalog>.Enumerator enumerator = this._catalogItemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, LordTitleCatalog> current = enumerator.Current;
        current.Value.OnCurrentCatalogClicked = (System.Action<LordTitleCatalog>) null;
        GameObject gameObject = current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._catalogItemDict.Clear();
    this._itemPool.Clear();
  }

  public void RepositionCatalog()
  {
    if ((UnityEngine.Object) this._subcatalogTable != (UnityEngine.Object) null)
    {
      this._subcatalogTable.repositionNow = true;
      this._subcatalogTable.Reposition();
    }
    if (!((UnityEngine.Object) this._parentCatalog != (UnityEngine.Object) null))
      return;
    this._parentCatalog.RepositionCatalog();
  }

  private LordTitleCatalog GenerateCatalogSlot(int catalogLevel, int catalogId)
  {
    this._itemPool.Initialize(this._subcatalogItemPrefab, this._subcatalogItemRoot);
    GameObject go = this._itemPool.AddChild(this._subcatalogTable.gameObject);
    NGUITools.SetActive(go, true);
    LordTitleCatalog component = go.GetComponent<LordTitleCatalog>();
    component.SetData(catalogLevel, catalogId);
    return component;
  }

  private void GenerateSubCatalog()
  {
    this.ClearCatalogData();
    List<LordTitleMainInfo> lordTitleMainInfoList = (List<LordTitleMainInfo>) null;
    if (this._catalogLevel == 1)
      lordTitleMainInfoList = ConfigManager.inst.DB_LordTitleMain.GetInfoListBySortId(this._catalogId);
    if (lordTitleMainInfoList == null)
      return;
    lordTitleMainInfoList.Sort((Comparison<LordTitleMainInfo>) ((a, b) => a.priority.CompareTo(b.priority)));
    for (int key = 0; key < lordTitleMainInfoList.Count; ++key)
    {
      if (lordTitleMainInfoList[key].internalId > 0)
      {
        LordTitleCatalog catalogSlot = this.GenerateCatalogSlot(this._catalogLevel + 1, lordTitleMainInfoList[key].internalId);
        this._catalogItemDict.Add(key, catalogSlot);
      }
    }
  }
}
