﻿// Decompiled with JetBrains decompiler
// Type: UpgradeBldngDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeBldngDlg : MonoBehaviour
{
  public UpgradeBldngDlg.RequirementClass[] mRequirements = new UpgradeBldngDlg.RequirementClass[6];
  public UpgradeBldngDlg.RewardClass[] mRewards = new UpgradeBldngDlg.RewardClass[6];
  public UILabel mTitle;
  public UILabel mBuildingTitle;
  public UILabel mBuildingLevel;
  public UILabel mStorageStats;
  public UISprite mStorageCurrent;
  public UISprite mStorageProjected;
  public UILabel mProductionStats;
  public UISprite mProductionCurrent;
  public UISprite mProductionProjected;
  public UILabel mActualBuildTime;
  public UILabel mStandardBuildTime;
  public UILabel mInstBuildCost;
  public UISprite mBuildingIcon;
  public UIButton mUpgradeBtn;
  public UIButton mInstUpgradeBtn;
  public UIAtlas mRural1Atlas;
  public UIAtlas mUrban1Atlas;
  public int _reqIndex;
  private System.Action _OnFinished;
  private CityManager.BuildingItem _building;
  private BuildingInfo _info;

  public void Show(CityManager.BuildingItem building, PlayerResourceData playerData, System.Action onFinished)
  {
    this._OnFinished = onFinished;
    this._building = building;
    this._info = ConfigManager.inst.DB_Building.GetData(building.mType, building.mLevel + 1);
    BuildingInfo info = this._info;
    int level1 = info.Building_Lvl + 1 + 2;
    BuildingInfo data1 = ConfigManager.inst.DB_Building.GetData(info.Type, level1);
    int level2 = info.Building_Lvl + 2 + 2;
    BuildingInfo data2 = ConfigManager.inst.DB_Building.GetData(info.Type, level2);
    BuildingInfo data3 = ConfigManager.inst.DB_Building.GetData(info.Type, info.Building_Lvl - 1);
    BuildingInfo data4 = ConfigManager.inst.DB_Building.GetData(info.Type, info.Building_Lvl);
    this.mTitle.text = ScriptLocalization.Get("UPGRADE_TO_LEVEL", true) + " " + info.Building_Lvl.ToString();
    this.CreateBuildingIcon(info.Type, this.mBuildingIcon);
    this.mBuildingTitle.text = ScriptLocalization.Get(info.Type, true);
    this.mBuildingLevel.text = string.Format("Lvl {0} - {1}", (object) (info.Building_Lvl - 1), (object) info.Building_Lvl);
    this.mStorageProjected.width = 1196 * (int) data4.Benefit_Value_1 / Mathf.Max(1, (int) data1.Benefit_Value_1);
    this.mStorageCurrent.width = 1196 * (int) data3.Benefit_Value_1 / Mathf.Max(1, (int) data1.Benefit_Value_1);
    string str1 = string.Format("{0} + {1}", (object) Utils.FormatThousands(data3.Benefit_Value_1.ToString()), (object) Utils.FormatThousands((data4.Benefit_Value_1 - data3.Benefit_Value_1).ToString()));
    this.mStorageStats.text = ScriptLocalization.Get(info.Benefit_ID_1, true) + ":   " + str1;
    this.mProductionProjected.width = 1196 * (int) data4.Benefit_Value_2 / Mathf.Max(1, (int) data2.Benefit_Value_2);
    this.mProductionCurrent.width = 1196 * (int) data3.Benefit_Value_2 / Mathf.Max(1, (int) data2.Benefit_Value_2);
    string str2 = string.Format("{0} + {1} {2}", (object) Utils.FormatThousands(data3.Benefit_Value_2.ToString()), (object) Utils.FormatThousands((data4.Benefit_Value_2 - data3.Benefit_Value_2).ToString()), (object) ScriptLocalization.Get("PER_HOUR", true));
    this.mProductionStats.text = ScriptLocalization.Get(info.Benefit_ID_2, true) + ":   " + str2;
    double buildTime = info.Build_Time;
    double seconds = buildTime;
    this.mActualBuildTime.text = Utils.ConvertSecsToString(buildTime);
    this.mStandardBuildTime.text = Utils.ConvertSecsToString(seconds);
    this.mInstBuildCost.text = "999";
    if (999 > PlayerData.inst.hostPlayer.Currency)
    {
      this.mInstUpgradeBtn.isEnabled = false;
      Utils.ColorFamily(this.mInstUpgradeBtn.gameObject, 0.6f);
    }
    CityManager inst = CityManager.inst;
    bool flag = true;
    string str3 = string.Empty;
    string str4 = string.Empty;
    if (info.Requirement_ID_1 != string.Empty)
      str3 = ConfigManager.inst.DB_Building.GetData(int.Parse(info.Requirement_ID_1)).Type;
    if (info.Requirement_ID_2 != string.Empty)
      str4 = ConfigManager.inst.DB_Building.GetData(int.Parse(info.Requirement_ID_2)).Type;
    if (str3.IndexOf("ITEM") > -1)
      info.Requirement_Value_1 = 0;
    if (str4.IndexOf("ITEM") > -1)
      info.Requirement_Value_1 = 0;
    if (info.Requirement_Value_1 > 0 && info.Requirement_Value_1 > inst.GetHighestBuildingLevelFor(str3))
      flag = false;
    if (info.Requirement_Value_2 > 0 && info.Requirement_Value_2 > inst.GetHighestBuildingLevelFor(str4))
      flag = false;
    if (info.Silver > 0.0 && info.Silver > (double) playerData.mSilver)
      flag = false;
    if (info.Food > 0.0 && info.Food > (double) playerData.mFood)
      flag = false;
    if (info.Wood > 0.0 && info.Wood > (double) playerData.mWood)
      flag = false;
    if (info.Ore > 0.0 && info.Ore > (double) playerData.mOre)
      flag = false;
    if (CityManager.inst.GetBuildingsInFlux() > 0)
      flag = false;
    if (!flag)
    {
      this.mUpgradeBtn.isEnabled = false;
      this.mInstUpgradeBtn.isEnabled = false;
    }
    this.HideAllRequirements();
    if (info.Requirement_Value_1 > 0)
      this.AddRequirement("EmpireIcon", str3, info.Requirement_Value_1, inst.GetHighestBuildingLevelFor(str3));
    if (info.Requirement_Value_2 > 0)
      this.AddRequirement("EmpireIcon", str4, info.Requirement_Value_2, inst.GetHighestBuildingLevelFor(str4));
    if (info.Silver > 0.0)
      this.AddRequirement("SilverIcon", string.Empty, (int) info.Silver, (int) playerData.mSilver);
    if (info.Food > 0.0)
      this.AddRequirement("FoodIcon", string.Empty, (int) info.Food, (int) playerData.mFood);
    if (info.Wood > 0.0)
      this.AddRequirement("WoodIcon", string.Empty, (int) info.Wood, (int) playerData.mWood);
    if (info.Ore <= 0.0)
      return;
    this.AddRequirement("OreIcon", string.Empty, (int) info.Ore, (int) playerData.mOre);
  }

  public void HideAllRequirements()
  {
    for (int index = 0; index < 6; ++index)
    {
      this.mRequirements[index].mIcon.enabled = false;
      this.mRequirements[index].mDesc.enabled = false;
      this.mRequirements[index].mTickCross.enabled = false;
    }
    this._reqIndex = 0;
  }

  public void AddRequirement(string iconName, string description, int amountRequired, int ownedAmount)
  {
    if (this._reqIndex >= 5)
      return;
    UpgradeBldngDlg.RequirementClass mRequirement = this.mRequirements[this._reqIndex];
    mRequirement.mIcon.enabled = true;
    mRequirement.mDesc.enabled = true;
    mRequirement.mTickCross.enabled = true;
    mRequirement.mIcon.spriteName = iconName;
    mRequirement.mIcon.MakePixelPerfect();
    mRequirement.mIcon.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
    mRequirement.mDesc.text = description.Length <= 0 ? Utils.FormatThousands(amountRequired.ToString()) : string.Format("{0} {1} {2}", (object) ScriptLocalization.Get(description, true), (object) ScriptLocalization.Get("UPGRADE_LEVEL", true), (object) Utils.FormatThousands(amountRequired.ToString()));
    mRequirement.mTickCross.spriteName = amountRequired <= ownedAmount ? "GreenTick" : "RedCross";
    if (amountRequired > ownedAmount)
      mRequirement.mDesc.color = Utils.GetColorFromHex(14091781U);
    else
      mRequirement.mDesc.color = Color.white;
    ++this._reqIndex;
  }

  public void AddLimitRequirement(int ownedAmount, int allowedAmount)
  {
    if (this._reqIndex >= 5)
      return;
    UpgradeBldngDlg.RequirementClass mRequirement = this.mRequirements[this._reqIndex];
    mRequirement.mIcon.enabled = true;
    mRequirement.mDesc.enabled = true;
    mRequirement.mTickCross.enabled = true;
    mRequirement.mIcon.spriteName = "EmpireIcon";
    mRequirement.mIcon.MakePixelPerfect();
    mRequirement.mIcon.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
    mRequirement.mDesc.text = "Built " + ownedAmount.ToString() + " / " + allowedAmount.ToString();
    mRequirement.mTickCross.spriteName = ownedAmount < allowedAmount ? "GreenTick" : "RedCross";
    if (ownedAmount >= allowedAmount)
      mRequirement.mDesc.color = Utils.GetColorFromHex(14091781U);
    else
      mRequirement.mDesc.color = Color.white;
    ++this._reqIndex;
  }

  private void CreateBuildingIcon(string buildingName, UISprite icon)
  {
    string key = buildingName;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (UpgradeBldngDlg.\u003C\u003Ef__switch\u0024map3F == null)
    {
      // ISSUE: reference to a compiler-generated field
      UpgradeBldngDlg.\u003C\u003Ef__switch\u0024map3F = new Dictionary<string, int>(15)
      {
        {
          "farm",
          0
        },
        {
          "mine",
          1
        },
        {
          "lumber_mill",
          2
        },
        {
          "house",
          3
        },
        {
          "marketplace",
          4
        },
        {
          "healers_tent",
          5
        },
        {
          "storehouse",
          6
        },
        {
          "prison",
          7
        },
        {
          "hero_tower",
          8
        },
        {
          "forge",
          9
        },
        {
          "knights_hall",
          10
        },
        {
          "troop_camp",
          11
        },
        {
          "university",
          12
        },
        {
          "hospital",
          13
        },
        {
          "barracks",
          14
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!UpgradeBldngDlg.\u003C\u003Ef__switch\u0024map3F.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        icon.atlas = this.mRural1Atlas;
        icon.spriteName = "Building_3x3_Farm";
        break;
      case 1:
        icon.atlas = this.mRural1Atlas;
        icon.spriteName = "Building_3x3_Mine";
        break;
      case 2:
        icon.atlas = this.mRural1Atlas;
        icon.spriteName = "Building_3x3_Wood";
        break;
      case 3:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_3x3_House";
        break;
      case 4:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_Marketplace";
        break;
      case 5:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_2x2_HealerTent";
        break;
      case 6:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_Storehouse";
        break;
      case 7:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_Prison";
        break;
      case 8:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_3x3_HeroTower";
        break;
      case 9:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_Forge";
        break;
      case 10:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_KnightsHall";
        break;
      case 11:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_2x2_TroopCamp";
        break;
      case 12:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_Academy";
        break;
      case 13:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_Temple";
        break;
      case 14:
        icon.atlas = this.mUrban1Atlas;
        icon.spriteName = "Building_4x4_TrainingGround";
        break;
    }
  }

  public void OnCancelBtnPressed()
  {
    this._OnFinished();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  public void OnInstUpgradePressed()
  {
    CityManager.inst.UpgradeBuilding(this._building.mID, true, 0.0, 0);
    this.OnCancelBtnPressed();
  }

  public void OnUpgradePressed()
  {
    CityManager.inst.UpgradeBuilding(this._building.mID, false, 0.0, 0);
    this.OnCancelBtnPressed();
  }

  [Serializable]
  public class RequirementClass
  {
    public UISprite mIcon;
    public UILabel mDesc;
    public UISprite mTickCross;
  }

  [Serializable]
  public class RewardClass
  {
    public UISprite mIcon;
    public UILabel mDesc;
  }
}
