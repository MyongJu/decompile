﻿// Decompiled with JetBrains decompiler
// Type: ParadeGroundDlgDetailSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using UnityEngine;

public class ParadeGroundDlgDetailSlot : MonoBehaviour
{
  [SerializeField]
  private ParadeGroundDlgDetailSlot.Panel panel;

  public void SetupTrap(string trapID, int troopCount)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(trapID);
    this.panel.troopName.text = ScriptLocalization.Get(data.Troop_Name_LOC_ID, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, data.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
    this.panel.troopCount.text = Utils.FormatShortThousands(troopCount);
    this.panel.troopLevel.text = Utils.GetRomaNumeralsBelowTen(data.Troop_Tier);
  }

  public void SetupTroop(string troopID, int cityTroopCount, int allTroopCount)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(troopID);
    this.panel.troopName.text = ScriptLocalization.Get(data.Troop_Name_LOC_ID, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, data.Troop_ICON, (System.Action<bool>) null, true, false, string.Empty);
    this.panel.troopCount.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousands(cityTroopCount), (object) Utils.FormatShortThousands(allTroopCount));
    this.panel.troopLevel.text = Utils.GetRomaNumeralsBelowTen(data.Troop_Tier);
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public UILabel troopName;
    public UILabel troopCount;
    public UILabel troopLevel;
  }
}
