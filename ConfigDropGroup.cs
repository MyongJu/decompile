﻿// Decompiled with JetBrains decompiler
// Type: ConfigDropGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigDropGroup
{
  private Dictionary<int, DropGroupData> m_DropGroupDataDictByInternalId;
  private Dictionary<string, DropGroupData> m_DropGroupDataDictByUniqueId;

  public void BuildDB(object result)
  {
    new ConfigParse().Parse<DropGroupData, string>(result as Hashtable, "ID", out this.m_DropGroupDataDictByUniqueId, out this.m_DropGroupDataDictByInternalId);
  }

  public DropGroupData GetByInternalId(int internalId)
  {
    DropGroupData dropGroupData;
    this.m_DropGroupDataDictByInternalId.TryGetValue(internalId, out dropGroupData);
    return dropGroupData;
  }

  public DropGroupData GetByUniqueId(string uniqueId)
  {
    DropGroupData dropGroupData;
    this.m_DropGroupDataDictByUniqueId.TryGetValue(uniqueId, out dropGroupData);
    return dropGroupData;
  }
}
