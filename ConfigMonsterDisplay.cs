﻿// Decompiled with JetBrains decompiler
// Type: ConfigMonsterDisplay
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigMonsterDisplay
{
  public Dictionary<string, MonsterDisplayInfo> _type2info = new Dictionary<string, MonsterDisplayInfo>();
  public Dictionary<int, MonsterDisplayInfo> _datas;

  public void BuildDB(object res)
  {
    Debug.Log((object) (nameof (res) + Utils.Object2Json(res)));
    ConfigParse configParse = new ConfigParse();
    configParse.Clear();
    configParse.Parse<MonsterDisplayInfo, int>(res as Hashtable, "internalId", out this._datas);
    Dictionary<int, MonsterDisplayInfo>.ValueCollection.Enumerator enumerator = this._datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this._type2info.ContainsKey(enumerator.Current.monsterType))
      {
        string[] strArray1 = enumerator.Current.offsetOrgValue.Split(',');
        if (strArray1.Length == 3)
        {
          enumerator.Current.offsetValue.x = float.Parse(strArray1[0]);
          enumerator.Current.offsetValue.y = float.Parse(strArray1[1]);
          enumerator.Current.offsetValue.z = float.Parse(strArray1[2]);
        }
        else
          enumerator.Current.offsetValue = Vector3.zero;
        string[] strArray2 = enumerator.Current.scalingOrgValue.Split(',');
        if (strArray2.Length == 3)
        {
          enumerator.Current.scalingValue.x = float.Parse(strArray2[0]);
          enumerator.Current.scalingValue.y = float.Parse(strArray2[1]);
          enumerator.Current.scalingValue.z = float.Parse(strArray2[2]);
        }
        else
          enumerator.Current.scalingValue = Vector3.one;
        string[] strArray3 = enumerator.Current.rotationOrgValue.Split(',');
        if (strArray3.Length == 3)
        {
          Vector3 vector3;
          vector3.x = float.Parse(strArray3[0]);
          vector3.y = float.Parse(strArray3[1]);
          vector3.z = float.Parse(strArray3[2]);
          enumerator.Current.rotationValue.eulerAngles = vector3;
        }
        else
          enumerator.Current.rotationValue = Quaternion.identity;
        this._type2info.Add(enumerator.Current.monsterType, enumerator.Current);
      }
    }
  }

  public MonsterDisplayInfo GetData(int internalId)
  {
    if (this._datas.ContainsKey(internalId))
      return this._datas[internalId];
    return (MonsterDisplayInfo) null;
  }

  public MonsterDisplayInfo GetDataByType(string type)
  {
    if (this._type2info.ContainsKey(type))
      return this._type2info[type];
    return (MonsterDisplayInfo) null;
  }
}
