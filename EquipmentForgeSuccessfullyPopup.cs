﻿// Decompiled with JetBrains decompiler
// Type: EquipmentForgeSuccessfullyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class EquipmentForgeSuccessfullyPopup : Popup
{
  public EquipmentBenefits benefitContent;
  public EquipmentComponent equipComponent;
  public UIButton euipBtn;
  public UILabel equipBtnLabel;
  public GameObject fxForge;
  public GameObject fxEnhance;
  private long equipmenItemID;
  private ConfigEquipmentMainInfo mainInfo;
  private BagType bagType;

  private void Init()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.equipmenItemID);
    this.benefitContent.Init(ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(myItemByItemId.internalId, myItemByItemId.enhanced).benefits);
    this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(myItemByItemId.internalId, myItemByItemId.enhanced, this.bagType, myItemByItemId.itemId));
    this.mainInfo = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(myItemByItemId.internalId);
    if (this.bagType == BagType.Hero && this.mainInfo.heroLevelMin > PlayerData.inst.heroData.level || this.bagType == BagType.DragonKnight && PlayerData.inst.dragonKnightData != null && this.mainInfo.heroLevelMin > PlayerData.inst.dragonKnightData.Level)
    {
      string format = this.bagType != BagType.Hero ? Utils.XLAT("dragon_knight_equip_level_requirement") : Utils.XLAT("forge_equip_requirement");
      this.euipBtn.isEnabled = false;
      this.equipBtnLabel.text = Utils.XLAT("forge_uppercase_equip_button") + " [FF0000](" + string.Format(format, (object) this.mainInfo.heroLevelMin) + ")";
    }
    else if (EquipmentManager.Instance.GetEquippedItemID(this.bagType, this.mainInfo.type) > 0)
    {
      this.euipBtn.isEnabled = true;
      if (myItemByItemId.isEquipped)
      {
        this.equipBtnLabel.text = Utils.XLAT("id_uppercase_okay");
        this.euipBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnOKBtnClick)));
      }
      else
      {
        this.equipBtnLabel.text = Utils.XLAT("forge_uppercase_replace_button");
        this.euipBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnReplaceBtnClick)));
      }
    }
    else
    {
      this.euipBtn.isEnabled = true;
      this.equipBtnLabel.text = Utils.XLAT("forge_uppercase_equip_button");
      this.euipBtn.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnEquipBtnClick)));
    }
    this.fxForge.SetActive(myItemByItemId.enhanced == 0);
    this.fxEnhance.SetActive(myItemByItemId.enhanced > 0);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnReplaceBtnClick()
  {
    int equippedItemId = EquipmentManager.Instance.GetEquippedItemID(this.bagType, this.mainInfo.type);
    InventoryItemData byItemId1 = DBManager.inst.GetInventory(this.bagType).GetByItemID(PlayerData.inst.uid, (long) equippedItemId);
    InventoryItemData byItemId2 = DBManager.inst.GetInventory(this.bagType).GetByItemID(PlayerData.inst.uid, this.equipmenItemID);
    if (byItemId1 == null || byItemId2 == null)
      return;
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, byItemId2, byItemId1, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.ReplaceEquipment(this.bagType, (long) equippedItemId, this.equipmenItemID, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.OnCloseBtnClick();
      if (this.bagType == BagType.Hero)
        UIManager.inst.OpenDlg("PlayerProfile/PlayerProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      else
        UIManager.inst.OpenDlg("DragonKnight/DragonKnightProfileDlg", (UI.Dialog.DialogParameter) null, true, true, true);
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  public void OnOKBtnClick()
  {
    this.OnCloseBtnClick();
  }

  public void OnEquipBtnClick()
  {
    List<Benefits.BenefitValuePair> allAddedBenifit;
    List<Benefits.BenefitValuePair> allRemovedBenefit;
    ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance;
    ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance;
    Utils.GetEquipmentBenefitChangedInfo(this.bagType, DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.equipmenItemID), (InventoryItemData) null, out allRemovedBenefit, out allAddedBenifit, out addEquipmentSuitEnhance, out delEquipmentSuitEnhance);
    EquipmentManager.Instance.Equip(this.bagType, this.equipmenItemID, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
      if (allAddedBenifit.Count <= 0 && allRemovedBenefit.Count <= 0 && (addEquipmentSuitEnhance == null && delEquipmentSuitEnhance == null))
        return;
      UIManager.inst.ShowEquipmentSuitBenefitChangeTip(allAddedBenifit, allRemovedBenefit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }));
  }

  private void Dispose()
  {
    this.benefitContent.Dispose();
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.equipmenItemID = (orgParam as EquipmentForgeSuccessfullyPopup.Parameter).equipmenItemID;
    this.bagType = (orgParam as EquipmentForgeSuccessfullyPopup.Parameter).bagType;
    this.Init();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.Dispose();
    base.OnClose(orgParam);
  }

  public class Parameter : Popup.PopupParameter
  {
    public long equipmenItemID;
    public BagType bagType;
  }
}
