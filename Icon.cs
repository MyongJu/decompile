﻿// Decompiled with JetBrains decompiler
// Type: Icon
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Icon : ComponentRenderBase
{
  private bool autoFillName = true;
  public UILabel[] labels;
  public UITexture texture;
  public GameObject[] visibles;
  public Transform mBorder;
  public UITexture iconBackground;
  public System.Action<Icon> OnIconPressDelegate;
  public System.Action<Icon> OnIconRelaseDelegate;
  public System.Action<Icon> OnIconClickDelegate;
  private bool autoShowTip;

  public void StartAutoTip()
  {
    this.autoShowTip = true;
  }

  public void StopAutoFillName()
  {
    this.autoFillName = false;
  }

  public void OnClickHandler()
  {
    if (this.OnIconClickDelegate != null)
      this.OnIconClickDelegate(this);
    if (!this.autoShowTip)
      return;
    IconData data = this.data as IconData;
    if (data == null || data.Data == null)
      return;
    string s = data.Data.ToString();
    int result = 0;
    if (!int.TryParse(s, out result))
      return;
    Utils.ShowItemTip(result, this.mBorder, 0L, 0L, 0);
  }

  protected virtual void LoadItemName()
  {
    if (!this.autoFillName || this.labels == null || this.labels.Length <= 0)
      return;
    IconData data = this.data as IconData;
    if (data == null || data.Data == null)
      return;
    string s = data.Data.ToString();
    int result = 0;
    if (!int.TryParse(s, out result))
      return;
    Utils.SetItemName(this.labels[0], result);
  }

  protected virtual void LoadBackground()
  {
    if (!((UnityEngine.Object) this.iconBackground != (UnityEngine.Object) null))
      return;
    IconData data = this.data as IconData;
    if (data == null || data.Data == null)
      return;
    string s = data.Data.ToString();
    int result = 0;
    if (!int.TryParse(s, out result))
      return;
    Utils.SetItemBackground(this.iconBackground, result);
  }

  public void OnPressHandler()
  {
    if (this.OnIconPressDelegate != null)
      this.OnIconPressDelegate(this);
    if (!this.autoShowTip)
      return;
    IconData data = this.data as IconData;
    if (data == null || data.Data == null)
      return;
    string s = data.Data.ToString();
    int result = 0;
    if (!int.TryParse(s, out result))
      return;
    Utils.DelayShowTip(result, this.mBorder, 0L, 0L, 0);
  }

  public void OnReleaseHandler()
  {
    if (this.OnIconRelaseDelegate != null)
      this.OnIconRelaseDelegate(this);
    if (!this.autoShowTip)
      return;
    Utils.StopShowItemTip();
  }

  public void LoadDefaultBackgroud()
  {
    if (!((UnityEngine.Object) this.iconBackground != (UnityEngine.Object) null))
      return;
    Utils.SetItemNormalBackground(this.iconBackground, 0);
  }

  public override void Init()
  {
    IconData data = this.data as IconData;
    if (data.seticon != null)
      this.SetData(data.image, data.seticon, data.lordTitleId, data.contents);
    else
      this.SetData(data.image, data.contents);
    this.LoadBackground();
    this.LoadItemName();
    if (data.visibles == null)
      return;
    this.SetVisibles(data.visibles);
  }

  public void Empty()
  {
    NGUITools.SetActive(this.gameObject, false);
  }

  public override void Dispose()
  {
    if ((UnityEngine.Object) this.texture != (UnityEngine.Object) null)
      BuilderFactory.Instance.Release((UIWidget) this.texture);
    this.OnIconPressDelegate = (System.Action<Icon>) null;
    this.OnIconRelaseDelegate = (System.Action<Icon>) null;
    this.OnIconClickDelegate = (System.Action<Icon>) null;
  }

  public void SetSingleData(string content, string path)
  {
    this.labels[0].text = content;
    if (!((UnityEngine.Object) this.texture != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, path, (System.Action<bool>) null, false, false, string.Empty);
  }

  public void SetData(string path, params string[] contents)
  {
    this.SetData(contents, path);
  }

  public void SetData(string path, string customIconUrl, int lordTitleId, string[] contents)
  {
    for (int index = 0; index < this.labels.Length; ++index)
    {
      if (index < contents.Length && (bool) ((UnityEngine.Object) this.labels[index]))
        this.labels[index].text = contents[index];
    }
    if (!((UnityEngine.Object) this.texture != (UnityEngine.Object) null))
      return;
    CustomIconLoader.Instance.requestCustomIcon(this.texture, path, customIconUrl, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.texture, lordTitleId, 1);
  }

  public virtual void SetData(string[] contents, string path)
  {
    for (int index = 0; index < this.labels.Length; ++index)
    {
      if (index < contents.Length && (bool) ((UnityEngine.Object) this.labels[index]))
        this.labels[index].text = contents[index];
    }
    if (!((UnityEngine.Object) this.texture != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture, path, (System.Action<bool>) null, false, true, string.Empty);
  }

  public void SetVisibles(bool[] allVisibleState)
  {
    if (this.visibles == null)
      return;
    for (int index = 0; index < allVisibleState.Length; ++index)
    {
      if (index < this.visibles.Length)
        this.visibles[index].SetActive(allVisibleState[index]);
    }
  }
}
