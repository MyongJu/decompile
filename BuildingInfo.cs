﻿// Decompiled with JetBrains decompiler
// Type: BuildingInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildingInfo
{
  public const string CITY_ZONE_RURAL = "rural";
  public const string CITY_ZONE_FIXED = "fixed";
  public const string CITY_ZONE_URBAN = "urban";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "type")]
  public string Type;
  [Config(Name = "city_zone")]
  public string City_Zone;
  [Config(Name = "destroyed")]
  public int Destrotyed;
  [Config(Name = "priority")]
  public int Priority;
  [Config(Name = "unique")]
  public int unique;
  [Config(Name = "level")]
  public int Building_Lvl;
  [Config(Name = "req_id_1")]
  public string Requirement_ID_1;
  [Config(Name = "req_id_2")]
  public string Requirement_ID_2;
  [Config(Name = "req_id_3")]
  public string Requirement_ID_3;
  [Config(Name = "req_id_4")]
  public string Requirement_ID_4;
  [Config(Name = "req_id_5")]
  public string Requirement_ID_5;
  [Config(Name = "req_value_1")]
  public int Requirement_Value_1;
  [Config(Name = "req_value_2")]
  public int Requirement_Value_2;
  [Config(Name = "req_value_3")]
  public int Requirement_Value_3;
  [Config(Name = "req_value_4")]
  public int Requirement_Value_4;
  [Config(Name = "req_value_5")]
  public int Requirement_Value_5;
  [Config(Name = "food")]
  public double Food;
  [Config(Name = "wood")]
  public double Wood;
  [Config(Name = "ore")]
  public double Ore;
  [Config(Name = "silver")]
  public double Silver;
  [Config(Name = "build_time")]
  public double Build_Time;
  [Config(Name = "benefit_id_1")]
  public string Benefit_ID_1;
  [Config(Name = "benefit_id_2")]
  public string Benefit_ID_2;
  [Config(Name = "benefit_id_3")]
  public string Benefit_ID_3;
  [Config(Name = "benefit_id_4")]
  public string Benefit_ID_4;
  [Config(Name = "benefit_value_1")]
  public double Benefit_Value_1;
  [Config(Name = "benefit_value_2")]
  public double Benefit_Value_2;
  [Config(Name = "benefit_value_3")]
  public double Benefit_Value_3;
  [Config(Name = "benefit_value_4")]
  public double Benefit_Value_4;
  [Config(Name = "rural_benefits_1")]
  public int GenerateInHour;
  [Config(Name = "rural_benefits_2")]
  public int Storage;
  [Config(Name = "collect_min")]
  public int CollectionMin;
  [Config(Name = "speedup_gold")]
  public int SpeedupGold;
  [Config(Name = "hero_xp")]
  public int Hero_XP;
  [Config(Name = "power")]
  public int Power;
  [Config(Name = "image_path")]
  public string Building_ImagePath;
  [Config(Name = "loc_id")]
  public string Building_LOC_ID;
  [Config(Name = "benefit_display_1")]
  public int benefit_Display_1;
  [Config(Name = "benefit_display_2")]
  public int benefit_Display_2;
  [Config(Name = "benefit_display_3")]
  public int benefit_Display_3;
  [Config(Name = "benefit_display_4")]
  public int benefit_Display_4;

  public bool Unique
  {
    get
    {
      return this.unique == 1;
    }
  }

  public bool Benefit_Display_1
  {
    get
    {
      return this.benefit_Display_1 == 1;
    }
  }

  public bool Benefit_Display_2
  {
    get
    {
      return this.benefit_Display_2 == 1;
    }
  }

  public bool Benefit_Display_3
  {
    get
    {
      return this.benefit_Display_3 == 1;
    }
  }

  public bool Benefit_Display_4
  {
    get
    {
      return this.benefit_Display_4 == 1;
    }
  }

  public string Building_Description_LOC_ID
  {
    get
    {
      return this.ID + "_description";
    }
  }

  public string Building_NAME_LOC_ID
  {
    get
    {
      return this.ID + "_name";
    }
  }

  public int BenefitWood
  {
    get
    {
      return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Wood, "calc_construct_wood_cost"));
    }
  }

  public int BenefitFood
  {
    get
    {
      return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Food, "calc_construct_food_cost"));
    }
  }

  public int BenefitOre
  {
    get
    {
      return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Ore, "calc_construct_ore_cost"));
    }
  }

  public int BenefitSilver
  {
    get
    {
      return (int) Mathf.Max(0.0f, ConfigManager.inst.DB_BenefitCalc.GetFinalData((float) this.Silver, "calc_construct_silver_cost"));
    }
  }
}
