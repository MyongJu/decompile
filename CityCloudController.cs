﻿// Decompiled with JetBrains decompiler
// Type: CityCloudController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CityCloudController : MonoBehaviour
{
  public float speedScale = 1f;

  private void Start()
  {
    this.GetComponent<ParticleSystem>().startSpeed *= this.speedScale * CityEnvironment.WindForce.x;
  }
}
