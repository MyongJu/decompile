﻿// Decompiled with JetBrains decompiler
// Type: TempleSkillType
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class TempleSkillType
{
  public const string HEAL_SPEED = "heal_speed";
  public const string INTERRUPT = "interrupt";
  public const string CHAINS = "chains";
  public const string RESEARCH_SPEED = "research_speed";
  public const string SHIELD = "shield";
  public const string FLAME = "flame";
  public const string BUILDING_SPEED = "building_speed";
  public const string FORCE_TELEPORT = "force_teleport";
  public const string WOUNDED_KILLER = "wounded_killer";
  public const string GATHER_SPEED = "gather_speed";
  public const string TENACIOUS = "tenacious";
  public const string MARCH_SPEED = "march_speed";
}
