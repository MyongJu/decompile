﻿// Decompiled with JetBrains decompiler
// Type: AssetConfigData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;

public class AssetConfigData
{
  public Dictionary<string, string> map = new Dictionary<string, string>();

  public void Merge(AssetConfigData assetConfigData)
  {
    if (assetConfigData == null)
      return;
    Dictionary<string, string>.KeyCollection.Enumerator enumerator = assetConfigData.map.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (!this.map.ContainsKey(enumerator.Current))
        this.map.Add(enumerator.Current, assetConfigData.map[enumerator.Current]);
    }
    this.CheckAssetHotfix();
  }

  private void CheckAssetHotfix()
  {
    string str = Utils.Object2Json(NetApi.inst.HotfixAssetContent);
    if (string.IsNullOrEmpty(str) || str == "null")
      return;
    using (Dictionary<string, string>.Enumerator enumerator = JsonReader.Deserialize<Dictionary<string, string>>(str).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        if (this.map.ContainsKey(current.Key))
          this.map[current.Key] = current.Value;
        else
          this.map.Add(current.Key, current.Value);
      }
    }
  }
}
