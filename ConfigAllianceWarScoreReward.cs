﻿// Decompiled with JetBrains decompiler
// Type: ConfigAllianceWarScoreReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigAllianceWarScoreReward
{
  private Dictionary<string, AllianceWarScoreRewardInfo> datas;
  private Dictionary<int, AllianceWarScoreRewardInfo> dicByUniqueId;
  private List<AllianceWarScoreRewardInfo> rewardInfoList;

  public List<AllianceWarScoreRewardInfo> RewardInfoList
  {
    get
    {
      return this.rewardInfoList;
    }
  }

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<AllianceWarScoreRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    this.rewardInfoList = new List<AllianceWarScoreRewardInfo>();
    using (Dictionary<int, AllianceWarScoreRewardInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.rewardInfoList.Add(enumerator.Current.Value);
    }
    this.rewardInfoList.Sort((Comparison<AllianceWarScoreRewardInfo>) ((x, y) => x.ScoreReq.CompareTo(y.ScoreReq)));
  }

  public AllianceWarScoreRewardInfo GetData(int id)
  {
    if (this.dicByUniqueId.ContainsKey(id))
      return this.dicByUniqueId[id];
    return (AllianceWarScoreRewardInfo) null;
  }

  public AllianceWarScoreRewardInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (AllianceWarScoreRewardInfo) null;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, AllianceWarScoreRewardInfo>) null;
    }
    if (this.dicByUniqueId != null)
    {
      this.dicByUniqueId.Clear();
      this.dicByUniqueId = (Dictionary<int, AllianceWarScoreRewardInfo>) null;
    }
    if (this.rewardInfoList == null)
      return;
    this.rewardInfoList.Clear();
    this.rewardInfoList = (List<AllianceWarScoreRewardInfo>) null;
  }
}
