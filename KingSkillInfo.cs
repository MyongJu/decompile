﻿// Decompiled with JetBrains decompiler
// Type: KingSkillInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class KingSkillInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "skill_type")]
  public string SkillType;
  [Config(Name = "kingdom_buff_id")]
  public int KingdomBuffId;
  [Config(Name = "cost")]
  public int Cost;
  [Config(Name = "cd")]
  public int CD;
  [Config(Name = "max_use")]
  public int MaxUse;
  [Config(Name = "auto_open_type")]
  public string AutoOpenType;
  [Config(Name = "skill_param_1")]
  public float SkillParam1;
  [Config(Name = "skill_param_2")]
  public float SkillParam2;
  [Config(Name = "name")]
  public string Name;
  [Config(Name = "description")]
  public string Description;
  [Config(Name = "icon")]
  public string Icon;

  public string Loc_Name
  {
    get
    {
      return ScriptLocalization.Get(this.Name, true);
    }
  }

  public string Loc_Desc
  {
    get
    {
      return ScriptLocalization.Get(this.Description, true);
    }
  }

  public string ImagePath
  {
    get
    {
      return "Texture/KingdomSkillIcons/" + this.Icon;
    }
  }

  public bool IsSingleCast
  {
    get
    {
      string skillType = this.SkillType;
      if (skillType != null)
      {
        if (KingSkillInfo.\u003C\u003Ef__switch\u0024map43 == null)
          KingSkillInfo.\u003C\u003Ef__switch\u0024map43 = new Dictionary<string, int>(2)
          {
            {
              "force_teleport",
              0
            },
            {
              "send_troop_forbidden",
              0
            }
          };
        int num;
        if (KingSkillInfo.\u003C\u003Ef__switch\u0024map43.TryGetValue(skillType, out num) && num == 0)
          return true;
      }
      return false;
    }
  }

  public bool IsHarmful
  {
    get
    {
      string skillType = this.SkillType;
      if (skillType != null)
      {
        if (KingSkillInfo.\u003C\u003Ef__switch\u0024map44 == null)
          KingSkillInfo.\u003C\u003Ef__switch\u0024map44 = new Dictionary<string, int>(2)
          {
            {
              "force_teleport",
              0
            },
            {
              "send_troop_forbidden",
              0
            }
          };
        int num;
        if (KingSkillInfo.\u003C\u003Ef__switch\u0024map44.TryGetValue(skillType, out num) && num == 0)
          return true;
      }
      return false;
    }
  }

  public bool MeetCondition(TileData tileData)
  {
    string skillType = this.SkillType;
    if (skillType != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (KingSkillInfo.\u003C\u003Ef__switch\u0024map45 == null)
      {
        // ISSUE: reference to a compiler-generated field
        KingSkillInfo.\u003C\u003Ef__switch\u0024map45 = new Dictionary<string, int>(2)
        {
          {
            "force_teleport",
            0
          },
          {
            "send_troop_forbidden",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (KingSkillInfo.\u003C\u003Ef__switch\u0024map45.TryGetValue(skillType, out num) && num == 0)
        return tileData.TileType == TileType.City;
    }
    return false;
  }
}
