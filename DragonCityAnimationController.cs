﻿// Decompiled with JetBrains decompiler
// Type: DragonCityAnimationController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

[ExecuteInEditMode]
public class DragonCityAnimationController : MonoBehaviour
{
  private DragonView dragonView;
  public Transform dragonViewRoot;
  public System.Action<Transform> onDragonLoaded;
  private ZScaleAnimation scalar;
  private string _curAnimationName;

  public DragonView DragonView
  {
    get
    {
      return this.dragonView;
    }
  }

  public void Init(Transform dragonViewRoot)
  {
    DragonViewData dragonViewData = DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 0);
    this.dragonView = new DragonView();
    this.dragonView.OnStateChange += new System.Action<SpriteView.State, SpriteView.State>(this.OnDragonViewStateChange);
    this.dragonView.LoadAsync((SpriteViewData) dragonViewData, true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this.dragonView.Mount(dragonViewRoot.transform, Vector3.zero, Quaternion.Euler(Vector3.one), Vector3.one);
      this.dragonView.SetRendererLayer(dragonViewRoot.gameObject.layer);
    }));
    this._curAnimationName = (string) null;
    this.scalar = this.GetComponent<ZScaleAnimation>();
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDragonDataChanged);
    if (PlayerData.inst.dragonData == null)
      return;
    if (PlayerData.inst.dragonData.MarchId == 0L)
      this.dragonView.Show();
    else
      this.dragonView.Hide();
  }

  private void OnDragonDataChanged(long id)
  {
    if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.CityMode || PlayerData.inst.uid != id)
      return;
    this.Refresh();
  }

  public void Refresh()
  {
    if (PlayerData.inst.dragonData == null)
      return;
    this.dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 0), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      this.GetComponent<Animator>().Play("StandBy");
      if (PlayerData.inst.dragonData.MarchId == 0L)
        this.dragonView.Show();
      else
        this.dragonView.Hide();
    }));
  }

  private void OnDragonViewStateChange(SpriteView.State arg1, SpriteView.State arg2)
  {
    if (arg1 != SpriteView.State.Loaded || arg2 != SpriteView.State.Displayed)
      return;
    this.dragonView.SetAnimationStateTrigger("EnterCity");
    this.dragonView.MainRenderer.sortingOrder = 200;
    if (this.onDragonLoaded == null)
      return;
    this.onDragonLoaded(this.dragonViewRoot);
  }

  private void DragonView_OnAnimationEvent(AnimationEvent obj)
  {
    if ((UnityEngine.Object) this.dragonView.animator == (UnityEngine.Object) null || string.IsNullOrEmpty(obj.stringParameter) || string.Equals(this._curAnimationName, obj.stringParameter))
      return;
    this.dragonView.animator.CrossFade(obj.stringParameter, (double) obj.floatParameter > 0.0 ? obj.floatParameter : 0.1f);
    this._curAnimationName = obj.stringParameter;
  }

  private void ConcubinesSelector(AnimationEvent obj)
  {
    this.GetComponent<Animator>().SetInteger("concubines", UnityEngine.Random.Range(1, 101));
  }

  public void Dispose()
  {
    this._curAnimationName = (string) null;
    this.dragonView.OnStateChange -= new System.Action<SpriteView.State, SpriteView.State>(this.OnDragonViewStateChange);
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDragonDataChanged);
    this.dragonView.Dispose();
  }

  private void Update()
  {
    if (!((UnityEngine.Object) null != (UnityEngine.Object) this.scalar))
      return;
    this.dragonView.SetScale(this.scalar.GetScale());
  }
}
