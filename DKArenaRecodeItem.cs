﻿// Decompiled with JetBrains decompiler
// Type: DKArenaRecodeItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DKArenaRecodeItem : MonoBehaviour
{
  public UILabel time;
  public UILabel userName;
  public UILabel isWin;
  private DragonKnightPVPResultPopup.Parameter _data;

  public void OnViewReport()
  {
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightArenaResultPopup", (Popup.PopupParameter) this._data);
  }

  public void SetData(DragonKnightPVPResultPopup.Parameter data)
  {
    this._data = data;
    this.time.text = this.GetFormatTime(data.time);
    this.userName.text = data.target.userName_Alliance_Name;
    string str = ScriptLocalization.Get("dragon_knight_pvp_success", true);
    this.isWin.color = Color.green;
    if (data.win == 0)
    {
      str = ScriptLocalization.Get("dragon_knight_pvp_fail", true);
      this.isWin.color = Color.red;
    }
    else if (data.win == 2)
    {
      str = ScriptLocalization.Get("dragon_knight_pvp_draw", true);
      this.isWin.color = Color.white;
    }
    this.isWin.text = str;
  }

  private string GetFormatTime(int startTime)
  {
    int num1 = NetServerTime.inst.ServerTimestamp - startTime;
    if (num1 < 60)
      return Utils.XLAT("chat_time_less_than_min");
    int num2 = num1 / 3600 / 24;
    int num3 = num1 / 3600 % 24;
    int num4 = num1 % 3600 / 60;
    return ScriptLocalization.GetWithPara("chat_time_display", new Dictionary<string, string>()
    {
      {
        "d",
        num2 <= 0 ? string.Empty : num2.ToString() + Utils.XLAT("chat_time_day")
      },
      {
        "h",
        num3 <= 0 ? string.Empty : num3.ToString() + Utils.XLAT("chat_time_hour")
      },
      {
        "m",
        num4 < 0 ? string.Empty : num4.ToString() + Utils.XLAT("chat_time_min")
      }
    }, true);
  }
}
