﻿// Decompiled with JetBrains decompiler
// Type: InteractiveTimerBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class InteractiveTimerBar : MonoBehaviour
{
  private string _actionName = string.Empty;
  public GameObject mHelpRoot;
  public GameObject mSpeedUpRoot;
  public GameObject mFreeRoot;
  public UISprite mHelpBar;
  public UISprite mSpeedUpBar;
  public UISprite mFreeBar;
  public UISprite mHelpBkg;
  public UISprite mSpeedUpBkg;
  public UISprite mFreeBkg;
  public UILabel mHelpDescription;
  public UILabel mSpeedUpDescription;
  public UILabel mFreeDescription;
  public UILabel mHelpTimeLeftValue;
  public UILabel mSpeedUpTimeLeftValue;
  public UILabel mFreeTimeLeftValue;
  public GameObject mCancelTimerPopup;
  public UILabel mCancelTimerTitle;
  public UILabel mCancelTimerDesc;
  private TimerHUDUIItem _hudItem;
  private JobHandle _jobItem;
  private System.Action _onCancelTimer;
  private System.Action _onRefresh;
  private bool _running;
  private float _updateCounter;

  public void SetDetails(TimerHUDUIItem jobToWatch, string cancelTitle, string cancelDesc, System.Action onJobCancelled, System.Action onRefresh)
  {
    this.HidePopup();
    this._hudItem = jobToWatch;
    this._jobItem = JobManager.Instance.GetJob(this._hudItem.mJobID);
    this.mCancelTimerTitle.text = Utils.XLAT(cancelTitle);
    this.mCancelTimerDesc.text = Utils.XLAT(cancelDesc);
    this._onCancelTimer = onJobCancelled;
    this._onRefresh = onRefresh;
    this._running = true;
    this._actionName = cancelTitle;
    this._updateCounter = 1f;
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
  }

  public void HidePopup()
  {
    this.mCancelTimerPopup.SetActive(false);
  }

  private void OnJobCompleted(object data)
  {
    Hashtable hashtable = data as Hashtable;
    int result = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || (!int.TryParse(hashtable[(object) "event_type"].ToString(), out result) || result != 31))
      return;
    this._running = false;
    this._onRefresh();
  }

  public void SetState(int state)
  {
    switch (state)
    {
      case 0:
        this.mFreeRoot.SetActive(false);
        this.mHelpRoot.SetActive(false);
        this.mSpeedUpRoot.SetActive(false);
        break;
      case 1:
        this.mFreeRoot.SetActive(false);
        this.mHelpRoot.SetActive(false);
        this.mSpeedUpRoot.SetActive(true);
        break;
      case 2:
        this.mFreeRoot.SetActive(false);
        this.mHelpRoot.SetActive(true);
        this.mSpeedUpRoot.SetActive(false);
        break;
      case 3:
        this.mFreeRoot.SetActive(true);
        this.mHelpRoot.SetActive(false);
        this.mSpeedUpRoot.SetActive(false);
        break;
    }
  }

  private void Update()
  {
    if (!this._running || (double) (this._updateCounter += Time.deltaTime) < 0.5)
      return;
    this._updateCounter = 0.0f;
    int num1 = this._jobItem.Duration();
    int num2 = this._jobItem.LeftTime();
    switch (this._hudItem.GetSpeedupState())
    {
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_FREE:
        this.SetState(3);
        this.mFreeBar.fillAmount = (float) (num1 - num2) / (float) num1;
        this.mFreeTimeLeftValue.text = Utils.ConvertSecsToString((double) num2);
        this.mFreeDescription.text = Utils.XLAT(this._actionName);
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_HELP:
        this.SetState(2);
        this.mHelpBar.fillAmount = (float) (num1 - num2) / (float) num1;
        this.mHelpTimeLeftValue.text = Utils.ConvertSecsToString((double) num2);
        this.mHelpDescription.text = Utils.XLAT(this._actionName);
        break;
      case TimerHUDUIItem.SpeedupTypes.SPEEDUP_SPEEDUP:
        this.SetState(1);
        this.mSpeedUpBar.fillAmount = (float) (num1 - num2) / (float) num1;
        this.mSpeedUpTimeLeftValue.text = Utils.ConvertSecsToString((double) num2);
        this.mSpeedUpDescription.text = Utils.XLAT(this._actionName);
        break;
    }
  }

  public void OnTimerActionBtnPressed()
  {
    this._hudItem.PushSpeedupBtn();
  }

  public void OnCancelTimerBtnPressed()
  {
    this.mCancelTimerPopup.SetActive(true);
  }

  public void OnCancelTimerConfirm()
  {
    this._running = false;
    this.mCancelTimerPopup.SetActive(false);
    if (this._onCancelTimer == null)
      return;
    this._onCancelTimer();
  }

  public void OnCancelTimerCancel()
  {
    this.mCancelTimerPopup.SetActive(false);
  }
}
