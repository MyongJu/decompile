﻿// Decompiled with JetBrains decompiler
// Type: CustomDefine
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Funplus.Abstract;
using Funplus.Internal;
using System.Collections.Generic;
using UnityEngine;

public class CustomDefine
{
  public static string GetSeverChannel()
  {
    string str = string.Empty;
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        str = "amazon";
        break;
      case CustomDefine.ChannelEnum.Onestore:
        str = "onestore";
        break;
      case CustomDefine.ChannelEnum.Samsung:
        str = "samsung";
        break;
      case CustomDefine.ChannelEnum.Mycard:
      case CustomDefine.ChannelEnum.ALIPAY:
      case CustomDefine.ChannelEnum.WECHAT:
      case CustomDefine.ChannelEnum.OFFICIAL:
        str = "official";
        break;
      case CustomDefine.ChannelEnum.RUUC:
        str = "ru_uc";
        break;
      case CustomDefine.ChannelEnum.TAPTAP:
        str = "tap_tap";
        break;
      case CustomDefine.ChannelEnum.SQWAN:
        str = "cn37";
        break;
      case CustomDefine.ChannelEnum.IOSCN:
        str = "ios_cn";
        break;
    }
    return str;
  }

  public static string GetChannel()
  {
    if (Application.isEditor)
      return string.Empty;
    string empty = string.Empty;
    if (Application.platform == RuntimePlatform.IPhonePlayer)
      return CustomDefine.Channel == CustomDefine.ChannelEnum.IOSCN ? "ios_cn" : "apple";
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        return "amazon";
      case CustomDefine.ChannelEnum.Onestore:
        return "onestore";
      case CustomDefine.ChannelEnum.Samsung:
        return "samsung";
      case CustomDefine.ChannelEnum.Mycard:
        return "my_card";
      case CustomDefine.ChannelEnum.RUUC:
        return "ru_uc";
      case CustomDefine.ChannelEnum.ALIPAY:
        return "alipay";
      case CustomDefine.ChannelEnum.WECHAT:
        return "wechat";
      case CustomDefine.ChannelEnum.OFFICIAL:
        return "official";
      case CustomDefine.ChannelEnum.TAPTAP:
        return "tap_tap";
      case CustomDefine.ChannelEnum.SQWAN:
        return "cn37";
      default:
        return "google";
    }
  }

  public static string[] GetAdjustInfo()
  {
    List<string> stringList = new List<string>();
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.IOS:
        stringList.Add("fpkxwvp0afwg");
        stringList.Add("e4jktv");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.GooglePlay:
        stringList.Add("mf4dlnm4a8lc");
        stringList.Add("39rblt");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.Amazon:
        stringList.Add("y26rme7vf7r4");
        stringList.Add("9eui7t");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.Onestore:
        stringList.Add("ur29ymtxbu2o");
        stringList.Add("f8d8gy");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.Samsung:
        stringList.Add("ejbexy2cdkw0");
        stringList.Add("xos1lb");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.Mycard:
        stringList.Add("9d27fbgtlfr4");
        stringList.Add("420686");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.RUUC:
        stringList.Add("mf4dlnm4a8lc");
        stringList.Add("auqw4x");
        stringList.Add("8aa3fe");
        break;
      case CustomDefine.ChannelEnum.ALIPAY:
        stringList.Add("9d27fbgtlfr4");
        stringList.Add("420686");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.WECHAT:
        stringList.Add("9d27fbgtlfr4");
        stringList.Add("420686");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.OFFICIAL:
        stringList.Add("9d27fbgtlfr4");
        stringList.Add("420686");
        stringList.Add(string.Empty);
        break;
      case CustomDefine.ChannelEnum.TAPTAP:
        stringList.Add("9d27fbgtlfr4");
        stringList.Add("420686");
        stringList.Add("rmop7m");
        break;
      case CustomDefine.ChannelEnum.IOSCN:
        stringList.Add("lcjiecb9241s");
        stringList.Add("9bx64t");
        stringList.Add(string.Empty);
        break;
    }
    return stringList.ToArray();
  }

  public static string GetVersionFlag()
  {
    string str = string.Empty;
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        str = ".Z";
        break;
      case CustomDefine.ChannelEnum.Onestore:
        str = ".K";
        break;
      case CustomDefine.ChannelEnum.Samsung:
        str = ".S";
        break;
      case CustomDefine.ChannelEnum.Mycard:
        str = ".C";
        break;
      case CustomDefine.ChannelEnum.RUUC:
        str = ".U";
        break;
      case CustomDefine.ChannelEnum.ALIPAY:
        str = ".A";
        break;
      case CustomDefine.ChannelEnum.WECHAT:
        str = ".W";
        break;
      case CustomDefine.ChannelEnum.OFFICIAL:
        str = ".O";
        break;
      case CustomDefine.ChannelEnum.TAPTAP:
        str = ".T";
        break;
      case CustomDefine.ChannelEnum.SQWAN:
        str = ".37";
        break;
      case CustomDefine.ChannelEnum.IOSCN:
        str = ".I";
        break;
    }
    return str;
  }

  public static string GetIAPChannel()
  {
    if (Application.isEditor)
      return string.Empty;
    string empty = string.Empty;
    if (Application.platform == RuntimePlatform.IPhonePlayer)
      return CustomDefine.IsiOSCNDefine() ? "applecniap" : "appleiap";
    switch (CustomDefine.Channel)
    {
      case CustomDefine.ChannelEnum.Amazon:
        return "amazoniap";
      case CustomDefine.ChannelEnum.Onestore:
        return "onestore";
      case CustomDefine.ChannelEnum.Samsung:
        return "samsungiap";
      case CustomDefine.ChannelEnum.Mycard:
        return "mycard";
      case CustomDefine.ChannelEnum.RUUC:
        return "ru_uc";
      case CustomDefine.ChannelEnum.ALIPAY:
        return "alipay";
      case CustomDefine.ChannelEnum.WECHAT:
        return "wechat";
      case CustomDefine.ChannelEnum.OFFICIAL:
        return "official";
      case CustomDefine.ChannelEnum.TAPTAP:
        return "tap_tap";
      case CustomDefine.ChannelEnum.SQWAN:
        return "cn37";
      default:
        return "googleplayiap";
    }
  }

  public static string GetSDKFlag()
  {
    string str = string.Empty;
    if (Application.isEditor)
      return "editor";
    str = ".sandbox";
    if (FunplusSettings.Environment != "sandbox")
      str = ".pro";
    return string.Empty;
  }

  public static bool IsSpecialChannel()
  {
    if (CustomDefine.Channel != CustomDefine.ChannelEnum.None && CustomDefine.Channel != CustomDefine.ChannelEnum.IOS)
      return CustomDefine.Channel != CustomDefine.ChannelEnum.GooglePlay;
    return false;
  }

  public static CustomDefine.ChannelEnum Channel
  {
    get
    {
      if (CustomDefine.IsOfficialDefine())
        return CustomDefine.ChannelEnum.OFFICIAL;
      if (CustomDefine.IsAmazonDefine())
        return CustomDefine.ChannelEnum.Amazon;
      if (CustomDefine.IsSamsungIapDefine())
        return CustomDefine.ChannelEnum.Samsung;
      if (CustomDefine.IsOnestoreDefine())
        return CustomDefine.ChannelEnum.Onestore;
      if (CustomDefine.IsMyCardDefine())
        return CustomDefine.ChannelEnum.Mycard;
      if (CustomDefine.IsRUUCDefine())
        return CustomDefine.ChannelEnum.RUUC;
      if (CustomDefine.IsAlipayDefine())
        return CustomDefine.ChannelEnum.ALIPAY;
      if (CustomDefine.IsWechatDefine())
        return CustomDefine.ChannelEnum.WECHAT;
      if (CustomDefine.IsTapTapDefine())
        return CustomDefine.ChannelEnum.TAPTAP;
      if (CustomDefine.IsSQWanPackage())
        return CustomDefine.ChannelEnum.SQWAN;
      if (CustomDefine.IsiOSCNDefine())
        return CustomDefine.ChannelEnum.IOSCN;
      if (Application.platform == RuntimePlatform.Android)
        return CustomDefine.ChannelEnum.GooglePlay;
      return Application.platform == RuntimePlatform.IPhonePlayer ? CustomDefine.ChannelEnum.IOS : CustomDefine.ChannelEnum.None;
    }
  }

  public static bool IsRUUCDefine()
  {
    return false;
  }

  public static bool IsTapTapDefine()
  {
    return false;
  }

  public static bool IsAmazonDefine()
  {
    return false;
  }

  public static bool IsSamsungIapDefine()
  {
    return false;
  }

  public static bool IsOnestoreDefine()
  {
    return false;
  }

  public static bool IsOfficialDefine()
  {
    return false;
  }

  public static bool IsMyCardDefine()
  {
    return false;
  }

  public static bool IsAlipayDefine()
  {
    return false;
  }

  public static bool IsWechatDefine()
  {
    return false;
  }

  public static bool IsProductionSDK()
  {
    return false;
  }

  public static bool IsSandboxSDK()
  {
    return false;
  }

  public static bool IsAccountUnbindUnLock()
  {
    return false;
  }

  public static bool IsSQWanPackage()
  {
    return false;
  }

  public static bool IsiOSCNDefine()
  {
    bool flag = false;
    if (!flag)
      flag = SettingReader.Instance.HasDefine("ios_cn");
    return flag;
  }

  public static bool IsReYunEnable()
  {
    bool flag = true;
    if (CustomDefine.IsiOSCNDefine())
      flag = false;
    return flag;
  }

  public static BasePaymentWrapper PaymentWrapper
  {
    get
    {
      switch (CustomDefine.Channel)
      {
        case CustomDefine.ChannelEnum.Amazon:
          return (BasePaymentWrapper) AmazonIapAndroid.Instance;
        case CustomDefine.ChannelEnum.Onestore:
          return (BasePaymentWrapper) OneStorePayAndroid.Instance;
        case CustomDefine.ChannelEnum.Samsung:
          return (BasePaymentWrapper) SamsungIapAndroid.Instance;
        case CustomDefine.ChannelEnum.ALIPAY:
          return (BasePaymentWrapper) AlipayAndroid.Instance;
        case CustomDefine.ChannelEnum.WECHAT:
          return (BasePaymentWrapper) WechatPayAndroid.Instance;
        case CustomDefine.ChannelEnum.OFFICIAL:
          return (BasePaymentWrapper) AlipayWechatAndroid.Instance;
        case CustomDefine.ChannelEnum.SQWAN:
          return (BasePaymentWrapper) SQWanIapAndroid.Instance;
        default:
          return (BasePaymentWrapper) null;
      }
    }
  }

  private struct ChannelConst
  {
    public const string IOS_CN = "ios_cn";
  }

  public enum ChannelEnum
  {
    None,
    IOS,
    GooglePlay,
    Amazon,
    Onestore,
    Samsung,
    Mycard,
    RUUC,
    ALIPAY,
    WECHAT,
    OFFICIAL,
    TAPTAP,
    SQWAN,
    IOSCN,
  }
}
