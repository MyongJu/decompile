﻿// Decompiled with JetBrains decompiler
// Type: IapResponse.Product
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace IapResponse
{
  [Serializable]
  public class Product
  {
    public string appid;
    public string id;
    public string name;
    public string type;
    public string kind;
    public int validity;
    public double price;
    public string startDate;
    public string endDate;
    public bool purchasability;
    public Status status;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Product] ");
      stringBuilder.Append("appid: " + this.appid + " ");
      stringBuilder.Append("id: " + this.id + " ");
      stringBuilder.Append("name: " + this.name + " ");
      stringBuilder.Append("type: " + this.type + " ");
      stringBuilder.Append("kind: " + this.kind + " ");
      stringBuilder.Append("validity: " + (object) this.validity + " ");
      stringBuilder.Append("price: " + (object) this.price + " ");
      stringBuilder.Append("startDate: " + this.startDate + " ");
      stringBuilder.Append("endDate: " + this.endDate + " ");
      stringBuilder.Append("purchasability: " + (object) this.purchasability + " ");
      if (this.status != null)
        stringBuilder.Append(this.status.ToString());
      return stringBuilder.ToString();
    }
  }
}
