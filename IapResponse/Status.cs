﻿// Decompiled with JetBrains decompiler
// Type: IapResponse.Status
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace IapResponse
{
  [Serializable]
  public class Status
  {
    public string code;
    public string message;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Status] ");
      stringBuilder.Append("code: " + this.code + " ");
      stringBuilder.Append("message: " + this.message + " ");
      return stringBuilder.ToString();
    }
  }
}
