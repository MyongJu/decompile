﻿// Decompiled with JetBrains decompiler
// Type: IapResponse.Response
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace IapResponse
{
  [Serializable]
  public class Response
  {
    public string api_version;
    public string identifier;
    public string method;
    public Result result;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Response] ");
      stringBuilder.Append("api_version: " + this.api_version + " ");
      stringBuilder.Append("identifier: " + this.identifier + " ");
      stringBuilder.Append("method: " + this.method + " ");
      if (this.result != null)
        stringBuilder.Append(" " + this.result.ToString());
      return stringBuilder.ToString();
    }
  }
}
