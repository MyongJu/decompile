﻿// Decompiled with JetBrains decompiler
// Type: IapResponse.Result
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;

namespace IapResponse
{
  [Serializable]
  public class Result
  {
    public string message;
    public string code;
    public string txid;
    public string receipt;
    public int count;
    public List<Product> product;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Result] ");
      stringBuilder.Append("message: " + this.message + " ");
      stringBuilder.Append("code: " + this.code + " ");
      stringBuilder.Append("txid: " + this.txid + " ");
      stringBuilder.Append("receipt: " + this.receipt + " ");
      stringBuilder.Append("count: " + (object) this.count + " ");
      if (this.product != null)
      {
        using (List<Product>.Enumerator enumerator = this.product.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Product current = enumerator.Current;
            stringBuilder.Append(current.ToString());
          }
        }
      }
      return stringBuilder.ToString();
    }
  }
}
