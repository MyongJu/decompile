﻿// Decompiled with JetBrains decompiler
// Type: FirstAnniversaryItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class FirstAnniversaryItemSlot : MonoBehaviour
{
  [SerializeField]
  private int _currentDay;
  [SerializeField]
  private UILabel _dayText;
  [SerializeField]
  private UILabel _dayActivedText;
  [SerializeField]
  private UILabel _dayDetailText;
  [SerializeField]
  private UISprite _giftSprite;
  [SerializeField]
  private GameObject _dailyCardClaimed;
  [SerializeField]
  private GameObject _dailyCardExpired;
  [SerializeField]
  private GameObject _dailyCardActived;
  public System.Action<SuperLoginMainInfo> OnCardPressed;
  private SuperLoginMainInfo _mainInfo;

  public void UpdateUI()
  {
    List<SuperLoginMainInfo> mainInfoListByGroup = ConfigManager.inst.DB_SuperLoginMain.GetSuperLoginMainInfoListByGroup("super_log_in_1_year_celebration");
    if (mainInfoListByGroup != null)
      this._mainInfo = mainInfoListByGroup.Find((Predicate<SuperLoginMainInfo>) (x => x.day == this._currentDay));
    if (this._mainInfo == null)
      return;
    SuperLoginGroupInfo superLoginGroupInfo = ConfigManager.inst.DB_SuperLoginGroup.Get(this._mainInfo.group);
    if (superLoginGroupInfo != null)
      this._dayDetailText.text = string.Format("{0}.{1}", (object) superLoginGroupInfo.startMonth, (object) (superLoginGroupInfo.startDate + this._currentDay - 1));
    if ((UnityEngine.Object) this._giftSprite != (UnityEngine.Object) null)
      this._giftSprite.spriteName = this._mainInfo.rewardIcon;
    NGUITools.SetActive(this._dayActivedText.gameObject, false);
    NGUITools.SetActive(this._dailyCardClaimed, false);
    NGUITools.SetActive(this._dailyCardExpired, false);
    NGUITools.SetActive(this._dailyCardActived, false);
    switch (this._mainInfo.RewardState + 1)
    {
      case SuperLoginPayload.DailyRewardState.CAN_GET:
        NGUITools.SetActive(this._dailyCardExpired, true);
        break;
      case SuperLoginPayload.DailyRewardState.HAS_GOT:
        NGUITools.SetActive(this._dailyCardActived, true);
        NGUITools.SetActive(this._dayActivedText.gameObject, true);
        break;
      case SuperLoginPayload.DailyRewardState.CANT_GET:
        NGUITools.SetActive(this._dailyCardClaimed, true);
        break;
    }
  }

  public void OnDailyCardPressed()
  {
    if (this._mainInfo != null && this._mainInfo.RewardState == SuperLoginPayload.DailyRewardState.CANT_GET || this.OnCardPressed == null)
      return;
    this.OnCardPressed(this._mainInfo);
  }
}
