﻿// Decompiled with JetBrains decompiler
// Type: CityPlotController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CityPlotController : MonoBehaviour
{
  public static System.Action<int, GameObject, string> onPlotClicked = (System.Action<int, GameObject, string>) null;
  private static List<CityPlotController> _hiddenList = new List<CityPlotController>();
  public CityPlotController.PLOT_TYPE mPlotType = CityPlotController.PLOT_TYPE.PLOT_RURAL;
  public int SlotId;

  private void Awake()
  {
    this.SlotId = Convert.ToInt32(this.name.Replace("Plot", string.Empty));
  }

  public static void Clear()
  {
    CityPlotController._hiddenList.Clear();
  }

  public void Reset()
  {
    this.gameObject.SetActive(true);
  }

  public void OnPlotClicked()
  {
    if (CityPlotController.onPlotClicked != null)
    {
      Transform child = this.transform.FindChild("Hint(Clone)");
      if ((UnityEngine.Object) null != (UnityEngine.Object) child)
      {
        string para = child.GetComponent<HintPrefab>().Para as string;
        CityPlotController.onPlotClicked(this.mPlotType != CityPlotController.PLOT_TYPE.PLOT_RURAL ? 2 : 1, this.gameObject, para);
      }
      else
        CityPlotController.onPlotClicked(this.mPlotType != CityPlotController.PLOT_TYPE.PLOT_RURAL ? 2 : 1, this.gameObject, (string) null);
    }
    LinkerHub.Instance.DisposeHint(this.transform);
  }

  public static Vector3 GetPlotPos(int slotid)
  {
    CityPlotController plot = CityPlotController.GetPlot(slotid);
    if ((bool) ((UnityEngine.Object) plot))
      return plot.transform.position;
    return Vector3.zero;
  }

  public static void SetVisible(int slotid, bool bVisible)
  {
    CityPlotController plot = CityPlotController.GetPlot(slotid);
    if ((UnityEngine.Object) null == (UnityEngine.Object) plot)
      return;
    if (!bVisible)
    {
      CityPlotController._hiddenList.Add(plot);
      plot.gameObject.SetActive(false);
    }
    else
    {
      CityPlotController._hiddenList.Remove(plot);
      plot.gameObject.SetActive(true);
    }
  }

  private static CityPlotController GetPlot(int slotid)
  {
    if ((UnityEngine.Object) null == (UnityEngine.Object) CitadelSystem.inst.MapRoot)
      return (CityPlotController) null;
    foreach (CityPlotController componentsInChild in CitadelSystem.inst.MapRoot.GetComponentsInChildren<CityPlotController>(true))
    {
      if (componentsInChild.SlotId == slotid)
        return componentsInChild;
    }
    return (CityPlotController) null;
  }

  public static CityPlotController GetIdlePlot(int tarZone, string tarBuilding = null)
  {
    if (tarZone != 1 && tarZone != 2)
      return (CityPlotController) null;
    CityPlotController[] componentsInChildren = CitadelSystem.inst.MapRoot.GetComponentsInChildren<CityPlotController>(false);
    if (tarBuilding == null)
    {
      foreach (CityPlotController cityPlotController in componentsInChildren)
      {
        if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
          return cityPlotController;
      }
    }
    CityPlotController.PlotPara plotPara = new CityPlotController.PlotPara();
    plotPara.distance = float.MaxValue;
    if ("stables" == tarBuilding || "range" == tarBuilding || "workshop" == tarBuilding)
    {
      BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("barracks");
      if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      {
        Vector3 position1 = buildingByType.transform.position;
        Vector2 a = new Vector2(position1.x, position1.y);
        foreach (CityPlotController cityPlotController in componentsInChildren)
        {
          if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
          {
            Vector3 position2 = cityPlotController.transform.position;
            Vector2 b = new Vector2(position2.x, position2.y);
            float num = Vector2.Distance(a, b);
            if ((double) num < (double) plotPara.distance)
            {
              plotPara.distance = num;
              plotPara.cpc = cityPlotController;
            }
          }
        }
      }
      else
      {
        foreach (CityPlotController cityPlotController in componentsInChildren)
        {
          if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
            return cityPlotController;
        }
      }
    }
    else if ("farm" == tarBuilding || "lumber_mill" == tarBuilding || ("house" == tarBuilding || "mine" == tarBuilding))
    {
      BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType(tarBuilding);
      if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      {
        Vector3 position1 = buildingByType.transform.position;
        Vector2 a = new Vector2(position1.x, position1.y);
        foreach (CityPlotController cityPlotController in componentsInChildren)
        {
          if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
          {
            Vector3 position2 = cityPlotController.transform.position;
            Vector2 b = new Vector2(position2.x, position2.y);
            float num = Vector2.Distance(a, b);
            if ((double) num < (double) plotPara.distance)
            {
              plotPara.distance = num;
              plotPara.cpc = cityPlotController;
            }
          }
        }
      }
      else
      {
        foreach (CityPlotController cityPlotController in componentsInChildren)
        {
          if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
            return cityPlotController;
        }
      }
    }
    else
    {
      foreach (CityPlotController cityPlotController in componentsInChildren)
      {
        if ((CityPlotController.PLOT_TYPE) tarZone == cityPlotController.mPlotType)
          return cityPlotController;
      }
    }
    return plotPara.cpc;
  }

  public enum PLOT_TYPE
  {
    PLOT_RURAL = 1,
    PLOT_URBAN = 2,
  }

  private class PlotPara
  {
    public float distance;
    public CityPlotController cpc;
  }
}
