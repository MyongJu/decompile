﻿// Decompiled with JetBrains decompiler
// Type: DiceFreeRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;

public class DiceFreeRewardPopup : Popup
{
  public UILabel freeRewardHint;
  public UILabel totalTopupGoldAmount;
  public UILabel collectedItemCount;
  public UILabel remainedItemCount;
  public UIButton topupButton;
  public UIButton topupClaimButton;
  public UILabel dailyRequiredHint;
  public UILabel dailyRewardItemCount;
  public UIButton dailyRewardClaimButton;
  public UIButton dailyRewardGotoButton;
  public UITexture freeRewardTexture;
  public UITexture dailyRewardItemTexture;
  private long _remainedClaim;
  private System.Action _freeClaimedHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DiceFreeRewardPopup.Parameter parameter = orgParam as DiceFreeRewardPopup.Parameter;
    if (parameter != null)
      this._freeClaimedHandler = parameter.freeClaimedHandler;
    this.UpdateUI();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnTopupBtnPressed()
  {
    this.OnCloseBtnPressed();
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnTopupClaimBtnPressed()
  {
    this.CollectReward(0);
  }

  public void OnDailyGotoBtnPressed()
  {
    RequestManager.inst.SendRequest("Quest:refreshDailyQuest", (Hashtable) null, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      this.OnCloseBtnPressed();
      UIManager.inst.OpenPopup("DailyActivy/DailyActiviesPopup", (Popup.PopupParameter) null);
    }), true);
  }

  public void OnDailyClaimBtnPressed()
  {
    this.CollectReward(1);
  }

  private void CollectReward(int type)
  {
    RequestManager.inst.SendRequest("casino:crapsClaim", new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) nameof (type),
        (object) type
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      DicePayload.Instance.TempDiceData.Decode(data as Hashtable);
      this.UpdateUI();
      DicePayload.Instance.ShowCollectEffect(DicePayload.Instance.TempDiceData.PropsId, (int) DicePayload.Instance.TempDiceData.RemainedGoldClaim);
      if (this._freeClaimedHandler == null)
        return;
      this._freeClaimedHandler();
    }), true);
  }

  private void UpdateUI()
  {
    if (DicePayload.Instance.TempDiceData == null)
      return;
    Dictionary<string, string> para = new Dictionary<string, string>();
    long getTargetItemGold = (long) DicePayload.Instance.TempDiceData.GetTargetItemGold;
    long targetItemGoldNumber = (long) DicePayload.Instance.TempDiceData.GetTargetItemGoldNumber;
    para.Add("0", getTargetItemGold.ToString());
    para.Add("1", targetItemGoldNumber.ToString());
    this.freeRewardHint.text = ScriptLocalization.GetWithPara("tavern_dice_gold_topup_tip", para, true);
    para.Clear();
    para.Add("0", Utils.FormatThousands(DicePayload.Instance.TempDiceData.CurrentGold.ToString()));
    this.totalTopupGoldAmount.text = ScriptLocalization.GetWithPara("event_lucky_draw_gold_topup_num", para, true);
    para.Clear();
    para.Add("0", DicePayload.Instance.TempDiceData.GoldClaimNumber.ToString());
    this.collectedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_taken", para, true);
    this._remainedClaim = DicePayload.Instance.TempDiceData.RemainedGoldClaim;
    para.Clear();
    para.Add("0", (this._remainedClaim <= 0L ? 0L : this._remainedClaim).ToString());
    this.remainedItemCount.text = ScriptLocalization.GetWithPara("event_lucky_draw_draws_remaining", para, true);
    if (this._remainedClaim > 0L)
    {
      NGUITools.SetActive(this.topupButton.gameObject, false);
      NGUITools.SetActive(this.topupClaimButton.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.topupButton.gameObject, true);
      NGUITools.SetActive(this.topupClaimButton.gameObject, false);
    }
    string str = DicePayload.Instance.TempDiceData.CurrentDailyScore.ToString() + "/" + DicePayload.Instance.TempDiceData.GetItemDailyScore.ToString();
    para.Clear();
    para.Add("0", str);
    this.dailyRequiredHint.text = ScriptLocalization.GetWithPara("event_lucky_draw_daily_reward_tip", para, true);
    this.dailyRewardItemCount.text = DicePayload.Instance.TempDiceData.GetItemDailyNumber.ToString();
    if (DicePayload.Instance.TempDiceData.CurrentDailyScore < DicePayload.Instance.TempDiceData.GetItemDailyScore)
    {
      NGUITools.SetActive(this.dailyRewardClaimButton.gameObject, false);
      NGUITools.SetActive(this.dailyRewardGotoButton.gameObject, true);
    }
    else
    {
      NGUITools.SetActive(this.dailyRewardClaimButton.gameObject, true);
      NGUITools.SetActive(this.dailyRewardGotoButton.gameObject, false);
      if (DicePayload.Instance.TempDiceData.DailyIsClaim)
        this.dailyRewardClaimButton.isEnabled = false;
    }
    MarksmanPayload.Instance.FeedMarksmanTexture(this.freeRewardTexture, DicePayload.Instance.TempDiceData.PropsImagePath);
    MarksmanPayload.Instance.FeedMarksmanTexture(this.dailyRewardItemTexture, DicePayload.Instance.TempDiceData.PropsImagePath);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action freeClaimedHandler;
  }
}
