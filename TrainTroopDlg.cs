﻿// Decompiled with JetBrains decompiler
// Type: TrainTroopDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TrainTroopDlg : UI.Dialog
{
  private int MAX_TRAIN_NUM_1 = int.MaxValue;
  private int TRAIN_LIMIT = int.MaxValue;
  private List<UnitRender> unitRenders = new List<UnitRender>();
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  private Hashtable itemHT = new Hashtable();
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private string _lastUnitImage = string.Empty;
  private bool autoFix = true;
  private const string UNIT_RENDER_PATH = "Prefab/UI/Dialogs/Barracks/Unit_Render";
  public UITexture troopClassIcon;
  public UILabel titleLabel;
  public Puppet2DRender puppet2DRender;
  public BarracksResourcesComponent topResources;
  public BarracksResourcesComponent costResources;
  public UIScrollView bottomScrollView;
  public UICenterOnChild uiCenterOnChild;
  public UILabel trainNumLabel;
  public UIInput trainNumInput;
  public UISlider trainSlider;
  public UILabel trainTimeLabel;
  public UILabel goldCostLable;
  public UIBuildingButton trainBtn;
  public UIBuildingButton instantBtn;
  public UIBuildingButton upgradeTroopsBtn;
  public UILabel unitNameLabel;
  public UILabel unitDescriptionLabel;
  public UILabel ownedNumberLabel;
  public UILabel upgradeTroopsBtnLabel;
  public GameObject rightBottomUnlockedGameObject;
  public GameObject rightBottomLockedGameObject;
  public GameObject rightBottomBusyGameOject;
  public UILabel lockedDescriptionLabel;
  public UILabel gotoUpgradeLabel;
  public TimerHUDUIItem timerHUDUIItem;
  public UILabel busyDescriptionLabel;
  public UITexture unitImageTexture;
  public GameObject leftImage;
  public UIInputOnGUI trainCountInput;
  public UIButton moreInfoBtn;
  public GameObject grid;
  public GameObject barrackOwn;
  public GameObject trapOwn;
  public UILabel trapLabelOwn;
  public UILabel trapLabelMax;
  public UISprite unitTrainningSprite;
  public UISprite trapTrainningSprite;
  public UILabel trianLabel;
  public UILabel instanceTrianLabel;
  public UIButton gotoBtn;
  public UIButton closeBtn;
  public GameObject renderTemplate;
  private long buildingID;
  private string buildingType;
  private string currentUnitID;
  private Unit_StatisticsInfo TSC;
  private int _troopCount;
  private BuildingInfo buildingInfo;
  private CancelTrainPopup messageBox;
  private BarracksMoreInfoPopup barracksMoreInfoPopup;
  private List<Unit_StatisticsInfo> units;
  private UnitRender currentRender;
  private UISprite trainningSprite;
  private TrainTroopDlg.Parameter data;
  private bool isScrollMoving;
  public UILabel goldLabel;
  public Color normalColor;
  private int gold;
  private int instTrainCost;
  private bool instantTrain;
  private Unit_StatisticsInfo unitToBeUpgraded;

  private void Update()
  {
    if (!this.bottomScrollView.isDragging)
      return;
    this.Focus();
  }

  private void OnScrollMoving()
  {
    this.Focus();
  }

  private void Focus()
  {
    using (List<UnitRender>.Enumerator enumerator = this.unitRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnitRender current = enumerator.Current;
        Transform cachedTransform = this.bottomScrollView.panel.cachedTransform;
        Vector3[] worldCorners = this.bottomScrollView.panel.worldCorners;
        Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
        if ((double) Mathf.Abs((cachedTransform.InverseTransformPoint(current.transform.position) - cachedTransform.InverseTransformPoint(position)).x) < 150.0 && this.TSC.ID != current.Unit.ID)
        {
          this.TSC = current.Unit;
          this.onDragStarted();
          this.currentRender = current;
          this.UpdateUI();
        }
      }
    }
  }

  public override void OnOpen(UIControler.UIParameter param)
  {
    base.OnOpen(param);
    this.data = param as TrainTroopDlg.Parameter;
    for (int index = 0; index < this.unitRenders.Count; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.unitRenders[index].gameObject);
    this.unitRenders.Clear();
  }

  public override void OnShow(UIControler.UIParameter param)
  {
    base.OnShow(param);
    this.instantTrain = false;
    if (!string.IsNullOrEmpty(this.data.UnitID))
    {
      Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(this.data.UnitID);
      if (data != null)
      {
        this.currentUnitID = this.data.UnitID;
        this.units = BarracksManager.Instance.GetAllUnitsCanTrain(data.FromBuildingType);
        int buildingLevelFor = CityManager.inst.GetHighestBuildingLevelFor(data.FromBuildingType);
        this.buildingInfo = ConfigManager.inst.DB_Building.GetData(data.FromBuildingType, buildingLevelFor);
        this.buildingID = CitadelSystem.inst.GetBuildControllerFromBuildingInfo(this.buildingInfo).mBuildingID;
        this.buildingType = data.FromBuildingType;
      }
    }
    else
    {
      this.buildingID = this.data.BuildingID;
      this.buildingType = this.data.BuildingType;
      this.currentUnitID = BarracksManager.Instance.HighestUnitIDCanTrainOfBuildingType(this.buildingType);
      this.units = BarracksManager.Instance.GetAllUnitsCanTrain(this.buildingType);
      this.buildingInfo = ConfigManager.inst.DB_Building.GetData(DBManager.inst.DB_CityMap.Get(new CityMapKey()
      {
        uid = PlayerData.inst.uid,
        cityId = (long) PlayerData.inst.cityId,
        buildingId = this.buildingID
      }).typeId);
    }
    this.AddEvtListeners();
    this.TSC = ConfigManager.inst.DB_Unit_Statistics.GetData(this.currentUnitID);
    this.InitMaxTrainNumAndTroopCount();
    this.InitUI();
    this.TryLoadBenefitDetailAndUpdateUI();
  }

  private void TryLoadBenefitDetailAndUpdateUI()
  {
    KingSkillInfo byType = ConfigManager.inst.DB_KingSkill.GetByType("after_war_training");
    if (byType == null)
      return;
    bool flag = false;
    if (KingdomBuffPayload.Instance.KingdomBuffDataList != null)
    {
      using (List<KingdomBuffPayload.KingdomBuffData>.Enumerator enumerator = KingdomBuffPayload.Instance.KingdomBuffDataList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KingdomBuffPayload.KingdomBuffData current = enumerator.Current;
          if (current != null && current.buffId == byType.KingdomBuffId)
          {
            flag = true;
            break;
          }
        }
      }
    }
    if (!flag)
      return;
    DBManager.inst.DB_UserBenefitParam.TryUpdateFromServer((System.Action<bool, object>) ((result, data) => this.UpdateUI()));
  }

  public override void OnHide(UIControler.UIParameter param)
  {
    this.CancelInvoke("LateCenterOn");
    this.RemoveEvtListeners();
    this._lastUnitImage = string.Empty;
    base.OnHide(param);
  }

  public void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnGotoUpgradeBtnClick()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnDisplayStateChangeBtnClick()
  {
    BarracksManager.Instance.ChangeDisplayState();
    this.UpdateDisplayState();
  }

  public void OnScrollViewClick()
  {
    if ((double) UICamera.currentTouch.pos.x < 100.0)
      this.OnScrollViewRightClick();
    else
      this.OnScrollViewLeftClick();
  }

  public void OnSliderSubtractBtnClick()
  {
    this._troopCount = Mathf.Max(0, this._troopCount - 1);
    this.trainSlider.value = (float) this._troopCount / (float) this.TRAIN_LIMIT;
    this.UpdateRightBottom();
  }

  public void OnSliderSubtractBtnDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.TRAIN_LIMIT;
    this.proxy.Slider = (UIProgressBar) this.trainSlider;
    this.proxy.Play(-1, -1f);
  }

  public void OnSliderSubtractBtnUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnTroopCountInputChange()
  {
    int num = 0;
    try
    {
      num = Convert.ToInt32(this.trainNumInput.value);
    }
    catch (FormatException ex)
    {
      num = 0;
    }
    catch (OverflowException ex)
    {
      num = this.MAX_TRAIN_NUM_1;
    }
    finally
    {
      this._troopCount = Mathf.Clamp(num, 0, this.TRAIN_LIMIT);
      this.trainNumInput.value = this._troopCount.ToString();
      if (this.autoFix)
        this.trainSlider.value = (float) this._troopCount / (float) this.TRAIN_LIMIT;
      this.UpdateRightBottom();
    }
  }

  public void OnSliderPlusBtnClick()
  {
    this._troopCount = Mathf.Min(this.TRAIN_LIMIT, this._troopCount + 1);
    this.trainSlider.value = (float) this._troopCount / (float) this.TRAIN_LIMIT;
    this.UpdateRightBottom();
  }

  public void OnSliderPlusBtnDown()
  {
    this.autoFix = false;
    this.proxy.MaxSize = this.TRAIN_LIMIT;
    this.proxy.Slider = (UIProgressBar) this.trainSlider;
    this.proxy.Play(1, -1f);
  }

  public void OnSliderPlusBtnUp()
  {
    this.autoFix = true;
    this.proxy.Stop();
  }

  public void OnTimerSpeedUpBtnPressed()
  {
    this.ConFirmCallBack();
  }

  private void ConFirmCallBack()
  {
    long mTrainingJobId = CityManager.inst.GetBuildingFromID(this.buildingID).mTrainingJobID;
    JobHandle unfinishedJob = JobManager.Instance.GetUnfinishedJob(mTrainingJobId);
    if (unfinishedJob == null)
      return;
    int cost = ItemBag.CalculateCost(new Hashtable()
    {
      {
        (object) "time",
        (object) unfinishedJob.LeftTime()
      }
    });
    if (PlayerData.inst.hostPlayer.Currency < cost)
      UIManager.inst.toast.Show(ScriptLocalization.Get("toast_gold_not_enough_train", true), (System.Action) null, 4f, false);
    else
      RequestManager.inst.SendRequest("City:speedUpByGold", new Hashtable()
      {
        {
          (object) "job_id",
          (object) mTrainingJobId
        },
        {
          (object) "gold",
          (object) cost
        }
      }, (System.Action<bool, object>) ((arg1, arg2) =>
      {
        if (!arg1)
          return;
        BarracksManager.Instance.CollectBuilding(this.buildingID, (System.Action) (() => this.UpdateUI()));
      }), true);
  }

  public void OnCancelTimerBtnPressed()
  {
    this.messageBox = PopupManager.Instance.Open<CancelTrainPopup>("CancelTrainConfirmPopup");
    this.messageBox.Initialize(!(this.buildingInfo.Type == "fortress") ? "barracks_uppercase_cancel_training" : "barracks_uppercase_cancel_building", !(this.buildingInfo.Type == "fortress") ? "barracks_cancel_training_description" : "barracks_cancel_building_description", !(this.buildingInfo.Type == "fortress") ? "barracks_cancel_training_warning" : "barracks_cancel_building_warning", "id_uppercase_yes", "id_uppercase_no", new System.Action(this.OnCancelYesPressed), new System.Action(this.OnCancelNoPressed), this.buildingID);
  }

  public void OnInstantTrainBtnPressed()
  {
    this.instantTrain = true;
    if (PlayerData.inst.hostPlayer.Currency < this.instTrainCost)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      AudioManager.Instance.PlaySound("sfx_troop_training_start", false);
      this.instantBtn.isEnabled = false;
      MessageHub.inst.GetPortByAction("City:trainTroop").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) this.buildingID, (object) "class", (object) this.TSC.ID, (object) "count", (object) this._troopCount, (object) "instant", (object) true), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (!bSuccess)
          return;
        BarracksManager.Instance.CollectBuilding(this.buildingID, (System.Action) (() => this.UpdateUI()));
      }), true);
    }
  }

  public void OnUpgradeTroopsBtnPressed()
  {
    if (DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(this.TSC.ID) == 0)
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_troops_level_up_nothing_in_castle"), (System.Action) null, 4f, false);
    }
    else
    {
      TroopsLevelUpDialog.Parameter parameter = new TroopsLevelUpDialog.Parameter();
      parameter.BuildingId = this.buildingID;
      parameter.buildingInfo = this.buildingInfo;
      parameter.UnitInfo = this.TSC;
      this.unitToBeUpgraded = this.TSC;
      UIManager.inst.OpenDlg("Barracks/TrainTroopConvertDlg", (UI.Dialog.DialogParameter) parameter, true, true, true);
    }
  }

  public void OnTrainBtnPressed()
  {
    this.instantTrain = false;
    AudioManager.Instance.PlaySound("sfx_troop_training_start", false);
    if (this.m_lackRes.Count > 0 || this.m_lackitem.Count > 0)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        requireRes = this.itemHT,
        action = (!(this.buildingType == "fortress") ? ScriptLocalization.Get("train_buy_confirm", true) : ScriptLocalization.Get("id_uppercase_build", true)),
        gold = this.gold,
        buyResourCallBack = new System.Action<int>(this.SendTrainRequest),
        title = ScriptLocalization.Get("train_notenoughresource_title", true),
        content = (!(this.buildingType == "fortress") ? ScriptLocalization.Get("train_notenoughresource_description", true) : ScriptLocalization.Get("traps_notenoughresource_description", true))
      });
    else
      this.SendTrainRequest(0);
  }

  private void SendTrainRequest(int gold = 0)
  {
    if (PlayerData.inst.hostPlayer.Currency < gold)
      Utils.ShowNotEnoughGoldTip();
    else
      MessageHub.inst.GetPortByAction("City:trainTroop").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) this.buildingID, (object) "class", (object) this.TSC.ID, (object) "count", (object) this._troopCount, (object) "instant", (object) false, (object) nameof (gold), (object) gold), (System.Action<bool, object>) ((bSuccess, data) =>
      {
        if (!bSuccess)
          return;
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      }), true);
  }

  public bool ShouldShowUpgradeTroopsBtn()
  {
    bool flag1 = false;
    if (BarracksManager.Instance.IsUpgradeTroopOpen)
    {
      if (this.buildingType != "fortress")
      {
        Unit_StatisticsInfo levelUnitCanTrain1 = BarracksManager.Instance.GetMaxLevelUnitCanTrain(this.buildingType);
        Unit_StatisticsInfo levelUnitCanTrain2 = BarracksManager.Instance.GetNextLevelUnitCanTrain(this.buildingType, this.TSC.internalId);
        int troopsCount = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.GetTroopsCount(this.TSC.ID);
        bool flag2 = false;
        if (levelUnitCanTrain1 != null && levelUnitCanTrain1.internalId == this.TSC.internalId)
          flag2 = true;
        bool flag3 = false;
        if (levelUnitCanTrain2 != null)
          flag3 = BarracksManager.Instance.IsUnitUnlocked(levelUnitCanTrain2.internalId);
        bool flag4 = false;
        if (BarracksManager.Instance.IsUnitTraining(this.buildingID) && BarracksManager.Instance.GetCurrentTrainningUnit(this.buildingID) != null)
          flag4 = true;
        bool flag5 = false;
        if (BarracksManager.Instance.IsBuildingUpgrading(this.buildingID))
          flag5 = true;
        flag1 = !flag2 && flag3 && (!flag4 && !flag5) && troopsCount != 0;
      }
    }
    else
      flag1 = false;
    return flag1;
  }

  private void AddEvtListeners()
  {
    GameEngine.Instance.events.OnResourceCapacityChanged += new System.Action<string, int>(this.OnResourceChanged);
    this.bottomScrollView.onDragStarted += new UIScrollView.OnDragNotification(this.onDragStarted);
    UISlider trainSlider = this.trainSlider;
    trainSlider.onDragFinished = trainSlider.onDragFinished + new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    this.uiCenterOnChild.onFinished += new SpringPanel.OnFinished(this.Refresh);
    this.bottomScrollView.onMomentumMove += new UIScrollView.OnDragNotification(this.OnScrollMoving);
    DBManager.inst.DB_City.onDataChanged += new System.Action<long>(this.RefreshTroop);
    DBManager.inst.DB_City.onDataRemoved += new System.Action(this.RefreshTroop);
    MessageHub.inst.GetPortByAction("job_complete").AddEvent(new System.Action<object>(this.OnJobCompleted));
  }

  private void RemoveEvtListeners()
  {
    GameEngine.Instance.events.OnResourceCapacityChanged -= new System.Action<string, int>(this.OnResourceChanged);
    this.bottomScrollView.onDragStarted -= new UIScrollView.OnDragNotification(this.onDragStarted);
    UISlider trainSlider = this.trainSlider;
    trainSlider.onDragFinished = trainSlider.onDragFinished - new UIProgressBar.OnDragFinished(this.OnSliderDragFinish);
    this.uiCenterOnChild.onFinished -= new SpringPanel.OnFinished(this.Refresh);
    this.bottomScrollView.onMomentumMove -= new UIScrollView.OnDragNotification(this.OnScrollMoving);
    DBManager.inst.DB_City.onDataChanged -= new System.Action<long>(this.RefreshTroop);
    DBManager.inst.DB_City.onDataRemoved -= new System.Action(this.RefreshTroop);
    BuilderFactory.Instance.Release((UIWidget) this.troopClassIcon);
    MessageHub.inst.GetPortByAction("job_complete").RemoveEvent(new System.Action<object>(this.OnJobCompleted));
  }

  private void InitUI()
  {
    this.rightBottomUnlockedGameObject.SetActive(true);
    this.rightBottomBusyGameOject.SetActive(false);
    this.rightBottomLockedGameObject.SetActive(false);
    this.moreInfoBtn.isEnabled = true;
    this.trainNumLabel.text = string.Empty;
    this.costResources.UpdateCostResources(0.0f, 0.0f, 0.0f, 0.0f, true);
    this.goldCostLable.text = string.Empty;
    this.ownedNumberLabel.text = string.Empty;
    this.trapOwn.SetActive(false);
    this.barrackOwn.SetActive(false);
    this.instantBtn.isEnabled = false;
    this.trainBtn.isEnabled = false;
    if (this.buildingInfo.Type == "fortress")
    {
      this.trianLabel.text = Utils.XLAT("id_uppercase_build");
      this.instanceTrianLabel.text = Utils.XLAT("id_uppercase_instant_build");
    }
    else
    {
      this.trianLabel.text = Utils.XLAT("id_uppercase_train");
      this.instanceTrianLabel.text = Utils.XLAT("id_uppercase_instant_train");
    }
    long mTrainingJobId = CityManager.inst.GetBuildingFromID(this.buildingID).mTrainingJobID;
    if (mTrainingJobId != 0L && JobManager.Instance.GetJob(mTrainingJobId).GetJobEvent() == JobEvent.JOB_UPGRADE_TROOPS)
    {
      this.TSC = BarracksManager.Instance.GetCurrentTrainningUnit(this.buildingID);
      this.currentUnitID = this.TSC.ID;
    }
    if (this.unitToBeUpgraded != null)
    {
      this.TSC = this.unitToBeUpgraded;
      this.currentUnitID = this.TSC.ID;
    }
    this.UpdateUnitRender();
    this.UpdateDisplayState();
    this.PositionScrollView();
  }

  private void UpdateUI()
  {
    if ((UnityEngine.Object) this.messageBox != (UnityEngine.Object) null && this.messageBox.gameObject.activeSelf)
      this.messageBox.Close();
    this.InitMaxTrainNumAndTroopCount();
    this.UpdateRightBottom();
    this.UpdateMoreInfo();
    this.UpdateTopResource();
    this.UpdateOwnedLabel();
    this.UpdateUpgradeTroopsBtn();
    this.UpdateImageTexture();
    this.UpdateDisplayState();
    if ((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null)
      this.currentRender.HighLight();
    if (!((UnityEngine.Object) this.titleLabel != (UnityEngine.Object) null))
      return;
    this.titleLabel.text = Utils.XLAT(this.buildingInfo.Building_LOC_ID);
  }

  private void UpdateImageTexture()
  {
    if (this.buildingType == "range" || this.buildingType == "stables" || this.buildingType == "barracks")
    {
      if (this._lastUnitImage != this.TSC.Troop_IMAGE)
      {
        Utils.SetPortrait(this.unitImageTexture, this.TSC.Troop_IMAGE);
        this._lastUnitImage = this.TSC.Troop_IMAGE;
      }
      if (!BarracksManager.Instance.IsUnitUnlocked(this.TSC.internalId))
        GreyUtility.Grey(this.unitImageTexture.gameObject);
      else
        GreyUtility.Normal(this.unitImageTexture.gameObject);
    }
    else
    {
      this.puppet2DRender.Release();
      this.puppet2DRender.Build(this.TSC.Troop_IMAGE, this.TSC.Puppet2DAnimPath, !BarracksManager.Instance.IsUnitUnlocked(this.TSC.internalId));
    }
  }

  private void PositionScrollView()
  {
    this.Invoke("LateCenterOn", 0.05f);
  }

  private void LateCenterOn()
  {
    if (this.unitRenders == null || (UnityEngine.Object) this == (UnityEngine.Object) null)
      return;
    using (List<UnitRender>.Enumerator enumerator = this.unitRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnitRender current = enumerator.Current;
        if (current.Unit.ID == this.currentUnitID)
        {
          this.uiCenterOnChild.CenterOn(current.transform);
          this.currentRender = current;
          this.UpdateUI();
          break;
        }
      }
    }
  }

  private void OnScrollViewLeftClick()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    int index = Mathf.Min(this.units.Count - 1, this.unitRenders.IndexOf(this.uiCenterOnChild.centeredObject.GetComponent<UnitRender>()) + 1);
    this.onDragStarted();
    this.currentUnitID = this.unitRenders[index].Unit.ID;
    this.currentRender = this.unitRenders[index];
    this.UpdateUI();
    this.uiCenterOnChild.CenterOn(this.unitRenders[index].transform);
  }

  private void OnScrollViewRightClick()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    int index = Mathf.Max(0, this.unitRenders.IndexOf(this.uiCenterOnChild.centeredObject.GetComponent<UnitRender>()) - 1);
    this.onDragStarted();
    this.currentUnitID = this.unitRenders[index].Unit.ID;
    this.currentRender = this.unitRenders[index];
    this.UpdateUI();
    this.uiCenterOnChild.CenterOn(this.unitRenders[index].transform);
  }

  private void onDragStarted()
  {
    this.moreInfoBtn.isEnabled = false;
    using (List<UnitRender>.Enumerator enumerator = this.unitRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnitRender current = enumerator.Current;
        if (current.gameObject.activeSelf)
          current.UnHighLight();
      }
    }
  }

  private void Refresh()
  {
    if ((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null && this.TSC.ID != this.currentUnitID)
    {
      UnitRender component = this.uiCenterOnChild.centeredObject.GetComponent<UnitRender>();
      this.TSC = component.Unit;
      this.currentUnitID = this.TSC.ID;
      this.moreInfoBtn.isEnabled = true;
      this.bottomScrollView.enabled = true;
      this.currentRender = component;
    }
    this.isScrollMoving = false;
    this.bottomScrollView.enabled = true;
    if (!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null))
      return;
    this.currentRender.HighLight();
  }

  private void RefreshTroop(long cityID)
  {
    this.UpdateOwnedLabel();
    this.UpdateRightBottom();
    this.UpdateUpgradeTroopsBtn();
  }

  private void RefreshTroop()
  {
    this.UpdateOwnedLabel();
    this.UpdateRightBottom();
    this.UpdateUpgradeTroopsBtn();
  }

  private void UpdateOwnedLabel()
  {
    if (this.buildingType == "fortress")
    {
      this.trapOwn.SetActive(true);
      this.barrackOwn.SetActive(false);
      this.trapLabelOwn.text = Utils.XLAT("barracks_own") + ":" + Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).cityTroops.GetTroopsCount(this.TSC.ID).ToString());
      this.trapLabelMax.text = Utils.XLAT("id_total") + ":" + Utils.FormatThousands(BarracksManager.Instance.GetTotalTrapNum().ToString()) + "/" + Utils.FormatThousands(this.TSC.GetTapBuildCapacity().ToString());
    }
    else
    {
      this.trapOwn.SetActive(false);
      this.barrackOwn.SetActive(true);
      this.ownedNumberLabel.text = Utils.XLAT("barracks_own") + ":" + Utils.FormatThousands(DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId).totalTroops.GetTroopsCount(this.TSC.ID).ToString());
    }
  }

  private void UpdateUpgradeTroopsBtn()
  {
    if (this.ShouldShowUpgradeTroopsBtn())
    {
      this.upgradeTroopsBtn.gameObject.SetActive(true);
      this.upgradeTroopsBtnLabel.text = Utils.XLAT("barracks_upgrade_troop_level");
    }
    else
      this.upgradeTroopsBtn.gameObject.SetActive(false);
  }

  private void InitMaxTrainNumAndTroopCount()
  {
    this.UpdateMaxTrainNum();
    if (!this.instantTrain)
      this._troopCount = this.MAX_TRAIN_NUM_1;
    if (this.buildingType == "fortress")
      this._troopCount = Mathf.Min(this._troopCount, this.TRAIN_LIMIT);
    this.trainSlider.value = (float) this._troopCount / (float) this.TRAIN_LIMIT;
  }

  private void UpdateTopResource()
  {
    this.topResources.UpdateOwnedResources();
  }

  private void UpdateMoreInfo()
  {
    if (!((UnityEngine.Object) this.barracksMoreInfoPopup != (UnityEngine.Object) null) || !this.barracksMoreInfoPopup.gameObject.activeSelf)
      return;
    this.barracksMoreInfoPopup.Close((UIControler.UIParameter) null);
  }

  private void UpdateDisplayState()
  {
    BarracksDisplayState currentDisplayState = BarracksManager.Instance.CurrentDisplayState;
    this.unitNameLabel.text = ScriptLocalization.Get(this.TSC.ID + "_name", true).ToUpper();
    this.unitDescriptionLabel.text = ScriptLocalization.Get(this.TSC.Troop_Class_DES_LOC, true);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.troopClassIcon, this.TSC.Troop_Class_ICON_PATH, (System.Action<bool>) null, true, false, string.Empty);
  }

  private void UpdateUnitRender()
  {
    using (List<UnitRender>.Enumerator enumerator = this.unitRenders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Hide();
    }
    this.ClearUnitRender();
    for (int index = 0; index < this.units.Count; ++index)
    {
      if (index < this.unitRenders.Count)
      {
        this.unitRenders[index].Show();
        this.unitRenders[index].UpdateUI(this.units[index], this.buildingID, new System.Action<UnitRender>(this.OnUnitRenderClick), this.bottomScrollView);
      }
      else
      {
        GameObject gameObject = NGUITools.AddChild(this.grid, this.renderTemplate);
        gameObject.SetActive(true);
        UnitRender component = gameObject.GetComponent<UnitRender>();
        component.UpdateUI(this.units[index], this.buildingID, new System.Action<UnitRender>(this.OnUnitRenderClick), this.bottomScrollView);
        this.unitRenders.Add(component);
      }
      if (this.units[index].ID == this.TSC.ID)
        this.unitRenders[index].HighLight();
    }
    this.grid.GetComponent<UIGrid>().Reposition();
    Utils.ExecuteAtTheEndOfFrame((System.Action) (() => this.bottomScrollView.ResetPosition()));
  }

  private void ClearUnitRender()
  {
    if (this.unitRenders == null)
      return;
    for (int index = 0; index < this.unitRenders.Count; ++index)
    {
      NGUITools.SetActive(this.unitRenders[index].gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.unitRenders[index].gameObject);
    }
    this.unitRenders.Clear();
  }

  private void OnUnitRenderClick(UnitRender render)
  {
    if (this.isScrollMoving)
      return;
    this.onDragStarted();
    this.TSC = render.Unit;
    this.currentRender = render;
    this.UpdateUI();
    this.uiCenterOnChild.CenterOn(render.transform);
    this.isScrollMoving = true;
    this.bottomScrollView.enabled = false;
  }

  private void OnMoreInfoBtnClick()
  {
    BarracksMoreInfoPopup.Parameter parameter = new BarracksMoreInfoPopup.Parameter();
    parameter.TSC = this.TSC;
    parameter.buildingID = this.buildingID;
    parameter.onClose = new System.Action(this.OnBarracksMoreInfoPopuoClose);
    if (this.buildingInfo.Type == "fortress")
      UIManager.inst.OpenPopup("TrapMoreInfoPopup", (Popup.PopupParameter) parameter);
    else
      UIManager.inst.OpenPopup("BarracksMoreInfoPopup", (Popup.PopupParameter) parameter);
  }

  private void OnBarracksMoreInfoPopuoClose()
  {
    this.trainCountInput.enabled = true;
    this.Refresh();
  }

  private void OnResourceChanged(string resourceType, int delta)
  {
    this.topResources.UpdateOwnedResources();
    this.UpdateRightBottom();
  }

  private void UpdateMaxTrainNum()
  {
    CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.MAX_TRAIN_NUM_1 = int.MaxValue;
    long currentResource1 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    long currentResource2 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    long currentResource3 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    long currentResource4 = (long) cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    float f1 = (double) this.TSC.BenefitCostFood(1) <= 0.0 ? 0.0f : (float) currentResource3 / this.TSC.BenefitCostFood(1);
    float f2 = (double) this.TSC.BenefitCostSilver(1) <= 0.0 ? 0.0f : (float) currentResource2 / this.TSC.BenefitCostSilver(1);
    float f3 = (double) this.TSC.BenefitCostWood(1) <= 0.0 ? 0.0f : (float) currentResource1 / this.TSC.BenefitCostWood(1);
    float f4 = (double) this.TSC.BenefitCostOre(1) <= 0.0 ? 0.0f : (float) currentResource4 / this.TSC.BenefitCostOre(1);
    if ((double) this.TSC.BenefitCostFood(1) > 0.0 && (double) f1 < (double) this.MAX_TRAIN_NUM_1)
      this.MAX_TRAIN_NUM_1 = Mathf.FloorToInt(f1);
    if ((double) this.TSC.BenefitCostWood(1) > 0.0 && (double) f3 < (double) this.MAX_TRAIN_NUM_1)
      this.MAX_TRAIN_NUM_1 = Mathf.FloorToInt(f3);
    if ((double) this.TSC.BenefitCostSilver(1) > 0.0 && (double) f2 < (double) this.MAX_TRAIN_NUM_1)
      this.MAX_TRAIN_NUM_1 = Mathf.FloorToInt(f2);
    if ((double) this.TSC.BenefitCostOre(1) > 0.0 && (double) f4 < (double) this.MAX_TRAIN_NUM_1)
      this.MAX_TRAIN_NUM_1 = Mathf.FloorToInt(f4);
    if (this.MAX_TRAIN_NUM_1 < 1)
      this.MAX_TRAIN_NUM_1 = 0;
    this.TRAIN_LIMIT = this.TSC.GetTrainLimit();
    if (this.MAX_TRAIN_NUM_1 > this.TRAIN_LIMIT)
      this.MAX_TRAIN_NUM_1 = this.TRAIN_LIMIT;
    this.MAX_TRAIN_NUM_1 = Mathf.Max(this.MAX_TRAIN_NUM_1, 1);
    this.trainSlider.enabled = true;
    this.trainSlider.GetComponent<Collider>().enabled = true;
    if (!(this.buildingType == "fortress") || this.TRAIN_LIMIT > 0)
      return;
    this.trainSlider.value = 0.0f;
    this.trainSlider.GetComponent<Collider>().enabled = false;
  }

  private void UpdateRightBottom()
  {
    Unit_StatisticsInfo currentTrainningUnit = BarracksManager.Instance.GetCurrentTrainningUnit(this.buildingID);
    if (BarracksManager.Instance.IsUnitTraining(this.buildingID) && currentTrainningUnit != null)
    {
      this.rightBottomUnlockedGameObject.SetActive(false);
      this.rightBottomUnlockedGameObject.SetActive(false);
      this.unitTrainningSprite.gameObject.SetActive(false);
      this.trapTrainningSprite.gameObject.SetActive(false);
      if (this.buildingType == "walls")
        this.trapTrainningSprite.gameObject.SetActive(true);
      else
        this.unitTrainningSprite.gameObject.SetActive(true);
      this.rightBottomBusyGameOject.SetActive(true);
      Dictionary<string, string> para = new Dictionary<string, string>()
      {
        {
          "troop_name",
          string.Format("[{0}]", (object) "ffaa00") + ScriptLocalization.Get(currentTrainningUnit.Troop_Name_LOC_ID, true) + "[-]"
        }
      };
      long mTrainingJobId = CityManager.inst.GetBuildingFromID(this.buildingID).mTrainingJobID;
      JobHandle job = JobManager.Instance.GetJob(mTrainingJobId);
      JobEvent jobEvent = job.GetJobEvent();
      this.busyDescriptionLabel.text = jobEvent != JobEvent.JOB_UPGRADE_TROOPS ? ScriptLocalization.GetWithPara("barracks_on_training_troops", para, true) : ScriptLocalization.GetWithPara("barracks_upgrading_troops", para, true);
      double secsRemaining = job != null ? (double) job.LeftTime() : 0.0;
      double totalSeconds = job != null ? (double) job.Duration() : 0.0;
      double num = 0.0;
      this.timerHUDUIItem.mJobID = mTrainingJobId;
      this.timerHUDUIItem.mFreeTimeSecs = num;
      if (this.buildingInfo.Type == "fortress")
      {
        this.timerHUDUIItem.SetDetails("time_bar_building", string.Empty, TimerType.TIMER_TRAP_BUILD, secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
      }
      else
      {
        string targetName = jobEvent != JobEvent.JOB_UPGRADE_TROOPS ? "time_bar_training" : "id_upgrading";
        TimerType timerType = jobEvent != JobEvent.JOB_UPGRADE_TROOPS ? TimerType.TIMER_TRAIN : TimerType.TIMER_UPGRADE_TROOPS;
        this.closeBtn.gameObject.SetActive(jobEvent != JobEvent.JOB_UPGRADE_TROOPS);
        this.timerHUDUIItem.SetDetails(targetName, string.Empty, timerType, secsRemaining, totalSeconds, 1, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, (TimerHUDUIItem.UpdateTimerHUDUIItem) null, false);
      }
      this.timerHUDUIItem.gameObject.SetActive(true);
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
      this.OnSecondHandler(0);
    }
    else if (BarracksManager.Instance.IsBuildingUpgrading(this.buildingID))
    {
      this.rightBottomUnlockedGameObject.SetActive(false);
      this.rightBottomUnlockedGameObject.SetActive(false);
      this.rightBottomBusyGameOject.SetActive(true);
      this.timerHUDUIItem.gameObject.SetActive(false);
      CityManager.BuildingItem buildingFromId = CityManager.inst.GetBuildingFromID(this.buildingID);
      this.busyDescriptionLabel.text = ScriptLocalization.GetWithPara("barracks_on_building", new Dictionary<string, string>()
      {
        {
          "building_name",
          string.Format("[{0}]", (object) "ffaa00") + ScriptLocalization.Get(ConfigManager.inst.DB_Building.GetData(buildingFromId.mType, buildingFromId.mLevel).Building_LOC_ID, true) + "[-] "
        }
      }, true);
    }
    else
    {
      this.rightBottomBusyGameOject.SetActive(false);
      if (BarracksManager.Instance.IsUnitUnlocked(this.TSC.internalId))
      {
        this.rightBottomLockedGameObject.SetActive(false);
        this.rightBottomUnlockedGameObject.SetActive(true);
        this.UpdateMaxTrainNum();
        this.UpdateRightBottonUnlockedState();
      }
      else
      {
        this.rightBottomUnlockedGameObject.SetActive(false);
        this.rightBottomLockedGameObject.SetActive(true);
        this.UpdateRightBottonLockedState();
      }
    }
  }

  private void OnSecondHandler(int timeStamp)
  {
    this.timerHUDUIItem.OnUpdate(timeStamp);
    JobHandle job = JobManager.Instance.GetJob(CityManager.inst.GetBuildingFromID(this.buildingID).mTrainingJobID);
    int num = ItemBag.CalculateCost(job != null ? job.LeftTime() : 0, 0);
    if (num < 0)
      num = 0;
    this.goldLabel.text = Utils.FormatThousands(num.ToString());
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= num)
      this.goldLabel.color = this.normalColor;
    else
      this.goldLabel.color = Color.red;
  }

  private void OnCancelYesPressed()
  {
    long mTrainingJobId = CityManager.inst.GetBuildingFromID(this.buildingID).mTrainingJobID;
    MessageHub.inst.GetPortByAction("City:cancelTrain").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "building_id", (object) this.buildingID, (object) "job_id", (object) mTrainingJobId), (System.Action<bool, object>) ((bSuccess, data) =>
    {
      if (!bSuccess)
        return;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    }), true);
  }

  private void OnCancelNoPressed()
  {
  }

  private void UpdateRightBottonUnlockedState()
  {
    this.costResources.UpdateCostResources(this.TSC.BenefitCostWood(this._troopCount), this.TSC.BenefitCostSilver(this._troopCount), this.TSC.BenefitCostFood(this._troopCount), this.TSC.BenefitCostOre(this._troopCount), true);
    float baseValue = (float) (this._troopCount * this.TSC.Training_Time);
    if ((double) baseValue < 0.0)
      baseValue = 0.0f;
    float finalData = ConfigManager.inst.DB_BenefitCalc.GetFinalData(baseValue, this.TSC.TRAIN_TIME_CAL_KEY);
    this.trainTimeLabel.text = Utils.ConvertSecsToString((double) (int) finalData);
    this.trainNumLabel.text = this._troopCount.ToString();
    this.trainNumInput.value = this._troopCount.ToString();
    this.m_lackRes.Clear();
    this.m_lackitem.Clear();
    this.itemHT.Clear();
    this.itemHT.Add((object) "time", (object) (int) finalData);
    this.itemHT.Add((object) "resources", (object) new Hashtable()
    {
      {
        (object) ItemBag.ItemType.food,
        (object) (long) this.TSC.BenefitCostFood(this._troopCount)
      },
      {
        (object) ItemBag.ItemType.wood,
        (object) (long) this.TSC.BenefitCostWood(this._troopCount)
      },
      {
        (object) ItemBag.ItemType.silver,
        (object) (long) this.TSC.BenefitCostSilver(this._troopCount)
      },
      {
        (object) ItemBag.ItemType.ore,
        (object) (long) this.TSC.BenefitCostOre(this._troopCount)
      }
    });
    this.m_lackRes = ItemBag.CalculateLackResources(this.itemHT);
    this.m_lackitem = ItemBag.CalculateLackItems(this.itemHT);
    this.instTrainCost = ItemBag.CalculateCost(this.itemHT);
    this.itemHT.Remove((object) "time");
    this.gold = ItemBag.CalculateCost(this.itemHT);
    this.goldCostLable.text = Utils.FormatThousands(this.instTrainCost.ToString());
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= this.instTrainCost)
      this.goldCostLable.color = this.normalColor;
    else
      this.goldCostLable.color = Color.red;
    this.UpdateBtnState();
  }

  private void UpdateRightBottonLockedState()
  {
    switch (BarracksManager.Instance.GetBlockedType(this.TSC.internalId))
    {
      case BarracksManager.BlockedType.RESEARCH:
        this.UpdateResearchLockState();
        break;
      case BarracksManager.BlockedType.BUILDING:
        this.UpdateBuildingLockState();
        break;
    }
  }

  private void UpdateBuildingLockState()
  {
    string str1 = "\"" + ScriptLocalization.Get(this.TSC.Troop_Name_LOC_ID, true) + "\"";
    BuildingInfo requireBuildingInfo = BarracksManager.Instance.GetRequireBuildingInfo(this.TSC);
    string str2 = "\"" + ScriptLocalization.Get(requireBuildingInfo.Building_LOC_ID, true) + "\"";
    string str3 = requireBuildingInfo.Building_Lvl.ToString();
    this.lockedDescriptionLabel.text = ScriptLocalization.GetWithPara("barracks_unlock_troops_description", new Dictionary<string, string>()
    {
      {
        "troop_name",
        str1
      },
      {
        "building_name",
        str2
      },
      {
        "NO.",
        str3
      }
    }, true);
    this.gotoUpgradeLabel.text = ScriptLocalization.Get("barracks_uppercase_gotoupgrade", true);
    this.gotoBtn.onClick.Clear();
    this.gotoBtn.onClick.Add(new EventDelegate((EventDelegate.Callback) (() =>
    {
      UIManager.inst.cityCamera.UseBoundary = true;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      string str4 = JsonWriter.Serialize((object) new LPFocusBuilding()
      {
        buildingType = this.buildingInfo.Type
      });
      LinkerHub.Instance.Distribute(JsonWriter.Serialize((object) new LinkerPara()
      {
        classname = "LPFocusBuilding",
        content = str4
      }));
    })));
  }

  private void UpdateResearchLockState()
  {
    this.lockedDescriptionLabel.text = ScriptLocalization.GetWithPara("walls_unlock_traps_description", new Dictionary<string, string>()
    {
      {
        "troop_name",
        "\"" + ScriptLocalization.Get(this.TSC.Troop_Name_LOC_ID, true) + "\""
      },
      {
        "research name",
        ResearchManager.inst.GetTechLevel(int.Parse(this.TSC.Requirement_ID_2)).Name
      }
    }, true);
    this.gotoUpgradeLabel.text = ScriptLocalization.Get("walls_uppercase_gotoresearch", true);
    this.gotoBtn.onClick.Clear();
    this.gotoBtn.onClick.Add(new EventDelegate((EventDelegate.Callback) (() =>
    {
      UIManager.inst.cityCamera.UseBoundary = true;
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
      string str = JsonWriter.Serialize((object) new LPFocusBuilding()
      {
        buildingType = "university"
      });
      LinkerHub.Instance.Distribute(JsonWriter.Serialize((object) new LinkerPara()
      {
        classname = "LPFocusBuilding",
        content = str
      }));
    })));
  }

  private void OnSliderDragFinish()
  {
    if (Mathf.RoundToInt((float) this.TRAIN_LIMIT * this.trainSlider.value) == this._troopCount)
      return;
    this._troopCount = Mathf.RoundToInt((float) this.TRAIN_LIMIT * this.trainSlider.value);
    this.UpdateRightBottom();
  }

  public void OnSliderValueChanged()
  {
    if (Mathf.RoundToInt((float) this.TRAIN_LIMIT * this.trainSlider.value) == this._troopCount)
      return;
    this._troopCount = Mathf.Clamp(Mathf.RoundToInt((float) this.TRAIN_LIMIT * this.trainSlider.value), 0, this.TRAIN_LIMIT);
    this.UpdateRightBottom();
  }

  private void UpdateBtnState()
  {
    if (this._troopCount == 0)
      this.trainBtn.isEnabled = false;
    else
      this.trainBtn.isEnabled = true;
    if (this._troopCount == 0)
      this.instantBtn.isEnabled = false;
    else
      this.instantBtn.isEnabled = true;
    if (!(this.buildingType == "fortress"))
      return;
    if (this._troopCount > this.TRAIN_LIMIT || this._troopCount == 0)
      this.instantBtn.isEnabled = false;
    else
      this.instantBtn.isEnabled = true;
  }

  private void OnJobCompleted(object result)
  {
    Hashtable hashtable = result as Hashtable;
    int result1 = 0;
    if (hashtable == null || !hashtable.ContainsKey((object) "event_type") || (!int.TryParse(hashtable[(object) "event_type"].ToString(), out result1) || !hashtable.ContainsKey((object) "building_id")) || !(hashtable[(object) "building_id"].ToString() == this.buildingID.ToString()))
      return;
    switch (result1)
    {
      case 30:
        this.Refresh();
        break;
      case 32:
      case 87:
        AudioManager.Instance.PlaySound("sfx_troop_training_complete", false);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
        break;
    }
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.unitToBeUpgraded = (Unit_StatisticsInfo) null;
    this.puppet2DRender.Release();
    BuilderFactory.Instance.Release((UIWidget) this.troopClassIcon);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    private string unitID = string.Empty;
    private long buildingID;
    private string buildingType;

    public string UnitID
    {
      get
      {
        return this.unitID;
      }
      set
      {
        this.unitID = value;
      }
    }

    public long BuildingID
    {
      get
      {
        return this.buildingID;
      }
      set
      {
        this.buildingID = value;
      }
    }

    public string BuildingType
    {
      get
      {
        return this.buildingType;
      }
      set
      {
        this.buildingType = value;
      }
    }
  }
}
