﻿// Decompiled with JetBrains decompiler
// Type: ItemUseOrBuyPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class ItemUseOrBuyPopup : Popup
{
  public Color normalColor = new Color(0.9686275f, 0.8f, 0.003921569f, 1f);
  private int _useCount = 1;
  private bool autoFix = true;
  private ProgressBarTweenProxy proxy = new ProgressBarTweenProxy();
  private bool autoFixProgressBar = true;
  public UILabel title;
  public Icon icon;
  public GameObject userCountContent;
  public UIButton leftButtton;
  public UIButton rightButton;
  public UIProgressBar progressBar;
  public UILabel countLabel;
  public UIInput inputCountLabel;
  public UILabel maxCountLabel;
  public UIButton useBt;
  public GameObject useContent;
  public UILabel buytitle;
  public Icon buyIcon;
  public UILabel itemDesc;
  public UIProgressBar buyProgressBar;
  public UILabel buyCountLabel;
  public UIInput buyInputCountLabel;
  public UILabel price;
  public UILabel buyAndUsePrice;
  public GameObject buyContent;
  public UIButton buyAndUseBt;
  public UIButton buybutton;
  public System.Action<bool, object> onUseOrBuyCallBack;
  private int id;
  private bool useItem;
  private bool useOne;
  private bool buyAndUse;
  private bool _canZero;
  public System.Action<int, int> onSelectItemsCallBack;
  private int _maxCount;
  private int _price;

  private int UseCount
  {
    get
    {
      return this._useCount;
    }
    set
    {
      if (this._useCount == value || value <= 0 || value > this.MaxCount)
        return;
      this._useCount = value;
      this.RefreshProgressBar(true);
    }
  }

  private int MaxCount
  {
    get
    {
      if (this.useItem)
      {
        if (this._maxCount > 0)
          return Mathf.Min(ItemBag.Instance.GetItemCount(this.id), this._maxCount);
        return ItemBag.Instance.GetItemCount(this.id);
      }
      ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id);
      int num = this._price;
      if (dataByInternalId != null)
        num = ItemBag.Instance.GetShopItemPrice(dataByInternalId.ID);
      int a = Mathf.FloorToInt((float) (PlayerData.inst.userData.currency.gold / (long) num));
      if (this._maxCount > 0)
        a = Mathf.Min(a, this._maxCount);
      if (a == 0)
        a = 1;
      return a;
    }
  }

  private UIProgressBar ProgressBar
  {
    get
    {
      if (this.useItem)
        return this.progressBar;
      return this.buyProgressBar;
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  public void OnCloseHandler()
  {
    if (this.useItem)
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    else
      UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.InitParam(orgParam as ItemUseOrBuyPopup.Parameter);
    this.UpdateUI();
  }

  public void OnAddItemDownHandler()
  {
    this.proxy.Slider = !this.useItem ? this.buyProgressBar : this.progressBar;
    this.autoFixProgressBar = false;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(1, -1f);
  }

  public void OnAddItemUpHandler()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  public void OnAddItemClick()
  {
  }

  public void OnRemoveItemClick()
  {
  }

  public void OnRemoveItemDownHandler()
  {
    this.proxy.Slider = !this.useItem ? this.buyProgressBar : this.progressBar;
    this.autoFixProgressBar = false;
    float targetValue = 1f / (float) this.MaxCount;
    if (this._canZero)
      targetValue = 0.0f;
    this.proxy.MaxSize = this.MaxCount;
    this.proxy.Play(-1, targetValue);
  }

  public void OnRemoveItemUpHandler()
  {
    this.autoFixProgressBar = true;
    this.proxy.Stop();
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemInfo = this.ItemInfo;
    if (this.useItem)
    {
      NGUITools.SetActive(this.buyContent, false);
      NGUITools.SetActive(this.useContent, true);
      if (!this._canZero)
        this._maxCount = this._maxCount <= 0 ? itemInfo.UseMaxCount : Mathf.Min(this._maxCount, itemInfo.UseMaxCount);
      IconData iconData = new IconData(itemInfo.ImagePath, new string[1]
      {
        itemInfo.LocName
      });
      iconData.Data = (object) itemInfo.internalId;
      this.icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      this.icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      this.icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      this.icon.FeedData((IComponentData) iconData);
      this.maxCountLabel.text = "/" + this.MaxCount.ToString();
      this.title.text = ScriptLocalization.Get("item_uppercase_use_item_title", true);
    }
    else
    {
      ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id);
      NGUITools.SetActive(this.useContent, false);
      NGUITools.SetActive(this.buyContent, true);
      NGUITools.SetActive(this.buybutton.gameObject, !this.buyAndUse);
      NGUITools.SetActive(this.buyAndUseBt.gameObject, this.buyAndUse);
      IconData iconData = new IconData(itemInfo.ImagePath, new string[2]
      {
        itemInfo.LocName,
        itemInfo.LocDescription
      });
      iconData.Data = (object) itemInfo.internalId;
      this.buyIcon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      this.buyIcon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      this.buyIcon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      this.buyIcon.FeedData((IComponentData) iconData);
      this.title.text = itemInfo.LocName;
      this.price.text = this.ShopItemPrice.ToString();
      if (dataByInternalId != null)
        this._maxCount = dataByInternalId.MaxBuyCount;
      if ((long) this.ShopItemPrice > PlayerData.inst.userData.currency.gold)
        this.price.color = Color.red;
      else
        this.price.color = this.normalColor;
      this.buyAndUsePrice.text = this.price.text;
      this.buyAndUsePrice.color = this.price.color;
    }
    this.autoFix = false;
    this.RefreshProgressBar(true);
  }

  private int ShopItemPrice
  {
    get
    {
      if (this._price > 0)
        return this.UseCount * this._price;
      return this.UseCount * ItemBag.Instance.GetShopItemPrice(ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id).ID);
    }
  }

  private void RefreshProgressBar(bool needToChangeBar = true)
  {
    if (this.useItem)
    {
      if (needToChangeBar)
        this.progressBar.value = Mathf.Clamp01((float) this.UseCount / (float) this.MaxCount);
      this.inputCountLabel.value = this.UseCount.ToString();
    }
    else
    {
      if (needToChangeBar)
        this.buyProgressBar.value = Mathf.Clamp01((float) this.UseCount / (float) this.MaxCount);
      this.buyInputCountLabel.value = this.UseCount.ToString();
      this.price.text = this.ShopItemPrice.ToString();
      if ((long) this.ShopItemPrice > PlayerData.inst.userData.currency.gold)
        this.price.color = Color.red;
      else
        this.price.color = this.normalColor;
      this.buyAndUsePrice.text = this.price.text;
      this.buyAndUsePrice.color = this.price.color;
    }
    this.autoFix = true;
  }

  public void OnProgressBarChange()
  {
    int num = Mathf.RoundToInt(this.ProgressBar.value * (float) this.MaxCount);
    if (num <= 0)
    {
      num = !this._canZero ? 1 : 0;
      if (this.autoFixProgressBar)
        this.ProgressBar.value = (float) num / (float) this.MaxCount;
    }
    if (num > this.MaxCount)
      num = this.MaxCount;
    if (this.autoFix)
      this._useCount = num;
    this.RefreshProgressBar(false);
  }

  public UIInput TextInput
  {
    get
    {
      if (this.useItem)
        return this.inputCountLabel;
      return this.buyInputCountLabel;
    }
  }

  public void OnTextInputCommit()
  {
    int result = 1;
    if (!int.TryParse(this.TextInput.value, out result))
      result = 1;
    if (result <= 0)
      result = !this._canZero ? 1 : 0;
    if (result > this.MaxCount)
      result = this.MaxCount;
    this._useCount = result;
    this.autoFix = false;
    this.RefreshProgressBar(this.autoFixProgressBar);
  }

  public void OnTextInputHandler()
  {
    int result = 1;
    if (!int.TryParse(this.TextInput.value, out result))
      return;
    if (result < 0)
      result = 0;
    if (result == 0 && !this._canZero)
      return;
    if (result > this.MaxCount)
      result = this.MaxCount;
    this._useCount = result;
    this.autoFix = false;
    this.RefreshProgressBar(this.autoFixProgressBar);
  }

  public void OnUseItem()
  {
    if (this.onSelectItemsCallBack != null)
    {
      this.onSelectItemsCallBack(this.id, this.UseCount);
      this.OnCloseHandler();
    }
    else
      ItemBag.Instance.UseItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
  }

  public void OnBuyAndUseItem()
  {
    if ((long) this.ShopItemPrice > PlayerData.inst.userData.currency.gold)
    {
      this.OnCloseHandler();
      Utils.ShowNotEnoughGoldTip();
    }
    else
      ItemBag.Instance.BuyAndUseShopItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
  }

  private void UseCallBack(bool success, object result)
  {
    if (this.onUseOrBuyCallBack != null)
      this.onUseOrBuyCallBack(success, result);
    this.OnCloseHandler();
  }

  private ItemStaticInfo ItemInfo
  {
    get
    {
      ItemStaticInfo itemStaticInfo;
      if (this.useItem)
      {
        itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.id);
      }
      else
      {
        ShopStaticInfo dataByInternalId = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id);
        itemStaticInfo = dataByInternalId == null ? ConfigManager.inst.DB_Items.GetItem(this.id) : ConfigManager.inst.DB_Items.GetItem(dataByInternalId.Item_InternalId);
      }
      return itemStaticInfo;
    }
  }

  public void OnBuyItem()
  {
    if ((long) this.ShopItemPrice > PlayerData.inst.userData.currency.gold)
    {
      this.OnCloseHandler();
      Utils.ShowNotEnoughGoldTip();
    }
    else if (ConfigManager.inst.DB_Shop.GetShopDataByInternalId(this.id) != null)
      ItemBag.Instance.BuyShopItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
    else
      ItemBag.Instance.BuySevenDayItem(this.id, this.UseCount, (Hashtable) null, new System.Action<bool, object>(this.UseCallBack));
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.onUseOrBuyCallBack = (System.Action<bool, object>) null;
    this.onSelectItemsCallBack = (System.Action<int, int>) null;
  }

  private void InitParam(ItemUseOrBuyPopup.Parameter param)
  {
    int num = 0;
    ShopStaticInfo shopStaticInfo = (ShopStaticInfo) null;
    this.onUseOrBuyCallBack = param.onUseOrBuyCallBack;
    this._canZero = param.canZero;
    this.onSelectItemsCallBack = param.onSelectItemsCallBack;
    if (param.item_id > 0 || !string.IsNullOrEmpty(param.itemID))
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(param.item_id) ?? ConfigManager.inst.DB_Items.GetItem(param.itemID);
      num = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
      shopStaticInfo = ConfigManager.inst.DB_Shop.GetShopInfoByItemInternalId(itemStaticInfo.internalId);
      this.id = num != 0 ? itemStaticInfo.internalId : (shopStaticInfo == null || param.price != 0 ? itemStaticInfo.internalId : shopStaticInfo.internalId);
    }
    if (param.shop_id > 0 || !string.IsNullOrEmpty(param.shopID))
    {
      shopStaticInfo = ConfigManager.inst.DB_Shop.GetShopDataByInternalId(param.shop_id) ?? ConfigManager.inst.DB_Shop.GetShopData(param.shopID);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopStaticInfo.Item_InternalId);
      num = ItemBag.Instance.GetItemCount(itemStaticInfo.internalId);
      this.id = num != 0 ? itemStaticInfo.internalId : shopStaticInfo.internalId;
    }
    if (num > 0)
    {
      this.useItem = param.type == ItemUseOrBuyPopup.Parameter.Type.UseItem;
      this.buyAndUse = false;
      if (!this.useItem && shopStaticInfo != null && param.price == 0)
        this.id = shopStaticInfo.internalId;
    }
    else
    {
      this.buyAndUse = param.type == ItemUseOrBuyPopup.Parameter.Type.UseItem;
      this.useItem = false;
    }
    this._maxCount = param.maxCount;
    this._price = param.price;
  }

  public class Parameter : Popup.PopupParameter
  {
    public ItemUseOrBuyPopup.Parameter.Type type;
    public int item_id;
    public int shop_id;
    public string itemID;
    public string shopID;
    public int maxCount;
    public int price;
    public bool canZero;
    public System.Action<bool, object> onUseOrBuyCallBack;
    public System.Action<int, int> onSelectItemsCallBack;

    public enum Type
    {
      UseItem,
      BuyItem,
    }
  }
}
