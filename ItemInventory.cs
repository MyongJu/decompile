﻿// Decompiled with JetBrains decompiler
// Type: ItemInventory
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class ItemInventory
{
  private static ItemInventory m_Instance;

  private ItemInventory()
  {
  }

  public static ItemInventory Instance
  {
    get
    {
      if (ItemInventory.m_Instance == null)
        ItemInventory.m_Instance = new ItemInventory();
      return ItemInventory.m_Instance;
    }
  }

  public void SendAddInventory(Hero_EquipmentData item)
  {
    if (item == null)
      return;
    MessageHub.inst.GetPortByAction("Item:setInventoryItem").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "item_property_id",
        (object) item.InternalID
      }
    }, (System.Action<bool, object>) null, true);
  }
}
