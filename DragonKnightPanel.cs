﻿// Decompiled with JetBrains decompiler
// Type: DragonKnightPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class DragonKnightPanel : MonoBehaviour
{
  public UITexture m_DKPortrait;
  public UILabel m_DKName;
  public UILabel m_DKLevel;
  public UILabel m_Score;
  public UITexture m_Title;

  public void SetData(DragonKnightData dkData, bool bigPortrait, long score = 1000)
  {
    if (dkData == null)
      return;
    if (bigPortrait)
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_DKPortrait, "Texture/DragonKnight/DungeonTexture/" + (dkData.Gender != DragonKnightGender.Female ? "dragon_knight_image_0" : "dragon_knight_image_1"), (System.Action<bool>) null, false, true, string.Empty);
    else
      BuilderFactory.Instance.HandyBuild((UIWidget) this.m_DKPortrait, "Texture/DragonKnight/Portrait/" + (dkData.Gender != DragonKnightGender.Female ? "dk_portrait_male" : "dk_portrait_female"), (System.Action<bool>) null, false, true, string.Empty);
    this.m_DKName.text = Utils.XLAT(dkData.Name);
    if ((UnityEngine.Object) this.m_DKLevel != (UnityEngine.Object) null)
      this.m_DKLevel.text = Utils.XLAT("id_level") + " " + (object) dkData.Level;
    if ((UnityEngine.Object) this.m_Score != (UnityEngine.Object) null)
      this.m_Score.text = Utils.XLAT("id_score") + (object) score;
    if (!((UnityEngine.Object) this.m_Title != (UnityEngine.Object) null))
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Title, ConfigManager.inst.DB_DKArenaTitleReward.GetTitleImage(score), (System.Action<bool>) null, true, false, string.Empty);
  }
}
