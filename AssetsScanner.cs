﻿// Decompiled with JetBrains decompiler
// Type: AssetsScanner
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

public class AssetsScanner
{
  private static AssetsScanner _instance;
  private AssetsScanner.Worker _worker;
  private Thread _workerThread;

  private AssetsScanner()
  {
  }

  public static AssetsScanner Instance
  {
    get
    {
      if (AssetsScanner._instance == null)
      {
        SqliteDatabase.Setup();
        AssetsScanner._instance = new AssetsScanner();
        if (AssetsScanner._instance == null)
          throw new ArgumentException("AssetManager hasn't been created yet.");
      }
      return AssetsScanner._instance;
    }
  }

  [Conditional("ENABLE_ASSET_SCANNER")]
  public void Start()
  {
    Debug.Log((object) "[Assets Scanner]Try to run AssetsScanner.");
    this._worker = new AssetsScanner.Worker();
    this._workerThread = new Thread(new ThreadStart(this._worker.Run));
    this._workerThread.Priority = System.Threading.ThreadPriority.Lowest;
    this._workerThread.Start();
    do
      ;
    while (!this._workerThread.IsAlive);
    Debug.Log((object) "[Assets Scanner]Run success.");
  }

  [Conditional("ENABLE_ASSET_SCANNER")]
  public void Stop()
  {
    Debug.Log((object) "[Assets Scanner]Try to stop AssetsScanner.");
    this._worker.RequestStop();
    this._workerThread.Join();
    Debug.Log((object) "[Assets Scanner]Stop success.");
  }

  [Conditional("ENABLE_ASSET_SCANNER")]
  public void AddAsset(string guid)
  {
    this._worker.AddAsset(guid);
  }

  private class Worker
  {
    private Queue<string> _assetsQueue = new Queue<string>();
    private string CreatePattern = "CREATE TABLE IF NOT EXISTS {0} {1};";
    private string CreateIndexPattern = "CREATE INDEX IF NOT EXISTS 'index' ON {0} {1};";
    private string ReplacePattern = "REPLACE INTO {0} {1} VALUES {2};";
    private string AssetsTablePattern = "(guid STRING PRIMARY KEY)";
    private string AssetsTableIndex = "(guid)";
    private volatile bool _shouldStop;
    private SqliteDatabase _db;

    public void Run()
    {
      this._shouldStop = false;
      this.OnStart();
      while (!this._shouldStop)
        this.OnRun();
      this.OnStop();
    }

    public void RequestStop()
    {
      this._shouldStop = true;
    }

    public void OnStart()
    {
      this._db = new SqliteDatabase("assets_log.db");
      this.InitializeAssetsTable();
    }

    public void OnStop()
    {
      lock (((ICollection) this._assetsQueue).SyncRoot)
      {
        while (this._assetsQueue.Count > 0)
          this.AddAssets2DB(this.PopAsset());
      }
      this._assetsQueue.Clear();
      this._db = (SqliteDatabase) null;
    }

    public void OnRun()
    {
      lock (((ICollection) this._assetsQueue).SyncRoot)
      {
        if (this._assetsQueue.Count > 0)
          this.AddAssets2DB(this.PopAsset());
      }
      Thread.Sleep(200);
    }

    public void AddAsset(string guid)
    {
      lock (((ICollection) this._assetsQueue).SyncRoot)
        this._assetsQueue.Enqueue(guid);
    }

    public string PopAsset()
    {
      string empty = string.Empty;
      return this._assetsQueue.Dequeue();
    }

    public void InitializeAssetsTable()
    {
      this.CreateTable("assets_log", this.AssetsTablePattern, this.AssetsTableIndex);
    }

    public void CreateTable(string tableName, string tablePattern, string index)
    {
      this._db.ExecuteQuery(string.Format(this.CreatePattern, (object) tableName, (object) tablePattern));
      this._db.ExecuteQuery(string.Format(this.CreateIndexPattern, (object) tableName, (object) index));
    }

    public void AddAssets2DB(string guid)
    {
      this._db.ExecuteQuery(string.Format(this.ReplacePattern, (object) "assets_log", (object) "(guid)", (object) string.Format("('{0}')", (object) guid)));
    }
  }
}
