﻿// Decompiled with JetBrains decompiler
// Type: ConfigBenefitCalc
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class ConfigBenefitCalc
{
  private const int CALC_OPTION_COUNT = 5;
  public const ConfigBenefitCalc.UnitCalcType ALL_CALC = ConfigBenefitCalc.UnitCalcType.upkeep | ConfigBenefitCalc.UnitCalcType.trainingTime | ConfigBenefitCalc.UnitCalcType.healingTime | ConfigBenefitCalc.UnitCalcType.health | ConfigBenefitCalc.UnitCalcType.attack | ConfigBenefitCalc.UnitCalcType.defense | ConfigBenefitCalc.UnitCalcType.load | ConfigBenefitCalc.UnitCalcType.speed;
  private const int TROOP_CLASS_HEAD_INDEX = 6;
  private const string UNIT_HEAD_CALC = "calc";
  private const string UNIT_TAIL_UPKEEP = "upkeep";
  private const string UNIT_TAIL_TRAINING_TIME = "training_time";
  private const string UNIT_TAIL_HEALING_TIME = "healing_time";
  private const string UNIT_TAIL_HEALTH = "health";
  private const string UNIT_TAIL_ATTACK = "attack";
  private const string UNIT_TAIL_DEFENSE = "defense";
  private const string UNIT_TAIL_LOAD = "load";
  private const string UNIT_TAIL_SPEED = "speed";
  public Dictionary<int, BenefitCalcInfo> datas;
  private Dictionary<string, int> dicByUniqueId;
  private static ConfigBenefitCalc.UnitFinalStruct _unitFinalStruct;

  public ConfigBenefitCalc()
  {
    this.datas = new Dictionary<int, BenefitCalcInfo>();
    this.dicByUniqueId = new Dictionary<string, int>();
  }

  public void BuildDB(object res)
  {
    Hashtable hashtable1 = res as Hashtable;
    if (hashtable1 == null)
    {
      D.error((object) "The Config({0}) load error ; res is not a hashtable or not exsit", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable1[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "The Config({0}) load error ; headers is not a hashtable or not exsit", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "formula");
        int index3 = ConfigManager.GetIndex(inHeader, "Para1s");
        int index4 = ConfigManager.GetIndex(inHeader, "Para2s");
        hashtable1.Remove((object) "headers");
        foreach (object key1 in (IEnumerable) hashtable1.Keys)
        {
          BenefitCalcInfo data = new BenefitCalcInfo();
          if (int.TryParse(key1.ToString(), out data.internalId))
          {
            ArrayList arr = hashtable1[key1] as ArrayList;
            if (arr != null)
            {
              data.ID = ConfigManager.GetString(arr, index1);
              data.formula = ConfigManager.GetInt(arr, index2);
              Hashtable hashtable2 = arr[index3] as Hashtable;
              if (hashtable2 != null && hashtable2.Count > 0)
              {
                data.param1 = new string[hashtable2.Count];
                int index5 = 0;
                foreach (object key2 in (IEnumerable) hashtable2.Keys)
                {
                  data.param1[index5] = key2.ToString();
                  ++index5;
                }
              }
              else
                data.param1 = (string[]) null;
              Hashtable hashtable3 = arr[index4] as Hashtable;
              if (hashtable3 != null && hashtable3.Count > 0)
              {
                data.param2 = new string[hashtable3.Count];
                int index5 = 0;
                foreach (object key2 in (IEnumerable) hashtable3.Keys)
                {
                  data.param2[index5] = key2.ToString();
                  ++index5;
                }
              }
              else
                data.param2 = (string[]) null;
              this.PushData(data.internalId, data.ID, data);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, BenefitCalcInfo>.ValueCollection Values
  {
    get
    {
      return this.datas.Values;
    }
  }

  private void PushData(int internalId, string uniqueId, BenefitCalcInfo data)
  {
    if (this.datas.ContainsKey(internalId))
      return;
    this.datas.Add(internalId, data);
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      this.dicByUniqueId.Add(uniqueId, internalId);
    ConfigManager.inst.AddData(internalId, (object) data);
  }

  public void Clear()
  {
    if (this.datas != null)
      this.datas.Clear();
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
  }

  public BenefitCalcInfo GetData(int internalId)
  {
    if (this.datas.ContainsKey(internalId))
      return this.datas[internalId];
    return (BenefitCalcInfo) null;
  }

  public BenefitCalcInfo GetData(string uniqueId)
  {
    if (!this.dicByUniqueId.ContainsKey(uniqueId))
      return (BenefitCalcInfo) null;
    return this.datas[this.dicByUniqueId[uniqueId]];
  }

  public int GetFinalData(int baseValue, string BenefitCalcID)
  {
    return (int) this.GetFinalData((float) baseValue, BenefitCalcID);
  }

  public float GetFinalData(float baseValue, string BenefitCalcID)
  {
    return this.GetFinalData(baseValue, BenefitCalcID, ConfigBenefitCalc.CalcOption.none);
  }

  public float GetFinalData(float baseValue, int BenefitCalcInternalID)
  {
    return this.GetFinalData(baseValue, this.datas[BenefitCalcInternalID].ID, ConfigBenefitCalc.CalcOption.none);
  }

  public float GetFinalData(float baseValue, string BenefitCalcID, ConfigBenefitCalc.CalcOption option)
  {
    BenefitCalcInfo data = this.GetData(BenefitCalcID);
    if (data == null)
      return baseValue;
    float p1 = 0.0f;
    float p2 = 0.0f;
    int num1 = (int) option;
    int num2 = 64;
    int num3 = 128;
    bool flag1 = (num2 & num1) > 0;
    bool flag2 = (num3 & num1) > 0;
    if (data.param1 != null)
    {
      for (int index = 0; index < data.param1.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param1[index]))
        {
          BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(data.param1[index]);
          float num4 = p1 + benefitInfo.total;
          if (flag1)
            num4 -= benefitInfo.artifact.value;
          if (flag2)
            num4 -= benefitInfo.title.value;
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[data.param1[index]];
          p1 = num4 + this.GetOptionValue(dbProperty.InternalID, option);
        }
      }
    }
    if (data.param2 != null)
    {
      for (int index = 0; index < data.param2.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param2[index]))
        {
          BenefitInfo benefitInfo = DBManager.inst.DB_Local_Benefit.Get(data.param2[index]);
          float num4 = p2 + benefitInfo.total;
          if (flag1)
            num4 -= benefitInfo.artifact.value;
          if (flag2)
            num4 -= benefitInfo.title.value;
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[data.param2[index]];
          p2 = num4 + this.GetOptionValue(dbProperty.InternalID, option);
        }
      }
    }
    switch (data.formula)
    {
      case 1:
        return BenefitCalcInfo.BenefitFormula1(baseValue, p1, p2);
      case 2:
        return BenefitCalcInfo.BenefitFormula2(baseValue, p1, p2);
      case 4:
        return BenefitCalcInfo.BenefitFormula4(baseValue, p1, p2);
      default:
        return baseValue;
    }
  }

  private float GetOptionValue(int benefitID, ConfigBenefitCalc.CalcOption config)
  {
    float result = 0.0f;
    if (config != ConfigBenefitCalc.CalcOption.none)
    {
      int num = (int) config;
      if ((num & 16) > 0 || (num & 1) > 0 || ((num & 2) > 0 || (num & 8) > 0) || (num & 4) > 0)
      {
        DragonKnightData dragonKnightData = DBManager.inst.DB_DragonKnight.GetDragonKnightData(0L);
        if (dragonKnightData != null)
        {
          PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[benefitID];
          result += dragonKnightData.GetBenefitValue(dbProperty.ID);
        }
      }
    }
    List<KingdomBuffPayload.KingdomBuffData> kingdomBuffDataList = KingdomBuffPayload.Instance.KingdomBuffDataList;
    if (kingdomBuffDataList != null)
      kingdomBuffDataList.ForEach((System.Action<KingdomBuffPayload.KingdomBuffData>) (x =>
      {
        KingdomBuffInfo kingdomBuffInfo = ConfigManager.inst.DB_KingdomBuff.Get(x.buffId);
        bool flag = true;
        KingSkillInfo byType = ConfigManager.inst.DB_KingSkill.GetByType("after_war_training");
        if (byType != null && byType.KingdomBuffId == x.buffId)
        {
          UserBenefitParamData benefitParamData = DBManager.inst.DB_UserBenefitParam.Get(PlayerData.inst.uid);
          if (benefitParamData != null && benefitParamData.KingdomTrainLeft <= 0)
            flag = false;
        }
        if (!flag || kingdomBuffInfo == null || kingdomBuffInfo.benefitId != benefitID)
          return;
        result += x.benefitValue;
      }));
    if (config == ConfigBenefitCalc.CalcOption.none)
      return result;
    DragonData.DragonBenfitsInfo dragonBenfitsInfo = (DragonData.DragonBenfitsInfo) null;
    if (PlayerData.inst != null && PlayerData.inst.dragonData != null && PlayerData.inst.dragonData.benefits != null)
      dragonBenfitsInfo = PlayerData.inst.dragonData.benefits;
    for (int index = 0; index < 5; ++index)
    {
      if ((config & (ConfigBenefitCalc.CalcOption) (1 << index)) > ConfigBenefitCalc.CalcOption.none)
      {
        int num = 1 << index;
        switch (num)
        {
          case 1:
            Dictionary<int, float> dictionary1;
            if (dragonBenfitsInfo != null && dragonBenfitsInfo.datas.TryGetValue("attack", out dictionary1) && (dictionary1 != null && dictionary1.ContainsKey(benefitID)))
            {
              result += dictionary1[benefitID];
              continue;
            }
            continue;
          case 2:
            Dictionary<int, float> dictionary2;
            if (dragonBenfitsInfo != null && dragonBenfitsInfo.datas.TryGetValue("defend", out dictionary2) && (dictionary2 != null && dictionary2.ContainsKey(benefitID)))
            {
              result += dictionary2[benefitID];
              continue;
            }
            continue;
          case 4:
            Dictionary<int, float> dictionary3;
            if (dragonBenfitsInfo != null && dragonBenfitsInfo.datas.TryGetValue("gather", out dictionary3) && (dictionary3 != null && dictionary3.ContainsKey(benefitID)))
            {
              result += dictionary3[benefitID];
              continue;
            }
            continue;
          case 8:
            Dictionary<int, float> dictionary4;
            if (dragonBenfitsInfo != null && dragonBenfitsInfo.datas.TryGetValue("attack_monster", out dictionary4) && (dictionary4 != null && dictionary4.ContainsKey(benefitID)))
            {
              result += dictionary4[benefitID];
              continue;
            }
            continue;
          default:
            if (num == 16 && dragonBenfitsInfo != null)
            {
              result += PlayerData.inst.dragonData.benefits.GetBenefit(benefitID);
              continue;
            }
            continue;
        }
      }
    }
    return result;
  }

  public float GetBenefitPercentage(string BenefitCalcID)
  {
    BenefitCalcInfo data = this.GetData(BenefitCalcID);
    if (data == null)
      return 0.0f;
    float num = 0.0f;
    if (data.param1 != null)
    {
      for (int index = 0; index < data.param1.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param1[index]))
          num += DBManager.inst.DB_Local_Benefit.Get(data.param1[index]).total;
      }
    }
    return num;
  }

  public float GetBenefitValue(string BenefitCalcID)
  {
    BenefitCalcInfo data = this.GetData(BenefitCalcID);
    if (data == null)
      return 0.0f;
    float num = 0.0f;
    if (data.param2 != null)
    {
      for (int index = 0; index < data.param2.Length; ++index)
      {
        if (!string.IsNullOrEmpty(data.param2[index]))
          num += DBManager.inst.DB_Local_Benefit.Get(data.param2[index]).total;
      }
    }
    return num;
  }

  public ConfigBenefitCalc.UnitFinalStruct GetUnitFinalData(string unitId, ConfigBenefitCalc.UnitCalcType calcType = ConfigBenefitCalc.UnitCalcType.upkeep | ConfigBenefitCalc.UnitCalcType.trainingTime | ConfigBenefitCalc.UnitCalcType.healingTime | ConfigBenefitCalc.UnitCalcType.health | ConfigBenefitCalc.UnitCalcType.attack | ConfigBenefitCalc.UnitCalcType.defense | ConfigBenefitCalc.UnitCalcType.load | ConfigBenefitCalc.UnitCalcType.speed, ConfigBenefitCalc.CalcOption option = ConfigBenefitCalc.CalcOption.none)
  {
    Unit_StatisticsInfo data = ConfigManager.inst.DB_Unit_Statistics.GetData(unitId);
    if (data == null)
    {
      D.error((object) "Can't find the {0}'s info", (object) unitId);
      return ConfigBenefitCalc._unitFinalStruct;
    }
    string str = data.Troop_Class_ID.Substring(6);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.upkeep) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.upkeep = this.GetFinalData(data.Upkeep, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "upkeep"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.trainingTime) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.trainingTime = (int) this.GetFinalData((float) data.Training_Time, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "training_time"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.healingTime) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.healingTime = (int) this.GetFinalData((float) data.Heal_Time, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "healing_time"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.health) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.health = (int) this.GetFinalData((float) data.Health, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "health"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.attack) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.attack = (int) this.GetFinalData((float) data.Attack, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "attack"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.defense) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.defense = (int) this.GetFinalData((float) data.Defense, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "defense"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.load) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.load = this.GetFinalData((float) data.GatherCapacity, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "load"), option);
    if ((calcType & ConfigBenefitCalc.UnitCalcType.speed) > (ConfigBenefitCalc.UnitCalcType) 0)
      ConfigBenefitCalc._unitFinalStruct.speed = this.GetFinalData(data.Speed, string.Format("{0}_{1}_{2}", (object) "calc", (object) str, (object) "speed"), option);
    return ConfigBenefitCalc._unitFinalStruct;
  }

  public class ConfigBenefitCalcConst
  {
    public const string CALC_RESEARCH_TIME = "calc_research_time";
    public const string CALC_HEALING_CAPACITY = "calc_healing_capacity";
    public const string CALC_TRAP_CAPACITY = "calc_trap_capacity";
    public const string CALC_CITY_DEFENSE = "calc_city_defense";
  }

  public struct CalcID
  {
    public const string RESEARCH_TIME = "";
    public const string MARCH_TIME = "";
  }

  public enum CalcOption
  {
    none = 0,
    dragonAttack = 1,
    dragonDefense = 2,
    dragonGather = 4,
    dragonAttackMonster = 8,
    dragonAll = 16, // 0x00000010
    excludeArtifact = 64, // 0x00000040
    excludeTitle = 128, // 0x00000080
  }

  public enum UnitCalcType
  {
    upkeep = 1,
    trainingTime = 2,
    healingTime = 4,
    health = 8,
    attack = 16, // 0x00000010
    defense = 32, // 0x00000020
    load = 64, // 0x00000040
    speed = 128, // 0x00000080
    count = 256, // 0x00000100
  }

  public struct UnitFinalStruct
  {
    public float upkeep;
    public int trainingTime;
    public int healingTime;
    public int health;
    public int attack;
    public int defense;
    public float load;
    public float speed;
  }
}
