﻿// Decompiled with JetBrains decompiler
// Type: NpcStoreItemSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UnityEngine;

public class NpcStoreItemSlot : DynamicReplaceItem
{
  private Color[] backgroundColer = new Color[3]
  {
    new Color(0.1333333f, 0.5568628f, 0.2078431f, 1f),
    new Color(0.1333333f, 0.2235294f, 0.5568628f, 1f),
    new Color(0.2509804f, 0.1294118f, 0.5568628f, 1f)
  };
  private float moveduration = 0.5f;
  public System.Action<int> onBuyButtonClick;
  public UITexture background;
  private int itemId;
  [SerializeField]
  private NpcStoreItemSlot.Panel panel;

  public int npcStoreItemInteranlId { private set; get; }

  public int index { private set; get; }

  public string itemName
  {
    get
    {
      return this.panel.slotName.text;
    }
  }

  public string costType { private set; get; }

  public int costValue { private set; get; }

  public void Setup(int itemInteranlId, int inIndex, System.Action<int> onBuyButtonClickEvent = null)
  {
    this.index = inIndex;
    this.npcStoreItemInteranlId = itemInteranlId;
    NpcStoreInfo data = ConfigManager.inst.DB_NpcStore.GetData(itemInteranlId);
    if (data != null)
    {
      this.itemId = data.itemId;
      BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.icon, data.imagePath, (System.Action<bool>) null, true, false, string.Empty);
      this.panel.slotName.text = ScriptLocalization.Get(data.description, true);
      if (this.itemId > 0)
        Utils.SetItemName(this.panel.slotName, this.itemId);
      else
        this.panel.slotName.color = Color.white;
      this.panel.slotCount.text = string.Format("x {0}", (object) data.itemValue);
      this.costType = data.costType;
      this.costValue = data.costCurrentValue;
      if (this.itemId > 0)
        Utils.SetItemBackground(this.background, this.itemId);
      else
        Utils.SetItemNormalBackground(this.background, 0);
      this.SetCurrencyType(data.costType);
      if (data.costType == "gold")
      {
        this.panel.discountContainer.gameObject.SetActive(true);
        this.panel.slotPrice.gameObject.SetActive(false);
        Color color = new Color(0.9686275f, 0.8f, 0.003921569f, 1f);
        this.panel.discountInitPrice.text = data.costInitValue.ToString();
        this.panel.discountInitPrice.color = color;
        Utils.SetPriceToLabel(this.panel.discountCurPrice, data.costCurrentValue);
      }
      else
      {
        this.panel.discountContainer.gameObject.SetActive(false);
        this.panel.slotPrice.gameObject.SetActive(true);
        this.panel.slotPrice.text = data.costCurrentValue.ToString();
      }
      this.panel.premiumIcon.gameObject.SetActive(data.descountLevel == 4);
      this.SetEffect(data.descountLevel);
    }
    this.onBuyButtonClick = onBuyButtonClickEvent;
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.itemId, this.panel.border, 0L, 0L, 0);
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.itemId, this.panel.border, 0L, 0L, 0);
  }

  public void OnItemRelease()
  {
    Utils.StopShowItemTip();
  }

  private void SetEffect(int descountLevel)
  {
    if (descountLevel == 1)
    {
      this.panel.effect.SetActive(false);
    }
    else
    {
      this.panel.effect.SetActive(true);
      foreach (Renderer componentsInChild in this.panel.effect.GetComponentsInChildren<Renderer>())
      {
        foreach (Material material in componentsInChild.materials)
          material.SetColor("_TintColor", this.backgroundColer[descountLevel - 2]);
      }
      foreach (ParticleSystem componentsInChild in this.panel.effect.GetComponentsInChildren<ParticleSystem>())
        componentsInChild.Play();
    }
  }

  public void SetCurrencyType(string costType)
  {
    string key = costType;
    if (key == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map91 == null)
    {
      // ISSUE: reference to a compiler-generated field
      NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map91 = new Dictionary<string, int>(5)
      {
        {
          "food",
          0
        },
        {
          "wood",
          1
        },
        {
          "ore",
          2
        },
        {
          "silver",
          3
        },
        {
          "gold",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map91.TryGetValue(key, out num))
      return;
    switch (num)
    {
      case 0:
        this.panel.slotPirceIcon.spriteName = "food_icon";
        break;
      case 1:
        this.panel.slotPirceIcon.spriteName = "wood_icon";
        break;
      case 2:
        this.panel.slotPirceIcon.spriteName = "ore_icon";
        break;
      case 3:
        this.panel.slotPirceIcon.spriteName = "silver_icon";
        break;
      case 4:
        this.panel.slotPirceIcon.spriteName = "icon_currency_premium";
        break;
    }
  }

  public bool ButtonEnable
  {
    set
    {
      if (this.panel == null || !((UnityEngine.Object) this.panel.BT_Buy != (UnityEngine.Object) null))
        return;
      this.panel.BT_Buy.isEnabled = value;
    }
    get
    {
      if (this.panel != null && (UnityEngine.Object) this.panel.BT_Buy != (UnityEngine.Object) null)
        return this.panel.BT_Buy.isEnabled;
      return false;
    }
  }

  public void RefreshCost()
  {
    string costType = this.costType;
    if (costType == null)
      return;
    // ISSUE: reference to a compiler-generated field
    if (NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map92 == null)
    {
      // ISSUE: reference to a compiler-generated field
      NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map92 = new Dictionary<string, int>(5)
      {
        {
          "food",
          0
        },
        {
          "wood",
          1
        },
        {
          "ore",
          2
        },
        {
          "silver",
          3
        },
        {
          "gold",
          4
        }
      };
    }
    int num;
    // ISSUE: reference to a compiler-generated field
    if (!NpcStoreItemSlot.\u003C\u003Ef__switch\u0024map92.TryGetValue(costType, out num))
      return;
    switch (num)
    {
      case 0:
        this.panel.BT_Buy.isEnabled = PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD) >= (double) this.costValue;
        break;
      case 1:
        this.panel.BT_Buy.isEnabled = PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD) >= (double) this.costValue;
        break;
      case 2:
        this.panel.BT_Buy.isEnabled = PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE) >= (double) this.costValue;
        break;
      case 3:
        this.panel.BT_Buy.isEnabled = PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER) >= (double) this.costValue;
        break;
      case 4:
        this.panel.BT_Buy.isEnabled = true;
        break;
    }
  }

  public void OnBuyClick()
  {
    if (this.onBuyButtonClick == null)
      return;
    this.onBuyButtonClick(this.index);
  }

  public override void Init(object args)
  {
    NpcStoreItemSlot.Data data = args as NpcStoreItemSlot.Data;
    this.Setup(data.itemInteranlId, data.inIndex, data.onBuyButtonClickEvent);
  }

  public override void FlyOut(System.Action action = null)
  {
    this.MoveAnimation((System.Action) (() =>
    {
      if (action != null)
        action();
      this.transform.parent = (Transform) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }));
  }

  public override void FlyIn(System.Action action = null)
  {
    this.MoveAnimation(action);
  }

  public void MoveAnimation(System.Action action = null)
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform);
    Vector3 localPosition = this.transform.localPosition;
    localPosition.x += relativeWidgetBounds.size.x;
    iTween.MoveTo(this.gameObject, iTween.Hash((object) "position", (object) localPosition, (object) "time", (object) this.moveduration, (object) "easetype", (object) iTween.EaseType.linear, (object) "islocal", (object) true, (object) "oncomplete", (object) "OnAnimationComplete", (object) "oncompleteparams", (object) action));
  }

  private void OnAnimationComplete(System.Action action = null)
  {
    if (action == null)
      return;
    action();
  }

  public class Data
  {
    public int itemInteranlId;
    public int inIndex;
    public System.Action<int> onBuyButtonClickEvent;
  }

  [Serializable]
  protected class Panel
  {
    public UITexture icon;
    public UILabel slotName;
    public UILabel slotCount;
    public UISprite slotPirceIcon;
    public UILabel slotPrice;
    public Transform premiumIcon;
    public Transform border;
    public Transform discountContainer;
    public UILabel discountCurPrice;
    public UILabel discountInitPrice;
    public UIButton BT_Buy;
    public GameObject effect;
  }
}
