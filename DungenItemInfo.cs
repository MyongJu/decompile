﻿// Decompiled with JetBrains decompiler
// Type: DungenItemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;
using UnityEngine;

public class DungenItemInfo : MonoBehaviour
{
  public UISprite[] arrows;
  public UILabel itemName;
  public UILabel itemDiscription;
  public UILabel weight;
  private DragonKnightDungeonData.BagInfo bagInfo;

  public int ArrowIndex
  {
    set
    {
      for (int index = 0; index < this.arrows.Length; ++index)
      {
        if (index == value)
          NGUITools.SetActive(this.arrows[index].gameObject, true);
        else
          NGUITools.SetActive(this.arrows[index].gameObject, false);
      }
    }
  }

  public void OnDiscardBtnClick()
  {
    UIManager.inst.OpenPopup("DragonKnight/DungeonInventoryDiscardPopup", (Popup.PopupParameter) new DungeonInventoryDiscardPopup.Parameter()
    {
      item_id = this.bagInfo.itemId,
      amount = this.bagInfo.count
    });
  }

  public void InitUI(DragonKnightDungeonData.BagInfo bagInfo, int arrowIndex)
  {
    this.bagInfo = bagInfo;
    this.ArrowIndex = arrowIndex;
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(bagInfo.itemId);
    this.itemName.text = itemStaticInfo.LocName;
    this.itemDiscription.text = itemStaticInfo.LocDescription;
    this.weight.text = Utils.FormatThousands((itemStaticInfo.DungeonWeight * bagInfo.count).ToString());
  }
}
