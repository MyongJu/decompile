﻿// Decompiled with JetBrains decompiler
// Type: WatchTowerBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class WatchTowerBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UILabel contentLabel;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    this.contentLabel.text = Utils.XLAT("watchtower_building_info_upgrade_bonuses_description");
  }
}
