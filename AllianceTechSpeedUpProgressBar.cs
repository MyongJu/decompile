﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechSpeedUpProgressBar
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AllianceTechSpeedUpProgressBar : StandardProgressBar
{
  public override void Init(int initcount, int maxcount)
  {
    this.maxCount = maxcount;
    if (this.maxCount == 0)
      this.Enabled(false);
    else
      this.Enabled(true);
    this.CurrentCount = initcount;
  }

  protected override void UpdateBarUI(int count)
  {
    if (this.maxCount == 0)
      this.bar.value = 0.0f;
    else
      this.bar.value = (float) count / (float) this.maxCount;
  }
}
