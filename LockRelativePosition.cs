﻿// Decompiled with JetBrains decompiler
// Type: LockRelativePosition
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LockRelativePosition : MonoBehaviour
{
  public float scaleMaxOffset = 5f;
  public Transform target;
  public bool lockX;
  public float offsetX;
  public bool lockY;
  public float offsetY;
  public bool lockZ;
  public float offsetZ;
  public bool scaleWithZOffset;
  private Vector3 startScale;

  private void Start()
  {
    if (!((Object) this.target != (Object) null) || !this.scaleWithZOffset)
      return;
    this.startScale = this.transform.localScale;
    this.Scale();
  }

  private void Scale()
  {
    this.transform.localScale = this.startScale * Mathfx.ScaleFloat(Mathf.Clamp(Mathf.Abs(this.transform.localPosition.z - this.target.localPosition.z), 0.0f, this.scaleMaxOffset), 0.0f, this.scaleMaxOffset, 1f, 0.0f);
  }

  private void LateUpdate()
  {
    if (!((Object) this.target != (Object) null))
      return;
    Vector3 position = this.transform.position;
    if (this.lockX)
      position.x = this.target.position.x + this.offsetX;
    if (this.lockY)
      position.y = this.target.position.y + this.offsetY;
    if (this.lockZ)
      position.z = this.target.position.z + this.offsetZ;
    if (this.scaleWithZOffset)
      this.Scale();
    this.transform.position = position;
  }
}
