﻿// Decompiled with JetBrains decompiler
// Type: IAPPackageInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class IAPPackageInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "pay_id")]
  public string pay_id;
  [Config(Name = "discount")]
  public double discount;
  [Config(Name = "times")]
  public int times;
  [Config(Name = "gold")]
  public long gold;
  [Config(Name = "reward_group_id")]
  public int reward_group_id;
  [Config(Name = "alliance_reward_group")]
  public int alliance_reward_group;
  [Config(Name = "bonus_gold")]
  public int bonus_gold;
  [Config(Name = "description")]
  public string description;

  public string productId
  {
    get
    {
      IAPPlatformInfo iapPlatformInfo = ConfigManager.inst.DB_IAPPlatform.Get(Application.platform, this.pay_id);
      if (iapPlatformInfo != null)
        return iapPlatformInfo.product_id;
      return string.Empty;
    }
  }
}
