﻿// Decompiled with JetBrains decompiler
// Type: UI.Alert
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

namespace UI
{
  public class Alert
  {
    public static bool Show(string description)
    {
      UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
      {
        type = CommonBlocker.CommonBlockerType.error,
        displayType = 8,
        descriptionText = description
      });
      return true;
    }

    public static bool Show(string description, string title)
    {
      UIManager.inst.ShowSystemBlocker("CommonBlocker", (SystemBlocker.SystemBlockerParameter) new CommonBlocker.Parameter()
      {
        type = CommonBlocker.CommonBlockerType.error,
        displayType = 12,
        descriptionText = description,
        title = title
      });
      return true;
    }
  }
}
