﻿// Decompiled with JetBrains decompiler
// Type: UI.UIManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
  public class UIManager : MonoBehaviour
  {
    public PriorityPopupQueue priorityPopupQueue = new PriorityPopupQueue();
    private Dictionary<string, Hud> _huds = new Dictionary<string, Hud>();
    private Dictionary<string, Dialog> _dialogs = new Dictionary<string, Dialog>();
    private Stack<Dialog> _dialogStack = new Stack<Dialog>();
    private List<string> _cacheIgnoreList = new List<string>();
    private Dictionary<int, Popup> _popups = new Dictionary<int, Popup>();
    private Stack<int> _popupStack = new Stack<int>();
    private int _LocalPopupID = 1;
    private Dictionary<string, SystemBlocker> _systemBlockers = new Dictionary<string, SystemBlocker>();
    private HubPort startRallyPort = MessageHub.inst.GetPortByAction("start_rally");
    private HubPort beRallyPort = MessageHub.inst.GetPortByAction("be_rally");
    private HubPort cancelRallyPort = MessageHub.inst.GetPortByAction("cancel_rally");
    private HubPort endRallyPort = MessageHub.inst.GetPortByAction("end_rally");
    private HubPort systemAnnoucementPort = MessageHub.inst.GetPortByAction("announcement");
    private HubPort systemMegaphonePort = MessageHub.inst.GetPortByAction("system_megaphone");
    private HubPort megaphonePort = MessageHub.inst.GetPortByAction("megaphone");
    private HubPort digsetCreatePort = MessageHub.inst.GetPortByAction("dig_site_create");
    private HubPort activtyCreatePort = MessageHub.inst.GetPortByAction("activity_notice");
    private int _occlusionCallStack = -1;
    public const string UI_PREFAB_ROOT = "Prefab/UI/";
    private const string Z_BLOCKER_PATH = "Prefab/UI/Common/z_Blocker";
    private const string SPLASH_SCREEN_PATH = "Loading/SplashScreen";
    private const string SPLASH_SCREEN_37_PATH = "Loading/SplashScreen_37";
    private const string SPLASH_SCREEN_CN_PATH = "Loading/SplashScreen_cn";
    private const string LOGO_SCREEN_PATH = "Prefab/UI/Common/LogoScreen";
    private const string TOAST_SCREEN_PATH = "Prefab/UI/Common/OverlayToast";
    public const string CLOUD_SCREEN_PATH = "Prefab/UI/Common/CloudScreen";
    private const string ANNOUNCEMENT_PATH = "Prefab/UI/Common/SystemAnnouncement";
    private const string MEGAPHONE_PATH = "Prefab/UI/Common/SystemMegaphone";
    public const string HUD_PRE_PATH = "Prefab/UI/Huds/";
    public const string DIALOG_PRE_PATH = "Prefab/UI/Dialogs/";
    public const string POPUP_PRE_PATH = "Prefab/UI/Popups/";
    public const int MAX_POPUP_COUNT = 20;
    public const int POPUP_BASE_PANEL_DEPTH = 60;
    public const int MANUAL_BLOCKER_PANEL_DEPTH = 90;
    private const int INVALID_POPUP_ID = 0;
    public const string SYSTEM_BLOCK_PRE_PATH = "Prefab/UI/System_Blockers/";
    private const string SUIT_TIP_PATH = "Prefab/UI/Common/EquipmentSuitBenifitTip";
    private static UIManager _inst;
    public UIRoot uiRoot;
    public Camera ui2DCamera;
    public Camera ui2DOverlayCamera;
    public Camera ui3DCamera;
    public TileCamera tileCamera;
    public CityCamera cityCamera;
    public GameObject fullScreenGrey;
    private UIAtlas _atlasLordTitle_UI;
    public SplashScreen splashScreen;
    public LogoScreen logoScreen;
    [HideInInspector]
    public OverlayToast toast;
    private CloudScreen _cloud;
    private bool _isInited;
    private PublicHUD _publicHUD;
    private TimerHUD _timerHUD;
    public SystemAnnouncement _systemAnnouncement;
    private SystemMegaphone _systemMegaphone;
    public System.Action<bool> onPublicHudStateChange;
    public bool AutoOpen;
    private UIPanel PopupZBlocker;
    private GameObject ManualZBlocker;
    public System.Action<bool> onAllPopupClosed;
    private ToastManager _toastManager;
    public System.Action<bool> onAllSystemBlockerClosed;
    private EquipmentSuitBenefitTip _equipmentSuitBenefitTip;

    public event System.Action OnWindowClear;

    public static UIManager inst
    {
      get
      {
        return UIManager._inst;
      }
    }

    public UIAtlas atlasCommon
    {
      get
      {
        return AtlasManager.inst.atlasCommon;
      }
    }

    public UIAtlas atlasBasicGui
    {
      get
      {
        return AtlasManager.inst.atlasBasicGui;
      }
    }

    public UIAtlas atlasGemIcons
    {
      get
      {
        return AtlasManager.inst.atlasGemIcons;
      }
    }

    public UIAtlas atlasPVETiles
    {
      get
      {
        return AtlasManager.inst.atlasPVETiles;
      }
    }

    public UIAtlas atlasGUI_2
    {
      get
      {
        return AtlasManager.inst.atlasGUI_2;
      }
    }

    public UIAtlas atlasScene_UI
    {
      get
      {
        return AtlasManager.inst.atlasScene_UI;
      }
    }

    public UIAtlas atlasLordTitle_UI
    {
      get
      {
        if ((UnityEngine.Object) this._atlasLordTitle_UI == (UnityEngine.Object) null)
        {
          GameObject gameObject = AssetManager.Instance.Load("Prefab/UI/Atlas/LordTitle_UI", typeof (GameObject)) as GameObject;
          if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
            this._atlasLordTitle_UI = gameObject.GetComponent<UIAtlas>();
        }
        return this._atlasLordTitle_UI;
      }
    }

    [HideInInspector]
    public CloudScreen Cloud
    {
      get
      {
        if ((UnityEngine.Object) this._cloud == (UnityEngine.Object) null)
          this._cloud = this._CreateGO<CloudScreen>("Prefab/UI/Common/CloudScreen", this.ui2DOverlayCamera.transform);
        return this._cloud;
      }
      set
      {
        this._cloud = value;
      }
    }

    public void Awake()
    {
      UIManager._inst = this;
      this.CreateItems();
      if (this._systemBlockers == null || this._systemBlockers.ContainsKey("NetErrorBlocker"))
        return;
      this._CreateSystemBlocker("NetErrorBlocker");
    }

    private void CreateItems()
    {
      this.CreateLogin();
    }

    private void CreateLogin()
    {
      this.splashScreen = !CustomDefine.IsiOSCNDefine() ? (!CustomDefine.IsSQWanPackage() ? (!(LocalizationManager.CurrentLanguageCode == "zh-CN") ? this._CreateGO<SplashScreen>("Loading/SplashScreen", this.ui2DOverlayCamera.transform) : this._CreateGO<SplashScreen>("Loading/SplashScreen_cn", this.ui2DOverlayCamera.transform)) : this._CreateGO<SplashScreen>("Loading/SplashScreen_37", this.ui2DOverlayCamera.transform)) : this._CreateGO<SplashScreen>("Loading/SplashScreen_cn", this.ui2DOverlayCamera.transform);
      this.logoScreen = this._CreateGO<LogoScreen>("Prefab/UI/Common/LogoScreen", this.ui2DCamera.transform);
    }

    public void Init()
    {
      this.AutoOpen = false;
      if (this._isInited)
        return;
      this.InitBasicBundle();
      this.InitCameras();
      this.InitHuds();
      this.InitSceneUIs();
      this.InitDialogs();
      this.InitPopups();
      this.InitToast();
      this.InitSystemBlockers();
      this.ClearOcclusionStack();
      this._isInited = true;
    }

    public void InitPublicHud(System.Action<PublicHUD> callBack = null)
    {
      if ((UnityEngine.Object) null != (UnityEngine.Object) this._publicHUD)
      {
        if (callBack == null)
          return;
        callBack(this._publicHUD);
      }
      else
      {
        string path = string.Format("{0}{1}", (object) "Prefab/UI/Huds/", (object) "PublicHUD");
        this._CreateUIAsync(path, this.ui2DCamera.transform, (System.Action<UnityEngine.Object, bool>) ((arg1, arg2) =>
        {
          if (arg2)
          {
            GameObject gameObject = UnityEngine.Object.Instantiate(arg1) as GameObject;
            gameObject.transform.parent = this.ui2DCamera.transform;
            gameObject.transform.localScale = Vector3.one;
            gameObject.SetActive(false);
            this._publicHUD = gameObject.GetComponent<PublicHUD>();
            if (callBack != null)
              callBack(this._publicHUD);
          }
          AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        }));
      }
    }

    public void ShowConfirmationBox(string title, string content, string left, string right, ChooseConfirmationBox.ButtonState states, System.Action rightCallback, System.Action leftCallback, System.Action closeCallback)
    {
      UIManager.inst.priorityPopupQueue.OpenPopup("ChooseConfirmationBox", (Popup.PopupParameter) new ChooseConfirmationBox.Parameter()
      {
        title = title,
        message = content,
        yesOrOklabel_left = left,
        nolabel_right = right,
        buttonState = states,
        yesOrOkHandler_right = rightCallback,
        noHandler_left = leftCallback,
        closeHandler = closeCallback
      }, 100);
    }

    private void InitBasicBundle()
    {
    }

    private void InitCameras()
    {
      this.ui2DCamera.gameObject.SetActive(true);
      this.ui3DCamera.gameObject.SetActive(false);
      this.tileCamera.gameObject.SetActive(false);
    }

    private GameObject _CreateUI(string path, Transform parent)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(path, typeof (GameObject))) as GameObject;
      AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      gameObject.transform.parent = parent;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(false);
      return gameObject;
    }

    private void _CreateUIAsync(string path, Transform parent, System.Action<UnityEngine.Object, bool> callBack)
    {
      AssetManager.Instance.LoadAsync(path, callBack, typeof (GameObject));
    }

    private T _CreateUI<T>(string path, Transform parent) where T : UIControler
    {
      GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(path, typeof (GameObject))) as GameObject;
      AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      gameObject.transform.parent = parent;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(false);
      T component = gameObject.GetComponent<T>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        ;
      return component;
    }

    private T _CreateGO<T>(string path, Transform parent) where T : MonoBehaviour
    {
      GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load(path, typeof (GameObject))) as GameObject;
      AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      gameObject.transform.parent = parent;
      gameObject.transform.localScale = Vector3.one;
      gameObject.SetActive(false);
      T component = gameObject.GetComponent<T>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        ;
      return component;
    }

    public void Dispose()
    {
      this.DestroySystemBlockers();
      this.DestroyToast();
      this.DestroyPopups();
      this.DestroyDlgs();
      this.DestroyHuds();
      this.OnWindowClear = (System.Action) null;
      this.ClearOcclusionStack();
      this._isInited = false;
    }

    public void EnableUI2DTouchAndMouse()
    {
      this.ui2DCamera.GetComponent<UICamera>().enabled = true;
    }

    public void DisableUI2DTouchAndMouse()
    {
      this.ui2DCamera.GetComponent<UICamera>().enabled = false;
    }

    private void InitHuds()
    {
      this.timerHUD.InitTimerHud();
      this.timerHUD.Display = false;
      if (PlayerData.inst == null || PlayerData.inst.mail == null)
        return;
      PlayerData.inst.mail.publicHUD = this.publicHUD;
    }

    private void DestroyHuds()
    {
      using (Dictionary<string, Hud>.KeyCollection.Enumerator enumerator = this._huds.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this._huds[enumerator.Current].Destroy((UIControler.UIParameter) null);
      }
      this._huds.Clear();
      if ((UnityEngine.Object) this._publicHUD != (UnityEngine.Object) null)
      {
        this._publicHUD.Dispose();
        this._publicHUD.Destroy((UIControler.UIParameter) null);
      }
      if (!((UnityEngine.Object) this._timerHUD != (UnityEngine.Object) null))
        return;
      this._timerHUD.Dispose();
      this._timerHUD.Destroy((UIControler.UIParameter) null);
    }

    private T _CreateHudUI<T>(string hudType) where T : Hud
    {
      return this._CreateUI<T>(string.Format("{0}{1}", (object) "Prefab/UI/Huds/", (object) hudType), this.ui2DCamera.transform);
    }

    public PublicHUD publicHUD
    {
      get
      {
        if ((UnityEngine.Object) this._publicHUD == (UnityEngine.Object) null)
          this._publicHUD = this._CreateHudUI<PublicHUD>("PublicHUD");
        return this._publicHUD;
      }
    }

    public TimerHUD timerHUD
    {
      get
      {
        this.InitTimerHud();
        return this._timerHUD;
      }
    }

    public void InitTimerHud()
    {
      if (!((UnityEngine.Object) this._timerHUD == (UnityEngine.Object) null))
        return;
      this._timerHUD = this._CreateHudUI<TimerHUD>("TimerHud");
    }

    public SystemAnnouncement systemAnnouncement
    {
      get
      {
        if ((UnityEngine.Object) this._systemAnnouncement == (UnityEngine.Object) null)
        {
          this._systemAnnouncement = this._CreateGO<SystemAnnouncement>("Prefab/UI/Common/SystemAnnouncement", this.ui2DOverlayCamera.transform);
          UISprite component = this._systemAnnouncement.GetComponent<UISprite>();
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
            this._systemAnnouncement.transform.localPosition = new Vector3((float) component.width / 2f, 580.5f, 0.0f);
          else
            this._systemAnnouncement.transform.localPosition = new Vector3(850f, 580.5f, 0.0f);
        }
        return this._systemAnnouncement;
      }
    }

    public SystemMegaphone systemMegaphone
    {
      get
      {
        if ((UnityEngine.Object) this._systemMegaphone == (UnityEngine.Object) null)
        {
          this._systemMegaphone = this._CreateGO<SystemMegaphone>("Prefab/UI/Common/SystemMegaphone", this.ui2DOverlayCamera.transform);
          this._systemMegaphone.transform.localPosition = new Vector3(104f, 450f, 0.0f);
        }
        return this._systemMegaphone;
      }
    }

    public void ShowHud(string hudType, Hud.HUDPrarameter param = null)
    {
      if (!this._huds.ContainsKey(hudType))
      {
        Hud ui = this._CreateUI<Hud>(string.Format("{0}{1}", (object) "Prefab/UI/Huds/", (object) hudType), this.ui2DCamera.transform);
        if (!((UnityEngine.Object) ui != (UnityEngine.Object) null))
          return;
        this._huds.Add(hudType, ui);
        ui.Create((UIControler.UIParameter) null);
      }
      if (!((UnityEngine.Object) this._huds[hudType] != (UnityEngine.Object) null))
        return;
      this._huds[hudType].Open((UIControler.UIParameter) param);
      this._huds[hudType].Show((UIControler.UIParameter) null);
    }

    public void ShowAnnouncementMessage(string message)
    {
      this.systemAnnouncement.ShowMessage(message);
    }

    public void HideHud(string hudType)
    {
      if (!this._huds.ContainsKey(hudType))
        return;
      this._huds[hudType].Hide((UIControler.UIParameter) null);
    }

    public void SetPublicHudVisible(bool visible)
    {
      if (!(bool) ((UnityEngine.Object) this.publicHUD))
        return;
      this.publicHUD.gameObject.SetActive(visible);
    }

    public void ShowPublicHud()
    {
      this.publicHUD.Show((UIControler.UIParameter) null);
      if (this.onPublicHudStateChange == null)
        return;
      this.onPublicHudStateChange(true);
    }

    public void HidePublicHud()
    {
      this.publicHUD.Hide((UIControler.UIParameter) null);
      if (this.onPublicHudStateChange == null)
        return;
      this.onPublicHudStateChange(false);
    }

    public void OnLostDevice()
    {
      if (!(bool) ((UnityEngine.Object) this.publicHUD))
        return;
      this.publicHUD.gameObject.SetActive(false);
      this.publicHUD.gameObject.SetActive(true);
    }

    public void ShowTimerHud()
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
        return;
      this.timerHUD.Display = true;
    }

    public void HideTimerHud()
    {
      if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode && GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PitMode)
        return;
      this.timerHUD.Display = false;
    }

    private void InitDialogs()
    {
      this._cacheIgnoreList.Add("QuestDlg");
      this._cacheIgnoreList.Add("BuildingConstruction");
      this._cacheIgnoreList.Add("Construction/ConstructionSelectDlg");
      this._cacheIgnoreList.Add("Barracks/TrainTroopDlg");
      for (int index = 0; index < this._cacheIgnoreList.Count; ++index)
        this.CreateDlg(this._cacheIgnoreList[index]);
      this._cacheIgnoreList.Add("ChatDlg");
    }

    public void _CreateDlgUIAsync(string dialogType)
    {
      if (this._dialogs.ContainsKey(dialogType))
        return;
      string path = string.Format("{0}{1}", (object) "Prefab/UI/Dialogs/", (object) dialogType);
      this._CreateUIAsync(path, this.ui2DCamera.transform, (System.Action<UnityEngine.Object, bool>) ((arg1, arg2) =>
      {
        if (arg2 && !this._dialogs.ContainsKey(dialogType))
        {
          GameObject gameObject = UnityEngine.Object.Instantiate(arg1) as GameObject;
          gameObject.transform.parent = this.ui2DCamera.transform;
          gameObject.transform.localScale = Vector3.one;
          gameObject.SetActive(false);
          Dialog component = gameObject.GetComponent<Dialog>();
          component.dialogType = dialogType;
          this._dialogs.Add(dialogType, component);
          component.Create((UIControler.UIParameter) null);
        }
        AssetManager.Instance.UnLoadAsset(path, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
      }));
    }

    private T _CreateDlgUI<T>(string dialogType) where T : Dialog
    {
      return this._CreateUI<T>(string.Format("{0}{1}", (object) "Prefab/UI/Dialogs/", (object) dialogType), this.ui2DCamera.transform);
    }

    public void CreateDlg(string dialogType)
    {
      if (this._dialogs.ContainsKey(dialogType))
        return;
      Dialog dlgUi = this._CreateDlgUI<Dialog>(dialogType);
      if (!((UnityEngine.Object) dlgUi != (UnityEngine.Object) null))
        return;
      dlgUi.dialogType = dialogType;
      this._dialogs.Add(dialogType, dlgUi);
      dlgUi.Create((UIControler.UIParameter) null);
    }

    private void DestroyDlgs()
    {
      using (Dictionary<string, Dialog>.KeyCollection.Enumerator enumerator = this._dialogs.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this._dialogs[enumerator.Current].Destroy((UIControler.UIParameter) null);
      }
      this._dialogs.Clear();
      this._dialogStack.Clear();
    }

    public bool IsDlgExist(string dialogType)
    {
      if (this._dialogs.ContainsKey(dialogType))
        return (UnityEngine.Object) this._dialogs[dialogType] != (UnityEngine.Object) null;
      return false;
    }

    public int dialogStackCount
    {
      get
      {
        return this._dialogStack.Count;
      }
    }

    public T GetDlg<T>(string dialogType) where T : Dialog
    {
      if (this._dialogs.ContainsKey(dialogType))
        return this._dialogs[dialogType] as T;
      return (T) null;
    }

    public bool IsTheCurrentDialog(string dialogType)
    {
      if (this._dialogStack.Count == 0)
        return false;
      return this._dialogStack.Peek().dialogType.Equals(dialogType);
    }

    public bool IsTheCurrentDialog(Dialog dialog)
    {
      if (this._dialogStack.Count == 0)
        return false;
      return this._dialogStack.Peek().Equals((object) dialog);
    }

    public void OpenDlg(string dialogType, Dialog.DialogParameter param = null, bool lockInput = true, bool hideToast = true, bool hidePublichud = true)
    {
      if (!this._dialogs.ContainsKey(dialogType))
        this.CreateDlg(dialogType);
      Dialog dialog = this._dialogs[dialogType];
      if (this._dialogStack.Count > 0)
        this._dialogStack.Peek().Hide((UIControler.UIParameter) null);
      this._dialogStack.Push(dialog);
      dialog.Show((UIControler.UIParameter) param);
      if (lockInput)
        GameDataManager.inst.LockScrolling();
      if (hidePublichud)
      {
        this.HidePublicHud();
        this.HideTimerHud();
      }
      this.CloseKingdomTouchCircle();
      BuildingMoveManager.Instance.ExitBuildingMove();
    }

    public void BackToPreDlg(Dialog.DialogParameter showParam = null, Dialog.DialogParameter closeParam = null)
    {
      if (this._dialogStack.Count > 0)
      {
        Dialog t = this._dialogStack.Pop();
        if (this._dialogStack.Contains(t))
          t.Hide((UIControler.UIParameter) closeParam);
        else if (!this._cacheIgnoreList.Contains(t.dialogType))
        {
          this._dialogs.Remove(t.dialogType);
          t.Destroy((UIControler.UIParameter) closeParam);
        }
        else
          t.Close((UIControler.UIParameter) closeParam);
      }
      if (this._dialogStack.Count > 0)
        this._dialogStack.Peek().Show((UIControler.UIParameter) showParam);
      else
        this.CloseDlg(closeParam);
    }

    public void CloseDlg(Dialog.DialogParameter param = null)
    {
      while (this._dialogStack.Count > 0)
      {
        Dialog dialog = this._dialogStack.Pop();
        if (!this._cacheIgnoreList.Contains(dialog.dialogType))
        {
          this._dialogs.Remove(dialog.dialogType);
          dialog.Destroy((UIControler.UIParameter) param);
        }
        else
          dialog.Close((UIControler.UIParameter) param);
      }
      if (this.OnWindowClear != null)
        this.OnWindowClear();
      GameDataManager.inst.UnlockScrolling();
      if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
        CitadelSystem.inst.MoveCameraBackToNormal();
      else if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode)
        PVPSystem.Instance.Map.BackToOriStats();
      this.ShowPublicHud();
      this.ShowTimerHud();
      Utils.ForceUnloadAssets();
    }

    public bool ExistAutoOpened
    {
      get
      {
        using (Dictionary<int, Popup>.Enumerator enumerator = this._popups.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.Value.isAutoOpen)
              return true;
          }
        }
        using (Dictionary<string, Dialog>.Enumerator enumerator = this._dialogs.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.Value.isAutoOpen)
              return true;
          }
        }
        return false;
      }
    }

    [HideInInspector]
    public int popupMaxPanelDepth
    {
      get
      {
        if (this._popupStack.Count == 0)
          return 60;
        return this._popups[this._popupStack.Peek()].maxPanelDepth;
      }
    }

    public int popupZBlockDepth
    {
      set
      {
        if (!((UnityEngine.Object) this.PopupZBlocker != (UnityEngine.Object) null))
          return;
        this.PopupZBlocker.depth = value;
      }
      get
      {
        if ((UnityEngine.Object) this.PopupZBlocker != (UnityEngine.Object) null)
          return this.PopupZBlocker.depth;
        return 0;
      }
    }

    private void InitPopups()
    {
      GameObject ui = this._CreateUI("Prefab/UI/Common/z_Blocker", this.ui2DOverlayCamera.transform);
      ui.name = "Popup zBlocker";
      ui.GetComponent<UIPanel>().depth = 60;
      this.PopupZBlocker = ui.GetComponent<UIPanel>();
      foreach (UIEventTrigger componentsInChild in ui.GetComponentsInChildren<UIEventTrigger>(true))
        componentsInChild.onClick.Add(new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.CloseTopPopup(new Popup.PopupParameter()))));
      ui.SetActive(false);
      this.ManualZBlocker = this._CreateUI("Prefab/UI/Common/z_Blocker", this.ui2DOverlayCamera.transform);
      this.ManualZBlocker.name = "Manual Blocker";
      this.ManualZBlocker.GetComponent<UIPanel>().depth = 90;
      this.ManualZBlocker.SetActive(false);
    }

    public void ShowManualBlocker()
    {
      if (!(bool) ((UnityEngine.Object) this.ManualZBlocker))
        return;
      this.ManualZBlocker.SetActive(true);
    }

    public void HideManualBlocker()
    {
      if (!(bool) ((UnityEngine.Object) this.ManualZBlocker))
        return;
      this.ManualZBlocker.SetActive(false);
    }

    public bool IsPopupExist(string popupType)
    {
      if (this._popups != null)
      {
        Dictionary<int, Popup>.ValueCollection.Enumerator enumerator = this._popups.Values.GetEnumerator();
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Type == popupType)
            return true;
        }
      }
      return false;
    }

    private T _CreatePopupUI<T>(string popupType, int inID) where T : Popup
    {
      T ui = this._CreateUI<T>(string.Format("{0}{1}", (object) "Prefab/UI/Popups/", (object) popupType), this.ui2DOverlayCamera.transform);
      ui.ID = inID;
      ui.popupType = popupType;
      return ui;
    }

    private void DestroyPopups()
    {
      Popup[] array = new Popup[this._popups.Count];
      this._popups.Values.CopyTo(array, 0);
      for (int index = 0; index < array.Length; ++index)
        array[index].Destroy((UIControler.UIParameter) null);
      this._popups.Clear();
      this._popupStack.Clear();
      if (!(bool) ((UnityEngine.Object) this.PopupZBlocker))
        return;
      this.PopupZBlocker.gameObject.SetActive(false);
    }

    public Popup OpenPopup(string popupType, Popup.PopupParameter param = null)
    {
      if (this._popupStack.Count > 20)
        D.warn((object) "Popup count is more than MAX_POPUP_COUNT define");
      if ((bool) ((UnityEngine.Object) this.PopupZBlocker))
        this.PopupZBlocker.gameObject.SetActive(true);
      Popup popupUi = this._CreatePopupUI<Popup>(popupType, this._LocalPopupID++);
      if ((UnityEngine.Object) popupUi != (UnityEngine.Object) null)
      {
        popupUi.ResetPanelDepth(this.popupMaxPanelDepth + 2);
        popupUi.Open((UIControler.UIParameter) param);
        popupUi.Show((UIControler.UIParameter) param);
        this._popupStack.Push(popupUi.ID);
        this._popups.Add(popupUi.ID, popupUi);
        this.popupZBlockDepth = popupUi.basePanelDepth - 1;
      }
      this.CloseKingdomTouchCircle();
      BuildingMoveManager.Instance.ExitBuildingMove();
      return popupUi;
    }

    public void ClosePopup(int popupID, Popup.PopupParameter param = null)
    {
      if (this._popups.ContainsKey(popupID) && this._popupStack.Count > 0)
      {
        if (this._popupStack.Peek() == popupID)
        {
          this._popupStack.Pop();
          this.UpdatePopupZBlockDepth();
          this._popups[popupID].Destroy((UIControler.UIParameter) param);
          this._popups.Remove(popupID);
        }
        else
        {
          Stack<int> intStack = new Stack<int>();
          while (this._popupStack.Count > 0)
          {
            if (this._popupStack.Peek() == popupID)
            {
              this._popupStack.Pop();
              break;
            }
            intStack.Push(this._popupStack.Pop());
          }
          while (intStack.Count > 0)
            this._popupStack.Push(intStack.Pop());
          this.UpdatePopupZBlockDepth();
          this._popups[popupID].Destroy((UIControler.UIParameter) param);
          this._popups.Remove(popupID);
        }
      }
      if (this._popupStack.Count == 0)
      {
        if ((bool) ((UnityEngine.Object) this.PopupZBlocker))
          this.PopupZBlocker.gameObject.SetActive(false);
        if (this.onAllPopupClosed != null)
          this.onAllPopupClosed(true);
        if (this.OnWindowClear != null && this._dialogStack.Count == 0)
          this.OnWindowClear();
      }
      Utils.ForceUnloadAssets();
    }

    private void UpdatePopupZBlockDepth()
    {
      if (this._popupStack.Count <= 0)
        return;
      this.popupZBlockDepth = this._popups[this._popupStack.Peek()].basePanelDepth - 1;
    }

    public void CloseTopPopup(Popup.PopupParameter param = null)
    {
      if (this._popupStack.Count == 0)
        return;
      this.ClosePopup(this._popupStack.Peek(), param);
    }

    public void CloseAllPopup(Popup.PopupParameter param = null)
    {
      while (this._popupStack.Count > 0)
        this._popups[this._popupStack.Pop()].Destroy((UIControler.UIParameter) param);
      this._popups.Clear();
      this.PopupZBlocker.gameObject.SetActive(false);
    }

    public int CheckTopPopup()
    {
      if (this._popupStack != null && this._popupStack.Count > 0)
        return this._popupStack.Peek();
      return 0;
    }

    public bool IsTopPopup(int popupId)
    {
      if (popupId == 0)
        return false;
      return this.CheckTopPopup() == popupId;
    }

    public void OpenIgnorableMessageBox(IgnorableMessageBoxPopup.Parameter parameter)
    {
      if (IgnorableMessageBoxPopup.IsIgnore(parameter.key))
      {
        if (parameter.onLeftBtnClick == null)
          return;
        parameter.onLeftBtnClick();
      }
      else
        UIManager.inst.OpenPopup("IgnorableMessageBox", (Popup.PopupParameter) parameter);
    }

    public ToastManager toastManager
    {
      get
      {
        if ((UnityEngine.Object) this._toastManager == (UnityEngine.Object) null)
          this.InitToast();
        return this._toastManager;
      }
    }

    public int popupStackCount
    {
      get
      {
        return this._popupStack.Count;
      }
    }

    private void InitToast()
    {
      this.toast = this._CreateGO<OverlayToast>("Prefab/UI/Common/OverlayToast", this.ui2DOverlayCamera.transform);
      this.toast.gameObject.SetActive(true);
      this._toastManager = ToastManager.Instance;
    }

    private void DestroyToast()
    {
      this._toastManager = (ToastManager) null;
    }

    public void ShowBasicToast(string toastID)
    {
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this._toastManager))
        return;
      this._toastManager.AddBasicToast(toastID);
    }

    public void ShowToast(string toastType, object args)
    {
      if (!((UnityEngine.Object) null != (UnityEngine.Object) this._toastManager))
        return;
      this._toastManager.AddToast(toastType, args);
    }

    private void InitSceneUIs()
    {
    }

    public void OpenCityTouchCircle(Transform parent)
    {
      CityTouchCircle.Instance.Open(parent);
    }

    public void CloseCityTouchCircle()
    {
      if (!CityTouchCircle.IsAvailable)
        return;
      CityTouchCircle.Instance.Close();
    }

    public void OpenKingdomArmyCircle(long marchId, Transform CircleButtonParent = null)
    {
      March.March march = GameEngine.Instance.marchSystem.marches[marchId];
      if (march == null || (UnityEngine.Object) march.troopsView == (UnityEngine.Object) null)
      {
        UIManager.inst.CloseDlg((Dialog.DialogParameter) null);
      }
      else
      {
        if (march.marchData == null)
          return;
        MarchData marchData = march.marchData;
        UIManager.inst.CloseKingdomTouchCircle();
        KingdomCirclePara para = KingdomTouchCircle.Instance.Para;
        para.mode = CircleMode.Troop;
        para.marchData = march.marchData;
        if (march.marchData.ownerUid == PlayerData.inst.uid && march.marchData.type != MarchData.MarchType.trade && march.marchData.type != MarchData.MarchType.scout && march.marchData.troopsInfo.totalCount > 0)
        {
          CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "march_troops", new EventDelegate((EventDelegate.Callback) (() => this.OnViewMarchDetail(marchId))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara);
        }
        if (march.marchData.ownerUid != PlayerData.inst.uid)
        {
          NPC_Info dataByUid = ConfigManager.inst.DB_NPC.GetDataByUid(march.marchData.ownerUid);
          if (dataByUid != null && "fallen_knight" == dataByUid.type)
          {
            CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_profile", "march_troops", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("MarchDetailPopUp", (Popup.PopupParameter) new MarchDetailPopUp.Parameter()
            {
              marchId = marchId
            }))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
            para.tList.Add(cityCircleBtnPara);
          }
        }
        if (marchData.ownerUid == PlayerData.inst.uid && marchData.type != MarchData.MarchType.rally && (marchData.type != MarchData.MarchType.rally_attack && marchData.type != MarchData.MarchType.gve_rally) && (marchData.type != MarchData.MarchType.gve_rally_attack && marchData.type != MarchData.MarchType.rab_rally && marchData.type != MarchData.MarchType.rab_rally_attack))
        {
          CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_city_speedup", "march_speedup", new EventDelegate((EventDelegate.Callback) (() => UIManager.inst.OpenPopup("MarchSpeedUpPopup", (Popup.PopupParameter) new MarchSpeedUpPopup.Parameter()
          {
            marchData = DBManager.inst.DB_March.Get(marchId)
          }))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara);
        }
        if (marchData.IsExpedition && marchData.ownerUid == PlayerData.inst.uid && (marchData.type != MarchData.MarchType.rally && marchData.type != MarchData.MarchType.gve_rally) && (marchData.type != MarchData.MarchType.rab_rally && marchData.type != MarchData.MarchType.scout))
        {
          CityCircleBtnPara cityCircleBtnPara = new CityCircleBtnPara(AtlasType.Gui_2, "icon_kingdom_call_back", "march_callback", new EventDelegate((EventDelegate.Callback) (() => GameEngine.Instance.marchSystem.Recall(marchId))), true, (string) null, (GameObject) null, (CityTouchCircleBtnAttachment.AttachmentData) null, false);
          para.tList.Add(cityCircleBtnPara);
        }
        if ((UnityEngine.Object) CircleButtonParent == (UnityEngine.Object) null)
          UIManager.inst.OpenKingdomTouchCircle(march.troopsView._circleBtnParent);
        else
          UIManager.inst.OpenKingdomTouchCircle(CircleButtonParent);
      }
    }

    public void OnViewMarchDetail(long marchId)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(marchId);
      if (marchData.type == MarchData.MarchType.gve_rally_attack || marchData.type == MarchData.MarchType.rally_attack || marchData.type == MarchData.MarchType.rab_rally_attack)
        PlayerData.inst.allianceWarManager.LoadWarDetalData(marchData.rallyId, PlayerData.inst.allianceId, (System.Action<long>) (rallyId =>
        {
          RallyData rallyData = DBManager.inst.DB_Rally.Get(rallyId);
          if (rallyData == null)
            return;
          AllianceWarManager.WarData data = PlayerData.inst.allianceWarManager.GetData(rallyId);
          KingdomWarMarchPopup.Parameter parameter = new KingdomWarMarchPopup.Parameter();
          parameter.marchCaptainId = marchData.ownerUid;
          parameter.troopCapacity = data.rallyCapacity;
          Dictionary<long, long> dictionary = new Dictionary<long, long>();
          Dictionary<long, RallySlotInfo>.ValueCollection.Enumerator enumerator = rallyData.slotsInfo_joined.Values.GetEnumerator();
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.marchId > 0L)
            {
              MarchData marchData1 = DBManager.inst.DB_March.Get(enumerator.Current.marchId);
              if (marchData1 != null)
                dictionary.Add(marchData1.ownerUid, marchData1.marchId);
            }
          }
          if (dictionary.Count <= 0)
            return;
          parameter.troops = dictionary;
          UIManager.inst.OpenPopup("Wonder/KingdomWarMarchPopup", (Popup.PopupParameter) parameter);
        }));
      else
        UIManager.inst.OpenPopup("MarchDetailPopUp", (Popup.PopupParameter) new MarchDetailPopUp.Parameter()
        {
          marchId = marchId
        });
    }

    public void OpenKingdomTouchCircle(Transform parent)
    {
      KingdomTouchCircle.Instance.Open(parent);
    }

    public void CloseKingdomTouchCircle()
    {
      if (!KingdomTouchCircle.IsAvailable)
        return;
      KingdomTouchCircle.Instance.Close();
    }

    private SystemBlocker _CreateSystemBlocker(string systemBlockType)
    {
      SystemBlocker systemBlocker;
      if (!this._systemBlockers.ContainsKey(systemBlockType))
      {
        systemBlocker = this._CreateUI<SystemBlocker>(string.Format("{0}{1}", (object) "Prefab/UI/System_Blockers/", (object) systemBlockType), this.ui2DOverlayCamera.transform);
        this._systemBlockers.Add(systemBlockType, systemBlocker);
      }
      else
        systemBlocker = this._systemBlockers[systemBlockType];
      return systemBlocker;
    }

    private void InitSystemBlockers()
    {
      this._CreateSystemBlocker("FullScreenWait");
      this._CreateSystemBlocker("CommonBlocker");
    }

    private void DestroySystemBlockers()
    {
      using (Dictionary<string, SystemBlocker>.KeyCollection.Enumerator enumerator = this._systemBlockers.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this._systemBlockers[enumerator.Current].Destroy((UIControler.UIParameter) null);
      }
      this._systemBlockers.Clear();
      this._systemBlockers = (Dictionary<string, SystemBlocker>) null;
    }

    public T GetSystemBlocker<T>(string systemBlockerType) where T : SystemBlocker
    {
      return this._systemBlockers[systemBlockerType] as T;
    }

    public void ShowSystemBlocker(string SystemBlockType, SystemBlocker.SystemBlockerParameter orgParam = null)
    {
      if (!this._systemBlockers.ContainsKey(SystemBlockType) || this._systemBlockers[SystemBlockType].gameObject.activeSelf)
        return;
      this._systemBlockers[SystemBlockType].Show((UIControler.UIParameter) orgParam);
      if (!(SystemBlockType == "FullScreenWait"))
        return;
      GameEngine.Instance.LockEscape();
    }

    public GameObject GetBlocker(string SystemBlockType)
    {
      if (this._systemBlockers.ContainsKey(SystemBlockType))
        return this._systemBlockers[SystemBlockType].gameObject;
      return (GameObject) null;
    }

    public void HideSystemBlocker(string SystemBlockType, SystemBlocker.SystemBlockerParameter orgParam = null)
    {
      if (!this._systemBlockers.ContainsKey(SystemBlockType) || !this._systemBlockers[SystemBlockType].gameObject.activeSelf)
        return;
      this._systemBlockers[SystemBlockType].Hide((UIControler.UIParameter) orgParam);
      if (SystemBlockType == "FullScreenWait")
        GameEngine.Instance.UnLockEscape();
      if (UIManager.inst.popupStackCount == 0 && !this.HasSystemBlocker)
        TutorialManager.Instance.Resume();
      if (TutorialManager.Instance.IsRunning || this.HasSystemBlocker || this.onAllSystemBlockerClosed == null)
        return;
      this.onAllSystemBlockerClosed(true);
    }

    public void ShowNetErrorBox(string title, string content, string left, string right, NetErrorBlocker.ButtonState states, System.Action rightCallback, System.Action leftCallback, System.Action closeCallback)
    {
      this.ShowSystemBlocker("NetErrorBlocker", (SystemBlocker.SystemBlockerParameter) new NetErrorBlocker.Parameter()
      {
        title = title,
        message = content,
        yesOrOklabel_left = left,
        nolabel_right = right,
        buttonState = states,
        yesOrOkHandler_right = rightCallback,
        noHandler_left = leftCallback,
        closeHandler = closeCallback
      });
    }

    public void HideNetErrorBox()
    {
      this.HideSystemBlocker("NetErrorBlocker", (SystemBlocker.SystemBlockerParameter) null);
    }

    public bool HasSystemBlocker
    {
      get
      {
        bool flag = false;
        using (Dictionary<string, SystemBlocker>.Enumerator enumerator = this._systemBlockers.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.Value.gameObject.activeSelf)
            {
              flag = true;
              break;
            }
          }
        }
        return flag;
      }
    }

    public void RegisterEvents()
    {
      this.startRallyPort.AddEvent(new System.Action<object>(this.OnRallyStart));
      this.beRallyPort.AddEvent(new System.Action<object>(this.OnBeRallyed));
      this.cancelRallyPort.AddEvent(new System.Action<object>(this.OnRallyCancel));
      this.systemAnnoucementPort.AddEvent(new System.Action<object>(this.OnAnnoucementRecived));
      this.systemMegaphonePort.AddEvent(new System.Action<object>(this.OnSystemMegaphoneRecived));
      this.megaphonePort.AddEvent(new System.Action<object>(this.OnMegahoneRecived));
      this.digsetCreatePort.AddEvent(new System.Action<object>(this.OnDigsetCreated));
      this.activtyCreatePort.AddEvent(new System.Action<object>(this.OnActivityStart));
    }

    public void UnregisterEvents()
    {
      this.startRallyPort.RemoveEvent(new System.Action<object>(this.OnRallyStart));
      this.beRallyPort.RemoveEvent(new System.Action<object>(this.OnBeRallyed));
      this.cancelRallyPort.RemoveEvent(new System.Action<object>(this.OnRallyCancel));
      this.systemAnnoucementPort.RemoveEvent(new System.Action<object>(this.OnAnnoucementRecived));
      this.systemMegaphonePort.RemoveEvent(new System.Action<object>(this.OnSystemMegaphoneRecived));
      this.megaphonePort.RemoveEvent(new System.Action<object>(this.OnMegahoneRecived));
      this.digsetCreatePort.RemoveEvent(new System.Action<object>(this.OnDigsetCreated));
      this.activtyCreatePort.RemoveEvent(new System.Action<object>(this.OnActivityStart));
    }

    public void OnActivityStart(object data)
    {
      ActivityManager.Intance.HasNewActivty = true;
    }

    public void OnRallyStart(object data)
    {
      Hashtable data1;
      long result;
      if (DatabaseTools.CheckAndParseOrgData(data, out data1) && long.TryParse(data1[(object) "rally_id"].ToString(), out result))
      {
        RallyData rallyData = DBManager.inst.DB_Rally.Get(result);
        if (rallyData != null)
        {
          if (rallyData.worldId != PlayerData.inst.userData.current_world_id)
            return;
          CityData cityData1 = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
          CityData cityData2 = DBManager.inst.DB_City.Get(rallyData.targetCityId);
          if (cityData1 != null && cityData2 != null)
          {
            this.toastManager.AddBasicToast("toast_march_rally_started", "K: X: Y:", MapUtils.GetCoordinateStringWithoutKXY(cityData1.cityLocation.K, cityData1.cityLocation.X, cityData1.cityLocation.Y), "City_Name", cityData1.cityName, "k: x: y:", MapUtils.GetCoordinateStringWithoutKXY(cityData2.cityLocation.K, cityData2.cityLocation.X, cityData2.cityLocation.Y), "City_Name_2", cityData2.cityName);
            return;
          }
        }
      }
      this.toastManager.AddBasicToast("toast_march_rally_started");
    }

    public void OnBeRallyed(object data)
    {
      Hashtable data1;
      long result;
      if (DatabaseTools.CheckAndParseOrgData(data, out data1) && long.TryParse(data1[(object) "rally_id"].ToString(), out result))
      {
        RallyData rallyData = DBManager.inst.DB_Rally.Get(result);
        if (rallyData != null)
        {
          CityData cityData1 = DBManager.inst.DB_City.Get(rallyData.ownerCityId);
          CityData cityData2 = DBManager.inst.DB_City.Get(rallyData.targetCityId);
          if (cityData1 != null && cityData2 != null)
          {
            if (rallyData.targetUid == PlayerData.inst.uid)
            {
              if (WatchtowerUtilities.GetWatchtowerLevel() < 5)
                return;
              this.toastManager.AddBasicToast("toast_march_rally_started_passive", "K: X: Y:", MapUtils.GetCoordinateStringWithoutKXY(cityData2.cityLocation.K, cityData2.cityLocation.X, cityData2.cityLocation.Y), "City_Name", cityData2.cityName);
              return;
            }
            this.toastManager.AddBasicToast("toast_march_rally_started", "K: X: Y:", MapUtils.GetCoordinateStringWithoutKXY(cityData1.cityLocation.K, cityData1.cityLocation.X, cityData1.cityLocation.Y), "City_Name", cityData1.cityName, "k: x: y:", MapUtils.GetCoordinateStringWithoutKXY(cityData2.cityLocation.K, cityData2.cityLocation.X, cityData2.cityLocation.Y), "City_Name_2", cityData2.cityName);
            return;
          }
        }
      }
      this.toastManager.AddBasicToast("toast_march_rally_started");
    }

    public void OnRallyCancel(object data)
    {
      Hashtable data1 = data as Hashtable;
      long id = 0;
      bool flag = false;
      if (data1 != null)
      {
        flag = data1.Contains((object) "need_toast");
        if (flag && data1[(object) "need_toast"].ToString() == "0")
          flag = false;
        if (data1.Contains((object) "rally_id"))
          id = long.Parse(data1[(object) "rally_id"].ToString());
      }
      RallyData rallyData = DBManager.inst.DB_Rally.Get(id);
      if (rallyData != null)
        flag = rallyData.worldId == PlayerData.inst.userData.current_world_id;
      long result1;
      long result2;
      if (DatabaseTools.CheckAndParseOrgData(data, out data1) && long.TryParse(data1[(object) "uid"].ToString(), out result1) && long.TryParse(data1[(object) "opp_uid"].ToString(), out result2))
      {
        if (!flag)
          return;
        CityData cityData1 = DBManager.inst.DB_City.Get(result1);
        CityData cityData2 = DBManager.inst.DB_City.Get(result2);
        if (cityData1 != null && cityData2 != null)
        {
          this.toastManager.AddBasicToast("toast_march_rally_canceled", "K: X: Y:", MapUtils.GetCoordinateStringWithoutKXY(cityData1.cityLocation.K, cityData1.cityLocation.X, cityData1.cityLocation.Y), "City_Name", cityData1.cityName, "k: x: y:", MapUtils.GetCoordinateStringWithoutKXY(cityData2.cityLocation.K, cityData2.cityLocation.X, cityData2.cityLocation.Y), "City_Name_2", cityData2.cityName);
          return;
        }
      }
      if (data1 == null || !flag)
        return;
      this.toastManager.AddBasicToast("toast_march_rally_canceled");
    }

    public void OnAnnoucementRecived(object data)
    {
      Hashtable hashtable = data as Hashtable;
      if (hashtable.ContainsKey((object) PlayerData.inst.userData.language))
      {
        this.systemAnnouncement.ShowMessage((hashtable[(object) PlayerData.inst.userData.language] as Hashtable)[(object) "text"].ToString());
      }
      else
      {
        if (!hashtable.ContainsKey((object) "en"))
          return;
        this.systemAnnouncement.ShowMessage((hashtable[(object) "en"] as Hashtable)[(object) "text"].ToString());
      }
    }

    public void OnSystemMegaphoneRecived(object data)
    {
      if (!GameEngine.IsReady())
        return;
      Hashtable hashtable = data as Hashtable;
      if (!hashtable.ContainsKey((object) "notice_key"))
        return;
      string str = hashtable[(object) "notice_key"].ToString();
      if (hashtable.Contains((object) "params"))
      {
        ArrayList arrayList = hashtable[(object) "params"] as ArrayList;
        Dictionary<string, string> para = new Dictionary<string, string>();
        if (arrayList != null)
        {
          for (int index = 0; index < arrayList.Count; ++index)
            para.Add(index.ToString(), Utils.XLAT(arrayList[index].ToString()));
          this.systemAnnouncement.ShowMessage(ScriptLocalization.GetWithPara(str, para, true));
        }
        else
          this.systemAnnouncement.ShowMessage(Utils.XLAT(str));
      }
      else
        this.systemAnnouncement.ShowMessage(Utils.XLAT(str));
      if (!hashtable.ContainsKey((object) "annv_end_time") || hashtable[(object) "annv_end_time"] == null)
        return;
      string s = hashtable[(object) "annv_end_time"].ToString();
      int result = 0;
      int.TryParse(s, out result);
      if (result <= 0)
        return;
      AnniversaryManager.Instance.EndTime = result;
    }

    public void OnMegahoneRecived(object data)
    {
      if (!GameEngine.IsReady())
        return;
      Hashtable hashtable = data as Hashtable;
      if (!hashtable.ContainsKey((object) "name") || !hashtable.ContainsKey((object) "content"))
        return;
      this.systemMegaphone.ShowMessage(hashtable[(object) "name"].ToString(), hashtable[(object) "content"].ToString());
    }

    public void OnDigsetCreated(object orgData)
    {
      Hashtable inData = orgData as Hashtable;
      if (inData == null)
        return;
      long outData = 0;
      DatabaseTools.UpdateData(inData, "uid", ref outData);
      if (outData != PlayerData.inst.uid)
        return;
      ToastManager.Instance.AddBasicToast("toast_treasure_map_treasure_discovered");
    }

    public void OnEnable()
    {
      this.RegisterEvents();
      UIButton.onAnyButtonClick += new System.Action(this.OnAnyButtonClick);
    }

    public void OnDisable()
    {
      this.UnregisterEvents();
      UIButton.onAnyButtonClick -= new System.Action(this.OnAnyButtonClick);
    }

    public bool IsDialogClear
    {
      get
      {
        return this._dialogStack.Count == 0;
      }
    }

    public bool IsPopupClear
    {
      get
      {
        return this._popupStack.Count == 0;
      }
    }

    public bool OnEscapeHandler()
    {
      if (this._popupStack.Count > 0)
      {
        this.CloseTopPopup((Popup.PopupParameter) null);
        return true;
      }
      if (this._dialogStack.Count <= 0)
        return false;
      Dialog t = this._dialogStack.Pop();
      this._dialogStack.Push(t);
      if (this._dialogStack.Count > 1)
        t.OnBackKeyPress();
      else
        t.OnCloseKeyPress();
      return true;
    }

    public void OnAnyButtonClick()
    {
      if (!GameEngine.IsReady())
        return;
      AudioManager.Instance.PlaySound("sfx_ui_click", false);
    }

    public void ShowEquipmentSuitBenefitChangeTip(List<Benefits.BenefitValuePair> allAddedBenifit, List<Benefits.BenefitValuePair> allRemovedBenefit, ConfigEquipmentSuitEnhanceInfo addEquipmentSuitEnhance, ConfigEquipmentSuitEnhanceInfo delEquipmentSuitEnhance)
    {
      if (!(bool) ((UnityEngine.Object) this._equipmentSuitBenefitTip))
      {
        GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentSuitBenifitTip", (System.Type) null)) as GameObject;
        AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentSuitBenifitTip", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
        gameObject.transform.SetParent(this.ui2DOverlayCamera.transform, false);
        gameObject.transform.localScale = Vector3.one;
        gameObject.transform.localPosition = Vector3.zero;
        this._equipmentSuitBenefitTip = gameObject.GetComponent<EquipmentSuitBenefitTip>();
      }
      this._equipmentSuitBenefitTip.Play(allRemovedBenefit, allAddedBenifit, addEquipmentSuitEnhance, delEquipmentSuitEnhance);
    }

    public void ClearOcclusionStack()
    {
      this._occlusionCallStack = -1;
    }

    public void SetOcclusion(bool state)
    {
      if (state)
        ++this._occlusionCallStack;
      if (this._occlusionCallStack == 0)
      {
        bool flag = false;
        if ((UnityEngine.Object) this.cityCamera != (UnityEngine.Object) null && GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.CityMode)
        {
          flag = !state;
          this.cityCamera.gameObject.SetActive(flag);
        }
        if ((UnityEngine.Object) this.tileCamera != (UnityEngine.Object) null && (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PVPMode || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode))
        {
          flag = !state;
          this.tileCamera.gameObject.SetActive(flag);
        }
        if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.DragonKnight || GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.MerlinTowerMode)
          flag = !state;
        this.ui2DCamera.clearFlags = flag ? CameraClearFlags.Depth : CameraClearFlags.Color;
      }
      if (state)
        return;
      --this._occlusionCallStack;
    }

    public struct HudType
    {
      public const string PUBLIC_HUD = "PublicHUD";
      public const string TIMER_HUD = "TimerHud";
      public const string SYSTEM_ANNOUNCEMENT = "SystemAnnouncement";
      public const string SYSTEM_MEGAHONE = "SystemMegaphone";
    }

    public struct DialogType
    {
      public const string AccountDialog = "Account/AccountDialog";
      public const string chatDlg = "ChatDlg";
      public const string heroDlg = "HeroDlg";
      public const string blacksmithDlg = "BlackSmithDlg";
      public const string gamblingDlg = "gamblingDlg";
      public const string skillsDlg = "SkillsDlg";
      public const string questDlg = "QuestDlg";
      public const string questEmpireDetailsDlg = "QuestEmpireDetailsDlg";
      public const string runesShopDlg = "RunesShopDlg";
      public const string hospitalDlg = "HospitalHealDlg";
      public const string troopTrainDlg = "Barracks/TrainTroopDlg";
      public const string TroopLevelUpDlg = "Barracks/TrainTroopConvertDlg";
      public const string trainingDlg = "TrainingDlg";
      public const string casinoDlg = "Casino/CasinoDlg";
      public const string casinoStoreDlg = "Casino/CasinoStoreDlg";
      public const string casinoPickCardDlg = "Casino/CasinoPickCardDlg";
      public const string casinoJackpotDlg = "Jackpot/JackpotDlg";
      public const string LotteryDiyPickDlg = "LotteryDiy/LotteryDiyPickDlg";
      public const string LotteryDiyPlayDlg = "LotteryDiy/LotteryDiyPlayDlg";
      public const string MarksmanPickDlg = "Marksman/MarksmanPickDlg";
      public const string MarksmanPlayDlg = "Marksman/MarksmanPlayDlg";
      public const string DicePlayDlg = "Dice/DicePlayDlg";
      public const string barracksInfoDlg = "BuildingInfoDlg_Barracks";
      public const string farmInfoDlg = "BuildingInfoDlg_Farm";
      public const string lumberMillInfoDlg = "BuildingInfoDlg_LumberMill";
      public const string mineInfoDlg = "BuildingInfoDlg_Mine";
      public const string houseInfoDlg = "BuildingInfoDlg_House";
      public const string blacksmithInfoDlg = "BuildingInfoDlg_Blacksmith";
      public const string embassyInfoDlg = "BuildingInfoDlg_Embassy";
      public const string heroTowerInfoDlg = "BuildingInfoDlg_HeroTower";
      public const string hospitalInfoDlg = "BuildingInfoDlg_Hospital";
      public const string marketplaceInfoDlg = "BuildingInfoDlg_Marketplace";
      public const string prisonInfoDlg = "BuildingInfoDlg_Prison";
      public const string storehouseInfoDlg = "BuildingInfoDlg_Storehouse";
      public const string universityInfoDlg = "BuildingInfoDlg_University";
      public const string warRallyInfoDlg = "BuildingInfoDlg_WarRally";
      public const string watchTowerInfoDlg = "BuildingInfoDlg_Watchtower";
      public const string strongholdInfoDlg = "BuildingInfoStronghold";
      public const string CityInfoDlg = "CityInfoDlg";
      public const string CityInfoRename = "CityInfoRename";
      public const string wallsInfoDlg = "BuildingInfoDlg_Wall";
      public const string pveInfoDlg = "BuildingInfoDlg_PVEPortal";
      public const string paradeGroundDlg = "ParadeGroundDlg";
      public const string sendMarchDlg = "MarchAllocDlg";
      public const string resetMarchDlg = "MarchResetDlg";
      public const string marchList = "MarchListDlg";
      public const string marchDetail = "MarchDetailDlg";
      public const string marchRecallDlg = "MarchRecallDlg";
      public const string marchInfoDlg = "MarchInfoDlg";
      public const string hallOfWarDlg = "HallOfWarDlg";
      public const string embassyDlg = "EmbassyDlg";
      public const string rallyDlg = "RallyDlg";
      public const string defenseDlg = "DefenseDlg";
      public const string ActivityMainDlg = "Activity/ActivityMainDlg";
      public const string ActivityDetailDlg = "Activity/ActivityDetailDlg";
      public const string AllianceActivityDetailDlg = "Activity/AllianceEventlDlg";
      public const string PersonalActivityDetailDlg = "Activity/PersonalActivityDetailDlg";
      public const string ActivityDetailDlgWithoutRank = "Activity/ActivityDetailDlgWithoutRank";
      public const string ActivityFestivalDlg = "Activity/FestivalEventDlg";
      public const string ActivityFestivalVisitDlg = "FestivalVisitDlg";
      public const string ActivityFallenKnightDlg = "Activity/FallenKnightEntryDlg";
      public const string ActivityFallenKnightRewardDlg = "Activity/FallenKnightRewardsDetailDlg";
      public const string SevenDaysEventDlg = "Activity/SevenDaysEventDlg";
      public const string itemStoreDlg = "ItemStore/ItemStoreDlg";
      public const string mailDlg = "Mail/MailMainDlg";
      public const string mailComposeDlg = "Mail/MailComposeDlg";
      public const string mailHistrotyDlg = "Mail/MailHistroryDlg";
      public const string researchDlg = "Research/ResearchBaseDlg";
      public const string researchTreeDlg = "Research/ResearchTreeDlg";
      public const string researchDetailDlg = "Research/ResearchDetailDlg";
      public const string researchDetailInfoDlg = "Research/ResearchDetailInfoDlg";
      public const string VipBaseDlg = "VIP/VipBaseDlg";
      public const string VIPItemsDlg = "VIP/VIPItemsDlg";
      public const string VIPPointItemsDlg = "VIP/VIPPointItemsDlg";
      public const string boostsDlg = "BoostsDlg";
      public const string allianceDlg = "Alliance/AllianceDlg";
      public const string allianceCustomizeDlg = "Alliance/AllianceCustomizeDialog";
      public const string allianceStoreDlg = "Alliance/AllianceStore";
      public const string allianceHelpRequestDlg = "Alliance/AllianceHelpRequestsDlg";
      public const string allianceInviteDialog = "Alliance/AllianceInviteDialog";
      public const string allianceMoreInvitesApplicantsDlg = "Alliance/AllianceMoreInvitesApplicantsDlg";
      public const string allianceBlockingDlg = "Alliance/AllianceBlockingDlg";
      public const string allianceTradeDlg = "Alliance/AllianceTradeDlg";
      public const string noMarketplaceDlg = "NoMarketplaceDlg";
      public const string allianceGiftsDlg = "Alliance/AllianceGiftsDlg";
      public const string allianceTransferLeadership = "Alliance/AllianceTransferLeadershipDlg";
      public const string allianceDiplomacyDlg = "Alliance/AllianceDiplomacyDlg";
      public const string allianceJoinDlg = "Alliance/AllianceJoinDlg";
      public const string allianceTechDlg = "Alliance/AllianceTechDlg";
      public const string allianceWarDlg = "Alliance/AllianceWarMainDlg";
      public const string allianceJoinAndApplyDialog = "Alliance/AllianceJoinAndApplyDialog";
      public const string allianceOtherMemberDialog = "Alliance/AllianceOtherMemberDialog";
      public const string allianceInviteConfirmPopup = "Alliance/AllianceInviteConfirmPopup";
      public const string allianceCityDialog = "Alliance/AllianceCityDlg";
      public const string allianceBossDetailsDlg = "Alliance/AllianceBossDetailsDlg";
      public const string constructionDlg = "ConstructionDlg";
      public const string speedUpDlg = "ItemStore/SpeedUpDlg";
      public const string moreResourcesDlg = "ItemStore/GetMoreResDlg";
      public const string moreItemDlg = "ItemStore/MoreItemDialog";
      public const string miniWonderDlg = "MiniWonderDialog";
      public const string searchCoordinateDlg = "popupSearchCoordinates";
      public const string playerProfileDlg = "PlayerProfile/PlayerProfileDlg";
      public const string playerProfileDlgDetail = "PlayerProfile/PlayerProfileDlgDetail";
      public const string playerProfileChangePortrait = "PlayerProfile/PlayerProfileChangePortraitDlg";
      public const string playerEquipment = "PlayerProfile/PlayerEquipmentDlg";
      public const string DragonKnightEquipmentDialg = "PlayerProfile/DragonKnightEquipmentDialog";
      public const string playerArtifactDlg = "PlayerProfile/PlayerArtifactDlg";
      public const string warchTowerMarchesDlg = "Watchtower/WatchtowerMarchesDialog";
      public const string popupMonsterTileInfoResource = "PveMonsterDlg";
      public const string popupTeleportConfirmation = "popupTeleportConfirmationDlg";
      public const string scoutConfirmDlg = "scoutConfirmDlg";
      public const string popupWorldSearchCoordinates = "popupWorldSearchCoordinates";
      public const string WarReportRoundDetail = "WarReportRoundDetail";
      public const string scoutReportDlg = "ScoutReportDlg";
      public const string WatchtowerDetailDlg = "WatchtowerDetail";
      public const string otherReport = "Mail/OtherReportDlg";
      public const string reinforceReportDlg = "ReinforceReportDlg";
      public const string RallyReportDlg = "RallyReportDlg";
      public const string systemMessageDlg = "Mail/SystemMessageDlg";
      public const string constructionSelectDlg = "Construction/ConstructionSelectDlg";
      public const string BuildingConstructionDlg = "BuildingConstruction";
      public const string BuildingResPopUpDlg = "BuildingResPopUp";
      public const string resourceTileInfoDlg = "ResourceInfoDlg";
      public const string worldBossDetaillDlg = "WorldBossDetaillDlg";
      public const string buildingInfoDlg = "BuildingInfo/BuildingInfoDlg";
      public const string buildingInfoDlgPub = "BuildingInfo/BuildingInfo_Pub";
      public const string boostMenuDlg = "Boost/BoostMenu";
      public const string boostsItemsDlg = "Boost/Boosts_Items";
      public const string legendDetailDlg = "Legend/LegendDetailDlg";
      public const string LegendRecruitDlg = "Legend/LegendRecruitDlg";
      public const string LegendRecruitmentDlg = "Legend/LegendRecruitmentDlg";
      public const string LegendExpMenuDialog = "Legend/LegendExpMenuDialog";
      public const string LegendAddExpAndDevourDialog = "Legend/LegendAddExpAndDevourDialog";
      public const string LegendListDlg = "Legend/LegendListDlg";
      public const string LegendTowerDlg = "Legend/LegendTowerDlg";
      public const string GetMoreLegnedDlg = "Legend/GetMoreLegend";
      public const string talentTreeDlg = "Talent/TalentTreeDlg";
      public const string RoulettePlayDialog = "Roulette/TurnableDlg";
      public const string LeaderBoardDetailDlg = "LeaderBoard/LeaderBoardDetailDlg";
      public const string LeaderBoardDlg = "LeaderBoard/LeaderRankBoardDlg";
      public const string WorldLeaderBoardDetailDlg = "LeaderBoard/WorldLeaderBoardDetailDlg";
      public const string WorldLeaderBoardDlg = "LeaderBoard/WorldLeaderRankBoardDlg";
      public const string TutorialDlg = "Tutorial/Tutorial";
      public const string SystemSettingDlg = "SystemSettingDlg";
      public const string NpcStoreDlg = "NpcStoreDialog";
      public const string MagicStoreDialog = "MagicStoreDialog";
      public const string MagicSelterDlg = "MagicSelterDlg";
      public const string DragonKnightTalentTreeDlg = "DragonKnight/DragonKnightTalentTreeDlg";
      public const string DragonKnightSkillDlg = "DragonKnight/DragonKnightSkillDlg";
      public const string DungeonAncientStoreDialog = "DragonKnight/DungeonAncientStoreDialog";
      public const string DragonKnightEarlyGuideDlg = "DragonKnight/DragonKnightGuideDlg";
      public const string WallDefense = "Wall/WallDefense";
      public const string RuralBlockUnlockDialog = "RuralBlock/RuralBlockUnlockDialog";
      public const string PVEMonsterDetailDialog = "PVEMonsterDetailDialog";
      public const string DragonSkillEnhanceDlg = "Dragon/DragonSkillEnhanceDlg";
      public const string DragonPropertyDlg = "Dragon/DragonPropertyDlg";
      public const string DragonSkillDlg = "Dragon/DragonSkillDlg";
      public const string DragonSkillConfigDialog = "Dragon/DragonSkillConfigDialog";
      public const string BuildingDecorationDlg = "CastleDecorationDlg";
      public const string BuildingInfoWishWellDialog = "WishWell/BuildingInfoWishWellDialog";
      public const string AllianceRallyDetialPopup = "Alliance/AllianceRallyDetialDlg";
      public const string AllianceBoardDialog = "Alliance/AllianceBoardDialog";
      public const string GveBossDetailDialog = "GvE/GveBossDetailDialog";
      public const string AllianceFortressDetailDlg = "Alliance/AllianceFortressDetailDlg";
      public const string AllianceWarehouseDlg = "Alliance/AllianceStorehouse";
      public const string AllianceWarehouseDetailDlg = "Alliance/AllianceWarehouseDetailDlg";
      public const string AllianceBuildingGatherDetailDlg = "Alliance/AllianceBuildingGatherDetailDlg";
      public const string AllianceBuildingConstructDetailDlg = "Alliance/AllianceBuildingConstructDetailDlg";
      public const string AllianceBossListDlg = "Alliance/AllianceBossTrialListDlg";
      public const string AllianceHospitalHeal = "Alliance/AllianceHospitalHealDlg";
      public const string AllianceHospitalDetailDialog = "Alliance/AllianceHospitalDetailDialog";
      public const string AllianceBuildingDefendDetailDlg = "Alliance/AllianceBuildingDefendDetailDlg";
      public const string AllianceWarDialog = "Alliance/AllianceWarDlg";
      public const string AllianceTempleSkillDialog = "Alliance/AllianceTempleSkillDlg";
      public const string AllianceChestDialog = "AllianceChest/AllianceChestDialog";
      public const string KingdomTitleDlg = "Wonder/KingdomTitleDlg";
      public const string WonderGiftDlg = "Wonder/WonderGiftDlg";
      public const string KingdomAllotResDlg = "Wonder/KingAllotResDlg";
      public const string EquipmentForgeDlg = "Equipment/EquipmentForgeDlg";
      public const string EquipmentTreasuryDlg = "Equipment/EquipmentTreasuryDlg";
      public const string SteelItemsDlg = "Equipment/SteelItemsDlg";
      public const string EquipmentEnhanceDlg = "Equipment/EquipmentEnhanceDlg";
      public const string EquipmentGemRefineDlg = "Gem/EquipmentGemRefineDlg";
      public const string DragonKnightEquipmentForgeDialog = "Equipment/DragonKnightEquipmentForgeDialog";
      public const string DragonKnightEquipmentEnhanceDialog = "Equipment/DragonKnightEquipmentEnhanceDialog";
      public const string DragonKnightEquipmentTreasuryDialog = "Equipment/DragonKnightEquipmentTreasuryDialog";
      public const string IAPRecommendedPackageDialog = "IAP/IAPRecommendedPackageDialog";
      public const string RebateByRechargeDlg = "Rebate/RebateByRechargeDlg";
      public const string VaultDlg = "Vault/VaultDlg";
      public const string AuctionHallDlg = "Auction/AuctionHallDlg";
      public const string AchievementDlg = "Achievement/AchievementDlg";
      public const string WonderSettingDlg = "Wonder/KingdomSettingDlg";
      public const string ArtifactDlg = "Artifact/ArtifactDlg";
      public const string ArtifactSaleStoreDlg = "Artifact/ArtifactSaleStoreDlg";
      public const string ArtifactBuyInventoryDlg = "Artifact/ArtifactBuyInventoryDlg";
      public const string HallOfKingDlg = "HallOfKing/HallOfKingDlg";
      public const string KingdomHospitalDlg = "KingdomHospitalDlg";
      public const string HeroRecruitDialog = "HeroCard/HeroRecruitDlg";
      public const string HeroRecruitLuckyDrawDialog = "HeroCard/HeroRecruitLuckyDrawDialog";
      public const string HeroRecruitTenLuckyDrawDialog = "HeroCard/HeroRecruitTenLuckyDrawDialog";
      public const string HeroInventoryDlg = "HeroCard/HeroInventoryDlg";
      public const string HeroParliamentDlg = "HeroCard/HeroParliamentDlg";
      public const string HeroPositionAssignDlg = "HeroCard/HeroPositionAssignDlg";
      public const string IAPStoreMonthlySpecialDlg = "IAP/IAPStoreMonthlyDisountDlg";
      public const string PitExploreDetailsDlg = "Pit/PitExploreDetailsDlg";
      public const string TradingHallDlg = "TradingHall/TradingHallDlg";
      public const string DKArenaMainDlg = "DKArena/DKArenaMainDlg";
      public const string AltarSummonDlg = "AltarSummon/AltarSummonDialog";
      public const string AnniversaryMainDlg = "Anniversary/AnniversaryMainDlg";
      public const string AnniversaryIapMainDlg = "Anniversary/AnniversaryIapMainDlg";
      public const string AnniversaryOverDlg = "Anniversary/AnniversaryOverDlg";
      public const string AnniversarySendMarchDlg = "Anniversary/AnniversarySendMarchDlg";
      public const string BuildingGloryUpgradeDlg = "BuildingGlory/BuildingGloryUpgradeDlg";
      public const string KingdomBossDetailDialog = "KingdomBoss/KingdomBossDetailDialog";
      public const string WorldBossActivityDetailsDlg = "KingdomBoss/WorldBossActivityDetailsDlg";
      public const string VerificationDlg = "VerificationDlg";
      public const string AllianceWarBriefDlg = "AllianceWarBriefDlg";
      public const string MerlinTrialsDialog = "MerlinTrials/MelinTrialDlg";
      public const string MerlinExchangeStoreDlg = "MerlinTrials/MerlinExchangeStoreDlg";
      public const string MerlinTrialsRankDlg = "MerlinTrials/MerlinTrialsRankDlg";
      public const string MerlinTrialsLevelDialog = "MerlinTrials/MelinTrialLevelDlg";
      public const string MerlinTrialsMarchDialog = "MerlinTrials/MerlinTrialsMarchAllocDlg";
      public const string MerlinTowerDlg = "MerlinTower/MerlinTowerDlg";
      public const string MerlinTowerRankDlg = "MerlinTower/MerlinTowerRankDlg";
      public const string MerlinTowerOreMarchAllocDlg = "MerlinTower/MerlinTowerOreMarchAllocDlg";
      public const string MerlinTowerMarchAllocDlg = "MerlinTower/MerlinTowerMarchAllocDlg";
      public const string LordTitleDlg = "LordTitle/LordTitleDlg";

      public static string IAPStoreDialog
      {
        get
        {
          return PlayerData.inst.IapUiType == 1 ? "IAP/IAPStoreDialog2" : "IAP/IAPStoreDialog";
        }
      }
    }

    public struct PopupType
    {
      public const string DailyActiviesPopup = "DailyActivy/DailyActiviesPopup";
      public const string LanguageSelection = "LanguageSelectPopup";
      public const string singleButtonPopup = "SingleButtonPopup";
      public const string doubleButtonPopup = "DoubleButtonPopup";
      public const string questRefreshPopup = "QuestRefreshPopup";
      public const string allianceFundBuyPopup = "AllianceFundBuyDialog";
      public const string allianceHonorBuyPopup = "AllianceHonorBuyDialog";
      public const string allianceSendTradePopup = "AllianceSendTradePopup";
      public const string IgnorableMessageBoxPopup = "IgnorableMessageBox";
      public const string MessageBoxWith1Button = "MessageBoxWith1Button";
      public const string messageBox = "MessageBox";
      public const string MessageBoxWith2ButtonsPopup = "MessageBoxWith2ButtonsPopup";
      public const string MessageBoxWith2Buttons = "MessageBoxWith2Buttons";
      public const string buyAndUsePopup = "BuyAndUsePopup";
      public const string getMoreSlotPopup = "GetMoreLegend";
      public const string destroyConfirmPopup = "BuildingInfo/DestroyConfirmPopup";
      public const string buildingInfo = "BuildingInfo/BuildingInfoMoreInfoPopup";
      public const string BuildingInfoMoreInfoBasePopup = "BuildingInfo/BuildingInfoMoreInfoBasePopup";
      public const string strongholdInfoMoreinfo = "BuildingInfoStrongholdMoreInfo";
      public const string barracksMoreInfoPopup = "BarracksMoreInfoPopup";
      public const string trapMoreInfoPopup = "TrapMoreInfoPopup";
      public const string commonDetailPopup = "CommonDescPopup";
      public const string DestroyFinalConfirmPopup = "BuildingInfo/DestroyFinalConfirmPopup";
      public const string TalentEnhanceDlg = "Talent/TalentEnhanceDlg";
      public const string getMoreItems2 = "GetMoreItems2";
      public const string DevourLegendPopup = "DevourLegendPopup";
      public const string LeaderBoardAllianceInfo = "LeaderBoard/LeaderBoardAllianceInfo";
      public const string LeaderBoardPlayerInfo = "LeaderBoard/LeaderBoardPlayerInfo";
      public const string timerChestConfirm = "TimerChestConfirmPopup";
      public const string TimerChestPopup = "TimerChestPopup";
      public const string BuilderRecruitPopup = "BuildQueue/BuilderRecruitPopup";
      public const string goldConfirmPopup = "GoldConsumePopup";
      public const string lordLevelUpPopup = "LordLevelUp";
      public const string allianceTechDonate = "AllianceTech/AllianceTechDonatePopUp";
      public const string BookmarkPopup = "Bookmark/BookmarkPopup";
      public const string BookmarkUpdatePopup = "Bookmark/BookmarkUpdatePopup";
      public const string BookmarkCreatePopup = "Bookmark/BookmarkCreatePopup";
      public const string BookmarkDeletePopup = "Bookmark/BookmarkDeletePopup";
      public const string RebuildPopup = "Teleport/RebuildPopup";
      public const string ForceBackPopup = "Teleport/ForceBackPopup";
      public const string ForceTeleportPopup = "Teleport/ForceTeleportPopup";
      public const string SendBackPopup = "Teleport/SendBackPopup";
      public const string ConfirmWithPeaceShield = "ConfirmWithPeaceShield";
      public const string ChooseConfirmationBox = "ChooseConfirmationBox";
      public const string QuickSearchPopup = "QuickSearchPopup";
      public const string GiftExchangePopup = "GiftExchange/GiftExchangePopup";
      public const string RecommendPopUp = "RecommendPopUp";
      public const string DailyActivyDetailPopUp = "DailyActivy/DailyActivyDetailPopUp";
      public const string DailyActivyRewardPopUp = "DailyActivy/DailyActivyRewardPopUp";
      public const string SignPopup = "Sign/SignPopup";
      public const string SignRewardPopup = "Sign/SignRewardPopup";
      public const string SignSingleRewardPopup = "Sign/SignSingleRewardPopup";
      public const string ActivityStartNotifyPopup = "Activity/ActivityStartNotifyPopup";
      public const string WishWellPopup = "WishWell/WishWellPopup";
      public const string UseItemPopup = "UseItemPopup";
      public const string UseItemCannotGoldPopup = "UseItemCannotGoldPopup";
      public const string UseOrBuyAndUseItemPopup = "UseOrBuyAndUseItemPopup";
      public const string allianceTechDonateRank = "AllianceTech/AllianceTechDonateRankPop";
      public const string AllianceTechSpeedUpPopup = "AllianceTech/AllianceTechSpeedUpPopup";
      public const string DragonNamePopup = "Dragon/DragonNamePopup";
      public const string DragonRenamePopup = "Dragon/DragonRenamePopup";
      public const string DragonLevelupPopup = "Dragon/DragonLevelupPopup";
      public const string DragonSkillUpgradePopup = "Dragon/DragonSkillUpgradePopup";
      public const string DragonSkillConfigDialog = "Dragon/DragonSkillConfigDialog";
      public const string DragonSkillDetailPopup = "Dragon/DragonSkillDetailPopup";
      public const string BuildingInfoWishWellMoreInfo = "WishWell/BuildingInfoWishWellMoreInfo";
      public const string BuildingGloryLevelUpPopup = "BuildingGlory/BuildingGloryLevelUpPopup";
      public const string CommonChoosePopup = "BuildingGlory/CommonChoosePopup";
      public const string EquipmentEffectPopup = "Equipment/EquipmentEffectPopup";
      public const string allianceWarLogPopup = "AllianceWar/AllianceWarLogPop";
      public const string BindAccountTipPopup = "BindAccountPopup";
      public const string AllianceRewardDistributionPopup = "AllianceWar/AllianceRewardDistributionPopup";
      public const string AllianceRewardInstructionPopup = "AllianceWar/AllianceRewardInstructionPopup";
      public const string warReportPopup = "Report/WarReportPopup";
      public const string KingdomBossReportPopup = "Report/KingdomBossReportPopup";
      public const string FestivalEventReportPopup = "Report/FestivalBossReport";
      public const string DragonAltarReportPopup = "Report/DragonAltarReportPopup";
      public const string DisastrousReportPopup = "Report/DisastrousReportPopup";
      public const string warReportDetailPopup = "Report/WarReportDetail";
      public const string monsterReportPopup = "Report/MonsterReportPopup";
      public const string TreasureMapReportPopup = "Report/TreasureMapReportPopup";
      public const string FirstKillPopup = "Report/FirstKillReportPopup";
      public const string GVEWarReportPopup = "Report/GVEWarReportPopup";
      public const string GVERewardsPopup = "Report/GVERewardsPopup";
      public const string AnniversaryBattleResultPopup = "Report/BattleResultPopup";
      public const string scoutReportPopup = "ScoutReportPopup";
      public const string allianceHospitalPopup = "AllianceHospitalMailPopup";
      public const string scoutBenefitComparisonPopup = "ScoutBenefitComparisonPopup";
      public const string AllianceBossResultPopup = "Report/AllianceBossResultPopup";
      public const string AllianceBossReportPopup = "Report/AllianceBossReportPopup";
      public const string WatchTowerDetailPopup = "WatchTowerDetailPopup";
      public const string speedUpDlg = "SpeedUpPopup";
      public const string VerificationPopup = "VerificationPopup";
      public const string ContactServicePopup = "ContactServicePopup ";
      public const string ItemUseOrBuyPopup = "ItemUseOrBuyPopup";
      public const string ChestItemDetailPopup = "ChestItemDetailPopup";
      public const string GemUsedDetailPopup = "Gem/GemUsedDetailPopup";
      public const string OtherReportPopup = "Mail/OtherReportPopup";
      public const string IAPSuccessPopup = "Mail/IAPSuccessPopup";
      public const string IAPDailySuccessPopup = "Mail/IAPDailySuccessPopup";
      public const string ExcaliburReportPopup = "Report/ExcaliburReportPopup";
      public const string ArtifactReportPopup = "Report/AritfactReportPopup";
      public const string AnnKingArenaOpenClosePopup = "Mail/AnnKingArenaOpenClosePopup";
      public const string GatherReportPopup = "Mail/GatherReportPopup";
      public const string SystemMailPopup = "Mail/SystemMailPopup";
      public const string AbyssMailPopup = "Mail/AbyssMailPopup";
      public const string UpdateMailPopup = "Mail/UpdateMailPopup";
      public const string AllianceRecommendMailPopup = "Mail/AllianceRecommendMailPopup";
      public const string AllianceIOPopup = "AllianceMail/AllianceIOPopup";
      public const string AllianceDonationPopup = "AllianceMail/AllianceDonationPopup";
      public const string TeleportInvitePopup = "AllianceMail/TeleportInvitePopup";
      public const string AllianceTradePopup = "AllianceMail/AllianceTradePopup";
      public const string AllianceLordChangePopup = "AllianceMail/AllianceLordChangePopup";
      public const string SystemTeleportInvitePopup = "AllianceMail/SystemTeleportInvitePopup";
      public const string AllianceCreatePopup = "Alliance/AllianceCreatePopup";
      public const string AllianceApplication = "Alliance/AllianceApplication";
      public const string AllianceJoinPopup = "Alliance/AllianceJoinPopup";
      public const string AllianceJoinEarlyPopup = "Alliance/AllianceJoinBPopup";
      public const string AllianceMemberOptionPopup = "Alliance/AllianceMemberOptionPopup";
      public const string AllianceTransferPopup = "Alliance/AllianceTransferPopup";
      public const string AlliancePromotePopup = "Alliance/AlliancePromotePopup";
      public const string AllianceDemotePopup = "Alliance/AllianceDemotePopup";
      public const string AllianceKickPopup = "Alliance/AllianceKickPopup";
      public const string AllianceRankPrivilegePopup = "Alliance/AllianceRankPrivilegePopup";
      public const string AllianceUsurpPopup = "Alliance/AllianceUsurpPopup";
      public const string AllianceInviteTeleportPopup = "Alliance/AllianceInviteTeleportPopup";
      public const string AllianceBoardMenuPopup = "Alliance/AllianceBoardMenuPopup";
      public const string AllianceChangePublicMessagePopup = "Alliance/AllianceChangePublicMessagePopup";
      public const string AllianceFortressViewPopup = "Alliance/AllianceFortViewFunction";
      public const string AllianceBossRallyPopup = "Alliance/AllianceBossRallyPopup";
      public const string AllianceBossDonatePopup = "Alliance/AllianceBossDonatePopup";
      public const string AllianceBossRewardsPopup = "Alliance/AllianceBossRewardsPopup";
      public const string AllianceBossRankingPopup = "Alliance/AllianceBossRankingPopup";
      public const string AllianceBossPortalInfoPopup = "Alliance/AlliancePortalInfoPopup";
      public const string AllianceBossHistoryPopup = "Alliance/AllianceBossHistoryPopup";
      public const string AllianceStoreHistoryPopup = "Alliance/AllianceStoreRecordPopup";
      public const string AllianceRecommendationGoldEntryPopup = "Alliance/AllianceRecommendationGoldEntryPopup";
      public const string AllianceLogPopup = "Alliance/AllianceLogPopup";
      public const string MarchDetailPopup = "MarchDetailPopUp";
      public const string DragonKnightEarlyGuidePopup = "DragonKnight/DragonKnightGuidePopup";
      public const string NotificationSettingPopup = "NotificationSettingPopup";
      public const string ActivityRankPopup = "Activity/ActivityRankPopup";
      public const string TimeLimitAllRewardPopup = "Activity/TimeLimitAllRewardPopup";
      public const string ActivityNotStartedPopup = "Activity/ActivityNotStartedPopup";
      public const string ActivityRewardsPopup = "Activity/ActivityRewardsPopup";
      public const string AuctionRewardsPopup = "Activity/AuctionRewardsPopup";
      public const string FresherRewardsPopup = "Activity/FresherRewardsPopup";
      public const string StartConfirmationPopup = "Activity/StartConfirmationPopup";
      public const string FallenKnightRewardsHelpPopup = "Activity/FallenKnightRewardsHelpPopup";
      public const string FallenKnightAllianceRankPopup = "Activity/FallenKnightAllianceRankPopup";
      public const string FallenKnightIndividualRankPopup = "Activity/FallenKnightIndividualRankPopup";
      public const string FallenKnightRewardsPopup = "Activity/FallenKnightRewardsPopup";
      public const string AllianceEventParticipationPopup = "Activity/AllianceEventParticipationPopup";
      public const string AllianceEventHowToPopup = "Activity/AllianceEventHowToPopup";
      public const string GveInstructions = "GvE/GveInstructions";
      public const string VIPLosePopup = "VIP/VIPLosePopup";
      public const string HelpPopup = "HelpPopup";
      public const string PlayerProfileRenamePopup = "PlayerProfileRenamePopup";
      public const string CityInfoDetail = "CityInfoDetail";
      public const string CityStatsPopup = "CityStatsPopup";
      public const string ItemTipPopup = "ItemTipDetailPopup";
      public const string GroupQuestPopup = "GroupQuest/GroupQuestPopup";
      public const string EquipmentForgeSuccessfullyPopup = "Equipment/EquipmentForgeSuccessfullyPopup";
      public const string SynthesizeSuccessfullyPopup = "Equipment/SynthesizeSuccessfullyPopup";
      public const string EquipmentDecomposePopup = "Equipment/EquipmentDecomposePopup";
      public const string EquipmentInfoPopup = "Equipment/EquipmentInfoPopup";
      public const string ItemComposePopup = "ItemComposePopup";
      public const string SellScrollPopup = "SellScrollPopup";
      public const string SellScrollChipPopup = "SellScrollChipPopup";
      public const string IAPBuyConfirmPopup = "IAPBuyConfirmPopup";
      public const string IAPBuySuccessPopup = "IAPBuySuccessPopup";
      public const string DissmissHealTroopPopup = "DissmissHealTroopPopup";
      public const string CancelHealConfirmPopup = "CancelHealConfirmPopup";
      public const string MarchSpeedUpPopup = "MarchSpeedUpPopup";
      public const string MonthCardPopup = "MonthCardPopup";
      public const string MonthCardPopupB = "MonthCardPopupB";
      public const string MonthCardPurchasePopup = "MonthCardPurchasePopup";
      public const string WebviewPopup = "WebviewPopup";
      public const string SubscriptionPopup = "SubscibePopup";
      public const string DragonTendencySkillsPopup = "Dragon/DragonTendencySkillsPopup";
      public const string AccountManagementPopup = "Account/AccountManagementPopup";
      public const string AccountSwitchPopup = "Account/AccountSwitchPopup";
      public const string ClipboardPopup = "ClipboardPopup";
      public const string UseItemConfirmPopup = "UseItemConfirmPopUp";
      public const string fallenKnightReportPopup = "FallenKnightReportPopup";
      public const string InfoKingdom = "InfoKingdom";
      public const string casinoGetMorePopup = "Casino/GetMorePopup";
      public const string casinoConfirmationPopup = "Casino/ConfirmationPopup";
      public const string casinoCardHistoryPopup = "Casino/CardHistoryPopup";
      public const string JackpotRewardPopup = "Jackpot/JackpotRewardPopup";
      public const string LotteryDiyFreeRewardPopup = "LotteryDiy/LotteryDiyFreeRewardPopup";
      public const string LotteryDiyHistoryPopup = "LotteryDiy/LotteryDiyHistoryPopup";
      public const string LotteryDiyConfirmPopup = "LotteryDiy/LotteryDiyConfirmPopup";
      public const string MarksmanHistoryPopup = "Marksman/MarksmanHistoryPopup";
      public const string GetMorePropsPopup = "Marksman/GetMorePropsPopup";
      public const string CommonConfirmPopup = "Marksman/CommonConfirmPopup";
      public const string DiceTenWinPopup = "Dice/DiceTenWinPopup";
      public const string DicePointExchangePopup = "Dice/DicePointExchangePopup";
      public const string DiceFreeRewardPopup = "Dice/DiceFreeRewardPopup";
      public const string DiceAllRewardPopup = "Dice/DiceAllRewardPopup";
      public const string SwitchKingdomPopup = "SwitchKingdomPopup";
      public const string ResearchingStatePopup = "ResearchingStatePopup";
      public const string ResearchFullLevelPopup = "ResearchFullLevelPopup";
      public const string ResearchDetailInfoPopup = "ResearchDetailInfoPopup";
      public const string AllianceBossResultReportPopup = "Report/AllianceBossResultReportPopup";
      public const string AllianceBossRewardsMailPopup = "Report/AllianceBossRewardsMailPopup";
      public const string BuildingHintPopup = "BuildingHintPopup";
      public const string RestoreManaPopup = "Alliance/DragonAltarRestoreManaPopup";
      public const string PowerUpPopup = "Alliance/DragonAltarPowerUpPopup";
      public const string SkillInfoPopup = "Alliance/DragonAltarSkillInfoPopup";
      public const string AllianceMagicWarLogPop = "Alliance/AllianceMagicWarLogPop";
      public const string AllianceWarTargetListPopup = "Alliance/AllianceMagicWarTargetListPopup";
      public const string IAPPaymentModePopup = "IAP/IAPPaymentModePopup";
      public const string IapRebatePopup = "IAP/LimitedTimeRebatePopup";
      public const string IAPStorePackageMorePopup = "IAP/IAPStoreMoreInfoPopup";
      public const string IAPDailyRechargePopup = "IAP/IAPDailyRechargePopup";
      public const string VaultConfirmationPopup = "Vault/VaultConfirmationPopup";
      public const string VaultDepositWithdrawPopup = "Vault/VaultDepositWithdrawPopup";
      public const string AuctionHallHistoryPopup = "Auction/AuctionHallHistoryPopup";
      public const string AuctionHallRecordPopup = "Auction/AuctionHallRecordPopup";
      public const string AuctionHallRecordInfoPopup = "Auction/AuctionHallRecordInfoPopup";
      public const string BigTimerChestCollectPopup = "BigTimerChestCollectPopup";
      public const string UpLoadCustomIconPopup = "UpLoadCustomIconPopup";
      public const string KingdomTitleInfoPopup = "Wonder/KingdomTitleInfoPopup";
      public const string KingdomArtifactPopup = "Wonder/KingdomArtifactPopup";
      public const string KingdomStatePopup = "Wonder/KingdomStatePopup";
      public const string KingdomWarMarchPopup = "Wonder/KingdomWarMarchPopup";
      public const string WonderGiftHistoryPopup = "Wonder/WonderGiftHistoryPopup";
      public const string KingdomTreasuryPopup = "Wonder/KingdomTreasuryPopup";
      public const string KingdomTreasuryRecordPopup = "Wonder/KingdomTreasuryRecordPopup";
      public const string DragonKnightTalentEnhancePopup = "DragonKnight/DragonKnightTalentEnhancePopup";
      public const string DragonKnightSkillInfoPopup = "DragonKnight/DragonKnightSkillInfoPopup";
      public const string DragonKnightSkillSettingPopup = "DragonKnight/DragonKnightSkillSettingPopup";
      public const string DragonKnightDungeonSettingPopup = "DragonKnight/DragonKnightDungeonSettingPopup";
      public const string DragonKnightDungeonEntryPopup = "DragonKnight/DragonKnightDungeonEntryPopup";
      public const string DragonKnightDungeonFinishPopup = "DragonKnight/DragonKnightDungeonFinishPopup";
      public const string DragonKnightDungeonRewardsPopup = "DragonKnight/DragonKnightDungeonRewardsPopup";
      public const string DragonKnightDungeonPickFloorPopup = "DragonKnight/DragonKnightDungeonPickFloorPopup";
      public const string DragonKnightBattleSplashPopup = "DragonKnight/DragonKnightBattleSplashPopup";
      public const string DragonKnightPVPSplashPopup = "DragonKnight/DragonKnightPVPSplashPopup";
      public const string DragonKnightLevelUpPopup = "DragonKnight/DragonKnightLevelUpPopup";
      public const string DragonKnightTemporaryItemPopup = "DungeonTemporaryItemsPopup";
      public const string DragonKnightEnterNextFloorConfirmPopup = "DragonKnight/EnterNextFloorConfirmPopup";
      public const string DragonKnightTalentSkillDetailPopup = "DragonKnight/DragonKnightTalentSkillDetailPopup";
      public const string DragonKnightPVPResultPopup = "DragonKnight/DragonKnightPVPResultPopup";
      public const string DragonKnightArenaResultPopup = "DragonKnight/DragonKnightArenaResultPopup";
      public const string DragonKnightPVPComparisonPopup = "DragonKnight/DragonKnightPVPComparisonPopup";
      public const string DungeonMopupFloorSelectionPopup = "DragonKnight/DungeonMopupFloorSelectionPopup";
      public const string DungeonMopupInventoryPopup = "DragonKnight/DungeonMopupInventoryPopup";
      public const string DungeonMopupChooseAmountPopup = "DragonKnight/DungeonMopupChooseAmountPopup";
      public const string DungeonMopupCompletePopup = "DragonKnight/DungeonMopupCompletePopup";
      public const string DungeonStorePurchasePopup = "DragonKnight/DungeonStorePurchasePopup";
      public const string DungeonCanelPopup = "DragonKnight/DungeonCanelPopup";
      public const string DKArenaRecodePopup = "DKArena/DKArenaRecodePopup";
      public const string DKArenaScoreRewardsPopup = "DKArena/DKArenaScoreRewardsPopup";
      public const string RewardDetailPopUp = "RewardDetailPopUp";
      public const string ArtifactDetailsPopup = "Artifact/ArtifactDetailsPopup";
      public const string ArtifactDescriptionPopup = "Artifact/ArtifactDescriptionPopup";
      public const string ArtifactConfirmBuyPopup = "Artifact/ArtifactConfirmBuyPopup";
      public const string KingdomWarEventPopup = "Activity/KingdomWarEventPopup";
      public const string SuperLoginChristmasEvePopup = "SuperLogin/SuperLoginChristmasEvePopup";
      public const string SuperLoginChristmasEveGiftPopup = "SuperLogin/SuperLoginChristmasEveGiftPopup";
      public const string SuperLoginFirstAnniversaryPopup = "SuperLogin/FirstAnniversaryPopup";
      public const string SuperLoginFirstAnniversaryRewardPopup = "SuperLogin/FirstAnniversaryRewardPopup";
      public const string KingdomBuffPopup = "KingdomBuff/KingdomBuffPopup";
      public const string KingSkillInfoPopup = "HallOfKing/KingSkillInfoPopup";
      public const string KingSkillHistoryPopup = "HallOfKing/KingSkillHistoryPopup";
      public const string TreasureMapPopup = "TreasureMap/TreasureMapPopup";
      public const string MerlinTowerBuybuffWith2ButtonsPopup = "MerlinTower/MerlinTowerBuybuffWith2ButtonsPopup";
      public const string MailItemListPopup = "Mail/MailItemListPopup";
      public const string MailSelectionPopup = "Mail/MailSelectContactPopup";
      public const string EquipmentGemEnhancePopup = "Gem/EquipmentGemEnhancePopup";
      public const string EquipmentGemEnhanceConfirmPopup = "Gem/EquipmentGemEnhanceConfirmPopup";
      public const string EquipmentGemInsetPopup = "Gem/EquipmentGemInsetPopup";
      public const string EquipmentGemRemovePopup = "Gem/EquipmentGemRemovePopup";
      public const string EquipmentGemHandbookPopup = "Gem/EquipGemHandbookPopup";
      public const string PersonalChestPopup = "AllianceChest/PersonalChestPopup";
      public const string AllianceChestPopup = "AllianceChest/AllianceChestPopup";
      public const string AllianceChestHistoryPopup = "AllianceChest/AllianceChestHistoryPopup";
      public const string HeroBookPopup = "ParliamentHero/HeroBookPopup";
      public const string HeroBookDetailPopup = "ParliamentHero/HeroBookDetailPopup";
      public const string HeroFragmentDetailPopup = "ParliamentHero/HeroFragmentDetailPopup";
      public const string HeroInventoryInfoPopup = "HeroCard/HeroInventoryInfoPopup";
      public const string HeroCardLevelUpPopup = "HeroCard/HeroCardLevelUpPopup";
      public const string HeroCardSuitInfoPopup = "HeroCard/HeroCardSuitInfoPopup";
      public const string TradingHallBuyPopup = "TradingHall/TradingHallBuyPopup";
      public const string TradingHallSellPopup = "TradingHall/TradingHallSellPopup";
      public const string TradingHallMailPopup = "TradingHall/TradingHallMailPopup";
      public const string TradingHallExpiredMailPopup = "TradingHall/TradingHallExpiredMailPopup";
      public const string PitMinimapPopup = "Pit/PitMinimapPopup";
      public const string PitAllRewardPopup = "Pit/PitAllRewardPopup";
      public const string PitGoHomePopup = "Pit/PitGoHomePopup";
      public const string PitExploreSignUpPopup = "Pit/PitExploreSignUpPopup";
      public const string PitExploreViewRankPopup = "Pit/PitExploreViewRankPopup";
      public const string PitExploreScoreRankPopup = "Pit/PitExploreScoreRankPopup";
      public const string PitScoreRewardPopup = "Pit/PitScoreRewardPopup";
      public const string AnniversaryIapPopup = "Anniversary/AnniversaryIapBuyPopup";
      public const string AnniversaryRewardsPopup = "Anniversary/AnniversaryKingRewardPopup";
      public const string AnniversaryKingDetailPopup = "Anniversary/AnniversaryKingDetailPopup";
      public const string AnniversaryIapPropsPopup = "Anniversary/AnniversaryIapPropsPopup";
      public const string AnniversaryAttackShowPopup = "Report/AnniversaryAttackShowPopup";
      public const string LoginErrPopup = "Account/LoginErrPopup";
      public const string HelpModEntrance = "Mod/ModEntrance";
      public const string ModHelpPopup = "Mod/ModHelpPopup";
      public const string RouletteFreeRewardPopup = "Roulette/TurnableFreeRewardPopup";
      public const string RouletteRewardSelectPopup = "Roulette/TurnableChooseRewardPopup";
      public const string RouletteMultiWinPopup = "Roulette/TurnableWinPopup";
      public const string ModResPopup = "Mod/ModResponPopup";
      public const string MailTypeSelect = "mail/MailTypePopup";
      public const string AltarSummonChoosePopup = "AltarSummon/AltarSummonChoosePopup";
      public const string LoginAnnouncement = "LoginAnnouncement/LoginAnnouncementPopup";
      public const string MerlinMysteryStorePopup = "MerlinTrials/MerlinMysteryStorePopup";
      public const string MerlinExchangePopup = "MerlinTrials/MerlinExchangePopup";
      public const string MerlinBuySuccessPopup = "MerlinTrials/MerlinBuySuccessPopup";
      public const string MerlinTrialsHistoryPopup = "MerlinTrials/MerlinTrialsHistoryPopup";
      public const string MerlinTrialsWarReportPopup = "MerlinTrials/MerlinTrialsWarReportPopup";
      public const string MerlinTrialsChestPopup = "MerlinTrials/MerlinTrialsChestPopup";
      public const string MerlinTrialsRewardPopup = "MerlinTrials/MerlinTrialsRewardPopup";
      public const string MerlinTrialsDetailPopup = "MerlinTrials/TrialDetailsPopup";
      public const string MerlinTrialsMonsterDetailPopup = "MerlinTrials/MonsterDetailPopup";
      public const string MerlinTrialsLevelCompletePopup = "MerlinTrials/MerlinLevelCompletePopup";
      public const string MerlinTowerBuffStorePopup = "MerlinTower/MerlinTowerBuffStorePopup";
      public const string MerlinTowerWarLogPop = "MerlinTower/MerlinTowerWarLogPop";
      public const string MerlinTowerViewBenefitsPopup = "MerlinTower/MerlinTowerViewBenefitsPopup";
      public const string MerlinTowerAttackShowPopup = "MerlinTower/MerlinTowerAttackShowPopup";
      public const string MerlinTowerSettingPopup = "MerlinTower/MerlinTowerSettingPopup";
      public const string MerlinTowerWarReportPopup = "MerlinTower/MerlinTowerWarReportPopup";
      public const string MerlinTowerGatherReportPopup = "Mail/MerlinTowerGatherReportPopup";
      public const string AllianceWarTrackPopup = "AllianceWar/AllianceWarTrackPopup";
      public const string AllianceWarScoreRewardPopup = "AllianceWar/AllianceWarScoreRewardPopup";
      public const string LordTitleTotalBenefitPopup = "LordTitle/LordTitleTotalBenefitPopup";
      public const string LordTitleDetailPopup = "LordTitle/LordTitleDetailPopup";
    }

    public struct SystemBlockerType
    {
      public const string fullScreenWait = "FullScreenWait";
      public const string commonSystemBlocker = "CommonBlocker";
      public const string netErrorBlocker = "NetErrorBlocker";
    }
  }
}
