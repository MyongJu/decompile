﻿// Decompiled with JetBrains decompiler
// Type: UI.UIControler
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace UI
{
  public class UIControler : MonoBehaviour
  {
    public UIControler.States _state = new UIControler.States();
    public bool isOcclusive;
    public bool isAutoOpen;

    public void Create(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(1, orgParam);
    }

    public void Open(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(2, orgParam);
    }

    public void Show(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(3, orgParam);
    }

    public void Focus(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(4, orgParam);
    }

    public void LoseFocus(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(3, orgParam);
    }

    public void Hide(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(2, orgParam);
    }

    public void Close(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(1, orgParam);
    }

    public void Destroy(UIControler.UIParameter orgParam = null)
    {
      this.ChangeState(0, orgParam);
    }

    public void ChangeState(int newState, UIControler.UIParameter orgParam = null)
    {
      if (newState == this._state.currentState)
        return;
      if (newState > this._state.currentState)
      {
        for (int currentState = this._state.currentState; currentState < newState; ++currentState)
          this._GetOutStateFunction_2Invalid(currentState)(orgParam);
      }
      else
      {
        for (int currentState = this._state.currentState; currentState > newState; --currentState)
          this._GetOutStateFunction_2Focus(currentState)(orgParam);
      }
      this._state.SetState(newState);
    }

    protected System.Action<UIControler.UIParameter> _GetOutStateFunction_2Invalid(int fromState)
    {
      switch (fromState)
      {
        case 0:
          return new System.Action<UIControler.UIParameter>(this._Create);
        case 1:
          return new System.Action<UIControler.UIParameter>(this._Open);
        case 2:
          return new System.Action<UIControler.UIParameter>(this._Show);
        case 3:
          return new System.Action<UIControler.UIParameter>(this._Focus);
        default:
          return (System.Action<UIControler.UIParameter>) null;
      }
    }

    protected System.Action<UIControler.UIParameter> _GetOutStateFunction_2Focus(int fromState)
    {
      switch (fromState)
      {
        case 1:
          return new System.Action<UIControler.UIParameter>(this._Destory);
        case 2:
          return new System.Action<UIControler.UIParameter>(this._Close);
        case 3:
          return new System.Action<UIControler.UIParameter>(this._Hide);
        case 4:
          return new System.Action<UIControler.UIParameter>(this._LoseFocus);
        default:
          return (System.Action<UIControler.UIParameter>) null;
      }
    }

    protected void _Create(UIControler.UIParameter orgParam)
    {
      this.OnCreate(orgParam);
      this.OnInit((UIControler.UIParameter) null);
    }

    protected virtual void _Open(UIControler.UIParameter orgParam)
    {
      this.isAutoOpen = UIManager.inst.AutoOpen;
      this.OnOpen(orgParam);
      OperationTrace.TraceOpenPage(this.gameObject.name, this.isAutoOpen);
    }

    protected virtual void _Show(UIControler.UIParameter orgParam)
    {
      if (this.isOcclusive)
        UIManager.inst.SetOcclusion(true);
      this.gameObject.SetActive(true);
      this.OnShow(orgParam);
    }

    protected void _Focus(UIControler.UIParameter orgParam)
    {
      this.OnFocus(orgParam);
    }

    protected void _LoseFocus(UIControler.UIParameter orgParam)
    {
      this.OnLoseFocus(orgParam);
    }

    protected virtual void _Hide(UIControler.UIParameter orgParam)
    {
      if (this.isOcclusive)
        UIManager.inst.SetOcclusion(false);
      this.OnHide(orgParam);
      this.gameObject.SetActive(false);
    }

    protected virtual void _Close(UIControler.UIParameter orgParam)
    {
      this.OnClose(orgParam);
      OperationTrace.TraceClosePage(this.gameObject.name, this.isAutoOpen);
      this.isAutoOpen = false;
    }

    protected void _Destory(UIControler.UIParameter orgParam)
    {
      this.OnFinalize(orgParam);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }

    public virtual void OnInit(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnCreate(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnOpen(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnShow(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnFocus(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnLoseFocus(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnHide(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnClose(UIControler.UIParameter orgParam = null)
    {
    }

    public virtual void OnFinalize(UIControler.UIParameter orgParam = null)
    {
    }

    public class States
    {
      public const int STATES_INVALID = 0;
      public const int STATES_UNSETUPED = 1;
      public const int STATES_SETUPED = 2;
      public const int STATES_DISPALYED = 3;
      public const int STATES_FOCUSED = 4;
      private int _state;

      public States()
      {
        this._state = 0;
      }

      public int currentState
      {
        get
        {
          return this._state;
        }
      }

      public bool isStatesInvalid
      {
        get
        {
          return this._state == 0;
        }
      }

      public bool isStatesUnsetuped
      {
        get
        {
          return this._state == 1;
        }
      }

      public bool isStatesSetuped
      {
        get
        {
          return this._state == 2;
        }
      }

      public bool isStatesDisplayed
      {
        get
        {
          return this._state == 3;
        }
      }

      public bool isStatesFocused
      {
        get
        {
          return this._state == 4;
        }
      }

      public void SetState(int state)
      {
        this._state = state;
      }
    }

    public class UIParameter
    {
    }
  }
}
