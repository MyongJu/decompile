﻿// Decompiled with JetBrains decompiler
// Type: UI.PriorityPopupQueue
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace UI
{
  public class PriorityPopupQueue
  {
    private List<PriorityPopupQueue.Record> m_Queue = new List<PriorityPopupQueue.Record>();

    public void OpenPopup(string popupType, Popup.PopupParameter param, int priority)
    {
      int index;
      for (index = 0; index < this.m_Queue.Count; ++index)
      {
        PriorityPopupQueue.Record record = this.m_Queue[index];
        if (priority >= record.Priority)
          break;
      }
      PriorityPopupQueue.Record record1 = new PriorityPopupQueue.Record();
      record1.Priority = priority;
      record1.Popup = UIManager.inst.OpenPopup(popupType, param);
      if ((bool) ((UnityEngine.Object) record1.Popup))
      {
        if (index == 0 && this.m_Queue.Count > 0)
          this.m_Queue[0].Popup.gameObject.SetActive(false);
        record1.Popup.onPriorityClose = new Action<Popup>(this.OnClose);
        record1.Popup.gameObject.SetActive(index == 0);
        this.m_Queue.Insert(index, record1);
      }
      if (this.m_Queue.Count <= 0)
        return;
      this.m_Queue[0].Popup.gameObject.SetActive(true);
      UIManager.inst.popupZBlockDepth = this.m_Queue[0].Popup.basePanelDepth - 1;
    }

    private void OnClose(Popup popup)
    {
      for (int index = 0; index < this.m_Queue.Count; ++index)
      {
        if (object.ReferenceEquals((object) this.m_Queue[index].Popup, (object) popup))
        {
          popup.onPriorityClose = (Action<Popup>) null;
          this.m_Queue.RemoveAt(index);
          break;
        }
      }
      if (this.m_Queue.Count <= 0)
        return;
      this.m_Queue[0].Popup.gameObject.SetActive(true);
      UIManager.inst.popupZBlockDepth = this.m_Queue[0].Popup.basePanelDepth - 1;
    }

    private class Record
    {
      public int Priority;
      public Popup Popup;
    }
  }
}
