﻿// Decompiled with JetBrains decompiler
// Type: UI.Dialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace UI
{
  public abstract class Dialog : UIControler
  {
    [HideInInspector]
    public string dialogType;
    [SerializeField]
    public Dialog.UIManagerPanel uiManagerPanel;

    public void SetButtons(bool showBack = true)
    {
      if ((UnityEngine.Object) this.uiManagerPanel.BT_Back != (UnityEngine.Object) null)
      {
        this.uiManagerPanel.BT_Back.gameObject.SetActive(showBack);
        if (this.uiManagerPanel.BT_Back.onClick.Count == 0)
          this.uiManagerPanel.BT_Back.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnBackButtonPress)));
      }
      if (!((UnityEngine.Object) this.uiManagerPanel.BT_Close != (UnityEngine.Object) null) || this.uiManagerPanel.BT_Close.onClick.Count != 0)
        return;
      this.uiManagerPanel.BT_Close.onClick.Add(new EventDelegate(new EventDelegate.Callback(this.OnCloseButtonPress)));
    }

    protected override void _Open(UIControler.UIParameter orgParam)
    {
      BuilderFactory.Instance.Register(this.dialogType, BuilderFactory.UIType.dialog);
      base._Open(orgParam);
    }

    protected override void _Close(UIControler.UIParameter orgParam)
    {
      BuilderFactory.Instance.Unregister(this.dialogType, BuilderFactory.UIType.dialog);
      base._Close(orgParam);
    }

    public override void OnShow(UIControler.UIParameter orgParam)
    {
      base.OnShow(orgParam);
      this.SetButtons(UIManager.inst.dialogStackCount > 1);
    }

    public virtual void OnBackButtonPress()
    {
      this.OnBackHandler();
      UIManager.inst.BackToPreDlg((Dialog.DialogParameter) null, (Dialog.DialogParameter) null);
    }

    public virtual void OnCloseButtonPress()
    {
      UIManager.inst.CloseDlg((Dialog.DialogParameter) null);
    }

    protected virtual void OnBackHandler()
    {
    }

    public void OnBackKeyPress()
    {
      if ((UnityEngine.Object) this.uiManagerPanel.BT_Back != (UnityEngine.Object) null)
        EventDelegate.Execute(this.uiManagerPanel.BT_Back.onClick);
      else
        UIManager.inst.BackToPreDlg((Dialog.DialogParameter) null, (Dialog.DialogParameter) null);
    }

    public void OnCloseKeyPress()
    {
      if ((UnityEngine.Object) this.uiManagerPanel.BT_Close != (UnityEngine.Object) null)
        EventDelegate.Execute(this.uiManagerPanel.BT_Close.onClick);
      else
        UIManager.inst.CloseDlg((Dialog.DialogParameter) null);
    }

    public class DialogParameter : UIControler.UIParameter
    {
    }

    [Serializable]
    public class UIManagerPanel
    {
      public UIButton BT_Back;
      public UIButton BT_Close;
    }
  }
}
