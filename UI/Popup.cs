﻿// Decompiled with JetBrains decompiler
// Type: UI.Popup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace UI
{
  public class Popup : UIControler
  {
    [HideInInspector]
    public string popupType;
    [HideInInspector]
    public int ID;
    public System.Action<Popup> onPriorityClose;

    public int basePanelDepth { get; private set; }

    public int maxPanelDepth { get; private set; }

    public void ResetPanelDepth(int basePanelDepth)
    {
      this.basePanelDepth = basePanelDepth;
      this.maxPanelDepth = int.MinValue;
      UIPanel[] componentsInChildren = this.gameObject.GetComponentsInChildren<UIPanel>(true);
      for (int index = 0; index < componentsInChildren.Length; ++index)
      {
        componentsInChildren[index].depth += basePanelDepth;
        this.maxPanelDepth = Math.Max(this.maxPanelDepth, componentsInChildren[index].depth);
      }
    }

    protected override void _Open(UIControler.UIParameter orgParam)
    {
      BuilderFactory.Instance.Register(this.popupType, BuilderFactory.UIType.popup);
      base._Open(orgParam);
    }

    protected override void _Close(UIControler.UIParameter orgParam)
    {
      BuilderFactory.Instance.Unregister(this.popupType, BuilderFactory.UIType.popup);
      base._Close(orgParam);
    }

    public override void OnClose(UIControler.UIParameter orgParam)
    {
      base.OnClose(orgParam);
    }

    public override void OnFinalize(UIControler.UIParameter orgParam = null)
    {
      base.OnFinalize(orgParam);
      if (this.onPriorityClose == null)
        return;
      this.onPriorityClose(this);
    }

    public virtual string Type
    {
      get
      {
        return nameof (Popup);
      }
    }

    public class PopupParameter : UIControler.UIParameter
    {
    }
  }
}
