﻿// Decompiled with JetBrains decompiler
// Type: ParticleSortingOrderModifier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParticleSortingOrderModifier : MonoBehaviour
{
  public string SortingLayerName = "Default";
  public int SortingOrder;
  private ParticleSystem[] m_Particles;

  private void Start()
  {
    this.m_Particles = this.GetComponentsInChildren<ParticleSystem>(true);
  }

  private void SetParticleSortingOrder()
  {
    if (this.m_Particles == null)
      return;
    int length = this.m_Particles.Length;
    for (int index = 0; index < length; ++index)
    {
      Renderer component = this.m_Particles[index].GetComponent<Renderer>();
      if ((Object) component != (Object) null)
      {
        component.sortingOrder = this.SortingOrder;
        component.sortingLayerName = this.SortingLayerName;
      }
    }
  }

  private void Update()
  {
    this.SetParticleSortingOrder();
  }
}
