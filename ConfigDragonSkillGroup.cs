﻿// Decompiled with JetBrains decompiler
// Type: ConfigDragonSkillGroup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigDragonSkillGroup
{
  private ConfigParse parse = new ConfigParse();
  private List<ConfigDragonSkillGroupInfo> skillGroups = new List<ConfigDragonSkillGroupInfo>();
  private List<ConfigDragonSkillGroupInfo> darkSkills = new List<ConfigDragonSkillGroupInfo>();
  private List<ConfigDragonSkillGroupInfo> lightSkills = new List<ConfigDragonSkillGroupInfo>();
  private Dictionary<string, ConfigDragonSkillGroupInfo> datas;
  private Dictionary<int, ConfigDragonSkillGroupInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    this.parse.Parse<ConfigDragonSkillGroupInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    using (Dictionary<string, ConfigDragonSkillGroupInfo>.Enumerator enumerator = this.datas.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, ConfigDragonSkillGroupInfo> current = enumerator.Current;
        this.skillGroups.Add(current.Value);
        if (current.Value.type == ConfigDragonSkillGroupInfo.DARK)
          this.darkSkills.Add(current.Value);
        else
          this.lightSkills.Add(current.Value);
      }
    }
    this.darkSkills.Sort(new Comparison<ConfigDragonSkillGroupInfo>(this.CompareItem));
    this.lightSkills.Sort(new Comparison<ConfigDragonSkillGroupInfo>(this.CompareItem));
  }

  private int CompareItem(ConfigDragonSkillGroupInfo a, ConfigDragonSkillGroupInfo b)
  {
    return a.priority.CompareTo(b.priority);
  }

  public ConfigDragonSkillGroupInfo GetDragonSkillGroupInfo(int internalId)
  {
    if (this.dicByUniqueId == null)
      return (ConfigDragonSkillGroupInfo) null;
    ConfigDragonSkillGroupInfo dragonSkillGroupInfo;
    this.dicByUniqueId.TryGetValue(internalId, out dragonSkillGroupInfo);
    return dragonSkillGroupInfo;
  }

  public ConfigDragonSkillGroupInfo GetDragonSkillGroupBySkillId(int skillId)
  {
    ConfigDragonSkillMainInfo skillMainInfo = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfo(skillId);
    if (skillMainInfo != null)
      return ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(skillMainInfo.group_id);
    return (ConfigDragonSkillGroupInfo) null;
  }

  public ConfigDragonSkillGroupInfo GetDragonSkillGroupInfo(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (ConfigDragonSkillGroupInfo) null;
  }

  public List<ConfigDragonSkillGroupInfo> GetDarkDragonSkillGroupInfo()
  {
    return this.darkSkills;
  }

  public List<ConfigDragonSkillGroupInfo> GetLightDragonSkillGroupInfo()
  {
    return this.lightSkills;
  }

  public bool CheckDragonSkillGroupIsUnLock(int groupId)
  {
    if (PlayerData.inst.dragonData == null)
      return false;
    ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(groupId);
    if (dragonSkillGroupInfo == null)
      return false;
    ConfigDragonSkillMainInfo infoOfGrounpByLevel = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfoOfGrounpByLevel(groupId, 1, false);
    if (infoOfGrounpByLevel == null)
      return false;
    if (dragonSkillGroupInfo.learn_method != 1)
      return infoOfGrounpByLevel.req_dragon_level <= PlayerData.inst.dragonData.Level;
    List<long> skillDataByGroupId = DBManager.inst.DB_DragonSkill.GetSkillDataByGroupId(groupId);
    if (skillDataByGroupId != null && skillDataByGroupId.Count > 0)
      return infoOfGrounpByLevel.req_dragon_level <= PlayerData.inst.dragonData.Level;
    return false;
  }

  public bool IsDragonLevelSatisfied(int groupId, int LevelUpPara = 1)
  {
    if (PlayerData.inst.dragonData == null)
      return false;
    ConfigDragonSkillMainInfo infoOfGrounpByLevel = ConfigManager.inst.DB_ConfigDragonSkillMain.GetSkillMainInfoOfGrounpByLevel(groupId, 1, false);
    if (infoOfGrounpByLevel == null)
      return false;
    return infoOfGrounpByLevel.req_dragon_level <= PlayerData.inst.dragonData.Level;
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, ConfigDragonSkillGroupInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, ConfigDragonSkillGroupInfo>) null;
  }
}
