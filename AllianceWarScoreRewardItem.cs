﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarScoreRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceWarScoreRewardItem : MonoBehaviour
{
  private readonly List<Icon> _allIcon = new List<Icon>();
  [SerializeField]
  private UILabel _labelScoreRequirement;
  [SerializeField]
  private UIGrid _rewardItemContainer;
  [SerializeField]
  private GameObject _rootCompleted;
  [SerializeField]
  private GameObject _rootUncompleted;
  [SerializeField]
  private GameObject _rootCollected;
  [SerializeField]
  private Icon _iconTemplate;
  private AllianceWarScoreRewardInfo _rewardInfo;

  private Icon CreateIcon()
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this._iconTemplate.gameObject);
    gameObject.transform.SetParent(this._rewardItemContainer.transform, false);
    gameObject.SetActive(true);
    Icon component = gameObject.GetComponent<Icon>();
    this._allIcon.Add(component);
    return component;
  }

  private void DestroyAllIcon()
  {
    using (List<Icon>.Enumerator enumerator = this._allIcon.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Icon current = enumerator.Current;
        if ((bool) ((UnityEngine.Object) current))
        {
          current.transform.SetParent((Transform) null);
          UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
        }
      }
    }
    this._allIcon.Clear();
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void Reset()
  {
    this._rootCollected.gameObject.SetActive(false);
    this._rootUncompleted.gameObject.SetActive(false);
    this._rootCompleted.gameObject.SetActive(false);
  }

  public void SetData(AllianceWarScoreRewardInfo rewardInfo)
  {
    this.Reset();
    this._rewardInfo = rewardInfo;
    this._labelScoreRequirement.text = rewardInfo.ScoreReq.ToString();
    this.DestroyAllIcon();
    using (List<Reward.RewardsValuePair>.Enumerator enumerator = this._rewardInfo.Rewards.GetRewards().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Reward.RewardsValuePair current = enumerator.Current;
        IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(current.internalID).ImagePath, new string[1]
        {
          current.value.ToString()
        })
        {
          Data = (object) current.internalID
        };
        Icon icon = this.CreateIcon();
        icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
        icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
        icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
        icon.StopAutoFillName();
        icon.FeedData((IComponentData) iconData);
      }
    }
    if (AllianceWarPayload.Instance.UserScore >= rewardInfo.ScoreReq)
    {
      if (AllianceWarPayload.Instance.CliamedRewardList.Contains(rewardInfo.ID))
        this._rootCollected.gameObject.SetActive(true);
      else
        this._rootCompleted.gameObject.SetActive(true);
    }
    else
      this._rootUncompleted.gameObject.SetActive(true);
    this._rewardItemContainer.Reposition();
  }

  public void OnButtonCollectClicked()
  {
    Debug.Log((object) nameof (OnButtonCollectClicked));
    Hashtable postData = new Hashtable();
    postData.Add((object) "reward_id", (object) this._rewardInfo.ID);
    string collectId = this._rewardInfo.ID;
    RequestManager.inst.SendRequest("AC:gainScoreReward", postData, (System.Action<bool, object>) ((result, data) =>
    {
      if (!result)
        return;
      List<string> cliamedRewardList = AllianceWarPayload.Instance.CliamedRewardList;
      if (!cliamedRewardList.Contains(collectId))
        cliamedRewardList.Add(collectId);
      RewardsCollectionAnimator.Instance.Clear();
      List<Reward.RewardsValuePair> rewards = this._rewardInfo.Rewards.GetRewards();
      for (int index = 0; index < rewards.Count; ++index)
      {
        Reward.RewardsValuePair rewardsValuePair = rewards[index];
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
        RewardsCollectionAnimator.Instance.items.Add(new ItemRewardInfo.Data()
        {
          icon = itemStaticInfo.ImagePath,
          count = (float) rewardsValuePair.value
        });
      }
      RewardsCollectionAnimator.Instance.CollectItems(false);
      this.SetData(this._rewardInfo);
    }), true);
  }
}
