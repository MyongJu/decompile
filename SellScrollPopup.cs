﻿// Decompiled with JetBrains decompiler
// Type: SellScrollPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;

public class SellScrollPopup : SellItemBasePopup
{
  public UITexture background;
  public UILabel woodLabel;
  private ConfigEquipmentScrollInfo info;

  public override void OnProgressValueChanged(int count)
  {
    base.OnProgressValueChanged(count);
    this.woodLabel.text = (this.info.sellWood * count).ToString();
  }

  public void UpdateScrollInfo(ConfigEquipmentScrollInfo info)
  {
    this.info = info;
    BuilderFactory.Instance.Build((UIWidget) this.background, info.QualityImagePath, (System.Action<bool>) null, true, false, true, string.Empty);
    BuilderFactory.Instance.Build((UIWidget) this.image, info.ImagePath(this.bagType), (System.Action<bool>) null, true, false, true, string.Empty);
    Utils.SetItemNormalBackground(this.background, ConfigManager.inst.DB_Items.GetItem(info.itemID).Quality);
    this.woodLabel.text = (info.sellWood * this.progress.CurrentCount).ToString();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.background);
    BuilderFactory.Instance.Release((UIWidget) this.image);
    base.OnHide(orgParam);
  }

  public void OnSellClick()
  {
    RewardsCollectionAnimator.Instance.Ress.Add(new ResRewardsInfo.Data()
    {
      rt = ResRewardsInfo.ResType.Wood,
      count = int.Parse(this.woodLabel.text)
    });
    ItemBag.Instance.SellItem(ConfigManager.inst.GetEquipmentScroll(this.bagType).GetDataByItemID(this.itemInfo.internalId).internalId, this.progress.CurrentCount, (System.Action<bool, object>) ((arg1, arg2) =>
    {
      if (!arg1)
        return;
      RewardsCollectionAnimator.Instance.CollectResource(false);
      UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    }));
  }
}
