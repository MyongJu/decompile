﻿// Decompiled with JetBrains decompiler
// Type: MonsterIdleOrStretchStateAction
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MonsterIdleOrStretchStateAction : StateMachineBehaviour
{
  public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    if (!(bool) ((Object) animator))
      return;
    if ((double) Random.Range(0.0f, 100f) < 10.0)
      animator.CrossFadeInFixedTime("Stretch", 0.01f);
    else
      animator.CrossFadeInFixedTime("Idle", 0.01f);
  }
}
