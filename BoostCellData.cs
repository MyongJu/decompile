﻿// Decompiled with JetBrains decompiler
// Type: BoostCellData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class BoostCellData
{
  public BoostItemData sum = new BoostItemData();
  public List<BoostItemData> items = new List<BoostItemData>();
  public string key;

  public static BoostCellData CreateRandom()
  {
    return new BoostCellData()
    {
      sum = {
        title = Random.Range(1f, float.MaxValue).ToString(),
        leftTime = (int) Random.Range(1f, float.MaxValue),
        basicEffect = Random.Range(1f, float.MaxValue),
        tempEffect = Random.Range(1f, float.MaxValue)
      },
      items = {
        BoostItemData.CreateRandom(),
        BoostItemData.CreateRandom(),
        BoostItemData.CreateRandom()
      }
    };
  }

  public static BoostCellData CreateFromInfo(string key, BenefitInfo info)
  {
    BoostCellData boostCellData = new BoostCellData();
    boostCellData.key = key;
    boostCellData.sum.title = BoostsConst.Key2Title[key];
    boostCellData.sum.basicEffect = info.total;
    boostCellData.sum.tempEffect = 0.0f;
    boostCellData.sum.leftTime = 0;
    if ((double) info.hero.value != 0.0)
      boostCellData.items.Add(BoostItemData.CreateFromInfo(BoostItemData.Type.Hero, info.hero));
    if (info.items != null)
    {
      for (int index = 0; index < info.items.Length; ++index)
      {
        if ((double) info.items[index].value != 0.0)
          boostCellData.items.Add(BoostItemData.CreateFromInfo(BoostItemData.Type.Item, info.items[index]));
      }
    }
    if ((double) info.research.value != 0.0)
      boostCellData.items.Add(BoostItemData.CreateFromInfo(BoostItemData.Type.Research, info.research));
    return boostCellData;
  }

  public void Clear()
  {
  }
}
