﻿// Decompiled with JetBrains decompiler
// Type: ContactManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections.Generic;
using UI;

public class ContactManager
{
  public static void AddContact(long uid, System.Action<bool, object> callback = null)
  {
    if (uid == PlayerData.inst.uid)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = "Can't add yourself to contact.",
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    else if (DBManager.inst.DB_Contacts.friends.Contains(uid))
    {
      string userName = DBManager.inst.DB_User.Get(uid).userName;
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = ScriptLocalization.GetWithPara("chat_contacts_already_added_description", new Dictionary<string, string>()
        {
          {
            "0",
            userName
          }
        }, true),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }
    else if (DBManager.inst.DB_Contacts.beBlocked.Contains(uid) || DBManager.inst.DB_Contacts.blocking.Contains(uid))
    {
      string userName = DBManager.inst.DB_User.Get(uid).userName;
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = ScriptLocalization.GetWithPara("chat_contacts_already_blocked_description", new Dictionary<string, string>()
        {
          {
            "0",
            userName
          }
        }, true),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }
    else if ((double) DBManager.inst.DB_Contacts.friends.Count >= ConfigManager.inst.DB_GameConfig.GetData("contact_friend_limit_value").Value)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = Utils.XLAT("chat_contacts_list_full_description"),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    else
      ContactManager.AddOrRemoveContact(uid, true, callback);
  }

  public static void RemoveContact(long uid, System.Action<bool, object> callback = null)
  {
    ContactManager.AddOrRemoveContact(uid, false, callback);
  }

  private static void AddOrRemoveContact(long uid, bool add, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Player:addOrRemoveContacts").SendRequest(Utils.Hash((object) "contact_uid", (object) uid, (object) "operate", !add ? (object) "remove" : (object) nameof (add)), callback, true);
  }

  public static void AcceptInvited(long uid, System.Action<bool, object> callback = null)
  {
    ContactManager.AcceptOrDenyInvited(uid, true, callback);
  }

  public static void DenyInvited(long uid, System.Action<bool, object> callback = null)
  {
    ContactManager.AcceptOrDenyInvited(uid, false, callback);
  }

  private static void AcceptOrDenyInvited(long uid, bool accept, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Player:acceptOrDenyContactsInvite").SendRequest(Utils.Hash((object) "contact_uid", (object) uid, (object) "operate", !accept ? (object) "deny" : (object) nameof (accept)), callback, true);
  }

  public static void BlockUser(long uid, System.Action<bool, object> callback = null)
  {
    if (uid == PlayerData.inst.uid)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = "Can't Block yourself to contact.",
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    else if (DBManager.inst.DB_Contacts.blocking.Contains(uid))
    {
      string userName = DBManager.inst.DB_User.Get(uid).userName;
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = ScriptLocalization.GetWithPara("chat_contacts_already_blocked_description", new Dictionary<string, string>()
        {
          {
            "0",
            userName
          }
        }, true),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    }
    else if ((double) DBManager.inst.DB_Contacts.blocking.Count >= ConfigManager.inst.DB_GameConfig.GetData("contact_block_limit_value").Value)
      UIManager.inst.OpenPopup("MessageBoxWith1Button", (Popup.PopupParameter) new MessageBoxWith1Button.Parameter()
      {
        Title = Utils.XLAT("chat_contacts_error_popup_title"),
        Content = Utils.XLAT("chat_contacts_blocked_list_full_description"),
        Okay = Utils.XLAT("id_uppercase_okay"),
        OkayCallback = (System.Action) null
      });
    else
      ContactManager.BlockOrUnblockUser(uid, true, callback);
  }

  public static void UnblockUser(long uid, System.Action<bool, object> callback = null)
  {
    ContactManager.BlockOrUnblockUser(uid, false, callback);
  }

  private static void BlockOrUnblockUser(long uid, bool block, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Player:blockOrUnblockContacts").SendRequest(Utils.Hash((object) "contact_uid", (object) uid, (object) "operate", !block ? (object) "unblock" : (object) nameof (block)), callback, true);
  }
}
