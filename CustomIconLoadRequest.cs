﻿// Decompiled with JetBrains decompiler
// Type: CustomIconLoadRequest
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CustomIconLoadRequest
{
  protected int RequestFrame;
  protected UITexture m_targetUITexture;
  protected Material m_targetMaterial;
  protected string m_remoteIcon;
  protected string m_localIconPath;
  protected bool m_localIconSetted;
  protected bool m_alreadySetted;
  protected Texture m_texture;
  protected bool m_localTextureLoaded;

  public CustomIconLoadRequest(Material targetMateiral, string localPath, string remoteIcon, bool needLoadLocal)
  {
    this.m_localTextureLoaded = false;
    this.m_localIconPath = localPath;
    this.m_alreadySetted = false;
    this.m_texture = (Texture) null;
    this.m_targetMaterial = targetMateiral;
    this.m_remoteIcon = remoteIcon;
    if (needLoadLocal)
    {
      Texture texture = AssetManager.Instance.Load(this.m_localIconPath, typeof (Texture)) as Texture;
      if ((bool) ((UnityEngine.Object) texture))
      {
        this.m_localTextureLoaded = true;
        this.m_targetMaterial.mainTexture = texture;
      }
      this.m_localIconSetted = true;
    }
    else
      this.m_localIconSetted = true;
    this.RequestCanceled = false;
    this.RequestFrame = Time.frameCount;
  }

  public CustomIconLoadRequest(UITexture targetUITexture, string localPath, string remoteIcon, bool needLoadLocal)
  {
    this.m_localTextureLoaded = false;
    this.m_localIconPath = localPath;
    this.m_alreadySetted = false;
    this.m_texture = (Texture) null;
    this.m_targetUITexture = targetUITexture;
    this.m_remoteIcon = remoteIcon;
    BuilderFactory.Instance.Release((UIWidget) targetUITexture);
    if (needLoadLocal)
    {
      this.m_localIconSetted = false;
      BuilderFactory.Instance.HandyBuild((UIWidget) targetUITexture, localPath, new System.Action<bool>(this.OnLocalIconSetted), true, false, string.Empty);
    }
    else
      this.m_localIconSetted = true;
    this.RequestCanceled = false;
    this.RequestFrame = Time.frameCount;
  }

  public bool RequestCanceled { set; get; }

  public Texture texture
  {
    set
    {
      this.m_alreadySetted = true;
      this.m_texture = value;
      if ((bool) ((UnityEngine.Object) this.m_targetUITexture))
        this.m_targetUITexture.mainTexture = value;
      if ((bool) ((UnityEngine.Object) this.m_targetMaterial))
        this.m_targetMaterial.mainTexture = value;
      if (!this.m_localTextureLoaded)
        return;
      AssetManager.Instance.UnLoadAsset(this.m_localIconPath, (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    }
  }

  public string Url
  {
    get
    {
      return this.m_remoteIcon;
    }
  }

  public bool LocalIconSetted
  {
    get
    {
      return this.m_localIconSetted;
    }
  }

  protected void OnLocalIconSetted(bool result)
  {
    this.m_localIconSetted = true;
  }

  public void DebugOutput()
  {
    string str = string.Empty;
    if ((bool) ((UnityEngine.Object) this.m_targetUITexture))
      str = string.Format("<color=yellow>{0},{1},{2},{3}</color>", (object) this.m_targetUITexture.GetInstanceID(), (object) this.m_localIconPath, (object) this.m_remoteIcon, (object) this.RequestFrame);
    if ((bool) ((UnityEngine.Object) this.m_targetMaterial))
      str = string.Format("<color=yellow>{0},{1},{2},{3}</color>", (object) this.m_targetMaterial.GetInstanceID(), (object) this.m_localIconPath, (object) this.m_remoteIcon, (object) this.RequestFrame);
    Debug.Log((object) str);
  }

  public UITexture TargetUITexture
  {
    get
    {
      return this.m_targetUITexture;
    }
  }

  public Material TargetMaterial
  {
    get
    {
      return this.m_targetMaterial;
    }
  }

  public bool isRequestValid()
  {
    return !this.RequestCanceled && ((bool) ((UnityEngine.Object) this.m_targetUITexture) || (bool) ((UnityEngine.Object) this.m_targetMaterial)) && (!this.m_alreadySetted || !(bool) ((UnityEngine.Object) this.m_targetUITexture) || !((UnityEngine.Object) this.m_targetUITexture.mainTexture != (UnityEngine.Object) this.m_texture)) && (!this.m_alreadySetted || !(bool) ((UnityEngine.Object) this.m_targetMaterial) || !((UnityEngine.Object) this.m_targetMaterial.mainTexture != (UnityEngine.Object) this.m_texture));
  }
}
