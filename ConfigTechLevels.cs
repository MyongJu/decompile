﻿// Decompiled with JetBrains decompiler
// Type: ConfigTechLevels
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using System.Collections.Generic;

public class ConfigTechLevels
{
  private static readonly int REQUIREMENT_COUNT = 4;
  private static readonly int EFFECT_COUNT = 5;
  private Dictionary<int, TechLevelData> TechLevels = new Dictionary<int, TechLevelData>();
  private Dictionary<string, int> TechLevelIDs = new Dictionary<string, int>();

  public void BuildDB(object res)
  {
    Hashtable hashtable = res as Hashtable;
    if (hashtable == null)
    {
      D.error((object) "{0} load error - invalid hashtable", (object) this.GetType());
    }
    else
    {
      ArrayList inHeader = hashtable[(object) "headers"] as ArrayList;
      if (inHeader == null)
      {
        D.error((object) "{0} load error - invalid header hashtable", (object) this.GetType());
      }
      else
      {
        int index1 = ConfigManager.GetIndex(inHeader, "id");
        int index2 = ConfigManager.GetIndex(inHeader, "researches_id");
        int index3 = ConfigManager.GetIndex(inHeader, "level");
        int index4 = ConfigManager.GetIndex(inHeader, "duration");
        int index5 = ConfigManager.GetIndex(inHeader, "power");
        int index6 = ConfigManager.GetIndex(inHeader, "food");
        int index7 = ConfigManager.GetIndex(inHeader, "wood");
        int index8 = ConfigManager.GetIndex(inHeader, "ore");
        int index9 = ConfigManager.GetIndex(inHeader, "silver");
        int index10 = ConfigManager.GetIndex(inHeader, "req_id_1");
        int index11 = ConfigManager.GetIndex(inHeader, "req_id_2");
        int index12 = ConfigManager.GetIndex(inHeader, "loc_research_name_id");
        int index13 = ConfigManager.GetIndex(inHeader, "loc_research_description_id");
        int[] numArray1 = new int[ConfigTechLevels.REQUIREMENT_COUNT];
        numArray1[0] = ConfigManager.GetIndex(inHeader, "req_id_3");
        numArray1[1] = ConfigManager.GetIndex(inHeader, "req_id_4");
        numArray1[2] = ConfigManager.GetIndex(inHeader, "req_id_5");
        numArray1[3] = ConfigManager.GetIndex(inHeader, "req_id_6");
        int[] numArray2 = new int[ConfigTechLevels.EFFECT_COUNT];
        numArray2[0] = ConfigManager.GetIndex(inHeader, "benefit_id_1");
        numArray2[1] = ConfigManager.GetIndex(inHeader, "benefit_id_2");
        numArray2[2] = ConfigManager.GetIndex(inHeader, "benefit_id_3");
        numArray2[3] = ConfigManager.GetIndex(inHeader, "benefit_id_4");
        numArray2[4] = ConfigManager.GetIndex(inHeader, "benefit_id_5");
        int[] numArray3 = new int[ConfigTechLevels.EFFECT_COUNT];
        numArray3[0] = ConfigManager.GetIndex(inHeader, "benefit_value_1");
        numArray3[1] = ConfigManager.GetIndex(inHeader, "benefit_value_2");
        numArray3[2] = ConfigManager.GetIndex(inHeader, "benefit_value_3");
        numArray3[3] = ConfigManager.GetIndex(inHeader, "benefit_value_4");
        numArray3[4] = ConfigManager.GetIndex(inHeader, "benefit_value_5");
        hashtable.Remove((object) "headers");
        foreach (object key in (IEnumerable) hashtable.Keys)
        {
          int result;
          if (int.TryParse(key.ToString(), out result))
          {
            ArrayList arr = hashtable[key] as ArrayList;
            if (arr != null)
            {
              TechLevelData techLevelData = new TechLevelData(result, ConfigManager.GetString(arr, index1), ConfigManager.GetInt(arr, index5), ConfigManager.GetInt(arr, index4), ConfigManager.GetInt(arr, index2), ConfigManager.GetInt(arr, index3), ConfigManager.GetString(arr, index12), ConfigManager.GetString(arr, index13));
              if (ConfigManager.GetInt(arr, index10) > 0)
                techLevelData.RequiredBuildingLevels.Add(ConfigManager.GetInt(arr, index10));
              if (ConfigManager.GetInt(arr, index11) > 0)
                techLevelData.RequiredBuildingLevels.Add(ConfigManager.GetInt(arr, index11));
              for (int index14 = 0; index14 < ConfigTechLevels.REQUIREMENT_COUNT; ++index14)
              {
                if (ConfigManager.GetInt(arr, numArray1[index14]) > 0)
                  techLevelData.RequiredTechLevels.Add(ConfigManager.GetInt(arr, numArray1[index14]));
              }
              techLevelData.Costs.Add(new KeyValuePair<CityResourceInfo.ResourceType, int>(CityResourceInfo.ResourceType.SILVER, ConfigManager.GetInt(arr, index9)));
              techLevelData.Costs.Add(new KeyValuePair<CityResourceInfo.ResourceType, int>(CityResourceInfo.ResourceType.FOOD, ConfigManager.GetInt(arr, index6)));
              techLevelData.Costs.Add(new KeyValuePair<CityResourceInfo.ResourceType, int>(CityResourceInfo.ResourceType.WOOD, ConfigManager.GetInt(arr, index7)));
              techLevelData.Costs.Add(new KeyValuePair<CityResourceInfo.ResourceType, int>(CityResourceInfo.ResourceType.ORE, ConfigManager.GetInt(arr, index8)));
              for (int index14 = 0; index14 < ConfigTechLevels.EFFECT_COUNT; ++index14)
              {
                if (ConfigManager.GetString(arr, numArray2[index14]).Length > 0)
                  techLevelData.Effects.Add(new Effect(ConfigManager.GetInt(arr, numArray2[index14]), 0, ConfigManager.GetFloat(arr, numArray3[index14]), Effect.SourceType.Research, (long) techLevelData.TechInternalID, 0, Effect.ConditionType.all));
              }
              this.Add(techLevelData);
            }
          }
        }
      }
    }
  }

  public Dictionary<int, TechLevelData>.ValueCollection Values
  {
    get
    {
      return this.TechLevels.Values;
    }
  }

  private void Add(TechLevelData techLevelData)
  {
    if (this.TechLevels.ContainsKey(techLevelData.InternalID))
      return;
    this.TechLevels.Add(techLevelData.InternalID, techLevelData);
    if (!this.TechLevelIDs.ContainsKey(techLevelData.ID))
      this.TechLevelIDs.Add(techLevelData.ID, techLevelData.InternalID);
    ConfigManager.inst.AddData(techLevelData.InternalID, (object) techLevelData);
  }

  public void Clear()
  {
    if (this.TechLevels != null)
      this.TechLevels.Clear();
    if (this.TechLevelIDs == null)
      return;
    this.TechLevelIDs.Clear();
  }

  public TechLevelData this[int internalID]
  {
    get
    {
      return this.GetData(internalID);
    }
  }

  public TechLevelData this[string ID]
  {
    get
    {
      return this.GetData(ID);
    }
  }

  private TechLevelData GetData(int internalID)
  {
    if (this.TechLevels.ContainsKey(internalID))
      return this.TechLevels[internalID];
    return (TechLevelData) null;
  }

  private TechLevelData GetData(string ID)
  {
    if (this.TechLevelIDs.ContainsKey(ID))
      return this.TechLevels[this.TechLevelIDs[ID]];
    return (TechLevelData) null;
  }
}
