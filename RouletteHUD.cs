﻿// Decompiled with JetBrains decompiler
// Type: RouletteHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UI;
using UnityEngine;

public class RouletteHUD : MonoBehaviour
{
  private string m_VfxName = string.Empty;
  private const string VFX_PATH = "Prefab/Roulette/";
  private const string VFX_NAME = "roulette_theme";
  public GameObject rootNode;
  public UILabel remainTime;
  private GameObject m_VFX;

  private void Start()
  {
    this.UpdateUI();
  }

  private void OnEnable()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondEvent);
  }

  private void OnDisable()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondEvent);
  }

  private void OnSecondEvent(int serverTimestamp)
  {
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    if (this.ShouldShowEntrance())
    {
      this.rootNode.gameObject.SetActive(true);
      this.SetVFX("roulette_theme");
      this.remainTime.text = Utils.FormatTime(RoulettePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp, true, false, true);
    }
    else
    {
      this.rootNode.gameObject.SetActive(false);
      this.DeleteVFX();
    }
  }

  private bool ShouldShowEntrance()
  {
    bool flag = false;
    if (!TutorialManager.Instance.IsRunning)
      flag = RoulettePayload.Instance.IsOpen && RoulettePayload.Instance.EndTime - NetServerTime.inst.ServerTimestamp >= 0 && !RoulettePayload.Instance.IsRouletteClose && !RoulettePayload.Instance.IsRoulettePaused;
    return flag;
  }

  public void OnRouletteButtonClicked()
  {
    UIManager.inst.OpenDlg("Roulette/TurnableDlg", (UI.Dialog.DialogParameter) null, true, true, true);
    OperationTrace.TraceClick(ClickArea.Roulette);
  }

  private void SetVFX(string vfxName)
  {
    if (!(this.m_VfxName != vfxName))
      return;
    this.DeleteVFX();
    this.m_VfxName = vfxName;
    this.m_VFX = AssetManager.Instance.HandyLoad("Prefab/Roulette/" + vfxName, typeof (GameObject)) as GameObject;
    if (!(bool) ((UnityEngine.Object) this.m_VFX))
      return;
    this.m_VFX = UnityEngine.Object.Instantiate<GameObject>(this.m_VFX);
    this.m_VFX.transform.parent = this.rootNode.transform;
    this.m_VFX.transform.localPosition = Vector3.zero;
    this.m_VFX.transform.localScale = Vector3.one;
  }

  private void DeleteVFX()
  {
    this.m_VfxName = string.Empty;
    if (!((UnityEngine.Object) this.m_VFX != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.m_VFX);
    this.m_VFX = (GameObject) null;
  }
}
