﻿// Decompiled with JetBrains decompiler
// Type: DicePlayDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DicePlayDlg : UI.Dialog
{
  private Dictionary<int, DiceRewardSlot> _itemDict = new Dictionary<int, DiceRewardSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private string[] LEFT_DICE_STATES = new string[6]
  {
    "2_1",
    "2_2",
    "2_3",
    "2_4",
    "2_5",
    "2_6"
  };
  private string[] RIGHT_DICE_STATES = new string[6]
  {
    "1_1",
    "1_2",
    "1_3",
    "1_4",
    "1_5",
    "1_6"
  };
  private int _firstLeftDiceNum = -1;
  private int _firstRightDiceNum = -1;
  private Dictionary<string, string> _scoreParam = new Dictionary<string, string>();
  private bool _doOnce = true;
  private bool _canPlayTen = true;
  [SerializeField]
  private UILabel _labelTitle;
  [SerializeField]
  private UILabel _labelPropsCount;
  [SerializeField]
  private UITexture _textureProps;
  [SerializeField]
  private UILabel[] _labelRewardNumerals;
  [SerializeField]
  private UITexture _textureNpc;
  [SerializeField]
  private UIButton _buttonRefresh;
  [SerializeField]
  private UIButton _buttonRefreshFree;
  [SerializeField]
  private UIButton _buttonPlayOne;
  [SerializeField]
  private UIButton _buttonPlayOneFree;
  [SerializeField]
  private UIButton _buttonPlayTen;
  [SerializeField]
  private UILabel _labelRefreshCost;
  [SerializeField]
  private UILabel _labelPlayOneCost;
  [SerializeField]
  private UILabel _labelPlayTenCost;
  [SerializeField]
  private UILabel _labelScore;
  [SerializeField]
  private UITexture _texturePlayOneProps;
  [SerializeField]
  private UITexture _texturePlayTenProps;
  [SerializeField]
  private GameObject _itemPrefab;
  [SerializeField]
  private Transform[] _iconRewardNodes;
  [SerializeField]
  private Animator _leftDiceAnimator;
  [SerializeField]
  private Animator _rightDiceAnimator;
  [SerializeField]
  private GameObject _scoreHintNode;
  [SerializeField]
  private GameObject _freeRewardHintNode;
  private Color _refreshCostTextColor;
  private Color _dicePlayCostTextColor;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    if (this._firstLeftDiceNum < 0 || this._firstRightDiceNum < 0)
    {
      this._firstLeftDiceNum = NGUITools.RandomRange(0, 5);
      this._firstRightDiceNum = NGUITools.RandomRange(0, 5);
    }
    this._refreshCostTextColor = this._labelRefreshCost.color;
    this._dicePlayCostTextColor = this._labelPlayOneCost.color;
    this.ResetPlayTenStatus();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  private void Start()
  {
    UIViewport componentInChildren = this.GetComponentInChildren<UIViewport>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    componentInChildren.sourceCamera = UIManager.inst.ui2DCamera;
  }

  public void OnPropsBuyPressed()
  {
    Utils.ShowIAPStore((UI.Dialog.DialogParameter) null);
  }

  public void OnFreeRewardPressed()
  {
    UIManager.inst.OpenPopup("Dice/DiceFreeRewardPopup", (Popup.PopupParameter) new DiceFreeRewardPopup.Parameter()
    {
      freeClaimedHandler = (System.Action) (() => this.ShowMainContent(false))
    });
  }

  public void OnRefreshPressed()
  {
    if (DicePayload.Instance.IsRefreshFree || DicePayload.Instance.HasEnoughGold2Refresh)
      DicePayload.Instance.RefreshDiceData((System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        this.RefreshRewards();
      }));
    else
      this.OnPropsBuyPressed();
  }

  public void OnRewardViewPressed()
  {
    UIManager.inst.OpenPopup("Dice/DiceAllRewardPopup", (Popup.PopupParameter) null);
  }

  public void OnScorePressed()
  {
    UIManager.inst.OpenPopup("Dice/DicePointExchangePopup", (Popup.PopupParameter) new DicePointExchangePopup.Parameter()
    {
      onPanelHide = (System.Action) (() => this.ShowMainContent(true))
    });
  }

  public void OnOnePlayPressed()
  {
    if (DicePayload.Instance.IsDiceOneFree || DicePayload.Instance.TempDiceData.PropsCount >= (long) DicePayload.Instance.DiceOneCost)
      this.PlayDice(1);
    else
      this.ShowGetMore();
  }

  public void OnTenPlayPressed()
  {
    if (DicePayload.Instance.TempDiceData.PropsCount >= (long) DicePayload.Instance.DiceTenCost)
    {
      if (!this._canPlayTen)
        return;
      this._buttonPlayTen.isEnabled = this._canPlayTen = false;
      this.PlayDice(10);
    }
    else
      this.ShowGetMore();
  }

  private void PlayDice(int playCount)
  {
    DicePayload.Instance.PlayDice(playCount, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret || data == null)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null && hashtable.ContainsKey((object) "reward"))
      {
        ArrayList arrayList = hashtable[(object) "reward"] as ArrayList;
        if (arrayList != null)
        {
          List<DiceData.RewardData> rewardDataList = new List<DiceData.RewardData>();
          for (int index = 0; index < arrayList.Count; ++index)
            rewardDataList.Add(new DiceData.RewardData(arrayList[index] as Hashtable));
          this.ShowDiceAnimation(DicePayload.Instance.GetLeftDiceNum(rewardDataList[rewardDataList.Count - 1]), rewardDataList[rewardDataList.Count - 1].Multiple);
          if (rewardDataList.Count > 0)
            this.StartCoroutine(this.ShowCollectEffect(rewardDataList, playCount));
        }
      }
      this.ShowMainContent(false);
    }));
  }

  private void ShowDiceAnimation(int leftDiceNum, int rightDiceNum)
  {
    if (leftDiceNum <= 0 || leftDiceNum > 6 || (rightDiceNum <= 0 || rightDiceNum > 6))
      return;
    Quaternion localRotation1 = this._leftDiceAnimator.transform.localRotation;
    Quaternion localRotation2 = this._rightDiceAnimator.transform.localRotation;
    this._leftDiceAnimator.transform.localRotation = Quaternion.Euler(localRotation1.x, localRotation1.y, (float) NGUITools.RandomRange(-180, 180));
    this._rightDiceAnimator.transform.localRotation = Quaternion.Euler(localRotation2.x, localRotation2.y, (float) NGUITools.RandomRange(-180, 180));
    this._firstLeftDiceNum = leftDiceNum - 1;
    this._firstRightDiceNum = rightDiceNum - 1;
    this._leftDiceAnimator.Play(this.LEFT_DICE_STATES[this._firstLeftDiceNum], 0, 0.0f);
    this._rightDiceAnimator.Play(this.RIGHT_DICE_STATES[this._firstRightDiceNum], 0, 0.0f);
    AudioManager.Instance.StopAndPlaySound("sfx_city_dice_roll");
  }

  [DebuggerHidden]
  private IEnumerator ShowCollectEffect(List<DiceData.RewardData> rewardDataList, int playedCount)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DicePlayDlg.\u003CShowCollectEffect\u003Ec__Iterator4C()
    {
      playedCount = playedCount,
      rewardDataList = rewardDataList,
      \u003C\u0024\u003EplayedCount = playedCount,
      \u003C\u0024\u003ErewardDataList = rewardDataList,
      \u003C\u003Ef__this = this
    };
  }

  private void ResetPlayTenStatus()
  {
    this._buttonPlayTen.isEnabled = this._canPlayTen = true;
  }

  [DebuggerHidden]
  private IEnumerator UpdateScoreText()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DicePlayDlg.\u003CUpdateScoreText\u003Ec__Iterator4D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ShowGetMore()
  {
    UIManager.inst.OpenPopup("Marksman/CommonConfirmPopup", (Popup.PopupParameter) new CommonConfirmPopup.Parameter()
    {
      title = Utils.XLAT("id_uppercase_insufficient_items"),
      content = Utils.XLAT("tavern_dice_insufficient_chips_description"),
      confirmButtonText = Utils.XLAT("id_uppercase_get_more"),
      onOkayCallback = new System.Action(this.OnPropsBuyPressed)
    });
  }

  private void RefreshRewards()
  {
    this.ShowMainContent(true);
    this.ShowRewardContent();
  }

  private void UpdateUI()
  {
    this.ShowMainContent(true);
    this.ShowRewardContent();
    this.ShowDiceContent();
  }

  private void TestWinTen()
  {
    List<DiceData.RewardData> rewardDataList = new List<DiceData.RewardData>();
    for (int index = 0; index < 10; ++index)
    {
      DiceData.RewardData rewardData = new DiceData.RewardData(Utils.Hash((object) "type", (object) 1, (object) "amount", (object) (90 + index), (object) "multiple", (object) 5));
      rewardDataList.Add(rewardData);
    }
    UIManager.inst.OpenPopup("Dice/DiceTenWinPopup", (Popup.PopupParameter) new DiceTenWinPopup.Parameter()
    {
      rewardDataList = rewardDataList
    });
  }

  private void ShowMainContent(bool updateScoreText = true)
  {
    this._labelTitle.text = DicePayload.Instance.Title;
    this._labelPropsCount.text = Utils.FormatThousands(DicePayload.Instance.TempDiceData.PropsCount.ToString());
    if (this._doOnce)
    {
      this._doOnce = false;
      MarksmanPayload.Instance.FeedMarksmanTexture(this._textureProps, DicePayload.Instance.TempDiceData.PropsImagePath);
      MarksmanPayload.Instance.FeedMarksmanTexture(this._textureNpc, "Texture/GUI_Textures/dock-1");
      MarksmanPayload.Instance.FeedMarksmanTexture(this._texturePlayOneProps, DicePayload.Instance.TempDiceData.PropsImagePath);
      MarksmanPayload.Instance.FeedMarksmanTexture(this._texturePlayTenProps, DicePayload.Instance.TempDiceData.PropsImagePath);
    }
    this._labelRefreshCost.text = DicePayload.Instance.RefreshCost.ToString();
    this._labelPlayOneCost.text = DicePayload.Instance.DiceOneCost.ToString();
    this._labelPlayTenCost.text = DicePayload.Instance.DiceTenCost.ToString();
    this._labelRefreshCost.color = !DicePayload.Instance.HasEnoughGold2Refresh ? Color.red : this._refreshCostTextColor;
    this._labelPlayOneCost.color = !DicePayload.Instance.HasEnoughProps2PlayOne ? Color.red : this._dicePlayCostTextColor;
    this._labelPlayTenCost.color = !DicePayload.Instance.HasEnoughProps2PlayTen ? Color.red : this._dicePlayCostTextColor;
    if (updateScoreText)
    {
      this._scoreParam.Clear();
      this._scoreParam.Add("0", Utils.FormatThousands(DicePayload.Instance.TempDiceData.Score.ToString()));
      this._labelScore.text = ScriptLocalization.GetWithPara("id_uppercase_score_num", this._scoreParam, true);
      NGUITools.SetActive(this._scoreHintNode, DicePayload.Instance.HasRewardCollectByScore);
    }
    NGUITools.SetActive(this._buttonRefresh.gameObject, !DicePayload.Instance.IsRefreshFree);
    NGUITools.SetActive(this._buttonRefreshFree.gameObject, DicePayload.Instance.IsRefreshFree);
    NGUITools.SetActive(this._buttonPlayOne.gameObject, !DicePayload.Instance.IsDiceOneFree);
    NGUITools.SetActive(this._buttonPlayOneFree.gameObject, DicePayload.Instance.IsDiceOneFree);
    NGUITools.SetActive(this._freeRewardHintNode, DicePayload.Instance.TempDiceData.HasFreeReward);
  }

  private void ShowRewardContent()
  {
    for (int index = 0; index < this._labelRewardNumerals.Length; ++index)
      this._labelRewardNumerals[index].text = Utils.GetRomaNumeralsBelowTen(index + 1);
    this.ClearData();
    for (int key = 0; key < this._iconRewardNodes.Length; ++key)
    {
      if (DicePayload.Instance.TempDiceData.RewardsDict.ContainsKey(key))
      {
        DiceRewardSlot itemRenderer = this.CreateItemRenderer(DicePayload.Instance.TempDiceData.RewardsDict[key], this._iconRewardNodes[key]);
        this._itemDict.Add(key, itemRenderer);
      }
    }
  }

  private void ShowDiceContent()
  {
    if (this._firstLeftDiceNum < 0 || this._firstRightDiceNum < 0)
      return;
    this._leftDiceAnimator.Play(this.LEFT_DICE_STATES[this._firstLeftDiceNum], 0, 1f);
    this._rightDiceAnimator.Play(this.RIGHT_DICE_STATES[this._firstRightDiceNum], 0, 1f);
  }

  private void ClearData()
  {
    using (Dictionary<int, DiceRewardSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private DiceRewardSlot CreateItemRenderer(DiceData.RewardData rewardData, Transform parent)
  {
    this._itemPool.Initialize(this._itemPrefab, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    DiceRewardSlot component = gameObject.GetComponent<DiceRewardSlot>();
    component.SetData(rewardData);
    return component;
  }
}
