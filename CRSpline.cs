﻿// Decompiled with JetBrains decompiler
// Type: CRSpline
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class CRSpline
{
  public ArrayList vp = new ArrayList();
  private float delta_t;

  public CRSpline()
  {
    this.delta_t = 0.0f;
  }

  private Vector3 Eq(float t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
  {
    float num1 = t * t;
    float num2 = num1 * t;
    float num3 = (float) (0.5 * (-(double) num2 + 2.0 * (double) num1 - (double) t));
    float num4 = (float) (0.5 * (3.0 * (double) num2 - 5.0 * (double) num1 + 2.0));
    float num5 = (float) (0.5 * (-3.0 * (double) num2 + 4.0 * (double) num1 + (double) t));
    float num6 = (float) (0.5 * ((double) num2 - (double) num1));
    return p1 * num3 + p2 * num4 + p3 * num5 + p4 * num6;
  }

  public void AddSplinePoint(Vector3 v)
  {
    this.vp.Add((object) new V3(v));
    this.delta_t = 1f / (float) this.vp.Count;
  }

  public Vector3 GetInterpolatedSplinePoint(float t)
  {
    int num = (int) ((double) t / (double) this.delta_t);
    int index1 = num - 1;
    if (index1 < 0)
      index1 = 0;
    else if (index1 >= this.vp.Count - 1)
      index1 = this.vp.Count - 1;
    int index2 = num;
    if (index2 < 0)
      index2 = 0;
    else if (index2 >= this.vp.Count - 1)
      index2 = this.vp.Count - 1;
    int index3 = num + 1;
    if (index3 < 0)
      index3 = 0;
    else if (index3 >= this.vp.Count - 1)
      index3 = this.vp.Count - 1;
    int index4 = num + 2;
    if (index4 < 0)
      index4 = 0;
    else if (index4 >= this.vp.Count - 1)
      index4 = this.vp.Count - 1;
    return this.Eq((t - this.delta_t * (float) num) / this.delta_t, (this.vp[index1] as V3).d, (this.vp[index2] as V3).d, (this.vp[index3] as V3).d, (this.vp[index4] as V3).d);
  }

  public int GetNumPoints()
  {
    return this.vp.Count;
  }

  public Vector3 GetNthPoint(int n)
  {
    return (this.vp[n] as V3).d;
  }
}
