﻿// Decompiled with JetBrains decompiler
// Type: IapVerifyReceipt.VerifyReceipt
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;

namespace IapVerifyReceipt
{
  [Serializable]
  public class VerifyReceipt
  {
    public int status;
    public string detail;
    public string message;
    public int count;
    public List<Product> product;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[VerifyReceipt] ");
      stringBuilder.Append("status: " + (object) this.status + " ");
      stringBuilder.Append("detail: " + this.detail + " ");
      stringBuilder.Append("message: " + this.message + " ");
      stringBuilder.Append("count: " + (object) this.count + " ");
      if (this.product != null)
      {
        using (List<Product>.Enumerator enumerator = this.product.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Product current = enumerator.Current;
            stringBuilder.Append(current.ToString());
          }
        }
      }
      return stringBuilder.ToString();
    }
  }
}
