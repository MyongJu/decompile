﻿// Decompiled with JetBrains decompiler
// Type: IapVerifyReceipt.Product
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;

namespace IapVerifyReceipt
{
  [Serializable]
  public class Product
  {
    public string log_time;
    public string appid;
    public string product_id;
    public double charge_amount;
    public string tid;
    public string detail_pname;
    public string bp_info;
    public string tcash_flag;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder("[Product] ");
      stringBuilder.Append("log_time: " + this.log_time + " ");
      stringBuilder.Append("appid: " + this.appid + " ");
      stringBuilder.Append("product_id: " + this.product_id + " ");
      stringBuilder.Append("charge_amount: " + (object) this.charge_amount + " ");
      stringBuilder.Append("tid: " + this.tid + " ");
      stringBuilder.Append("detail_pname: " + this.detail_pname + " ");
      stringBuilder.Append("bp_info: " + this.bp_info + " ");
      stringBuilder.Append("tcash_flag: " + this.tcash_flag + " ");
      return stringBuilder.ToString();
    }
  }
}
