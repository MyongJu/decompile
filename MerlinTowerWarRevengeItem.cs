﻿// Decompiled with JetBrains decompiler
// Type: MerlinTowerWarRevengeItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinTowerWarRevengeItem : MonoBehaviour
{
  public UILabel playerName;
  public Icon rewardIcon;
  public UITexture texture;
  public UILabel remainTimeValue;
  private int endTime;
  public System.Action OnRevengeDisappear;

  public void SetData(string name, int itemId = 0, long count = 0)
  {
    if ((UnityEngine.Object) this.rewardIcon != (UnityEngine.Object) null)
    {
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem("item_magic_tower_coin_1");
      if (itemStaticInfo != null)
      {
        IconData iconData = new IconData();
        iconData.image = itemStaticInfo.ImagePath;
        iconData.contents = new string[1]{ string.Empty };
        iconData.contents[0] = count.ToString();
        iconData.Data = (object) itemStaticInfo.internalId;
        this.rewardIcon.StopAutoFillName();
        this.rewardIcon.StartAutoTip();
        this.rewardIcon.FeedData((IComponentData) iconData);
      }
    }
    this.playerName.text = name;
  }

  public void SetRemainTime(int time)
  {
    this.endTime = time;
    time = this.endTime - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
      Oscillator.Instance.secondEvent += new System.Action<int>(this.OnProcess);
    this.RefreshTime();
  }

  private void RefreshTime()
  {
    int time = this.endTime - NetServerTime.inst.ServerTimestamp;
    if (time > 0)
    {
      this.remainTimeValue.text = Utils.FormatTime(time, false, false, true);
    }
    else
    {
      this.remainTimeValue.text = string.Empty;
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
      if (this.OnRevengeDisappear == null)
        return;
      this.OnRevengeDisappear();
    }
  }

  public void Clear()
  {
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnProcess);
    this.OnRevengeDisappear = (System.Action) null;
  }

  private void OnProcess(int time)
  {
    this.RefreshTime();
  }
}
