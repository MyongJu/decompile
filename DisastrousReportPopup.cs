﻿// Decompiled with JetBrains decompiler
// Type: DisastrousReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DisastrousReportPopup : BaseReportPopup
{
  private WarReportContent wrc;
  public IconListComponent ilc;
  public GameObject objAllianceWarNode;

  private void Init()
  {
    List<IconData> data = new List<IconData>();
    data.Add(new IconData("Texture/BuildingConstruction/food_icon", new string[1]
    {
      "-0"
    }));
    data.Add(new IconData("Texture/BuildingConstruction/wood_icon", new string[1]
    {
      "-0"
    }));
    data.Add(new IconData("Texture/BuildingConstruction/ore_icon", new string[1]
    {
      "-0"
    }));
    data.Add(new IconData("Texture/BuildingConstruction/silver_icon", new string[1]
    {
      "-0"
    }));
    if (this.wrc.dragon_attr != null)
    {
      IconData iconData1 = new IconData("Texture/ItemIcons/item_dragon_exp", new string[2]
      {
        "+" + this.wrc.dragon_attr.dragon_exp,
        ScriptLocalization.Get("id_dragon_xp", true)
      });
      data.Add(iconData1);
      IconData iconData2 = new IconData("Texture/Dragon/icon_dragon_power_dark", new string[2]
      {
        "+" + this.wrc.dragon_attr.dark,
        ScriptLocalization.Get("id_dragon_dark_power", true)
      });
      data.Add(iconData2);
    }
    this.ilc.FeedData((IComponentData) new IconListComponentData(data));
    NGUITools.SetActive(this.objAllianceWarNode, this.maildata.IsInAllianceWar());
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/mail_report_loser", (System.Action<bool>) null, true, false, string.Empty);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.wrc = JsonReader.Deserialize<WarReport>(Utils.Object2Json((object) this.param.hashtable)).data;
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }
}
