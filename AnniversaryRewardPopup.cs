﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class AnniversaryRewardPopup : Popup
{
  public AnniversaryRewardContainer[] rewards;
  public UILabel title;
  public UILabel desc;
  public AnniversaryRewardContainer normalRewardContainer;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.UpdateUI();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("event_1_year_tournament_rewards_name", true);
    this.desc.text = ScriptLocalization.Get("event_1_year_tournament_rewards_description", true);
    string empty = string.Empty;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("anniversary_champion_worshipped_times");
    Dictionary<string, string> para = new Dictionary<string, string>();
    int num1 = 50;
    int num2 = 1000;
    if (data != null)
    {
      num1 = data.ValueInt;
      if (num1 <= 0)
        num1 = 50;
    }
    para.Add("1", num2.ToString());
    int factor = num2 / num1;
    string withPara = ScriptLocalization.GetWithPara("event_1_year_tournament_praise_rewards_description", para, true);
    List<AnniversaryChampionMainInfo> allData = ConfigManager.inst.DB_AnniversaryChampionMain.GetAllData();
    Reward reward = (Reward) null;
    for (int index = 0; index < allData.Count; ++index)
    {
      AnniversaryChampionMainInfo championMainInfo = allData[index];
      AnniversaryChampionRewardInfo championRewardInfo = ConfigManager.inst.DB_AnniversaryChampionRewards.GetItem(championMainInfo.RewardId);
      this.rewards[index].SetData(championMainInfo.LocalName, championRewardInfo.Rewards, 1);
      if (reward == null)
        reward = ConfigManager.inst.DB_AnniversaryChampionRewards.GetItem(championMainInfo.AnnvWorshipedRewardId).Rewards;
    }
    this.normalRewardContainer.SetData(withPara, reward, factor);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }
}
