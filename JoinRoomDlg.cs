﻿// Decompiled with JetBrains decompiler
// Type: JoinRoomDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class JoinRoomDlg : MonoBehaviour
{
  private List<JoinRoomBar> _roomBars = new List<JoinRoomBar>();
  private long _currentRoomId;
  private int _roomBarsTop;
  [SerializeField]
  private JoinRoomDlg.Panel panel;

  public void OnSearchButtonClick()
  {
    MessageHub.inst.GetPortByAction("Chat:searchRoom").SendRequest(Utils.Hash((object) "search_name", (object) this.panel.input.value), new System.Action<bool, object>(this.OnSearchCallBack), true);
  }

  public void OnSearchCallBack(bool result, object orgData)
  {
    if (result)
      this.RefreshRoomList(orgData);
    this.panel.roomListTable.repositionNow = true;
  }

  public void OnCloseButtonClick()
  {
    this.Hide();
  }

  public void OnDoneButtonClick()
  {
    if (this._currentRoomId != 0L)
      MessageHub.inst.GetPortByAction("Chat:joinRoom").SendRequest(Utils.Hash((object) "room_id", (object) this._currentRoomId), (System.Action<bool, object>) ((result, orgSrc) =>
      {
        if (!result)
          return;
        MessageHub.inst.GetPortByAction("Chat:getInitChatRooms").SendRequest((Hashtable) null, (System.Action<bool, object>) ((arg1, arg2) =>
        {
          if (!arg1)
            return;
          ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this._currentRoomId);
          if (chatRoomData == null)
            return;
          if (chatRoomData.isOpen)
            UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_join_success"), (System.Action) null, 4f, false);
          else
            UIManager.inst.toast.Show(Utils.XLAT("toast_chat_room_apply_sent"), (System.Action) null, 4f, false);
        }), true);
      }), true);
    this.Hide();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    MessageHub.inst.GetPortByAction("Chat:getRecomChatRooms").SendRequest(Utils.Hash((object) "uid", (object) PlayerData.inst.uid), new System.Action<bool, object>(this.OnSearchCallBack), true);
  }

  public void Hide()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
  }

  private void RefreshRoomList(object orgData)
  {
    for (int index = 0; index < this._roomBars.Count; ++index)
      this._roomBars[index].gameObject.SetActive(false);
    this._roomBarsTop = 0;
    this._currentRoomId = 0L;
    ArrayList data;
    BaseData.CheckAndParseOrgData(orgData, out data);
    if (data != null)
    {
      if (data.Count > 0)
      {
        this.panel.NoSearchValue.gameObject.SetActive(false);
        for (int index = 0; index < data.Count; ++index)
          this.AddRoom(data[index]);
      }
      else
        this.panel.NoSearchValue.gameObject.SetActive(true);
    }
    this.panel.roomListTable.repositionNow = true;
  }

  private void OnRoomBarClick(long roomId)
  {
    for (int index = 0; index < this._roomBars.Count; ++index)
      this._roomBars[index].isChecked = this._roomBars[index].roomId == roomId;
    this._currentRoomId = roomId;
    if (this._currentRoomId <= 0L)
      return;
    ChatRoomData chatRoomData = DBManager.inst.DB_room.Get(this._currentRoomId);
    if (chatRoomData == null)
      return;
    this.panel.buttonTitle.text = !chatRoomData.isOpen ? Utils.XLAT("id_uppercase_apply") : Utils.XLAT("chat_join_room");
  }

  private void AddRoom(object orgData)
  {
    if (this._roomBarsTop >= this._roomBars.Count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.roomListTable.gameObject, this.panel.orgRoomBar.gameObject);
      gameObject.name = "room_" + (object) (100 + this._roomBars.Count);
      this._roomBars.Add(gameObject.GetComponent<JoinRoomBar>());
    }
    this._roomBars[this._roomBarsTop].Show(orgData);
    this._roomBars[this._roomBarsTop].gameObject.SetActive(true);
    this._roomBars[this._roomBarsTop].isChecked = false;
    this._roomBars[this._roomBarsTop].onJoinRoomBarClicked = new System.Action<long>(this.OnRoomBarClick);
    ++this._roomBarsTop;
  }

  [Serializable]
  protected class Panel
  {
    public UIInput input;
    public UITable roomListTable;
    public JoinRoomBar orgRoomBar;
    public Transform NoSearchValue;
    public UILabel buttonTitle;
  }
}
