﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossPayload
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class KingdomBossPayload
{
  private static KingdomBossPayload _instance;
  private HubPort port;

  public event System.Action OnKingdomBossFinish;

  public static KingdomBossPayload Instance
  {
    get
    {
      if (KingdomBossPayload._instance == null)
        KingdomBossPayload._instance = new KingdomBossPayload();
      return KingdomBossPayload._instance;
    }
  }

  public void Initialize()
  {
    this.port = MessageHub.inst.GetPortByAction("world_boss_end");
    this.port.AddEvent(new System.Action<object>(this.OnFinishHandler));
  }

  private void OnFinishHandler(object result)
  {
    if (this.OnKingdomBossFinish == null)
      return;
    this.OnKingdomBossFinish();
  }

  public void Dispose()
  {
    if (this.port == null)
      return;
    this.port.RemoveEvent(new System.Action<object>(this.OnFinishHandler));
    this.port = (HubPort) null;
  }
}
