﻿// Decompiled with JetBrains decompiler
// Type: RuralBlockUnlockDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RuralBlockUnlockDialog : UI.Dialog
{
  private List<GameObject> _requirements = new List<GameObject>();
  private Dictionary<string, int> m_lackitem = new Dictionary<string, int>(1);
  private Dictionary<string, long> m_lackRes = new Dictionary<string, long>(4);
  public float zoomFocusDis = 0.8f;
  private Hashtable requireHT = new Hashtable();
  private RuralBlockInfo curRbi;
  private GameObject tarGO;
  private float _requirementsY;
  private bool bshowPopup;
  private int instUnlockAmount;
  public ConstructionRequirementBtn btntemplate;
  public UIWidget focus;

  private void Init()
  {
    this.Reset();
    CitadelSystem.inst.FocusConstructionBuilding(this.tarGO, this.focus, this.zoomFocusDis);
    Hashtable hashtable = new Hashtable();
    this.requireHT.Clear();
    this._requirementsY = this.btntemplate.transform.localPosition.y;
    CityData byUid = DBManager.inst.DB_City.GetByUid(PlayerData.inst.uid);
    if (this.curRbi.reqFood > 0L)
    {
      string str = "Texture/BuildingConstruction/food_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
      this.AddRequirement(str, (string) null, currentResource, (double) this.curRbi.reqFood, new EventDelegate((EventDelegate.Callback) (() => GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.FOOD))));
      if (currentResource < (double) this.curRbi.reqFood)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) this.curRbi.reqFood - currentResource));
        hashtable[(object) ItemBag.ItemType.food] = (object) this.curRbi.reqFood;
      }
    }
    if (this.curRbi.reqWood > 0L)
    {
      string str = "Texture/BuildingConstruction/wood_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
      this.AddRequirement(str, (string) null, currentResource, (double) this.curRbi.reqWood, new EventDelegate((EventDelegate.Callback) (() => GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.WOOD))));
      if (currentResource < (double) this.curRbi.reqWood)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) this.curRbi.reqWood - currentResource));
        hashtable[(object) ItemBag.ItemType.wood] = (object) this.curRbi.reqWood;
      }
    }
    if (this.curRbi.reqSilver > 0L)
    {
      string str = "Texture/BuildingConstruction/silver_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
      this.AddRequirement(str, (string) null, currentResource, (double) this.curRbi.reqSilver, new EventDelegate((EventDelegate.Callback) (() => GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER))));
      if (currentResource < (double) this.curRbi.reqSilver)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) this.curRbi.reqSilver - currentResource));
        hashtable[(object) ItemBag.ItemType.silver] = (object) this.curRbi.reqSilver;
      }
    }
    if (this.curRbi.reqOre > 0L)
    {
      string str = "Texture/BuildingConstruction/ore_icon";
      double currentResource = byUid.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
      this.AddRequirement(str, (string) null, currentResource, (double) this.curRbi.reqOre, new EventDelegate((EventDelegate.Callback) (() => GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.ORE))));
      if (currentResource < (double) this.curRbi.reqOre)
      {
        this.bshowPopup = true;
        this.m_lackRes.Add(str, (long) Convert.ToInt32((double) this.curRbi.reqOre - currentResource));
        hashtable[(object) ItemBag.ItemType.ore] = (object) this.curRbi.reqOre;
      }
    }
    this.btntemplate.gameObject.SetActive(false);
    this.requireHT[(object) "resources"] = (object) hashtable;
    this.instUnlockAmount = ItemBag.CalculateCost(this.requireHT);
  }

  public GameObject AddRequirement(string icon, string name, double amount, double need, EventDelegate ed = null)
  {
    GameObject prefab = Utils.DuplicateGOB(this.btntemplate.gameObject, this.btntemplate.transform.parent);
    prefab.SetActive(true);
    ConstructionRequirementBtn component1 = prefab.GetComponent<ConstructionRequirementBtn>();
    UIButton component2 = component1.mGetMoreBtn.GetComponent<UIButton>();
    component2.onClick.Clear();
    if (ed != null)
      component2.onClick.Add(ed);
    string str1 = "[e71200]";
    string str2 = "[-]";
    string reqDetails;
    if (amount < need)
      reqDetails = str1 + Utils.ConvertNumberToNormalString((long) amount) + str2 + "/" + Utils.ConvertNumberToNormalString((long) need);
    else
      reqDetails = Utils.ConvertNumberToNormalString((long) amount) + "/" + Utils.ConvertNumberToNormalString((long) need);
    component1.SetDetails(icon, reqDetails, (string) null, amount >= need, true);
    this._requirements.Add(prefab);
    this.AdjustHeight(ref prefab);
    return prefab;
  }

  private void AdjustHeight(ref GameObject prefab)
  {
    Utils.SetLPY(prefab, this._requirementsY);
    this._requirementsY -= (float) prefab.GetComponent<UIWidget>().height;
  }

  public void OnUnlockPressed()
  {
    if (this.bshowPopup)
      UIManager.inst.OpenPopup("BuildingResPopUp", (Popup.PopupParameter) new BuildingResPopUp.Parameter()
      {
        lackItem = this.m_lackitem,
        lackRes = this.m_lackRes,
        requireRes = this.requireHT,
        action = ScriptLocalization.Get("id_uppercase_unlock", true),
        gold = this.instUnlockAmount,
        buyResourCallBack = new System.Action<int>(this.ConfiremCallBack),
        title = ScriptLocalization.Get("construction_build_notenoughresource", true),
        content = ScriptLocalization.Get("rural_area_unlock_resources_not_enough_description", true)
      });
    else
      this.UnlockBlock(false);
  }

  public void OnInstUnlockPressed()
  {
    if (GameEngine.Instance.PlayerData.hostPlayer.Currency >= this.instUnlockAmount)
      this.UnlockBlock(true);
    else
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.GOLD);
  }

  private void ConfiremCallBack(int gold)
  {
    if (PlayerData.inst.hostPlayer.Currency < gold)
    {
      Utils.ShowNotEnoughGoldTip();
    }
    else
    {
      if (GameEngine.Instance.PlayerData.hostPlayer.Currency < gold)
        return;
      MessageHub.inst.GetPortByAction("City:blockUnlock").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "block_id", (object) this.curRbi.internalId, (object) "instant", (object) false, (object) nameof (gold), (object) gold), (System.Action<bool, object>) ((ret, obj) =>
      {
        this.tarGO.GetComponent<RuralBlock>().UnlockBlock();
        this.OnCloseBtnPressed();
      }), true);
      AudioManager.Instance.PlaySound("sfx_rural_unlock_rural_area", false);
    }
  }

  private void UnlockBlock(bool instant)
  {
    MessageHub.inst.GetPortByAction("City:blockUnlock").SendRequest(Utils.Hash((object) "city_id", (object) CityManager.inst.GetCityID(), (object) "block_id", (object) this.curRbi.internalId, (object) nameof (instant), (object) instant), (System.Action<bool, object>) ((ret, obj) =>
    {
      this.tarGO.GetComponent<RuralBlock>().UnlockBlock();
      this.OnCloseBtnPressed();
    }), true);
    AudioManager.Instance.PlaySound("sfx_rural_unlock_rural_area", false);
  }

  public void OnCloseBtnPressed()
  {
    this.Reset();
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Reset()
  {
    for (int index = this._requirements.Count - 1; index >= 0; --index)
      PrefabManagerEx.Instance.Destroy(this._requirements[index]);
    this._requirements.Clear();
    this._requirementsY = this.btntemplate.transform.localPosition.y;
    this.btntemplate.gameObject.SetActive(true);
    this.m_lackRes.Clear();
    this.bshowPopup = false;
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    RuralBlockUnlockDialog.Parameter parameter = orgParam as RuralBlockUnlockDialog.Parameter;
    this.curRbi = parameter.curRbi;
    this.tarGO = parameter.target;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public RuralBlockInfo curRbi;
    public GameObject target;
  }
}
