﻿// Decompiled with JetBrains decompiler
// Type: TrackEvent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Facebook.Unity;
using Funplus;
using System.Collections.Generic;
using UnityEngine;

public class TrackEvent
{
  private Dictionary<string, string> _adjustEventsAndroid = new Dictionary<string, string>();
  private Dictionary<string, string> _adjustEventsIOS = new Dictionary<string, string>();
  private Queue<TrackEvent.EventTrackData> caches = new Queue<TrackEvent.EventTrackData>();
  public const int TRACK_LEVEL = 15;
  public const int TRACK_LEVEL_10 = 10;
  public const int TRACK_LEVEL_4 = 4;
  public const int TRACK_LEVEL_5 = 5;
  public const int TRACK_LOGIN_DAY_2 = 1;
  public const int TRACK_LOGIN_DAY_7 = 7;
  private static TrackEvent _instance;

  public TrackEvent()
  {
    this._adjustEventsAndroid.Add("completed_tutorial", "zhdzck");
    this._adjustEventsAndroid.Add("purchase", "urqqkn");
    this._adjustEventsAndroid.Add("paying_user", "5ekh78");
    this._adjustEventsAndroid.Add("stronghold_level_10", "qioz5t");
    this._adjustEventsAndroid.Add("day_2_login", "3zkadv");
    this._adjustEventsAndroid.Add("day_7_login", "dj41b0");
    this._adjustEventsAndroid.Add("stronghold_level_4", "s3csq3");
    this._adjustEventsAndroid.Add("stronghold_level_5", "s4gt7w");
    this._adjustEventsIOS.Add("completed_tutorial", "aqx39q");
    this._adjustEventsIOS.Add("purchase", "2kcw18");
    this._adjustEventsIOS.Add("paying_user", "a9880u");
    this._adjustEventsIOS.Add("stronghold_level_10", "uylcck");
    this._adjustEventsIOS.Add("day_2_login", "ncnbau");
    this._adjustEventsIOS.Add("day_7_login", "sb6or2");
    this._adjustEventsIOS.Add("stronghold_level_4", "795km6");
    this._adjustEventsIOS.Add("stronghold_level_5", "bdte0y");
  }

  public static TrackEvent Instance
  {
    get
    {
      if (TrackEvent._instance == null)
        TrackEvent._instance = new TrackEvent();
      return TrackEvent._instance;
    }
  }

  public void TraceEventToFB(string eventName, params string[] param)
  {
    if (CustomDefine.IsSQWanPackage() || Application.isEditor)
      return;
    Dictionary<string, object> parameters = (Dictionary<string, object>) null;
    if (param != null && param.Length > 0)
    {
      parameters = new Dictionary<string, object>();
      int index = 0;
      while (index < param.Length)
      {
        parameters.Add(param[index], (object) param[index + 1]);
        index += 2;
      }
    }
    FB.LogAppEvent(eventName, new float?(), parameters);
  }

  public void TracePurchaseToFB(float price, string curreny, Dictionary<string, object> param = null)
  {
    if (CustomDefine.IsSQWanPackage() || Application.isEditor || Application.platform != RuntimePlatform.Android)
      return;
    FB.LogPurchase(price, curreny, param);
  }

  public void Trace(string eventName, TrackEvent.Type type = TrackEvent.Type.Facebook, params string[] datas)
  {
    if (CustomDefine.IsSpecialChannel() || type == TrackEvent.Type.None || Application.isEditor)
      return;
    Dictionary<string, string> dictionary = new Dictionary<string, string>();
    int index = 0;
    while (index < datas.Length)
    {
      dictionary.Add(datas[index], datas[index + 1]);
      index += 2;
    }
    TrackEvent.EventTrackData data = new TrackEvent.EventTrackData();
    data.type = type;
    data.eventName = eventName;
    data.param = dictionary;
    if (FunplusSdk.Instance.IsSdkInstalled())
    {
      while (this.caches.Count > 0)
        this.Send(this.caches.Dequeue());
      this.Send(data);
    }
    else
      this.caches.Enqueue(data);
  }

  private bool HadSend(string type)
  {
    return PlayerPrefsEx.HasUidKey(type);
  }

  private void Send(TrackEvent.EventTrackData data)
  {
    if (this.HadSend(data.eventName))
      return;
    if ((data.type & TrackEvent.Type.Facebook) > TrackEvent.Type.None)
      FunplusFacebook.Instance.LogEvent(data.GetContent());
    if ((data.type & TrackEvent.Type.Adjust) > TrackEvent.Type.None)
    {
      string token = string.Empty;
      if (this._adjustEventsIOS.ContainsKey(data.eventName))
        token = Application.platform != RuntimePlatform.Android ? this._adjustEventsIOS[data.eventName] : this._adjustEventsAndroid[data.eventName];
      AdjustProxy.Instance.TraceEvent(token);
    }
    PlayerPrefsEx.SetIntByUid(data.eventName, 1);
  }

  public enum Type
  {
    None = 0,
    Facebook = 1,
    Adjust = 4,
  }

  public struct Name
  {
    public const string JOIN_ALLIANCE = "join_alliance";
    public const string STRONGHOLD_LEVEL = "stronghold_level_15";
    public const string COMPLETED_TUTORIAL = "completed_tutorial";
    public const string PURCHASE = "purchase";
    public const string PAYING_USER = "paying_user";
    public const string STRONGHOLD_LEVEL_10 = "stronghold_level_10";
    public const string STRONGHOLD_LEVEL_5 = "stronghold_level_5";
    public const string STRONGHOLD_LEVEL_4 = "stronghold_level_4";
    public const string LOGIN_2_DAY = "day_2_login";
    public const string LOGIN_7_DAY = "day_7_login";
  }

  private class EventTrackData
  {
    public string eventName;
    public TrackEvent.Type type;
    public Dictionary<string, string> param;

    public string GetContent()
    {
      return this.eventName;
    }
  }

  public struct FacebookEventNames
  {
    public const string STRONGHOLD_LEVEL = "Stronghold Level";
    public const string COMPLETED_TUTORIAL = "Completed Tutorial";
    public const string JOIN_ALLIANCE = "Join Group";
    public const string PURCHASE = "purchase";
  }
}
