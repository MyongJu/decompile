﻿// Decompiled with JetBrains decompiler
// Type: DragonInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class DragonInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "level")]
  public int level;
  [Config(Name = "exp")]
  public int exp;
  [Config(Name = "level_boost")]
  public float level_boost;
  [Config(Name = "age")]
  public int age;
  [Config(Name = "power")]
  public int power;
  [Config(Name = "skill_slot")]
  public int skill_slot;
  [Config(Name = "tendency_min")]
  public int tendency_min;
  [Config(Name = "min_light_1")]
  public float min_light_1;
  [Config(Name = "min_light_2")]
  public float min_light_2;
  [Config(Name = "min_dark_1")]
  public float min_dark_1;
  [Config(Name = "min_dark_2")]
  public float min_dark_2;
  [Config(Name = "tendency_skill_level")]
  public int tendency_skill_level;
  [Config(Name = "tendency_skill_rate")]
  public float tendency_skill_rate;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "Rewards")]
  public Reward Rewards;
  [Config(Name = "tendency_light_1_min")]
  public int tendency_light_1_min;
  [Config(Name = "tendency_light_2_min")]
  public int tendency_light_2_min;
  [Config(Name = "tendency_dark_1_min")]
  public int tendency_dark_1_min;
  [Config(Name = "tendency_dark_2_min")]
  public int tendency_dark_2_min;
}
