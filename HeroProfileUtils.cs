﻿// Decompiled with JetBrains decompiler
// Type: HeroProfileUtils
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class HeroProfileUtils
{
  public static bool HasNewHeroProfile()
  {
    List<HeroProfileInfo> heroProfileInfoList = ConfigManager.inst.DB_HeroProfile.GetHeroProfileInfoList();
    if (heroProfileInfoList != null)
    {
      for (int index = 0; index < heroProfileInfoList.Count; ++index)
      {
        if (heroProfileInfoList[index].IsNew && PlayerPrefsEx.GetIntByUid("hero_profile_" + heroProfileInfoList[index].id, 1) == 1)
          return true;
      }
    }
    return false;
  }

  public static void SetProfileNewStatus(bool isNew = false)
  {
    List<HeroProfileInfo> heroProfileInfoList = ConfigManager.inst.DB_HeroProfile.GetHeroProfileInfoList();
    if (heroProfileInfoList == null)
      return;
    for (int index = 0; index < heroProfileInfoList.Count; ++index)
    {
      if (heroProfileInfoList[index].IsNew)
        PlayerPrefsEx.SetIntByUid("hero_profile_" + heroProfileInfoList[index].id, !isNew ? 0 : 1);
    }
  }

  public static bool IsUnlockedHeroPortrait(int heroProfileId)
  {
    HeroProfileInfo byImageIndex = ConfigManager.inst.DB_HeroProfile.GetByImageIndex(heroProfileId);
    return byImageIndex == null || !byImageIndex.IsLocked || PlayerData.inst.userData.IsPortraitUnlocked(heroProfileId);
  }

  public static void UnlockHeroProfile(int heroProfileId, System.Action<bool, object> callback = null)
  {
    MessageHub.inst.GetPortByAction("Player:unlockPortrait").SendRequest(new Hashtable()
    {
      {
        (object) "uid",
        (object) PlayerData.inst.uid
      },
      {
        (object) "portrait",
        (object) heroProfileId
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (callback == null)
        return;
      callback(ret, data);
    }), true);
  }
}
