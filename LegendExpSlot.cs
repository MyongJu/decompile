﻿// Decompiled with JetBrains decompiler
// Type: LegendExpSlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LegendExpSlot : MonoBehaviour
{
  [SerializeField]
  private UITexture m_Icon;
  [SerializeField]
  private UILabel m_Count;
  [SerializeField]
  private UILabel m_Gain;
  private ItemStaticInfo m_ItemInfo;
  private System.Action<ItemStaticInfo> m_Callback;

  public void OnSlotPressed()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback(this.m_ItemInfo);
  }

  public void SetData(ItemStaticInfo itemInfo, System.Action<ItemStaticInfo> callback)
  {
    this.m_ItemInfo = itemInfo;
    this.m_Callback = callback;
    this.UpdateUI();
  }

  public ItemStaticInfo GetItemStaticInfo()
  {
    return this.m_ItemInfo;
  }

  private void UpdateUI()
  {
    int itemCount = ItemBag.Instance.GetItemCount(this.m_ItemInfo.internalId);
    int gain = (int) this.m_ItemInfo.Value;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_Icon, this.m_ItemInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this.SetCount(itemCount);
    this.SetGain(gain);
  }

  private void SetCount(int count)
  {
    if (!((UnityEngine.Object) this.m_Count != (UnityEngine.Object) null))
      return;
    this.m_Count.text = string.Format("x {0}", (object) count);
  }

  private void SetGain(int gain)
  {
    if (!((UnityEngine.Object) this.m_Gain != (UnityEngine.Object) null))
      return;
    this.m_Gain.text = string.Format("+ {0} EXP", (object) Utils.FormatThousands(gain.ToString()));
  }
}
