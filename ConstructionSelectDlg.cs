﻿// Decompiled with JetBrains decompiler
// Type: ConstructionSelectDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ConstructionSelectDlg : UI.Dialog
{
  [Tooltip("The build zoom distance")]
  public float zoomFocusDis = 650f;
  private List<ConstructionBuildingRender> renders = new List<ConstructionBuildingRender>();
  private string buildingType = string.Empty;
  private const string RENDER_PATH = "Prefab/UI/Dialogs/Construction/BuildingRender";
  public UIWidget mBuildingIcon;
  public GameObject buildingRenderParent;
  public UILabel mBuildLabel;
  public UIScrollView bottomScrollView;
  public UICenterOnChild uiCenterOnChild;
  public UIGrid grid;
  public UITexture buildingImage;
  public UILabel buildingName;
  public UILabel buidingDescription;
  public UILabel limitLable;
  public UILabel limitLableName;
  public UITexture dynaBG;
  private ConstructionBuildingRender currentRender;
  private int zone;
  private GameObject targetPlot;
  private bool isScrollMoving;

  public override void OnCreate(UIControler.UIParameter orgParam)
  {
    base.OnCreate(orgParam);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    ConstructionSelectDlg.Parameter parameter = orgParam as ConstructionSelectDlg.Parameter;
    this.zone = parameter.zone;
    this.targetPlot = parameter.targetPlot;
    this.buildingType = parameter.buildingType;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.InitUI();
    this.PositionScrollView();
    this.bottomScrollView.onDragStarted += new UIScrollView.OnDragNotification(this.onDragStarted);
    this.uiCenterOnChild.onFinished += new SpringPanel.OnFinished(this.OnCenterOnFinish);
    this.bottomScrollView.onMomentumMove += new UIScrollView.OnDragNotification(this.OnScrollMoving);
    this.bottomScrollView.onStoppedMoving += new UIScrollView.OnDragNotification(this.OnScrollMoving);
    CitadelSystem.inst.FocusConstructionBuilding(this.targetPlot, this.mBuildingIcon, this.zoomFocusDis);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    CitadelSystem.inst.DeleteBuildingPreview();
    this.bottomScrollView.onDragStarted -= new UIScrollView.OnDragNotification(this.onDragStarted);
    this.uiCenterOnChild.onFinished -= new SpringPanel.OnFinished(this.OnCenterOnFinish);
    this.bottomScrollView.onMomentumMove -= new UIScrollView.OnDragNotification(this.OnScrollMoving);
    this.bottomScrollView.onStoppedMoving -= new UIScrollView.OnDragNotification(this.OnScrollMoving);
  }

  public void OnBackBtnClick()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnChooseClick()
  {
    if (!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null))
      return;
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(this.currentRender.InternalID);
    UIManager.inst.OpenDlg("BuildingConstruction", (UI.Dialog.DialogParameter) new BuildingConstruction.Parameter()
    {
      buildingid = data.Type,
      tarLevel = 1,
      targetPlot = this.targetPlot
    }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnScrollViewClick()
  {
    if ((double) UICamera.currentTouch.pos.x < 100.0)
      this.OnScrollViewRightClick();
    else
      this.OnScrollViewLeftClick();
  }

  private void OnScrollViewRightClick()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    int index = Mathf.Max(0, this.renders.IndexOf(this.uiCenterOnChild.centeredObject.GetComponent<ConstructionBuildingRender>()) - 1);
    if (!this.renders[index].gameObject.activeInHierarchy)
      return;
    this.onDragStarted();
    this.currentRender = this.renders[index];
    this.UpdateUI();
    this.currentRender.HighLight();
    this.uiCenterOnChild.CenterOn(this.renders[index].transform);
  }

  private void OnScrollViewLeftClick()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    int index = Mathf.Min(this.renders.Count - 1, this.renders.IndexOf(this.uiCenterOnChild.centeredObject.GetComponent<ConstructionBuildingRender>()) + 1);
    if (!this.renders[index].gameObject.activeInHierarchy)
      return;
    this.onDragStarted();
    this.currentRender = this.renders[index];
    this.UpdateUI();
    this.currentRender.HighLight();
    this.uiCenterOnChild.CenterOn(this.renders[index].transform);
  }

  private void PositionScrollView()
  {
    this.bottomScrollView.ResetPosition();
    this.Invoke("LateCenterOn", 0.05f);
  }

  private void LateCenterOn()
  {
    if (!((UnityEngine.Object) this.currentRender != (UnityEngine.Object) null))
      return;
    this.uiCenterOnChild.CenterOn(this.currentRender.transform);
  }

  private void Update()
  {
    if (!this.bottomScrollView.isDragging)
      return;
    this.Focus();
  }

  private void OnScrollMoving()
  {
    this.Focus();
  }

  private void Focus()
  {
    using (List<ConstructionBuildingRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConstructionBuildingRender current = enumerator.Current;
        Transform cachedTransform = this.bottomScrollView.panel.cachedTransform;
        Vector3[] worldCorners = this.bottomScrollView.panel.worldCorners;
        Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
        if ((double) Mathf.Abs((cachedTransform.InverseTransformPoint(current.transform.position) - cachedTransform.InverseTransformPoint(position)).x) < 150.0 && this.currentRender.InternalID != current.InternalID && current.gameObject.activeInHierarchy)
        {
          this.currentRender = current;
          this.UpdateUI();
          this.onDragStarted();
          this.currentRender.HighLight();
        }
      }
    }
  }

  private void OnCenterOnFinish()
  {
    if (!((UnityEngine.Object) this.uiCenterOnChild.centeredObject != (UnityEngine.Object) null))
      return;
    this.isScrollMoving = false;
    this.bottomScrollView.enabled = true;
  }

  private void InitUI()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.targetPlot.GetComponentInParent<SuperPlotController>())
    {
      this.dynaBG.width = 1712;
      this.dynaBG.alpha = 1f;
    }
    else
    {
      this.dynaBG.width = 3136;
      this.dynaBG.alpha = 0.572549f;
    }
    CityManager inst = CityManager.inst;
    this.mBuildLabel.text = Utils.XLAT("id_uppercase_build");
    List<BuildingInfo> buildingInfosOfZone = ConfigManager.inst.DB_Building.GetBuildingInfosOfZone(this.zone);
    using (List<ConstructionBuildingRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.gameObject.SetActive(false);
    }
    List<ConstructionBuildingRender.RenderData> renderDataList = new List<ConstructionBuildingRender.RenderData>();
    for (int index = 0; index < buildingInfosOfZone.Count; ++index)
    {
      BuildingInfo buildingInfo = buildingInfosOfZone[index];
      if (!buildingInfo.Unique || inst.GetHighestBuildingLevelFor(buildingInfo.Type) <= -1)
      {
        bool bRequirementsOk = this.IsRequirementsOk(buildingInfo.Type, 1);
        if (inst.GetBuildingCount(buildingInfo.Type) < ConfigManager.inst.DB_ConfigRuralBuildingLimit.GetBuildingLimit(buildingInfo.Type))
          renderDataList.Add(new ConstructionBuildingRender.RenderData(bRequirementsOk, buildingInfo));
      }
    }
    renderDataList.Sort((Comparison<ConstructionBuildingRender.RenderData>) ((a, b) =>
    {
      if (a.bRequirementsOk == b.bRequirementsOk)
        return a.buildingInfo.Priority - b.buildingInfo.Priority;
      return a.bRequirementsOk ? -1 : 1;
    }));
    for (int index = 0; index < renderDataList.Count; ++index)
    {
      if (index < this.renders.Count)
      {
        this.renders[index].gameObject.SetActive(true);
        this.renders[index].SetDetails(renderDataList[index], new System.Action<ConstructionBuildingRender>(this.OnRenderClick), this.bottomScrollView);
      }
      else
      {
        ConstructionBuildingRender component = NGUITools.AddChild(this.grid.gameObject, AssetManager.Instance.HandyLoad("Prefab/UI/Dialogs/Construction/BuildingRender", (System.Type) null) as GameObject).GetComponent<ConstructionBuildingRender>();
        component.SetDetails(renderDataList[index], new System.Action<ConstructionBuildingRender>(this.OnRenderClick), this.bottomScrollView);
        this.renders.Add(component);
      }
    }
    if (this.renders.Count <= 0)
      return;
    Transform transform = this.renders[0].transform;
    this.currentRender = this.renders[0];
    if (!string.IsNullOrEmpty(this.buildingType))
    {
      using (List<ConstructionBuildingRender>.Enumerator enumerator = this.renders.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ConstructionBuildingRender current = enumerator.Current;
          if (current.BuildingType == this.buildingType)
          {
            this.currentRender = current;
            break;
          }
        }
      }
    }
    this.onDragStarted();
    this.currentRender.HighLight();
    this.UpdateUI();
    this.grid.repositionNow = true;
  }

  private void OnRenderClick(ConstructionBuildingRender render)
  {
    if (this.isScrollMoving)
      return;
    this.onDragStarted();
    this.currentRender = render;
    this.UpdateUI();
    render.HighLight();
    this.uiCenterOnChild.CenterOn(render.transform);
    this.isScrollMoving = true;
    this.bottomScrollView.enabled = false;
  }

  private int Compare(Transform a, Transform b)
  {
    bool isOk1 = a.GetComponent<ConstructionBuildingRender>().IsOK;
    bool isOk2 = b.GetComponent<ConstructionBuildingRender>().IsOK;
    int internalId1 = a.GetComponent<ConstructionBuildingRender>().InternalID;
    int internalId2 = b.GetComponent<ConstructionBuildingRender>().InternalID;
    if (isOk1 == isOk2)
      return internalId1 - internalId2;
    return isOk1 ? -1 : 1;
  }

  private void UpdateUI()
  {
    this.buildingName.text = Utils.XLAT(this.currentRender.BuildingType + "_name");
    this.buidingDescription.text = Utils.XLAT(this.currentRender.BuildingType + "_description");
    if (this.zone == 1)
    {
      this.limitLable.gameObject.SetActive(true);
      this.limitLableName.gameObject.SetActive(true);
      this.limitLable.text = CityManager.inst.GetBuildingCount(this.currentRender.BuildingType).ToString() + " / " + (object) ConfigManager.inst.DB_ConfigRuralBuildingLimit.GetBuildingLimit(this.currentRender.BuildingType);
    }
    else
    {
      this.limitLable.gameObject.SetActive(false);
      this.limitLableName.gameObject.SetActive(false);
    }
    CitadelSystem.inst.CreateBuildingPreview(this.currentRender.BuildingType, this.targetPlot.GetComponent<CityPlotController>().SlotId);
  }

  private bool CheckRequirement(string idStr, int reqValue)
  {
    if (reqValue < 1)
      return true;
    int result = -1;
    int.TryParse(idStr, out result);
    if (ItemBag.Instance.GetItemCount(result) > 0)
      return true;
    string buildingType = string.Empty;
    if (idStr != string.Empty && ConfigManager.inst.DB_Building.GetData(result) != null)
      buildingType = ConfigManager.inst.DB_Building.GetData(result).Type;
    return CityManager.inst.GetHighestBuildingLevelFor(buildingType) >= reqValue;
  }

  private bool IsRequirementsOk(string refName, int level)
  {
    BuildingInfo data = ConfigManager.inst.DB_Building.GetData(refName, level);
    return data != null && this.CheckRequirement(data.Requirement_ID_1, data.Requirement_Value_1) && (this.CheckRequirement(data.Requirement_ID_2, data.Requirement_Value_2) && this.CheckRequirement(data.Requirement_ID_3, data.Requirement_Value_3)) && this.CheckRequirement(data.Requirement_ID_4, data.Requirement_Value_4);
  }

  private void onDragStarted()
  {
    using (List<ConstructionBuildingRender>.Enumerator enumerator = this.renders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UnHighLight();
    }
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int zone;
    public GameObject targetPlot;
    public string buildingType;
  }
}
