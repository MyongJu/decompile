﻿// Decompiled with JetBrains decompiler
// Type: PlaceBasePresent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;

public class PlaceBasePresent : BasePresent
{
  public override void Refresh()
  {
    int level = 0;
    string formula = this.Formula;
    int num1 = formula.IndexOf('&');
    int num2 = formula.LastIndexOf('=');
    if (num1 > -1)
      level = int.Parse(formula.Substring(num2 + 1));
    RuralBlockInfo dataByStrongholdLev = ConfigManager.inst.DB_RuralBlock.GetDataByStrongholdLev(level);
    if (dataByStrongholdLev != null)
    {
      CityData cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
      string str = dataByStrongholdLev.internalId.ToString();
      if (cityData.blockInfo != null && cityData.blockInfo.ContainsKey((object) str) && "1" == cityData.blockInfo[(object) str].ToString())
      {
        this.ProgressValue = 1f;
        this.ProgressContent = "1/1";
      }
      else
      {
        this.ProgressValue = 0.0f;
        this.ProgressContent = "0/1";
      }
    }
    else
    {
      this.ProgressValue = 0.0f;
      this.ProgressContent = "0/1";
    }
  }

  public override int IconType
  {
    get
    {
      return 1;
    }
  }
}
