﻿// Decompiled with JetBrains decompiler
// Type: SpriteAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour
{
  public float speedMultiplier = 1f;
  public SpriteRenderer spriteRenderer;
  public SpriteAnimator.Animation[] animations;
  [HideInInspector]
  public bool loop;
  public string playAnimationOnStart;
  private bool looped;

  public bool playing { get; private set; }

  public SpriteAnimator.Animation currentAnimation { get; private set; }

  public int currentFrame { get; private set; }

  private void Start()
  {
    if (!(bool) ((UnityEngine.Object) this.spriteRenderer))
      this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    if (!(this.playAnimationOnStart != string.Empty))
      return;
    this.Play(this.playAnimationOnStart, true, 0);
  }

  private void OnDisable()
  {
    this.playing = false;
    this.currentAnimation = (SpriteAnimator.Animation) null;
  }

  public void Play(string name, bool loop = true, int startFrame = 0)
  {
    SpriteAnimator.Animation animation = this.GetAnimation(name);
    if (animation != null)
    {
      if (animation == this.currentAnimation)
        return;
      this.ForcePlay(name, loop, startFrame);
    }
    else
      Debug.LogWarning((object) ("could not find animation: " + name));
  }

  public void ForcePlay(string name)
  {
    SpriteAnimator.Animation animation = this.GetAnimation(name);
    if (animation == null)
      return;
    this.ForcePlay(animation, animation.loop, 0);
  }

  public void ForcePlay(string name, bool loop, int startFrame = 0)
  {
    this.ForcePlay(this.GetAnimation(name), loop, startFrame);
  }

  public void ForcePlay(SpriteAnimator.Animation animation, bool loop, int startFrame)
  {
    if (animation != null)
    {
      this.loop = loop;
      this.currentAnimation = animation;
      this.playing = true;
      this.currentFrame = startFrame;
      this.spriteRenderer.sprite = animation.frames[this.currentFrame];
      this.StopAllCoroutines();
      this.StartCoroutine(this.PlayAnimation(this.currentAnimation));
    }
    else
      Debug.LogWarning((object) ("Could not find animation: " + this.name));
  }

  public void SlipPlay(string name, int wantFrame, params string[] otherNames)
  {
    for (int index = 0; index < otherNames.Length; ++index)
    {
      if (this.currentAnimation != null && this.currentAnimation.name == otherNames[index])
      {
        this.Play(name, true, this.currentFrame);
        break;
      }
    }
    this.Play(name, true, wantFrame);
  }

  public bool IsPlaying(string name)
  {
    if (this.currentAnimation != null)
      return this.currentAnimation.name == name;
    return false;
  }

  public SpriteAnimator.Animation GetAnimation(string name)
  {
    foreach (SpriteAnimator.Animation animation in this.animations)
    {
      if (animation.name == name)
        return animation;
    }
    return (SpriteAnimator.Animation) null;
  }

  [DebuggerHidden]
  private IEnumerator CueAnimation(string animationName, float minTime, float maxTime)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpriteAnimator.\u003CCueAnimation\u003Ec__IteratorB4()
    {
      minTime = minTime,
      maxTime = maxTime,
      animationName = animationName,
      \u003C\u0024\u003EminTime = minTime,
      \u003C\u0024\u003EmaxTime = maxTime,
      \u003C\u0024\u003EanimationName = animationName,
      \u003C\u003Ef__this = this
    };
  }

  [DebuggerHidden]
  private IEnumerator PlayAnimation(SpriteAnimator.Animation animation)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpriteAnimator.\u003CPlayAnimation\u003Ec__IteratorB5()
    {
      animation = animation,
      \u003C\u0024\u003Eanimation = animation,
      \u003C\u003Ef__this = this
    };
  }

  private void NextFrame(SpriteAnimator.Animation animation)
  {
    this.looped = false;
    ++this.currentFrame;
    foreach (SpriteAnimator.AnimationTrigger trigger in animation.triggers)
    {
      if (trigger.frame == this.currentFrame)
        this.gameObject.SendMessageUpwards(trigger.name);
    }
    if (this.currentFrame < animation.frames.Length)
      return;
    if (this.loop)
      this.currentFrame = 0;
    else
      this.currentFrame = animation.frames.Length - 1;
  }

  public int GetFacing()
  {
    return (int) Mathf.Sign(this.spriteRenderer.transform.localScale.x);
  }

  public void FlipTo(float dir)
  {
    if ((double) dir < 0.0)
      this.spriteRenderer.transform.localScale = new Vector3(-1f, 1f, 1f);
    else
      this.spriteRenderer.transform.localScale = new Vector3(1f, 1f, 1f);
  }

  public void FlipTo(Vector3 position)
  {
    if ((double) position.x - (double) this.transform.position.x < 0.0)
      this.spriteRenderer.transform.localScale = new Vector3(-1f, 1f, 1f);
    else
      this.spriteRenderer.transform.localScale = new Vector3(1f, 1f, 1f);
  }

  [Serializable]
  public class AnimationTrigger
  {
    public string name;
    public int frame;
  }

  [Serializable]
  public class Animation
  {
    public string name;
    public int fps;
    public bool loop;
    public UnityEngine.Sprite[] frames;
    public string sequenceCode;
    public string cue;
    public SpriteAnimator.AnimationTrigger[] triggers;
  }
}
