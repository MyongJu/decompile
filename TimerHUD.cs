﻿// Decompiled with JetBrains decompiler
// Type: TimerHUD
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimerHUD : Hud
{
  private static bool m_Collapsed = true;
  private static readonly System.Type[] EMPTY_PARAMS = new System.Type[0];
  private static readonly object[] EMPTY_ARGS = new object[0];
  private Dictionary<long, TimerRenderer> m_TimerDict = new Dictionary<long, TimerRenderer>();
  private List<TimerRenderer> m_TimerList = new List<TimerRenderer>();
  private string MORE_FORMAT = string.Empty;
  [SerializeField]
  private GameObject m_ItemPrefab;
  [SerializeField]
  private GameObject m_TimerRoot;
  [SerializeField]
  private GameObject m_ButtonRoot;
  [SerializeField]
  private UISprite m_Background;
  [SerializeField]
  private UIButton m_ExpandButton;
  [SerializeField]
  private UIButton m_CollapseButton;
  [SerializeField]
  private UILabel m_MoreLabel;
  [SerializeField]
  private UITable m_Table;

  public bool Display
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  private void Start()
  {
    this.MORE_FORMAT = ScriptLocalization.Get("march_tab_view_more", true);
    this.UpdateUI();
  }

  private void OnJobCreated(long jobId)
  {
    this.AddTimer(jobId);
  }

  private void OnJobRemove(long jobId)
  {
    this.RemoveTimer(jobId);
  }

  private void OnLeave()
  {
    List<long> longList = new List<long>();
    int count1 = this.m_TimerList.Count;
    for (int index = 0; index < count1; ++index)
    {
      TimerRenderer timer = this.m_TimerList[index];
      if (TimerHUD.CanRemove(timer.Job))
        longList.Add(timer.Job.GetJobID());
    }
    int count2 = longList.Count;
    for (int index = 0; index < count2; ++index)
      this.RemoveTimer(longList[index]);
  }

  public void InitTimerHud()
  {
    Dictionary<long, JobHandle>.Enumerator enumerator = JobManager.Instance.GetJobDict().GetEnumerator();
    while (enumerator.MoveNext())
      this.AddTimer(enumerator.Current.Key);
    Oscillator.Instance.updateEvent += new System.Action<double>(this.OnUpdate);
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove += new System.Action<long>(this.OnJobRemove);
    AllianceManager.Instance.onLeave += new System.Action(this.OnLeave);
  }

  public void Dispose()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.OnUpdate);
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreated);
    JobManager.Instance.OnJobRemove -= new System.Action<long>(this.OnJobRemove);
    AllianceManager.Instance.onLeave -= new System.Action(this.OnLeave);
    using (List<TimerRenderer>.Enumerator enumerator = this.m_TimerList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TimerRenderer current = enumerator.Current;
        current.gameObject.SetActive(false);
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.m_TimerDict.Clear();
    this.m_TimerList.Clear();
  }

  public void AddTimer(long jobId)
  {
    if (this.m_TimerDict.ContainsKey(jobId))
      return;
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (!this.CanDisplay(job))
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ItemPrefab);
    gameObject.SetActive(true);
    gameObject.transform.parent = this.m_Table.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    TimerRenderer component = gameObject.GetComponent<TimerRenderer>();
    component.SetData(job);
    this.m_TimerDict.Add(jobId, component);
    this.m_TimerList.Add(component);
    this.UpdateUI();
  }

  public void RemoveTimer(long jobId)
  {
    TimerRenderer timerRenderer;
    if (!this.m_TimerDict.TryGetValue(jobId, out timerRenderer))
      return;
    this.m_TimerList.Remove(timerRenderer);
    this.m_TimerDict.Remove(jobId);
    timerRenderer.gameObject.SetActive(false);
    timerRenderer.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) timerRenderer.gameObject);
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    int count = this.m_TimerList.Count;
    this.m_TimerRoot.SetActive(count != 0);
    bool flag = TimerHUD.m_Collapsed;
    if (count > 2)
    {
      this.m_ButtonRoot.SetActive(true);
      this.m_MoreLabel.text = string.Format(this.MORE_FORMAT, (object) (count - 2));
    }
    else
    {
      this.m_ButtonRoot.SetActive(false);
      flag = true;
    }
    for (int index = 0; index < count; ++index)
      this.m_TimerList[index].gameObject.SetActive(true);
    if (flag)
    {
      for (int index = 2; index < count; ++index)
        this.m_TimerList[index].gameObject.SetActive(false);
    }
    this.m_ExpandButton.gameObject.SetActive(TimerHUD.m_Collapsed);
    this.m_CollapseButton.gameObject.SetActive(!TimerHUD.m_Collapsed);
    this.Reposition();
  }

  private void Reposition()
  {
    this.m_Table.onReposition = new UITable.OnReposition(this.OnReposition);
    this.m_Table.repositionNow = true;
  }

  private void OnReposition()
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.m_Table.transform);
    this.m_Background.width = (int) relativeWidgetBounds.size.x;
    this.m_Background.height = (int) relativeWidgetBounds.size.y;
    this.m_Background.ResizeCollider();
    bool activeSelf = this.m_ButtonRoot.activeSelf;
    this.m_ButtonRoot.SetActive(false);
    this.m_ButtonRoot.SetActive(activeSelf);
  }

  public void OnExpand()
  {
    TimerHUD.m_Collapsed = false;
    this.UpdateUI();
  }

  public void OnCollapse()
  {
    TimerHUD.m_Collapsed = true;
    this.UpdateUI();
  }

  private void OnUpdate(double dt)
  {
    int count = this.m_TimerList.Count;
    for (int index = 0; index < count; ++index)
      this.m_TimerList[index].OnUpdate(NetServerTime.inst.ServerTimestamp);
  }

  private bool CanDisplay(JobHandle job)
  {
    if ((bool) job)
    {
      switch (job.GetJobEvent())
      {
        case JobEvent.JOB_PVP_MARCHING:
        case JobEvent.JOB_PVP_RETURNING:
        case JobEvent.JOB_PVP_GATHERING:
        case JobEvent.JOB_PVP_ENCAMPING:
        case JobEvent.JOB_PVP_HODING_WONDER:
        case JobEvent.JOB_PVP_REINFORCING:
        case JobEvent.JOB_PVP_DEFENDING_FORTRESS:
        case JobEvent.JOB_PVP_DEMOLISHING_FORTRESS:
        case JobEvent.JOB_PVP_BUILDING:
        case JobEvent.JOB_PVP_DIGGING:
          return true;
        case JobEvent.JOB_PVP_WAIT_RALLY:
          if (job.RallyID != -1L)
            return !this.ContainsRally(job.RallyID);
          return false;
      }
    }
    return false;
  }

  private static bool CanRemove(JobHandle job)
  {
    return (bool) job && job.GetJobEvent() == JobEvent.JOB_PVP_WAIT_RALLY;
  }

  private bool ContainsRally(long rallyId)
  {
    for (int index = 0; index < this.m_TimerList.Count; ++index)
    {
      if (this.m_TimerList[index].Job.RallyID == rallyId)
        return true;
    }
    return false;
  }
}
