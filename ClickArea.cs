﻿// Decompiled with JetBrains decompiler
// Type: ClickArea
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public static class ClickArea
{
  public static readonly string ClickBuildingPrefix = "CityBuilding_";
  public static readonly string ClickCircleMenuPrefix = "CircleMenu_";
  public static readonly string ShopItemDetailPrefix = "ShopItemDetail_";
  public static readonly string MonthCardOutSide = nameof (MonthCardOutSide);
  public static readonly string MonthCardTimerCheast = nameof (MonthCardTimerCheast);
  public static readonly string FortuneTellerOutSide = nameof (FortuneTellerOutSide);
  public static readonly string FirstMarksmanOutSide = nameof (FirstMarksmanOutSide);
  public static readonly string DailyRewardOutSide = nameof (DailyRewardOutSide);
  public static readonly string Vip = "vip";
  public static readonly string Resource = nameof (Resource);
  public static readonly string ActivityCenter = nameof (ActivityCenter);
  public static readonly string PackageShop = nameof (PackageShop);
  public static readonly string Item = nameof (Item);
  public static readonly string MonthCardInFarm = nameof (MonthCardInFarm);
  public static readonly string PlayerInformation = nameof (PlayerInformation);
  public static readonly string SwichToKingdomMap = nameof (SwichToKingdomMap);
  public static readonly string SwichToCity = nameof (SwichToCity);
  public static readonly string MiniMap = nameof (MiniMap);
  public static readonly string TempleSkill = nameof (TempleSkill);
  public static readonly string HallOfKing = nameof (HallOfKing);
  public static readonly string PitRank = nameof (PitRank);
  public static readonly string PitGoBack = nameof (PitGoBack);
  public static readonly string ChestLibrary = nameof (ChestLibrary);
  public static readonly string PersonalChest = nameof (PersonalChest);
  public static readonly string AllianceWarBrife = nameof (AllianceWarBrife);
  public static readonly string Quest = nameof (Quest);
  public static readonly string Alliance = nameof (Alliance);
  public static readonly string Mail = nameof (Mail);
  public static readonly string Talent = nameof (Talent);
  public static readonly string IAPRecommendPackage = nameof (IAPRecommendPackage);
  public static readonly string IAPStore = nameof (IAPStore);
  public static readonly string IAPDailyRecharge = nameof (IAPDailyRecharge);
  public static readonly string IAPAnniversary = nameof (IAPAnniversary);
  public static readonly string IapRebate = nameof (IapRebate);
  public static readonly string AnniversaryTournament = nameof (AnniversaryTournament);
  public static readonly string Dice = nameof (Dice);
  public static readonly string Lottery = nameof (Lottery);
  public static readonly string FirstAnniversary = nameof (FirstAnniversary);
  public static readonly string KingdomBuff = nameof (KingdomBuff);
  public static readonly string Roulette = nameof (Roulette);
  public static readonly string GroupQuest = nameof (GroupQuest);
  public static readonly string QuickSearchMonster = nameof (QuickSearchMonster);
  public static readonly string SearchCoordinate = nameof (QuickSearchMonster);
}
