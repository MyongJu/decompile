﻿// Decompiled with JetBrains decompiler
// Type: AbyssMineInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class AbyssMineInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "lvl")]
  public int level;
  [Config(Name = "area_inside")]
  public int areaInside;
  [Config(Name = "area_outside")]
  public int areaOutside;
  [Config(Name = "pickingrate_s")]
  public float pickingRate;
  [Config(Name = "int_resource_value")]
  public int resourceValue;
}
