﻿// Decompiled with JetBrains decompiler
// Type: MailList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class MailList
{
  private List<long> _mailIDs = new List<long>();
  private List<AbstractMailEntry> entryList = new List<AbstractMailEntry>();
  private const int MAIL_STORAGE_TIME = 36000;
  private const string MAIL_STORAGE_TIME_KEY = "mail_storage_time";
  public System.Action<AbstractMailEntry, bool> mailAdded;
  public System.Action<AbstractMailEntry> mailRemoved;
  public System.Action<AbstractMailEntry> mailUpdate;
  public MailController controller;
  private long _lowMailId;
  private bool initailized;
  private int _unReadCount;

  public MailList(MailCategory type)
  {
    this.MailCategory = type;
  }

  public MailList(MailCategory type, MailController controller, MailAPI api)
  {
    this.MailCategory = type;
    this.controller = controller;
    this.MailAPI = api;
  }

  public MailAPI MailAPI { get; set; }

  public MailCategory MailCategory { get; private set; }

  public int LoadedCount
  {
    get
    {
      return this._mailIDs.Count;
    }
  }

  public int TotalCount
  {
    get
    {
      return this.LoadedCount;
    }
  }

  public long LowerMailId
  {
    get
    {
      return this._lowMailId;
    }
    set
    {
      this._lowMailId = value;
    }
  }

  public void Init(System.Action initCallBack)
  {
    if (this.MailCategory == MailCategory.Favorite)
    {
      this._mailIDs.Clear();
      this.entryList.Clear();
      this._lowMailId = 0L;
      this.GetMoreFavMail(initCallBack, true);
    }
    else if (!this.initailized)
    {
      this._mailIDs.Clear();
      this.entryList.Clear();
      this._lowMailId = 0L;
      this.GetMoreMail(initCallBack, true, true);
      this.initailized = true;
    }
    else
      initCallBack();
  }

  public int GetMailIndex(long mailID)
  {
    for (int index = 0; index < this._mailIDs.Count; ++index)
    {
      if (this._mailIDs[index] == mailID)
        return index;
    }
    return 0;
  }

  public void AddMailID(long mailID, bool fromLoad)
  {
    if (this._mailIDs.Contains(mailID))
    {
      this.entryList.Sort((Comparison<AbstractMailEntry>) ((x, y) => (int) (y.timeAsInt - x.timeAsInt)));
    }
    else
    {
      this._mailIDs.Add(mailID);
      this._mailIDs.Sort((Comparison<long>) ((x, y) => -(int) (x - y)));
      AbstractMailEntry mailById = this.GetMailByID(mailID);
      if (mailById.LanguageMatch())
        this.entryList.Add(mailById);
      this.entryList.Sort((Comparison<AbstractMailEntry>) ((x, y) => (int) (y.timeAsInt - x.timeAsInt)));
      if (!fromLoad && !mailById.isRead)
        ++this.UnReadCount;
      if (this.mailAdded == null)
        return;
      this.mailAdded(this.controller.mailsById[mailID], fromLoad);
    }
  }

  public void AddMailEntry(AbstractMailEntry entry, bool fromLoad)
  {
    if (this._mailIDs.Contains(entry.mailID))
    {
      this.entryList.Sort((Comparison<AbstractMailEntry>) ((x, y) => (int) (y.timeAsInt - x.timeAsInt)));
    }
    else
    {
      this._mailIDs.Add(entry.mailID);
      this._mailIDs.Sort((Comparison<long>) ((x, y) => -(int) (x - y)));
      if (this.GetMailByID(entry.mailID).LanguageMatch())
        this.entryList.Add(this.GetMailByID(entry.mailID));
      this.entryList.Sort((Comparison<AbstractMailEntry>) ((x, y) => (int) (y.timeAsInt - x.timeAsInt)));
      if (!fromLoad)
        ++this.UnReadCount;
      if (this.mailAdded == null)
        return;
      this.mailAdded(entry, fromLoad);
    }
  }

  public void RemoveMail(long mailID, bool sendEvent = true)
  {
    if (!this._mailIDs.Contains(mailID))
      return;
    this._mailIDs.Remove(mailID);
    AbstractMailEntry mailById = this.GetMailByID(mailID);
    if (!mailById.isRead)
      --this.UnReadCount;
    this.entryList.Remove(mailById);
    if (this.mailRemoved == null || !sendEvent)
      return;
    this.mailRemoved((AbstractMailEntry) null);
  }

  public void UpdateMailEntry(AbstractMailEntry entry)
  {
    if (this.mailUpdate == null)
      return;
    this.mailUpdate(entry);
  }

  public AbstractMailEntry GetMailByID(long mailID)
  {
    return this.controller.mailsById[mailID];
  }

  public void GetMailByIndex(int index, System.Action<AbstractMailEntry> callback)
  {
    if (index >= this.LoadedCount || index < 0 || callback == null)
      return;
    if (this._mailIDs.Count > index)
    {
      callback(this.controller.mailsById[this._mailIDs[index]]);
    }
    else
    {
      this.GetMoreMail((System.Action) null, true, true);
      this.GetMailByIndex(index, callback);
    }
  }

  public AbstractMailEntry GetNextMail(AbstractMailEntry current)
  {
    List<AbstractMailEntry> mail = this.GetMail();
    if (!mail.Contains(current))
      return (AbstractMailEntry) null;
    int num = mail.IndexOf(current);
    if (num < mail.Count - 2)
      return mail[num + 1];
    return (AbstractMailEntry) null;
  }

  public AbstractMailEntry GetPreMail(AbstractMailEntry current)
  {
    List<AbstractMailEntry> mail = this.GetMail();
    if (!mail.Contains(current))
      return (AbstractMailEntry) null;
    int num = mail.IndexOf(current);
    if (num > 0)
      return mail[num - 1];
    return (AbstractMailEntry) null;
  }

  public List<AbstractMailEntry> GetMail()
  {
    return this.entryList;
  }

  public List<AbstractMailEntry> FilterMailList(string senderName)
  {
    List<AbstractMailEntry> abstractMailEntryList = new List<AbstractMailEntry>();
    List<long>.Enumerator enumerator = this._mailIDs.GetEnumerator();
    while (enumerator.MoveNext())
    {
      AbstractMailEntry mailById = this.GetMailByID(enumerator.Current);
      if (mailById.sender.Contains(senderName))
        abstractMailEntryList.Add(mailById);
    }
    return abstractMailEntryList;
  }

  public void GetMoreMail(System.Action initCallBack = null, bool blockScene = true, bool warningfull = true)
  {
    if (this._mailIDs.Count < this.controller.FullCount())
    {
      this.controller.LoadMailList(this.MailCategory, this._lowMailId, initCallBack, blockScene);
    }
    else
    {
      if (warningfull)
        this.controller.ShowFullWarning();
      if (initCallBack == null)
        return;
      initCallBack();
    }
  }

  public void GetMoreFavMail(System.Action initCallBack = null, bool blockScene = true)
  {
    this.controller.LoadFavMailList(this._lowMailId, initCallBack, blockScene);
  }

  private int MailStorageTime()
  {
    if (ConfigManager.inst.DB_GameConfig.GetData("mail_storage_time") != null)
      return (int) ConfigManager.inst.DB_GameConfig.GetData("mail_storage_time").Value;
    return 36000;
  }

  public int GetUnreadedCount()
  {
    return this._unReadCount;
  }

  public void Clear()
  {
    this.mailAdded = (System.Action<AbstractMailEntry, bool>) null;
    this.mailRemoved = (System.Action<AbstractMailEntry>) null;
    this.mailUpdate = (System.Action<AbstractMailEntry>) null;
  }

  public int UnReadCount
  {
    get
    {
      return this._unReadCount;
    }
    set
    {
      this._unReadCount = value;
    }
  }

  public void Sort()
  {
    this.entryList.Sort((Comparison<AbstractMailEntry>) ((x, y) => (int) (y.timeAsInt - x.timeAsInt)));
    if (this.mailUpdate == null)
      return;
    this.mailUpdate((AbstractMailEntry) null);
  }
}
