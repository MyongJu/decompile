﻿// Decompiled with JetBrains decompiler
// Type: Shooter
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class Shooter : MonoBehaviour
{
  private Shooter.ShooterPara sp;
  private bool canShoot;
  private Vector3 currentPos;
  private Vector3 currentSpeed;
  private float rTime;
  private float abSpeed;
  private float delaytime;
  public string shootname;

  public void Shoot(Shooter.ShooterPara sp, float delay = 0.0f)
  {
    this.sp = sp;
    this.currentSpeed = sp.initspeed * sp.speedFactor;
    this.currentPos = sp.go.transform.localPosition;
    this.rTime = sp.reductionTime;
    this.abSpeed = sp.absorbBaseSpeed;
    this.StartCoroutine(this.Begin(delay));
  }

  [DebuggerHidden]
  private IEnumerator Begin(float time)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Shooter.\u003CBegin\u003Ec__IteratorB3()
    {
      time = time,
      \u003C\u0024\u003Etime = time,
      \u003C\u003Ef__this = this
    };
  }

  private void Update()
  {
    if (!this.canShoot)
      return;
    this.abSpeed += this.sp.absorbAcceleration * Time.deltaTime;
    Vector3 vector3 = (this.sp.end - this.sp.go.transform.localPosition).normalized * this.abSpeed;
    this.rTime -= Time.deltaTime;
    if ((double) this.rTime < 0.0)
      this.rTime = 0.0f;
    this.currentSpeed *= this.rTime;
    this.currentPos += (this.currentSpeed + vector3) * Time.deltaTime;
    if ((double) Vector3.Dot(this.currentPos - this.sp.end, this.sp.go.transform.localPosition - this.sp.end) <= 0.0)
    {
      this.sp.go.transform.localPosition = Vector3.zero;
      this.canShoot = false;
      if (this.sp.onFinished == null)
        return;
      this.sp.onFinished();
    }
    else
      this.sp.go.transform.localPosition = this.currentPos;
  }

  public class ShooterPara
  {
    public float speedFactor = 20f;
    public float reductionTime = 1f;
    public GameObject go;
    public Vector3 begin;
    public Vector3 end;
    public Vector3 initspeed;
    public System.Action onFinished;
    public float absorbBaseSpeed;
    public float absorbAcceleration;
  }
}
