﻿// Decompiled with JetBrains decompiler
// Type: DungeonMopupRewardRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DungeonMopupRewardRenderer : MonoBehaviour
{
  public UITexture _itemIcon;
  public UILabel _itemCount;

  public void SetData(int itemId, int count)
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(itemId);
    if (itemStaticInfo == null)
      return;
    BuilderFactory.Instance.HandyBuild((UIWidget) this._itemIcon, itemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    this._itemCount.text = count.ToString();
  }
}
