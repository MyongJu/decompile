﻿// Decompiled with JetBrains decompiler
// Type: LegendCard
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using UI;
using UnityEngine;

public class LegendCard : MonoBehaviour
{
  public UITexture card;
  public UILabel reqLvlLbl;
  public UILabel reqCoinLbl;
  public GameObject infoBtn;
  private LegendCard.Data _data;
  private int curLvl;
  private long silver;
  public int wrapIndex;
  public int realIndex;
  public System.Action<GameObject, int, int> recruiteSuccess;

  public void SeedData(LegendCard.Data data)
  {
    this._data = data;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.card, data.image, (System.Action<bool>) null, true, false, string.Empty);
    BuildingController buildingByType = CitadelSystem.inst.GetBuildingByType("dragon_lair");
    if ((UnityEngine.Object) null != (UnityEngine.Object) buildingByType)
      this.curLvl = buildingByType.mBuildingItem.mLevel;
    this.reqLvlLbl.text = data.name;
    if (this.curLvl < data.reqLvl)
      this.reqLvlLbl.color = Color.red;
    else
      this.reqLvlLbl.color = Color.white;
    this.silver = (long) PlayerData.inst.playerCityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    this.reqCoinLbl.text = (long) data.reqCoin <= this.silver ? Utils.FormatShortThousands(data.reqCoin) + "/" + Utils.FormatShortThousandsLong(this.silver) : "[ff0000]" + Utils.FormatShortThousands(data.reqCoin) + "[-]/" + Utils.FormatShortThousandsLong(this.silver);
    if ("Texture/Legend/Image/card_hero_locked" == data.image)
      this.infoBtn.SetActive(false);
    else
      this.infoBtn.SetActive(true);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.card);
  }

  public void Reset()
  {
    this.transform.localPosition = Vector3.zero;
  }

  public void OnCardClicked()
  {
    if (this.curLvl < this._data.reqLvl)
      return;
    if (this.silver < (long) this._data.reqCoin)
      GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.SILVER);
    else if (DBManager.inst.DB_Legend.IsMaxLegendCount())
    {
      if (DBManager.inst.DB_Legend.IsMaxSlot())
        UIManager.inst.toast.Show(ScriptLocalization.Get("legend_max_slot", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenPopup("GetMoreLegend", (Popup.PopupParameter) new GetMoreLegendDlg.GetMoreSlotDialogParamer());
    }
    else
      MessageHub.inst.GetPortByAction("Legend:recruit").SendRequest(new Hashtable()
      {
        {
          (object) "city_id",
          (object) PlayerData.inst.cityId
        },
        {
          (object) "building_id",
          (object) CityManager.inst.mTavern.mID
        },
        {
          (object) "legend_config_id",
          (object) this._data.configId
        },
        {
          (object) "uid",
          (object) PlayerData.inst.uid
        }
      }, new System.Action<bool, object>(this.RecruiteCallBack), true);
  }

  private void RecruiteCallBack(bool success, object result)
  {
    if (!success)
      return;
    UIManager.inst.toast.Show(string.Format(ScriptLocalization.Get("toast_summon_success", true), (object) this.reqLvlLbl.text), (System.Action) null, 4f, false);
    if (this.recruiteSuccess == null)
      return;
    this.recruiteSuccess(this.gameObject, this.wrapIndex, this.realIndex);
  }

  public void OnInfoClicked()
  {
    UIManager.inst.OpenDlg("Legend/LegendRecruitDlg", (UI.Dialog.DialogParameter) new LegendRecruitDlg.LegendRecruitDlgParameter()
    {
      legendConfigId = this._data.configId
    }, true, true, true);
  }

  public class Data
  {
    public int configId;
    public string image;
    public string name;
    public int reqLvl;
    public int reqCoin;
  }
}
