﻿// Decompiled with JetBrains decompiler
// Type: DragonSkillDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class DragonSkillDlg : UI.Dialog
{
  public float dragonRotateFactor = 1f;
  public UILabel title;
  public UILabel dragonName;
  public UITexture renameIcon;
  public SkillContainer[] container;
  public DragonPowerItem powerItem;
  public GameObject dragonRoot;
  private DragonView _dragonView;
  public UITexture dragonTexture;
  public GameObject nomarlBg;
  public GameObject light_01_bg;
  public GameObject light_02_bg;
  public GameObject dark_01_bg;
  public GameObject dark_02_bg;
  public GameObject resetSkillEffect;
  public UIButton resetButton;
  public UIButton assignButton;
  public DragonTendencySkilBar tendencySkillBar;
  private bool needRefresh;

  private void PlayDragonBg()
  {
    NGUITools.SetActive(this.nomarlBg, false);
    NGUITools.SetActive(this.light_01_bg, false);
    NGUITools.SetActive(this.light_02_bg, false);
    NGUITools.SetActive(this.dark_01_bg, false);
    NGUITools.SetActive(this.dark_02_bg, false);
    switch (PlayerData.inst.dragonData.tendency)
    {
      case DragonData.Tendency.normal:
        NGUITools.SetActive(this.nomarlBg, true);
        break;
      case DragonData.Tendency.ferocious:
        NGUITools.SetActive(this.dark_02_bg, true);
        break;
      case DragonData.Tendency.swift:
        NGUITools.SetActive(this.dark_01_bg, true);
        break;
      case DragonData.Tendency.pure:
        NGUITools.SetActive(this.light_01_bg, true);
        break;
      case DragonData.Tendency.shine:
        NGUITools.SetActive(this.light_02_bg, true);
        break;
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    if ((UnityEngine.Object) this.dragonRoot != (UnityEngine.Object) null)
    {
      this._dragonView = new DragonView();
      this._dragonView.Mount(this.dragonRoot.transform);
      this._dragonView.SetPosition(Vector3.zero);
      this._dragonView.SetRotation(Quaternion.identity);
      this._dragonView.SetScale(new Vector3(400f, 400f, 400f));
      this._dragonView.SetRendererLayer("UI3D");
    }
    this.AddEventHandler();
    this.UpdateUI();
    if (!LinkerHub.Instance.NeedGuide)
      return;
    LinkerHub.Instance.MountHint(this.assignButton.transform, 0, (object) null);
    LinkerHub.Instance.NeedGuide = false;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.RefreshDragon();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    if (this._dragonView != null)
    {
      this._dragonView.Dispose();
      this._dragonView = (DragonView) null;
    }
    base.OnClose(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  private void Update()
  {
    if (!this.needRefresh)
      return;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    this.needRefresh = false;
    NGUITools.SetActive(this.resetSkillEffect, false);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.renameIcon, Utils.GetUITextPath() + "item_dragon_rename", (System.Action<bool>) null, true, false, string.Empty);
    this.title.text = ScriptLocalization.Get("dragon_skill_title", true);
    this.dragonName.text = ScriptLocalization.Get(PlayerData.inst.dragonData.Name, true);
    this.powerItem.Refresh();
    for (int index = 0; index < this.container.Length; ++index)
      this.container[index].Refresh();
    this.powerItem.Refresh();
    if (this._dragonView != null)
      this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
      {
        if (!ret)
          return;
        this._dragonView.SetRendererLayer("UI3D");
        this._dragonView.Show();
        this._dragonView.SetAnimationStateTrigger("EnterUI");
      }));
    this.resetButton.isEnabled = DBManager.inst.DB_DragonSkill.CanResetAllSKill();
    this.PlayDragonBg();
    this.tendencySkillBar.UpdateUI();
  }

  private void RefreshDragon()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.LoadAsync((SpriteViewData) DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 1), true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer("UI3D");
      this._dragonView.PlayAnimation("idle", 0.0f);
    }));
  }

  private void Clear()
  {
    for (int index = 0; index < this.container.Length; ++index)
      this.container[index].Clear();
    this.powerItem.Clear();
    BuilderFactory.Instance.Release((UIWidget) this.renameIcon);
  }

  private void ResetEffect()
  {
    if (!((UnityEngine.Object) this.resetSkillEffect != (UnityEngine.Object) null))
      return;
    NGUITools.SetActive(this.resetSkillEffect, false);
    NGUITools.SetActive(this.resetSkillEffect, true);
  }

  public void ResetSkill()
  {
    ShopStaticInfo shopData = ConfigManager.inst.DB_Shop.GetShopData("shopitem_dragon_skill_reset_all");
    UIManager.inst.OpenPopup("UseOrBuyAndUseItemPopup", (Popup.PopupParameter) new UseOrBuyAndUseItemPopup.Parameter()
    {
      itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(shopData.Item_InternalId),
      callback = new System.Action<bool, object>(this.UseItemCallBack),
      titleText = Utils.XLAT("dragon_skill_reset_skill_title"),
      btnText = Utils.XLAT("dragon_skill_reset_skill_button")
    });
  }

  private void UseItemCallBack(bool success, object result)
  {
    if (!success)
      return;
    this.ResetEffect();
    AudioManager.Instance.PlaySound("sfx_city_dragon_skill_reset_all", false);
  }

  public void OnHelpBtClick()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_dragon_skill"
    });
  }

  public void DistributeSkill()
  {
    LinkerHub.Instance.DisposeHint(this.assignButton.transform);
    UIManager.inst.OpenDlg("Dragon/DragonSkillConfigDialog", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void AddEventHandler()
  {
    DBManager.inst.DB_Dragon.onDataChanged += new System.Action<long>(this.OnDataChanged);
    DBManager.inst.DB_DragonSkill.onDataChanged += new System.Action<long>(this.OnDataChanged);
    UICamera.onDrag += new UICamera.VectorDelegate(this.OnDragEvent);
  }

  private void OnDataChanged(long id)
  {
    if (id != PlayerData.inst.uid)
      return;
    this.needRefresh = true;
  }

  private void RemoveEventHandler()
  {
    UICamera.onDrag -= new UICamera.VectorDelegate(this.OnDragEvent);
    DBManager.inst.DB_Dragon.onDataChanged -= new System.Action<long>(this.OnDataChanged);
    DBManager.inst.DB_DragonSkill.onDataChanged -= new System.Action<long>(this.OnDataChanged);
  }

  private void OnDragEvent(GameObject go, Vector2 delta)
  {
    if ((UnityEngine.Object) go != (UnityEngine.Object) this.dragonTexture.gameObject)
      return;
    this.dragonRoot.transform.Rotate(Vector3.up, -delta.x / this.dragonRotateFactor);
  }

  public void ChangeDragonName()
  {
    UIManager.inst.OpenPopup("Dragon/DragonRenamePopup", (Popup.PopupParameter) null);
  }
}
