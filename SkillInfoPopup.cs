﻿// Decompiled with JetBrains decompiler
// Type: SkillInfoPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class SkillInfoPopup : Popup
{
  private SkillInfoPopup.SkillButtonState curSkillBtnState = SkillInfoPopup.SkillButtonState.INVALID;
  public UITexture skillTexture;
  public UISprite lockSprite;
  public UISprite manaSprite;
  public UILabel skillName;
  public UILabel skillDesc;
  public UILabel hintText;
  public UILabel needSpendCount;
  public UILabel btnHintText;
  public UIButton skillBtn;
  private int itemId;
  private long attackingAllianceId;
  private bool skillTextureSet;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.attackingAllianceId = 0L;
    this.skillTextureSet = false;
    SkillInfoPopup.Parameter parameter = orgParam as SkillInfoPopup.Parameter;
    if (parameter != null)
    {
      this.itemId = parameter.itemId;
      this.attackingAllianceId = parameter.attackingAllianceId;
    }
    this.AddEventHandler();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.RemoveEventHandler();
  }

  public void OnSkillBtnPressed()
  {
    switch (this.curSkillBtnState)
    {
      case SkillInfoPopup.SkillButtonState.INSUFFICIENT_LEVEL:
      case SkillInfoPopup.SkillButtonState.MANA_OTHER_USING:
      case SkillInfoPopup.SkillButtonState.MANA_COOLING_DOWN:
      case SkillInfoPopup.SkillButtonState.INVALID:
        this.OnCloseBtnPressed();
        break;
      case SkillInfoPopup.SkillButtonState.MANA_NOT_ENOUGH:
        UIManager.inst.OpenPopup("Alliance/DragonAltarRestoreManaPopup", (Popup.PopupParameter) null);
        this.OnCloseBtnPressed();
        break;
      case SkillInfoPopup.SkillButtonState.MANA_CAN_RELEASE:
        TempleSkillInfo skillInfo = ConfigManager.inst.DB_TempleSkill.GetById(this.itemId);
        if (skillInfo == null)
          break;
        UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
        UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
        if (skillInfo.NeedSelection && this.attackingAllianceId == 0L)
        {
          if (GameEngine.Instance.CurrentGameMode != GameEngine.GameMode.PVPMode)
          {
            UIManager.inst.Cloud.CoverCloud((System.Action) (() =>
            {
              GameEngine.Instance.CurrentGameMode = GameEngine.GameMode.PVPMode;
              PVPMap.PendingGotoRequest = PlayerData.inst.CityData.Location;
              if (skillInfo.TargetArea > 1)
                PVPSystem.Instance.Map.EnterMultipleSpellMode(skillInfo, PlayerData.inst.CityData.Location);
              else
                PVPSystem.Instance.Map.EnterSingleSpellMode(skillInfo);
            }));
            break;
          }
          if (skillInfo.TargetArea > 1)
          {
            PVPSystem.Instance.Map.EnterMultipleSpellMode(skillInfo);
            break;
          }
          PVPSystem.Instance.Map.EnterSingleSpellMode(skillInfo);
          break;
        }
        Hashtable postData = new Hashtable();
        postData[(object) "config_id"] = (object) skillInfo.internalId;
        if (this.attackingAllianceId != 0L)
        {
          Hashtable hashtable = new Hashtable();
          hashtable[(object) "alliance_id"] = (object) this.attackingAllianceId;
          postData[(object) "target"] = (object) hashtable;
        }
        MessageHub.inst.GetPortByAction("AllianceTemple:castSpell").SendRequest(postData, (System.Action<bool, object>) ((result, pram) =>
        {
          if (!result || skillInfo.NeedDonation != 0)
            return;
          AudioManager.Instance.PlaySound("sfx_alliance_altar_cast_spell", false);
        }), true);
        break;
    }
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void UpdateUI()
  {
    AllianceMagicData currentPowerUpSkill = DBManager.inst.DB_AllianceMagic.GetCurrentPowerUpSkill();
    AllianceData allianceData = DBManager.inst.DB_Alliance.Get(PlayerData.inst.allianceId);
    TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(this.itemId);
    bool flag1 = currentPowerUpSkill != null;
    int time = 0;
    if (allianceData != null && byId != null)
    {
      time = DBManager.inst.DB_AllianceMagic.GetSkillCdTime(PlayerData.inst.allianceId, byId.internalId);
      if (allianceData.manaPoint < byId.spend)
        this.needSpendCount.color = Color.red;
    }
    if (!Utils.IsR4Member() && !Utils.IsR5Member())
    {
      this.btnHintText.text = Utils.XLAT("id_uppercase_okay");
      this.hintText.text = Utils.XLAT("alliance_altar_spell_rank_tip");
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.INSUFFICIENT_LEVEL;
    }
    else if (flag1)
    {
      this.btnHintText.text = Utils.XLAT("id_uppercase_okay");
      this.hintText.text = Utils.XLAT("alliance_altar_spell_busy_tip");
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.MANA_OTHER_USING;
    }
    else if (time > 0)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para.Add("0", Utils.FormatTime(time, true, false, true));
      this.btnHintText.text = Utils.XLAT("id_uppercase_okay");
      this.hintText.text = ScriptLocalization.GetWithPara("alliance_altar_spell_cooldown_remaining", para, true);
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.MANA_COOLING_DOWN;
    }
    else if (this.attackingAllianceId != 0L)
    {
      this.btnHintText.text = Utils.XLAT("alliance_altar_uppercase_disrupt");
      this.hintText.text = string.Empty;
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.MANA_CAN_RELEASE;
    }
    else if (byId != null && allianceData != null && byId.spend <= allianceData.manaPoint)
    {
      this.btnHintText.text = Utils.XLAT("alliance_altar_cast_spell_button");
      this.hintText.text = string.Empty;
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.MANA_CAN_RELEASE;
    }
    else
    {
      this.btnHintText.text = Utils.XLAT("alliance_altar_use_elixir_button");
      this.hintText.text = Utils.XLAT("alliance_altar_spell_points_tip");
      this.curSkillBtnState = SkillInfoPopup.SkillButtonState.MANA_NOT_ENOUGH;
    }
    if (byId == null)
      return;
    this.needSpendCount.text = Utils.FormatThousands(byId.spend.ToString());
    this.skillName.text = byId.Name;
    this.skillDesc.text = ConfigManager.inst.DB_TempleSkill.GetSkillDescription(this.itemId);
    bool flag2 = Utils.GetPlayerAllianceTempleLevel() >= byId.UnlockLevel;
    NGUITools.SetActive(this.lockSprite.gameObject, !flag2);
    if (!this.skillTextureSet)
    {
      BuilderFactory.Instance.HandyBuild((UIWidget) this.skillTexture, "Texture/DragonAltarSkills/" + byId.Icon, (System.Action<bool>) null, true, false, string.Empty);
      this.skillTextureSet = true;
    }
    if (!flag2)
    {
      if (this.curSkillBtnState == SkillInfoPopup.SkillButtonState.MANA_CAN_RELEASE)
      {
        this.btnHintText.text = Utils.XLAT("id_uppercase_okay");
        this.hintText.text = string.Empty;
        this.curSkillBtnState = SkillInfoPopup.SkillButtonState.INVALID;
      }
      GreyUtility.Grey(this.skillTexture.gameObject);
      GreyUtility.Grey(this.manaSprite.gameObject);
    }
    else
    {
      GreyUtility.Normal(this.skillTexture.gameObject);
      GreyUtility.Normal(this.manaSprite.gameObject);
    }
  }

  private void UpdateSkillBtnStatus(bool isEnable)
  {
    this.skillBtn.isEnabled = isEnable;
    this.skillBtn.UpdateColor(true);
  }

  private void OnSecondHandler(int time)
  {
    this.UpdateUI();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.OnSecondHandler);
  }

  private void RemoveEventHandler()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.OnSecondHandler);
  }

  public class Parameter : Popup.PopupParameter
  {
    public int itemId;
    public long attackingAllianceId;
  }

  public enum SkillButtonState
  {
    INSUFFICIENT_LEVEL,
    MANA_NOT_ENOUGH,
    MANA_OTHER_USING,
    MANA_CAN_RELEASE,
    MANA_COOLING_DOWN,
    INVALID,
  }
}
