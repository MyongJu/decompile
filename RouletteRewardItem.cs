﻿// Decompiled with JetBrains decompiler
// Type: RouletteRewardItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class RouletteRewardItem
{
  public int groupId = -1;
  public int strongholdRank = -1;
  public int index;
  public float startAngle;
  public float endAngle;
  public float stopAngle;
  public string rewardItemName;
  public string rewardItemType;
  public int rewardItemCount;
}
