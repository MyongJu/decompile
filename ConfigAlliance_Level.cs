﻿// Decompiled with JetBrains decompiler
// Type: ConfigAlliance_Level
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigAlliance_Level
{
  private Dictionary<string, Alliance_LevelInfo> m_DataByUniqueID;
  private Dictionary<int, Alliance_LevelInfo> m_DataByInternalID;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<Alliance_LevelInfo, string>(res as Hashtable, "ID", out this.m_DataByUniqueID, out this.m_DataByInternalID);
  }

  public void Clear()
  {
    if (this.m_DataByUniqueID != null)
      this.m_DataByUniqueID.Clear();
    if (this.m_DataByInternalID == null)
      return;
    this.m_DataByInternalID.Clear();
  }

  public Alliance_LevelInfo GetData(int internalId)
  {
    if (this.m_DataByInternalID.ContainsKey(internalId))
      return this.m_DataByInternalID[internalId];
    return (Alliance_LevelInfo) null;
  }

  public Alliance_LevelInfo GetDataByLevel(int level)
  {
    Dictionary<string, Alliance_LevelInfo>.Enumerator enumerator = this.m_DataByUniqueID.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.Value.Alliance_Level == level)
        return enumerator.Current.Value;
    }
    return new Alliance_LevelInfo();
  }

  public int MaxLevel
  {
    get
    {
      return this.m_DataByUniqueID.Count;
    }
  }
}
