﻿// Decompiled with JetBrains decompiler
// Type: AnniversaryOtherController
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class AnniversaryOtherController : MonoBehaviour
{
  public UILabel eventTitle;
  public UILabel eventDescription;
  public UILabel awardsTitle;
  public UILabel buttonLabel;
  public UIButton actionButton;

  public void OnEnable()
  {
    EventDelegate.Add(this.actionButton.onClick, new EventDelegate.Callback(this.OnBtnClicked));
  }

  public void OnDisable()
  {
    EventDelegate.Remove(this.actionButton.onClick, new EventDelegate.Callback(this.OnBtnClicked));
  }

  public virtual void UpdatePageContent()
  {
    this.eventTitle.text = Utils.XLAT("help_event_intro_title");
    this.awardsTitle.text = Utils.XLAT("id_rewards");
  }

  public abstract void OnBtnClicked();

  public abstract void UpdateUI();
}
