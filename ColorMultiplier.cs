﻿// Decompiled with JetBrains decompiler
// Type: ColorMultiplier
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ColorMultiplier : MonoBehaviour
{
  public float currentValue = 1f;
  private Color _originalColor;
  private UISprite _sprite;
  private UILabel _label;

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ColorMultiplier.\u003CStart\u003Ec__Iterator49()
    {
      \u003C\u003Ef__this = this
    };
  }

  public void MultiplyColor(float value)
  {
    this.StartCoroutine(this._MultiplyColor(value));
  }

  [DebuggerHidden]
  private IEnumerator _MultiplyColor(float value)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ColorMultiplier.\u003C_MultiplyColor\u003Ec__Iterator4A()
    {
      value = value,
      \u003C\u0024\u003Evalue = value,
      \u003C\u003Ef__this = this
    };
  }
}
