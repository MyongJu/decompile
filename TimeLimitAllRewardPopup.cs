﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitAllRewardPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimeLimitAllRewardPopup : Popup
{
  private ActivityMainInfo mainInfo;
  private List<RoundActivityRankRewardUIBaseData> rewardInfos;
  public UITable table;
  public UIScrollView scrollView;
  public Icon template1;
  public Icon template2;
  public Icon subTitle;
  public UILabel subTitleLabel;
  private bool isTotalReward;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.isTotalReward = (orgParam as TimeLimitAllRewardPopup.Parameter).isTotalReward;
    this.rewardInfos = ActivityManager.Intance.GetRoundReward((orgParam as TimeLimitAllRewardPopup.Parameter).data, this.isTotalReward);
    this.subTitleLabel.text = (orgParam as TimeLimitAllRewardPopup.Parameter).subTitleString;
    this.UpdateUI();
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void UpdateUI()
  {
    this.ClearTable();
    for (int index = 0; index < this.rewardInfos.Count; ++index)
      this.AddReward(this.rewardInfos[index]);
    this.table.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
  }

  private void AddReward(RoundActivityRankRewardUIBaseData reward)
  {
    GameObject gameObject1 = NGUITools.AddChild(this.table.gameObject, this.subTitle.gameObject);
    gameObject1.SetActive(true);
    gameObject1.GetComponent<Icon>().FeedData((IComponentData) new IconData(string.Empty, new string[1]
    {
      reward.Title
    }));
    List<int> currentItems = reward.CurrentItems;
    Dictionary<int, int> currentRewardItems = reward.CurrentRewardItems;
    for (int index = 0; index < currentItems.Count; ++index)
    {
      int internalId = currentItems[index];
      int num = currentRewardItems[internalId];
      GameObject gameObject2 = NGUITools.AddChild(this.table.gameObject, this.template1.gameObject);
      gameObject2.SetActive(true);
      ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
      IconData iconData = new IconData(itemStaticInfo.ImagePath, new string[2]
      {
        itemStaticInfo.LocName,
        "X" + num.ToString()
      });
      iconData.Data = (object) internalId;
      Icon component = gameObject2.GetComponent<Icon>();
      component.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      component.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      component.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      component.FeedData((IComponentData) iconData);
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private void ClearTable()
  {
    for (int index = 0; index < this.table.transform.childCount; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.table.transform.GetChild(index).gameObject);
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool isTotalReward = true;
    public string subTitleString;
    public ActivityBaseData data;
  }
}
