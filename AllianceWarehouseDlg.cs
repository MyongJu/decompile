﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarehouseDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Diagnostics;
using UI;
using UnityEngine;

public class AllianceWarehouseDlg : UI.Dialog
{
  public float m_scaleForCityIcon = 1.4f;
  public Vector3 m_offsetForCityIcon = new Vector3(0.0f, 70f, 0.0f);
  public float m_scaleForWarehouse = 1.4f;
  public Vector3 m_offsetForWarehouse = new Vector3(0.0f, 70f, 0.0f);
  private const int TagFood = 1;
  private const int TagWood = 2;
  private const int TagOre = 3;
  private const int TagSliver = 4;
  public GameObject m_rootStore;
  public GameObject m_rootTakeback;
  public UIInput m_foodCount;
  public UIInput m_woodCount;
  public UIInput m_oreCount;
  public UIInput m_sliverCount;
  public UISlider m_foodSlider;
  public UISlider m_woodSlider;
  public UISlider m_oreSlider;
  public UISlider m_sliverSlider;
  public UILabel m_title;
  public Color m_normalTextColor;
  public Color m_warningTextColor;
  public UITexture m_myCityIcon_store;
  public UILabel m_myResource_store;
  public UILabel m_myFoodCount_store;
  public UILabel m_myWoodCount_store;
  public UILabel m_myOreCount_store;
  public UILabel m_mySliverCount_store;
  public UITexture m_warehouseIcon_store;
  public UILabel m_warehouseResource_store;
  public UILabel m_warehouseFootCount_store;
  public UILabel m_warehouseWoodCount_store;
  public UILabel m_warehouseOreCount_store;
  public UILabel m_warehouseSliverCount_store;
  public UILabel m_warehouseDailyLimit_store;
  public UILabel m_warehouseMaxCapicity_store;
  public UILabel m_time_store;
  public UIButton m_storeButton;
  public UITexture m_myCityIcon_takeback;
  public UILabel m_myFoodCount_takeback;
  public UILabel m_myWoodCount_takeback;
  public UILabel m_myOreCount_takeback;
  public UILabel m_mySliverCount_takeback;
  public UITexture m_warehouseIcon_takeback;
  public UILabel m_warehouseResource_takeback;
  public UILabel m_warehouseFootCount_takeback;
  public UILabel m_warehouseWoodCount_takeback;
  public UILabel m_warehouseOreCount_takeback;
  public UILabel m_warehouseSliverCount_takeback;
  public UILabel m_time_takeback;
  public UIButton m_takebackButton;
  protected long m_food;
  protected long m_wood;
  protected long m_ore;
  protected long m_sliver;
  protected bool m_ignoreValueChangesFromUI;
  protected AllianceWarehouseDlg.Parameter m_Parameter;
  protected CityData m_cityData;
  protected long _AlreadyStoredToday;
  protected long _AlreadyStoredTotal;
  protected bool m_showWarning;

  protected long AlreadyStoredToday
  {
    get
    {
      return this._AlreadyStoredToday;
    }
  }

  protected long AlreadyStoredTotal
  {
    get
    {
      return this._AlreadyStoredTotal;
    }
  }

  protected long CurrentTotalWeight
  {
    get
    {
      return this.m_food * this.WeightFood + this.m_wood * this.WeightWood + this.m_ore * this.WeightOre + this.m_sliver * this.WeightSliver;
    }
  }

  protected long MaxDailyStoredResourceWeight
  {
    get
    {
      return (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData(ConfigManager.inst.DB_DailyActivies.GetCurrentRewardFactor().AllianceStorageDaily, "calc_alliance_storage_daily_limit");
    }
  }

  protected long MaxTotalStoredResourceWeight
  {
    get
    {
      return (long) ConfigManager.inst.DB_BenefitCalc.GetFinalData(ConfigManager.inst.DB_DailyActivies.GetCurrentRewardFactor().AllianceStorageTotal, "calc_alliance_storage_total_limit");
    }
  }

  protected long WeightFood
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("food_load").ValueInt;
    }
  }

  protected long WeightWood
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("wood_load").ValueInt;
    }
  }

  protected long WeightOre
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("ore_load").ValueInt;
    }
  }

  protected long WeightSliver
  {
    get
    {
      return (long) ConfigManager.inst.DB_GameConfig.GetData("silver_load").ValueInt;
    }
  }

  protected long MaxDailyStoredFood
  {
    get
    {
      return this.MaxValue(0L, this.MinValue((this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday) / this.WeightFood, this.FoodInCity));
    }
  }

  protected long MaxDailyStoredWood
  {
    get
    {
      return this.MaxValue(0L, this.MinValue((this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday) / this.WeightWood, this.WoodInCity));
    }
  }

  protected long MaxDailyStoredOre
  {
    get
    {
      return this.MaxValue(0L, this.MinValue((this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday) / this.WeightOre, this.OreInCity));
    }
  }

  protected long MaxDailyStoredSliver
  {
    get
    {
      return this.MaxValue(0L, this.MinValue((this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday) / this.WeightSliver, this.SliverInCity));
    }
  }

  protected long FoodInCity
  {
    get
    {
      return (long) this.m_cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.FOOD);
    }
  }

  protected long WoodInCity
  {
    get
    {
      return (long) this.m_cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.WOOD);
    }
  }

  protected long OreInCity
  {
    get
    {
      return (long) this.m_cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.ORE);
    }
  }

  protected long SliverInCity
  {
    get
    {
      return (long) this.m_cityData.resources.GetCurrentResource(CityResourceInfo.ResourceType.SILVER);
    }
  }

  protected AllianceWareHouseData.ResourceData ResourceInWarehouse
  {
    get
    {
      AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.buildingConfigId);
      if (buildingConfigId != null)
        return buildingConfigId.getResourceDataByUserId(PlayerData.inst.uid);
      return new AllianceWareHouseData.ResourceData();
    }
  }

  protected Coordinate CityCoordinate
  {
    get
    {
      return PlayerData.inst.CityData.Location;
    }
  }

  protected Coordinate WareHouseCoordinate
  {
    get
    {
      AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.buildingConfigId);
      if (buildingConfigId != null)
        return new Coordinate(PlayerData.inst.userData.world_id, buildingConfigId.MapX, buildingConfigId.MapY);
      return new Coordinate(PlayerData.inst.userData.world_id, 0, 0);
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.m_Parameter = orgParam as AllianceWarehouseDlg.Parameter;
    this.m_cityData = DBManager.inst.DB_City.Get((long) PlayerData.inst.cityId);
    this.m_foodSlider.value = 0.0f;
    this.m_woodSlider.value = 0.0f;
    this.m_oreSlider.value = 0.0f;
    this.m_sliverSlider.value = 0.0f;
    this.InitUI();
    this.UpdateUI();
    if (this.m_Parameter.mode != AllianceWarehouseDlg.Mode.Store)
      return;
    this.RequestStoreLimitFromServer();
  }

  protected void RequestStoreLimitFromServer()
  {
    AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.buildingConfigId);
    if (buildingConfigId == null)
      return;
    Hashtable postData = new Hashtable();
    postData[(object) "k"] = (object) this.WareHouseCoordinate.K;
    postData[(object) "building_id"] = (object) buildingConfigId.WarehouseId;
    MessageHub.inst.GetPortByAction("map:getUserAllianceWarehouseStoreLimitInfo").SendRequest(postData, new System.Action<bool, object>(this.OnReceivedStoreLimitData), true);
  }

  protected void OnReceivedStoreLimitData(bool result, object data)
  {
    if (!result)
      return;
    Hashtable inData = data as Hashtable;
    if (inData != null)
    {
      DatabaseTools.UpdateData(inData, "daily", ref this._AlreadyStoredToday);
      DatabaseTools.UpdateData(inData, "total", ref this._AlreadyStoredTotal);
    }
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
  }

  private GameObject CreateWorldObjectUnderUI(string prefabPath, UITexture parentTexture)
  {
    GameObject original = AssetManager.Instance.HandyLoad(prefabPath, (System.Type) null) as GameObject;
    GameObject go = (GameObject) null;
    if ((bool) ((UnityEngine.Object) original))
    {
      go = UnityEngine.Object.Instantiate<GameObject>(original);
      SpriteRenderer[] componentsInChildren = go.GetComponentsInChildren<SpriteRenderer>();
      Vector3 vector3_1 = float.MaxValue * Vector3.one;
      Vector3 vector3_2 = float.MinValue * Vector3.one;
      foreach (SpriteRenderer spriteRenderer in componentsInChildren)
      {
        spriteRenderer.sortingOrder = 1;
        vector3_1 = Vector3.Min(spriteRenderer.bounds.min, vector3_1);
        vector3_2 = Vector3.Max(spriteRenderer.bounds.max, vector3_2);
      }
      Bounds bounds = new Bounds();
      bounds.SetMinMax(vector3_1, vector3_2);
      NGUITools.SetLayer(go, LayerMask.NameToLayer("NGUI"));
      go.transform.SetParent(parentTexture.transform);
      SpriteRenderer spriteRenderer1 = componentsInChildren[0];
      Vector3 center = bounds.center;
      float x = bounds.size.x;
      float y = bounds.size.y;
      float num = (float) parentTexture.width / x;
      go.transform.localScale = num * Vector3.one;
      go.transform.localPosition = -1f * center;
      this.m_myCityIcon_store.enabled = false;
    }
    return go;
  }

  private void InitUI()
  {
    int index = Mathf.Min(Mathf.Max(0, PlayerData.inst.playerCityData.level - 1), MapUtils.CityPrefabNames.Length - 1);
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
    {
      this.CreateWorldObjectUnderUI("Prefab/Tiles/" + MapUtils.CityPrefabNames[index], this.m_myCityIcon_store);
      this.m_myCityIcon_store.enabled = false;
      this.CreateWorldObjectUnderUI("Prefab/Tiles/tiles_alliance_storehouse", this.m_warehouseIcon_store).GetComponent<AllianceStoreHouseController>().Reset();
      this.m_warehouseIcon_store.enabled = false;
    }
    if (this.m_Parameter.mode != AllianceWarehouseDlg.Mode.Takeback)
      return;
    this.CreateWorldObjectUnderUI("Prefab/Tiles/" + MapUtils.CityPrefabNames[index], this.m_myCityIcon_takeback);
    this.m_myCityIcon_takeback.enabled = false;
    this.CreateWorldObjectUnderUI("Prefab/Tiles/tiles_alliance_storehouse", this.m_warehouseIcon_takeback).GetComponent<AllianceStoreHouseController>().Reset();
    this.m_warehouseIcon_takeback.enabled = false;
  }

  private void RefreshForCityResourceChanged(int time)
  {
    this.m_ignoreValueChangesFromUI = true;
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
      this.AjustMaxValueForStore(1);
    else
      this.AjustMaxValueForTakeback(1);
    this.SetResourceValuesToControl(1);
    this.SetResourceValuesToControl(2);
    this.SetResourceValuesToControl(3);
    this.SetResourceValuesToControl(4);
    this.UpdateUI();
    this.m_ignoreValueChangesFromUI = false;
  }

  private void UpdateUI()
  {
    this.m_title.text = this.m_Parameter.mode != AllianceWarehouseDlg.Mode.Store ? ScriptLocalization.Get("alliance_storehouse_retrieve_title", true) : ScriptLocalization.Get("alliance_storehouse_store_title", true);
    this.m_rootStore.SetActive(this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store);
    this.m_rootTakeback.SetActive(this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Takeback);
    this.RefreshCommonUI();
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
      this.RefreshStoreUI();
    if (this.m_Parameter.mode != AllianceWarehouseDlg.Mode.Takeback)
      return;
    this.RefreshTakebackUI();
  }

  private void RefreshCommonUI()
  {
    this.m_foodCount.value = Utils.FormatThousands(this.m_food.ToString());
    this.m_woodCount.value = Utils.FormatThousands(this.m_wood.ToString());
    this.m_oreCount.value = Utils.FormatThousands(this.m_ore.ToString());
    this.m_sliverCount.value = Utils.FormatThousands(this.m_sliver.ToString());
  }

  private string MyWarehouseName
  {
    get
    {
      AllianceWareHouseData buildingConfigId = DBManager.inst.DB_AllianceWarehouse.GetMyWarehouseDataByBuildingConfigId(this.m_Parameter.buildingConfigId);
      if (buildingConfigId != null && !string.IsNullOrEmpty(buildingConfigId.Name))
        return buildingConfigId.Name;
      return ScriptLocalization.Get("alliance_storehouse_name", true);
    }
  }

  private void RefreshStoreUI()
  {
    this.m_myResource_store.text = ScriptLocalization.Get("alliance_storehouse_my_resources_subtitle", true);
    this.m_warehouseResource_store.text = this.MyWarehouseName;
    this.m_myFoodCount_store.text = Utils.FormatShortThousandsLong(this.FoodInCity);
    this.m_myWoodCount_store.text = Utils.FormatShortThousandsLong(this.WoodInCity);
    this.m_myOreCount_store.text = Utils.FormatShortThousandsLong(this.OreInCity);
    this.m_mySliverCount_store.text = Utils.FormatShortThousandsLong(this.SliverInCity);
    this.m_warehouseFootCount_store.text = this.m_food <= 0L ? this.ResourceInWarehouse.food.ToString() : string.Format("{0}+{1}", (object) this.ResourceInWarehouse.food, (object) this.m_food);
    this.m_warehouseWoodCount_store.text = this.m_wood <= 0L ? this.ResourceInWarehouse.wood.ToString() : string.Format("{0}+{1}", (object) this.ResourceInWarehouse.wood, (object) this.m_wood);
    this.m_warehouseOreCount_store.text = this.m_ore <= 0L ? this.ResourceInWarehouse.ore.ToString() : string.Format("{0}+{1}", (object) this.ResourceInWarehouse.ore, (object) this.m_ore);
    this.m_warehouseSliverCount_store.text = this.m_sliver <= 0L ? this.ResourceInWarehouse.sliver.ToString() : string.Format("{0}+{1}", (object) this.ResourceInWarehouse.sliver, (object) this.m_sliver);
    this.m_warehouseDailyLimit_store.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousandsLong(this.CurrentTotalWeight + this.AlreadyStoredToday), (object) Utils.FormatShortThousandsLong(this.MaxDailyStoredResourceWeight));
    this.m_warehouseMaxCapicity_store.text = string.Format("{0}/{1}", (object) Utils.FormatShortThousandsLong(this.CurrentTotalWeight + this.AlreadyStoredTotal), (object) Utils.FormatShortThousandsLong(this.MaxTotalStoredResourceWeight));
    this.m_time_store.text = Utils.FormatTime(MarchData.CalcTradeTime(this.CityCoordinate, this.WareHouseCoordinate), false, false, true);
    this.m_storeButton.isEnabled = this.CurrentTotalWeight > 0L;
  }

  private void RefreshTakebackUI()
  {
    this.m_warehouseResource_takeback.text = this.MyWarehouseName;
    this.m_myFoodCount_takeback.text = this.m_food <= 0L ? Utils.FormatShortThousandsLong(this.FoodInCity) : string.Format("{0}+{1}", (object) Utils.FormatShortThousandsLong(this.FoodInCity), (object) Utils.FormatShortThousandsLong(this.m_food));
    this.m_myWoodCount_takeback.text = this.m_wood <= 0L ? Utils.FormatShortThousandsLong(this.WoodInCity) : string.Format("{0}+{1}", (object) Utils.FormatShortThousandsLong(this.WoodInCity), (object) Utils.FormatShortThousandsLong(this.m_wood));
    this.m_myOreCount_takeback.text = this.m_ore <= 0L ? Utils.FormatShortThousandsLong(this.OreInCity) : string.Format("{0}+{1}", (object) Utils.FormatShortThousandsLong(this.OreInCity), (object) Utils.FormatShortThousandsLong(this.m_ore));
    this.m_mySliverCount_takeback.text = this.m_sliver <= 0L ? Utils.FormatShortThousandsLong(this.SliverInCity) : string.Format("{0}+{1}", (object) Utils.FormatShortThousandsLong(this.SliverInCity), (object) Utils.FormatShortThousandsLong(this.m_sliver));
    this.m_warehouseFootCount_takeback.text = Utils.FormatThousands(this.ResourceInWarehouse.food.ToString());
    this.m_warehouseWoodCount_takeback.text = Utils.FormatThousands(this.ResourceInWarehouse.wood.ToString());
    this.m_warehouseOreCount_takeback.text = Utils.FormatThousands(this.ResourceInWarehouse.ore.ToString());
    this.m_warehouseSliverCount_takeback.text = Utils.FormatThousands(this.ResourceInWarehouse.sliver.ToString());
    this.m_time_takeback.text = Utils.FormatTime(MarchData.CalcTradeTime(this.CityCoordinate, this.WareHouseCoordinate), false, false, true);
    this.m_takebackButton.isEnabled = this.CurrentTotalWeight > 0L;
  }

  protected void GetResourceValuesFromSliderControl(int sliderTag)
  {
    long num1;
    long num2;
    long num3;
    long num4;
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
    {
      num1 = this.MaxDailyStoredFood;
      num2 = this.MaxDailyStoredWood;
      num3 = this.MaxDailyStoredOre;
      num4 = this.MaxDailyStoredSliver;
    }
    else
    {
      num1 = this.ResourceInWarehouse.food;
      num2 = this.ResourceInWarehouse.wood;
      num3 = this.ResourceInWarehouse.ore;
      num4 = this.ResourceInWarehouse.sliver;
    }
    switch (sliderTag)
    {
      case 1:
        this.m_food = (long) ((double) this.m_foodSlider.value * (double) num1);
        break;
      case 2:
        this.m_wood = (long) ((double) this.m_woodSlider.value * (double) num2);
        break;
      case 3:
        this.m_ore = (long) ((double) this.m_oreSlider.value * (double) num3);
        break;
      case 4:
        this.m_sliver = (long) ((double) this.m_sliverSlider.value * (double) num4);
        break;
    }
  }

  protected long GetLongValueFromEdit(UIInput input)
  {
    long result = 0;
    long.TryParse(input.value, out result);
    return result;
  }

  protected void GetResourceValuesFromEditControl(int sliderTag)
  {
    long num1 = 0;
    long num2 = 0;
    long num3 = 0;
    long num4 = 0;
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
    {
      num1 = this.MaxDailyStoredFood;
      num2 = this.MaxDailyStoredWood;
      num3 = this.MaxDailyStoredOre;
      num4 = this.MaxDailyStoredSliver;
    }
    else
    {
      num1 = this.ResourceInWarehouse.food;
      num2 = this.ResourceInWarehouse.wood;
      num3 = this.ResourceInWarehouse.ore;
      num4 = this.ResourceInWarehouse.sliver;
    }
    switch (sliderTag)
    {
      case 1:
        this.m_food = this.GetLongValueFromEdit(this.m_foodCount);
        break;
      case 2:
        this.m_wood = this.GetLongValueFromEdit(this.m_woodCount);
        break;
      case 3:
        this.m_ore = this.GetLongValueFromEdit(this.m_oreCount);
        break;
      case 4:
        this.m_sliver = this.GetLongValueFromEdit(this.m_sliverCount);
        break;
    }
  }

  protected void SetResourceValuesToControl(int sliderTag)
  {
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
    {
      switch (sliderTag)
      {
        case 1:
          this.m_foodSlider.value = this.m_food > 0L ? (float) this.m_food / (float) this.MaxDailyStoredFood : 0.0f;
          break;
        case 2:
          this.m_woodSlider.value = this.m_wood > 0L ? (float) this.m_wood / (float) this.MaxDailyStoredWood : 0.0f;
          break;
        case 3:
          this.m_oreSlider.value = this.m_ore > 0L ? (float) this.m_ore / (float) this.MaxDailyStoredOre : 0.0f;
          break;
        case 4:
          this.m_sliverSlider.value = this.m_sliver > 0L ? (float) this.m_sliver / (float) this.MaxDailyStoredSliver : 0.0f;
          break;
      }
    }
    else
    {
      switch (sliderTag)
      {
        case 1:
          this.m_foodSlider.value = this.m_food > 0L ? (float) this.m_food / (float) this.ResourceInWarehouse.food : 0.0f;
          break;
        case 2:
          this.m_woodSlider.value = this.m_wood > 0L ? (float) this.m_wood / (float) this.ResourceInWarehouse.wood : 0.0f;
          break;
        case 3:
          this.m_oreSlider.value = this.m_ore > 0L ? (float) this.m_ore / (float) this.ResourceInWarehouse.ore : 0.0f;
          break;
        case 4:
          this.m_sliverSlider.value = this.m_sliver > 0L ? (float) this.m_sliver / (float) this.ResourceInWarehouse.sliver : 0.0f;
          break;
      }
    }
  }

  protected long MaxValue(long a, long b)
  {
    if (a > b)
      return a;
    return b;
  }

  protected long MinValue(long a, long b)
  {
    if (a < b)
      return a;
    return b;
  }

  protected void AjustMaxValueForStore(int sliderTag)
  {
    long num1 = this.m_food * this.WeightFood + this.m_wood * this.WeightWood + this.m_ore * this.WeightOre + this.m_sliver * this.WeightSliver;
    long num2 = this.MaxValue(0L, this.MinValue(this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday, this.MaxTotalStoredResourceWeight - this.AlreadyStoredTotal));
    if (num1 > num2)
    {
      long num3 = num1 - num2;
      switch (sliderTag)
      {
        case 1:
          this.m_food -= (long) Mathf.CeilToInt((float) num3 / (float) this.WeightFood);
          break;
        case 2:
          this.m_wood -= (long) Mathf.CeilToInt((float) num3 / (float) this.WeightWood);
          break;
        case 3:
          this.m_ore -= (long) Mathf.CeilToInt((float) num3 / (float) this.WeightOre);
          break;
        case 4:
          this.m_sliver -= (long) Mathf.CeilToInt((float) num3 / (float) this.WeightSliver);
          break;
        default:
          D.error((object) "invalid sliderTag: {0}", (object) sliderTag);
          break;
      }
    }
    this.m_food = this.MinValue(this.m_food, this.FoodInCity);
    this.m_wood = this.MinValue(this.m_wood, this.WoodInCity);
    this.m_ore = this.MinValue(this.m_ore, this.OreInCity);
    this.m_sliver = this.MinValue(this.m_sliver, this.SliverInCity);
  }

  protected void AjustMaxValueForTakeback(int sliderTag)
  {
    this.m_food = this.MinValue(this.m_food, this.ResourceInWarehouse.food);
    this.m_wood = this.MinValue(this.m_wood, this.ResourceInWarehouse.wood);
    this.m_ore = this.MinValue(this.m_ore, this.ResourceInWarehouse.ore);
    this.m_sliver = this.MinValue(this.m_sliver, this.ResourceInWarehouse.sliver);
  }

  public void OnSliderValueChanged(int sliderTag)
  {
    if (this.m_ignoreValueChangesFromUI)
      return;
    this.m_ignoreValueChangesFromUI = true;
    this.GetResourceValuesFromSliderControl(sliderTag);
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
      this.AjustMaxValueForStore(sliderTag);
    else
      this.AjustMaxValueForTakeback(sliderTag);
    this.SetResourceValuesToControl(sliderTag);
    this.UpdateUI();
    this.CheckToShowWarning();
    this.m_ignoreValueChangesFromUI = false;
  }

  public void OnFoodSliderValueChanged()
  {
    this.OnSliderValueChanged(1);
  }

  public void OnWoodSliderValueChanged()
  {
    this.OnSliderValueChanged(2);
  }

  public void OnOreSliderValueChanged()
  {
    this.OnSliderValueChanged(3);
  }

  public void OnSliverSliderValueChanged()
  {
    this.OnSliderValueChanged(4);
  }

  protected void OnEditValueChanged(int editTag)
  {
    if (this.m_ignoreValueChangesFromUI)
      return;
    this.m_ignoreValueChangesFromUI = true;
    this.GetResourceValuesFromEditControl(editTag);
    if (this.m_Parameter.mode == AllianceWarehouseDlg.Mode.Store)
      this.AjustMaxValueForStore(editTag);
    else
      this.AjustMaxValueForTakeback(editTag);
    this.SetResourceValuesToControl(editTag);
    this.UpdateUI();
    this.CheckToShowWarning();
    this.m_ignoreValueChangesFromUI = false;
  }

  public void OnFoodEditValueChanged()
  {
    this.OnEditValueChanged(1);
  }

  public void OnWoodEditValueChanged()
  {
    this.OnEditValueChanged(2);
  }

  public void OnOreEditValueChanged()
  {
    this.OnEditValueChanged(3);
  }

  public void OnSliverEditValueChanged()
  {
    this.OnEditValueChanged(4);
  }

  public void OnTakebackButtonClicked()
  {
    Hashtable resources = new Hashtable();
    resources[(object) "food"] = (object) this.m_food;
    resources[(object) "wood"] = (object) this.m_wood;
    resources[(object) "ore"] = (object) this.m_ore;
    resources[(object) "silver"] = (object) this.m_sliver;
    GameEngine.Instance.marchSystem.StartFetchWareHouse(this.WareHouseCoordinate, resources, new System.Action<bool, object>(this.OnTakebackResult));
  }

  protected void OnTakebackResult(bool result, object data)
  {
    this.OnCloseButtonPress();
  }

  public void OnStoreButtonClicked()
  {
    Hashtable resources = new Hashtable();
    resources[(object) "food"] = (object) this.m_food;
    resources[(object) "wood"] = (object) this.m_wood;
    resources[(object) "ore"] = (object) this.m_ore;
    resources[(object) "silver"] = (object) this.m_sliver;
    GameEngine.Instance.marchSystem.StartTradeWareHouse(this.WareHouseCoordinate, resources, new System.Action<bool, object>(this.OnStoreResult));
  }

  protected void OnStoreResult(bool result, object data)
  {
    this.OnCloseButtonPress();
  }

  public void OnPanelClicked()
  {
    this.CheckToShowWarning();
  }

  protected void CheckToShowWarning()
  {
    if (this.m_showWarning)
      return;
    if (this.CurrentTotalWeight >= this.MaxDailyStoredResourceWeight - this.AlreadyStoredToday)
    {
      TweenScale component = this.m_warehouseDailyLimit_store.GetComponent<TweenScale>();
      if ((bool) ((UnityEngine.Object) component))
      {
        this.m_showWarning = true;
        component.enabled = true;
        component.ResetToBeginning();
        component.Play();
        this.m_warehouseDailyLimit_store.color = this.m_warningTextColor;
        this.StartCoroutine(this.StopTweenScale(component.duration * 4f, component, (System.Action) (() =>
        {
          this.m_warehouseDailyLimit_store.color = this.m_normalTextColor;
          this.m_showWarning = false;
        })));
      }
    }
    if (this.CurrentTotalWeight < this.MaxTotalStoredResourceWeight - this.AlreadyStoredTotal)
      return;
    TweenScale component1 = this.m_warehouseMaxCapicity_store.GetComponent<TweenScale>();
    if (!(bool) ((UnityEngine.Object) component1))
      return;
    this.m_showWarning = true;
    component1.enabled = true;
    component1.ResetToBeginning();
    component1.Play();
    this.m_warehouseMaxCapicity_store.color = this.m_warningTextColor;
    this.StartCoroutine(this.StopTweenScale(component1.duration * 4f, component1, (System.Action) (() =>
    {
      this.m_warehouseMaxCapicity_store.color = this.m_normalTextColor;
      this.m_showWarning = false;
    })));
  }

  [DebuggerHidden]
  protected IEnumerator StopTweenScale(float duation, TweenScale tweenScale, System.Action callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AllianceWarehouseDlg.\u003CStopTweenScale\u003Ec__Iterator28()
    {
      duation = duation,
      tweenScale = tweenScale,
      callback = callback,
      \u003C\u0024\u003Eduation = duation,
      \u003C\u0024\u003EtweenScale = tweenScale,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.RefreshForCityResourceChanged);
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.RefreshForCityResourceChanged);
  }

  public enum Mode
  {
    Store = 1,
    Takeback = 2,
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public int buildingConfigId;
    public AllianceWarehouseDlg.Mode mode;
  }
}
