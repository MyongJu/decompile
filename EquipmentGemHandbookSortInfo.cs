﻿// Decompiled with JetBrains decompiler
// Type: EquipmentGemHandbookSortInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class EquipmentGemHandbookSortInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "slot_type")]
  public string slotType;
  [Config(Name = "priority")]
  public int priority;
  [Config(Name = "name")]
  public string name;

  public string LocName
  {
    get
    {
      return Utils.XLAT(this.name);
    }
  }
}
