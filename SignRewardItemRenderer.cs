﻿// Decompiled with JetBrains decompiler
// Type: SignRewardItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SignRewardItemRenderer : MonoBehaviour
{
  public UITexture m_ItemIcon;
  public UILabel m_ItemName;
  public UILabel m_ItemCount;
  public UITexture m_Background;
  private ItemStaticInfo m_ItemStaticInfo;
  private DropMainData m_DropMainData;
  private int m_ItemId;

  public void SetData(DropMainData dropMainData)
  {
    this.m_DropMainData = dropMainData;
    this.m_ItemStaticInfo = ConfigManager.inst.DB_Items.GetItem(dropMainData.ItemId);
    this.m_ItemId = dropMainData.ItemId;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.m_ItemIcon, this.m_ItemStaticInfo.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    Utils.SetItemName(this.m_ItemName, this.m_ItemStaticInfo.internalId);
    Utils.SetItemBackground(this.m_Background, this.m_ItemStaticInfo.internalId);
    this.m_ItemCount.text = "x " + dropMainData.MinValue.ToString();
  }

  public ItemStaticInfo GetItemStaticInfo()
  {
    return this.m_ItemStaticInfo;
  }

  public DropMainData GetDropMainData()
  {
    return this.m_DropMainData;
  }

  public void OnItemPress()
  {
    Utils.DelayShowTip(this.m_ItemId, this.m_Background.transform, 0L, 0L, 0);
  }

  public void OnItemRelese()
  {
    Utils.StopShowItemTip();
  }

  public void OnItemClick()
  {
    Utils.ShowItemTip(this.m_ItemId, this.m_Background.transform, 0L, 0L, 0);
  }
}
