﻿// Decompiled with JetBrains decompiler
// Type: EquipmentInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentInfo : MonoBehaviour
{
  private List<EquipmentSuitItem> allEquipmentSuitItem = new List<EquipmentSuitItem>();
  private List<EquipmentGemItem> _allEquipmentGemItem = new List<EquipmentGemItem>();
  public const string SUIT_ITEM_PATH = "Prefab/UI/Common/EquipmentSuitItem";
  public const string SUIT_GEM_PATH = "Prefab/UI/Common/EquipmentGemItem";
  public EquipmentComponent equipComponent;
  public EquipmentBenefits benefitContent;
  public UITable table;
  public Transform equipmentSuitItemContainer;
  public Transform equipmentGemItemContainer;
  private int equipmentID;
  private long itemID;
  private BagType bagType;

  public void Init(BagType bagType, int equipmentID, long itemID)
  {
    bool equipmentChanged = this.itemID != itemID;
    this.bagType = bagType;
    this.equipmentID = equipmentID;
    this.itemID = itemID;
    this.UpdateEquipmentGem(equipmentID, itemID);
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(bagType).GetMyItemByItemID(itemID);
    this.UpdateEquipmentSuitBenefit(bagType, equipmentID, myItemByItemId);
    this.DrawEquipment();
    this.DrawBenefit();
    Utils.ExecuteAtTheEndOfFrame((System.Action) (() =>
    {
      this.table.Reposition();
      if (!equipmentChanged)
        return;
      this.table.GetComponentInParent<UIScrollView>().ResetPosition();
    }));
  }

  public void Dispose()
  {
    this.equipComponent.Dispose();
    this.benefitContent.Dispose();
  }

  private void DrawEquipment()
  {
    InventoryItemData myItemByItemId = DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.itemID);
    this.equipComponent.FeedData((IComponentData) new EquipmentComponentData(this.equipmentID, myItemByItemId.enhanced, this.bagType, myItemByItemId.itemId));
  }

  private void DrawBenefit()
  {
    this.benefitContent.Init(ConfigManager.inst.GetEquipmentProperty(this.bagType).GetDataByEquipIDAndEnhanceLevel(this.equipmentID, DBManager.inst.GetInventory(this.bagType).GetMyItemByItemID(this.itemID).enhanced).benefits);
  }

  private void ClearEquipmentSuitItem()
  {
    using (List<EquipmentSuitItem>.Enumerator enumerator = this.allEquipmentSuitItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentSuitItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this.allEquipmentSuitItem.Clear();
  }

  private void UpdateEquipmentSuitBenefit(BagType bagType, int equipmentId, InventoryItemData inventoryItemData)
  {
    this.ClearEquipmentSuitItem();
    ConfigEquipmentMainInfo data = ConfigManager.inst.GetEquipmentMain(bagType).GetData(equipmentId);
    if (data == null || data.suitGroup == 0)
      return;
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentSuitItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentSuitItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentSuitItem component = gameObject.GetComponent<EquipmentSuitItem>();
    component.SetData(PlayerData.inst.uid, bagType, data.suitGroup, inventoryItemData);
    this.allEquipmentSuitItem.Add(component);
  }

  private void ClearEquipmentGemItem()
  {
    using (List<EquipmentGemItem>.Enumerator enumerator = this._allEquipmentGemItem.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentGemItem current = enumerator.Current;
        current.transform.parent = (Transform) null;
        UnityEngine.Object.Destroy((UnityEngine.Object) current.gameObject);
      }
    }
    this._allEquipmentGemItem.Clear();
  }

  private void UpdateEquipmentGem(int equipmentInternalId, long itemId)
  {
    this.ClearEquipmentGemItem();
    GameObject gameObject = UnityEngine.Object.Instantiate(AssetManager.Instance.Load("Prefab/UI/Common/EquipmentGemItem", (System.Type) null)) as GameObject;
    AssetManager.Instance.UnLoadAsset("Prefab/UI/Common/EquipmentGemItem", (System.Type) null, (System.Action<UnityEngine.Object, bool>) null);
    gameObject.transform.SetParent(this.equipmentGemItemContainer, true);
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.SetActive(true);
    EquipmentGemItem component = gameObject.GetComponent<EquipmentGemItem>();
    InventoryItemData byItemId = DBManager.inst.GetInventory(this.bagType).GetByItemID(PlayerData.inst.uid, itemId);
    if (byItemId != null)
      component.SetData(byItemId, true, (UIScrollView) null);
    else
      component.SetData(this.bagType, equipmentInternalId, (UIScrollView) null);
    this._allEquipmentGemItem.Add(component);
  }
}
