﻿// Decompiled with JetBrains decompiler
// Type: RoomProcessorArena
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UI;

public class RoomProcessorArena : IRoomEventProcessor
{
  public bool IsSkipAble()
  {
    return false;
  }

  public void ProcessRoomEvent(Room room)
  {
    BattleManager.Instance.SetupPVP(room.AllPlayer, room.AllPlayerSkill, true);
    UIManager.inst.OpenPopup("DragonKnight/DragonKnightPVPSplashPopup", (Popup.PopupParameter) new DragonKnightPVPSplashPopup.Parameter()
    {
      target = DBManager.inst.DB_User.Get(room.AllPlayer[0]),
      targetDK = DBManager.inst.DB_DragonKnight.Get(room.AllPlayer[0]),
      targetArena = DBManager.inst.DB_DKArenaDB.GetDataById(room.AllPlayer[0])
    });
    DragonKnightSystem.Instance.Controller.BattleHud.HideBagAndItems();
    DragonKnightSystem.Instance.Controller.BattleHud.ShowSpeedAndQuickBattle();
  }
}
