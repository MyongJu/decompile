﻿// Decompiled with JetBrains decompiler
// Type: TroopBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;

public class TroopBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UILabel contentLabel;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    List<Unit_StatisticsInfo> allUnitsCanTrain = BarracksManager.Instance.GetAllUnitsCanTrain(this.buildingInfo.Type);
    Unit_StatisticsInfo unitStatisticsInfo = (Unit_StatisticsInfo) null;
    for (int index = 0; index < allUnitsCanTrain.Count; ++index)
    {
      if (allUnitsCanTrain[index].Requirement_Value_1 > (double) this.buildingInfo.Building_Lvl)
      {
        unitStatisticsInfo = allUnitsCanTrain[index];
        break;
      }
    }
    if (unitStatisticsInfo != null)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["2"] = Utils.XLAT(unitStatisticsInfo.Troop_Name_LOC_ID);
      para["1"] = unitStatisticsInfo.Requirement_Value_1.ToString();
      this.contentLabel.text = ScriptLocalization.GetWithPara("barracks_building_info_upgrade_bonuses_description", para, true);
    }
    else
      this.contentLabel.text = Utils.XLAT("barracks_building_info_upgrade_bonuses_complete");
  }
}
