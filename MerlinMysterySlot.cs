﻿// Decompiled with JetBrains decompiler
// Type: MerlinMysterySlot
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MerlinMysterySlot : MonoBehaviour
{
  [SerializeField]
  private ItemIconRenderer _itemIcon;
  [SerializeField]
  private UILabel _labelItemName;
  [SerializeField]
  private UILabel _labelItemPrice;
  [SerializeField]
  private GameObject _selectedStatus;
  private MerlinTrialsShopSpecificInfo _shopInfo;
  public System.Action<MerlinTrialsShopSpecificInfo> OnItemPressCallback;

  public void SetData(MerlinTrialsShopSpecificInfo shopInfo)
  {
    this._shopInfo = shopInfo;
    this.ResetSelectStatus();
    this.UpdateUI();
  }

  public void OnBuyPressed()
  {
    if (this._shopInfo == null)
      return;
    IAPStorePackagePayload.Instance.BuySpecialProduct(this._shopInfo.internalId, this._shopInfo.ProductId, this._shopInfo.name, this._shopInfo.IapGroupName, (System.Action) (() => this.UpdateUI()));
  }

  public void OnItemPressed()
  {
    if (this._shopInfo == null)
      return;
    if (this.OnItemPressCallback != null)
      this.OnItemPressCallback(this._shopInfo);
    this.ShowSelectStatus(true);
  }

  public void ResetSelectStatus()
  {
    NGUITools.SetActive(this._selectedStatus, false);
  }

  private void UpdateUI()
  {
    if (this._shopInfo == null)
      return;
    this._itemIcon.SetData(this._shopInfo.ImagePath);
    this._labelItemName.text = this._shopInfo.Name;
    this._labelItemPrice.text = PaymentManager.Instance.GetFormattedPrice(this._shopInfo.ProductId, this._shopInfo.payId);
  }

  public void ShowSelectStatus(bool show)
  {
    MerlinMysterySlot[] componentsInChildren = this.transform.parent.GetComponentsInChildren<MerlinMysterySlot>();
    if (componentsInChildren != null)
    {
      for (int index = 0; index < componentsInChildren.Length; ++index)
        componentsInChildren[index].ResetSelectStatus();
    }
    NGUITools.SetActive(this._selectedStatus, true);
  }
}
