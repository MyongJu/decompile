﻿// Decompiled with JetBrains decompiler
// Type: ConfigMagicTowerShop
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigMagicTowerShop
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<string, MagicTowerShopInfo> datas;
  private Dictionary<int, MagicTowerShopInfo> dicByUniqueId;
  private List<int> _costItems;

  public void BuildDB(object res)
  {
    this.parse.Parse<MagicTowerShopInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
  }

  private int SortDelegate(ItemStaticInfo a, ItemStaticInfo b)
  {
    return a.Priority.CompareTo(b.Priority);
  }

  public MagicTowerShopInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (MagicTowerShopInfo) null;
  }

  public MagicTowerShopInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (MagicTowerShopInfo) null;
  }

  public List<MagicTowerShopInfo> GetWithLayer(int layer)
  {
    List<MagicTowerShopInfo> magicTowerShopInfoList = new List<MagicTowerShopInfo>();
    MagicTowerMainInfo dataWithLevel = ConfigManager.inst.DB_MagicTowerMain.GetDataWithLevel(layer);
    if (dataWithLevel != null)
    {
      int internalId = dataWithLevel.internalId;
      Dictionary<int, MagicTowerShopInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Value.MagicTowerMainId == internalId)
          magicTowerShopInfoList.Add(enumerator.Current.Value);
      }
      magicTowerShopInfoList.Sort(new Comparison<MagicTowerShopInfo>(this.Compare));
    }
    return magicTowerShopInfoList;
  }

  private int Compare(MagicTowerShopInfo a, MagicTowerShopInfo b)
  {
    return Math.Sign(a.order - b.order);
  }

  public void Clear()
  {
    if (this.datas != null)
    {
      this.datas.Clear();
      this.datas = (Dictionary<string, MagicTowerShopInfo>) null;
    }
    if (this.dicByUniqueId == null)
      return;
    this.dicByUniqueId.Clear();
    this.dicByUniqueId = (Dictionary<int, MagicTowerShopInfo>) null;
  }
}
