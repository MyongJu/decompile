﻿// Decompiled with JetBrains decompiler
// Type: BuildConfig
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BuildConfig
{
  public static string DEPLOY_ASSETS_FOLDER = Application.dataPath + "/_BUILD/DeployAssets";
  public static string BUILD_FOLDER_PATH = Application.streamingAssetsPath + "/build";
  public static string BUNDLE_CACHE_FOLDER = Application.streamingAssetsPath + "/build/cache";
  public static string CONFIG_CACHE_FOLDER = Application.streamingAssetsPath + "/build/config";
  public static string ASSETS_CONFIG_PATH = BuildConfig.BUILD_BUNDLE_FOLDER_PATH + "/assetsconfig.json";
  public static string BUNDLE_VERSION_LIST_PATH = BuildConfig.BUNDLE_CACHE_FOLDER + "/VersionList.json";
  public static string MANIFEST_PATH = BuildConfig.BUILD_BUNDLE_FOLDER_PATH + "/bundle.manifest";
  public static string BUNDLE_CAPCITY_PATH = BuildConfig.BUNDLE_CACHE_FOLDER + "/bundlecapcity.json";
  public static string BUNDLE_ORIGIN_CAPCITY_PATH = BuildConfig.BUNDLE_CACHE_FOLDER + "/bundleorigincapcity.json";
  public static string APP_VERSION_FILE_PATH = Application.streamingAssetsPath + "/version.json";
  public static string BUILD_TEMP_FOLDER = Application.streamingAssetsPath + "/Temp";
  public static string BUNDLE_TYPE_G_FOLDER = BuildConfig.BUILD_TEMP_FOLDER + "/G";
  public static string BUNDLE_TYPE_R_FOLDER = BuildConfig.BUILD_TEMP_FOLDER + "/R";
  public static string BUILD_BUNDLE_FOLDER_PATH = BuildConfig.BUILD_TEMP_FOLDER + "/build/bundle";
  public static string ZIPED_BUNDLE_FOLDER = BuildConfig.BUILD_TEMP_FOLDER + "/ZipedBundle";
  public static string CONFIG_FOLDER = BuildConfig.BUILD_TEMP_FOLDER + "/config";
  public const string BUILD_FOLDER = "build";
}
