﻿// Decompiled with JetBrains decompiler
// Type: HeroInformationDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class HeroInformationDlg : MonoBehaviour
{
  [SerializeField]
  private HeroInformationDlg.Panel panel;

  public void Setup(long itemId)
  {
  }

  [Serializable]
  protected class Panel
  {
    public ItemSlot_New itemSlot;
    public UILabel itemName;
    public UILabel itemDetail;
    public UILabel itemLevel;
    public UIGrid benefitsGrid;
    public UIScrollView scrollView;
    public ItemSkillBenefitCompareSlot benefitSlotOrg;
  }
}
