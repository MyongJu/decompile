﻿// Decompiled with JetBrains decompiler
// Type: MerlinTrialsRankDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class MerlinTrialsRankDlg : UI.Dialog
{
  private Dictionary<int, MerlinTrialsRankSlot> _itemDict = new Dictionary<int, MerlinTrialsRankSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private MerlinTrialsRankDlg.RankType _rankType = MerlinTrialsRankDlg.RankType.TOTAL;
  private List<MerlinTrialsPayload.RankData> _rankDataList = new List<MerlinTrialsPayload.RankData>();
  private int _currentPageIndex = 1;
  [SerializeField]
  private UILabel _labelNoRank;
  [SerializeField]
  private UILabel _labelRankTitle;
  [SerializeField]
  private UILabel _labelCompleteNumber;
  [SerializeField]
  private UILabel _labelLostNumber;
  [SerializeField]
  private HelpBtnComponent _helpButtonComponent;
  [SerializeField]
  private UILabel _labelYourRank;
  [SerializeField]
  private UILabel _labelYourRankPrefix;
  [SerializeField]
  private UILabel _labelNoYourRank;
  [SerializeField]
  private UILabel _labelRankPageText;
  [SerializeField]
  private UIScrollView _scrollView;
  [SerializeField]
  private UITable _table;
  [SerializeField]
  private GameObject _itemPrefab;
  private int _groupId;
  private MerlinTrialsPayload.RankData _selfRankData;
  private int _totalPageCount;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    MerlinTrialsRankDlg.Parameter parameter = orgParam as MerlinTrialsRankDlg.Parameter;
    if (parameter != null)
    {
      this._rankType = parameter.rankType;
      this._groupId = parameter.groupId;
    }
    this._itemPool.Initialize(this._itemPrefab, this._table.gameObject);
    this.ShowMainContent();
    MessageHub.inst.GetPortByAction("Merlin:getLeaderBoard").SendRequest(new Hashtable()
    {
      {
        (object) "groupId",
        this._rankType != MerlinTrialsRankDlg.RankType.TOTAL ? (object) this._groupId.ToString() : (object) "total"
      }
    }, (System.Action<bool, object>) ((ret, data) =>
    {
      if (!ret)
        return;
      Hashtable hashtable = data as Hashtable;
      if (hashtable != null)
      {
        if (hashtable.ContainsKey((object) "me"))
        {
          Hashtable data1 = hashtable[(object) "me"] as Hashtable;
          if (data1 != null)
            this._selfRankData = new MerlinTrialsPayload.RankData(data1);
        }
        if (hashtable.ContainsKey((object) "rank"))
        {
          ArrayList arrayList = hashtable[(object) "rank"] as ArrayList;
          if (arrayList != null)
          {
            for (int index = 0; index < arrayList.Count; ++index)
            {
              Hashtable data1 = arrayList[index] as Hashtable;
              if (data1 != null)
                this._rankDataList.Add(new MerlinTrialsPayload.RankData(data1));
            }
          }
        }
      }
      this._totalPageCount = Mathf.CeilToInt((float) this._rankDataList.Count / 10f);
      this.UpdateSelfUI();
      this.UpdateUI();
    }), true);
    this._labelRankPageText.text = "0/0";
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this._selfRankData = (MerlinTrialsPayload.RankData) null;
    this._rankDataList.Clear();
    this.ClearData();
  }

  public void OnPreviousClick()
  {
    if (this._currentPageIndex <= 1)
      return;
    --this._currentPageIndex;
    this.UpdateUI();
  }

  public void OnNextClick()
  {
    if (this._currentPageIndex >= this._totalPageCount)
      return;
    ++this._currentPageIndex;
    this.UpdateUI();
  }

  private void ShowMainContent()
  {
    if (this._rankType == MerlinTrialsRankDlg.RankType.SINGLE)
    {
      MerlinTrialsGroupInfo merlinTrialsGroupInfo = ConfigManager.inst.DB_MerlinTrialsGroup.Get(this._groupId);
      if (merlinTrialsGroupInfo != null)
        this._labelRankTitle.text = ScriptLocalization.GetWithPara("trial_benefit_ranking", new Dictionary<string, string>()
        {
          {
            "0",
            merlinTrialsGroupInfo.LocName
          }
        }, true);
      this._labelCompleteNumber.text = Utils.XLAT("trial_successes_ranking_subtitle");
      this._labelLostNumber.text = Utils.XLAT("trial_highest_losses_ranking_subtitle");
    }
    else
    {
      if (this._rankType != MerlinTrialsRankDlg.RankType.TOTAL)
        return;
      this._labelRankTitle.text = Utils.XLAT("trial_total_ranking");
      this._labelCompleteNumber.text = Utils.XLAT("trial_challenges_ranking_subtitle");
      this._labelLostNumber.text = Utils.XLAT("trial_total_losses_ranking_subtitle");
    }
  }

  private void UpdateSelfUI()
  {
    if (this._selfRankData != null)
      this._labelYourRank.text = this._selfRankData.rank.ToString();
    NGUITools.SetActive(this._labelYourRankPrefix.gameObject, this._selfRankData != null);
    NGUITools.SetActive(this._labelYourRank.gameObject, this._selfRankData != null);
    NGUITools.SetActive(this._labelNoYourRank.gameObject, this._selfRankData == null);
  }

  private void UpdateUI()
  {
    this._labelRankPageText.text = string.Format("{0}/{1}", (object) (this._currentPageIndex <= this._totalPageCount ? this._currentPageIndex : this._totalPageCount), (object) this._totalPageCount);
    this.ClearData();
    int key = 0;
    int num = (this._currentPageIndex - 1) * 10;
    for (; key < 10; ++key)
    {
      if (key + num < this._rankDataList.Count)
      {
        MerlinTrialsRankSlot slot = this.GenerateSlot(this._rankDataList[key + num]);
        this._itemDict.Add(key, slot);
      }
    }
    this.Reposition();
    NGUITools.SetActive(this._labelNoRank.gameObject, this._rankDataList.Count <= 0);
  }

  private void ClearData()
  {
    using (Dictionary<int, MerlinTrialsRankSlot>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this._table.repositionNow = true;
    this._table.Reposition();
    this._scrollView.ResetPosition();
  }

  private MerlinTrialsRankSlot GenerateSlot(MerlinTrialsPayload.RankData rankData)
  {
    GameObject gameObject = this._itemPool.AddChild(this._table.gameObject);
    gameObject.SetActive(true);
    MerlinTrialsRankSlot component = gameObject.GetComponent<MerlinTrialsRankSlot>();
    component.SetData(rankData);
    return component;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public MerlinTrialsRankDlg.RankType rankType;
    public int groupId;
  }

  public enum RankType
  {
    SINGLE,
    TOTAL,
  }
}
