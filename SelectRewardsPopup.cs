﻿// Decompiled with JetBrains decompiler
// Type: SelectRewardsPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UI;

public class SelectRewardsPopup : Popup
{
  private Dictionary<int, List<Reward>> rewardsMap = new Dictionary<int, List<Reward>>();
  private Dictionary<int, Reward> mainRewardsMap = new Dictionary<int, Reward>();
  private Dictionary<int, RewardSelectionRender> gradeMap = new Dictionary<int, RewardSelectionRender>();
  private List<int> maxCountList = new List<int>();
  private List<int> gradeList = new List<int>();
  private List<int> AllSelections = new List<int>();
  private List<int> originalOrder = new List<int>();
  private const string ADD = "tavern_chance_confirm_add_name";
  private const string TOAST_POLL_NOT_FULL = "toast_tavern_wheel_pool_empty";
  private const string HELP_DIS = "help_tavern_chance_description";
  private const string ROULETTE_CLOSED = "tavern_chance_closed_description";
  public UILabel setRewardsPoolLabel;
  public UIButtonBar buttonBar;
  public RewardSelectionRender gradeOne;
  public RewardSelectionRender gradeTwo;
  public RewardSelectionRender gradeThree;
  private RewardSelectionRender currentGrade;
  private RewardSelectionRender nextGrade;
  private System.Action<List<int>> selectionCompleteHandler;
  private int minGradeWithoutFull;
  private List<int> haveSelected;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    SelectRewardsPopup.Parameter parameter = orgParam as SelectRewardsPopup.Parameter;
    if (parameter == null)
      return;
    this.haveSelected = parameter.rewards;
    this.selectionCompleteHandler = parameter.SelectionCompleteHandler;
    this.RecordOriginalOrder();
    this.AddEventHandler();
    this.InitData();
    this.UpdateUI();
    this.SetDefaultTab();
  }

  private void InitData()
  {
    this.GetAllGradesRewards();
    this.InitGradeMap();
  }

  private void UpdateUI()
  {
    this.UpdateTabLabels();
    this.SetDefaultTab();
    this.setRewardsPoolLabel.text = Utils.XLAT("tavern_chance_confirm_add_name");
  }

  private void InitGradeMap()
  {
    this.gradeMap.Add(1, this.gradeOne);
    this.gradeMap.Add(2, this.gradeTwo);
    this.gradeMap.Add(3, this.gradeThree);
    Dictionary<int, RewardSelectionRender>.Enumerator enumerator = this.gradeMap.GetEnumerator();
    while (enumerator.MoveNext())
      enumerator.Current.Value.Grade = enumerator.Current.Key;
  }

  private void UpdateTabLabels()
  {
    for (int index = 0; index < this.gradeList.Count; ++index)
    {
      if (this.gradeMap.ContainsKey(this.gradeList[index]))
      {
        RewardSelectionRender grade = this.gradeMap[this.gradeList[index]];
        if (this.haveSelected != null)
          grade.RefreshTabTitles(this.gradeList[index], this.maxCountList[index], this.maxCountList[index]);
        else
          grade.RefreshTabTitles(this.gradeList[index], 0, this.maxCountList[index]);
      }
    }
  }

  private void SetDefaultTab()
  {
    this.buttonBar.SelectedIndex = 0;
  }

  private void AddEventHandler()
  {
    this.buttonBar.OnSelectedHandler += new System.Action<int>(this.OnTabSelectedHandler);
    this.gradeOne.OnBoothFullHandler += new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
    this.gradeTwo.OnBoothFullHandler += new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
    this.gradeThree.OnBoothFullHandler += new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
  }

  public void GetAllGradesRewards()
  {
    if (RoulettePayload.Instance.GroupId == 0)
      return;
    List<TavernWheelGradeInfo> gradesWithGroupId = ConfigManager.inst.DB_TavernWheelGrade.GetGradesWithGroupId(RoulettePayload.Instance.GroupId);
    for (int key = 0; key < gradesWithGroupId.Count; ++key)
    {
      List<TavernWheelMainInfo> mainWithGradeId = ConfigManager.inst.DB_TavernWheelMain.GetMainWithGradeId(gradesWithGroupId[key].internalId);
      List<Reward> rewardList = new List<Reward>();
      for (int index = 0; index < mainWithGradeId.Count; ++index)
      {
        rewardList.Add(mainWithGradeId[index].rewards);
        this.mainRewardsMap.Add(mainWithGradeId[index].internalId, mainWithGradeId[index].rewards);
      }
      this.rewardsMap.Add(key, rewardList);
      this.maxCountList.Add(gradesWithGroupId[key].num);
      this.gradeList.Add(gradesWithGroupId[key].grade);
    }
  }

  private void OnTabSelectedHandler(int index)
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
    {
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    }
    else
    {
      List<Reward> rewardList = (List<Reward>) null;
      if (this.rewardsMap.ContainsKey(index))
        rewardList = this.rewardsMap[index];
      switch (index)
      {
        case 0:
          this.nextGrade = this.gradeOne;
          break;
        case 1:
          this.nextGrade = this.gradeTwo;
          break;
        case 2:
          this.nextGrade = this.gradeThree;
          break;
      }
      if ((UnityEngine.Object) null == (UnityEngine.Object) this.currentGrade)
        this.currentGrade = this.nextGrade;
      else if ((UnityEngine.Object) this.currentGrade != (UnityEngine.Object) this.nextGrade)
      {
        this.currentGrade.Hide();
        this.currentGrade = this.nextGrade;
      }
      int grade = this.gradeList[index];
      if (this.haveSelected == null)
        this.currentGrade.Show(rewardList, this.mainRewardsMap, this.maxCountList[index], grade);
      else
        this.currentGrade.Show(this.haveSelected, rewardList, this.mainRewardsMap, this.maxCountList[index], grade);
    }
  }

  private void OnBoothFullHandler(RewardSelectionRender render)
  {
    RewardSelectionRender rewardSelectionRender = (RewardSelectionRender) null;
    for (int index = this.gradeList.Count - 1; index >= 0; --index)
    {
      if (render.Grade != this.gradeList[index] && this.gradeMap.ContainsKey(this.gradeList[index]))
      {
        RewardSelectionRender grade = this.gradeMap[this.gradeList[index]];
        if (!grade.isBoothPanelFull())
        {
          if (render.Grade == 1)
          {
            rewardSelectionRender = grade;
          }
          else
          {
            this.buttonBar.SelectedIndex = grade.Index;
            break;
          }
        }
      }
    }
    if (!((UnityEngine.Object) null != (UnityEngine.Object) rewardSelectionRender))
      return;
    this.buttonBar.SelectedIndex = rewardSelectionRender.Index;
  }

  private bool AreAllGradesFull()
  {
    bool flag = true;
    for (int index = 0; index < this.gradeList.Count; ++index)
    {
      if (this.gradeMap.ContainsKey(this.gradeList[index]))
      {
        RewardSelectionRender grade = this.gradeMap[this.gradeList[index]];
        if (!grade.isBoothPanelFull())
        {
          flag = false;
          this.minGradeWithoutFull = grade.Index;
          break;
        }
      }
    }
    return flag;
  }

  private void RecordOriginalOrder()
  {
    if (this.haveSelected == null)
      return;
    for (int index = 0; index < this.haveSelected.Count; ++index)
    {
      TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(this.haveSelected[index]);
      if (tavernWheelMainInfo != null)
        this.originalOrder.Add(ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId).grade);
    }
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    this.RemoveEventHandler();
    this.gradeOne.Release();
    this.gradeTwo.Release();
    this.gradeThree.Release();
  }

  private void RemoveEventHandler()
  {
    this.buttonBar.OnSelectedHandler -= new System.Action<int>(this.OnTabSelectedHandler);
    this.gradeOne.OnBoothFullHandler -= new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
    this.gradeTwo.OnBoothFullHandler -= new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
    this.gradeThree.OnBoothFullHandler -= new System.Action<RewardSelectionRender>(this.OnBoothFullHandler);
  }

  public void SetRewardsPool()
  {
    if (RoulettePayload.Instance.IsRoulettePaused || RoulettePayload.Instance.IsRouletteClose)
      UIManager.inst.toast.Show(Utils.XLAT("tavern_chance_closed_description"), (System.Action) null, 4f, false);
    else if (this.AreAllGradesFull())
    {
      this.MergeAllGradesRewards();
      this.SendSetRewardsRequest(new System.Action<bool, object>(this.OnSetRewardCallback), true);
    }
    else if (!this.currentGrade.isBoothPanelFull())
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_tavern_wheel_pool_empty"), (System.Action) null, 4f, false);
    }
    else
    {
      UIManager.inst.toast.Show(Utils.XLAT("toast_tavern_wheel_pool_empty"), (System.Action) null, 4f, false);
      this.buttonBar.SelectedIndex = this.minGradeWithoutFull;
    }
  }

  private void MergeAllGradesRewards()
  {
    List<int> unordered = new List<int>();
    for (int index = this.gradeList.Count - 1; index >= 0; --index)
    {
      if (this.gradeMap.ContainsKey(this.gradeList[index]))
      {
        RewardSelectionRender grade = this.gradeMap[this.gradeList[index]];
        unordered.AddRange((IEnumerable<int>) grade.SelectedItems);
      }
    }
    if (this.haveSelected != null)
    {
      List<int> newOrderedRewards = this.GetNewOrderedRewards(unordered);
      if (newOrderedRewards.Count > 0)
        this.AllSelections.AddRange((IEnumerable<int>) newOrderedRewards);
      else
        this.AllSelections.AddRange((IEnumerable<int>) unordered);
    }
    else
    {
      int count = unordered.Count;
      for (int index1 = 0; index1 < count; ++index1)
      {
        int index2 = 0;
        if (index1 > 0)
          index2 = UnityEngine.Random.Range(0, unordered.Count);
        this.AllSelections.Add(unordered[index2]);
        unordered.RemoveAt(index2);
      }
    }
  }

  private List<int> GetNewOrderedRewards(List<int> unordered)
  {
    List<int> intList1 = new List<int>();
    List<int> intList2 = new List<int>();
    for (int index1 = 0; index1 < this.originalOrder.Count; ++index1)
    {
      for (int index2 = 0; index2 < unordered.Count; ++index2)
      {
        TavernWheelMainInfo tavernWheelMainInfo = ConfigManager.inst.DB_TavernWheelMain.Get(unordered[index2]);
        if (tavernWheelMainInfo != null)
        {
          TavernWheelGradeInfo tavernWheelGradeInfo = ConfigManager.inst.DB_TavernWheelGrade.Get(tavernWheelMainInfo.gradeId);
          if (!intList1.Contains(index2) && tavernWheelGradeInfo.grade == this.originalOrder[index1])
          {
            intList1.Add(index2);
            intList2.Add(unordered[index2]);
            break;
          }
        }
      }
    }
    return intList2;
  }

  private void OnSetRewardCallback(bool ret, object data)
  {
    if (!ret)
      return;
    if (this.selectionCompleteHandler != null)
      this.selectionCompleteHandler(this.AllSelections);
    this.OnCloseBtnClicked();
  }

  private void SendSetRewardsRequest(System.Action<bool, object> callback, bool blockScreen = true)
  {
    Hashtable postData = new Hashtable();
    ArrayList arrayList = new ArrayList();
    arrayList.AddRange((ICollection) this.AllSelections);
    postData[(object) "items"] = (object) arrayList;
    MessageHub.inst.GetPortByAction("tavernWheel:setRewards").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
    {
      if (ret)
      {
        if (callback == null)
          return;
        callback(ret, data);
      }
      else
        this.AllSelections.Clear();
    }), blockScreen);
  }

  public void OnCloseBtnClicked()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public class Parameter : Popup.PopupParameter
  {
    public List<int> rewards;
    public System.Action<List<int>> SelectionCompleteHandler;
  }
}
