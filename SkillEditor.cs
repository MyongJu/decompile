﻿// Decompiled with JetBrains decompiler
// Type: SkillEditor
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SkillEditor : MonoBehaviour
{
  public string m_FileName;
  public bool m_Play;
  private UnityEngine.Animation m_Animation;
  private GuardEventReceiver m_Receiver;

  private void Update()
  {
    if (!(bool) ((UnityEngine.Object) this.m_Animation) || !(bool) ((UnityEngine.Object) this.m_Receiver))
    {
      this.m_Animation = UnityEngine.Object.FindObjectOfType<UnityEngine.Animation>();
      this.m_Receiver = UnityEngine.Object.FindObjectOfType<GuardEventReceiver>();
    }
    else
    {
      if (!this.m_Play)
        return;
      this.m_Play = false;
      GuardSkill guardSkill = GuardSkillParser.Parse(this.m_Receiver.gameObject, AssetManager.Instance.HandyLoad("Prefab/DragonKnight/Objects/NPCSKills/" + this.m_FileName, (System.Type) null) as TextAsset);
      if (guardSkill == null)
        return;
      for (int index = 0; index < guardSkill.EventDataList.Count; ++index)
        this.m_Receiver.SetEventData(guardSkill.EventDataList[index]);
      this.m_Animation.CrossFade(guardSkill.Take);
    }
  }
}
