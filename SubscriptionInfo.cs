﻿// Decompiled with JetBrains decompiler
// Type: SubscriptionInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SubscriptionInfo
{
  public long internalId;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "pay_id")]
  public string pay_id;
  [Config(Name = "effective_duration")]
  public int days;
  [Config(Name = "icon")]
  public string icon;
  [Config(Name = "icon_is_receivable")]
  public string rewardicon;
  [Config(CustomParse = true, Name = "Rewards", ParseFuncKey = "ParseComposeItems")]
  public Dictionary<long, int> rewards;
  [Config(CustomParse = true, Name = "Benefits", ParseFuncKey = "ParseComposeBenefits")]
  public Dictionary<long, float> benefits;
}
