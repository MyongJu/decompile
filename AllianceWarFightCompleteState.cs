﻿// Decompiled with JetBrains decompiler
// Type: AllianceWarFightCompleteState
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceWarFightCompleteState : AllianceWarBaseState
{
  private List<GroupItemRender> renderList = new List<GroupItemRender>();
  private const string WAR_RANK = "alliance_warfare_match_over_subtitle";
  private const string PERSONAL_AWARD_MAIL = "alliance_warfare_match_over_description";
  private const string FINAL_SCORE = "alliance_warfare_match_ranking_status";
  private const string DISTRIBUTE_AWARD = "alliance_warfare_assign_rewards_button";
  private const string ALLIANCE_REWARD = "id_alliance_rewards";
  public GameObject itemPrefab;
  public UITable table;
  public UILabel groupingResult;
  public UILabel tipsStart;
  public UILabel distributeLabel;
  public UIButton distributeButton;

  public override void Show(bool requestData)
  {
    base.Show(requestData);
    this.RefreshGroupingDatas();
  }

  public override void UpdateUI()
  {
    this.instruction.text = Utils.XLAT("alliance_warfare_event_description");
    int time = 0;
    if (NetServerTime.inst.ServerTimestamp < this.allianceWarData.RegisterStartTime)
      time = this.allianceWarData.RegisterStartTime - NetServerTime.inst.ServerTimestamp;
    else if (NetServerTime.inst.ServerTimestamp > this.allianceWarData.FightEndTime)
      time = this.allianceWarData.FightEndTime + this.allianceWarData.Interval - NetServerTime.inst.ServerTimestamp;
    this.state.text = ScriptLocalization.GetWithPara("alliance_warfare_unopened_status", new Dictionary<string, string>()
    {
      {
        "0",
        Utils.FormatTime(time, true, true, true)
      }
    }, true);
    this.groupingResult.text = Utils.XLAT("alliance_warfare_match_over_subtitle");
    this.tipsStart.text = Utils.XLAT("alliance_warfare_match_over_description");
    if (this.IsAllianceLeader())
      this.distributeLabel.text = Utils.XLAT("alliance_warfare_assign_rewards_button");
    else
      this.distributeLabel.text = Utils.XLAT("id_alliance_rewards");
  }

  private bool IsAllianceLeader()
  {
    return PlayerData.inst.allianceData.creatorId == PlayerData.inst.uid;
  }

  private void RefreshGroupingDatas()
  {
    if (this.allianceWarData.HistoryGroupDataList == null)
      return;
    for (int index = 0; index < this.allianceWarData.HistoryGroupDataList.Count; ++index)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.itemPrefab);
      gameObject.SetActive(true);
      gameObject.transform.parent = this.table.transform;
      gameObject.transform.localScale = new Vector3(0.85f, 0.85f, 1f);
      gameObject.transform.localPosition = Vector3.zero;
      GroupItemRender component = gameObject.GetComponent<GroupItemRender>();
      component.FeedData(this.allianceWarData.HistoryGroupDataList[index], index + 1, true);
      this.renderList.Add(component);
    }
  }

  public override void RefreshDataAfterReqeustCallback()
  {
    this.RefreshGroupingDatas();
  }

  private void ClearData()
  {
    for (int index = 0; index < this.renderList.Count; ++index)
    {
      this.renderList[index].gameObject.transform.parent = (Transform) null;
      this.renderList[index].gameObject.SetActive(false);
      Object.Destroy((Object) this.renderList[index].gameObject);
    }
    this.renderList.Clear();
  }

  public override void Hide()
  {
    base.Hide();
    this.ClearData();
  }

  public void OnDistributeBtnPressed()
  {
    UIManager.inst.OpenPopup("AllianceWar/AllianceRewardDistributionPopup", (Popup.PopupParameter) null);
  }
}
