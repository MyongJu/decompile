﻿// Decompiled with JetBrains decompiler
// Type: LegendItemUI
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class LegendItemUI : MonoBehaviour
{
  public UITexture legendIconTexture;
  public UILabel legendLevel;
  public UILabel legendPower;
  public UIButton legendinfo;
  public GameObject content;
  [NonSerialized]
  public long legendID;
  public System.Action<long> LegendClickHandler;

  private void Start()
  {
  }

  public void LegendIconClick()
  {
    if (this.LegendClickHandler == null || this.legendID <= 0L)
      return;
    this.LegendClickHandler(this.legendID);
  }

  private void Update()
  {
  }

  public void SetLengendInfo(LegendInfo info)
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.legendIconTexture, info.Icon, (System.Action<bool>) null, true, false, string.Empty);
    NGUITools.SetActive(this.content, true);
    this.legendinfo.gameObject.SetActive(true);
  }

  public void Clear()
  {
    BuilderFactory.Instance.Release((UIWidget) this.legendIconTexture);
    NGUITools.SetActive(this.content, false);
    this.legendinfo.gameObject.SetActive(false);
  }
}
