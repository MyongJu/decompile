﻿// Decompiled with JetBrains decompiler
// Type: LevelUpSliderComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class LevelUpSliderComponentData : IComponentData
{
  private int _curLevel;
  private int _newLevel;
  private long _curExp;
  private long _curLevelExpMax;
  private long _newExp;
  private long _newLevelExpMax;
  private bool _isMaxLevel;

  public LevelUpSliderComponentData()
  {
  }

  public LevelUpSliderComponentData(int curLevel, int newLevel, long curExp, long newExp, long curLevelExpMax, long newLevelExpMax, bool isMaxLevel)
  {
    this.CurLevel = curLevel;
    this.NewLevel = newLevel;
    this.CurExp = curExp;
    this.NewExp = newExp;
    this.CurLevelExpMax = curLevelExpMax;
    this.NewLevelExpMax = newLevelExpMax;
    this.IsMaxLevel = isMaxLevel;
  }

  public int CurLevel
  {
    set
    {
      this._curLevel = value;
    }
    get
    {
      return this._curLevel;
    }
  }

  public int NewLevel
  {
    set
    {
      this._newLevel = value;
    }
    get
    {
      return this._newLevel;
    }
  }

  public long CurExp
  {
    set
    {
      this._curExp = value;
    }
    get
    {
      return this._curExp;
    }
  }

  public long CurLevelExpMax
  {
    set
    {
      this._curLevelExpMax = value;
    }
    get
    {
      return this._curLevelExpMax;
    }
  }

  public long NewExp
  {
    set
    {
      this._newExp = value;
    }
    get
    {
      return this._newExp;
    }
  }

  public long NewLevelExpMax
  {
    set
    {
      this._newLevelExpMax = value;
    }
    get
    {
      return this._newLevelExpMax;
    }
  }

  public bool IsMaxLevel
  {
    set
    {
      this._isMaxLevel = value;
    }
    get
    {
      return this._isMaxLevel;
    }
  }
}
