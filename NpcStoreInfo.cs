﻿// Decompiled with JetBrains decompiler
// Type: NpcStoreInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class NpcStoreInfo
{
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_type")]
  public string itemType;
  [Config(Name = "item_id")]
  public int itemId;
  [Config(Name = "item_value")]
  public int itemValue;
  [Config(Name = "cost_type")]
  public string costType;
  [Config(Name = "init_cost_value")]
  public int costInitValue;
  [Config(Name = "current_cost_value")]
  public int costCurrentValue;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "description")]
  public string description;
  [Config(Name = "discount_level")]
  public int descountLevel;

  public string imagePath
  {
    get
    {
      return string.Format("Texture/ItemIcons/{0}", (object) this.image);
    }
  }
}
