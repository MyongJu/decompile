﻿// Decompiled with JetBrains decompiler
// Type: KingdomBossDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomBossDetailDlg : UI.Dialog
{
  private int _maxReward = 3;
  private const string DAMAGE_AWARD_PATH = "Texture/ItemIcons/item_gve_kill_chest_6";
  private const string KILL_AWARD_PATH = "Texture/ItemIcons/item_gve_kill_chest_6";
  private const string AUCTION_AWARD_PATH = "Texture/ItemIcons/icon_iap_gold2";
  public UILabel title;
  public UILabel desc;
  public UITexture monsterIcon;
  public RewardNormalItem[] rewardItems;
  public UILabel spirit;
  public UIButton attackBt;
  public UILabel normalTip;
  public UIProgressBar progressBar;
  public UILabel progressDesc;
  public PVEMonsterPreview previewPrefab;
  private Coordinate _targetLocation;
  private TileData _tileData;
  private GameObject monster;
  private PVEMonsterPreview preview;
  private bool _open;
  public UITexture damageAwardImage;
  public UITexture killIAwardmage;
  public UITexture auctionAwardImage;
  public UITexture damageRewardBg;
  public UITexture killRewardBg;
  public UITexture auctionRewardBg;
  private int _currentCount;

  private void LoadAwardsTextures()
  {
    BuilderFactory.Instance.HandyBuild((UIWidget) this.damageAwardImage, "Texture/ItemIcons/item_gve_kill_chest_6", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.killIAwardmage, "Texture/ItemIcons/item_gve_kill_chest_6", (System.Action<bool>) null, false, true, string.Empty);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.auctionAwardImage, "Texture/ItemIcons/icon_iap_gold2", (System.Action<bool>) null, false, true, string.Empty);
    Utils.SetItemNormalBackground(this.damageRewardBg, 0);
    Utils.SetItemNormalBackground(this.killRewardBg, 0);
    Utils.SetItemNormalBackground(this.auctionRewardBg, 0);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomBossDetailDlg.Parameter parameter = orgParam as KingdomBossDetailDlg.Parameter;
    if (orgParam != null)
    {
      this._targetLocation = parameter.coordinate;
      this._tileData = PVPMapData.MapData.GetReferenceAt(this._targetLocation);
    }
    this._maxReward = this.rewardItems.Length;
    this.monster = UnityEngine.Object.Instantiate<GameObject>(this.previewPrefab.gameObject);
    NGUITools.SetActive(this.monster, true);
    this.preview = this.monster.GetComponent<PVEMonsterPreview>();
    NGUITools.SetLayer(this.monster, LayerMask.NameToLayer("UI3D"));
    this.preview.InitializeCharacter(this._tileData);
    this.spirit.text = ScriptLocalization.Get("pve_monster_uppercase_attack", true);
    this.desc.text = string.Empty;
    this.title.text = string.Empty;
    this.LoadAwardsTextures();
    this.CheckCount();
  }

  public void OnViewHelp()
  {
    UIManager.inst.OpenPopup("HelpPopup", (Popup.PopupParameter) new HelpPopup.Parameter()
    {
      id = "help_worldboss_reward"
    });
  }

  private void CheckCount()
  {
    RequestManager.inst.SendRequest("WorldBoss:getTimes", new Hashtable()
    {
      {
        (object) "kingdom_id",
        (object) this._targetLocation.K
      }
    }, new System.Action<bool, object>(this.OnCallBack), true);
  }

  private void OnCallBack(bool success, object result)
  {
    if (!success || result == null)
      return;
    this._currentCount = Utils.TryParseInt(result as Hashtable, (object) "times");
    this.UpdateUI();
    int num = 10;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("attack_worldboss_num_max");
    if (data != null)
      num = data.ValueInt;
    this.spirit.text = ScriptLocalization.Get("pve_monster_uppercase_attack", true) + "    (" + (object) this._currentCount + "/" + (object) num + ")";
    if (this._currentCount >= num)
    {
      this.spirit.color = Color.red;
      this.attackBt.isEnabled = false;
    }
    else
    {
      this.spirit.color = Color.white;
      this.attackBt.isEnabled = true;
    }
  }

  public void OnCloseHandler()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  private void Clear()
  {
    for (int index = 0; index < this.rewardItems.Length; ++index)
      this.rewardItems[index].Clear();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.monster);
    this.preview = (PVEMonsterPreview) null;
  }

  private void UpdateUI()
  {
    if (this._tileData == null)
      return;
    if (PVPSystem.Instance.Map.IsForeigner() && !PVPMap.InHeitudi(this._targetLocation) || PlayerData.inst.playerCityData.cityLocation.K != this._targetLocation.K)
    {
      this.attackBt.isEnabled = false;
      NGUITools.SetActive(this.attackBt.gameObject, false);
      NGUITools.SetActive(this.normalTip.gameObject, true);
    }
    else
      this.attackBt.isEnabled = this._tileData.Location.K == PlayerData.inst.playerCityData.cityLocation.K;
    KingdomBossData kingdomBossData = this._tileData.KingdomBossData;
    float num1 = (float) kingdomBossData.CurrentTroopCount / (float) kingdomBossData.StartTroopCount;
    this.progressBar.value = num1;
    this.progressDesc.text = string.Format("{0:0.00%}", (object) num1);
    KingdomBossStaticInfo kingdomBossStaticInfo = ConfigManager.inst.DB_KingdomBoss.GetItem(kingdomBossData.configId);
    if (kingdomBossStaticInfo == null)
      return;
    int bossLevel = kingdomBossStaticInfo.BossLevel;
    this.desc.text = kingdomBossStaticInfo.GetTalk();
    if ((UnityEngine.Object) this.title != (UnityEngine.Object) null)
    {
      string localName = kingdomBossStaticInfo.LocalName;
      new Dictionary<string, string>()
      {
        {
          "0",
          bossLevel.ToString()
        }
      };
      this.title.text = Utils.XLAT("id_lv") + (object) bossLevel + " " + localName;
    }
    int num2 = 10;
    GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("attack_worldboss_num_max");
    if (data != null)
      num2 = data.ValueInt;
    this.spirit.text = ScriptLocalization.Get("pve_monster_uppercase_attack", true) + "    (" + (object) 0 + "/" + (object) num2 + ")";
  }

  private void SetRewardItem(Reward reward)
  {
    if (reward == null)
      return;
    List<Reward.RewardsValuePair> rewards = reward.GetRewards();
    for (int index = 0; index < rewards.Count; ++index)
    {
      Reward.RewardsValuePair rewardsValuePair = rewards[index];
      if (this._maxReward != 0 && index < this.rewardItems.Length)
      {
        ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(rewardsValuePair.internalID);
        this.rewardItems[index].SetData(RewardNormalItem.RewardType.item, itemStaticInfo.LocName, itemStaticInfo.ImagePath);
        --this._maxReward;
      }
    }
  }

  public void AttackHandler()
  {
    if (DBManager.inst.DB_March.marchOwner.Count >= PlayerData.inst.playerCityData.marchSlot)
    {
      GameEngine.Instance.marchSystem.ShowMaxMarchSlotMessage();
    }
    else
    {
      int num = 10;
      GameConfigInfo data = ConfigManager.inst.DB_GameConfig.GetData("attack_worldboss_num_max");
      if (data != null)
        num = data.ValueInt;
      if (this._currentCount >= num)
        UIManager.inst.toast.Show(ScriptLocalization.Get("monster_attack_not_enough_stamina_description", true), (System.Action) null, 4f, false);
      else
        UIManager.inst.OpenDlg("MarchAllocDlg", (UI.Dialog.DialogParameter) new MarchAllocDlg.Parameter()
        {
          location = this._targetLocation,
          marchType = MarchAllocDlg.MarchType.worldBossAttack,
          energyCost = 0
        }, 1 != 0, 1 != 0, 1 != 0);
    }
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public Coordinate coordinate;
  }
}
