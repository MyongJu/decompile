﻿// Decompiled with JetBrains decompiler
// Type: Properties
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Properties
{
  private List<Properties.PropertyValue> _effectiveValues = new List<Properties.PropertyValue>();
  private int _owner = -1;

  public Properties(int owner)
  {
    this._owner = owner;
    GameEngine.Instance.events.onPropertyValueChanged += new System.Action<Events.PropertyValueChangedArgs>(this.OnPropertyValueChanged);
  }

  private void OnPropertyValueChanged(Events.PropertyValueChangedArgs args)
  {
    if (args.Owner != this._owner && args.Owner != 0)
      return;
    List<Properties.PropertyValue> propertyValueList = new List<Properties.PropertyValue>();
    for (int index = 0; index < this._effectiveValues.Count; ++index)
    {
      if (this._effectiveValues[index].PropertyID.Equals(args.PropertyID))
        propertyValueList.Add(this._effectiveValues[index]);
    }
    for (int index = 0; index < propertyValueList.Count; ++index)
      this._effectiveValues.Remove(propertyValueList[index]);
  }

  public float EffectiveBonus(string propertyID, Effect.ConditionType condition = Effect.ConditionType.all)
  {
    Properties.PropertyValue propertyValue = (Properties.PropertyValue) null;
    for (int index = 0; index < this._effectiveValues.Count; ++index)
    {
      if (this._effectiveValues[index].PropertyID.Equals(propertyID) && (this._effectiveValues[index].Condition | condition) == condition)
      {
        propertyValue = this._effectiveValues[index];
        break;
      }
    }
    if (propertyValue == null)
    {
      propertyValue = new Properties.PropertyValue()
      {
        PropertyID = propertyID,
        Condition = condition
      };
      this._effectiveValues.Add(propertyValue);
    }
    return propertyValue.Value;
  }

  private class PropertyValue
  {
    public string PropertyID;
    public Effect.ConditionType Condition;
    public float Value;
  }
}
