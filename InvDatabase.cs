﻿// Decompiled with JetBrains decompiler
// Type: InvDatabase
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Examples/Item Database")]
public class InvDatabase : MonoBehaviour
{
  private static bool mIsDirty = true;
  public List<InvBaseItem> items = new List<InvBaseItem>();
  private static InvDatabase[] mList;
  public int databaseID;
  public UIAtlas iconAtlas;

  public static InvDatabase[] list
  {
    get
    {
      if (InvDatabase.mIsDirty)
      {
        InvDatabase.mIsDirty = false;
        InvDatabase.mList = NGUITools.FindActive<InvDatabase>();
      }
      return InvDatabase.mList;
    }
  }

  private void OnEnable()
  {
    InvDatabase.mIsDirty = true;
  }

  private void OnDisable()
  {
    InvDatabase.mIsDirty = true;
  }

  private InvBaseItem GetItem(int id16)
  {
    int index = 0;
    for (int count = this.items.Count; index < count; ++index)
    {
      InvBaseItem invBaseItem = this.items[index];
      if (invBaseItem.id16 == id16)
        return invBaseItem;
    }
    return (InvBaseItem) null;
  }

  private static InvDatabase GetDatabase(int dbID)
  {
    int index = 0;
    for (int length = InvDatabase.list.Length; index < length; ++index)
    {
      InvDatabase invDatabase = InvDatabase.list[index];
      if (invDatabase.databaseID == dbID)
        return invDatabase;
    }
    return (InvDatabase) null;
  }

  public static InvBaseItem FindByID(int id32)
  {
    InvDatabase database = InvDatabase.GetDatabase(id32 >> 16);
    if ((Object) database != (Object) null)
      return database.GetItem(id32 & (int) ushort.MaxValue);
    return (InvBaseItem) null;
  }

  public static InvBaseItem FindByName(string exact)
  {
    int index1 = 0;
    for (int length = InvDatabase.list.Length; index1 < length; ++index1)
    {
      InvDatabase invDatabase = InvDatabase.list[index1];
      int index2 = 0;
      for (int count = invDatabase.items.Count; index2 < count; ++index2)
      {
        InvBaseItem invBaseItem = invDatabase.items[index2];
        if (invBaseItem.name == exact)
          return invBaseItem;
      }
    }
    return (InvBaseItem) null;
  }

  public static int FindItemID(InvBaseItem item)
  {
    int index = 0;
    for (int length = InvDatabase.list.Length; index < length; ++index)
    {
      InvDatabase invDatabase = InvDatabase.list[index];
      if (invDatabase.items.Contains(item))
        return invDatabase.databaseID << 16 | item.id16;
    }
    return -1;
  }
}
