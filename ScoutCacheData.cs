﻿// Decompiled with JetBrains decompiler
// Type: ScoutCacheData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;

public class ScoutCacheData
{
  private Hashtable _mailData = new Hashtable();
  private int _scoutTime;
  private long _mailId;

  public int ScoutTime
  {
    get
    {
      return this._scoutTime;
    }
  }

  public long MailId
  {
    get
    {
      return this._mailId;
    }
  }

  public Hashtable MailData
  {
    get
    {
      return this._mailData;
    }
  }

  public bool IsMailRead { get; set; }

  public bool HasAvailableScoutReport
  {
    get
    {
      return NetServerTime.inst.ServerTimestamp - this._scoutTime < 600;
    }
  }

  public void Decode(Hashtable data)
  {
    if (data == null)
    {
      this._scoutTime = 0;
      this._mailId = 0L;
    }
    else
    {
      DatabaseTools.UpdateData(data, "time", ref this._scoutTime);
      DatabaseTools.UpdateData(data, "mail_id", ref this._mailId);
    }
    this._mailData.Clear();
  }

  public void SetMailData(Hashtable data)
  {
    if (data == null || !data.ContainsKey((object) "mail"))
      return;
    DatabaseTools.CheckAndParseOrgData(data[(object) "mail"], out this._mailData);
  }

  public struct Params
  {
    public const string SCOUT_TIME = "time";
    public const string MAIL_ID = "mail_id";
    public const string MAIL = "mail";
  }
}
