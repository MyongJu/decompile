﻿// Decompiled with JetBrains decompiler
// Type: DWRequirementUtil
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class DWRequirementUtil
{
  public static Dictionary<DWRequirement.RequirementType, string> prefabPathDic = (Dictionary<DWRequirement.RequirementType, string>) null;
  public static Dictionary<DWRequirement.RequirementType, GameObject> prefabDic = new Dictionary<DWRequirement.RequirementType, GameObject>();

  public static GameObject GetRequirePrefab(DWRequirement.RequirementType type)
  {
    GameObject gameObject = (GameObject) null;
    DWRequirementUtil.prefabDic.TryGetValue(type, out gameObject);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      gameObject = AssetManager.Instance.HandyLoad(DWRequirementUtil.PrefabPathDic[type], (System.Type) null) as GameObject;
      DWRequirementUtil.prefabDic.Add(type, gameObject);
    }
    return gameObject;
  }

  public static Dictionary<DWRequirement.RequirementType, string> PrefabPathDic
  {
    get
    {
      if (DWRequirementUtil.prefabPathDic == null)
      {
        DWRequirementUtil.prefabPathDic = new Dictionary<DWRequirement.RequirementType, string>();
        DWRequirementUtil.prefabPathDic.Add(DWRequirement.RequirementType.FOOD, "Prefab/GUI_Prefabs/Research/Requirement/ResourceRequirement");
        DWRequirementUtil.prefabPathDic.Add(DWRequirement.RequirementType.ORE, "Prefab/GUI_Prefabs/Research/Requirement/ResourceRequirement");
        DWRequirementUtil.prefabPathDic.Add(DWRequirement.RequirementType.SILVER, "Prefab/GUI_Prefabs/Research/Requirement/ResourceRequirement");
        DWRequirementUtil.prefabPathDic.Add(DWRequirement.RequirementType.WOOD, "Prefab/GUI_Prefabs/Research/Requirement/ResourceRequirement");
      }
      return DWRequirementUtil.prefabPathDic;
    }
  }

  public static DWRequirement.RequirementType ConvertCityDataResourceType(CityResourceInfo.ResourceType sourceType)
  {
    switch (sourceType)
    {
      case CityResourceInfo.ResourceType.FOOD:
        return DWRequirement.RequirementType.FOOD;
      case CityResourceInfo.ResourceType.WOOD:
        return DWRequirement.RequirementType.WOOD;
      case CityResourceInfo.ResourceType.ORE:
        return DWRequirement.RequirementType.ORE;
      case CityResourceInfo.ResourceType.SILVER:
        return DWRequirement.RequirementType.SILVER;
      default:
        return DWRequirement.RequirementType.NONE;
    }
  }
}
