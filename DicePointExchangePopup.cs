﻿// Decompiled with JetBrains decompiler
// Type: DicePointExchangePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class DicePointExchangePopup : Popup
{
  private List<DicePointsBoxSlot> _boxSlotList = new List<DicePointsBoxSlot>();
  private GameObjectPool _itemPool = new GameObjectPool();
  private List<DicePointsBox> boxs;
  private long score;
  public UILabel UIScore;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject _boxPrefab;
  private System.Action _onPanelHide;

  public void OnCloseHandler()
  {
    this.clearData();
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    DicePointExchangePopup.Parameter parameter = orgParam as DicePointExchangePopup.Parameter;
    if (parameter != null)
      this._onPanelHide = parameter.onPanelHide;
    this.createData();
    this.createUI();
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
    if (this._onPanelHide == null)
      return;
    this._onPanelHide();
  }

  private void ClearData()
  {
    using (List<DicePointsBoxSlot>.Enumerator enumerator = this._boxSlotList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._boxSlotList.Clear();
    this._itemPool.Clear();
  }

  private void UpdateUI()
  {
    this.UIScore.text = ScriptLocalization.Get("event_my_points", true) + ": " + Utils.FormatThousands(this.score.ToString());
    using (List<DicePointsBoxSlot>.Enumerator enumerator = this._boxSlotList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateUI();
    }
  }

  private DicePointsBoxSlot CreatePointsBoxSlot(DicePointsBox box, Transform parent)
  {
    this._itemPool.Initialize(this._boxPrefab, parent.gameObject);
    GameObject gameObject = this._itemPool.AddChild(parent.gameObject);
    gameObject.SetActive(true);
    DicePointsBoxSlot component = gameObject.GetComponent<DicePointsBoxSlot>();
    component.SetData(box);
    component.OnExchangReward += new DicePointsBoxSlot.OnExchangRewardHandler(this.ExchangeCallBack);
    return component;
  }

  private void createUI()
  {
    this.boxs.Sort((Comparison<DicePointsBox>) ((a, b) =>
    {
      if (a.State != b.State)
        return b.State.CompareTo((object) a.State);
      return a.Level.CompareTo(b.Level);
    }));
    using (List<DicePointsBox>.Enumerator enumerator = this.boxs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this._boxSlotList.Add(this.CreatePointsBoxSlot(enumerator.Current, this.table.transform));
    }
  }

  private void updateData()
  {
    this.score = DicePayload.Instance.TempDiceData.Score;
    for (int index = 0; index < this.boxs.Count; ++index)
    {
      this.boxs[index].setState(this.score);
      this._boxSlotList[index].UpdateData(this.boxs[index]);
    }
  }

  private void createData()
  {
    this.score = DicePayload.Instance.TempDiceData.Score;
    this.boxs = new List<DicePointsBox>();
    List<DiceScoreBoxInfo> diceScoreBoxList = ConfigManager.inst.DB_DiceScoreBox.DiceScoreBoxList;
    diceScoreBoxList.Sort((Comparison<DiceScoreBoxInfo>) ((x, y) => x.level - y.level));
    using (List<DiceScoreBoxInfo>.Enumerator enumerator = diceScoreBoxList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DicePointsBox dicePointsBox = new DicePointsBox(enumerator.Current);
        dicePointsBox.setState(this.score);
        this.boxs.Add(dicePointsBox);
      }
    }
  }

  private void clearData()
  {
    this.boxs.Clear();
  }

  private void ExchangeCallBack(int itemId, int count)
  {
    this.updateData();
    this.UpdateUI();
    DicePayload.Instance.ShowCollectEffect(itemId, count);
  }

  public class Parameter : Popup.PopupParameter
  {
    public System.Action onPanelHide;
  }
}
