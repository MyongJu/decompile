﻿// Decompiled with JetBrains decompiler
// Type: TileMagic
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections.Generic;
using UnityEngine;

public class TileMagic
{
  protected List<long> m_previousMagicId = new List<long>();
  protected long m_benifetBuffEffectId = -1;
  protected long m_deleteBuffEffectId = -1;
  protected long m_assaultEffectId = -1;
  protected long m_powerupEffectId = -1;
  private const string VFX_PATH = "Prefab/VFX/Altar/";
  private DummyPoint m_dummyPoint;
  private TileData m_TileData;
  private int m_sortingLayerId;

  public TileMagic(TileData tileData)
  {
    this.m_TileData = tileData;
  }

  protected void CreateBenefetEffect(Transform parent)
  {
    this.DestroyBenefetEffect();
    this.m_benifetBuffEffectId = VfxManager.Instance.CreateAndPlay("Prefab/VFX/Altar/fx_dragon_altar_buff", parent);
  }

  protected void DestroyBenefetEffect()
  {
    if (this.m_benifetBuffEffectId == -1L)
      return;
    VfxManager.Instance.Delete(this.m_benifetBuffEffectId);
    this.m_benifetBuffEffectId = -1L;
  }

  protected void CreateDeleteBuffEffect(Transform parent)
  {
    this.DestroyDeleteBuffEffect();
    this.m_deleteBuffEffectId = VfxManager.Instance.CreateAndPlay("Prefab/VFX/Altar/fx_dragon_altar_debuff", parent);
  }

  protected void DestroyDeleteBuffEffect()
  {
    if (this.m_deleteBuffEffectId == -1L)
      return;
    VfxManager.Instance.Delete(this.m_deleteBuffEffectId);
    this.m_deleteBuffEffectId = -1L;
  }

  protected void CreatePowerupEffect(Transform parent)
  {
    this.DestroyPowerupEffect();
    this.m_powerupEffectId = VfxManager.Instance.CreateAndPlay("Prefab/VFX/Altar/fx_altar_locking", parent);
  }

  protected void DestroyPowerupEffect()
  {
    if (this.m_powerupEffectId == -1L)
      return;
    VfxManager.Instance.Delete(this.m_powerupEffectId);
    this.m_powerupEffectId = -1L;
  }

  protected void CreateAssaultEffect(Transform parent, string effectName)
  {
    AudioManager.Instance.PlaySound("sfx_alliance_altar_cast_spell", false);
    this.DestroyAssaultEffect();
    if (!(bool) ((Object) this.m_dummyPoint))
      this.m_dummyPoint = DummyPoint.Create(parent.position, PVPSystem.Instance.Map.transform);
    this.m_assaultEffectId = VfxManager.Instance.CreateAndPlay("Prefab/VFX/Altar/" + effectName, (IVfxTarget) this.m_dummyPoint);
  }

  protected void DestroyAssaultEffect()
  {
    if ((bool) ((Object) this.m_dummyPoint))
      DummyPoint.Destory(this.m_dummyPoint);
    if (this.m_assaultEffectId == -1L)
      return;
    VfxManager.Instance.Delete(this.m_assaultEffectId);
    this.m_assaultEffectId = -1L;
  }

  public void Hide()
  {
    this.DestroyBenefetEffect();
    this.DestroyDeleteBuffEffect();
    this.DestroyPowerupEffect();
  }

  public void Reset()
  {
    this.m_previousMagicId.Clear();
    this.DestroyBenefetEffect();
    this.DestroyDeleteBuffEffect();
    this.DestroyAssaultEffect();
    this.DestroyPowerupEffect();
    this.m_previousMagicId.Clear();
  }

  public void UpdateVFX(Transform transform)
  {
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    List<long> longList = new List<long>();
    IEnumerator<long> enumerator1 = this.m_TileData.MagicIds.GetEnumerator();
    while (enumerator1.MoveNext())
      longList.Add(enumerator1.Current);
    using (List<long>.Enumerator enumerator2 = longList.GetEnumerator())
    {
      while (enumerator2.MoveNext())
      {
        AllianceMagicData allianceMagicData = DBManager.inst.DB_AllianceMagic.Get(enumerator2.Current);
        if (allianceMagicData != null)
        {
          if (allianceMagicData.CurrentState == "releasing")
          {
            TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(allianceMagicData.ConfigId);
            if (byId != null)
            {
              if (byId.IsHarmfull == 1)
                flag2 = true;
              else
                flag1 = true;
            }
          }
          if (allianceMagicData.CurrentState == "charging")
            flag3 = true;
        }
      }
    }
    using (List<long>.Enumerator enumerator2 = this.m_previousMagicId.GetEnumerator())
    {
      while (enumerator2.MoveNext())
      {
        long current = enumerator2.Current;
        if (!longList.Contains(current))
        {
          AllianceMagicData allianceMagicData = DBManager.inst.DB_AllianceMagic.Get(current);
          if (allianceMagicData != null && allianceMagicData.CurrentState == "releasing")
          {
            TempleSkillInfo byId = ConfigManager.inst.DB_TempleSkill.GetById(allianceMagicData.ConfigId);
            if (byId != null && !string.IsNullOrEmpty(byId.DisplayEffect))
              this.CreateAssaultEffect(transform, byId.DisplayEffect);
          }
          else if (allianceMagicData != null)
            ;
        }
      }
    }
    if (flag1 && this.m_benifetBuffEffectId == -1L)
      this.CreateBenefetEffect(transform);
    if (flag2 && this.m_deleteBuffEffectId == -1L)
      this.CreateDeleteBuffEffect(transform);
    if (flag3 && this.m_powerupEffectId == -1L)
      this.CreatePowerupEffect(transform);
    if (!flag1)
      this.DestroyBenefetEffect();
    if (!flag2)
      this.DestroyDeleteBuffEffect();
    if (!flag3)
      this.DestroyPowerupEffect();
    this.m_previousMagicId.Clear();
    this.m_previousMagicId.AddRange((IEnumerable<long>) longList);
    this.SetSortingLayerID(this.m_sortingLayerId);
  }

  public void SetSortingLayerID(int sortingLayerID)
  {
    this.m_sortingLayerId = sortingLayerID;
    long[] numArray = new long[4]
    {
      this.m_benifetBuffEffectId,
      this.m_deleteBuffEffectId,
      this.m_powerupEffectId,
      this.m_assaultEffectId
    };
    foreach (long guid in numArray)
    {
      if (guid != -1L)
      {
        IVfx vfxByGuid = VfxManager.Instance.GetVfxByGuid(guid);
        if (vfxByGuid != null && (bool) ((Object) (vfxByGuid as MonoBehaviour)))
        {
          ParticleSortingOrderModifier component = vfxByGuid.gameObject.GetComponent<ParticleSortingOrderModifier>();
          if ((Object) component != (Object) null)
            component.SortingLayerName = SortingLayer.IDToName(sortingLayerID);
        }
      }
    }
  }
}
