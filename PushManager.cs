﻿// Decompiled with JetBrains decompiler
// Type: PushManager
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using System.Collections;
using System.Collections.Generic;

public class PushManager
{
  private const string IM_PNB = "pnb";
  private const string IM_RTM = "rtm";
  public const int RETRY_INTERVAL = 50;
  private int _sTime;
  private bool _connectState;
  private bool _tmpState;
  private List<long> _ids;
  private System.Action<bool> _connectCallback;
  private IPushSystem _pushSystem;
  private static PushManager _instance;
  private bool isKitout;

  public event System.Action<bool> ConnectCallback;

  public event System.Action ConsistentErrorCallback;

  public event System.Action OnKickedOutCallback;

  public static PushManager inst
  {
    get
    {
      if (PushManager._instance == null)
        PushManager._instance = new PushManager();
      return PushManager._instance;
    }
  }

  public void SyncConnectionState()
  {
    this._tmpState = this._pushSystem != null && this._pushSystem.ConnectionState;
  }

  public void NotifyDisconnect()
  {
    this._tmpState = false;
  }

  public void NotifyKickedOut()
  {
    this.isKitout = true;
  }

  public void Init(long userChannel)
  {
    this.isKitout = false;
    if (this._pushSystem != null)
      return;
    this._pushSystem = (IPushSystem) new RtmPushSystem();
    if (this._pushSystem == null)
      return;
    this._pushSystem.Init(userChannel);
    this._pushSystem.OnConsistentError += new System.Action(this.HandleOnConsistentError);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int timestamp)
  {
    this.UpdateState();
  }

  private void UpdateState()
  {
    if (this.isKitout)
    {
      this.isKitout = false;
      if (this.OnKickedOutCallback != null)
        this.OnKickedOutCallback();
    }
    if (this._connectState && !this._tmpState)
      NetWorkDetector.Instance.IsConnectionAvailable = false;
    else if (!this._connectState && this._tmpState)
      NetWorkDetector.Instance.IsConnectionAvailable = true;
    this._connectState = this._tmpState;
  }

  public void Dispose()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    this._sTime = 0;
    this._connectCallback = (System.Action<bool>) null;
    this._tmpState = false;
    this._connectState = false;
    if (this._pushSystem != null)
    {
      this._pushSystem.OnConsistentError -= new System.Action(this.HandleOnConsistentError);
      this._pushSystem.Dispose();
      this._pushSystem = (IPushSystem) null;
    }
    if (this._ids == null)
      return;
    this._ids.Clear();
    this._ids = (List<long>) null;
  }

  public void ReConnect()
  {
    this._tmpState = false;
    this._connectState = false;
    this.DoConnect(this._ids, this._connectCallback);
  }

  public void ConnectFail()
  {
    if (this.ConnectCallback == null)
      return;
    this.ConnectCallback(false);
  }

  public void DoConnect(List<long> ids, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._ids = ids;
    this._connectCallback = callback;
    this._pushSystem.DoConnect(ids, (System.Action<bool>) (flag =>
    {
      this._tmpState = flag;
      if (this._connectCallback != null)
        this._connectCallback(flag);
      if (this.ConnectCallback == null)
        return;
      this.ConnectCallback(flag);
    }));
  }

  public void GroupJoin(long id, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.GroupJoin(id, callback);
  }

  public void GroupLeave(long id, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.GroupLeave(id, callback);
  }

  public void Send(string action, long id, object msg, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.Send(id, this.MsgData(action, msg), callback);
  }

  public void SendGroup(string action, long id, object msg, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.SendGroup(id, this.MsgData(action, msg), callback);
  }

  public void PushGroup(string action, long id, object msg, System.Action<bool> callback = null)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.PushGroup(id, msg, callback);
  }

  public void GetOnlineUsers(List<long> uid, System.Action<List<long>> callback)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.GetOnlineUsers(uid, callback);
  }

  public void HistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.HistoryMessage(id, num, offset, callback, mType);
  }

  public void GroupHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.GroupHistoryMessage(id, num, offset, callback, mType);
  }

  public void PersonalHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback)
  {
    if (this._pushSystem == null)
      return;
    this._pushSystem.PersenalHistoryMessage(id, num, offset, callback);
  }

  private object MsgData(string action, object msg)
  {
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "cmd"] = (object) action;
    hashtable[(object) "payload"] = msg;
    return (object) hashtable;
  }

  private void HandleOnConsistentError()
  {
    if (this.ConsistentErrorCallback == null)
      return;
    this.ConsistentErrorCallback();
  }
}
