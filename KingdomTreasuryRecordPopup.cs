﻿// Decompiled with JetBrains decompiler
// Type: KingdomTreasuryRecordPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class KingdomTreasuryRecordPopup : Popup
{
  private Dictionary<int, KingdomTreasuryRecordItemRenderer> _itemDict = new Dictionary<int, KingdomTreasuryRecordItemRenderer>();
  private GameObjectPool _itemPool = new GameObjectPool();
  public GameObject noRecordNode;
  public UILabel panelTitle;
  public UIScrollView scrollView;
  public UITable table;
  public GameObject itemPrefab;
  public GameObject itemRoot;
  private string _recordType;
  private ArrayList _recordData;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    KingdomTreasuryRecordPopup.Parameter parameter = orgParam as KingdomTreasuryRecordPopup.Parameter;
    if (parameter != null)
    {
      this._recordType = parameter.recordType;
      this._recordData = parameter.recordData;
    }
    this._itemPool.Initialize(this.itemPrefab, this.itemRoot);
    this.ShowRecordContent();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.ClearData();
  }

  public void OnCloseBtnPressed()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void ShowRecordContent()
  {
    this.panelTitle.text = !(this._recordType == "assign_log") ? Utils.XLAT("throne_treasury_uppercase_tax_history") : Utils.XLAT("throne_treasury_uppercase_distribution_history");
    this.ClearData();
    if (this._recordData != null)
    {
      List<KeyValuePair<int, Hashtable>> keyValuePairList = new List<KeyValuePair<int, Hashtable>>();
      for (int index = 0; index < this._recordData.Count; ++index)
      {
        Hashtable inData = this._recordData[index] as Hashtable;
        int outData = 0;
        if (inData != null)
          DatabaseTools.UpdateData(inData, "time", ref outData);
        keyValuePairList.Add(new KeyValuePair<int, Hashtable>(outData, inData));
      }
      keyValuePairList.Sort((Comparison<KeyValuePair<int, Hashtable>>) ((a, b) => b.Key.CompareTo(a.Key)));
      for (int key = 0; key < keyValuePairList.Count; ++key)
      {
        KingdomTreasuryRecordItemRenderer itemRenderer = this.CreateItemRenderer(this._recordType, keyValuePairList[key].Key, keyValuePairList[key].Value);
        this._itemDict.Add(key, itemRenderer);
      }
    }
    NGUITools.SetActive(this.noRecordNode, this._recordData == null || this._recordData.Count <= 0);
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.Reposition()));
  }

  private void ClearData()
  {
    using (Dictionary<int, KingdomTreasuryRecordItemRenderer>.Enumerator enumerator = this._itemDict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject gameObject = enumerator.Current.Value.gameObject;
        gameObject.SetActive(false);
        this._itemPool.Release(gameObject);
      }
    }
    this._itemDict.Clear();
    this._itemPool.Clear();
  }

  private void Reposition()
  {
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
    this.scrollView.gameObject.SetActive(false);
    this.scrollView.gameObject.SetActive(true);
  }

  private KingdomTreasuryRecordItemRenderer CreateItemRenderer(string recordType, int recordTime, Hashtable recordData)
  {
    GameObject gameObject = this._itemPool.AddChild(this.table.gameObject);
    gameObject.SetActive(true);
    KingdomTreasuryRecordItemRenderer component = gameObject.GetComponent<KingdomTreasuryRecordItemRenderer>();
    component.SetData(recordType, recordTime, recordData);
    return component;
  }

  public class Parameter : Popup.PopupParameter
  {
    public string recordType;
    public ArrayList recordData;
  }
}
