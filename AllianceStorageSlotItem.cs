﻿// Decompiled with JetBrains decompiler
// Type: AllianceStorageSlotItem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using UnityEngine;

public class AllianceStorageSlotItem : MonoBehaviour
{
  public UILabel m_indexLabel;
  public UITexture m_playerIconTexture;
  public UILabel m_playerNameLabel;
  public UILabel m_resourceWeightLabel;
  public UILabel m_foodLabel;
  public UILabel m_woodLabel;
  public UILabel m_oreLabel;
  public UILabel m_silverLabel;
  public UIWidget m_backgroundWidget;
  public Transform m_arrawTransform;
  public int[] m_backgroundHeight;
  public GameObject m_rootOpened;
  protected bool m_isOpened;
  protected UITable _ParentTable;

  public UITable ParentTable
  {
    get
    {
      return this._ParentTable;
    }
    set
    {
      this._ParentTable = value;
    }
  }

  public void SetData(int index, long userId, AllianceWareHouseData.ResourceData resourceData)
  {
    this.m_indexLabel.text = index.ToString();
    UserData userData = DBManager.inst.DB_User.Get(userId);
    if (userData == null)
      return;
    this.m_playerNameLabel.text = userData.userName;
    CustomIconLoader.Instance.requestCustomIcon(this.m_playerIconTexture, UserData.GetPortraitIconPath(userData.portrait), userData.Icon, false);
    LordTitlePayload.Instance.ApplyUserAvator(this.m_playerIconTexture, userData.LordTitle, 1);
    this.m_resourceWeightLabel.text = Utils.FormatShortThousandsLong(resourceData.TotalWeight);
    this.m_foodLabel.text = Utils.FormatShortThousandsLong(resourceData.food);
    this.m_woodLabel.text = Utils.FormatShortThousandsLong(resourceData.wood);
    this.m_oreLabel.text = Utils.FormatShortThousandsLong(resourceData.ore);
    this.m_silverLabel.text = Utils.FormatShortThousandsLong(resourceData.sliver);
    this.RefreshOpenState();
  }

  public void OnItemClicked()
  {
    this.m_isOpened = !this.m_isOpened;
    this.RefreshOpenState();
    if (!(bool) ((Object) this.ParentTable))
      return;
    this.ParentTable.Reposition();
  }

  protected void RefreshOpenState()
  {
    this.m_backgroundWidget.height = this.m_backgroundHeight[!this.m_isOpened ? 0 : 1];
    this.m_arrawTransform.localRotation = Quaternion.AngleAxis(!this.m_isOpened ? 0.0f : -90f, Vector3.forward);
    this.m_rootOpened.SetActive(this.m_isOpened);
  }
}
