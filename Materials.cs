﻿// Decompiled with JetBrains decompiler
// Type: Materials
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class Materials : ICustomParse
{
  private List<Materials.MaterialElement> elements;

  public void Parse(object content)
  {
    this.elements = new List<Materials.MaterialElement>();
    ArrayList arrayList = content as ArrayList;
    if (arrayList == null)
      return;
    for (int index = 0; index < arrayList.Count; ++index)
    {
      foreach (DictionaryEntry dictionaryEntry in arrayList[index] as Hashtable)
        this.elements.Add(new Materials.MaterialElement(int.Parse(dictionaryEntry.Key.ToString()), long.Parse(dictionaryEntry.Value.ToString())));
    }
  }

  public List<Materials.MaterialElement> GetMaterials()
  {
    return this.elements;
  }

  public class MaterialElement
  {
    public int internalID;
    public long amount;

    public MaterialElement(int internalID, long amount)
    {
      this.internalID = internalID;
      this.amount = amount;
    }
  }
}
