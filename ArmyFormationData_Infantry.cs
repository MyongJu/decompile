﻿// Decompiled with JetBrains decompiler
// Type: ArmyFormationData_Infantry
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ArmyFormationData_Infantry : ArmyFormationData
{
  protected override MarchConfig.MarchInfo configInfo
  {
    get
    {
      return (MarchConfig.MarchInfo) MarchConfig.infantryInfo;
    }
  }

  public override Vector3[] CalcAttackFormation(Vector3 deltaPos, float radius, Vector3 dir)
  {
    this.centerDeltaPos = deltaPos;
    dir.Normalize();
    Vector3[] pos = new Vector3[this.curUnitCount];
    Vector3[] vector3Array = new Vector3[3];
    if ((double) Mathf.Abs(dir.x) > (double) Mathf.Abs(dir.y) * 2.0)
    {
      if ((double) dir.x > 0.0)
      {
        vector3Array[0] = this.configInfo.formation_action[1];
        vector3Array[1] = this.configInfo.formation_action[2];
        vector3Array[2] = this.configInfo.formation_action[3];
      }
      else
      {
        vector3Array[0] = this.configInfo.formation_action[3];
        vector3Array[1] = this.configInfo.formation_action[0];
        vector3Array[2] = this.configInfo.formation_action[1];
      }
    }
    else if ((double) dir.y > 0.0)
    {
      vector3Array[0] = this.configInfo.formation_action[2];
      vector3Array[1] = this.configInfo.formation_action[3];
      vector3Array[2] = this.configInfo.formation_action[0];
    }
    else
    {
      vector3Array[0] = this.configInfo.formation_action[0];
      vector3Array[1] = this.configInfo.formation_action[1];
      vector3Array[2] = this.configInfo.formation_action[2];
    }
    float num1 = 1f / (float) this.maxUnitCount;
    int num2 = 1;
    int num3 = 1;
    for (int index1 = 0; index1 < this.curUnitFormation_x; ++index1)
    {
      for (int index2 = 0; index2 < this.curUnitFormation_y; ++index2)
      {
        if (index2 < this.curUnitFormation_y / 2)
        {
          pos[index1 * this.curUnitFormation_y + index2] = Vector3.Lerp(vector3Array[1], vector3Array[0], num1 * 2f * (float) num2);
          ++num2;
        }
        else
        {
          pos[index1 * this.curUnitFormation_y + index2] = Vector3.Lerp(vector3Array[1], vector3Array[2], num1 * 2f * (float) num3);
          ++num3;
        }
      }
    }
    this.RecalcAttackFormation(ref pos, deltaPos, radius);
    return pos;
  }
}
