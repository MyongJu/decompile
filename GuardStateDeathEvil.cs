﻿// Decompiled with JetBrains decompiler
// Type: GuardStateDeathEvil
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class GuardStateDeathEvil : AbstractGuardState
{
  public GuardStateDeathEvil(RoundPlayer player)
    : base(player)
  {
  }

  public override string Key
  {
    get
    {
      return "guard_death";
    }
  }

  public override Hashtable Data { get; set; }

  public override void OnEnter()
  {
    DragonKnightUtils.UpdateAndPlay(this.Player.Model + "_death", this.Player);
  }

  public override void OnProcess()
  {
    if (this.Player.Animation.isPlaying)
      return;
    this.Player.StateMacine.SetState("guard_death_end", (Hashtable) null);
  }

  public override void OnExit()
  {
  }

  public override void Dispose()
  {
  }
}
