﻿// Decompiled with JetBrains decompiler
// Type: AllianceTechDlgPanel
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AllianceTechDlgPanel : MonoBehaviour
{
  private int _tier;
  [SerializeField]
  private AllianceTechDlgPanel.Panel panel;

  public void Setup(int tier, int focusedTechId)
  {
    this._tier = tier;
    List<int> techIdListByTier = DBManager.inst.DB_AllianceTech.GetAllianceTechIdListByTier(this._tier);
    this.panel.title.text = ScriptLocalization.GetWithPara("alliance_knowledge_level_num", new Dictionary<string, string>()
    {
      {
        "num",
        this._tier.ToString()
      }
    }, true);
    if (techIdListByTier == null)
      return;
    for (int count = this.panel.slots.Count; count < techIdListByTier.Count; ++count)
    {
      GameObject gameObject = NGUITools.AddChild(this.panel.allianceGrid.gameObject, this.panel.allianceSlotOrg.gameObject);
      gameObject.name = string.Format("tech_{0}", (object) (100 + count));
      AllianceTechSlot component = gameObject.GetComponent<AllianceTechSlot>();
      component.onSlotClick = new System.Action<int>(this.OnDonateClick);
      component.onMarkClick = new System.Action<int>(this.OnMarkButtonClick);
      component.onSpeedUpClick = new System.Action<int>(this.OnSpeedUpButtonClick);
      component.transform.localPosition = new Vector3(0.0f, (float) (-310 * count), 0.0f);
      component.onResearchClick = new System.Action<int>(this.OnResearchButtonClick);
      this.panel.slots.Add(component);
    }
    for (int index = 0; index < techIdListByTier.Count; ++index)
    {
      this.panel.slots[index].gameObject.SetActive(true);
      this.panel.slots[index].Setup(techIdListByTier[index]);
    }
    int num = 0;
    if (focusedTechId != 0)
    {
      for (int index = 0; index < techIdListByTier.Count; ++index)
      {
        this.panel.slots[index].gameObject.SetActive(true);
        this.panel.slots[index].Setup(techIdListByTier[index]);
        if (focusedTechId == techIdListByTier[index])
        {
          num = index;
          break;
        }
      }
    }
    else
    {
      for (int index = 0; index < techIdListByTier.Count; ++index)
      {
        if (PlayerData.inst.allianceData.techMark == techIdListByTier[index])
        {
          num = index;
          break;
        }
      }
    }
    for (int count = techIdListByTier.Count; count < this.panel.slots.Count; ++count)
      this.panel.slots[count].gameObject.SetActive(false);
    this.panel.scorllView.ResetPosition();
    this.panel.scorllView.MoveRelative(new Vector3(0.0f, (float) (310 * num), 0.0f));
    this.gameObject.SetActive(false);
    this.gameObject.SetActive(true);
  }

  public void OnDonateClick(int researchId)
  {
    if (ConfigManager.inst.DB_AllianceTech.IsMaxLevel(researchId))
      return;
    UIManager.inst.OpenPopup("AllianceTech/AllianceTechDonatePopUp", (Popup.PopupParameter) new AllianceTechDonateDlg.AllianceTechDonateParam()
    {
      researchId = researchId
    });
  }

  public void OnMarkButtonClick(int researchId)
  {
    RequestManager.inst.SendRequest("AllianceTech:mark", Utils.Hash((object) "research_id", (object) researchId), (System.Action<bool, object>) ((result, orgData) => this.Setup(this._tier, 0)), true);
  }

  public void OnResearchButtonClick(int researchId)
  {
    RequestManager.inst.SendRequest("AllianceTech:research", Utils.Hash((object) "research_id", (object) researchId), (System.Action<bool, object>) ((result, orgData) => this.Setup(this._tier, researchId)), true);
  }

  public void OnSpeedUpButtonClick(int researchId)
  {
    if (researchId != DBManager.inst.DB_AllianceTech.researchingId)
      D.error((object) "want to speedup {0}, but now researching is {1}", (object) researchId, (object) DBManager.inst.DB_AllianceTech.researchingId);
    else if (!PlayerData.inst.isAllianceR4Member)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_permission"), (System.Action) null, 4f, false);
    else if (AllianceTechSpeedUpLogic.Instance.MaxCanUseCount <= 0)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_store_insufficient_funds"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenPopup("AllianceTech/AllianceTechSpeedUpPopup", (Popup.PopupParameter) new AllianceTechSpeedUpPopup.Parameter()
      {
        researchId = researchId
      });
  }

  [Serializable]
  protected class Panel
  {
    public List<AllianceTechSlot> slots = new List<AllianceTechSlot>();
    public Transform panelTrans;
    public UILabel title;
    public UIScrollView scorllView;
    public UIGrid allianceGrid;
    public AllianceTechSlot allianceSlotOrg;
  }
}
