﻿// Decompiled with JetBrains decompiler
// Type: AllianceRewardsInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AllianceRewardsInfo
{
  public int Internal_ID;
  [Config(Name = "id")]
  public string id;
  [Config(Name = "rank_min")]
  public int rank_min;
  [Config(Name = "rank_max")]
  public int rank_max;
  [Config(Name = "fund")]
  public int fund;
  [Config(Name = "honor")]
  public int honor;
  [Config(Name = "alliance_reward_id_1")]
  public int allianc_reward_id_1;
  [Config(Name = "alliance_reward_value_1")]
  public int allianc_reward_value_1;
  [Config(Name = "alliance_reward_id_2")]
  public int allianc_reward_id_2;
  [Config(Name = "alliance_reward_value_2")]
  public int allianc_reward_value_2;
  [Config(Name = "alliance_reward_id_3")]
  public int allianc_reward_id_3;
  [Config(Name = "alliance_reward_value_3")]
  public int allianc_reward_value_3;
  [Config(Name = "individual_reward_id_1")]
  public int individual_reward_id_1;
  [Config(Name = "individual_reward_value_1")]
  public int individual_reward_value_1;
  [Config(Name = "individual_reward_id_2")]
  public int individual_reward_id_2;
  [Config(Name = "individual_reward_value_2")]
  public int individual_reward_value_2;
  [Config(Name = "individual_reward_id_3")]
  public int individual_reward_id_3;
  [Config(Name = "individual_reward_value_3")]
  public int individual_reward_value_3;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "AllianceRewards")]
  public Reward allianceRewards;
  [Config(CustomParse = true, CustomType = typeof (Reward), Name = "IndividualRewards")]
  public Reward individualRewards;

  public int GetAllianceRewardId(string index)
  {
    int num1 = 0;
    string key = index;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map20 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map20 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map20.TryGetValue(key, out num2))
      {
        switch (num2)
        {
          case 0:
            num1 = this.allianc_reward_id_1;
            break;
          case 1:
            num1 = this.allianc_reward_id_2;
            break;
          case 2:
            num1 = this.allianc_reward_id_3;
            break;
        }
      }
    }
    return num1;
  }

  public int GetAllianceRewardCount(string index)
  {
    int num1 = 0;
    string key = index;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map21 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map21 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map21.TryGetValue(key, out num2))
      {
        switch (num2)
        {
          case 0:
            num1 = this.allianc_reward_value_1;
            break;
          case 1:
            num1 = this.allianc_reward_value_2;
            break;
          case 2:
            num1 = this.allianc_reward_value_3;
            break;
        }
      }
    }
    return num1;
  }

  public int GetIndividualRewardId(string index)
  {
    int num1 = 0;
    string key = index;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map22 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map22 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map22.TryGetValue(key, out num2))
      {
        switch (num2)
        {
          case 0:
            num1 = this.individual_reward_id_1;
            break;
          case 1:
            num1 = this.individual_reward_id_2;
            break;
          case 2:
            num1 = this.individual_reward_id_3;
            break;
        }
      }
    }
    return num1;
  }

  public int GetIndividualRewardCount(string index)
  {
    int num1 = 0;
    string key = index;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map23 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map23 = new Dictionary<string, int>(3)
        {
          {
            "1",
            0
          },
          {
            "2",
            1
          },
          {
            "3",
            2
          }
        };
      }
      int num2;
      // ISSUE: reference to a compiler-generated field
      if (AllianceRewardsInfo.\u003C\u003Ef__switch\u0024map23.TryGetValue(key, out num2))
      {
        switch (num2)
        {
          case 0:
            num1 = this.individual_reward_value_1;
            break;
          case 1:
            num1 = this.individual_reward_value_2;
            break;
          case 2:
            num1 = this.individual_reward_value_3;
            break;
        }
      }
    }
    return num1;
  }

  public Dictionary<int, int> GetAllianceRewards()
  {
    return new Dictionary<int, int>()
    {
      {
        this.allianc_reward_id_1,
        this.allianc_reward_value_1
      },
      {
        this.allianc_reward_id_2,
        this.allianc_reward_value_2
      },
      {
        this.allianc_reward_id_3,
        this.allianc_reward_value_3
      }
    };
  }

  public Dictionary<int, int> GetIndividualRewards()
  {
    return new Dictionary<int, int>()
    {
      {
        this.individual_reward_id_1,
        this.individual_reward_value_1
      },
      {
        this.individual_reward_id_2,
        this.individual_reward_value_2
      },
      {
        this.individual_reward_id_3,
        this.individual_reward_value_3
      }
    };
  }
}
