﻿// Decompiled with JetBrains decompiler
// Type: TimeLimitActivityRankPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class TimeLimitActivityRankPopup : Popup
{
  private bool isNormal = true;
  private List<TimeLimitActivityRankContainer> containers = new List<TimeLimitActivityRankContainer>();
  public UIGrid grid;
  public UIScrollView scroll;
  public TimeLimitActivityRankContainer containerPrefab;
  public UILabel titleLabel;
  public UILabel stepTip;
  public UILabel rankTip;
  private ActivityBaseData data;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    TimeLimitActivityRankPopup.Parameter parameter = orgParam as TimeLimitActivityRankPopup.Parameter;
    if (parameter != null)
      this.isNormal = parameter.isStepRank;
    this.titleLabel.text = ScriptLocalization.Get("event_view_strongest_lord_history", true);
    if (this.isNormal)
      this.titleLabel.text = ScriptLocalization.Get("event_view_ranking_history", true);
    this.data = parameter.data;
    ActivityBaseData.ActivityType type = this.data.Type;
    switch (type)
    {
      case ActivityBaseData.ActivityType.TimeLimit:
        if (this.isNormal)
        {
          ActivityManager.Intance.LoadStepRankData(new System.Action(this.UpdateUI));
          break;
        }
        ActivityManager.Intance.LoadTotalRankData(new System.Action(this.UpdateUI));
        break;
      case ActivityBaseData.ActivityType.AllianceActivity:
        if (this.isNormal)
        {
          ActivityManager.Intance.LoadAllianceStepRankData(new System.Action(this.UpdateUI));
          break;
        }
        ActivityManager.Intance.LoadAllianceTotalRankData(new System.Action(this.UpdateUI));
        break;
      default:
        if (type != ActivityBaseData.ActivityType.Personal)
          break;
        ActivityManager.Intance.LoadPersonalTotalRankData(new System.Action(this.UpdateUI));
        break;
    }
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void CreateFakeData()
  {
    List<int> intList = new List<int>();
    intList.Add(NetServerTime.inst.ServerTimestamp);
    intList.Add(NetServerTime.inst.ServerTimestamp - 604800);
    int num = 5;
    for (int index = 0; index < intList.Count; ++index)
    {
      GameObject go = NGUITools.AddChild(this.grid.gameObject, this.containerPrefab.gameObject);
      NGUITools.SetActive(go, true);
      TimeLimitActivityRankContainer component = go.GetComponent<TimeLimitActivityRankContainer>();
      string withPara = ScriptLocalization.Get("event_view_ranking_history", true);
      if (this.isNormal)
        withPara = ScriptLocalization.GetWithPara("event_view_strongest_lord_history", new Dictionary<string, string>()
        {
          {
            "0",
            (index + 1).ToString()
          },
          {
            "1",
            num.ToString()
          }
        }, true);
      component.SetData(intList[index], withPara, this.isNormal);
      this.containers.Add(component);
    }
    this.grid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scroll.ResetPosition()));
  }

  private void UpdateUI()
  {
    List<int> rankTime = ActivityManager.Intance.GetRankTime(this.isNormal);
    int num = 1;
    this.titleLabel.text = ScriptLocalization.Get("event_view_strongest_lord_history", true);
    NGUITools.SetActive(this.stepTip.gameObject, false);
    NGUITools.SetActive(this.rankTip.gameObject, false);
    if (this.isNormal)
    {
      if (this.data is RoundActivityData)
        num = (this.data as RoundActivityData).TotalRounds;
      this.titleLabel.text = ScriptLocalization.Get("event_view_ranking_history", true);
    }
    if (rankTime.Count == 0)
    {
      if (this.isNormal)
        NGUITools.SetActive(this.stepTip.gameObject, true);
      else
        NGUITools.SetActive(this.rankTip.gameObject, true);
    }
    int count = rankTime.Count;
    RoundActivityData data = this.data as RoundActivityData;
    int totalCount = count;
    if (data != null)
      totalCount = data.TotalRounds;
    if (this.isNormal)
    {
      for (int index = count - 1; index > -1; --index)
        this.SetItemData(rankTime[index], totalCount, index + 1);
    }
    else
    {
      for (int index = 0; index < count; ++index)
        this.SetItemData(rankTime[index], totalCount, index + 1);
    }
    this.grid.Reposition();
    this.scroll.ResetPosition();
  }

  private void SetItemData(int time, int totalCount, int index)
  {
    if (ActivityManager.Intance.GetCurrentRankData(time, this.isNormal, false).Count == 0)
      return;
    GameObject go = NGUITools.AddChild(this.grid.gameObject, this.containerPrefab.gameObject);
    NGUITools.SetActive(go, true);
    TimeLimitActivityRankContainer component = go.GetComponent<TimeLimitActivityRankContainer>();
    string withPara = ScriptLocalization.Get("event_strongest_lords_subtitle", true);
    if (this.data.Type == ActivityBaseData.ActivityType.AllianceActivity)
      withPara = ScriptLocalization.Get("event_strongest_alliances_subtitle", true);
    if (this.isNormal)
      withPara = ScriptLocalization.GetWithPara("event_stage_num_rankings", new Dictionary<string, string>()
      {
        {
          "1",
          index.ToString() + "/" + totalCount.ToString()
        }
      }, true);
    component.SetData(time, withPara, this.isNormal);
    this.containers.Add(component);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    for (int index = 0; index < this.containers.Count; ++index)
      this.containers[index].Clear();
    this.containers.Clear();
  }

  public class Parameter : Popup.PopupParameter
  {
    public bool isStepRank = true;
    public ActivityBaseData data;
  }
}
