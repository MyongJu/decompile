﻿// Decompiled with JetBrains decompiler
// Type: ReYunTrack
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ReYunTrack : MonoBehaviour
{
  private static ReYunTrack _instance;

  public static ReYunTrack Instance
  {
    get
    {
      if (!(bool) ((Object) ReYunTrack._instance))
      {
        ReYunTrack._instance = Object.FindObjectOfType(typeof (ReYunTrack)) as ReYunTrack;
        if (!(bool) ((Object) ReYunTrack._instance))
          ReYunTrack._instance = new GameObject(nameof (ReYunTrack)).AddComponent(typeof (ReYunTrack)) as ReYunTrack;
      }
      return ReYunTrack._instance;
    }
  }

  private void Awake()
  {
    Object.DontDestroyOnLoad((Object) this);
  }

  public static AndroidJavaObject getApplicationContext()
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    {
      using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
        return androidJavaObject.Call<AndroidJavaObject>(nameof (getApplicationContext));
    }
  }

  public void Track_Init(string appKey, string channelId)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("initWithKeyAndChannelId", (object) ReYunTrack.getApplicationContext(), (object) appKey, (object) channelId);
  }

  public void Track_Register(string account)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("setRegisterWithAccountID", new object[1]
      {
        (object) account
      });
  }

  public void Track_Login(string account)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("setLoginSuccessBusiness", new object[1]
      {
        (object) account
      });
  }

  public void Track_SetPaymentStart(string transactionId, string paymentType, string currencyType, float currencyAmount)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("setPaymentStart", (object) transactionId, (object) paymentType, (object) currencyType, (object) currencyAmount);
  }

  public void Track_SetPayment(string transactionId, string paymentType, string currencyType, float currencyAmount)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("setPayment", (object) transactionId, (object) paymentType, (object) currencyType, (object) currencyAmount);
  }

  public void Track_SetEvent(string eventName)
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      androidJavaClass.CallStatic("setEvent", new object[1]
      {
        (object) eventName
      });
  }

  public string Track_getDeviceId()
  {
    using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.reyun.sdk.ReYunTrack"))
      return androidJavaClass.CallStatic<string>("getDeviceId");
  }

  public void Track_setPrintLog(bool print)
  {
  }
}
