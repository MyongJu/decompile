﻿// Decompiled with JetBrains decompiler
// Type: VIPItemsDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VIPItemsDlg : UI.Dialog
{
  public BetterList<VIPListItem> items = new BetterList<VIPListItem>();
  private GameObjectPool pools = new GameObjectPool();
  public VIPListItem itemPrefab;
  public UILabel title;
  public UIGrid grid;
  public UIScrollView scrollView;
  public GameObject itemRoot;

  private void Awake()
  {
    this.itemRoot = this.gameObject;
    this.pools.Initialize(this.itemPrefab.gameObject, this.itemRoot);
  }

  public void OnBackBtPress()
  {
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  public void OnCloseBackBtPress()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    JobManager.Instance.OnJobCreated += new System.Action<long>(this.OnJobCreated);
    this.UpdateUI();
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.Clear();
  }

  private void UpdateUI()
  {
    this.scrollView.gameObject.SetActive(false);
    List<ItemStaticInfo> vipItems = ItemBag.Instance.GetVIPItems();
    vipItems.Sort(new Comparison<ItemStaticInfo>(this.CompareItem));
    for (int index = 0; index < vipItems.Count; ++index)
      this.CreateBoostListItem(ItemBag.Instance.GetCurrentSaleShopItemsByItem_InternalId(vipItems[index].internalId));
    this.title.text = ScriptLocalization.Get("VIP", true);
    this.ResetTabel();
  }

  private int CompareItem(ItemStaticInfo a, ItemStaticInfo b)
  {
    List<ShopStaticInfo> byItemInternalId1 = ItemBag.Instance.GetCurrentSaleShopItemsByItem_InternalId(a.internalId);
    List<ShopStaticInfo> byItemInternalId2 = ItemBag.Instance.GetCurrentSaleShopItemsByItem_InternalId(b.internalId);
    int num1 = 0;
    int num2 = 0;
    if (byItemInternalId1.Count > 0)
      num1 = ItemBag.Instance.GetShopItemPrice(byItemInternalId1[0].ID);
    if (byItemInternalId2.Count > 0)
      num2 = ItemBag.Instance.GetShopItemPrice(byItemInternalId2[0].ID);
    return num1.CompareTo(num2);
  }

  private void OnJobCreated(long jobId)
  {
    JobHandle job = JobManager.Instance.GetJob(jobId);
    if (!(bool) job || job.GetJobEvent() != JobEvent.JOB_VIP)
      return;
    UIManager.inst.BackToPreDlg((UI.Dialog.DialogParameter) null, (UI.Dialog.DialogParameter) null);
  }

  private void ResetTabel()
  {
    this.scrollView.gameObject.SetActive(true);
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  private void OnRepositionHandler()
  {
    this.grid.onReposition -= new UIGrid.OnReposition(this.OnRepositionHandler);
    this.scrollView.ResetPosition();
  }

  private void CreateBoostListItem(List<ShopStaticInfo> shopInfo)
  {
    using (List<ShopStaticInfo>.Enumerator enumerator = shopInfo.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.CreateBoostListItem((ItemStaticInfo) null, enumerator.Current);
    }
  }

  private void CreateBoostListItem(ItemStaticInfo info, ShopStaticInfo shopInfo = null)
  {
    this.itemPrefab.gameObject.SetActive(true);
    VIPListItem vipListItem = this.pools.AddChild(this.grid.gameObject).AddMissingComponent<VIPListItem>();
    vipListItem.SetShopItem(shopInfo);
    vipListItem.SetItem(info, (System.Action) null);
    this.itemPrefab.gameObject.SetActive(false);
    this.items.Add(vipListItem);
  }

  private void Clear()
  {
    foreach (VIPListItem vipListItem in this.items)
    {
      vipListItem.gameObject.SetActive(false);
      vipListItem.Clear();
      this.pools.Release(vipListItem.gameObject);
    }
    this.items.Clear();
    JobManager.Instance.OnJobCreated -= new System.Action<long>(this.OnJobCreated);
  }
}
