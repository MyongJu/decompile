﻿// Decompiled with JetBrains decompiler
// Type: ConfigFallenKnightRankReward
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigFallenKnightRankReward
{
  private List<FallenKnightRankRewardInfo> rankRewardList = new List<FallenKnightRankRewardInfo>();
  private Dictionary<string, FallenKnightRankRewardInfo> datas;
  private Dictionary<int, FallenKnightRankRewardInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FallenKnightRankRewardInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Debug.Log((object) Utils.Object2Json(res));
    Dictionary<string, FallenKnightRankRewardInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.ID))
        this.rankRewardList.Add(enumerator.Current);
    }
    this.rankRewardList.Sort(new Comparison<FallenKnightRankRewardInfo>(this.SortByID));
  }

  public List<FallenKnightRankRewardInfo> GetRankRewardList()
  {
    return this.rankRewardList;
  }

  public FallenKnightRankRewardInfo GetFirstRankRewardInfo(ConfigFallenKnightRankReward.RankType type)
  {
    for (int index = 0; index < this.rankRewardList.Count; ++index)
    {
      if ((ConfigFallenKnightRankReward.RankType) this.rankRewardList[index].Type == type)
        return this.rankRewardList[index];
    }
    return (FallenKnightRankRewardInfo) null;
  }

  public int SortByID(FallenKnightRankRewardInfo a, FallenKnightRankRewardInfo b)
  {
    return this.GetRankRewardInfoId(a.ID).CompareTo(this.GetRankRewardInfoId(b.ID));
  }

  private int GetRankRewardInfoId(string id)
  {
    return int.Parse(id.Replace("activity_rank_reward_", string.Empty));
  }

  public enum RankType
  {
    AllianceRank,
    IndividualRank,
  }
}
