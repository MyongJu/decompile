﻿// Decompiled with JetBrains decompiler
// Type: AlliancMemeberScorePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class AlliancMemeberScorePopup : Popup
{
  public UIGrid grid;
  public UILabel title;
  public UIScrollView scrollView;
  public GameObject normtalItem;
  public GameObject backItem;
  private bool inited;
  private List<AlliancePlayerScoreData> scores;
  private ActivityBaseData data;
  private bool isDestory;

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.title.text = ScriptLocalization.Get("event_alliance_member_points_button", true);
    this.LoadData();
  }

  private void OnDestroy()
  {
    this.isDestory = true;
  }

  public void OnCloaseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  private void LoadData()
  {
    RequestManager.inst.SendRequest("activity:getAllianceMemberScore", (Hashtable) null, new System.Action<bool, object>(this.LoadStepRankDataCallBack), true);
  }

  private void LoadStepRankDataCallBack(bool success, object data)
  {
    if (this.isDestory)
      return;
    this.scores = new List<AlliancePlayerScoreData>();
    if (success)
    {
      ArrayList arrayList = data as ArrayList;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        AlliancePlayerScoreData alliancePlayerScoreData = new AlliancePlayerScoreData();
        if (alliancePlayerScoreData.Decode(arrayList[index]))
          this.scores.Add(alliancePlayerScoreData);
      }
      this.scores.Sort(new Comparison<AlliancePlayerScoreData>(this.Compare));
    }
    this.UpdatUI();
  }

  private int Compare(AlliancePlayerScoreData a, AlliancePlayerScoreData b)
  {
    if (a.Score > b.Score)
      return -1;
    return a.Score == b.Score ? 0 : 1;
  }

  public void UpdatUI()
  {
    if (this.inited || this.isDestory)
      return;
    this.ClearGrid();
    for (int index = 0; index < this.scores.Count; ++index)
    {
      AlliancMemeberScoreItem memeberScoreItem = this.GetItem(index);
      this.scores[index].Rank = index + 1;
      memeberScoreItem.SetData(this.scores[index]);
    }
    this.grid.Reposition();
    Utils.ExecuteInSecs(0.01f, (System.Action) (() => this.scrollView.ResetPosition()));
    this.inited = true;
  }

  private AlliancMemeberScoreItem GetItem(int index)
  {
    GameObject gameObject = NGUITools.AddChild(this.grid.gameObject, (index + 1) % 2 != 0 ? this.backItem : this.normtalItem);
    gameObject.SetActive(true);
    return gameObject.GetComponent<AlliancMemeberScoreItem>();
  }

  private void ClearGrid()
  {
    UIUtils.CleanGrid(this.grid);
  }

  public class Parameter : Popup.PopupParameter
  {
    public ActivityBaseData data;
  }
}
