﻿// Decompiled with JetBrains decompiler
// Type: RewardDetailPopUp
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class RewardDetailPopUp : Popup
{
  private List<Icon> items = new List<Icon>();
  private Dictionary<int, int> _items = new Dictionary<int, int>();
  public UILabel title;
  public UIButton claimBt;
  public UITable table;
  public Icon rewardItemPrefab;
  public UIScrollView scrollView;
  public UILabel normalTip;
  private int reward_id;
  private int XPRewrds;
  public System.Action onCloseHandler;

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    RewardDetailPopUp.Parameter parameter = orgParam as RewardDetailPopUp.Parameter;
    if (parameter == null)
      return;
    this.reward_id = parameter.reward_id;
    this.title.text = parameter.title;
    this.onCloseHandler = parameter.onCloseHandler;
    this._items = parameter.items;
    this.UpdateUI();
    this.AddEventHandler();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
    this.Clear();
    this.RemoveEventHandler();
  }

  public void OnCloseHandler()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
    if (this.onCloseHandler == null)
      return;
    this.onCloseHandler();
  }

  private void Clear()
  {
    List<Icon>.Enumerator enumerator = this.items.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.Dispose();
      NGUITools.SetActive(enumerator.Current.gameObject, false);
      UnityEngine.Object.Destroy((UnityEngine.Object) enumerator.Current.gameObject);
    }
    this.items.Clear();
  }

  private void AddEventHandler()
  {
  }

  private void RemoveEventHandler()
  {
  }

  private void UpdateUI()
  {
    this.CreateRewardItem();
    this.table.repositionNow = true;
    this.table.Reposition();
    this.scrollView.ResetPosition();
  }

  private void CreateRewardItem()
  {
    Dictionary<int, int>.KeyCollection.Enumerator enumerator = this._items.Keys.GetEnumerator();
    while (enumerator.MoveNext())
    {
      Icon icon = this.GetItem();
      IconData iconData = new IconData(ConfigManager.inst.DB_Items.GetItem(enumerator.Current).ImagePath, new string[1]
      {
        "X" + this._items[enumerator.Current].ToString()
      });
      iconData.Data = (object) enumerator.Current;
      icon.StopAutoFillName();
      icon.OnIconClickDelegate = new System.Action<Icon>(this.OnIconClick);
      icon.OnIconPressDelegate = new System.Action<Icon>(this.OnIconPress);
      icon.OnIconRelaseDelegate = new System.Action<Icon>(this.OnIconRelease);
      icon.FeedData((IComponentData) iconData);
    }
  }

  private void OnIconClick(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.ShowItemTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconPress(Icon icon)
  {
    IconData data = icon.data as IconData;
    if (data == null || data.Data == null)
      return;
    Utils.DelayShowTip((int) data.Data, icon.mBorder, 0L, 0L, 0);
  }

  private void OnIconRelease(Icon icon)
  {
    Utils.StopShowItemTip();
  }

  private Icon GetItem()
  {
    GameObject go = NGUITools.AddChild(this.table.gameObject, this.rewardItemPrefab.gameObject);
    Icon component = go.GetComponent<Icon>();
    this.items.Add(component);
    NGUITools.SetActive(go, true);
    return component;
  }

  public void OnClaimClickHandler()
  {
    this.OnCloseHandler();
  }

  public class Parameter : Popup.PopupParameter
  {
    public int reward_id;
    public string title;
    public System.Action onCloseHandler;
    public Dictionary<int, int> items;
  }
}
