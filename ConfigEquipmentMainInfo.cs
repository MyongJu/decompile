﻿// Decompiled with JetBrains decompiler
// Type: ConfigEquipmentMainInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

public class ConfigEquipmentMainInfo
{
  public const string ICON_PATH_PREFIX = "Texture/ItemIcons/";
  public const string FRAME_PATH_PREFIX = "Texture/Equipment/frame_equipment_";
  public const string CATEGORY_PATH_PREFIX = "Texture/Equipment/icon_equipment_";
  public int internalId;
  [Config(Name = "id")]
  public string ID;
  [Config(Name = "item_id")]
  public int itemID;
  [Config(Name = "type")]
  public int type;
  [Config(Name = "quality")]
  public int quality;
  [Config(Name = "hero_level_min")]
  public int heroLevelMin;
  [Config(Name = "tendency")]
  public int tendency;
  [Config(Name = "name")]
  public string name;
  [Config(Name = "image")]
  public string image;
  [Config(Name = "suit_group")]
  public int suitGroup;
  [Config(Name = "gem_slot_param")]
  public string gemSlotParam;
  private GemSlotConditions gemSlotConditions;

  public GemSlotConditions GemSlotConditions
  {
    get
    {
      if (this.gemSlotConditions == null)
      {
        this.gemSlotConditions = new GemSlotConditions();
        this.gemSlotConditions.Parse(this.gemSlotParam);
      }
      return this.gemSlotConditions;
    }
  }

  public static string GetCategoryPath(HeroItemType itemType, BagType bagType)
  {
    if (bagType == BagType.DragonKnight && itemType == HeroItemType.RING)
      return "Texture/Equipment/icon_equipment_9";
    return "Texture/Equipment/icon_equipment_" + (object) itemType;
  }

  public string ImagePath
  {
    get
    {
      return ConfigManager.inst.DB_Items.GetItem(this.itemID).ImagePath;
    }
  }

  public string QualityImagePath
  {
    get
    {
      return "Texture/Equipment/frame_equipment_" + (object) this.quality;
    }
  }

  public string LocNameId
  {
    get
    {
      return ConfigManager.inst.DB_Items.GetItem(this.itemID).Loc_Name_Id;
    }
  }

  public string LocName
  {
    get
    {
      return ConfigManager.inst.DB_Items.GetItem(this.itemID).LocName;
    }
  }

  public string GetQualityImagePathByPrefix(string prefix)
  {
    return "Texture/Equipment/" + prefix + "frame_equipment_" + (object) this.quality;
  }
}
