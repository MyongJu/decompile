﻿// Decompiled with JetBrains decompiler
// Type: VIPDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class VIPDlg : UI.Dialog
{
  public Dictionary<int, VIPStatEntry> statBonuses = new Dictionary<int, VIPStatEntry>();
  private Dictionary<int, VIPLevelUIEntry> vipLevelUIEntries = new Dictionary<int, VIPLevelUIEntry>();
  public UIProgressBar xpProgress;
  public UILabel xpProgressLabel;
  public UIProgressBar vipRemainingProgress;
  public UILabel vipRemainingLabel;
  public UILabel vipOnLabel;
  public UIButton vipOnButton;
  public UITable vipLevelTable;
  public GameObject vipLevelTemplate;
  public UILabel streakCount;
  public UILabel pointsEarnedTomorrow;
  public UITable statTable;
  public GameObject statBonusTemplate;
  private int selectedVIPEntry;
  private bool shown;

  public void RenderUI()
  {
    this.ClearVipUIEntries();
    Dictionary<int, VIP_Level_Info> levelDatas = ConfigManager.inst.DB_VIP_Level.levelDatas;
    for (int vipLevel = PlayerData.inst.hostPlayer.VIPLevel; vipLevel <= levelDatas.Count; ++vipLevel)
    {
      GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.vipLevelTemplate, this.vipLevelTable.gameObject.transform);
      VIPLevelUIEntry component = gameObject.GetComponent<VIPLevelUIEntry>();
      component.parentDlg = this;
      component.SetActiveState(vipLevel == PlayerData.inst.hostPlayer.VIPLevel);
      component.SetHighlighted(false);
      component.SetLevel(vipLevel);
      this.vipLevelUIEntries.Add(vipLevel, component);
      gameObject.SetActive(true);
    }
    this.vipLevelTable.repositionNow = true;
    this.UpdateUI();
  }

  public void UpdateUI()
  {
    int vipPoints = PlayerData.inst.hostPlayer.VIPPoints;
    int vipLevel = PlayerData.inst.hostPlayer.VIPLevel;
    if (vipLevel < ConfigManager.inst.DB_VIP_Level.levelDatas.Count)
      ++vipLevel;
    int reqValue1 = ConfigManager.inst.DB_VIP_Level.levelDatas[vipLevel].req_value_1;
    this.xpProgressLabel.text = string.Format("{0} / {1}", (object) vipPoints, (object) reqValue1);
    this.xpProgress.value = vipPoints >= reqValue1 ? 1f : (float) vipPoints / (float) reqValue1;
    for (int key = PlayerData.inst.hostPlayer.VIPLevel - 1; this.vipLevelUIEntries.ContainsKey(key); --key)
      this.DestroyVIPUIEntry(key);
    if (this.selectedVIPEntry < PlayerData.inst.hostPlayer.VIPLevel)
      this.OnClickLevelEntry(PlayerData.inst.hostPlayer.VIPLevel);
    this.vipLevelUIEntries[PlayerData.inst.hostPlayer.VIPLevel].SetActiveState(true);
    this.streakCount.text = PlayerData.inst.hostPlayer.ConsecutiveLoginDays.ToString();
    this.pointsEarnedTomorrow.text = PlayerData.inst.hostPlayer.NextLoginBonus.ToString();
    this.UpdateTimer();
  }

  public void Update()
  {
    if (!this.shown)
      return;
    this.UpdateTimer();
  }

  public void OnClickClose()
  {
    this.shown = false;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public void OnClickInfo()
  {
    Debug.LogWarning((object) "Info clicked");
  }

  public void OnClickLevelEntry(int level)
  {
    if (this.vipLevelUIEntries.ContainsKey(this.selectedVIPEntry))
      this.vipLevelUIEntries[this.selectedVIPEntry].SetHighlighted(false);
    this.selectedVIPEntry = level;
    this.vipLevelUIEntries[this.selectedVIPEntry].SetHighlighted(true);
    List<Effect>.Enumerator enumerator = ConfigManager.inst.DB_VIP_Level.levelDatas[level].Effects.GetEnumerator();
    while (enumerator.MoveNext())
    {
      int propertyInternalId = enumerator.Current.PropertyInternalID;
      if (!this.statBonuses.ContainsKey(propertyInternalId))
      {
        GameObject gameObject = PrefabManagerEx.Instance.Spawn(this.statBonusTemplate, this.statTable.gameObject.transform);
        VIPStatEntry component = gameObject.GetComponent<VIPStatEntry>();
        this.statTable.repositionNow = true;
        this.statBonuses.Add(propertyInternalId, component);
        gameObject.SetActive(true);
      }
      this.statBonuses[propertyInternalId].SetData(enumerator.Current);
    }
  }

  public void OnClickVIPPointsNow()
  {
    GameEngine.Instance.events.Publish_onCityHUDResourceClicked(CityManager.ResourceTypes.GOLD);
  }

  public void OnClickTurnOnVip()
  {
    UIManager.inst.OpenDlg("ItemStore/ItemStoreDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  private void OnPlayerVipChanged(Events.PlayerVIPStatusChanged obj)
  {
    this.UpdateUI();
  }

  private void OnPlayerStatChanged(Events.PlayerStatChangedArgs obj)
  {
    if (obj.Stat != Events.PlayerStat.VIP)
      return;
    this.UpdateUI();
  }

  private void RegisterEventListeners()
  {
    GameEngine.Instance.events.onPlayerStatChanged += new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
    GameEngine.Instance.events.onPlayerVipStatusChanged += new System.Action<Events.PlayerVIPStatusChanged>(this.OnPlayerVipChanged);
  }

  private void UnregisterEventListeners()
  {
    GameEngine.Instance.events.onPlayerStatChanged -= new System.Action<Events.PlayerStatChangedArgs>(this.OnPlayerStatChanged);
    GameEngine.Instance.events.onPlayerVipStatusChanged -= new System.Action<Events.PlayerVIPStatusChanged>(this.OnPlayerVipChanged);
  }

  private void ClearVipUIEntries()
  {
    Dictionary<int, VIPLevelUIEntry>.ValueCollection.Enumerator enumerator = this.vipLevelUIEntries.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      enumerator.Current.gameObject.name = this.vipLevelTemplate.name;
      PrefabManagerEx.Instance.Destroy(enumerator.Current.gameObject);
    }
    this.vipLevelUIEntries.Clear();
  }

  private void DestroyVIPUIEntry(int key)
  {
    if (!this.vipLevelUIEntries.ContainsKey(key))
      return;
    this.vipLevelUIEntries[key].gameObject.name = this.vipLevelTemplate.name;
    PrefabManagerEx.Instance.Destroy(this.vipLevelUIEntries[key].gameObject);
    this.vipLevelUIEntries.Remove(key);
    this.vipLevelTable.repositionNow = true;
  }

  private void UpdateTimer()
  {
    if (PlayerData.inst.hostPlayer.VIPActive)
    {
      JobHandle job = JobManager.Instance.GetJob(PlayerData.inst.hostPlayer.VipJobId);
      int num1 = job.StartTime();
      int num2 = job.EndTime();
      float num3 = (float) (num2 - num1);
      this.vipRemainingProgress.value = (float) (num2 - NetServerTime.inst.ServerTimestamp) / num3;
      this.vipOnLabel.text = ScriptLocalization.Get("vip_on", true);
      this.vipRemainingLabel.text = Utils.ConvertSecsToString((double) (num2 - NetServerTime.inst.ServerTimestamp));
    }
    else
    {
      this.vipRemainingProgress.value = 0.0f;
      this.vipOnLabel.text = ScriptLocalization.Get("vip_off", true);
      this.vipRemainingLabel.text = Utils.ConvertSecsToString(0.0);
    }
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this.shown = true;
    this.RegisterEventListeners();
    this.RenderUI();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.shown = true;
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.shown = false;
    this.UnregisterEventListeners();
    this.ClearVipUIEntries();
  }
}
