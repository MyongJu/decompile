﻿// Decompiled with JetBrains decompiler
// Type: LordTitleBenefitSolt
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LordTitleBenefitSolt : MonoBehaviour
{
  [SerializeField]
  private UILabel _labelBenefitName;
  [SerializeField]
  private UILabel _labelBenefitValue;
  private int _benefitId;
  private float _benefitValue;

  public void SetData(int benefitId, float benefitValue)
  {
    this._benefitId = benefitId;
    this._benefitValue = benefitValue;
    this.UpdateUI();
  }

  private void UpdateUI()
  {
    PropertyDefinition dbProperty = ConfigManager.inst.DB_Properties[this._benefitId];
    string str1 = dbProperty == null ? "unknow" : dbProperty.Name;
    string str2 = dbProperty == null ? this._benefitValue.ToString() : dbProperty.ConvertToDisplayString((double) this._benefitValue, false, true);
    this._labelBenefitName.text = str1;
    this._labelBenefitValue.text = str2;
  }
}
