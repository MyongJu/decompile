﻿// Decompiled with JetBrains decompiler
// Type: ConfigFallenKnightMain
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class ConfigFallenKnightMain
{
  private List<FallenKnightMainInfo> mainInfoList = new List<FallenKnightMainInfo>();
  private Dictionary<string, FallenKnightMainInfo> datas;
  private Dictionary<int, FallenKnightMainInfo> dicByUniqueId;

  public void BuildDB(object res)
  {
    new ConfigParse().Parse<FallenKnightMainInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Dictionary<string, FallenKnightMainInfo>.ValueCollection.Enumerator enumerator = this.datas.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current != null && !string.IsNullOrEmpty(enumerator.Current.ID))
        this.mainInfoList.Add(enumerator.Current);
    }
    this.mainInfoList.Sort(new Comparison<FallenKnightMainInfo>(this.SortByID));
  }

  public List<FallenKnightMainInfo> GetFallenKnightMainList()
  {
    return this.mainInfoList;
  }

  public int SortByID(FallenKnightMainInfo a, FallenKnightMainInfo b)
  {
    return int.Parse(a.ID).CompareTo(int.Parse(b.ID));
  }
}
