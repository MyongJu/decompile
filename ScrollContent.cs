﻿// Decompiled with JetBrains decompiler
// Type: ScrollContent
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ScrollContent : MonoBehaviour
{
  public EquipmentForgeContent content;
  public GameObject empty;
  private List<ConfigEquipmentScrollInfo> currentList;
  private BagType bagType;

  public void Init(BagType bagType)
  {
    this.bagType = bagType;
    List<ConsumableItemData> consumableScrollItems = ItemBag.Instance.GetConsumableScrollItems(bagType);
    this.currentList = new List<ConfigEquipmentScrollInfo>();
    for (int index = 0; index < consumableScrollItems.Count; ++index)
      this.currentList.Add(ConfigManager.inst.GetEquipmentScroll(bagType).GetDataByItemID(consumableScrollItems[index].internalId));
    this.Sort(this.currentList);
    this.empty.SetActive(this.currentList.Count == 0);
    this.content.gameObject.SetActive(this.currentList.Count > 0);
    if (this.currentList.Count > 0)
      this.content.Init(this.bagType, this.currentList, false, 0);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved += new System.Action<int>(this.OnItemDataChanged);
  }

  public void ViewScroll(int targetSelectInfoID)
  {
    this.content.Init(this.bagType, this.currentList, false, targetSelectInfoID);
  }

  private void OnItemDataChanged(int itemID)
  {
    this.Init(this.bagType);
  }

  public void Dispose()
  {
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnItemDataChanged);
    DBManager.inst.DB_Item.onDataRemoved -= new System.Action<int>(this.OnItemDataChanged);
    this.content.Dispose();
  }

  public void OnGogoForgeClick()
  {
    InventoryItemData itemByInternalId = DBManager.inst.GetInventory(this.bagType).GetMyItemByInternalID((long) this.content.current.scrollInfo.equipmenID);
    if (itemByInternalId != null)
      UIManager.inst.OpenDlg(this.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentEnhanceDialog" : "Equipment/EquipmentEnhanceDlg", (UI.Dialog.DialogParameter) new EquipmentEnhanceDlg.Parameter()
      {
        bagType = this.bagType,
        selectedEquipmentItemID = itemByInternalId.itemId
      }, 1 != 0, 1 != 0, 1 != 0);
    else if (EquipmentManager.Instance.HasUnCollectedEquipment(this.bagType))
      UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    else
      UIManager.inst.OpenDlg(this.bagType != BagType.Hero ? "Equipment/DragonKnightEquipmentForgeDialog" : "Equipment/EquipmentForgeDlg", (UI.Dialog.DialogParameter) new EquipmentForgeDlg.Parameter()
      {
        bagType = this.bagType,
        currentSelectInfoID = this.content.current.scrollInfo.internalId
      }, 1 != 0, 1 != 0, 1 != 0);
  }

  public void OnSellClick()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.content.current.scrollInfo.itemID);
    (UIManager.inst.OpenPopup("SellScrollPopup", (Popup.PopupParameter) new SellItemBasePopup.Parameter()
    {
      bagType = this.bagType,
      itemInfo = itemStaticInfo
    }) as SellScrollPopup).UpdateScrollInfo(this.content.current.scrollInfo);
  }

  private void Sort(List<ConfigEquipmentScrollInfo> list)
  {
    list.Sort((Comparison<ConfigEquipmentScrollInfo>) ((x, y) =>
    {
      ConfigEquipmentMainInfo data1 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(x.equipmenID);
      ConfigEquipmentMainInfo data2 = ConfigManager.inst.GetEquipmentMain(this.bagType).GetData(y.equipmenID);
      if (data1.type != data2.type)
        return data1.type.CompareTo(data2.type);
      if (data1.quality != data2.quality)
        return data2.quality.CompareTo(data1.quality);
      if (data1.heroLevelMin == data2.heroLevelMin)
        return data1.tendency.CompareTo(data2.tendency);
      return data2.heroLevelMin.CompareTo(data1.heroLevelMin);
    }));
  }
}
