﻿// Decompiled with JetBrains decompiler
// Type: KingdomTouchCircle
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using System.Text;
using UI;
using UnityEngine;

public class KingdomTouchCircle : SceneUI
{
  public float radius = 200f;
  public float openTime = 1f;
  private int maxBtn = 6;
  private KingdomCirclePara para = new KingdomCirclePara();
  public float angleRetain = 100f;
  private static KingdomTouchCircle _instance;
  public UILabel lblCoord;
  public UILabel lblResCount;
  public UISprite usResType;
  public UILabel lblAllianceName;
  public UILabel lblType;
  public UILabel lblTargetPos;
  public KingdomTouchCircleBtn btnTemplate;
  public GameObject titleGroup;
  public GameObject tileG;
  public GameObject ownerG;
  public UILabel owner;
  public GameObject troopG;
  public UISprite bookmarkIcon;
  public GameObject addButton;
  public GameObject editButton;
  private List<KingdomTouchCircleBtn> btnList;
  private List<Vector3> btnTarPos;
  private Vector3 corOldPos;

  public static KingdomTouchCircle Instance
  {
    get
    {
      if ((UnityEngine.Object) KingdomTouchCircle._instance == (UnityEngine.Object) null)
      {
        KingdomTouchCircle._instance = (UnityEngine.Object.Instantiate(AssetManager.Instance.HandyLoad("Prefab/Kingdom/KingdomTouchCircle", (System.Type) null)) as GameObject).GetComponent<KingdomTouchCircle>();
        if ((UnityEngine.Object) null == (UnityEngine.Object) KingdomTouchCircle._instance)
          throw new ArgumentException("lost of KingdomTouchCircle");
        KingdomTouchCircle._instance.Init();
      }
      return KingdomTouchCircle._instance;
    }
  }

  public static bool IsAvailable
  {
    get
    {
      return (bool) ((UnityEngine.Object) KingdomTouchCircle._instance);
    }
  }

  public KingdomCirclePara Para
  {
    get
    {
      return this.para;
    }
    set
    {
      this.para = value;
    }
  }

  public void Init()
  {
    this.btnList = new List<KingdomTouchCircleBtn>(this.maxBtn);
    for (int index = 0; index < this.maxBtn; ++index)
    {
      KingdomTouchCircleBtn kingdomTouchCircleBtn = UnityEngine.Object.Instantiate<KingdomTouchCircleBtn>(this.btnTemplate);
      kingdomTouchCircleBtn.transform.parent = this.btnTemplate.transform.parent;
      kingdomTouchCircleBtn.transform.localScale = this.btnTemplate.transform.localScale;
      this.btnList.Add(kingdomTouchCircleBtn);
    }
    this.Reset();
    this.btnTemplate.gameObject.SetActive(false);
    this.corOldPos = this.lblCoord.transform.localPosition;
    this.gameObject.SetActive(false);
    KingdomCircleBtnCollection.Instance.Create();
  }

  private void UpdateTileBookmarkButton()
  {
    if (!GameEngine.IsAvailable || GameEngine.IsShuttingDown || (!GameEngine.IsReady() || this.Para.mode != CircleMode.Tile))
      return;
    Bookmark bookmark = DBManager.inst.DB_Bookmark.Get(this.Para.tData.Location);
    this.bookmarkIcon.spriteName = bookmark != null ? Utils.GetIconSpriteName(bookmark.type) : "icon_bookmark";
    if (GameEngine.Instance.CurrentGameMode == GameEngine.GameMode.PitMode)
    {
      this.addButton.SetActive(false);
      this.editButton.SetActive(false);
      this.bookmarkIcon.transform.parent.gameObject.SetActive(false);
    }
    else
    {
      this.addButton.SetActive(bookmark == null);
      this.editButton.SetActive(bookmark != null);
      this.bookmarkIcon.transform.parent.gameObject.SetActive(true);
    }
  }

  private bool UpdateData()
  {
    if (this.Para == null || this.Para.mode == CircleMode.Tile && this.Para.tList.Count == 0)
    {
      this.Close();
      return false;
    }
    this.btnTarPos = new List<Vector3>(this.Para.tList.Count);
    for (int index = 0; index < this.Para.tList.Count && index < this.btnList.Count; ++index)
    {
      this.btnTarPos.Add(this.btnList[index].transform.localPosition);
      this.btnList[index].gameObject.SetActive(true);
      this.btnList[index].GetComponent<KingdomTouchCircleBtn>().Init(this.Para.tList[index]);
    }
    if (this.Para.tList.Count > 0)
      this.CalcBtnPosition();
    if (this.Para.mode == CircleMode.Tile)
    {
      this.UpdateTileBookmarkButton();
      this.tileG.SetActive(true);
      this.troopG.SetActive(false);
      this.lblCoord.text = "X:" + (object) this.Para.tData.Location.X + ",Y:" + (object) this.Para.tData.Location.Y;
      this.UpdateResourceData();
    }
    else if (this.Para.mode == CircleMode.Troop)
    {
      this.tileG.SetActive(false);
      this.troopG.SetActive(true);
      AllianceData allianceData = DBManager.inst.DB_Alliance.Get(this.Para.marchData.ownerAllianceId);
      StringBuilder stringBuilder = new StringBuilder();
      if (allianceData != null)
        stringBuilder.Append("(" + allianceData.allianceAcronym + ")");
      stringBuilder.Append(this.Para.marchData.ownerUserName);
      this.lblAllianceName.text = ScriptLocalization.Get(stringBuilder.ToString(), true);
      if (this.Para.marchData.IsAttack)
        this.lblType.text = ScriptLocalization.Get("time_bar_marching", true);
      if (this.Para.marchData.IsEncamp)
        this.lblType.text = ScriptLocalization.Get("time_bar_uppercase_encamping", true);
      if (this.Para.marchData.IsReturn)
        this.lblType.text = ScriptLocalization.Get("time_bar_uppercase_returning", true);
      Coordinate targetLocation = this.Para.marchData.targetLocation;
      this.lblTargetPos.text = "[u]" + ScriptLocalization.Get("id_to", true) + " X:" + (object) targetLocation.X + ", Y:" + (object) targetLocation.Y + "[/u]";
    }
    return true;
  }

  public void OnCoordClickHandler()
  {
    Debug.Log((object) "goto");
    PVPSystem.Instance.Map.GotoLocation(this.Para.marchData.targetLocation, false);
  }

  private void UpdateResourceData()
  {
    if (this.para.tData == null)
      return;
    if (this.para.tData.TileType >= TileType.Resource1 && this.para.tData.TileType <= TileType.Resource4 || this.para.tData.TileType == TileType.PitMine)
    {
      this.usResType.gameObject.SetActive(true);
      this.lblResCount.gameObject.SetActive(true);
      switch (this.para.tData.TileType)
      {
        case TileType.Resource1:
          this.usResType.spriteName = "food_icon";
          break;
        case TileType.Resource2:
          this.usResType.spriteName = "wood_icon";
          break;
        case TileType.Resource3:
          this.usResType.spriteName = "ore_icon";
          break;
        case TileType.Resource4:
          this.usResType.spriteName = "silver_icon";
          break;
        case TileType.PitMine:
          this.usResType.spriteName = "dragon_fossil_icon";
          break;
      }
      this.lblResCount.text = Utils.FormatThousands(this.para.tData.Value.ToString());
      this.lblCoord.transform.localPosition = this.corOldPos;
      if (this.para.tData.OwnerID != PlayerData.inst.uid && this.para.tData.OwnerID != 0L)
      {
        this.ownerG.SetActive(true);
        UserData userData = DBManager.inst.DB_User.Get(this.para.tData.OwnerID);
        string str = (string) null;
        if (userData.allianceId != 0L)
        {
          AllianceData allianceData = DBManager.inst.DB_Alliance.Get(userData.allianceId);
          if (allianceData != null)
            str = allianceData.allianceAcronym;
        }
        this.owner.text = str != null ? "(" + str + ")" + userData.userName : userData.userName;
      }
      else
        this.ownerG.SetActive(false);
    }
    else if (this.para.tData.TileType == TileType.AllianceFortress)
    {
      this.usResType.gameObject.SetActive(true);
      this.lblResCount.gameObject.SetActive(true);
      this.usResType.spriteName = "icon_fort_guard";
      this.lblResCount.text = Utils.FormatThousands(AllianceBuildUtils.CalcCurrentFortressDurabilityByFortressData(this.para.tData.FortressData).ToString());
      this.lblCoord.transform.localPosition = this.corOldPos;
      this.ownerG.SetActive(false);
    }
    else if (this.para.tData.TileType == TileType.AllianceWarehouse)
    {
      this.usResType.gameObject.SetActive(true);
      this.lblResCount.gameObject.SetActive(true);
      this.usResType.spriteName = "icon_fort_guard";
      this.lblResCount.text = Utils.FormatThousands(this.para.tData.WareHouseData.Durability.ToString());
      this.lblCoord.transform.localPosition = this.corOldPos;
      this.ownerG.SetActive(false);
    }
    else if (this.para.tData.TileType == TileType.AllianceSuperMine)
    {
      this.usResType.gameObject.SetActive(true);
      this.lblResCount.gameObject.SetActive(true);
      this.ownerG.SetActive(false);
      this.lblCoord.transform.localPosition = this.corOldPos;
      if (this.para.tData.SuperMineData.CurrentState == AllianceSuperMineData.State.COMPLETE)
      {
        int configId = this.para.tData.SuperMineData.ConfigId;
        this.lblResCount.text = Utils.FormatThousands(AllianceBuildUtils.GetSuperMineGatheredResources(configId).ToString());
        this.usResType.spriteName = ConfigManager.inst.DB_AllianceBuildings.Get(configId).ResourceType + "_icon";
      }
      else
      {
        this.usResType.spriteName = "icon_fort_guard";
        this.lblResCount.text = Utils.FormatThousands(this.para.tData.SuperMineData.Durability.ToString());
      }
    }
    else if (this.para.tData.TileType == TileType.AllianceHospital)
    {
      this.usResType.gameObject.SetActive(true);
      this.lblResCount.gameObject.SetActive(true);
      this.usResType.spriteName = "icon_fort_guard";
      this.lblResCount.text = Utils.FormatThousands(this.para.tData.HospitalData.Durability.ToString());
      this.lblCoord.transform.localPosition = this.corOldPos;
      this.ownerG.SetActive(false);
    }
    else
    {
      this.usResType.gameObject.SetActive(false);
      this.lblResCount.gameObject.SetActive(false);
      this.lblCoord.transform.localPosition = this.corOldPos + new Vector3(0.0f, 10f, 0.0f);
      this.ownerG.SetActive(false);
    }
  }

  private void UpdateGatherProgress()
  {
    if (this.para.mode != CircleMode.Tile)
      return;
    if (this.para.tData.TileType >= TileType.Resource1 && this.para.tData.TileType <= TileType.Resource4 || this.para.tData.TileType == TileType.PitMine)
    {
      MarchData marchData = DBManager.inst.DB_March.Get(this.para.tData.MarchID);
      if (marchData == null)
        return;
      JobHandle job = JobManager.Instance.GetJob(marchData.jobId);
      if (job == null || job.IsFinished())
        return;
      int num1 = Math.Min(marchData.CalculateTroopLoad(), this.para.tData.Value);
      int num2 = job.Duration();
      float num3 = (float) (1.0 - (double) job.LeftTime() / (double) num2);
      this.lblResCount.text = Utils.FormatThousands(((int) ((double) this.para.tData.Value - (double) num1 * (double) num3)).ToString());
    }
    else
    {
      if (this.para.tData.TileType != TileType.AllianceSuperMine || this.para.tData.SuperMineData == null || this.para.tData.SuperMineData.CurrentState != AllianceSuperMineData.State.COMPLETE)
        return;
      if (this.para.tData.SuperMineData.AllianceId == PlayerData.inst.allianceId)
      {
        this.lblResCount.text = Utils.FormatThousands(AllianceBuildUtils.GetSuperMineRemainingResources(this.para.tData.SuperMineData.ConfigId).ToString());
      }
      else
      {
        long num = this.para.tData.SuperMineData.Amount;
        AllianceSuperMineData asmd = DBManager.inst.DB_AllianceSuperMine.Get((long) this.para.tData.ResourceId);
        if (asmd != null)
          num = asmd.Amount - AllianceBuildUtils.GetSuperMineTotalGatheredResources(asmd);
        this.lblResCount.text = Utils.FormatThousands(num <= 0L ? "0" : num.ToString());
      }
    }
  }

  private void CalcBtnPosition()
  {
    int count = this.btnTarPos.Count;
    if (count == 1)
      this.btnTarPos[0] = new Vector3(0.0f, this.radius, 0.0f);
    else if (count < 6)
    {
      this.btnTarPos[0] = new Vector3(-this.radius, 0.0f, 0.0f);
      this.btnTarPos[count - 1] = new Vector3(this.radius, 0.0f, 0.0f);
      float num = (float) (180.0 / (double) (count - 1) / 360.0 * 2.0 * 3.14159274101257);
      for (int index = 1; index < count - 1; ++index)
      {
        float x = -Mathf.Cos(num * (float) index) * this.radius;
        float y = Mathf.Sin(num * (float) index) * this.radius;
        this.btnTarPos[index] = new Vector3(x, y);
      }
    }
    else
    {
      float num1 = (360f - this.angleRetain) / (float) (count - 1);
      float num2 = (float) (-(180.0 - (double) this.angleRetain) / 2.0);
      for (int index = 0; index < count; ++index)
      {
        float x = -Mathf.Cos((float) ((double) num2 / 360.0 * 2.0 * 3.14159274101257)) * this.radius;
        float y = Mathf.Sin((float) ((double) num2 / 360.0 * 2.0 * 3.14159274101257)) * this.radius;
        this.btnTarPos[index] = new Vector3(x, y);
        num2 += num1;
      }
    }
  }

  private void PlayBtnAnimation()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1, (object) "time", (object) this.openTime, (object) "easetype", (object) iTween.EaseType.easeOutBack, (object) "onupdate", (object) "CircleAnimation", (object) "oncomplete", (object) "OnAnimationComplete"));
  }

  private void OnAnimationComplete()
  {
    this.CircleAnimation(1f);
  }

  private void CircleAnimation(float value)
  {
    for (int index = this.btnTarPos.Count - 1; index >= 0; --index)
    {
      GameObject gameObject = this.btnList[index].gameObject;
      gameObject.SetActive(true);
      gameObject.transform.localPosition = this.btnTarPos[index] * value;
    }
    this.titleGroup.transform.localPosition = new Vector3(0.0f, -this.radius * value, 1f);
  }

  private void ForceToTargetPlace()
  {
    for (int index = 0; index < this.btnTarPos.Count; ++index)
      this.btnList[index].gameObject.transform.localPosition = this.btnTarPos[index];
    this.titleGroup.transform.localPosition = new Vector3(0.0f, -this.radius);
  }

  private void Reset()
  {
    for (int index = 0; index < this.btnList.Count; ++index)
    {
      this.btnList[index].transform.localPosition = Vector3.zero;
      this.btnList[index].gameObject.SetActive(false);
    }
    this.titleGroup.transform.localPosition = Vector3.zero;
    if (this.btnTarPos != null)
      this.btnTarPos.Clear();
    if (this.Para == null)
      return;
    this.Para.tList.Clear();
  }

  public void OnBookmarkButtonPressed()
  {
    TileData referenceAt = PVPMapData.MapData.GetReferenceAt(this.Para.tData.Location);
    Bookmark bookmark = DBManager.inst.DB_Bookmark.Get(referenceAt.Location);
    if (bookmark != null)
      UIManager.inst.OpenPopup("Bookmark/BookmarkUpdatePopup", (Popup.PopupParameter) new BookmarkUpdatePopup.Parameter()
      {
        bookmark = bookmark
      });
    else
      UIManager.inst.OpenPopup("Bookmark/BookmarkCreatePopup", (Popup.PopupParameter) new BookmarkCreatePopup.Parameter()
      {
        tileData = referenceAt
      });
  }

  public override void Open(Transform parent)
  {
    base.Open(parent);
    if (this.UpdateData())
      this.PlayBtnAnimation();
    this.UpdateUIInfo(0);
    this.AddEventHandler();
  }

  public override void Close()
  {
    base.Close();
    this.Reset();
    if (PVPSystem.Instance.IsAvailable && (UnityEngine.Object) PVPSystem.Instance.Map != (UnityEngine.Object) null)
      PVPSystem.Instance.Map.HideHighlight();
    this.RemoveEventHandle();
  }

  private void Start()
  {
  }

  private void Update()
  {
    if (this.Para.mode == CircleMode.Troop)
      UIManager.inst.tileCamera.SetTargetLookPosition(this.transform.position, false);
    this.ScaleInKingdom();
  }

  private void AddEventHandler()
  {
    Oscillator.Instance.secondEvent += new System.Action<int>(this.UpdateUIInfo);
  }

  private void RemoveEventHandle()
  {
    if (!Oscillator.IsAvailable)
      return;
    Oscillator.Instance.secondEvent -= new System.Action<int>(this.UpdateUIInfo);
  }

  private void UpdateUIInfo(int time)
  {
    this.UpdateTileBookmarkButton();
    this.UpdateResourceData();
    this.UpdateGatherProgress();
  }
}
