﻿// Decompiled with JetBrains decompiler
// Type: OtherReportPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UI;
using UnityEngine;

public class OtherReportPopup : BaseReportPopup
{
  private List<GameObject> resourcesList = new List<GameObject>();
  private Dictionary<MailType, string> texturePath = new Dictionary<MailType, string>()
  {
    {
      MailType.MAIL_TYPE_PVP_BE_SCOUTED_REPORT,
      "Texture/GUI_Textures/mail_report_scoute"
    }
  };
  public UILabel contentLabel;
  public UILabel bottomButtonLabel;
  public UIButton bottomButton;
  private WarMessage message;

  private void Init()
  {
    string str = "Texture/GUI_Textures/gather_report_header";
    if (this.texturePath.ContainsKey(this.maildata.type))
      str = this.texturePath[this.maildata.type];
    BuilderFactory.Instance.HandyBuild((UIWidget) this.headerTexture, "Texture/GUI_Textures/gather_report_header", (System.Action<bool>) null, false, false, string.Empty);
    this.contentLabel.text = this.maildata.GetBodyString();
    this.bottomButton.gameObject.SetActive(false);
    if (this.maildata.type != MailType.MAIL_TYPE_ALLIANCE_AC_NOTICE)
      return;
    this.bottomButtonLabel.text = Utils.XLAT("event_center_name");
    this.bottomButton.gameObject.SetActive(this.IsAllianceWarSpecialMail);
  }

  public void OnBottomButtonClicked()
  {
    if (this.maildata.type != MailType.MAIL_TYPE_ALLIANCE_AC_NOTICE)
      return;
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    if (ActivityManager.Intance.allianceWar.IsEmpty())
      UIManager.inst.publicHUD.OnActivityBtPressed();
    else if (PlayerData.inst.allianceData == null)
      UIManager.inst.toast.Show(Utils.XLAT("toast_alliance_warfare_no_alliance"), (System.Action) null, 4f, false);
    else
      UIManager.inst.OpenDlg("Alliance/AllianceWarDlg", (UI.Dialog.DialogParameter) new AllianceWarMainDlg.Parameter()
      {
        allianceWarData = ActivityManager.Intance.allianceWar
      }, true, true, true);
  }

  public void OnCloseBtnClick()
  {
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.Init();
  }

  private bool IsAllianceWarSpecialMail
  {
    get
    {
      string empty = string.Empty;
      if (this.maildata.HtTitle != null && this.maildata.HtTitle.ContainsKey((object) "key") && this.maildata.HtTitle[(object) "key"] != null)
        empty = this.maildata.HtTitle[(object) "key"].ToString();
      return empty == "mail_title_alliance_warfare_registered" || empty == "mail_title_alliance_warfare_begun" || (empty == "mail_title_alliance_warfare_opponents" || empty == "mail_title_alliance_warfare_finished");
    }
  }
}
