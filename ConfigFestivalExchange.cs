﻿// Decompiled with JetBrains decompiler
// Type: ConfigFestivalExchange
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

public class ConfigFestivalExchange
{
  private ConfigParse parse = new ConfigParse();
  private Dictionary<int, FestivalExchangeInfo> dicByUniqueId;
  public Dictionary<string, FestivalExchangeInfo> datas;

  public void BuildDB(object res)
  {
    this.parse.Parse<FestivalExchangeInfo, string>(res as Hashtable, "ID", out this.datas, out this.dicByUniqueId);
    Dictionary<int, FestivalExchangeInfo>.Enumerator enumerator = this.dicByUniqueId.GetEnumerator();
    while (enumerator.MoveNext())
    {
      FestivalExchangeInfo festivalExchangeInfo = enumerator.Current.Value;
      festivalExchangeInfo.refreshCosts = new int[10];
      festivalExchangeInfo.refreshCosts[0] = festivalExchangeInfo.refreshCost0;
      festivalExchangeInfo.refreshCosts[1] = festivalExchangeInfo.refreshCost1;
      festivalExchangeInfo.refreshCosts[2] = festivalExchangeInfo.refreshCost2;
      festivalExchangeInfo.refreshCosts[3] = festivalExchangeInfo.refreshCost3;
      festivalExchangeInfo.refreshCosts[4] = festivalExchangeInfo.refreshCost4;
      festivalExchangeInfo.refreshCosts[5] = festivalExchangeInfo.refreshCost5;
      festivalExchangeInfo.refreshCosts[6] = festivalExchangeInfo.refreshCost6;
      festivalExchangeInfo.refreshCosts[7] = festivalExchangeInfo.refreshCost7;
      festivalExchangeInfo.refreshCosts[8] = festivalExchangeInfo.refreshCost8;
      festivalExchangeInfo.refreshCosts[9] = festivalExchangeInfo.refreshCost9;
    }
  }

  public FestivalExchangeInfo GetData(string id)
  {
    if (this.datas.ContainsKey(id))
      return this.datas[id];
    return (FestivalExchangeInfo) null;
  }

  public FestivalExchangeInfo GetData(int internalId)
  {
    if (this.dicByUniqueId.ContainsKey(internalId))
      return this.dicByUniqueId[internalId];
    return (FestivalExchangeInfo) null;
  }

  public void Clear()
  {
    this.datas = (Dictionary<string, FestivalExchangeInfo>) null;
    this.dicByUniqueId = (Dictionary<int, FestivalExchangeInfo>) null;
  }
}
