﻿// Decompiled with JetBrains decompiler
// Type: PersonalActivityDetailDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using I2.Loc;
using System.Collections.Generic;
using UI;

public class PersonalActivityDetailDlg : UI.Dialog
{
  public ActivityStageRank stageRank;
  public PersonalActivityCurrentStepDetail detail;
  public UILabel title;
  public UILabel remainTime;
  public UIButtonBar bar;
  private ActivityBaseData _data;
  public UILabel[] tabLabels;

  public void OnViewTotalRewards()
  {
    TimeLimitAllRewardPopup.Parameter parameter = new TimeLimitAllRewardPopup.Parameter()
    {
      isTotalReward = true,
      subTitleString = Utils.XLAT("event_overall_rankings_name")
    };
    parameter.data = this._data;
    UIManager.inst.OpenPopup("Activity/TimeLimitAllRewardPopup", (Popup.PopupParameter) parameter);
  }

  public void OnViewRanking()
  {
    UIManager.inst.OpenPopup("Activity/ActivityRankPopup", (Popup.PopupParameter) new TimeLimitActivityRankPopup.Parameter()
    {
      isStepRank = false,
      data = this._data
    });
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    this._data = (orgParam as PersonalActivityDetailDlg.Parameter).data;
    this.UpdateUI();
    this.bar.OnSelectedHandler += new System.Action<int>(this.OnSelectedChange);
    Oscillator.Instance.secondEvent += new System.Action<int>(this.Process);
  }

  private void Process(int time)
  {
    if (this._data == null)
      return;
    this.remainTime.text = ScriptLocalization.GetWithPara("event_finishes_num", new Dictionary<string, string>()
    {
      {
        "1",
        Utils.FormatTime(this._data.RemainTime, true, false, true)
      }
    }, true);
  }

  private void OnSelectedChange(int index)
  {
    NGUITools.SetActive(this.detail.gameObject, index == 0);
    NGUITools.SetActive(this.stageRank.gameObject, index == 1);
  }

  private void UpdateUI()
  {
    this.title.text = ScriptLocalization.Get("event_solo_event_title", true);
    this.remainTime.text = ScriptLocalization.GetWithPara("event_finishes_num", new Dictionary<string, string>()
    {
      {
        "1",
        Utils.FormatTime(this._data.RemainTime, true, false, true)
      }
    }, true);
    PersonalActivityData data = this._data as PersonalActivityData;
    UILabel tabLabel1 = this.tabLabels[0];
    string str1 = Utils.XLAT("event_solo_event_stage_name") + " " + PlayerData.inst.CityData.mStronghold.mLevel.ToString();
    this.tabLabels[1].text = str1;
    string str2 = str1;
    tabLabel1.text = str2;
    UILabel tabLabel2 = this.tabLabels[2];
    string str3 = Utils.XLAT("event_overall_ranking_name") + " " + (data.CurrentRank <= 0 ? "~" : data.CurrentRank.ToString());
    this.tabLabels[3].text = str3;
    string str4 = str3;
    tabLabel2.text = str4;
    this.detail.UpdateUI(this._data);
    this.stageRank.UpdateUI(this._data);
    this.bar.SelectedIndex = 0;
  }

  public void OnViewHowToGetPoints()
  {
    UIManager.inst.OpenPopup("Activity/AllianceEventHowToPopup", (Popup.PopupParameter) new AllianceActivityRequirementPopup.Parameter()
    {
      data = this._data
    });
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.bar.OnSelectedHandler -= new System.Action<int>(this.OnSelectedChange);
    if (Oscillator.IsAvailable)
      Oscillator.Instance.secondEvent -= new System.Action<int>(this.Process);
    this._data = (ActivityBaseData) null;
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public ActivityBaseData data;
  }
}
