﻿// Decompiled with JetBrains decompiler
// Type: RtmPushSystem
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using Rtm.Connection;
using Rtm.Rpc;
using Rtm.Rpc.Adapter;
using System;
using System.Collections;
using System.Collections.Generic;
using Thrift.Collections;

public class RtmPushSystem : IPushSystem
{
  private int _lastMsgOnAuth = -1;
  public const int MTYPE_NORMAL = 100;
  public const int MTYPE_PUSH = 120;
  public const int PING_INTERVAL = 30;
  private int _sPingTime;
  private long _userChannel;
  private System.Action<bool> _connectCallback;
  private TSocketClient _sc;
  private TRtmClient _rtmc;
  private RtmCallback.Rtm_Process _rtmProcess;
  private bool _reconnect;
  public static string _lastPushMsgContent;

  public event System.Action OnConsistentError;

  public bool ConnectionState
  {
    get
    {
      if (this._rtmc != null)
        return this._rtmc.Connected;
      return false;
    }
  }

  public void Init(long userChannel)
  {
    this._userChannel = userChannel;
    this._reconnect = false;
    this._lastMsgOnAuth = RtmCallback.LastTimeMark = -1;
    RtmPushSystem._lastPushMsgContent = string.Empty;
  }

  private void InitRtm()
  {
    if (this._sc == null)
    {
      bool flag = false;
      string rtmUrl = NetApi.inst.RTM_URL;
      ArrayList pushSystemNodes = NetApi.inst.PushSystemNodes;
      if (pushSystemNodes != null && pushSystemNodes.Count > 0)
      {
        int index = UnityEngine.Random.Range(0, pushSystemNodes.Count);
        rtmUrl = pushSystemNodes[index] as string;
      }
      if (!string.IsNullOrEmpty(rtmUrl))
      {
        try
        {
          this._sc = new TSocketClient(rtmUrl);
          this._sc.SetConnectionTimeout(NetApi.inst.ConnectTimout);
          this._sc.Timeout = NetApi.inst.PushSystemTimeout;
          NetApi.inst.RTM_URL = rtmUrl;
          flag = true;
        }
        catch (Exception ex)
        {
          NetApi.inst.RTM_Status = NetApi.RTMSTATE.ConnectionFail;
          NetApi.inst.RTMError = ex.Message;
        }
      }
    }
    if (this._sc == null)
    {
      if (this._connectCallback == null)
        return;
      this._connectCallback(false);
    }
    else
    {
      if (this._rtmProcess == null)
        this._rtmProcess = new RtmCallback.Rtm_Process(new System.Action(this.Disconnect));
      if (this._rtmc != null)
        return;
      this._rtmc = new TRtmClient(this._sc, (RtmClientProcessor) this._rtmProcess, NetApi.inst.PushSystemTimeout, 5000, NetApi.inst.PushSystemPingTimeout);
    }
  }

  private void Process(int timestamp)
  {
    if (this._rtmc == null || !this._rtmc.Connected || timestamp - this._sPingTime <= 30)
      return;
    this._sPingTime = timestamp;
    this.Ping();
  }

  private void ResetRtm()
  {
    this._sc = (TSocketClient) null;
    this._reconnect = false;
    this._rtmProcess = (RtmCallback.Rtm_Process) null;
    if (this._rtmc == null)
      return;
    this._rtmc.Close();
    this._rtmc = (TRtmClient) null;
  }

  private void Disconnect()
  {
    if (this._connectCallback == null)
      return;
    this._connectCallback(false);
  }

  private void Auth()
  {
    if (this._rtmc == null)
      return;
    NetApi.inst.RTM_Status = NetApi.RTMSTATE.InAuth;
    this._rtmc.client.Auth(NetApi.inst.PushSystemId, this._userChannel, NetApi.inst.RtmToken, (_RtmClientCallBack) RtmCallback.PopRtm_Auth(new System.Action<bool>(this.AuthCallback)));
    this._lastMsgOnAuth = RtmCallback.LastTimeMark;
  }

  private void AuthCallback(bool ret)
  {
    if (ret)
    {
      if (this._reconnect)
        this.CheckConsistency();
      this._reconnect = true;
    }
    NetApi.inst.RTM_Status = ret ? NetApi.RTMSTATE.Success : NetApi.RTMSTATE.AuthFail;
    if (this._connectCallback == null)
      return;
    this._connectCallback(ret);
  }

  private void Ping()
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.Ping((_RtmClientCallBack) RtmCallback.PopRtm_Ping());
  }

  private void ClearUnread()
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.CheckUnread((_RtmClientCallBack) RtmCallback.PopRtm_ClearUnread());
  }

  private void CheckUnread()
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.CheckUnread((_RtmClientCallBack) RtmCallback.PopRtm_CheckUnread(new System.Action<List<MsgParam>>(this.UnreadMsg)));
  }

  private void UnreadMsg(List<MsgParam> hlist)
  {
    if (this._rtmc == null || hlist == null)
      return;
    for (int index = 0; index < hlist.Count; ++index)
      this._rtmc.client.HistoryMsg(hlist[index], (_RtmClientCallBack) RtmCallback.PopRtm_UnreadMsg());
  }

  public void Dispose()
  {
    this.ResetRtm();
    this._lastMsgOnAuth = RtmCallback.LastTimeMark = -1;
    RtmPushSystem._lastPushMsgContent = string.Empty;
    this._connectCallback = (System.Action<bool>) null;
    this._sPingTime = 0;
    this._userChannel = 0L;
  }

  public void DoConnect(List<long> ids, System.Action<bool> callback)
  {
    if (this._rtmc == null || !this._rtmc.Connected)
    {
      this._connectCallback = callback;
      NetApi.inst.RTM_Status = NetApi.RTMSTATE.InConnection;
      this.InitRtm();
      if (this._rtmc != null)
        this._rtmc.AsyncConnect(new RtmConnectionCallback(this.AsyncConnectCallback));
    }
    if (this._rtmc == null || !this._rtmc.Connected)
      return;
    this.Auth();
  }

  private void AsyncConnectCallback(bool flag, string msg)
  {
    if (flag)
    {
      NetApi.inst.RTM_Status = NetApi.RTMSTATE.ConnectionSuccess;
      this.Auth();
    }
    else
    {
      NetApi.inst.RTM_Status = NetApi.RTMSTATE.ConnectionFail;
      this.Disconnect();
    }
  }

  public void GroupJoin(long id, System.Action<bool> callback)
  {
    if (callback == null)
      return;
    callback(true);
  }

  public void GroupLeave(long id, System.Action<bool> callback)
  {
    if (callback == null)
      return;
    callback(true);
  }

  public void Send(long id, object msg, System.Action<bool> callback)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.Send(id, (byte) 100, Utils.Object2Json(msg), (_RtmClientCallBack) RtmCallback.PopRtm_Send(callback));
  }

  public void SendGroup(long id, object msg, System.Action<bool> callback)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.SendGroup(id, (byte) 100, Utils.Object2Json(msg), (_RtmClientCallBack) RtmCallback.PopRtm_SendGroup(callback));
  }

  public void PushGroup(long id, object msg, System.Action<bool> callback)
  {
    if (this._rtmc == null || msg == null)
      return;
    this._rtmc.client.SendGroup(id, (byte) 120, msg.ToString(), (_RtmClientCallBack) RtmCallback.PopRtm_SendGroup(callback));
  }

  public void GetOnlineUsers(List<long> uids, System.Action<List<long>> callback)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.GetOnlineUsers(uids, (_RtmClientCallBack) RtmCallback.PopRtm_GetOnlineUsers(callback));
  }

  public void HistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.HistoryMsgNew(new MsgParamNew()
    {
      Msg_type = (byte) 1,
      From_xid = id,
      Num = num,
      Offset = offset,
      Mtypes = new THashSet<byte>() { mType }
    }, (_RtmClientCallBack) RtmCallback.PopRtm_HistoryMsg(callback));
  }

  public void GroupHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback, byte mType)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.HistoryMsgNew(new MsgParamNew()
    {
      Msg_type = (byte) 2,
      From_xid = id,
      Num = num,
      Offset = offset,
      Mtypes = new THashSet<byte>() { mType }
    }, (_RtmClientCallBack) RtmCallback.PopRtm_HistoryMsg(callback));
  }

  public void PersenalHistoryMessage(long id, int num, long offset, System.Action<MsgResult> callback)
  {
    if (this._rtmc == null)
      return;
    this._rtmc.client.HistoryMsgP2P(new MsgParam()
    {
      Msg_type = (byte) 1,
      From_xid = id,
      Num = num,
      Offset = offset
    }, (_RtmClientCallBack) RtmCallback.PopRtm_HistoryMsg(callback));
  }

  public void CheckConsistency()
  {
    if (RtmPushSystem._lastPushMsgContent == null)
      return;
    this.HistoryMessage(100L, 1, -1L, new System.Action<MsgResult>(this.ConsistencyCheckCallback), (byte) 90);
  }

  private void ConsistencyCheckCallback(MsgResult result)
  {
    if (result == null || result.Msgs == null || 1 > result.Msgs.Count)
      return;
    MsgContent msg = result.Msgs[0];
    if (msg == null || RtmPushSystem._lastPushMsgContent == null || (string.Compare(msg.Body, RtmPushSystem._lastPushMsgContent) == 0 || this.OnConsistentError == null))
      return;
    this.OnConsistentError();
  }
}
