﻿// Decompiled with JetBrains decompiler
// Type: RenderTextureSwitch
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RenderTextureSwitch : MonoBehaviour
{
  public string lowLevelLayer = "NGUI";
  public Camera renderCamera;
  public GameObject objectRoot;
  public GameObject canvas;

  public bool CheckSystemSupport()
  {
    return SystemInfo.supportsRenderTextures;
  }

  public void SwitchToLowLevel()
  {
    this.objectRoot.layer = LayerMask.NameToLayer(this.lowLevelLayer);
    this.renderCamera.gameObject.SetActive(false);
    this.canvas.SetActive(false);
  }

  public void CheckSystemAndSwitch()
  {
    if (this.CheckSystemSupport())
      return;
    this.SwitchToLowLevel();
  }

  private void Awake()
  {
    this.CheckSystemAndSwitch();
  }
}
