﻿// Decompiled with JetBrains decompiler
// Type: ChatRoomMemberList
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class ChatRoomMemberList : MonoBehaviour
{
  private ChatRoomMemberList.Data data = new ChatRoomMemberList.Data();
  public UITable contentTable;
  [SerializeField]
  private ChatRoomMemberList.Panel panel;

  public void Setup(long roomId)
  {
    this.data.roomId = roomId;
    this.data.roomData = DBManager.inst.DB_room.Get(this.data.roomId);
    this.Refresh();
  }

  public void Refresh()
  {
    ChatRoomMemberContainer.param.roomId = this.data.roomId;
    this.panel.owner.Setup(ChatRoomMemberContainer.param);
    this.panel.admin.Setup(ChatRoomMemberContainer.param);
    this.panel.member.Setup(ChatRoomMemberContainer.param);
    Dictionary<long, ChatRoomMember>.ValueCollection.Enumerator enumerator = this.data.roomData.members.Values.GetEnumerator();
    while (enumerator.MoveNext())
    {
      if (enumerator.Current.uid == PlayerData.inst.uid)
      {
        if (enumerator.Current.title > ChatRoomMember.Title.member)
        {
          this.panel.baned.gameObject.SetActive(true);
          this.panel.baned.Setup(ChatRoomMemberContainer.param);
          break;
        }
        this.panel.baned.gameObject.SetActive(false);
        break;
      }
    }
    Utils.ExecuteInSecs(0.1f, (System.Action) (() =>
    {
      if (!((UnityEngine.Object) this.contentTable != (UnityEngine.Object) null))
        return;
      this.contentTable.Reposition();
      this.GetComponent<UIScrollView>().ResetPosition();
    }));
  }

  public void Reset()
  {
    this.panel.owner = this.transform.Find("Table/0_OwnerContainer").gameObject.GetComponent<ChatRoomMemberContainer>();
    this.panel.owner.title = ChatRoomMember.Title.owner;
    this.panel.owner.titleText = "Owner*";
    this.panel.owner.titleIconName = "star_rank";
    this.panel.owner.Reset();
    this.panel.admin = this.transform.Find("Table/1_AdminContainer").gameObject.GetComponent<ChatRoomMemberContainer>();
    this.panel.admin.title = ChatRoomMember.Title.admin;
    this.panel.admin.titleText = "Admin*";
    this.panel.admin.titleIconName = "icon_crown";
    this.panel.admin.Reset();
    this.panel.member = this.transform.Find("Table/2_MemberContainer").gameObject.GetComponent<ChatRoomMemberContainer>();
    this.panel.member.title = ChatRoomMember.Title.member;
    this.panel.member.titleText = "Member*";
    this.panel.member.titleIconName = "alliance_members_icon";
    this.panel.member.Reset();
    this.panel.baned = this.transform.Find("Table/3_BanedContainer").gameObject.GetComponent<ChatRoomMemberContainer>();
    this.panel.baned.title = ChatRoomMember.Title.blackList;
    this.panel.baned.titleText = "Baned*";
    this.panel.baned.titleIconName = "alliance_members_icon";
    this.panel.baned.Reset();
  }

  public void OnRoomDataChanged(long roomId)
  {
    if (roomId != this.data.roomId)
      return;
    this.Setup(roomId);
  }

  public void OnRoomMemberDataChanged(long roomId, long uid)
  {
    if (roomId != this.data.roomId)
      return;
    this.Setup(roomId);
  }

  private void OnRoomRemoved(long roomId)
  {
    if (roomId != this.data.roomId)
      return;
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
  }

  public class Data
  {
    public long roomId;
    public ChatRoomData roomData;
  }

  [Serializable]
  public class Panel
  {
    public ChatRoomMemberContainer owner;
    public ChatRoomMemberContainer admin;
    public ChatRoomMemberContainer member;
    public ChatRoomMemberContainer baned;
  }
}
