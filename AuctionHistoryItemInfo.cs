﻿// Decompiled with JetBrains decompiler
// Type: AuctionHistoryItemInfo
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AuctionHistoryItemInfo
{
  private int _historyTime;
  private Dictionary<string, string> _historyParams;
  private AuctionHelper.AuctionHistoryType _historyType;

  public AuctionHistoryItemInfo(AuctionHelper.AuctionHistoryType historyType, int historyTime, Dictionary<string, string> historyParams)
  {
    this._historyType = historyType;
    this._historyTime = historyTime;
    this._historyParams = historyParams;
  }

  public AuctionHelper.AuctionHistoryType HistoryType
  {
    get
    {
      return this._historyType;
    }
  }

  public int HistoryTime
  {
    get
    {
      return this._historyTime;
    }
  }

  public Dictionary<string, string> HistoryParams
  {
    get
    {
      return this._historyParams;
    }
  }
}
