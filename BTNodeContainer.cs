﻿// Decompiled with JetBrains decompiler
// Type: BTNodeContainer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class BTNodeContainer : BTNode
{
  protected List<BTNode> _children = new List<BTNode>();

  public void AddNode(BTNode node)
  {
    if (this.HasNode(node))
      this.RemoveNode(node);
    this._children.Add(node);
  }

  public bool HasNode(BTNode node)
  {
    return this._children.Contains(node);
  }

  public void RemoveNode(BTNode node)
  {
    if (!this.HasNode(node))
      return;
    this._children.Remove(node);
  }

  public override void Reset()
  {
    for (int index = 0; index < this._children.Count; ++index)
      this._children[index].Reset();
    base.Reset();
  }
}
