﻿// Decompiled with JetBrains decompiler
// Type: DragonLevelUpPopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UI;
using UnityEngine;

public class DragonLevelUpPopup : Popup
{
  private int curLevel;
  private DragonView _dragonView;
  [SerializeField]
  private DragonLevelUpPopup.Panel panel;

  public void OnCloseButtonClick()
  {
    this.StartCoroutine(this.PlayCloseAnimation());
    UIManager.inst.ClosePopup(this.ID, (Popup.PopupParameter) null);
  }

  public void OnDragonSkillButtonClick()
  {
    this.OnCloseButtonClick();
    UIManager.inst.OpenDlg("Dragon/DragonSkillDlg", (UI.Dialog.DialogParameter) null, true, true, true);
  }

  [DebuggerHidden]
  private IEnumerator PlayCloseAnimation()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DragonLevelUpPopup.\u003CPlayCloseAnimation\u003Ec__Iterator51()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CreateDragon()
  {
    this.ReleaseDragon();
    DragonViewData dragonViewData = DragonUtils.GetDragonViewData(PlayerData.inst.dragonData, 0);
    this._dragonView = new DragonView();
    this._dragonView.Mount(this.panel.dragonroot.transform, Vector3.zero, Quaternion.Euler(Vector3.one), Vector3.one);
    this._dragonView.LoadAsync((SpriteViewData) dragonViewData, true, (System.Action<bool, UnityEngine.Object>) ((ret, asset) =>
    {
      if (!ret)
        return;
      this._dragonView.SetRendererLayer(this.panel.dragonroot.layer);
    }));
    this._dragonView.OnStateChange += new System.Action<SpriteView.State, SpriteView.State>(this.OnDragonViewStateChange);
  }

  private void OnDragonViewStateChange(SpriteView.State arg1, SpriteView.State arg2)
  {
    if (arg1 != SpriteView.State.Initialized || arg2 != SpriteView.State.Loaded)
      ;
  }

  private void ReleaseDragon()
  {
    if (this._dragonView == null)
      return;
    this._dragonView.Dispose();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    TutorialManager.Instance.Pause();
    AudioManager.Instance.PlaySound("sfx_ui_dragon_levelup", false);
    this.curLevel = (orgParam as DragonLevelUpPopup.Parameter).level;
    float num1 = 0.0f;
    if (this.curLevel > 1)
      num1 = ConfigManager.inst.DB_ConfigDragon.GetDragonInfo("dragon_level_" + (this.curLevel - 1).ToString()).level_boost;
    DragonInfo dragonInfo1 = ConfigManager.inst.DB_ConfigDragon.GetDragonInfo("dragon_level_" + this.curLevel.ToString());
    this.panel.level.text = ScriptLocalization.Get("barracks_02_uppercase_level", true) + " " + (object) dragonInfo1.level;
    string normalString3 = Utils.ConvertNumberToNormalString3(((double) dragonInfo1.level_boost - (double) num1) * 100.0);
    this.panel.lightPower.text = "+" + normalString3 + "%";
    this.panel.darkPower.text = "+" + normalString3 + "%";
    if (dragonInfo1.Rewards.rewards != null)
    {
      int index = 0;
      using (Dictionary<string, int>.Enumerator enumerator = dragonInfo1.Rewards.rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          int internalId = int.Parse(current.Key);
          int num2 = current.Value;
          ItemRewardInfo.Data d = new ItemRewardInfo.Data();
          ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(internalId);
          d.name = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
          d.icon = itemStaticInfo.ImagePath;
          d.count = (float) num2;
          d.quality = itemStaticInfo.Quality;
          d.itemId = itemStaticInfo.internalId;
          this.panel.items[index].SeedData(d);
          ++index;
          if (index == 3)
            break;
        }
      }
    }
    int num3;
    if (this.curLevel == 1)
    {
      num3 = dragonInfo1.skill_slot;
    }
    else
    {
      DragonInfo dragonInfo2 = ConfigManager.inst.DB_ConfigDragon.GetDragonInfo("dragon_level_" + (this.curLevel - 1).ToString());
      num3 = dragonInfo1.skill_slot - dragonInfo2.skill_slot;
    }
    List<ConfigDragonSkillMainInfo> all = ConfigManager.inst.DB_ConfigDragonSkillMain.DragonSkills.FindAll((Predicate<ConfigDragonSkillMainInfo>) (d =>
    {
      if (this.curLevel == d.req_dragon_level)
        return 1 == d.level;
      return false;
    }));
    for (int index = 0; index < 2; ++index)
    {
      if (index >= all.Count)
      {
        this.panel.unlockSkills[index].gameObject.SetActive(false);
      }
      else
      {
        UnlockSkillInfo.Data d = new UnlockSkillInfo.Data();
        ConfigDragonSkillGroupInfo dragonSkillGroupInfo = ConfigManager.inst.DB_ConfigDragonSkillGroup.GetDragonSkillGroupInfo(all[index].group_id);
        d.name = Utils.XLAT(dragonSkillGroupInfo.localization_name);
        d.icon = dragonSkillGroupInfo.IconPath;
        this.panel.unlockSkills[index].SeedData(d);
      }
    }
    if (num3 != 0)
    {
      this.panel.unlockSlot.SeedData(new ItemRewardInfo.Data()
      {
        count = (float) num3
      });
      this.panel.notip.gameObject.SetActive(false);
    }
    else
    {
      if (all.Count == 0)
      {
        this.panel.notip.gameObject.SetActive(true);
      }
      else
      {
        this.panel.notip.gameObject.SetActive(false);
        Vector3 localPosition = this.panel.unlockSkillGroup.localPosition;
        localPosition.x = this.panel.unlockSlot.transform.localPosition.x;
        this.panel.unlockSkillGroup.localPosition = localPosition;
      }
      this.panel.unlockSlot.gameObject.SetActive(false);
    }
    this.CreateDragon();
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "uid"] = (object) PlayerData.inst.uid;
    hashtable[(object) "value"] = (object) this.curLevel;
    hashtable[(object) "name"] = (object) "last_notify_dragon_level";
    DBManager.inst.DB_Stats.Update((object) hashtable, (long) (NetServerTime.inst.UpdateTime * 1000.0));
    PlayerData.inst.dragonController.ClaimDragonLevelup(this.curLevel);
    BuilderFactory.Instance.HandyBuild((UIWidget) this.panel.headerbg, "Texture/lord_level_up_banner", (System.Action<bool>) null, true, true, string.Empty);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    this.ReleaseDragon();
    TutorialManager.Instance.Resume();
    CitadelSystem.inst.OpenDragonLevelUpPopUp();
  }

  [Serializable]
  protected class Panel
  {
    public List<UnlockSkillInfo> unlockSkills = new List<UnlockSkillInfo>(2);
    public List<ItemRewardInfo> items = new List<ItemRewardInfo>(3);
    public UILabel level;
    public UILabel lightPower;
    public UILabel darkPower;
    public UILabel notip;
    public GameObject dragonroot;
    public ItemRewardInfo unlockSlot;
    public Transform unlockSkillGroup;
    public UITexture headerbg;
  }

  public class Parameter : Popup.PopupParameter
  {
    public int level;
  }
}
