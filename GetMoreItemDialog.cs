﻿// Decompiled with JetBrains decompiler
// Type: GetMoreItemDialog
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using UI;
using UnityEngine;

public class GetMoreItemDialog : GetMoreDialog
{
  [SerializeField]
  private UILabel m_Title;
  [SerializeField]
  private UILabel m_Require;
  [SerializeField]
  private UILabel m_Amount;
  private ShopStaticInfo m_ShopInfo;
  private double m_Needed;

  private bool Compare(ShopStaticInfo shopInfo, object extra)
  {
    return shopInfo.internalId == this.m_ShopInfo.internalId;
  }

  private void UpdateUI()
  {
    ItemStaticInfo itemStaticInfo = ConfigManager.inst.DB_Items.GetItem(this.m_ShopInfo.Item_InternalId);
    string str = ScriptLocalization.Get(itemStaticInfo.Loc_Name_Id, true);
    int itemCount = ItemBag.Instance.GetItemCount(itemStaticInfo.ID);
    this.m_Title.text = str.ToUpper();
    this.m_Require.text = string.Format("{0} Required:", (object) str);
    this.m_Amount.text = string.Format("{0}/{1}", (object) itemCount, (object) (int) this.m_Needed);
  }

  private void OnDataChanged(int itemId)
  {
    if (itemId != this.m_ShopInfo.Item_InternalId)
      return;
    this.UpdateUI();
  }

  public override bool CanBuyAndUse()
  {
    return false;
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
  }

  public override void OnOpen(UIControler.UIParameter orgParam)
  {
    base.OnOpen(orgParam);
    GetMoreItemDialog.Parameter parameter = orgParam as GetMoreItemDialog.Parameter;
    this.m_ShopInfo = parameter.shopInfo;
    this.m_Needed = parameter.needed;
    DBManager.inst.DB_Item.onDataChanged += new System.Action<int>(this.OnDataChanged);
    this.UpdateUI();
    this.UpdateData(new GetMoreDialog.CompareMethod(this.Compare), (object) null);
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    base.OnClose(orgParam);
    DBManager.inst.DB_Item.onDataChanged -= new System.Action<int>(this.OnDataChanged);
  }

  public class Parameter : UI.Dialog.DialogParameter
  {
    public ShopStaticInfo shopInfo;
    public double needed;
  }
}
