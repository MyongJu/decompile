﻿// Decompiled with JetBrains decompiler
// Type: SystemTeleportInvitePopup
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Pathfinding.Serialization.JsonFx;
using System.Collections;
using UI;

public class SystemTeleportInvitePopup : BaseReportPopup
{
  private SystemTeleportInvitePopup.SystemTeleportInviteData m_SystemTeleportInviteData;
  public UILabel m_Content;
  public UILabel m_AllianceName;
  public AllianceSymbol m_AllianceSymbol;
  public UIButton m_TeleportButton;

  public void OnTeleportClicked()
  {
    AllianceData allianceData = PlayerData.inst.allianceData;
    if (allianceData != null)
    {
      Hashtable postData = new Hashtable();
      postData[(object) "target_uid"] = (object) allianceData.creatorId;
      MessageHub.inst.GetPortByAction("Player:loadUserBasicInfo").SendRequest(postData, (System.Action<bool, object>) ((ret, data) =>
      {
        if (!ret)
          return;
        if (DBManager.inst.DB_City.GetByUid(allianceData.creatorId).cityLocation.K == PlayerData.inst.playerCityData.cityLocation.K)
          SystemTeleportInvitePopup.TakeAction();
        else
          UIManager.inst.toast.Show(Utils.XLAT("toast_excalibur_war_tele_leader_not_in_kingdom"), (System.Action) null, 4f, false);
      }), true);
    }
    else
      SystemTeleportInvitePopup.TakeAction();
  }

  private static void TakeAction()
  {
    UIManager.inst.CloseDlg((UI.Dialog.DialogParameter) null);
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    if (TeleportHighlight.HasAllianceTeleport())
      TeleportManager.Instance.OnInviteTeleport(TeleportMode.ALLIANCE_TELEPORT);
    else
      TeleportManager.Instance.OnInviteTeleport(TeleportMode.ADVANCE_TELEPORT);
  }

  private void UpdateUI()
  {
    this.m_Content.text = this.maildata.GetBodyString();
    this.m_AllianceSymbol.SetSymbols(this.m_SystemTeleportInviteData.symbol_code);
    this.m_AllianceName.text = string.Format("({0}){1}", (object) this.m_SystemTeleportInviteData.acronym, (object) this.m_SystemTeleportInviteData.name);
    this.UpdateButton();
  }

  private void UpdateButton()
  {
    long allianceId = PlayerData.inst.allianceId;
    this.m_TeleportButton.isEnabled = allianceId != 0L && allianceId == this.m_SystemTeleportInviteData.alliance_id;
  }

  private void Update()
  {
    if (UIManager.inst.IsTopPopup(this.ID))
      this.m_AllianceSymbol.gameObject.SetActive(true);
    else
      this.m_AllianceSymbol.gameObject.SetActive(false);
    this.UpdateButton();
  }

  public override void OnShow(UIControler.UIParameter orgParam)
  {
    base.OnShow(orgParam);
    this.m_SystemTeleportInviteData = JsonReader.Deserialize<SystemTeleportInvitePopup.SystemTeleportInviteData>(Utils.Object2Json(this.param.hashtable[(object) "data"]));
    this.UpdateUI();
  }

  public override void OnHide(UIControler.UIParameter orgParam)
  {
    base.OnHide(orgParam);
  }

  public class SystemTeleportInviteData
  {
    public int symbol_code;
    public string name;
    public string acronym;
    public long alliance_id;
  }
}
