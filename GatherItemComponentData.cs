﻿// Decompiled with JetBrains decompiler
// Type: GatherItemComponentData
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class GatherItemComponentData : IComponentData
{
  public Hashtable dragon_attr;
  public Hashtable rewards;
  public Hashtable param;

  public GatherItemComponentData(Hashtable dragon_attr, Hashtable rewards, Hashtable param = null)
  {
    this.dragon_attr = dragon_attr;
    this.rewards = rewards;
    this.param = param;
  }
}
