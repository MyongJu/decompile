﻿// Decompiled with JetBrains decompiler
// Type: DragSpin
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragSpin : MonoBehaviour
{
  public float speed = 1f;
  public bool selfUpRotation = true;
  public Transform target;
  private Transform _trans;

  private void Start()
  {
    this._trans = this.transform;
  }

  private void OnDrag(Vector2 delta)
  {
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
    if (this.selfUpRotation)
      this._trans.Rotate(Vector3.up, -0.5f * delta.x * this.speed);
    else if ((Object) this.target != (Object) null)
      this.target.localRotation = Quaternion.Euler(0.0f, -0.5f * delta.x * this.speed, 0.0f) * this.target.localRotation;
    else
      this._trans.localRotation = Quaternion.Euler(0.0f, -0.5f * delta.x * this.speed, 0.0f) * this._trans.localRotation;
  }
}
