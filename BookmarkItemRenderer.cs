﻿// Decompiled with JetBrains decompiler
// Type: BookmarkItemRenderer
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using System.Collections;
using UI;
using UnityEngine;

public class BookmarkItemRenderer : MonoBehaviour
{
  public UIButton BT_shared;
  public UISprite m_TileIcon;
  public UILabel m_TileName;
  public UILabel m_K;
  public UILabel m_X;
  public UILabel m_Y;
  public UIToggle m_Select;
  private Bookmark m_Bookmark;
  private System.Action m_Callback;

  public void SetData(Bookmark bookmark, System.Action callback)
  {
    this.m_Bookmark = bookmark;
    this.m_Callback = callback;
    this.m_Select.Set(false);
    this.UpdateUI();
  }

  public Bookmark Bookmark
  {
    get
    {
      return this.m_Bookmark;
    }
  }

  public bool Selected
  {
    get
    {
      return this.m_Select.value;
    }
    set
    {
      this.m_Select.value = value;
    }
  }

  public void UpdateUI()
  {
    this.m_TileIcon.spriteName = Utils.GetIconSpriteName(this.m_Bookmark.type);
    this.m_TileName.text = this.m_Bookmark.markName;
    this.m_K.text = this.m_Bookmark.coordinate.K.ToString();
    this.m_X.text = this.m_Bookmark.coordinate.X.ToString();
    this.m_Y.text = this.m_Bookmark.coordinate.Y.ToString();
    this.BT_shared.isEnabled = PlayerData.inst.allianceId != 0L;
  }

  public void OnSelectChanged()
  {
    if (this.m_Callback == null)
      return;
    this.m_Callback();
  }

  public void OnEditClick()
  {
    UIManager.inst.OpenPopup("Bookmark/BookmarkUpdatePopup", (Popup.PopupParameter) new BookmarkUpdatePopup.Parameter()
    {
      bookmark = this.m_Bookmark
    });
  }

  public void OnDeleteClick()
  {
    UIManager.inst.OpenPopup("Bookmark/BookmarkDeletePopup", (Popup.PopupParameter) new BookmarkDeletePopup.Parameter()
    {
      onYes = (BookmarkDeletePopup.OnYesCallback) (() =>
      {
        Hashtable postData = this.m_Bookmark.Encode();
        postData.Add((object) "uid", (object) PlayerData.inst.uid);
        MessageHub.inst.GetPortByAction("Player:deleteBookmark").SendRequest(postData, (System.Action<bool, object>) null, true);
      })
    });
  }

  public void OnGotoClick()
  {
    UIManager.inst.CloseAllPopup((Popup.PopupParameter) null);
    PVPSystem.Instance.Map.GotoLocation(this.m_Bookmark.coordinate, false);
    PVPSystem.Instance.Map.ShowCircleAt(this.m_Bookmark.coordinate);
  }

  public void OnSharedClick()
  {
    if (NetServerTime.inst.ServerTimestamp - PlayerPrefsEx.GetInt("bookmarch_shared_time") > 30)
    {
      PlayerPrefsEx.SetInt("bookmarch_shared_time", NetServerTime.inst.ServerTimestamp);
      PlayerPrefsEx.Save();
      ChatMessageManager.SendBookmark(this.m_Bookmark.coordinate);
      ToastManager.Instance.AddBasicToast("toast_coordinates_shared_success");
    }
    else
      UIManager.inst.toast.Show(Utils.XLAT("toast_share_coordinates_warning"), (System.Action) null, 4f, false);
  }
}
