﻿// Decompiled with JetBrains decompiler
// Type: MessageHub
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using Rtm.Connection;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

public class MessageHub
{
  private Dictionary<string, HubPort> _dic = new Dictionary<string, HubPort>(512);
  public bool showMsgBody = true;
  private static MessageHub _instance;
  private ObjectPool<MessageInfo> _mInfoPool;
  private ConcurrentQueue<MessageInfo> _recordQueue;
  private ConcurrentQueue<MessageHub.HistoryMsgReslut> _historyMsgQueue;

  public static MessageHub inst
  {
    get
    {
      if (MessageHub._instance == null)
        MessageHub._instance = new MessageHub();
      return MessageHub._instance;
    }
  }

  public void Init()
  {
    Oscillator.Instance.updateEvent += new System.Action<double>(this.Process);
    this._mInfoPool = new ObjectPool<MessageInfo>();
    this._recordQueue = new ConcurrentQueue<MessageInfo>();
    this._historyMsgQueue = new ConcurrentQueue<MessageHub.HistoryMsgReslut>();
  }

  private void Process(double timestamp)
  {
    this.ProcessHistoryMessage();
    MessageInfo result1;
    MessageInfo result2;
    if (this._recordQueue == null || !this._recordQueue.TryPeek(out result1) || !this._recordQueue.TryDequeue(out result2))
      return;
    this.PushMessage(result2);
  }

  private void ShowMessageLog(MessageInfo msg)
  {
    if (msg.Action != null && msg.Action.Equals("Player:ping") || !this.showMsgBody)
      ;
  }

  private void PushMessage(MessageInfo msgInfo)
  {
    if (msgInfo.TimeStamp > 0L)
    {
      NetServerTime.inst.SetServerTime((double) msgInfo.TimeStamp / 1000.0);
      if (msgInfo.Data != null)
        DBManager.inst.ReciveDatas(msgInfo.Data, msgInfo.TimeStamp);
    }
    if (msgInfo.Action != null)
      this.GetPortByAction(msgInfo.Action).MergeMessage(msgInfo.Payload);
    if (msgInfo.Callback != null)
      msgInfo.Callback(msgInfo.Ret, msgInfo.Payload);
    this.ReleaseMsgInfo(msgInfo);
  }

  public void OnMessage(MessageInfo msgInfo)
  {
    if (msgInfo != null)
      this._recordQueue.Enqueue(msgInfo);
    else
      this.OnError(nameof (OnMessage), "msgInfo is null");
  }

  public void OnError(string action, string error)
  {
  }

  public MessageInfo PopMsgInfo()
  {
    return this._mInfoPool.Allocate();
  }

  public void ReleaseMsgInfo(MessageInfo msgInfo)
  {
    msgInfo.Dispose();
    this._mInfoPool.Release(msgInfo);
  }

  public HubPort GetPortByAction(string action)
  {
    if (!this._dic.ContainsKey(action))
      this._dic[action] = new HubPort(action);
    return this._dic[action];
  }

  public void Dispose()
  {
    if (Oscillator.IsAvailable)
      Oscillator.Instance.updateEvent -= new System.Action<double>(this.Process);
    IDictionaryEnumerator enumerator = (IDictionaryEnumerator) this._dic.GetEnumerator();
    while (enumerator.MoveNext())
      (enumerator.Entry.Value as HubPort).Dispose();
    this._dic.Clear();
    if (this._mInfoPool != null)
    {
      this._mInfoPool.Clear();
      this._mInfoPool = (ObjectPool<MessageInfo>) null;
    }
    this._recordQueue = (ConcurrentQueue<MessageInfo>) null;
    this._historyMsgQueue = (ConcurrentQueue<MessageHub.HistoryMsgReslut>) null;
  }

  public void PushHistoryResult(MessageHub.HistoryMsgReslut result)
  {
    if (result == null)
      return;
    this._historyMsgQueue.Enqueue(result);
  }

  private void ProcessHistoryMessage()
  {
    if (this._historyMsgQueue == null)
      return;
    MessageHub.HistoryMsgReslut result;
    while (this._historyMsgQueue.TryDequeue(out result))
    {
      if (result != null)
      {
        result.Call();
        result.Dispose();
      }
    }
  }

  public class HistoryMsgReslut
  {
    public System.Action<MsgResult> callback;
    public MsgResult param;

    public void Call()
    {
      if (this.callback == null)
        return;
      this.callback(this.param);
    }

    public void Dispose()
    {
      this.callback = (System.Action<MsgResult>) null;
      this.param = (MsgResult) null;
    }
  }
}
