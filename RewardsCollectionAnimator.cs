﻿// Decompiled with JetBrains decompiler
// Type: RewardsCollectionAnimator
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class RewardsCollectionAnimator : MonoBehaviour
{
  [HideInInspector]
  public List<ItemRewardInfo.Data> items = new List<ItemRewardInfo.Data>();
  [HideInInspector]
  public List<ResRewardsInfo.Data> Ress = new List<ResRewardsInfo.Data>(5);
  [HideInInspector]
  public List<Item2RewardInfo.Data> items2 = new List<Item2RewardInfo.Data>();
  [HideInInspector]
  public List<RewardScoreInfo.Data> scores = new List<RewardScoreInfo.Data>();
  public float spreadTime = 1f;
  public float holdTime = 0.5f;
  public float foldTime = 1f;
  public float delayTime = 0.2f;
  public float flyTime = 0.2f;
  public int maxnuminoneline = 4;
  public float gapTime = 2f;
  public float itemGapX = 300f;
  public float itemGapY = 300f;
  public float resGapX = 300f;
  public float resGapY = 300f;
  private const string collectPrefab = "Prefab/UI/Common/RewardsCollection";
  public Transform itemsRoot;
  public Transform xpRoot;
  public Transform powerRoot;
  public Transform resRoot;
  public Transform scoreRoot;
  public Transform itemsEnd;
  public Transform xpEnd;
  public Transform powerEnd;
  public Transform resEnd;
  public Transform scoreEnd;
  public GameObject blocker;
  private static RewardsCollectionAnimator _instance;
  private bool needBlocker;
  private int mXP;
  private int mPower;
  private bool canLateUpdate;
  private bool needCollectItem;
  private bool needCollectItem2;
  private bool needCollectXP;
  private bool needCollectPower;
  private bool needCollectCoin;
  private bool needCollectScore;
  private bool needCollectRes;

  public static RewardsCollectionAnimator Instance
  {
    get
    {
      if ((UnityEngine.Object) RewardsCollectionAnimator._instance == (UnityEngine.Object) null)
      {
        RewardsCollectionAnimator._instance = Utils.DuplicateGOB(AssetManager.Instance.HandyLoad("Prefab/UI/Common/RewardsCollection", (System.Type) null) as GameObject, UIManager.inst.ui2DOverlayCamera.transform).GetComponent<RewardsCollectionAnimator>();
        RewardsCollectionAnimator._instance.blocker.SetActive(false);
        if ((UnityEngine.Object) null == (UnityEngine.Object) RewardsCollectionAnimator._instance)
          throw new ArgumentException("RewardsCollectionAnimator hasn't been created yet.");
      }
      return RewardsCollectionAnimator._instance;
    }
  }

  public void Clear()
  {
    this.items.Clear();
    this.items2.Clear();
    this.Ress.Clear();
    this.scores.Clear();
  }

  public void LateUpdate()
  {
    if (!this.canLateUpdate)
      return;
    this.canLateUpdate = false;
    RewardsCollectionAnimationClip collectionAnimationClip = new GameObject("messi")
    {
      transform = {
        parent = this.transform,
        localScale = Vector3.one
      }
    }.AddComponent<RewardsCollectionAnimationClip>();
    collectionAnimationClip.onAllCollectFinished += new System.Action<GameObject>(this.OnClipFinished);
    this.blocker.SetActive(this.needBlocker);
    int num = 0;
    if (this.needCollectXP)
    {
      collectionAnimationClip.mXP = this.mXP;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectXP(0.0f));
      ++num;
      this.needCollectXP = false;
    }
    if (this.needCollectPower)
    {
      collectionAnimationClip.mPower = this.mPower;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectPower(0.0f));
      ++num;
      this.needCollectPower = false;
    }
    if (this.needCollectItem)
    {
      collectionAnimationClip.items = this.items;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectItem(this.gapTime * (float) num));
      ++num;
      this.needCollectItem = false;
      this.items = new List<ItemRewardInfo.Data>();
    }
    if (this.needCollectItem2)
    {
      collectionAnimationClip.items2 = this.items2;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectItem2(this.gapTime * (float) num));
      ++num;
      this.needCollectItem2 = false;
      this.items2 = new List<Item2RewardInfo.Data>();
    }
    if (this.needCollectRes)
    {
      collectionAnimationClip.Ress = this.Ress;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectRes(this.gapTime * (float) num));
      ++num;
      this.needCollectRes = false;
      this.Ress = new List<ResRewardsInfo.Data>();
    }
    if (this.needCollectCoin)
    {
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectCoins(0.0f));
      ++num;
      this.needCollectCoin = false;
    }
    if (this.needCollectScore)
    {
      collectionAnimationClip.scores = this.scores;
      collectionAnimationClip.StartCoroutine(collectionAnimationClip.BeginCollectScore(this.gapTime * (float) num));
      ++num;
      this.needCollectScore = false;
      this.scores = new List<RewardScoreInfo.Data>();
    }
    float time = (float) ((double) (num - 1) * (double) this.gapTime + (double) this.spreadTime + (double) this.holdTime + (double) this.foldTime + 0.5);
    collectionAnimationClip.StartCoroutine(collectionAnimationClip.InvokeFinishCallback(time));
  }

  private void OnClipFinished(GameObject tar)
  {
    tar.transform.parent = (Transform) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) tar);
    this.blocker.SetActive(false);
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      Transform child = this.transform.GetChild(index);
      if ("canbedelete" == child.name)
      {
        child.gameObject.SetActive(false);
        UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
      }
    }
  }

  public bool NeedBlocker
  {
    get
    {
      return this.needBlocker;
    }
  }

  public void CollectItems(bool showBlocker = true)
  {
    if (this.items.Count == 0)
      return;
    this.canLateUpdate = true;
    this.needCollectItem = true;
    this.needBlocker = showBlocker;
  }

  public void CollectItems2(bool showBlocker = true)
  {
    if (this.items2.Count == 0)
      return;
    this.canLateUpdate = true;
    this.needCollectItem2 = true;
    this.needBlocker = showBlocker;
  }

  public void CollectScore(bool showBlocker = true)
  {
    if (this.scores.Count == 0)
      return;
    this.canLateUpdate = true;
    this.needCollectScore = true;
    this.needBlocker = showBlocker;
  }

  public void CollectXP(int xp, bool showBlocker = true)
  {
    if (xp == 0)
      return;
    this.mXP = xp;
    this.canLateUpdate = true;
    this.needCollectXP = true;
    this.needBlocker = showBlocker;
  }

  public void CollectPower(int power, bool showBlocker = true)
  {
    if (power == 0)
      return;
    this.mPower = power;
    this.canLateUpdate = true;
    this.needCollectPower = true;
    this.needBlocker = showBlocker;
  }

  public void CollectResource(bool showBlocker = true)
  {
    if (this.Ress.Count == 0)
      return;
    this.canLateUpdate = true;
    this.needCollectRes = true;
    this.needBlocker = showBlocker;
  }

  public void CollectCoins()
  {
    this.canLateUpdate = true;
    this.needCollectCoin = true;
  }

  internal ResRewardsInfo.Data GetRRID(KeyValuePair<string, int> sd)
  {
    if ("food" == sd.Key)
      return new ResRewardsInfo.Data()
      {
        count = sd.Value,
        rt = ResRewardsInfo.ResType.Food
      };
    if ("wood" == sd.Key)
      return new ResRewardsInfo.Data()
      {
        count = sd.Value,
        rt = ResRewardsInfo.ResType.Wood
      };
    if ("ore" == sd.Key)
      return new ResRewardsInfo.Data()
      {
        count = sd.Value,
        rt = ResRewardsInfo.ResType.Ore
      };
    if ("silver" == sd.Key)
      return new ResRewardsInfo.Data()
      {
        count = sd.Value,
        rt = ResRewardsInfo.ResType.Silver
      };
    if (!("steel" == sd.Key))
      return (ResRewardsInfo.Data) null;
    return new ResRewardsInfo.Data()
    {
      count = sd.Value,
      rt = ResRewardsInfo.ResType.Steel
    };
  }
}
