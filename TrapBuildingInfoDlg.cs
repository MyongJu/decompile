﻿// Decompiled with JetBrains decompiler
// Type: TrapBuildingInfoDlg
// Assembly: Assembly-CSharp, Version=4.12.6598.34475, Culture=neutral, PublicKeyToken=null
// MVID: 117C5EA5-1942-44C4-A271-22ED43202EF1
// Assembly location: D:\Work\apk_reversing\assets\bin\Data\Managed\Assembly-CSharp.dll

using DB;
using I2.Loc;
using System;
using System.Collections.Generic;
using UI;

public class TrapBuildingInfoDlg : BuildingInfoBaseDlg
{
  public UILabel contentLabel;
  public UITexture texture1;
  public UILabel nameLabel1;
  public UILabel valueLabel1;
  public UITexture texture2;
  public UILabel nameLabel2;
  public UILabel valueLabel2;

  protected override void UpdateUI()
  {
    base.UpdateUI();
    List<Unit_StatisticsInfo> allUnitsCanTrain = BarracksManager.Instance.GetAllUnitsCanTrain(this.buildingInfo.Type);
    Unit_StatisticsInfo unitStatisticsInfo = (Unit_StatisticsInfo) null;
    for (int index = 0; index < allUnitsCanTrain.Count; ++index)
    {
      if (allUnitsCanTrain[index].Requirement_Value_1 > (double) this.buildingInfo.Building_Lvl)
      {
        unitStatisticsInfo = allUnitsCanTrain[index];
        break;
      }
    }
    if (unitStatisticsInfo != null)
    {
      Dictionary<string, string> para = new Dictionary<string, string>();
      para["2"] = Utils.XLAT(unitStatisticsInfo.Troop_Name_LOC_ID);
      para["1"] = unitStatisticsInfo.Requirement_Value_1.ToString();
      this.contentLabel.text = ScriptLocalization.GetWithPara("barracks_building_info_upgrade_bonuses_description", para, true);
    }
    else
      this.contentLabel.text = Utils.XLAT("trap_factory_building_info_upgrade_bonuses_complete");
    PropertyDefinition dbProperty1 = ConfigManager.inst.DB_Properties[int.Parse(this.buildingInfo.Benefit_ID_1)];
    this.nameLabel1.text = dbProperty1.Name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture1, dbProperty1.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float total1 = DBManager.inst.DB_Local_Benefit.Get("prop_trap_capacity_base_value").total;
    string BenefitCalcID1 = "calc_trap_capacity";
    int finalData1 = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(total1, BenefitCalcID1);
    string str1 = (double) finalData1 < (double) total1 ? "-" : "+";
    this.valueLabel1.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) total1) + "[-][54AE07]" + str1 + Utils.ConvertNumberToNormalString((long) Math.Abs((float) finalData1 - total1)) + "[-]";
    PropertyDefinition dbProperty2 = ConfigManager.inst.DB_Properties[int.Parse(this.buildingInfo.Benefit_ID_2)];
    this.nameLabel2.text = dbProperty2.Name;
    BuilderFactory.Instance.HandyBuild((UIWidget) this.texture2, dbProperty2.ImagePath, (System.Action<bool>) null, true, false, string.Empty);
    float total2 = DBManager.inst.DB_Local_Benefit.Get("prop_trap_training_capacity_base_value").total;
    string BenefitCalcID2 = "calc_trap_training_capacity";
    int finalData2 = (int) ConfigManager.inst.DB_BenefitCalc.GetFinalData(total2, BenefitCalcID2);
    string str2 = (double) finalData1 < (double) total1 ? "-" : "+";
    this.valueLabel2.text = "[B4B4B4]" + Utils.ConvertNumberToNormalString((long) total2) + "[-][54AE07]" + str1 + Utils.ConvertNumberToNormalString((long) Math.Abs((float) finalData2 - total2)) + "[-]";
  }

  public override void OnClose(UIControler.UIParameter orgParam)
  {
    BuilderFactory.Instance.Release((UIWidget) this.texture1);
    base.OnClose(orgParam);
  }
}
